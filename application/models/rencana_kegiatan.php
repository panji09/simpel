<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Rencana_kegiatan extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
    {
        parent::__construct();
        $this->load->model('model_pelatihan');
        $this->load->model('model_lpp');
        $this->load->model('model_narasumber');
        $this->load->model('model_request_pelatihan');
        $this->load->model('model_rencana_kegiatan');
        if( ($this->session->userdata('logged_in')!=1) )
        {
            redirect('admin/login');
        }
    }

	public function index()
    {
    	$data['username'] = $this->session->userdata('username');
		$data['role'] = $this->session->userdata('role');

		$data['notiftotalPending'] = $this->model_narasumber->countNotifPendingRequest();
		$data['notiflisttotalPending'] = $this->model_narasumber->getNotifPendingRequest();

		$data['notiftotalApproved'] = $this->model_narasumber->countNotifApprovedRequest();
		$data['notiflisttotalApproved'] = $this->model_narasumber->getNotifApprovedRequest();

		$data['notiftotalCanceled'] = $this->model_narasumber->countNotifCanceledRequest();
		$data['notiflisttotalCanceled'] = $this->model_narasumber->getNotifCanceledRequest();

		$data['renKegiatan'] = $this->model_rencana_kegiatan->getRencanaKegiatan();

		$this->load->view('layout_admin/header',$data);
		$this->load->view('narasumber/rencana_kegiatan');
		$this->load->view('layout_admin/footer');
    }

    public function the_rencana_kegiatan()
    {
    	$data['username'] = $this->session->userdata('username');
		$data['role'] = $this->session->userdata('role');

		//$data['tema_pelatihan'] = $this->model_pelatihan->getAllTemaPelatihan();

		$data['notiftotalPending'] = $this->model_request_pelatihan->countNotifPendingRequest();
		$data['notiflisttotalPending'] = $this->model_request_pelatihan->getNotifPendingRequest();

		$data['notiftotalApproved'] = $this->model_request_pelatihan->countNotifApprovedRequest();
		$data['notiflisttotalApproved'] = $this->model_request_pelatihan->getNotifApprovedRequest();

		$data['notiftotalCanceled'] = $this->model_request_pelatihan->countNotifCanceledRequest();
		$data['notiflisttotalCanceled'] = $this->model_request_pelatihan->getNotifCanceledRequest();

		$this->load->view('layout_admin/header',$data);
		$this->load->view('narasumber/add_rencana');
		$this->load->view('layout_admin/footer');
    }

    public function add_rencana_kegiatan()
    {
    	/* $data['username'] = $this->session->userdata('username');
		$data['role'] = $this->session->userdata('role');

		$data['notiftotalPending'] = $this->model_narasumber->countNotifPendingRequest();
		$data['notiflisttotalPending'] = $this->model_narasumber->getNotifPendingRequest();

		$data['notiftotalApproved'] = $this->model_narasumber->countNotifApprovedRequest();
		$data['notiflisttotalApproved'] = $this->model_narasumber->getNotifApprovedRequest();

		$data['notiftotalCanceled'] = $this->model_narasumber->countNotifCanceledRequest();
		$data['notiflisttotalCanceled'] = $this->model_narasumber->getNotifCanceledRequest(); */
 
		//$data['addKegiatan'] = $this->model_rencana_kegiatan->addRencanaKegiatan();		

    	//$this->form_validation->set_rules('id_narasumber', 'id_narasumber', 'required');
		$this->form_validation->set_rules('dari_tanggal', 'dari_tanggal', 'required');
		$this->form_validation->set_rules('sampai_tanggal', 'sampai_tanggal', 'required');
		$this->form_validation->set_rules('jumlah_jp', 'jumlah_jp', 'required');
		$this->form_validation->set_rules('keterangan', 'keterangan', 'required');

		if ($this->form_validation->run() == FALSE)
		{
			$this->session->set_flashdata('status', '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.validation_errors().'</div>');
				redirect('rencana_kegiatan/the_rencana_kegiatan',$data);
		}else{
			$data = $this->model_rencana_kegiatan->addRencanaKegiatan();
			if($data){
				//$data['status'] = "success";
				$this->session->set_flashdata('status', '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Insert Data Berhasil</div>');
				redirect('rencana_kegiatan/the_rencana_kegiatan',$data);
		}else{
			//$data['status'] = "failed";
			$this->session->set_flashdata('status', '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Gagal Insert Data, Coba Beberapa Saat Lagi</div>');
			redirect('rencana_kegiatan/the_rencana_kegiatan',$data);
			}
		}
    }

}