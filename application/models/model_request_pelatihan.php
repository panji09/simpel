<?php
class Model_request_pelatihan extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }
    
    public function getAllRequest()
    {
        $this->db->select('*');
        $this->db->join('tbl_tema_pelatihan', 'tbl_tema_pelatihan.id_tema = tbl_request_pelatihan.id_tema');
        $this->db->from('tbl_request_pelatihan');

        $query = $this->db->get();

        return $query->result();
    }
    public function countAllRequest()
    {
        $query = $this->db->query("SELECT (SELECT COUNT(*) FROM (select * from tbl_request_pelatihan) AS ROW) AS ROW");
        $id_key = $query->row();

        if($id_key!=null){
            return $id_key->ROW;
        }else{
            return 0;
        }
    }
    public function countAllRequestByIdLpp($id_lpp)
    {
        $query = $this->db->query("SELECT (SELECT COUNT(*) FROM (select * from tbl_request_pelatihan WHERE id_lpp='$id_lpp') AS ROW) AS ROW");
        $id_key = $query->row();

        if($id_key!=null){
            return $id_key->ROW;
        }else{
            return 0;
        }
    }
    public function getAllRequestWhere($stat)
    {
        $this->db->select('*');
        $this->db->join('tbl_tema_pelatihan', 'tbl_tema_pelatihan.id_tema = tbl_request_pelatihan.id_tema');
        $this->db->where('tbl_request_pelatihan.approve',$stat);
        $this->db->from('tbl_request_pelatihan');

        $query = $this->db->get();

        return $query->result();
    }
    public function addNewRequest()
    {
        $tema_pelatihan = $this->input->post('tema_pelatihan');
        $dari_tanggal = $this->input->post('dari_tanggal');
        $sampai_tanggal = $this->input->post('sampai_tanggal');
        $isi_pelatihan = $this->input->post('isi_pelatihan');
        $tempat_pelatihan = $this->input->post('tempat_pelatihan');
        $id = $this->session->userdata('id');
        $date = date('Y-m-d');

        if($this->db->query("INSERT INTO tbl_request_pelatihan VALUES('','$date','$tema_pelatihan','$isi_pelatihan','$tempat_pelatihan','$dari_tanggal','$sampai_tanggal','lpp','$id','0','0','')")){
            return true;
        }else{
            return false;
        }
    }

    public function getApprovedRequest()
    {
        $this->db->select('*');
        $this->db->join('tbl_tema_pelatihan', 'tbl_tema_pelatihan.id_tema = tbl_request_pelatihan.id_tema');
        $this->db->where('approve','1');
        $this->db->from('tbl_request_pelatihan');

        $query = $this->db->get();

        return $query->result();
    }
    public function countApprovedRequest()
    {
        $query = $this->db->query("SELECT (SELECT COUNT(*) FROM (select * from tbl_request_pelatihan WHERE approve = '1') AS ROW) AS ROW");
        $id_key = $query->row();

        if($id_key!=null){
            return $id_key->ROW;
        }else{
            return 0;
        }
    }
    public function getCanceledRequest()
    {
        $this->db->select('*');
        $this->db->where('approve','2');
        $this->db->from('tbl_request_pelatihan');

        $query = $this->db->get();

        return $query->result();
    }
    public function countCanceledRequest()
    {
        $query = $this->db->query("SELECT (SELECT COUNT(*) FROM (select * from tbl_request_pelatihan WHERE approve = '2') AS ROW) AS ROW");
        $id_key = $query->row();

        if($id_key!=null){
            return $id_key->ROW;
        }else{
            return 0;
        }
    }
    public function getPendingRequest()
    {
        $this->db->select('*');
        $this->db->where('approve','0');
        $this->db->from('tbl_request_pelatihan');

        $query = $this->db->get();

        return $query->result();
    }
    public function countPendingRequest()
    {
        $query = $this->db->query("SELECT (SELECT COUNT(*) FROM (select * from tbl_request_pelatihan WHERE approve = '0') AS ROW) AS ROW");
        $id_key = $query->row();

        if($id_key!=null){
            return $id_key->ROW;
        }else{
            return 0;
        }
    }


    public function getNotifApprovedRequest()
    {
        $this->db->select('*');
        $this->db->join('tbl_tema_pelatihan', 'tbl_tema_pelatihan.id_tema = tbl_request_pelatihan.id_tema');
        $this->db->where('tbl_request_pelatihan.approve','1');
        $this->db->where('tbl_request_pelatihan.read','0');
        $this->db->from('tbl_request_pelatihan');

        $query = $this->db->get();

        return $query->result();
    }
    public function countNotifApprovedRequest()
    {
        $query = $this->db->query("SELECT (SELECT COUNT(*) FROM (SELECT * FROM  `tbl_request_pelatihan` WHERE  `approve` =1 AND  `read` =0) AS ROW) AS ROW");
        $id_key = $query->row();

        if($id_key!=null){
            return $id_key->ROW;
        }else{
            return 0;
        }
    }
    public function getNotifPendingRequest()
    {
        $this->db->select('*');
        $this->db->join('tbl_tema_pelatihan', 'tbl_tema_pelatihan.id_tema = tbl_request_pelatihan.id_tema');
        $this->db->where('tbl_request_pelatihan.approve','0');
        $this->db->where('tbl_request_pelatihan.read','0');
        $this->db->from('tbl_request_pelatihan');

        $query = $this->db->get();

        return $query->result();
    }
    public function countNotifPendingRequest()
    {
        $query = $this->db->query("SELECT (SELECT COUNT(*) FROM (SELECT * FROM  `tbl_request_pelatihan` WHERE  `approve` =0 AND  `read` =0) AS ROW) AS ROW");
        $id_key = $query->row();

        if($id_key!=null){
            return $id_key->ROW;
        }else{
            return 0;
        }
    }
    public function getNotifCanceledRequest()
    {
        $this->db->select('*');
        $this->db->join('tbl_tema_pelatihan', 'tbl_tema_pelatihan.id_tema = tbl_request_pelatihan.id_tema');
        $this->db->where('tbl_request_pelatihan.approve','2');
        $this->db->where('tbl_request_pelatihan.read','0');
        $this->db->from('tbl_request_pelatihan');

        $query = $this->db->get();

        return $query->result();
    }
    public function countNotifCanceledRequest()
    {
        $query = $this->db->query("SELECT (SELECT COUNT(*) FROM (SELECT * FROM  `tbl_request_pelatihan` WHERE  `approve` =2 AND  `read` =0) AS ROW) AS ROW");
        $id_key = $query->row();

        if($id_key!=null){
            return $id_key->ROW;
        }else{
            return 0;
        }
    }

    public function setRead($val,$id)
    {
        $data = array(
               'readed' => $val
            );

        $this->db->where('id_pelatihan', $id);
        $this->db->update('tbl_pelatihan', $data); 
    }

    public function setReadAll($val)
    {
        $data = array(
               'readed' => $val
            );

        $this->db->where('approve_pelatihan', '2');
        $this->db->update('tbl_pelatihan', $data); 

        return true;
    }

    public function getDetailRequestById($id)
    {
        $this->db->select('*');
        $this->db->join('tbl_tema_pelatihan', 'tbl_tema_pelatihan.id_tema = tbl_request_pelatihan.id_tema');
        $this->db->where('tbl_request_pelatihan.id_request',$id);
        $this->db->from('tbl_request_pelatihan');

        $query = $this->db->get();

        return $query->result();
    }
    public function approveRequest()
    {
        $id_request = $this->input->post('id_request');
        $id_lpp = $this->input->post('id_lpp');
        $tema_pelatihan = $this->input->post('tema_pelatihan');
        $dari_tanggal = $this->input->post('dari_tanggal');
        $sampai_tanggal = $this->input->post('sampai_tanggal');
        $isi_pelatihan = $this->input->post('isi_pelatihan');
        $instansi = $this->input->post('instansi');
        if(isset($instansi)){
            $instansi = $this->input->post('instansi');
        }else{
            $instansi = "";
        }
        $tempat_pelatihan = $this->input->post('tempat_pelatihan');
        $id = $this->session->userdata('id');
        $date = date('Y-m-d');
        $approve = $this->input->post('submit');
        $alasan_ditolak = $this->input->post('alasan_ditolak');

        if($approve=="approve"){
            if($this->db->query("INSERT INTO tbl_pelatihan VALUES('','$tema_pelatihan','$isi_pelatihan','$tempat_pelatihan','$dari_tanggal','$sampai_tanggal','$instansi','lkpp','$id_lpp','$id_request','$date')")){

                $this->updateApprovalRequest('1',$id_request,'');
                return "success_approve";
            }else{
                return "gagal_input";
            }

        }else if($approve=="cancel"){
            
            $this->updateApprovalRequest('2',$id_request,$alasan_ditolak);
            return "success_tolak";
        } 
    }

    public function approveRequestSchedule()
    {
        $id_request = $this->input->post('id_request');
        $id_lpp = $this->input->post('id_lpp');
        $tema_pelatihan = $this->input->post('tema_pelatihan');
        $dari_tanggal = $this->input->post('dari_tanggal');
        $sampai_tanggal = $this->input->post('sampai_tanggal');
        $isi_pelatihan = $this->input->post('isi_pelatihan');

        $contact_nama = $this->input->post('contact_nama');
        $contact_hp = $this->input->post('contact_hp');

        $provinsi = $this->input->post('provinsi');
        $jumlah_panitia = $this->input->post('jumlah_panitia');
        $jumlah_kelas = $this->input->post('jumlah_kelas');
        $jabatan = $this->input->post('jabatan');
        $email = $this->input->post('email');

        $instansi = $this->input->post('instansi');
        if(isset($instansi)){
            $instansi = $this->input->post('instansi');
        }else{
            $instansi = "";
        }
        $tempat_pelatihan = $this->input->post('tempat_pelatihan');
        $id = $this->session->userdata('id');
        $date = date('Y-m-d');
        $approve = $this->input->post('submit');
        $alasan_ditolak = $this->input->post('alasan_ditolak');
        $jumlah_peserta = $this->input->post('jumlah_peserta');
        $total_jp = $this->input->post('total_jp');

        $config['upload_path'] = '././assets/upload/surat_permohonan/';
        $config['allowed_types'] = 'pdf|doc|docx|xls|xlsx|ppt|pptx';
        $config['max_size'] = '100000';
        $config['overwrite'] = TRUE;

        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if ( ! $this->upload->do_upload())
        {
            $msg = $this->upload->display_errors('', '');
            $status = $msg;

            return $status;
        }
        else
        {
            $data = array('upload_data' => $this->upload->data());
            foreach ($data as $item => $value):
                $file = $value['file_name'];
            endforeach;

            if($this->db->query("INSERT INTO tbl_schedule_pelatihan VALUES(
                '',
                '$tema_pelatihan',
                '$isi_pelatihan',
                '$contact_nama',
                '$contact_hp',
                '$tempat_pelatihan',
                '$dari_tanggal',
                '$sampai_tanggal',
                '$instansi',
                'lpp',
                '$id_lpp',
                '$id_request',
                '1',
                '0',
                '$jumlah_peserta',
                '$file',
                '$date',
                '$total_jp',
                '$kota',
                '$provinsi',
                '$jumlah_panitia',
                '$jumlah_kelas',
                '$jabatan',
                '$email')")){
            
                return "success_approve";
            }else{
                return "gagal_input";
            }
            
        }

        

    }

    public function updateApprovalRequest($val,$id_request,$alasan_ditolak)
    {
        if($val=='1'){
            $update = $this->db->query("UPDATE tbl_request_pelatihan SET approve='$val' WHERE id_request='$id_request' ");
        }else{
            $update = $this->db->query("UPDATE tbl_request_pelatihan SET approve='$val', alasan_ditolak = '$alasan_ditolak' WHERE id_request='$id_request' ");
        }
        
        if($update){
            return true;
        }else{
            return false;
        }
    }
}