<?php
class Model_pelatihan extends CI_Model {

    private $sqlFindPelatihan = "";

    function __construct()
    {
        parent::__construct();
    }
    public function getNotifNarasumber($id)
    {
        $this->db->select('*');/**/
        $this->db->join('tbl_pelatihan', 'pelatihan_narasumber.id_pelatihan = tbl_pelatihan.id_pelatihan');
        $this->db->join('tbl_tema_pelatihan', 'tbl_tema_pelatihan.id_tema = tbl_pelatihan .id_tema');
        $this->db->join('tbl_narasumber_new', 'pelatihan_narasumber.id_narasumber = tbl_narasumber_new.id_narasumber');
        $this->db->where_not_in('pelatihan_narasumber.approve','0');
        $this->db->where('pelatihan_narasumber.aktif','0');
        $this->db->where('tbl_pelatihan.id_lpp',$id);
        $this->db->from('pelatihan_narasumber');

        $query = $this->db->get();

        return $query->result();
    }
    public function getNotifNarasumberFasilitator($id)
    {
        $now = date('Y-m-d');

        $this->db->select('*');/**/
        $this->db->join('tbl_pelatihan', 'pelatihan_fasilitator.id_pelatihan = tbl_pelatihan.id_pelatihan');
        $this->db->join('tbl_tema_pelatihan', 'tbl_tema_pelatihan.id_tema = tbl_pelatihan .id_tema');
        $this->db->join('tbl_fasilitator', 'tbl_fasilitator.id_fasilitator = pelatihan_fasilitator.id_fasilitator');
        $this->db->join('pelatihan_narasumber', 'pelatihan_narasumber.id_pelatihan = pelatihan_fasilitator.id_pelatihan');
        $this->db->join('tbl_narasumber_new', 'pelatihan_narasumber.id_narasumber = tbl_narasumber_new.id_narasumber');
        $this->db->where("pelatihan_narasumber.tgl_pelatihan >= '$now' ");
        //$this->db->where('pelatihan_narasumber.aktif','0');
        //$this->db->where_not_in('pelatihan_narasumber.approve','0');
        //$this->db->where('pelatihan_fasilitator.id_fasilitator',$id);
        $this->db->from('pelatihan_fasilitator');
        $this->db->limit(20);

        $query = $this->db->get();

        return $query->result();
    }
    public function countNotifNarasumber($id)
    {
        $this->db->select('*');/**/
        $this->db->join('tbl_pelatihan', 'pelatihan_narasumber.id_pelatihan = tbl_pelatihan.id_pelatihan');
        $this->db->join('tbl_tema_pelatihan', 'tbl_tema_pelatihan.id_tema = tbl_pelatihan .id_tema');
        $this->db->join('tbl_narasumber_new', 'pelatihan_narasumber.id_narasumber = tbl_narasumber_new.id_narasumber');
        $this->db->where_not_in('pelatihan_narasumber.approve','0');
        $this->db->where('pelatihan_narasumber.aktif','0');
        $this->db->where('tbl_pelatihan.id_lpp',$id);
        $this->db->from('pelatihan_narasumber');

        $query = $this->db->get();
        $rowcount = $query->num_rows();

        return $rowcount;
    }
    public function countNotifNarasumberFasilitator($id)
    {
        $this->db->select('*');/**/
        $this->db->join('tbl_pelatihan', 'pelatihan_fasilitator.id_pelatihan = tbl_pelatihan.id_pelatihan');
        $this->db->join('tbl_tema_pelatihan', 'tbl_tema_pelatihan.id_tema = tbl_pelatihan .id_tema');
        $this->db->join('tbl_fasilitator', 'tbl_fasilitator.id_fasilitator = pelatihan_fasilitator.id_fasilitator');
        $this->db->join('pelatihan_narasumber', 'pelatihan_narasumber.id_pelatihan = pelatihan_fasilitator.id_pelatihan');
        $this->db->join('tbl_narasumber_new', 'pelatihan_narasumber.id_narasumber = tbl_narasumber_new.id_narasumber');
        $this->db->where('pelatihan_narasumber.aktif','0');
        $this->db->where_not_in('pelatihan_narasumber.approve','0');
        $this->db->where('pelatihan_fasilitator.id_fasilitator',$id);
        $this->db->from('pelatihan_fasilitator');

        $query = $this->db->get();
        $rowcount = $query->num_rows();

        return $rowcount;
    }
    
    public function setAktif($id)
    {
        $data = array(
            'aktif' => '1',
        );

        $this->db->where('id', $id);
        $this->db->update('pelatihan_narasumber', $data);
    }

    public function setAktifFasilitator($id)
    {
        $data = array(
            'aktif' => '1',
        );

        $this->db->where('id', $id);
        $this->db->update('pelatihan_narasumber', $data);
    }

    public function getAllPelatihan()
    {
        $now = date('Y-m-d');

        $this->db->select('*');
        $this->db->join('tbl_tema_pelatihan', 'tbl_tema_pelatihan.id_tema = tbl_pelatihan .id_tema');
        $this->db->join('tbl_lpp', 'tbl_lpp.id_lpp = tbl_pelatihan .id_lpp');
        //$this->db->join('pelatihan_fasilitator', 'pelatihan_fasilitator.id_pelatihan = tbl_pelatihan .id_pelatihan','left outer');
        //$this->db->join('tbl_fasilitator', 'tbl_fasilitator.id_fasilitator = pelatihan_fasilitator .id_fasilitator','left outer');
        $this->db->order_by('tbl_pelatihan.id_pelatihan','DESC');
        $this->db->from('tbl_pelatihan');
        $this->db->where("tbl_pelatihan.sampai_tanggal >= '$now' ");
        $this->db->where("tbl_pelatihan.approve_pelatihan = 1 ");

        $query = $this->db->get();

        return $query->result();
    }

    public function findPelatihan()
    {
        $now = date('Y-m-d');

        $dari_tanggal = $this->input->post('dari_tanggal');
        $sampai_tanggal = $this->input->post('sampai_tanggal');
        $lpp = $this->input->post('lpp');
        $pelatihan = $this->input->post('pelatihan');
        $provinsi = $this->input->post('provinsi');

        $condition = "";

        if($dari_tanggal!=null){
            $condition .= " AND tbl_pelatihan.mulai_tanggal >= '$dari_tanggal'";
        }

        if($sampai_tanggal!=null){
            $condition .= " AND tbl_pelatihan.mulai_tanggal <= '$sampai_tanggal'";
        }

        if($lpp!=null){
            $condition .= " AND tbl_pelatihan.id_lpp = '$lpp'";
        }

        if($pelatihan!=null){
            $condition .= " AND tbl_pelatihan.id_tema = '$pelatihan'";
        }

        if($provinsi!=null){
            $condition .= " AND tbl_lpp.provinsi LIKE '%$provinsi%'";
        }
        
        $sql = "SELECT * FROM 
        tbl_pelatihan,tbl_tema_pelatihan,tbl_lpp 
        WHERE 
        tbl_pelatihan.id_tema = tbl_tema_pelatihan.id_tema AND
        tbl_pelatihan.id_lpp = tbl_lpp.id_lpp AND
        tbl_pelatihan.approve_pelatihan = 1
        ".$condition;

        $this->sqlFindPelatihan = $sql;

        $query = $this->db->query($sql);

        return $query->result();
    }

    public function getsqlFindPelatihan()
    {
        $query = $this->db->query($this->sqlFindPelatihan);
        return $query->result();
    }

    public function getArsipPelatihan()
    {
        $now = date('Y-m-d');

        $this->db->select('*');
        $this->db->join('tbl_tema_pelatihan', 'tbl_tema_pelatihan.id_tema = tbl_pelatihan .id_tema');
        $this->db->join('tbl_lpp', 'tbl_lpp.id_lpp = tbl_pelatihan .id_lpp');
        $this->db->order_by('tbl_pelatihan.id_pelatihan','DESC');
        $this->db->from('tbl_pelatihan');
        $this->db->where("tbl_pelatihan.sampai_tanggal < '$now' ");
        $this->db->where("tbl_pelatihan.approve_pelatihan = 1 ");

        $query = $this->db->get();

        return $query->result();
    }
    public function getFasilitatoriPelatihan()
    {
        $now = date('Y-m-d');

        $this->db->select('*');
        $this->db->join('tbl_tema_pelatihan', 'tbl_tema_pelatihan.id_tema = tbl_pelatihan .id_tema');
        $this->db->join('tbl_lpp', 'tbl_lpp.id_lpp = tbl_pelatihan .id_lpp');
        $this->db->from('tbl_pelatihan');
        $this->db->where("tbl_pelatihan.minta_fasilitator = '1' ");
        $this->db->where("tbl_pelatihan.fasilitatori_readed = '0' ");
        $this->db->order_by('tbl_pelatihan.id_pelatihan','DESC');

        $query = $this->db->get();

        return $query->result();
    }
    public function countFasilitatoriPelatihan()
    {
        $now = date('Y-m-d');

        $this->db->select('*');
        $this->db->join('tbl_tema_pelatihan', 'tbl_tema_pelatihan.id_tema = tbl_pelatihan .id_tema');
        $this->db->join('tbl_lpp', 'tbl_lpp.id_lpp = tbl_pelatihan .id_lpp');
        $this->db->from('tbl_pelatihan');
        $this->db->where("tbl_pelatihan.minta_fasilitator = '1' ");
        $this->db->where("tbl_pelatihan.fasilitatori_readed = '0' ");
        $this->db->order_by('tbl_pelatihan.id_pelatihan','DESC');

        $query = $this->db->get();
        $rowcount = $query->num_rows();

        return $rowcount;
    }
    public function getPelatihanDitolak()
    {
        $now = date('Y-m-d');

        $this->db->select('*');
        $this->db->join('tbl_tema_pelatihan', 'tbl_tema_pelatihan.id_tema = tbl_pelatihan .id_tema');
        $this->db->join('tbl_lpp', 'tbl_lpp.id_lpp = tbl_pelatihan .id_lpp');
        $this->db->order_by('tbl_pelatihan.id_pelatihan','DESC');
        $this->db->from('tbl_pelatihan');
        $this->db->where("tbl_pelatihan.approve_pelatihan = 2 ");

        $query = $this->db->get();

        return $query->result();
    }

    public function getAllPelatihanByIdLpp($id)
    {
        $now = date('Y-m-d');

        $this->db->select('*');
        $this->db->join('tbl_tema_pelatihan', 'tbl_tema_pelatihan.id_tema = tbl_pelatihan .id_tema');
        $this->db->from('tbl_pelatihan');
        $this->db->where('tbl_pelatihan.id_lpp',$id);
        $this->db->where_not_in('tbl_pelatihan.approve_pelatihan','2');
        $this->db->where("tbl_pelatihan.sampai_tanggal >= '$now' ");
        $this->db->order_by('tbl_pelatihan.id_pelatihan','DESC');

        $query = $this->db->get();

        return $query->result();
    }
    public function getAllPelatihanDitolakByIdLpp($id)
    {
        $now = date('Y-m-d');

        $this->db->select('*');
        $this->db->join('tbl_tema_pelatihan', 'tbl_tema_pelatihan.id_tema = tbl_pelatihan .id_tema');
        $this->db->from('tbl_pelatihan');
        $this->db->where('tbl_pelatihan.id_lpp',$id);
        $this->db->where('tbl_pelatihan.approve_pelatihan','2');
        $this->db->where("tbl_pelatihan.sampai_tanggal >= '$now' ");
        $this->db->order_by('tbl_pelatihan.id_pelatihan','DESC');

        $query = $this->db->get();

        return $query->result();
    }
    public function getArsipPelatihanByIdLpp($id)
    {
        $now = date('Y-m-d');
        
        $this->db->select('*');
        $this->db->join('tbl_tema_pelatihan', 'tbl_tema_pelatihan.id_tema = tbl_pelatihan .id_tema');
        $this->db->from('tbl_pelatihan');
        $this->db->where('tbl_pelatihan.id_lpp',$id);
        $this->db->where("tbl_pelatihan.sampai_tanggal < '$now' ");
        $this->db->order_by('tbl_pelatihan.id_pelatihan','DESC');

        $query = $this->db->get();

        return $query->result();
    }
    public function getAllSchedulePelatihanByIdLpp($id)
    {
        $this->db->select('*');
        $this->db->join('tbl_tema_pelatihan', 'tbl_tema_pelatihan.id_tema = tbl_schedule_pelatihan .id_tema');
        $this->db->from('tbl_schedule_pelatihan');
        $this->db->where('tbl_schedule_pelatihan.id_lpp',$id);
        $this->db->where_not_in('tbl_schedule_pelatihan.approved','2');
        $this->db->order_by('tbl_schedule_pelatihan.id_pelatihan','DESC');

        $query = $this->db->get();

        return $query->result();
    }
    public function getActiveSchedulePelatihanByIdLpp($id)
    {
        $this->db->select('*');
        $this->db->join('tbl_tema_pelatihan', 'tbl_tema_pelatihan.id_tema = tbl_schedule_pelatihan .id_tema');
        $this->db->from('tbl_schedule_pelatihan');
        $this->db->where('tbl_schedule_pelatihan.id_lpp',$id);
        $this->db->where('tbl_schedule_pelatihan.approved','1');
        $this->db->order_by('tbl_schedule_pelatihan.id_pelatihan','DESC');

        $query = $this->db->get();

        return $query->result();
    }
    public function getAllSchedulePelatihan()
    {
        /*$this->db->select('*');
        $this->db->join('tbl_tema_pelatihan', 'tbl_tema_pelatihan.id_tema = tbl_schedule_pelatihan .id_tema');
        $this->db->from('tbl_schedule_pelatihan');
        $this->db->where('tbl_schedule_pelatihan.approved','0');
        $this->db->order_by('tbl_schedule_pelatihan.id_pelatihan','DESC');

        $query = $this->db->get();

        return $query->result();*/
        $this->db->select('*');
        $this->db->join('tbl_tema_pelatihan', 'tbl_tema_pelatihan.id_tema = tbl_pelatihan .id_tema');
        $this->db->join('tbl_lpp', 'tbl_lpp.id_lpp = tbl_pelatihan .id_lpp');
        $this->db->from('tbl_pelatihan');
        $this->db->where('tbl_pelatihan.approve_pelatihan','0');
        $this->db->order_by('tbl_pelatihan.id_pelatihan','DESC');

        $query = $this->db->get();

        return $query->result();
    }

    public function countAllPelatihanByIdLpp($id)
    {
        $query = $this->db->query("SELECT (SELECT COUNT(*) FROM (select * from tbl_pelatihan WHERE id_lpp = '$id' AND approve_pelatihan='1') AS ROW) AS ROW");
        $id_key = $query->row();

        if($id_key!=null){
            return $id_key->ROW;
        }else{
            return 0;
        }
    }

    public function countAllPelatihanByIdFasilitator($id)
    {
        $query = $this->db->query("SELECT (SELECT COUNT(*) FROM (select * from pelatihan_fasilitator WHERE id_fasilitator = '$id') AS ROW) AS ROW");
        $id_key = $query->row();

        if($id_key!=null){
            return $id_key->ROW;
        }else{
            return 0;
        }
    }

    public function countschedule_pelatihan($id)
    {
        $query = $this->db->query("SELECT (SELECT COUNT(*) FROM (select * from tbl_schedule_pelatihan WHERE readed = '0' AND (approved='1' OR approved='2') AND  id_lpp = '$id') AS ROW) AS ROW");
        $id_key = $query->row();

        if($id_key!=null){
            return $id_key->ROW;
        }else{
            return 0;
        }
    }

    public function countpelatihanbaru($id)
    {
        $query = $this->db->query("SELECT (SELECT COUNT(*) FROM (select * from tbl_pelatihan WHERE readed = '0' AND (approve_pelatihan='1' ) AND  id_lpp = '$id') AS ROW) AS ROW");
        $id_key = $query->row();

        if($id_key!=null){
            return $id_key->ROW;
        }else{
            return 0;
        }
    }

    public function countpelatihanditolak($id)
    {
        $query = $this->db->query("SELECT (SELECT COUNT(*) FROM (select * from tbl_pelatihan WHERE readed = '0' AND (approve_pelatihan='2' ) AND  id_lpp = '$id') AS ROW) AS ROW");
        $id_key = $query->row();

        if($id_key!=null){
            return $id_key->ROW;
        }else{
            return 0;
        }
    }

    public function countScheduleToBeApproved()
    {
        $query = $this->db->query("SELECT (SELECT COUNT(*) FROM (select * from tbl_pelatihan WHERE approve_pelatihan = '0') AS ROW) AS ROW");
        $id_key = $query->row();

        if($id_key!=null){
            return $id_key->ROW;
        }else{
            return 0;
        }
    }
    
    public function getFasilitator()
    {
        $this->db->select('*');
        $this->db->from('tbl_fasilitator');

        $query = $this->db->get();

        return $query->result();
    }

    public function getFasilitatorByID($id)
    {
        $this->db->select('*');
        $this->db->from('tbl_fasilitator');
        $this->db->where('id_fasilitator',$id);

        $query = $this->db->get();

        return $query->result();
    }

    public function getAllTemaPelatihan()
    {
    	$this->db->select('*');
        $this->db->from('tbl_tema_pelatihan');

        $query = $this->db->get();

        return $query->result();
    }
    public function getAllTemaPelatihanLimit($limit, $start)
    {
        $this->db->select('*');
        $this->db->limit($limit, $start);
        $this->db->order_by('id_tema','DESC');
        $this->db->from('tbl_tema_pelatihan');

        $query = $this->db->get();

        return $query->result();
    }
    public function getAllTemaPelatihanAll()
    {
        $this->db->select('*');
        $this->db->order_by('id_tema','DESC');
        $this->db->from('tbl_tema_pelatihan');

        $query = $this->db->get();

        return $query->result();
    }
    public function getTemaPelatihanById($id)
    {
        $this->db->select('*');
        $this->db->where('id_tema',$id);
        $this->db->from('tbl_tema_pelatihan');

        $query = $this->db->get();

        return $query->result();
    }
    public function getTemaPelatihanByIdRequest($id)
    {
        $this->db->select('*');
        $this->db->join('tbl_tema_pelatihan', 'tbl_tema_pelatihan.id_tema = tbl_pelatihan .id_tema');
        $this->db->where('tbl_pelatihan.id_request',$id);
        $this->db->from('tbl_pelatihan');

        $query = $this->db->get();

        return $query->result();
    }
    public function getJudulPelatihan($id)
    {
        $this->db->select('*');
        $this->db->join('tbl_tema_pelatihan', 'tbl_tema_pelatihan.id_tema = tbl_pelatihan .id_tema');
        $this->db->where('tbl_pelatihan.id_pelatihan',$id);
        $this->db->from('tbl_pelatihan');

        $query = $this->db->get();
        $id_key = $query->row();

        return $id_key->judul_pelatihan;
    }
    public function getPelatihanByIdPelatihan($id)
    {
        $this->db->select('*');
        $this->db->join('tbl_tema_pelatihan', 'tbl_tema_pelatihan.id_tema = tbl_pelatihan .id_tema');
        $this->db->join('tbl_lpp', 'tbl_lpp.id_lpp = tbl_pelatihan.id_lpp');
        $this->db->where('tbl_pelatihan.id_pelatihan',$id);
        $this->db->from('tbl_pelatihan');

        $query = $this->db->get();

        return $query->result();
    }
    public function getScheduleByIdPelatihan($id)
    {
        $this->db->select('*');
        $this->db->join('tbl_tema_pelatihan', 'tbl_tema_pelatihan.id_tema = tbl_schedule_pelatihan .id_tema');
        $this->db->join('tbl_lpp', 'tbl_lpp.id_lpp = tbl_schedule_pelatihan.id_lpp');
        $this->db->where('tbl_schedule_pelatihan.id_pelatihan',$id);
        $this->db->from('tbl_schedule_pelatihan');

        $query = $this->db->get();

        return $query->result();
    }
    public function getPelatihanByIdPelatihanIdFasilitator($id)
    {
        $this->db->select('*');
        $this->db->join('pelatihan_fasilitator', 'pelatihan_fasilitator.id_fasilitator = tbl_fasilitator.id_fasilitator');
        $this->db->join('tbl_pelatihan', 'tbl_pelatihan.id_pelatihan = pelatihan_fasilitator .id_pelatihan');
        $this->db->join('tbl_tema_pelatihan', 'tbl_tema_pelatihan.id_tema = tbl_pelatihan .id_tema');
        $this->db->where('pelatihan_fasilitator.id_fasilitator',$id);
        $this->db->where('pelatihan_fasilitator.readed','1');
        $this->db->where('tbl_pelatihan.arsip','0');
        $this->db->from('tbl_fasilitator');

        $query = $this->db->get();

        return $query->result();
    }
    public function countPelatihanByIdPelatihanIdFasilitator($id)
    {
        $this->db->select('*');
        $this->db->join('pelatihan_fasilitator', 'pelatihan_fasilitator.id_fasilitator = tbl_fasilitator.id_fasilitator');
        $this->db->join('tbl_pelatihan', 'tbl_pelatihan.id_pelatihan = pelatihan_fasilitator .id_pelatihan');
        $this->db->join('tbl_tema_pelatihan', 'tbl_tema_pelatihan.id_tema = tbl_pelatihan .id_tema');
        $this->db->where('pelatihan_fasilitator.id_fasilitator',$id);
        $this->db->where('pelatihan_fasilitator.readed','1');
        $this->db->where('tbl_pelatihan.arsip','0');
        $this->db->from('tbl_fasilitator');

        $query = $this->db->get();

        
        $rowcount = $query->num_rows();

        return $rowcount;
    }
    public function getPelatihanByIdPelatihanIdFasilitatorArsip($id)
    {
        $this->db->select('*');
        $this->db->join('pelatihan_fasilitator', 'pelatihan_fasilitator.id_fasilitator = tbl_fasilitator.id_fasilitator');
        $this->db->join('tbl_pelatihan_arsip_fasilitator', 'tbl_pelatihan_arsip_fasilitator.id_pelatihan = pelatihan_fasilitator.id_pelatihan');
        $this->db->join('tbl_tema_pelatihan', 'tbl_tema_pelatihan.id_tema = tbl_pelatihan_arsip_fasilitator .id_tema');
        $this->db->where('pelatihan_fasilitator.id_fasilitator',$id);
        $this->db->where('pelatihan_fasilitator.readed','1');
        $this->db->from('tbl_fasilitator');

        $query = $this->db->get();

        return $query->result();
    }
    public function countArsipPelatihanByIdPelatihanIdFasilitator($id)
    {
        $this->db->select('*');
        $this->db->join('pelatihan_fasilitator', 'pelatihan_fasilitator.id_fasilitator = tbl_fasilitator.id_fasilitator');
        $this->db->join('tbl_pelatihan', 'tbl_pelatihan.id_pelatihan = pelatihan_fasilitator .id_pelatihan');
        $this->db->join('tbl_tema_pelatihan', 'tbl_tema_pelatihan.id_tema = tbl_pelatihan .id_tema');
        $this->db->where('pelatihan_fasilitator.id_fasilitator',$id);
        $this->db->where('pelatihan_fasilitator.readed','1');
        $this->db->where('tbl_pelatihan.arsip','1');
        $this->db->from('tbl_fasilitator');

        $query = $this->db->get();

        
        $rowcount = $query->num_rows();

        return $rowcount;
    }
    public function getNewPelatihanByIdPelatihanIdFasilitator($id)
    {
        $this->db->select('*');
        $this->db->join('pelatihan_fasilitator', 'pelatihan_fasilitator.id_fasilitator = tbl_fasilitator.id_fasilitator');
        $this->db->join('tbl_pelatihan', 'tbl_pelatihan.id_pelatihan = pelatihan_fasilitator .id_pelatihan');
        $this->db->join('tbl_tema_pelatihan', 'tbl_tema_pelatihan.id_tema = tbl_pelatihan .id_tema');
        $this->db->where('pelatihan_fasilitator.id_fasilitator',$id);
        $this->db->where('pelatihan_fasilitator.readed','0');
        $this->db->from('tbl_fasilitator');

        $query = $this->db->get();

        return $query->result();
    }
    public function getPelatihanNarasumberById($id_pelatihan)
    {
        $this->db->select('*');
        $this->db->join('tbl_pelatihan', 'pelatihan_narasumber.id_pelatihan = tbl_pelatihan.id_pelatihan');
        $this->db->join('tbl_narasumber', 'pelatihan_narasumber.id_narasumber = tbl_narasumber.id_narasumber');
        $this->db->join('tbl_tema_pelatihan', 'tbl_tema_pelatihan.id_tema = tbl_pelatihan.id_tema');
        $this->db->where('pelatihan_narasumber.id_pelatihan',$id_pelatihan);
        $this->db->from('pelatihan_narasumber');

        $query = $this->db->get();

        return $query->result();
    }
    public function getIdTemaWhereIdRequest($id)
    {
        $this->db->select('*');
        $this->db->where('id_request',$id);
        $this->db->from('tbl_request_form');
        $query = $this->db->get();
        $id_key = $query->row();

        return $id_key->id_tema;
    }
    public function getTanggalDariPelatihanById($id)
    {
        $this->db->select('*');
        $this->db->where('id_request',$id);
        $this->db->from('tbl_pelatihan');
        $query = $this->db->get();
        $id_key = $query->row();

        return $id_key->mulai_tanggal;
    }
    public function getTanggalSampaiPelatihanById($id)
    {
        $this->db->select('*');
        $this->db->where('id_request',$id);
        $this->db->from('tbl_pelatihan');
        $query = $this->db->get();
        $id_key = $query->row();

        return $id_key->sampai_tanggal;
    }
    public function getTanggalDariPelatihanByIdPelatihan($id)
    {
        $this->db->select('*');
        $this->db->where('id_pelatihan',$id);
        $this->db->from('tbl_pelatihan');
        $query = $this->db->get();
        $id_key = $query->row();

        return $id_key->mulai_tanggal;
    }
    public function getTanggalSampaiPelatihanByIdPelatihan($id)
    {
        $this->db->select('*');
        $this->db->where('id_pelatihan',$id);
        $this->db->from('tbl_pelatihan');
        $query = $this->db->get();
        $id_key = $query->row();

        return $id_key->sampai_tanggal;
    }
    public function addNewPelatihan()
    {
        //$id_request = $this->input->post('id_request');
        $id_request = 0;
        $tema_pelatihan = $this->input->post('tema_pelatihan');
        $dari_tanggal = $this->input->post('dari_tanggal');
        $sampai_tanggal = $this->input->post('sampai_tanggal');
        $isi_pelatihan = $this->input->post('isi_pelatihan');
        $tempat_pelatihan = $this->input->post('tempat_pelatihan');
        $instansi = $this->input->post('instansi');
        $id = $this->session->userdata('id');
        $createdBy = $this->session->userdata('role');

        if($createdBy=="lkpp"){
            $proses = $this->db->query("INSERT INTO tbl_pelatihan VALUES('','$tema_pelatihan','$isi_pelatihan','$tempat_pelatihan','$dari_tanggal','$sampai_tanggal','$createdBy','','$id','$id_request','','','')");
        }else if($createdBy=="lpp"){
           $proses = $this->db->query("INSERT INTO tbl_pelatihan VALUES('','$tema_pelatihan','$isi_pelatihan','$tempat_pelatihan','$dari_tanggal','$sampai_tanggal','$createdBy','','$id','$id_request','','','')");
        }

        if($proses){
            $this->setRequestApproved($id_request);
            return true;
        }else{
            return false;
        }
    }
    public function setRequestApproved($id_request)
    {
        $data = array(
               'approve' => '1',
            );

        $this->db->where('id_request', $id_request);
        $this->db->update('tbl_request_form', $data); 
    }
    public function cancelPelatihan($id_request,$keterangan_ditolak)
    {
        $data = array(
               'approve' => '2',
               'keterangan_ditolak' => $keterangan_ditolak,
            );

        $this->db->where('id_request', $id_request);
        $proses = $this->db->update('tbl_request_form', $data); 

        if($proses){
            return true;
        }else{
            return false;
        }
    }
    
    public function insert_new_tema_pelatihan()
    {
        $judul_tema = $this->input->post('judul_tema');
        $keterangan_tema = $this->input->post('keterangan_tema');
        $id = $this->session->userdata('id');

            $proses = $this->db->query("INSERT INTO tbl_tema_pelatihan VALUES('','$judul_tema','$keterangan_tema','lkpp','$id')");

        if($proses){
            return true;
        }else{
            return false;
        }
    }

    public function update_tema_pelatihan()
    {
        $judul_tema = $this->input->post('judul_tema');
        $keterangan_tema = $this->input->post('keterangan_tema');
        $id = $this->session->userdata('id');

            $proses = $this->db->query("UPDATE tbl_tema_pelatihan SET judul_pelatihan = '$judul_tema' , keterangan = '$keterangan_tema' ");

        if($proses){
            return true;
        }else{
            return false;
        }
    }

    public function insert_narasumber_fasilitator()
    {
        $id_pelatihan = $this->input->post('id_pelatihan');
        $id_fasilitator = $this->input->post('id_fasilitator');

        $proses = $this->db->query("INSERT INTO pelatihan_fasilitator VALUES('','$id_pelatihan','$id_fasilitator','0','')");

        if($proses){
            return true;
        }else{
            return false;
        }
    }

    public function insert_narasumber_fasilitator2($id_pelatihan)
    {
        $id_fasilitator = $this->input->post('id_fasilitator');

        $proses = $this->db->query("INSERT INTO pelatihan_fasilitator VALUES('','$id_pelatihan','$id_fasilitator','0','')");

        if($proses){
            $this->db->query("UPDATE tbl_pelatihan SET fasilitatori_readed='1' WHERE id_pelatihan='$id_pelatihan' ");
            return true;
        }else{
            return false;
        }
    }

    public function count_fasilitator_by_pelatihan($id)
    {
        $query = $this->db->query("SELECT (SELECT COUNT(*) FROM pelatihan_fasilitator  WHERE id_pelatihan = '$id') AS ROW");
        $id_key = $query->row();

        if($id_key!=null){
            return $id_key->ROW;
        }else{
            return 0;
        }
    }

    public function get_fasilitator_by_pelatihan($id)
    {
        $this->db->select('*');
        $this->db->join('pelatihan_fasilitator', 'tbl_fasilitator.id_fasilitator = pelatihan_fasilitator.id_fasilitator');
        $this->db->where('pelatihan_fasilitator.id_pelatihan',$id);
        $this->db->from('tbl_fasilitator');
        $query = $this->db->get();

        return $query->result();
    }
    public function get_materi()
    {
        $this->db->select('*');
        $this->db->from('tbl_library');
        $query = $this->db->get();

        return $query->result();
    }
    public function get_dokumen()
    {
        $this->db->select('*');
        $this->db->from('tbl_dokumen');
        $query = $this->db->get();

        return $query->result();
    }
    public function get_dokumen_where($id)
    {
        $this->db->select('*');
        $this->db->where('id_dokumen',$id);
        $this->db->from('tbl_dokumen');
        $query = $this->db->get();

        return $query->result();
    }
    public function get_kategori()
    {
        $this->db->select('*');
        $this->db->from('kategori_galeri');
        $query = $this->db->get();

        return $query->result();
    }
    public function get_foto()
    {
        $this->db->select('*');
        $this->db->from('galeri_foto');
        $this->db->join('kategori_galeri', 'kategori_galeri.id_kategori = galeri_foto.id_kategori');
        $this->db->limit(1,0);
        $this->db->order_by('id_foto','DESC');
        $query = $this->db->get();

        return $query->result();
    }
    public function get_all_foto()
    {
        $this->db->select('*');
        $this->db->from('galeri_foto');
        $this->db->join('kategori_galeri', 'kategori_galeri.id_kategori = galeri_foto.id_kategori');
        $this->db->order_by('id_foto','DESC');
        $query = $this->db->get();

        return $query->result();
    }
    public function get_fotoByID($id)
    {
        $this->db->select('*');
        $this->db->from('galeri_foto');
        $this->db->join('kategori_galeri', 'kategori_galeri.id_kategori = galeri_foto.id_kategori');
        $this->db->where('galeri_foto.id_kategori',$id);
        $this->db->order_by('id_foto','DESC');
        $query = $this->db->get();

        return $query->result();
    }
    public function get_foto_kategori()
    {
        $this->db->select('*');
        $this->db->from('galeri_foto');
        $this->db->join('kategori_galeri', 'kategori_galeri.id_kategori = galeri_foto.id_kategori');
        $this->db->group_by('galeri_foto.id_kategori');
        $query = $this->db->get();

        return $query->result();
    }
    public function get_materi_by($role)
    {
        $this->db->select('*');
        $this->db->from('tbl_library');
        $this->db->where('role',$role);
        $this->db->or_where('role','all');
        $query = $this->db->get();

        return $query->result();
    }

    public function delete_tema_pelatihan($id)
    {
        $proses = $this->db->query("DELETE FROM tbl_tema_pelatihan WHERE id_tema = '$id' ");

        if($proses){
            return true;
        }else{
            return false;
        }
    }

    public function add_fasilitator()
    {
        $nama_fasilitator = $this->input->post('nama_fasilitator');
        $proses = $this->db->query("INSERT INTO tbl_fasilitator (id_fasilitator,nama_fasilitator) VALUES ('','$nama_fasilitator','0')");

        if($proses){
            return true;
        }else{
            return false;
        }
    }

    public function update_fasilitator($id)
    {
        $nama_lengkap = $this->input->post('nama_lengkap');
        $alamat = $this->input->post('alamat');
        $no_telp = $this->input->post('no_telp');
        $tempat_lahir = $this->input->post('tempat_lahir');
        $tgl_lahir = $this->input->post('tgl_lahir');
        $gelar = $this->input->post('gelar');
        $jenis_kelamin = $this->input->post('jenis_kelamin');
        $pendidikan = $this->input->post('pendidikan');
        $instansi = $this->input->post('instansi');
        $satuan_kerja = $this->input->post('satuan_kerja');
        $provinsi = $this->input->post('provinsi');
        $kota = $this->input->post('kota');
        $email = $this->input->post('email');
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        $no_sertifikat = $this->input->post('no_sertifikat');
        $hasil_evaluasi = $this->input->post('hasil_evaluasi');

        $pass = base64_encode(md5($password));

            $proses = $this->db->query("UPDATE `tbl_fasilitator` SET 
                `nama_fasilitator` = '$nama_lengkap', 
                `alamat_fasilitator` = '$alamat', 
                `no_telp_fasilitator` = '$no_telp', 
                `tempat_lahir_fasilitator` = '$tempat_lahir', 
                `tanggal_lahir_fasilitator` = '$tgl_lahir', 
                `gelar_fasilitator` = '$gelar', 
                `jeniskel_fasilitator` = '$jenis_kelamin', 
                `pendidikan_fasilitator` = '$pendidikan', 
                `provinsi_fasilitator` = '$provinsi', 
                `kota_fasilitator` = '$kota', 
                `email_fasilitator` = '$email', 
                `status_aktif_fasilitator` = '1', 
                `instansi_fasilitator` = '$instansi', 
                `username` = '$username',
                `password` = '$pass',
                `no_telp` = '1'
                WHERE 
                `tbl_fasilitator`.`id_fasilitator` = '$id'");

        if($proses){
            return true;
        }else{
            return false;
        }
    }

    public function newRequestFasilitator($id)
    {
        $query = $this->db->query("SELECT (SELECT COUNT(*) FROM pelatihan_fasilitator  WHERE id_fasilitator = '$id' AND readed='0') AS ROW");
        $id_key = $query->row();

        if($id_key!=null){
            return $id_key->ROW;
        }else{
            return 0;
        }
    }
    public function setRequestRead($id,$param)
    {
        $data = array(
               'readed' => $param,
            );

        $this->db->where('id_pelatihan', $id);
        $this->db->update('pelatihan_fasilitator', $data); 
    }
    public function insert_new_fasilitator()
    {
        $nama_lengkap = $this->input->post('nama_lengkap');
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        /*$alamat = $this->input->post('alamat');
        $no_telp = $this->input->post('no_telp');
        $tempat_lahir = $this->input->post('tempat_lahir');
        $tgl_lahir = $this->input->post('tgl_lahir');
        $gelar = $this->input->post('gelar');
        $jenis_kelamin = $this->input->post('jenis_kelamin');
        $pendidikan = $this->input->post('pendidikan');
        $instansi = $this->input->post('instansi');
        $satuan_kerja = $this->input->post('satuan_kerja');
        $provinsi = $this->input->post('provinsi');
        $kota = $this->input->post('kota');
        $email = $this->input->post('email');*/
        /*$no_sertifikat = $this->input->post('no_sertifikat');
        $hasil_evaluasi = $this->input->post('hasil_evaluasi');

        
            $pangkat = $this->input->post('pangkat');
            if($pangkat)){
                $pangkat = $this->input->post('pangkat');
            }else{
                $pangkat = "";
            }

            $golongan = $this->input->post('golongan');
            if($golongan)){
                $golongan = $this->input->post('golongan');
            }else{
                $golongan = "";
            }

            $nip = $this->input->post('nip');
            if($nip)){
                $nip = $this->input->post('nip');
            }else{
                $nip = "";
            }
            
            $status_pegawai = $this->input->post('status_pegawai');
            if($status_pegawai)){
                $status_pegawai = $this->input->post('status_pegawai');
            }else{
                $status_pegawai = "";
            }
            
            $jenis_pegawai = $this->input->post('jenis_pegawai');
            if($jenis_pegawai)){
                $jenis_pegawai = $this->input->post('jenis_pegawai');
            }else{
                $jenis_pegawai = "";
            }
            
            $jabatan = $this->input->post('jabatan');
            if($jabatan)){
                $jabatan = $this->input->post('jabatan');
            }else{
                $jabatan = "";
            }*/
            

        $pass = base64_encode(md5($password));

        $proses = $this->db->query("INSERT INTO  `tbl_fasilitator` (
            `id_fasilitator` ,
            `nama_fasilitator` ,
            `alamat_fasilitator` ,
            `no_telp_fasilitator` ,
            `tempat_lahir_fasilitator` ,
            `tanggal_lahir_fasilitator` ,
            `nip_fasilitator` ,
            `gelar_fasilitator` ,
            `jeniskel_fasilitator` ,
            `golongan_fasilitator` ,
            `pendidikan_fasilitator` ,
            `photo_fasilitator` ,
            `pangkat_fasilitator` ,
            `provinsi_fasilitator` ,
            `kota_fasilitator` ,
            `email_fasilitator` ,
            `status_aktif_fasilitator` ,
            `username` ,
            `password` ,
            `instansi_fasilitator` ,
            `no_telp` ,
            `email`
            ) VALUES(
            '',
            '$nama_lengkap',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '$username',
            '$pass',
            '',
            '',
            '')");
        
        if($proses){
            return true;
        }else{
            return false;
        }

    }
    public function insert_new_materi()
    {
        $judul_materi = $this->input->post('judul_materi');
        $kategori_materi = $this->input->post('kategori_materi');
        $role = $this->input->post('role');

        $config['upload_path'] = '././assets/upload/materi_pelatihan/';
        $config['allowed_types'] = 'rar|zip|pdf|doc|docx|xls|xlsx|ppt|pptx|jpeg|jpg|png|bmp|gif';
        $config['max_size'] = '50000';

        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if ( ! $this->upload->do_upload())
        {
            $msg = $this->upload->display_errors('', '');
            $status = $msg;
        }
        else
        {
            $data = array('upload_data' => $this->upload->data());
            foreach ($data as $item => $value):
                $file = $value['file_name'];
            endforeach;

            $photo = $data['file_name'];
            $proses_berita = $this->db->query("INSERT INTO tbl_library VALUES('','$judul_materi','$kategori_materi','$file','$role')");

            if($proses_berita){
                $status = "success";
            }else{
                $status = "failed";
            }
            
        }

        

        return $status;
    }
    public function insert_new_dokumen()
    {
        $judul_materi = $this->input->post('judul_dokumen');
        $kategori_materi = $this->input->post('kategori_dokumen');
        $role = $this->input->post('role');

        $config['upload_path'] = '././assets/upload/dokumen/';
        $config['allowed_types'] = 'pdf|doc|docx|xls|xlsx|ppt|pptx';
        $config['max_size'] = '50000';

        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if ( ! $this->upload->do_upload())
        {
            $msg = $this->upload->display_errors('', '');
            $status = $msg;
        }
        else
        {
            $data = array('upload_data' => $this->upload->data());
            foreach ($data as $item => $value):
                $file = $value['file_name'];
            endforeach;

            $photo = $data['file_name'];
            $proses_berita = $this->db->query("INSERT INTO tbl_dokumen VALUES('','$judul_materi','$kategori_materi','$file','')");

            if($proses_berita){
                $status = "success";
            }else{
                $status = "failed";
            }
            
        }

        

        return $status;
    }
    public function insert_new_regulasi()
    {
        $judul_materi = $this->input->post('judul_dokumen');
        $kategori_materi = $this->input->post('kategori_dokumen');
        $role = $this->input->post('role');

        $config['upload_path'] = '././assets/upload/regulasi/';
        $config['allowed_types'] = 'pdf|doc|docx|xls|xlsx|ppt|pptx';
        $config['max_size'] = '50000';

        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if ( ! $this->upload->do_upload())
        {
            $msg = $this->upload->display_errors('', '');
            $status = $msg;
        }
        else
        {
            $data = array('upload_data' => $this->upload->data());
            foreach ($data as $item => $value):
                $file = $value['file_name'];
            endforeach;

            $photo = $data['file_name'];
            $proses_berita = $this->db->query("INSERT INTO tbl_regulasi VALUES('','$judul_materi','$kategori_materi','$file','')");

            if($proses_berita){
                $status = "success";
            }else{
                $status = "failed";
            }
            
        }

        

        return $status;
    }

    function update_dokumen(){
        $id_dokumen = $this->input->post('id_dokumen');
        $judul_materi = $this->input->post('judul_dokumen');
        $kategori_materi = $this->input->post('kategori_dokumen');
        $role = $this->input->post('role');

        $config['upload_path'] = '././assets/upload/dokumen/';
        $config['allowed_types'] = 'pdf|doc|docx|xls|xlsx|ppt|pptx';
        $config['max_size'] = '50000';

        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if ($cek_photo!="on") {
            if ( ! $this->upload->do_upload())
            {
                $msg = $this->upload->display_errors('', '');
                $status = $msg;
            }
            else
            {
                $data = array('upload_data' => $this->upload->data());
                foreach ($data as $item => $value):
                    $file = $value['file_name'];
                endforeach;

                $photo = $data['file_name'];
                $proses_berita = $this->db->query("UPDATE tbl_dokumen SET judul_dokumen ='$judul_materi',
                    kategori_dokumen ='$kategori_materi',
                    file_dokumen = '$file'
                    WHERE
                    id_dokumen = '$id_dokumen'");

                if($proses_berita){
                    $status = "success";
                }else{
                    $status = "failed";
                }
                
            }
        }else{
            $proses_berita = $this->db->query("UPDATE tbl_dokumen SET judul_dokumen ='$judul_materi',
                kategori_dokumen ='$kategori_materi'
                WHERE
                id_dokumen = '$id_dokumen'");

            if($proses_berita){
                $status = "success";
            }else{
                $status = "failed";
            }
        }

        

        return $status;
    }

    function update_regulasi(){
        $id_dokumen = $this->input->post('id_dokumen');
        $judul_materi = $this->input->post('judul_dokumen');
        $kategori_materi = $this->input->post('kategori_regulasi');
        $role = $this->input->post('role');

        $config['upload_path'] = '././assets/upload/regulasi/';
        $config['allowed_types'] = 'pdf|doc|docx|xls|xlsx|ppt|pptx';
        $config['max_size'] = '50000';

        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if ($cek_photo!="on") {
            if ( ! $this->upload->do_upload())
            {
                $msg = $this->upload->display_errors('', '');
                $status = $msg;
            }
            else
            {
                $data = array('upload_data' => $this->upload->data());
                foreach ($data as $item => $value):
                    $file = $value['file_name'];
                endforeach;

                $photo = $data['file_name'];
                $proses_berita = $this->db->query("UPDATE tbl_regulasi SET judul_dokumen ='$judul_materi',
                    kategori_dokumen ='$kategori_materi',
                    file_dokumen = '$file'
                    WHERE
                    id_dokumen = '$id_dokumen'");

                if($proses_berita){
                    $status = "success";
                }else{
                    $status = "failed";
                }
                
            }
        }else{
            $proses_berita = $this->db->query("UPDATE tbl_regulasi SET judul_dokumen ='$judul_materi',
                kategori_dokumen ='$kategori_materi'
                WHERE
                id_dokumen = '$id_dokumen'");

            if($proses_berita){
                $status = "success";
            }else{
                $status = "failed";
            }
        }

        

        return $status;
    }

    public function insert_new_foto()
    {
        $judul = $this->input->post('judul_foto');
        $deskripsi = $this->input->post('deskripsi_foto');

        $config['upload_path'] = '././assets/upload/galeri/';
        $config['allowed_types'] = 'jpg|jpeg|png|PNG|JPEG|JPG';
        $config['max_size'] = '10000';

        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if ( ! $this->upload->do_upload())
        {
            $msg = $this->upload->display_errors('', '');
            $status = $msg;
        }
        else
        {
            $data = array('upload_data' => $this->upload->data());
            foreach ($data as $item => $value):
                $file = $value['file_name'];
            endforeach;

            $this->resize_image('././assets/upload/galeri/'.$file,250,250);
            
            $photo = $data['file_name'];
            $proses_berita = $this->db->query("INSERT INTO galeri_foto VALUES('','$judul','','$file','$deskripsi')");

            if($proses_berita){
                $status = "success";
            }else{
                $status = "failed";
            }
            
        }

        

        return $status;
    }

    public function tolakSchedule($id,$alasan_ditolak)
    {
        $now = date('Y-m-d');
        $data = array(
               'approve_pelatihan' => '2',
               'tgl_diapprove' => $now,
               'alasan_ditolak' => $alasan_ditolak,
            );

        $this->db->where('id_pelatihan', $id);
        $this->db->update('tbl_pelatihan', $data); 

        return "success_approve";
    }
    public function approveSchedule($id)
    {
        $now = date('Y-m-d');
        $data = array(
               'approve_pelatihan' => '1',
               'tgl_diapprove' => $now,
            );

        $this->db->where('id_pelatihan', $id);
        $this->db->update('tbl_pelatihan', $data); 

        return "success_approve";

        /*$now = date('Y-m-d');
        $data = array(
               'approved' => '1',
            );

        $this->db->where('id_pelatihan', $id);
        $this->db->update('tbl_schedule_pelatihan', $data); 

        return "success_approve";*/

        /*$this->db->select('*');
        $this->db->where('tbl_schedule_pelatihan.id_pelatihan',$id);
        $this->db->from('tbl_schedule_pelatihan');
        $query = $this->db->get();
        $data_pelatihan = $query->result();

        foreach ($data_pelatihan as $data):
            $proses = $this->db->query("INSERT INTO tbl_pelatihan VALUES
                ('',
                '$data->id_tema',
                '$data->isi_pelatihan',
                '$data->tempat_pelatihan',
                '$data->mulai_tanggal',
                '$data->sampai_tanggal',
                '$data->instansi',
                '$data->createdBy',
                '$data->id_lpp',
                '',
                '$data->id_request'
                '$now')");
        endforeach;

        if($proses){
            return "success_approve";
        }else{
            return "gagal";
        }*/
    }
    public function create_pelatihan_berdasarkan_schedule()
    {
        $id = $this->input->post('id_pelatihan');
        $now = date('Y-m-d');

        $data = array(
               'approved' => '2',
            );

        $this->db->where('id_pelatihan', $id);
        $this->db->update('tbl_schedule_pelatihan', $data); 

        $this->db->select('*');
        $this->db->where('tbl_schedule_pelatihan.id_pelatihan',$id);
        $this->db->from('tbl_schedule_pelatihan');
        $query = $this->db->get();
        $data_pelatihan = $query->result();

        foreach ($data_pelatihan as $data):
            $proses = $this->db->query("INSERT INTO tbl_pelatihan VALUES
                ('',
                '$data->id_tema',
                '$data->isi_pelatihan',
                '$data->contact_nama',
                '$data->contact_hp',
                '$data->tempat_pelatihan',
                '$data->mulai_tanggal',
                '$data->sampai_tanggal',
                '$data->instansi',
                '$data->created_by',
                '$data->id_lpp',
                '$data->id_request',
                '0',
                '0',
                '$now',
                '$data->jumlah_peserta',
                '$data->surat_permohonan',
                '',
                '',
                '',
                '$data->total_jp',
                '',
                '',
                '$data->kota',
                '$data->provinsi',
                '$data->jumlah_panitia',
                '$data->jumlah_kelas',
                '$data->jabatan',
                '$data->email')");

        if($proses){
            return "success_approve";
        }else{
            return "gagal";
        }
        endforeach;

        
    }

    public function insert_peserta($id)
    {
        $id_peserta = $this->input->post('id_peserta');
        $id_pelatihan = $id;
        $nama_lengkap = $this->input->post('nama_lengkap');
        $gelar_akademis = $this->input->post('gelar_akademis');
        $jenis_kelamin = $this->input->post('jenis_kelamin');
        $NIP = $this->input->post('nip');
        $no_ktp = $this->input->post('no_ktp');
        $tempat_lahir = $this->input->post('tempat_lahir');
        $tgl_lahir = $this->input->post('tanggal_lahir');
        $pendidikan_terakhir = $this->input->post('pendidikan_terakhir');
        $status_kepegawaian = $this->input->post('status_kepegawaian');
        $instansi = $this->input->post('instansi');
        $lembaga = $this->input->post('lembaga');
        $domisili_kabupaten = $this->input->post('domisili_kabupaten');
        $domisili_provinsi = $this->input->post('domisili_provinsi');
        $no_telepon = $this->input->post('no_telepon');
        $alamat_email = $this->input->post('alamat_email');
        $pengalaman_kerja_bidang_pengadaan_barang_pemerintah = $this->input->post('pengalaman_kerja_bidang_pengadaan_barang_pemerintah');
        $pernah_mengikuti_pelatihan_barang_jasa = $this->input->post('pernah_mengikuti_pelatihan_barang_jasa');
        $berapa_kali = $this->input->post('berapa_kali');
        $alasan_mengikuti = $this->input->post('alasan_mengikuti');

        $proses_tbl_peserta = $this->db->query("INSERT INTO tbl_peserta VALUES('$id_peserta','$nama_lengkap','$gelar_akademis','$jenis_kelamin','$NIP','$no_ktp','$tempat_lahir','$tgl_lahir','$pendidikan_terakhir','$status_kepegawaian','$instansi','$lembaga','$domisili_kabupaten','$domisili_provinsi','$no_telepon','$alamat_email','$pengalaman_kerja_bidang_pengadaan_barang_pemerintah','$pernah_mengikuti_pelatihan_barang_jasa','$berapa_kali','$alasan_mengikuti','$id_pelatihan','','')");

        if($proses_tbl_peserta){
            return true;
        }else{
            return false;
        }

    }


    public function countRowTable($table,$param,$value)
    {
        $this->db->select('*');
        $this->db->where($param,$value);
        $this->db->from($table);

        $query = $this->db->get();
        $rowcount = $query->num_rows();

        return $rowcount;
    }

    public function resize_image($file_path, $width, $height) {

        $config['image_library'] = 'gd2';
        $config['source_image'] = $file_path;
        $config['create_thumb'] = TRUE;
        $config['maintain_ratio'] = TRUE;
        $config['width']    = $width;
        $config['height']   = $height;

        $this->load->library('image_lib', $config); 

        $this->image_lib->resize();

    }

    public function getDetaillPelatihan($id_pelatihan)
    {
        $this->db->select('*');
        $this->db->from('tbl_pelatihan');
        $this->db->join('tbl_tema_pelatihan', 'tbl_tema_pelatihan.id_tema = tbl_pelatihan .id_tema');
        $this->db->join('pelatihan_narasumber', 'pelatihan_narasumber.id_pelatihan = tbl_pelatihan .id_pelatihan');
        $this->db->join('tbl_lpp', 'tbl_lpp.id_lpp = tbl_pelatihan .id_lpp');
        $this->db->where('tbl_pelatihan.id_pelatihan',$id_pelatihan);

        $query = $this->db->get();

        return $query->result();
    }
}