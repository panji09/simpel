<?php
class Model_rencana_kegiatan extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }

    public function getRencanaKegiatan($id)
    {
        $this->db->select('*');
        $this->db->from('rencana_kegiatan');
        $this->db->join('tbl_narasumber_new', 'tbl_narasumber_new.id_narasumber = rencana_kegiatan.id_narasumber');
        $this->db->where('rencana_kegiatan.id_narasumber',$id);
        $this->db->order_by('rencana_kegiatan.dari_tanggal','DESC');
        
        $query = $this->db->get();

        return $query->result();   
           
    }

    public function getAllAgenda($param)
    {
        $this->db->select('*');
        $this->db->from('rencana_kegiatan');
        $this->db->join('tbl_narasumber_new', 'tbl_narasumber_new.id_narasumber = rencana_kegiatan.id_narasumber');
        $this->db->where('rencana_kegiatan.approved',$param);
        $this->db->order_by('rencana_kegiatan.dari_tanggal','DESC');
        
        $query = $this->db->get();

        return $query->result();   
    }
    public function countAllAgenda($param)
    {
        $this->db->select('*');
        $this->db->from('rencana_kegiatan');
        $this->db->join('tbl_narasumber_new', 'tbl_narasumber_new.id_narasumber = rencana_kegiatan.id_narasumber');
        $this->db->where('rencana_kegiatan.approved',$param);
        $this->db->order_by('rencana_kegiatan.dari_tanggal','DESC');
        
        $query = $this->db->get();
        $rowcount = $query->num_rows();

        return $rowcount;
    }
    public function getDetailAgenda($param)
    {
        $this->db->select('*');
        $this->db->from('rencana_kegiatan');
        $this->db->join('tbl_narasumber_new', 'tbl_narasumber_new.id_narasumber = rencana_kegiatan.id_narasumber');
        $this->db->where('rencana_kegiatan.id_rencana',$param);
        $this->db->order_by('rencana_kegiatan.id_rencana','DESC');
        
        $query = $this->db->get();

        return $query->result();   
    }
    public function addRencanaKegiatan()
    {   
        //$id_rencana = $this->input->post('id_rencana')
        //$id_narasumber = $this->input->post('id_narasumber');
        /* $id = $this->session->userdata('id');
        $dari_tanggal = $this->input->post('dari_tanggal');
        $sampai_tanggal = $this->input->post('sampai_tanggal');
        $jumlah_jp = $this->input->post('jumlah_jp');
        $keterangan = $this->input->post('keterangan');
        $date = date('Y-m-d');
        $createdBy = $this->session->userdata('role');

        if($this->db->query("INSERT INTO rencana_kegiatan VALUES('','$id','$dari_tanggal','$sampai_tanggal','$jumlah_jp','$keterangan')")){
            return true;
        }else{
            return false;
        } */

        $id = $this->session->userdata('id');
        $dari_tanggal = $this->input->post('dari_tanggal');
        $sampai_tanggal = $this->input->post('sampai_tanggal');
        $jumlah_jp = $this->input->post('jumlah_jp');
        $keterangan = $this->input->post('keterangan');
        $instansi = $this->input->post('instansi');
        $judul_materi = $this->input->post('judul_materi');
        $penugasan_oleh = $this->input->post('penugasan_oleh');
        $date = date('Y-m-d');
        $createdBy = $this->session->userdata('role');

        $config['upload_path'] = '././assets/upload/bukti_kegiatan/';
        $config['allowed_types'] = 'doc|docx|rar|xls|ppt|png|jpg|jpeg|JPG|JPEG';
        $config['max_size'] = '25000';
        $config['max_width']  = '3508';
        $config['max_height']  = '2480';
        $config['overwrite'] = true;

        $filename = $_FILES['userfile']['name'];
        $filename = str_replace(" ", "_", $filename);
        
        $config['file_name'] = $filename;

        $this->load->library('upload', $config);
        $this->upload->overwrite = true;
        $this->upload->initialize($config);


        if ( ! $this->upload->do_upload())
        {
            /*$msg = $this->upload->display_errors('', '');
            $status = $msg;*/
            $dat = array('upload_data' => $this->upload->data());
            $data = array('upload_data' => $this->upload->data());
            $status = "not_upload";
        }
        else
        {
            $data = array('upload_data' => $this->upload->data());
            $status = "success";
        }

        //echo $filename;

        //return $data;

        $this->db->trans_start();

        $this->db->query("INSERT INTO rencana_kegiatan VALUES('','$id','$dari_tanggal','$sampai_tanggal','$jumlah_jp','$keterangan','$instansi','$judul_materi','$penugasan_oleh','$filename',0,'')");

        $user_logins_id = $this->session->userdata('id');
        //$user_logins_id = $this->db->insert_id();
        //$this->db->query("INSERT INTO cek_kegiatan_narasumber VALUES('','$user_logins_id','$dari_tanggal','$sampai_tanggal')");


        $this->db->trans_complete();

    }

    public function getRencanaKegiatanEdit($id_rencana) 
    {
        //$this->db->order_by('judul','ASC');
        $this->db->where('id_rencana',$id_rencana);
        $query = $getData = $this->db->get('rencana_kegiatan');    
        if($getData->num_rows() > 0)    
        return $query;   
        else   
        return null;        
    }

    public function editRencanaKegiatan() 
    {
        $id_rencana = $this->input->post('id_rencana');
        $data = array(
            'id_narasumber' => $this->input->post('id_narasumber'),
            'dari_tanggal' => $this->input->post('dari_tanggal'),
            'sampai_tanggal' => $this->input->post('sampai_tanggal'),
            'jumlah_jp' => $this->input->post('jumlah_jp'),
            'keterangan' => $this->input->post('keterangan')
        );   
            $this->db->where('id_rencana',$this->input->post('id_rencana',$id_rencana));
            $this->db->update('rencana_kegiatan', $data);        
    }

    public function deleteRencanaKegiatan($id_rencana) 
    {
        $this->db->where('id_rencana',$id_rencana);
        $this->db->delete('rencana_kegiatan');    
    }
    public function approve($id)
    {
        $data = array(
            'approved' => '1'
        );   
            $this->db->where('id_rencana',$id);
            $this->db->update('rencana_kegiatan', $data); 
    }
}