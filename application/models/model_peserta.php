<?php
class Model_peserta extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }
    
    public function getAllPesertaByIdPelatihan($id)
    {
        //$id_pelatihan = $this->getIdPelatihanByIdRequest($id);

        $this->db->select('*');
        //$this->db->join('tbl_pelatihan', 'tbl_pelatihan.id_pelatihan = pelatihan_peserta.id_pelatihan');
        //$this->db->join('tbl_tema_pelatihan', 'tbl_pelatihan.id_tema = tbl_tema_pelatihan.id_tema');
        //$this->db->join('tbl_peserta', 'tbl_peserta.id_peserta = pelatihan_peserta.id_peserta');
        $this->db->from('tbl_peserta');
        $this->db->where('id_pelatihan',$id);
        $this->db->limit(50,0);

        $query = $this->db->get();

        return $query->result();
    }
    public function getDetailPeserta($id)
    {

        $this->db->select('*');
        $this->db->from('tbl_peserta');
        $this->db->where('id_peserta',$id);

        $query = $this->db->get();

        return $query->result();
    }
    public function getIdPelatihanByIdRequest($id)
    {
        $this->db->select('*');
        $this->db->where('id_request',$id);
        $this->db->from('tbl_pelatihan');
        $query = $this->db->get();
        $id_key = $query->row();

        return $id_key->id_pelatihan;
    }
    public function input_peserta()
    {
        $id = $this->input->post('id_request');
        $nama_lengkap = $this->input->post('nama_lengkap');
        $gelar_akademis = $this->input->post('gelar_akademis');
        $jenis_kelamin = $this->input->post('jenis_kelamin');
        $nip = $this->input->post('nip');
        $no_ktp = $this->input->post('no_ktp');
        $tempat_lahir = $this->input->post('tempat_lahir');
        $tanggal_lahir = $this->input->post('tanggal_lahir');
        $pendidikan_terakhir = $this->input->post('pendidikan_terakhir');
        $status_kepegawaian = $this->input->post('status_kepegawaian');
        $instansi = $this->input->post('instansi');
        $lembaga = $this->input->post('lembaga');
        $domisili_kabupaten = $this->input->post('domisili_kabupaten');
        $domisili_provinsi = $this->input->post('domisili_provinsi');
        $no_telepon = $this->input->post('no_telepon');
        $alamat_email = $this->input->post('alamat_email');
        $pengalaman = $this->input->post('pengalaman');
        $pernah = $this->input->post('pernah');
        $berapa_kali = $this->input->post('berapa_kali');
        $alasan_mengikuti = $this->input->post('alasan_mengikuti');

        $id_pelatihan = $this->getIdPelatihanByIdRequest($id);

        //insert ke table peserta
        $proses_tbl_peserta = $this->db->query("INSERT INTO tbl_peserta VALUES('','$nama_lengkap','$gelar_akademis','$jenis_kelamin','$nip','$no_ktp','$tempat_lahir','$tanggal_lahir','$pendidikan_terakhir','$status_kepegawaian','$instansi','$lembaga','$domisili_kabupaten','$domisili_provinsi','$no_telepon','$alamat_email','$pengalaman','$pernah','$berapa_kali','$alasan_mengikuti')");

        if($proses_tbl_peserta){
            //get last id peserta
            $id_peserta = $this->getLastIdPeserta();
        }

        $proses_peserta_pelatihan = $this->db->query("INSERT INTO pelatihan_peserta VALUES('','$id_peserta','$id_pelatihan','','')");

        if($proses_peserta_pelatihan && $proses_tbl_peserta){
            return true;
        }else{
            return false;
        }
    }
    public function getLastIdPeserta()
    {
        $this->db->select('*');
        $this->db->from('tbl_peserta');
        $this->db->order_by('id_peserta','DESC');
        $this->db->limit(1,0);
        $query = $this->db->get();
        $id_key = $query->row();

        return $id_key->id_peserta;
    }

    public function delete($table,$param,$id)
    {
        $proses = $this->db->query("DELETE FROM $table WHERE $param = '$id' ");

        if($proses){
            return true;
        }else{
            return false;
        }
    }

    public function updatePeserta()
    {
        

        $data = array(
            'nama_lengkap' => $this->input->post('nama_lengkap'),
            'gelar_akademis' => $this->input->post('gelar_akademis'),
            'jenis_kelamin' => $this->input->post('jenis_kelamin'),
            'nip' => $this->input->post('nip'),
            'no_ktp' => $this->input->post('no_ktp'),
            'tempat_lahir' => $this->input->post('tempat_lahir'),
            'tgl_lahir' => $this->input->post('tempat_lahir'),
            'pendidikan_terakhir' => $this->input->post('pendidikan_terakhir'),
            'status_kepegawaian' => $this->input->post('status_kepegawaian'),
            'instansi' => $this->input->post('instansi'),
            'lembaga' => $this->input->post('lembaga'),
            'domisili_kabupaten' => $this->input->post('domisili_kabupaten'),
            'domisili_provinsi' => $this->input->post('domisili_provinsi'),
            'no_telepon' => $this->input->post('no_telepon'),
            'alamat_email' => $this->input->post('alamat_email'),
            'pengalaman_kerja_bidang_pengadaan_barang_pemerintah' => $this->input->post('pengalaman'),
            'pernah_mengikuti_pelatihan_barang_jasa' => $this->input->post('pernah'),
            'pre_test' => $this->input->post('pre_test'),
            'post_test' => $this->input->post('post_test'),
        );   

        $this->db->where('id_peserta',$this->input->post('id_peserta'));
        $proses_tbl_peserta = $this->db->update('tbl_peserta', $data);  

        if($proses_tbl_peserta){
            return true;
        }else{
            return false;
        }
    }
}