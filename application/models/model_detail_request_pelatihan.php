<?php
class Model_detail_request_pelatihan extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }
    
    public function getAllRequestPelatihanById($id_all) 
    {
        /* $this->db->where('id',$id_all);
        $query = $getData = $this->db->get('pelatihan_narasumber');    
        if($getData->num_rows() > 0)    
        return $query;   
        else   
        return null; */

        $this->db->select('*');
        $this->db->join('tbl_pelatihan', 'tbl_pelatihan.id_pelatihan = pelatihan_narasumber.id_pelatihan');
        $this->db->where('tbl_pelatihan.id_pelatihan',$id_all);
        $this->db->from('pelatihan_narasumber');

        $query = $this->db->get();

        return $query->result();       
    }

    public function detailAllRequestPelatihan($id_all) 
    {
        /* $id_rencana = $this->input->post('id');
        $data = array(
            'id_pelatihan' => $this->input->post('id_pelatihan'),
            'tgl_pelatihan' => $this->input->post('dari_tanggal'),
            'dari_jam' => $this->input->post('sampai_tanggal'),
            'sampai_jam' => $this->input->post('jumlah_jp'),
            'total_jam' => $this->input->post('keterangan')
        );   
            $this->db->where('id',$this->input->post('id',$id_all));
            $this->db->update('pelatihan_narasumber', $data); */
    }

    public function setRequestApproved($id_request)
    {
        $data = array(
               'approve' => '1',
            );

        $this->db->where('id_request', $id_request);
        $this->db->update('tbl_request_form', $data); 
    }
    
    public function cancelPelatihan($id_request,$keterangan_ditolak)
    {
        $data = array(
               'approve' => '2',
               'keterangan_ditolak' => $keterangan_ditolak,
            );

        $this->db->where('id_request', $id_request);
        $proses = $this->db->update('tbl_request_form', $data); 

        if($proses){
            return true;
        }else{
            return false;
        }
    }
}
