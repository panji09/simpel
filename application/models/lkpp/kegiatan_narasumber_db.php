<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Kegiatan_narasumber_db extends CI_Model{
    function __construct(){
        // Call the Model constructor
        parent::__construct(); 
    }
    
    function exist($lpp_id){
        return $this->get_all(array('id' => $lpp_id))->num_rows() == 1;
    }
    
    function email_exist($email){
	return $this->get_all(array('email' => $email, 'approve' => 1))->num_rows() == 1;
    }
    
    function get($lpp_id){
        $query = $this->get_all(array('id' => $lpp_id));
        if($query->num_rows() == 1){
            return $query->row();
        }else{
            $field_obj = new stdClass();
            foreach($query->list_fields() as $field){
                $field_obj->$field = '';
            }
            return $field_obj;
        }
    }
    
    function get_all($filter=array(),$limit=null, $offset=0){
	$this->db->select('*');
	$this->db->from('tbl_lpp');
	if($filter){
	    
	    if(isset($filter['id']))
		$this->db->where('id_lpp', $filter['id']);
	    
	    if(isset($filter['email']))
		$this->db->where('email', $filter['email']);
	    
	    if(isset($filter['approve']))
		$this->db->where('approve', $filter['approve']);
	    
	    if(isset($filter['search_name']))
		$this->db->like('nama_narasumber', $filter['search_name']);
	    
	    if(isset($filter['approve']))
		$this->db->where('approve', $filter['approve']);
	    
	    //hasil_akreditasi
	    if(isset($filter['akreditasi_in']))
		$this->db->where_in('hasil_akreditasi', $filter['akreditasi_in']);
	    
	    if(isset($filter['akreditasi_not_in']))
		$this->db->where_not_in('hasil_akreditasi', $filter['akreditasi_not_in']);
	    
        }
        
        if($limit)
	  $this->db->limit($limit, $offset);
	
	//$this->db->order_by('content_dinamis.id', 'DESC');
	
	return $this->db->get();
    }
    
    function save($lpp_id=null, $data_lpp=array()){
	$result = false;
	
	if($data_lpp){
	    
	    if(!$lpp_id && $this->exist($lpp_id)){
		//update
		$this->db->where('lpp_id', $lpp_id);
		$result=$this->db->update('tbl_lpp', $data_lpp);
	    }else{
		//insert
		$result=$this->db->insert('tbl_lpp', $data_lpp);
	    }
	}
	
	return $result;
    }
}
?> 
