<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Narasumber_db extends CI_Model{
    function __construct(){
        // Call the Model constructor
        parent::__construct(); 
    }
    
    function get($narasumber_id){
        $query=$this->db->get_where('tbl_narasumber_new',array('id_narasumber' => $narasumber_id));
        if($query->num_rows() == 1){
            return $query->row();
        }else{
            $field_obj = new stdClass();
            foreach($query->list_fields() as $field){
                $field_obj->$field = '';
            }
            return $field_obj;
        }
    }
    
    function get_all($filter=array(),$limit=null, $offset=0){
	$this->db->select('*');
	$this->db->from('tbl_narasumber_new');
	if($filter){
	    if(isset($filter['search_name']))
		$this->db->like('nama_narasumber', $filter['search_name']);
	    
	    if(isset($filter['aktif']))
		$this->db->where('status_aktif_narasumber', $filter['aktif']);
	  
        }
        
        if($limit)
	  $this->db->limit($limit, $offset);
	
	//$this->db->order_by('content_dinamis.id', 'DESC');
	
	return $this->db->get();
    }
    
    function save(){
    
    }
}
?> 
