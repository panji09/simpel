<?php
class Model_berita extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }
    
    public function getAllBerita()
    {
        $this->db->select('*');
        $this->db->from('tbl_berita');
        $this->db->limit(5,0);
        $this->db->where('is_header','0');
        $this->db->order_by('tgl_berita','DESC');
        $query = $this->db->get();

        return $query->result();
    }

    public function getBerita($limit,$start)
    {
        $this->db->select('*');
        $this->db->from('tbl_berita');
        $this->db->limit($limit,$start);
        $this->db->order_by('tgl_berita','DESC');
        $query = $this->db->get();

        return $query->result();
    }
    public function findBerita($limit,$start,$keyword)
    {
        if($start==1){
            $start = 0;
        }

        $this->db->select('*');
        $this->db->from('tbl_berita');
        $this->db->like('judul_berita', $keyword);
        $this->db->limit($limit,$start);
        $query = $this->db->get();

        return $query->result();
    }
    public function getSemuaBerita()
    {
        $this->db->select('*');
        $this->db->from('tbl_berita');
        $this->db->order_by('tgl_berita','DESC');
        $query = $this->db->get();

        return $query->result();
    }
    public function getBeritaLainya($id)
    {
        $this->db->select('*');
        $this->db->from('tbl_berita');
        $this->db->order_by('tgl_berita','DESC');
        $this->db->where_not_in('id_berita',$id);
        $this->db->limit(5,0);
        $query = $this->db->get();

        return $query->result();
    }
    public function getAllPengumuman($value='')
    {
        $this->db->select('*');
        $this->db->from('tbl_pengumuman');
        //$this->db->limit($limit, $start);
        $this->db->order_by('tgl_pengumuman','DESC');
        $query = $this->db->get();

        return $query->result();
    }
    public function getPengumumanById($id)
    {
        $this->db->select('*');
        $this->db->from('tbl_pengumuman');
        $this->db->where('id_pengumuman',$id);
        $query = $this->db->get();

        return $query->result();
    }

    public function getPengumuman($value='')
    {
        $this->db->select('*');
        $this->db->from('tbl_pengumuman');
        $this->db->where('header','0');
        $this->db->order_by('tgl_pengumuman','DESC');
        $query = $this->db->get();

        return $query->result();
    }
    public function getheaderPengumuman()
    {
        $this->db->select('*');
        $this->db->from('tbl_pengumuman');
        //$this->db->limit($limit, $start);
        $this->db->where('header','1');
        $this->db->order_by('tgl_pengumuman','DESC');
        $query = $this->db->get();

        return $query->result();
    }
    public function getheaderBerita()
    {
        $this->db->select('*');
        $this->db->from('tbl_berita');
        //$this->db->limit($limit, $start);
        $this->db->where('is_header','1');
        $this->db->order_by('tgl_berita','DESC');
        $query = $this->db->get();

        return $query->result();
    }
    public function getDetailBerita($id)
    {
        $this->db->select('*');
        $this->db->from('tbl_berita');
        $this->db->where('id_berita', $id);
        $query = $this->db->get();

        return $query->result();
    }
    public function getDetailPengumuman($id)
    {
        $this->db->select('*');
        $this->db->from('tbl_pengumuman');
        $this->db->where('id_pengumuman', $id);
        $query = $this->db->get();

        return $query->result();
    }
    public function getDetailBeritaHome($judul)
    {
        $judul = str_replace('_', ' ', $judul);

        $this->db->select('*');
        $this->db->from('tbl_berita');
        $this->db->like('judul_berita', $judul);
        $query = $this->db->get();

        return $query->result();
    }
    public function insert_new_pengumuman()
    {
        $judul_pengumuman = $this->input->post('judul_pengumuman');
        $isi_pengumuman = $this->input->post('isi_pengumuman');
        $tgl_pengumuman = $this->input->post('tgl_pengumuman');
        $header = $this->input->post('header');


        $config['upload_path'] = '././assets/upload/pengumuman/';
        $config['allowed_types'] = 'pdf|doc|docx|xls|xlsx|ppt|pptx';
        $config['max_size'] = '1000';
        

        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if ( ! $this->upload->do_upload())
        {
            $msg = $this->upload->display_errors('', '');
            $status = $msg;
        }
        else
        {
            $data = array('upload_data' => $this->upload->data());
            foreach ($data as $item => $value):
                $photo_name = $value['file_name'];
            endforeach;

            $proses_berita = $this->db->query("INSERT INTO tbl_pengumuman VALUES('','$judul_pengumuman','$isi_pengumuman','$tgl_pengumuman','$photo_name','$header')");

            if($proses_berita){
                $status = "success";
            }else{
                $status = "gagal";
            }
        }
        

        

        return $status;
    }
    public function edit_pengumuman($id)
    {
        $judul_pengumuman = $this->input->post('judul_pengumuman');
        $tgl_pengumuman = $this->input->post('tgl_pengumuman');
        $header = $this->input->post('header');


        $proses_berita = $this->db->query("UPDATE tbl_pengumuman set isi_pengumuman = '$judul_pengumuman',tgl_pengumuman = '$tgl_pengumuman',header = '$header' WHERE id_pengumuman = '$id'");

        if($proses_berita){
            $status = "success";
        }else{
            $status = "gagal";
        }
        

        

        return $status;
    }
    public function insert_new_berita()
    {
        $judul_berita = $this->input->post('judul_berita');
        $tgl_berita = $this->input->post('tgl_berita');
        $photo_berita = $this->input->post('photo_berita');
        $isi_berita = $this->input->post('isi_berita');
        $header = $this->input->post('header');

        $config['upload_path'] = '././assets/upload/berita/';
        $config['allowed_types'] = 'gif|jpg|png|xls|pdf|xlsx|doc|docx|ppt|pptx';
        $config['max_size'] = '1000';
        $config['max_width']  = '1024';
        $config['max_height']  = '768';
        

        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if ( ! $this->upload->do_upload())
        {
            $msg = $this->upload->display_errors('', '');
            $status = $msg;
        }
        else
        {
            $data = array('upload_data' => $this->upload->data());
            foreach ($data as $item => $value):
                $photo_name = $value['file_name'];
            endforeach;

            $this->resize_image('././assets/upload/berita/'.$photo_name,250,250);

            //$photo = $data['file_name'];
            $proses_berita = $this->db->query("INSERT INTO tbl_berita VALUES('','$judul_berita','$photo_name','$tgl_berita','$isi_berita','$header')");

            $status = "success";
        }

        

        return $status;
    }
    public function update_berita($id) 
    {

        $judul_berita = $this->input->post('judul_berita');
        $tgl_berita = $this->input->post('tgl_berita');
        $photo_berita = $this->input->post('photo_berita');
        $isi_berita = $this->input->post('isi_berita');
        $cek_photo = $this->input->post('cek_photo');

        $config['upload_path'] = '././assets/upload/berita/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = '1000';
        $config['max_width']  = '1024';
        $config['max_height']  = '768';
        $config['overwrite'] = true;

        if ($cek_photo!="on") {

            $this->load->library('upload', $config);
            $this->upload->initialize($config);

            if ( ! $this->upload->do_upload())
            {
                $msg = $this->upload->display_errors('', '');
                $status = $msg;
            }
            else
            {
                $data_upload = array('upload_data' => $this->upload->data());
                foreach ($data_upload as $item => $value):
                    $photo_name = $value['file_name'];
                endforeach;

                $this->resize_image('././assets/upload/berita/'.$photo_name,250,250);

                $data = array(
                    'judul_berita' => $this->input->post('judul_berita'),
                    'tgl_berita' => $this->input->post('tgl_berita'),
                    'photo_berita' => $photo_name,
                    'isi_berita' => $this->input->post('isi_berita')
                );   
                    $this->db->where('id_berita',$id);
                    $this->db->update('tbl_berita', $data);  

                $status = "success";
            }

        }else{
            $data = array(
                    'judul_berita' => $this->input->post('judul_berita'),
                    'tgl_berita' => $this->input->post('tgl_berita'),
                    'isi_berita' => $this->input->post('isi_berita')
                );   
                    $this->db->where('id_berita',$id);
                    $this->db->update('tbl_berita', $data);  

                $status = "success";

        }

        

        return $status;


              
    }

    public function resize_image($file_path, $width, $height) {

        $config['image_library'] = 'gd2';
        $config['source_image'] = $file_path;
        $config['create_thumb'] = TRUE;
        $config['maintain_ratio'] = TRUE;
        $config['width']    = $width;
        $config['height']   = $height;

        $this->load->library('image_lib', $config); 

        $this->image_lib->resize();

    }
}