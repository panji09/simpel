<?php

/* Model Main */

class Main_model extends CI_Model {

	function __construct()
	{
		parent::__construct();	
	}
	function get_list_all($table)
	{	
		$query = $this->db->get($table);

		return $query->result();
	}
	function get_list($table, $limit = null, $sort = null)
	{
		if($limit != null) {
			$this->db->limit($limit['perpage'],$limit['offset']);
		}
		if($sort != null) {
			$this->db->order_by($sort['by'],$sort['sorting']);
		}
		$query = $this->db->get($table);
		
		return $query;
	}
	function get_list_where($table, $where = array())
	{
		$this->db->where($where);
		$query = $this->db->get($table);
		
		return $query->result();
	}
	function get_string_field_where($field,$table, $where = array())
	{
		$this->db->select($field);
		$this->db->from($table);
		$this->db->where($where);

		foreach ($this->db->get()->result() as $data) {
			$value = $data->$field;
		}
		
		if(isset($value)){
			return $value;
		}else{
			return "-";
		}
		
	}
	function get_field_where($field,$table, $where = array())
	{
		$this->db->select($field);
		$this->db->from($table);
		$this->db->where($where);

		foreach ($this->db->get()->result() as $data) {
			$value = $data->$field;
		}
		
		if(isset($value)){
			if($value!=0){
				return $value;
			}else{
				return "-";
			}
			
		}else{
			return "-";
		}
		
	}
	function get_field_order_limit($field,$table,$limit, $sort)
	{
		$this->db->select($field);
		$this->db->from($table);
		if($limit != null) {
			$this->db->limit($limit['perpage'],$limit['offset']);
		}
		if($sort != null) {
			$this->db->order_by($sort['by'],$sort['sorting']);
		}

		foreach ($this->db->get()->result() as $data) {
			$value = $data->$field;
		}
		
		if(isset($value)){
			return $value;
		}else{
			return "0";
		}
		
	}
	function get_join_where($table,$table_join,$where_join, $where = array())
	{
		$this->db->select('*');
		$this->db->from($table);
		$this->db->join($table_join,$where_join);
		$this->db->where($where);
		
		return $this->db->get()->result();
	}
	function get_join_where_array($table = array(),$table_join = array(),$where_join = array(), $where = array())
	{
		
		$this->db->select('*');
		$this->db->from($table);
		for ($i=0; $i < count($table_join); $i++) { 
			/*echo $table_join[$i];*/
			$this->db->join($table_join[$i],$where_join[$i]);
		}
		$this->db->where($where);
		
		return $this->db->get()->result();
	}
	
	function get_join_where_array_group($table = array(),$table_join = array(),$where_join = array(), $where = array(), $group = array())
	{
		
		$this->db->select('*');
		$this->db->from($table);
		for ($i=0; $i < count($table_join); $i++) { 
			/*echo $table_join[$i];*/
			$this->db->join($table_join[$i],$where_join[$i]);
		}
		$this->db->where($where);
		$this->db->group_by($group);
		
		return $this->db->get()->result();
	}

	function get_join_where_array_group_order($table = array(),$table_join = array(),$where_join = array(), $where = array(), $group = array(),$order_by,$order_stat)
	{
		
		$this->db->select('*');
		$this->db->from($table);
		for ($i=0; $i < count($table_join); $i++) { 
			/*echo $table_join[$i];*/
			$this->db->join($table_join[$i],$where_join[$i]);
		}
		$this->db->where($where);
		$this->db->group_by($group);
		$this->db->order_by($order_by,$order_stat);
		
		return $this->db->get()->result();
	}
	
	public function get_query($query)
	{
		return $this->db->query($query)->result();
	}

	function get_json_where_array($field,$table = array(),$table_join = array(),$where_join = array(), $where = array(),$order_by,$order_stat)
	{
		
		$this->db->select($field);
		$this->db->from($table);
		for ($i=0; $i < count($table_join); $i++) { 
			/*echo $table_join[$i];*/
			$this->db->join($table_join[$i],$where_join[$i]);
		}
		$this->db->where($where);
		$this->db->order_by($order_by,$order_stat);
		
		return $this->db->get()->result();
	}
	
	public function count_row($table,$where = array())
	{
		$this->db->from($table);
		$this->db->where($where);

		$query = $this->db->get();
		$rowcount = $query->num_rows();

		return $rowcount;
	}

	public function count_row_join($table,$table_join = array(),$where_join = array(), $where = array())
	{
		
		$this->db->select('*');
		$this->db->from($table);
		for ($i=0; $i < count($table_join); $i++) { 
			/*echo $table_join[$i];*/
			$this->db->join($table_join[$i],$where_join[$i]);
		}
		$this->db->where($where);
		$query = $this->db->get();
		$rowcount = $query->num_rows();

		return $rowcount;
	}

	public function insert_into($table,$array)
	{
		$insert = $this->db->set($array);
		$insert = $this->db->insert($table); 
		
		if($insert){
			$result = "Success, Data Berhasil Ditambahkan ke Database";
		}else{
			$result = "Error, Data Gagal Dimasukan Silahkan Coba Beberapa Saat Lagi";
		}

		return $result;
	}

	public function update_data($table,$data,$where= array())
	{
		$update = $this->db->where($where);
		$update = $this->db->update($table, $data);
		
		if($update){
			$result = true;
		}else{
			$result = false;
		}

		return $result;
	}

	public function delete($table,$field,$value)
	{
		$this->db->query("DELETE FROM $table WHERE $field = $value ");
		return true;
	}


	public function login($u,$pw)
	{

		  $this->db->select('*');
    $this->db->where('user_name',$u);
    $this->db->where('user_pass', $pw);
    //$this->db->where('status', 'aktif');
    //$this->db->limit(1);
    $Q = $this->db->get('tbl_user');
    $this->session->set_userdata('lastquery', $this->db->last_query());
    if ($Q->num_rows() > 0){
      $row = $Q->row_array();
      return $row;
    }else{
      $this->session->set_flashdata('error', 'Sorry, try again!');  
      return array();
    } 

	}
}