<?php
class Model_lpp extends CI_Model {

    function __construct()
    {
        parent::__construct();
        $this->load->library('MY_PHPMailer');
    }
    
    public function getAllLpp()
    {
        $this->db->select('*');
        $this->db->from('tbl_lpp');
        $this->db->where('approve','1');
        $this->db->order_by('id_lpp','DESC');

        $query = $this->db->get();

        return $query->result();
    }

    public function getLppTerakreditasi()
    {
        $this->db->select('*');
        $this->db->from('tbl_lpp');
        $this->db->where('approve','1');
        $this->db->where_not_in('hasil_akreditasi',' ');
        $this->db->where_not_in('hasil_akreditasi','-');
        $this->db->order_by('id_lpp','DESC');

        $query = $this->db->get();

        return $query->result();
    }

    public function findLpp($keyword)
    {
        $this->db->select('*');
        $this->db->from('tbl_lpp');
        $this->db->like('nama_lpp',$keyword);
        $this->db->order_by('id_lpp','DESC');

        $query = $this->db->get();

        return $query->result();
    }

    public function getLppTerdaftar()
    {
        $this->db->select('*');
        $this->db->from('tbl_lpp');
        $this->db->where('approve','1');
        $this->db->where('hasil_akreditasi',' ');
        $this->db->order_by('id_lpp','DESC');

        $query = $this->db->get();

        return $query->result();
    }
    public function getLppBelumAkreditasi()
    {
        $this->db->select('*');
        $this->db->from('tbl_lpp');
        $this->db->where('approve','0');
        $this->db->order_by('id_lpp','DESC');

        $query = $this->db->get();

        return $query->result();
    }
    public function getLppBaru()
    {
        $this->db->select('*');
        $this->db->from('tbl_lpp');
        $this->db->where('approve','0');

        $query = $this->db->get();

        return $query->result();
    }
    public function getDetailLpp($id)
    {
        $sql = "SELECT * FROM tbl_lpp WHERE id_lpp = '$id' ";

        $query = $this->db->query($sql);
        return $query->result();
    }
    public function countAllLpp()
    {
        $query = $this->db->query("SELECT (SELECT COUNT(*) FROM (select * from tbl_lpp) AS ROW) AS ROW");
        $id_key = $query->row();

        if($id_key!=null){
            return $id_key->ROW;
        }else{
            return 0;
        }
    }
    public function countLPPBaru()
    {
        $query = $this->db->query("SELECT (SELECT COUNT(*) FROM (select * from tbl_lpp WHERE approve = '0') AS ROW) AS ROW");
        $id_key = $query->row();

        if($id_key!=null){
            return $id_key->ROW;
        }else{
            return 0;
        }
    }
    public function insert_lpp($file_dokumen)
    {
        $nama_lpp = $this->input->post('nama_lpp');
        $penanggungjawab = $this->input->post('penanggungjawab');
        $jabatan = $this->input->post('jabatan');
        $no_ktp = $this->input->post('no_ktp');

        $program = $this->input->post('program');
        $alamat_lpp = $this->input->post('alamat_lpp');
        $no_telp_lpp = $this->input->post('no_telp_lpp');
        $no_fax = $this->input->post('no_fax');
        $email = $this->input->post('email');
        $password = base64_encode(md5($this->input->post('password')));

        $kontak_nama = $this->input->post('kontak_nama');
        $kontak_telp = $this->input->post('kontak_telp');
        $kontak_email = $this->input->post('kontak_email');

        $lampiran_ktp = $file_dokumen['lampiran_ktp'];

        if(isset($file_dokumen['surat_ijin_operasional'])){
            $surat_ijin_operasional = $file_dokumen['surat_ijin_operasional'];
        }else{
            $surat_ijin_operasional = " ";
        }

        if(isset($file_dokumen['surat_ket_domisili'])){
            $surat_ket_domisili = $file_dokumen['surat_ket_domisili'];
        }else{
            $surat_ket_domisili = " ";
        }

        if(isset($file_dokumen['akta_notaris'])){
            $akta_notaris = $file_dokumen['akta_notaris'];
        }else{
            $akta_notaris = " ";
        }

        if(isset($file_dokumen['mekanisme_keuangan'])){
            $mekanisme_keuangan = $file_dokumen['mekanisme_keuangan'];
        }else{
            $mekanisme_keuangan = " ";
        }

        if(isset($file_dokumen['sop'])){
            $sop = $file_dokumen['sop'];
        }else{
            $sop = " ";
        }

        if(isset($file_dokumen['surat_komitmen'])){
            $surat_komitmen = $file_dokumen['surat_komitmen'];
        }else{
            $surat_komitmen = " ";
        }

        if(isset($file_dokumen['tugas_pelaksana'])){
            $tugas_pelaksana = $file_dokumen['tugas_pelaksana'];
        }else{
            $tugas_pelaksana = " ";
        }
        
        /*$surat_ket_domisili = $file_dokumen['surat_ket_domisili'];
        $akta_notaris = $file_dokumen['akta_notaris'];
        $mekanisme_keuangan = $file_dokumen['mekanisme_keuangan'];
        $sop = $file_dokumen['sop'];
        $surat_komitmen = $file_dokumen['surat_komitmen'];
        $tugas_pelaksana = $file_dokumen['tugas_pelaksana'];*/

        $username = str_replace(" ","_",$nama_lpp);

        /*$username = $this->input->post('username');
        $password = $this->input->post('password');

        $no_akreditasi_lpp = $this->input->post('no_akreditasi_lpp');
        $hasil_akreditasi = $this->input->post('hasil_akreditasi');
        $satker = $this->input->post('satker');
        $file_akreditasi = $this->input->post('file_akreditasi');*/

        
        $proses = $this->db->query("INSERT INTO tbl_lpp VALUES('','$nama_lpp','$alamat_lpp','$no_telp_lpp','$username','$password','','','','','','','','','$penanggungjawab','$jabatan','$no_ktp','$lampiran_ktp','$program','$no_fax','$email','$kontak_nama','$kontak_telp','$kontak_email','$surat_ijin_operasional','$surat_ket_domisili','$akta_notaris','$mekanisme_keuangan','$sop','$surat_komitmen','$tugas_pelaksana','','','','','','','','','','')");
        
        if($proses){
            return true;
        }else{
            return false;
        }

    }

    public function verifikasi_lpp($id_lpp)
    {
        $proses = $this->db->query("UPDATE tbl_lpp SET verified = '1' WHERE id_lpp = '$id_lpp' ");
        
        if($proses){
            return true;
        }else{
            return false;
        }
    }

    public function verifikasi($jenis,$param,$id)
    {
        $jenis = "status_".$jenis;
        $proses = $this->db->query("UPDATE tbl_lpp SET $jenis = '$param' WHERE id_lpp = '$id' ");
        
        if($proses){
            return "success";
        }else{
            return "verifikasi gagal";
        }
    }

    public function aktifkan_lpp($id_lpp)
    {
        $this->load->library('email');
        $proses = $this->db->query("UPDATE tbl_lpp SET approve = '1' WHERE id_lpp = '$id_lpp' ");
        $email = $this->getEmailLpp($id_lpp);

        /* Setting Email pendaftaran lpp */
        $message = "<h1>SELAMAT </h1> \n 
        Pendaftaran LPP anda telah disetujui silahkan login melalui link dibawah ini untuk login 
        \n 
        http://simpel.lkpp.go.id/admin/login ";

        $mail             = new PHPMailer();

        //$body             = $mail->getFile('contents.html');
        //$body             = eregi_replace("[\]",'',$body);

        $mail->IsSMTP();
        $mail->SMTPAuth   = true;                  // enable SMTP authentication
        $mail->SMTPSecure = "";                 // sets the prefix to the servier
        $mail->Host       = "mail.lkpp.go.id";      // sets GMAIL as the SMTP server
        $mail->Port       = 25;                   // set the SMTP port for the GMAIL server

        $mail->Username   = "simpel@lkpp.go.id";  // GMAIL username
        $mail->Password   = "jeruks1988";            // GMAIL password
        $mail->AddReplyTo($email);
        $mail->AddAddress($email, "SIMPEL LKPP");
        $mail->From       = "simpel@lkpp.go.id";
        $mail->FromName   = "SIMPEL LKPP";

        $mail->Subject    = "Aktivasi LPP Telah Disetujui";

        $mail->Body       = "Hi,<br>".$message."<br>";                      //HTML Body
       
        $mail->WordWrap   = 50; // set word wrap

        $mail->MsgHTML($message);

        $mail->IsHTML(true); // send as HTML

        if(!$mail->Send()) {
          echo "Mailer Error: " . $mail->ErrorInfo;
        } else {
            redirect('lkpp/');
        }

                //echo $this->email->print_debugger();
        if($proses){
            return true;    
        }else{
            return false;
        }
    }

    public function get_lpp_by_satker($satker) {
      
      $this->db->select('id_lpp, nama_lpp');
      $this->db->where('is_satker',$satker);
      $records=$this->db->get('tbl_lpp');
      $data=array();
      
      //$data[0] = 'SELECT';
      foreach ($records->result() as $row)
      {
        $data[$row->id_lpp] = $row->nama_lpp;
      }
      return ($data);
    }

    public function getAkreditasiLpp($id)
    {
        $query = $this->db->query("SELECT hasil_akreditasi from tbl_lpp WHERE id_lpp = '$id' ");
        $id_key = $query->row();

        if($id_key!=null){
            return $id_key->hasil_akreditasi;
        }else{
            return 0;
        }
    }

    public function getEmailLpp($id)
    {
        $query = $this->db->query("SELECT email from tbl_lpp WHERE id_lpp = '$id' ");
        $id_key = $query->row();

        if($id_key!=null){
            return $id_key->email;
        }else{
            return 0;
        }
    }

    public function getInstansiLpp($id)
    {
        $query = $this->db->query("SELECT instansi from tbl_lpp WHERE id_lpp = '$id' ");
        $id_key = $query->row();

        if($id_key!=null){
            return $id_key->instansi;
        }else{
            return 0;
        }
    }

    public function getIdLppFromIdPelatihan($id)
    {
        $query = $this->db->query("SELECT id_lpp from tbl_pelatihan WHERE id_pelatihan = '$id' ");
        $id_key = $query->row();

        if($id_key!=null){
            return $id_key->id_lpp;
        }else{
            return 0;
        }
    }

    public function getLppName($id)
    {
        $query = $this->db->query("SELECT nama_lpp from tbl_lpp WHERE id_lpp = '$id' ");
        $id_key = $query->row();

        if($id_key!=null){
            return $id_key->nama_lpp;
        }else{
            return 0;
        }
    }
    public function update_data_lpp()
    {
        $id_lpp = $this->input->post('id_lpp');
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        $password = base64_encode(md5($password));

        $nama_lpp = $this->input->post('nama_lpp');
        $penanggungjawab = $this->input->post('penanggungjawab');
        $no_ktp = $this->input->post('no_ktp');

        $program = $this->input->post('program');
        $alamat_lpp = $this->input->post('alamat_lpp');
        $no_telp_lpp = $this->input->post('no_telp_lpp');
        $no_fax = $this->input->post('no_fax');
        $email = $this->input->post('email');

        $kontak_nama = $this->input->post('kontak_nama');
        $kontak_telp = $this->input->post('kontak_telp');
        $kontak_email = $this->input->post('kontak_email');

        $hasil_akreditasi = $this->input->post('hasil_akreditasi');
        $no_akreditasi_lpp = $this->input->post('no_akreditasi_lpp');

        $masa_berlaku = $this->input->post('masa_berlaku');

        $website = $this->input->post('website');
        $jabatan = $this->input->post('jabatan');

        $proses = $this->db->query("UPDATE tbl_lpp SET 
            nama_lpp = '$nama_lpp',
            alamat_lpp = '$alamat_lpp',
            no_telp_lpp = '$no_telp_lpp',
            username = '$username',
            penanggungjawab = '$penanggungjawab',
            no_ktp = '$no_ktp',
            program = '$program',
            no_fax = '$no_fax',
            email = '$email',
            kontak_nama = '$kontak_nama',
            kontak_tlp = '$kontak_telp',
            kontak_email = '$kontak_email',
            no_akreditasi_lpp = '$no_akreditasi_lpp',
            masa_berlaku = '$masa_berlaku',
            jabatan = '$jabatan',
            website = '$website'
            WHERE
            id_lpp = '$id_lpp'
            ");

        if($proses){
            $status = "success";
        }else{
            $status = "failed";
        }

        
        return $status;
        /*if($proses){
            return true;
        }else{
            return false;
        }*/
    }
    public function update_data_lpp_lkpp()
    {
        $id_lpp = $this->input->post('id_lpp');
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        $password = base64_encode(md5($password));

        $nama_lpp = $this->input->post('nama_lpp');
        $penanggungjawab = $this->input->post('penanggungjawab');
        $no_ktp = $this->input->post('no_ktp');

        $program = $this->input->post('program');
        $alamat_lpp = $this->input->post('alamat_lpp');
        $no_telp_lpp = $this->input->post('no_telp_lpp');
        $no_fax = $this->input->post('no_fax');
        $email = $this->input->post('email');

        $kontak_nama = $this->input->post('kontak_nama');
        $kontak_telp = $this->input->post('kontak_telp');
        $kontak_email = $this->input->post('kontak_email');

        $hasil_akreditasi = $this->input->post('hasil_akreditasi');
        $no_akreditasi_lpp = $this->input->post('no_akreditasi_lpp');

        $masa_berlaku = $this->input->post('masa_berlaku');

        $website = $this->input->post('website');
        $password_state = $this->input->post('password_state');

        if($password_state=="on"){
            $proses = $this->db->query("UPDATE tbl_lpp SET 
                nama_lpp = '$nama_lpp',
                alamat_lpp = '$alamat_lpp',
                no_telp_lpp = '$no_telp_lpp',
                username = '$username',
                penanggungjawab = '$penanggungjawab',
                no_ktp = '$no_ktp',
                program = '$program',
                no_fax = '$no_fax',
                email = '$email',
                kontak_nama = '$kontak_nama',
                kontak_tlp = '$kontak_telp',
                kontak_email = '$kontak_email',
                no_akreditasi_lpp = '$no_akreditasi_lpp',
                hasil_akreditasi = '$hasil_akreditasi',
                masa_berlaku = '$masa_berlaku',
                website = '$website'
                WHERE
                id_lpp = '$id_lpp'
                ");

            if($proses){
                $status = "success";
            }else{
                $status = "failed";
            }
        }else{
            $proses = $this->db->query("UPDATE tbl_lpp SET 
                nama_lpp = '$nama_lpp',
                alamat_lpp = '$alamat_lpp',
                no_telp_lpp = '$no_telp_lpp',
                username = '$username',
                penanggungjawab = '$penanggungjawab',
                no_ktp = '$no_ktp',
                program = '$program',
                no_fax = '$no_fax',
                email = '$email',
                kontak_nama = '$kontak_nama',
                kontak_tlp = '$kontak_telp',
                kontak_email = '$kontak_email',
                no_akreditasi_lpp = '$no_akreditasi_lpp',
                hasil_akreditasi = '$hasil_akreditasi',
                password = '$password',
                masa_berlaku = '$masa_berlaku',
                website = '$website'
                WHERE
                id_lpp = '$id_lpp'
                ");

            if($proses){
                $status = "success";
            }else{
                $status = "failed";
            }
        }
        

        
        return $status;
        /*if($proses){
            return true;
        }else{
            return false;
        }*/
    }
    public function delete($table,$param,$id)
    {
        $proses = $this->db->query("DELETE FROM $table WHERE $param = '$id' ");

        if($proses){
            return true;
        }else{
            return false;
        }
    }
}



/*
INSERT INTO tbl_lpp VALUES(
'',
'Unit Layanan Pembangunan Daerah',
'Gedung ICT Center Lantai 4 Universitas Diponegoro, Jl. Prof. H. Soedarto, SH Tembalang-Semarang 2',
'2321',
'Unit_Layanan_Pembangunan_Daerah',
'MjhlOGJlMTZjZGI2YzBmY2IxYjc3OWEwNDFjOWIxNDY=',
'',
'',
'',
'',
'',
'',
'',
'aditya eka putra',
'1231231285675675',
'ktp.jpg',
'aditya eka putra',
'Pelatihan dan Ujian Sertifikasi Pengadaan Barang/Jasa Pemerintah',
'2233123',
'swara.ogawa@gmail.com',
'aditd',
'23213',
'a.a@a.com',
'suraat_ijin_operasional.doc',
'keterangan_domisili.doc',
'akta_notaris.doc',
'mekanisme_keuangan.doc',
'sop.docx',
'komitmen.doc',
'tugas_pelaksana.pdf')*/