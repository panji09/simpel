<?php
class Model_notif_pelatihan extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }
    
    public function getAllNotif()
    {
        $this->db->select('*');
        $this->db->join('tbl_narasumber', 'tbl_narasumber.id_narasumber = pelatihan_narasumber.id_narsumber');
        $this->db->from('pelatihan_narasumber');

        $query = $this->db->get();

        return $query->result();
    }
    public function countAllNotif()
    {
        $query = $this->db->query("SELECT (SELECT COUNT(*) FROM (select * from tbl_request_pelatihan) AS ROW) AS ROW");
        $id_key = $query->row();

        if($id_key!=null){
            return $id_key->ROW;
        }else{
            return 0;
        }
    }

    public function getAllNotifWhere($stat)
    {
        $this->db->select('*');
        $this->db->join('tbl_tema_pelatihan', 'tbl_tema_pelatihan.id_tema = tbl_request_pelatihan.id_tema');
        $this->db->where('tbl_request_pelatihan.approve',$stat);
        $this->db->from('tbl_request_pelatihan');

        $query = $this->db->get();

        return $query->result();
    }
    public function addNewRequest()
    {
        $tema_pelatihan = $this->input->post('tema_pelatihan');
        $dari_tanggal = $this->input->post('dari_tanggal');
        $sampai_tanggal = $this->input->post('sampai_tanggal');
        $isi_pelatihan = $this->input->post('isi_pelatihan');
        $tempat_pelatihan = $this->input->post('tempat_pelatihan');
        $id = $this->session->userdata('id');
        $date = date('Y-m-d');

        if($this->db->query("INSERT INTO tbl_request_pelatihan VALUES('','$date','$tema_pelatihan','$isi_pelatihan','$tempat_pelatihan','$dari_tanggal','$sampai_tanggal','lpp','$id','0','0')")){
            return true;
        }else{
            return false;
        }
    }

    public function getApprovedRequest()
    {
        $this->db->select('*');
        $this->db->join('tbl_tema_pelatihan', 'tbl_tema_pelatihan.id_tema = tbl_request_pelatihan.id_tema');
        $this->db->where('approve','1');
        $this->db->from('tbl_request_pelatihan');

        $query = $this->db->get();

        return $query->result();
    }
    public function countApprovedRequest()
    {
        $query = $this->db->query("SELECT (SELECT COUNT(*) FROM (select * from tbl_request_pelatihan WHERE approve = '1') AS ROW) AS ROW");
        $id_key = $query->row();

        if($id_key!=null){
            return $id_key->ROW;
        }else{
            return 0;
        }
    }
    public function getCanceledRequest()
    {
        $this->db->select('*');
        $this->db->where('approve','2');
        $this->db->from('tbl_request_pelatihan');

        $query = $this->db->get();

        return $query->result();
    }
    public function countCanceledRequest()
    {
        $query = $this->db->query("SELECT (SELECT COUNT(*) FROM (select * from tbl_request_pelatihan WHERE approve = '2') AS ROW) AS ROW");
        $id_key = $query->row();

        if($id_key!=null){
            return $id_key->ROW;
        }else{
            return 0;
        }
    }
    public function getPendingRequest()
    {
        $this->db->select('*');
        $this->db->where('approve','0');
        $this->db->from('tbl_request_pelatihan');

        $query = $this->db->get();

        return $query->result();
    }
    public function countPendingRequest()
    {
        $query = $this->db->query("SELECT (SELECT COUNT(*) FROM (select * from tbl_request_pelatihan WHERE approve = '0') AS ROW) AS ROW");
        $id_key = $query->row();

        if($id_key!=null){
            return $id_key->ROW;
        }else{
            return 0;
        }
    }


    public function getNotifApprovedRequest()
    {
        $this->db->select('*');
        $this->db->join('tbl_tema_pelatihan', 'tbl_tema_pelatihan.id_tema = tbl_request_pelatihan.id_tema');
        $this->db->where('tbl_request_pelatihan.approve','1');
        $this->db->where('tbl_request_pelatihan.read','0');
        $this->db->from('tbl_request_pelatihan');

        $query = $this->db->get();

        return $query->result();
    }
    public function countNotifApprovedRequest()
    {
        $query = $this->db->query("SELECT (SELECT COUNT(*) FROM (SELECT * FROM  `tbl_request_pelatihan` WHERE  `approve` =1 AND  `read` =0) AS ROW) AS ROW");
        $id_key = $query->row();

        if($id_key!=null){
            return $id_key->ROW;
        }else{
            return 0;
        }
    }
    public function getNotifPendingRequest()
    {
        $this->db->select('*');
        $this->db->join('tbl_tema_pelatihan', 'tbl_tema_pelatihan.id_tema = tbl_request_pelatihan.id_tema');
        $this->db->where('tbl_request_pelatihan.approve','0');
        $this->db->where('tbl_request_pelatihan.read','0');
        $this->db->from('tbl_request_pelatihan');

        $query = $this->db->get();

        return $query->result();
    }
    public function countNotifPendingRequest()
    {
        $query = $this->db->query("SELECT (SELECT COUNT(*) FROM (SELECT * FROM  `tbl_request_pelatihan` WHERE  `approve` =0 AND  `read` =0) AS ROW) AS ROW");
        $id_key = $query->row();

        if($id_key!=null){
            return $id_key->ROW;
        }else{
            return 0;
        }
    }
    public function getNotifCanceledRequest()
    {
        $this->db->select('*');
        $this->db->join('tbl_tema_pelatihan', 'tbl_tema_pelatihan.id_tema = tbl_request_pelatihan.id_tema');
        $this->db->where('tbl_request_pelatihan.approve','2');
        $this->db->where('tbl_request_pelatihan.read','0');
        $this->db->from('tbl_request_pelatihan');

        $query = $this->db->get();

        return $query->result();
    }
    public function countNotifCanceledRequest()
    {
        $query = $this->db->query("SELECT (SELECT COUNT(*) FROM (SELECT * FROM  `tbl_request_pelatihan` WHERE  `approve` =2 AND  `read` =0) AS ROW) AS ROW");
        $id_key = $query->row();

        if($id_key!=null){
            return $id_key->ROW;
        }else{
            return 0;
        }
    }

}