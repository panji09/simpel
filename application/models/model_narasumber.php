<?php
class Model_narasumber extends CI_Model {

    private $sqlFindNarasumber = "";
    private $sqlFindNarasumberCondition = "";
    private $conditionToBeReturn;

    function __construct()
    {
        parent::__construct();
        $this->load->library('MY_Upload');
    }
    public function record_count($table) {
        return $this->db->count_all($table);
    }
    public function getAbsolutelyAllNarasumber()
    {
        /*$this->db->select('*');
        $this->db->join('tbl_kegiatan_natasumber', 'tbl_narasumber_new.id_narasumber = tbl_kegiatan_natasumber.id_narasumber');
        $this->db->limit($limit, $start);
        $this->db->from('tbl_narasumber_new');*/
        /*$sql = "SELECT * FROM (`tbl_narasumber_new`) JOIN `tbl_kegiatan_natasumber` ON `tbl_narasumber_new`.`id_narasumber` = `tbl_kegiatan_natasumber`.`id_narasumber` LIMIT $start,$limit";*/
        $sql = "SELECT * FROM tbl_narasumber_new ORDER BY nama_narasumber ASC";

        $query = $this->db->query($sql);
        return $query->result();
    }
    public function findNarasumberHome($nama)
    {
        /*$this->db->select('*');
        $this->db->join('tbl_kegiatan_natasumber', 'tbl_narasumber_new.id_narasumber = tbl_kegiatan_natasumber.id_narasumber');
        $this->db->limit($limit, $start);
        $this->db->from('tbl_narasumber_new');*/
        /*$sql = "SELECT * FROM (`tbl_narasumber_new`) JOIN `tbl_kegiatan_natasumber` ON `tbl_narasumber_new`.`id_narasumber` = `tbl_kegiatan_natasumber`.`id_narasumber` LIMIT $start,$limit";*/
        $sql = "SELECT * FROM tbl_narasumber_new WHERE nama_narasumber LIKE '%$nama%' ";

        $query = $this->db->query($sql);
        return $query->result();
    }
    public function getAllNarasumber($limit, $start)
    {
        /*$this->db->select('*');
        $this->db->join('tbl_kegiatan_natasumber', 'tbl_narasumber_new.id_narasumber = tbl_kegiatan_natasumber.id_narasumber');
        $this->db->limit($limit, $start);
        $this->db->from('tbl_narasumber_new');*/
        /*$sql = "SELECT * FROM (`tbl_narasumber_new`) JOIN `tbl_kegiatan_natasumber` ON `tbl_narasumber_new`.`id_narasumber` = `tbl_kegiatan_natasumber`.`id_narasumber` LIMIT $start,$limit";*/
        $sql = "SELECT id_narasumber,nama_narasumber,lokasi,instansi_narsum AS instansi FROM tbl_narasumber_new LIMIT $start,$limit";

        $query = $this->db->query($sql);
        return $query->result();
    }
    public function getNarasumberName($id)
    {
        $query = $this->db->query("SELECT nama_narasumber from tbl_narasumber_new WHERE id_narasumber = '$id' ");
        $id_key = $query->row();

        if($id_key!=null){
            return $id_key->nama_narasumber;
        }else{
            return 0;
        }
    }
    public function getNarasumberById($id,$lokasi)
    {
        $condition = "";

        if($id!=null){
            $condition = " id_narasumber = '$id' ";
        }

        if($lokasi!=null){
            $condition = " lokasi LIKE '%$lokasi%' ";
        }

        $sql = "SELECT * FROM tbl_narasumber_new WHERE $condition";

        $query = $this->db->query($sql);
        return $query->result();
    }
    public function findNarasumber($start,$limit)
    {
        $narasumber = $this->input->post('id_narasumber');
        $dari_tanggal = $this->input->post('dari_tanggal');
        $sampai_tanggal = $this->input->post('sampai_tanggal');
        $catatan = $this->input->post('catatan');
        $lokasi = $this->input->post('lokasi');
        $instansi = $this->input->post('instansi');
        $JP = $this->input->post('JP');
        $JP_terkecil = $this->input->post('JP_terkecil');
        $tahun = $this->input->post('tahun');
        //$limit = $this->input->post('limit');

        $condition = "";
        $sort = "";
        $limits = "";
        $sort_status = 0;
        $limit_status = 0;
        $sort_condition = "";
        $condition_inside_total_jp = "";

        if($narasumber!=null){
            $condition .= "tbl_narasumber_new.id_narasumber = '$narasumber' ";
            if(($tahun!=null)||($lokasi!=null)){
                $condition .= " AND ";
            }
        }  

        if($tahun!=null){
            $condition .= "year(tbl_kegiatan_natasumber.tanggal) = '$tahun' ";
            if(($narasumber!=null)||($lokasi!=null)){
                $condition .= " AND ";
                $condition_inside_total_jp .= "AND year(tbl_kegiatan_natasumber.tanggal) = '$tahun' ";
            }else{
                $condition_inside_total_jp .= "AND year(tbl_kegiatan_natasumber.tanggal) = '$tahun' ";
            }
        }  

        if($lokasi!=null){
            $condition .= "tbl_narasumber_new.lokasi LIKE '%$lokasi%' ";
            if(($tahun!=null)||($narasumber!=null)){
                $condition .= " AND ";
                $condition_inside_total_jp .= "AND tbl_narasumber_new.lokasi LIKE '%$lokasi%' ";
            }else{
                $condition_inside_total_jp .= "AND tbl_narasumber_new.lokasi LIKE '%$lokasi%' ";
            }
        }  

        if($JP!=null){
            if($JP == "terbanyak"){
                $sort .= "GROUP BY tbl_narasumber_new.id_narasumber ORDER BY total_jp DESC";
                $sort_status = 1;
            }else{
                $sort .= "GROUP BY tbl_narasumber_new.id_narasumber ORDER BY total_jp ASC";
                $sort_status = 1;
            }
            
        }  

        /*if($JP_terkecil!=null){
            $sort .= "ORDER BY tbl_kegiatan_natasumber.jumlah_jp ASC";
            $sort_status = 1;
        } */

        /*if($limit!=null){
            $limits .= " LIMIT $limit,$start ";
            $limit_status = 1;
        }    */   
        
        if($condition!= ""){
            $condition = "AND ".$condition;
        }
        
        if($sort_status == 1){
            $conditionsort = $condition." ".$sort;
            $sorter = str_replace("AND  ORDER ","ORDER ",$conditionsort);
            $sorter = str_replace("AND  GROUP ","GROUP ",$conditionsort);
            //$newcondition = str_replace(" ","*",$conditionsort);

            //echo $newcondition;
            if($limit_status == 1){
                $newcondition = $sorter." ".$limits;
                $newcondition = str_replace("AND   LIMIT ","LIMIT ",$newcondition);
            }else{
                $newcondition = $sorter;
            }
        }else if($limit_status == 1){
            $conditionlimit = $condition." ".$limits;
            $limiter = str_replace("AND   LIMIT ","LIMIT ",$conditionlimit);
            //$newcondition = str_replace(" ","*",$conditionsort);

            //echo $newcondition;
            if($sort_status == 1){
                $newcondition = $limiter." ".$sort;
                $newcondition = str_replace("AND  ORDER ","ORDER ",$newcondition);
                $sorter = str_replace("AND  GROUP ","GROUP ",$conditionsort);
            }else{
                $newcondition = $limiter;
            }
        }




        if(($sort_status == 0)&&($limit_status == 0)){
            $newcondition = $condition;
        }
        
        //echo $sort_status;

        /*SELECT tbl_narasumber_new.id_narasumber,nama_narasumber,no_telp,lokasi,email,tanggal,(SELECT SUM(jumlah_jp) AS total_jp FROM tbl_kegiatan_natasumber WHERE tbl_kegiatan_natasumber.id_narasumber = tbl_narasumber_new.id_narasumber) AS total_jp FROM `tbl_narasumber_new`,tbl_kegiatan_natasumber WHERE tbl_narasumber_new.id_narasumber = tbl_kegiatan_natasumber.id_narasumber AND tbl_kegiatan_natasumber.tanggal >= '2013-01-01' AND tbl_kegiatan_natasumber.tanggal <= '2013-01-31' ORDER BY tbl_kegiatan_natasumber.jumlah_jp DESC*/

        /*YANG LAMA*/
        /*$sql = "SELECT id_narasumber,nama_narasumber,no_telp,lokasi,email,(SELECT SUM(jumlah_jp) AS total_jp FROM tbl_kegiatan_natasumber WHERE tbl_kegiatan_natasumber.id_narasumber = tbl_narasumber_new.id_narasumber) AS total_jp FROM `tbl_narasumber_ne` ".$newcondition;*/

        /*BENER TAPI DIGANTI LAGI*/
        /*$sql = "SELECT tbl_narasumber_new.id_narasumber,nama_narasumber,no_telp,lokasi,email,tanggal,(SELECT SUM(jumlah_jp) AS total_jp FROM tbl_kegiatan_natasumber WHERE tbl_kegiatan_natasumber.id_narasumber = tbl_narasumber_new.id_narasumber ) AS total_jp FROM `tbl_narasumber_new`,tbl_kegiatan_natasumber WHERE tbl_narasumber_new.id_narasumber = tbl_kegiatan_natasumber.id_narasumber ".$newcondition;*/

        $sql = "SELECT tbl_narasumber_new.id_narasumber,nama_narasumber,no_telp,lokasi,email,tanggal,instansi,(SELECT SUM(jumlah_jp) AS total_jp FROM tbl_kegiatan_natasumber WHERE tbl_kegiatan_natasumber.id_narasumber = tbl_narasumber_new.id_narasumber $condition_inside_total_jp) AS total_jp FROM `tbl_narasumber_new`,tbl_kegiatan_natasumber WHERE tbl_narasumber_new.id_narasumber = tbl_kegiatan_natasumber.id_narasumber ".$newcondition;

        $this->sqlFindNarasumber = $sql;
        $this->sqlFindNarasumberCondition = $newcondition;

        //$this->setConditionToBeReturn($newcondition);

        $query = $this->db->query($sql);
        return $query->result();
    }

    /*public function setConditionToBeReturn($condition)
    {
        $this->conditionToBeReturn = $condition;
    }
    public function getConditionToBeReturn()
    {
        return $this->conditionToBeReturn;
    }*/

    public function getRowFromfindNarasumber()
    {
        $sqlString = explode('LIMIT',$this->sqlFindNarasumber);
        $sql = "SELECT (SELECT COUNT(*) FROM ($sqlString[0]) AS ROW) AS ROW";
        $query = $this->db->query($sql);
        $id_key = $query->row();

        return $id_key->ROW;
    }
    public function getCondition()
    {
        return $this->sqlFindNarasumberCondition;
    }
    public function getNarasumber()
    {
        $this->db->select('*');
        $this->db->order_by('nama_narasumber','ASC');
        $this->db->from('tbl_narasumber_new');

        $query = $this->db->get();

        return $query->result();
    }
    public function countAllNarasumber()
    {
        $query = $this->db->query("SELECT (SELECT COUNT(*) FROM (select * from tbl_narasumber_new) AS ROW) AS ROW");
        $id_key = $query->row();

        if($id_key!=null){
            return $id_key->ROW;
        }else{
            return 0;
        }
    }

    public function getAllNarasumberByIdPelatihan($id)
    {
        $id_pelatihan = $this->getIdPelatihanByIdRequest($id);

        $this->db->select('*');
        $this->db->join('tbl_pelatihan', 'tbl_pelatihan.id_pelatihan = pelatihan_narasumber.id_pelatihan');
        $this->db->join('tbl_tema_pelatihan', 'tbl_pelatihan.id_tema = tbl_tema_pelatihan.id_tema');
        $this->db->join('tbl_narasumber_new', 'tbl_narasumber_new.id_narasumber = pelatihan_narasumber.id_narasumber');
        $this->db->from('pelatihan_narasumber');
        $this->db->where('pelatihan_narasumber.id_pelatihan',$id_pelatihan);

        $query = $this->db->get();

        return $query->result();
    }
    public function getAllNarasumberByRealIdPelatihan($id)
    {
        $this->db->select('*');
        $this->db->join('tbl_pelatihan', 'tbl_pelatihan.id_pelatihan = pelatihan_narasumber.id_pelatihan');
        $this->db->join('tbl_tema_pelatihan', 'tbl_pelatihan.id_tema = tbl_tema_pelatihan.id_tema');
        $this->db->join('tbl_narasumber_new', 'tbl_narasumber_new.id_narasumber = pelatihan_narasumber.id_narasumber');
        $this->db->from('pelatihan_narasumber');
        $this->db->where('pelatihan_narasumber.id_pelatihan',$id);

        $query = $this->db->get();

        return $query->result();
    }
    public function getAllNarasumberByRealIdPelatihanTidakDitolak($id)
    {
        $this->db->select('*');
        $this->db->join('tbl_pelatihan', 'tbl_pelatihan.id_pelatihan = pelatihan_narasumber.id_pelatihan');
        $this->db->join('tbl_tema_pelatihan', 'tbl_pelatihan.id_tema = tbl_tema_pelatihan.id_tema');
        $this->db->join('tbl_narasumber_new', 'tbl_narasumber_new.id_narasumber = pelatihan_narasumber.id_narasumber');
        $this->db->from('pelatihan_narasumber');
        $this->db->where('pelatihan_narasumber.id_pelatihan',$id);
        $this->db->where_not_in('pelatihan_narasumber.approve','2');

        $query = $this->db->get();

        return $query->result();
    }
    public function countTotalJPByRealIdPelatihan($id)
    {

        $query = $this->db->query("SELECT
                    *,
                    SUM(total_jam) AS Total
                FROM
                    pelatihan_narasumber
                WHERE
                    approve = '1'
                AND
                    id_pelatihan = '$id'");

        $id_key = $query->row();

        return $id_key->Total;
    }
    public function countTotalJPByRealIdPelatihanNotApprove($id)
    {

        $query = $this->db->query("SELECT
                    *,
                    SUM(total_jam) AS Total
                FROM
                    pelatihan_narasumber
                WHERE
                    approve != '2'
                AND
                    id_pelatihan = '$id'");

        $id_key = $query->row();

        return $id_key->Total;
    }
    public function countTotalJPAllByRealIdPelatihan($id)
    {

        $query = $this->db->query("SELECT
                    *,
                    SUM(total_jam) AS Total
                FROM
                    pelatihan_narasumber");

        $id_key = $query->row();

        return $id_key->Total;
    }
    public function getAllTemporaryNarasumberByRealIdPelatihan($id)
    {
        $this->db->select('*');
        $this->db->join('tbl_pelatihan', 'tbl_pelatihan.id_pelatihan = pelatihan_narasumber_temporary.id_pelatihan');
        $this->db->join('tbl_tema_pelatihan', 'tbl_pelatihan.id_tema = tbl_tema_pelatihan.id_tema');
        $this->db->join('tbl_narasumber_new', 'tbl_narasumber_new.id_narasumber = pelatihan_narasumber_temporary.id_narasumber');
        $this->db->from('pelatihan_narasumber_temporary');
        $this->db->where('pelatihan_narasumber_temporary.id_pelatihan',$id);

        $query = $this->db->get();

        return $query->result();
    }
    public function getAprovedNarasumberByRealIdPelatihan($id)
    {
        $this->db->select('*');
        $this->db->join('tbl_pelatihan', 'tbl_pelatihan.id_pelatihan = pelatihan_narasumber.id_pelatihan');
        $this->db->join('tbl_tema_pelatihan', 'tbl_pelatihan.id_tema = tbl_tema_pelatihan.id_tema');
        $this->db->join('tbl_narasumber_new', 'tbl_narasumber_new.id_narasumber = pelatihan_narasumber.id_narasumber');
        $this->db->from('pelatihan_narasumber');
        $this->db->where('pelatihan_narasumber.id_pelatihan',$id);
        $this->db->where('pelatihan_narasumber.approve','1');

        $query = $this->db->get();

        return $query->result();
    }
    public function getIdPelatihanByIdRequest($id)
    {
        $this->db->select('*');
        $this->db->where('id_request',$id);
        $this->db->from('tbl_pelatihan');
        $query = $this->db->get();
        $id_key = $query->row();

        return $id_key->id_pelatihan;
    }
    public function getAllPelatihanNarasumber() 
    {
        $this->db->select('*'); 
        $this->db->from('pelatihan_narasumber');

        $query = $this->db->get();

        return $query->result();
    }
    public function getNarasumberAvailable($dari_tanggal,$sampai_tanggal)
    {
        //$query = $this->db->query("SELECT * FROM cek_kegiatan_narasumber,tbl_narasumber_new WHERE cek_kegiatan_narasumber.id_narasumber = tbl_narasumber_new.id_narasumber AND (cek_kegiatan_narasumber.dari_tanggal NOT BETWEEN '$dari_tanggal' AND '$sampai_tanggal')AND (cek_kegiatan_narasumber.sampai_tanggal NOT BETWEEN '$dari_tanggal' AND '$sampai_tanggal') GROUP BY cek_kegiatan_narasumber.id_narasumber");\

        $query = $this->db->query("SELECT *,tbl_narasumber_new.id_narasumber AS id_narsum
        FROM tbl_narasumber_new
        LEFT OUTER JOIN cek_kegiatan_narasumber
        ON cek_kegiatan_narasumber.id_narasumber = tbl_narasumber_new.id_narasumber AND (cek_kegiatan_narasumber.dari_tanggal NOT BETWEEN '$dari_tanggal' AND '$sampai_tanggal')AND (cek_kegiatan_narasumber.sampai_tanggal NOT BETWEEN '$dari_tanggal' AND '$sampai_tanggal')");

        /*$query_2 = $this->db->query("SELECT * FROM tbl_narasumber_new");*/

        $return = $query->result();
        if($return){
            return $query->result();
        }/*else{
            return $query_2->result();
        }*/
        
    }
    public function input_narasumber_temporary()
    {
        //$id_request = $this->input->post('id_request');
        //$id_pelatihan = $this->getIdPelatihanByIdRequest($id_request);
        $id_pelatihan = $this->input->post('id_pelatihan');
        $tgl_pelatihan = $this->input->post('tgl_pelatihan');
        $dari_jam = $this->input->post('dari_jam');
        $sampai_jam = $this->input->post('sampai_jam');
        $id_narasumber = $this->input->post('id_narasumber');
        $total_jp = $this->input->post('total_jp');
        $materi_pelatihan = $this->input->post('materi_pelatihan');
        $total_jam = $sampai_jam - $dari_jam;


            $proses_narasumber_pelatihan = $this->db->query("INSERT INTO pelatihan_narasumber_temporary VALUES('','$id_pelatihan','$tgl_pelatihan','$dari_jam','$sampai_jam','$id_narasumber','$total_jp','','','','','','$materi_pelatihan')");

            if($proses_narasumber_pelatihan){

                $status = true;
            }else{
                $status = true;
            }
       

        return $status;

        
    }
    public function input_narasumber()
    {
        //$id_request = $this->input->post('id_request');
        //$id_pelatihan = $this->getIdPelatihanByIdRequest($id_request);
        $id_pelatihan = $this->input->post('id_pelatihan');
        $tgl_pelatihan = $this->input->post('tgl_pelatihan');
        $dari_jam = $this->input->post('dari_jam');
        $sampai_jam = $this->input->post('sampai_jam');
        $id_narasumber = $this->input->post('id_narasumber');
        $total_jp = $this->input->post('total_jp');
        $total_jam = $sampai_jam - $dari_jam;

        $config['upload_path'] = '././assets/upload/materi_pelatihan/';
        $config['allowed_types'] = 'pdf|doc|docx|xls|xlsx|ppt|pptx';
        $config['max_size'] = '10000';
        $config['max_width']  = '1024';
        $config['max_height']  = '768';
        $config['overwrite'] = true;


        $this->load->library('upload', $config);
        $this->upload->overwrite = true;
        $this->upload->initialize($config);


        if ( ! $this->upload->do_upload())
        {
            $msg = $this->upload->display_errors('', '');
            $status = $msg;
        }
        else
        {
            $data = array('upload_data' => $this->upload->data());
            $status = true;

             $data = array('upload_data' => $this->upload->data());
                foreach ($data as $item => $value):
                    $file = $value['file_name'];
                endforeach;

            $proses_narasumber_pelatihan = $this->db->query("INSERT INTO pelatihan_narasumber VALUES('','$id_pelatihan','$tgl_pelatihan','$dari_jam','$sampai_jam','$id_narasumber','$total_jp','','','','','','$file')");

            if($proses_narasumber_pelatihan){

                $status = true;
            }else{
                $status = true;
            }
        }

       

        return $status;

        
    }

    public function getApprovedRequest()
    {
        $this->db->select('*');
        $this->db->join('tbl_narasumber_new', 'tbl_narasumber_new.id_narasumber = pelatihan_narasumber.id_narasumber');
        $this->db->where('approve','1');
        $this->db->from('pelatihan_narasumber');

        $query = $this->db->get();

        return $query->result();
    }

    public function countApprovedRequest()
    {
        $query = $this->db->query("SELECT (SELECT COUNT(*) FROM (select * from pelatihan_narasumber WHERE approve = '1') AS ROW) AS ROW");
        $id_key = $query->row();

        if($id_key!=null){
            return $id_key->ROW;
        }else{
            return 0;
        }
    }

    public function getCanceledRequest()
    {
        $this->db->select('*');
        $this->db->where('approve','2');
        $this->db->from('pelatihan_narasumber');

        $query = $this->db->get();

        return $query->result();
    }

    public function countCanceledRequest()
    {
        $query = $this->db->query("SELECT (SELECT COUNT(*) FROM (select * from pelatihan_narasumber WHERE approve = '2') AS ROW) AS ROW");
        $id_key = $query->row();

        if($id_key!=null){
            return $id_key->ROW;
        }else{
            return 0;
        }
    }

    public function getPendingRequest()
    {
        $this->db->select('*');
        $this->db->where('approve','0');
        $this->db->from('pelatihan_narasumber');

        $query = $this->db->get();

        return $query->result();
    }

    public function countPendingRequest()
    {
        $query = $this->db->query("SELECT (SELECT COUNT(*) FROM (select * from pelatihan_narasumber WHERE approve = '0') AS ROW) AS ROW");
        $id_key = $query->row();

        if($id_key!=null){
            return $id_key->ROW;
        }else{
            return 0;
        }
    }

    public function getNotifApprovedRequest()
    {
        $this->db->select('*');
        $this->db->join('tbl_narasumber_new', 'tbl_narasumber_new.id_narasumber = pelatihan_narasumber.id_narasumber');
        $this->db->where('pelatihan_narasumber.approve','1');
        //$this->db->where('pelatihan_narasumber.read','0');
        $this->db->from('pelatihan_narasumber');

        $query = $this->db->get();

        return $query->result();
    }

    public function countNotifApprovedRequest()
    {
        $query = $this->db->query("SELECT (SELECT COUNT(*) FROM (SELECT * FROM  `pelatihan_narasumber` WHERE  `approve` =1) AS ROW) AS ROW");
        $id_key = $query->row();

        if($id_key!=null){
            return $id_key->ROW;
        }else{
            return 0;
        }
    }

    public function getJPagendapribadi($id)
    {
        $query = $this->db->query("SELECT SUM(jumlah_jp) as jumlah FROM rencana_kegiatan WHERE id_narasumber='$id' AND approved='1'");
        $id_key = $query->row();

        if($id_key!=null){
            return $id_key->jumlah;
        }else{
            return 0;
        }
    }

    public function getJPlkpp($id)
    {
        $query = $this->db->query("SELECT SUM(total_jam) as jumlah FROM pelatihan_narasumber WHERE id_narasumber='$id' AND approve='1'");
        $id_key = $query->row();

        if($id_key!=null){
            return $id_key->jumlah;
        }else{
            return 0;
        }
    }

    public function getNotifPendingRequest($id)
    {
       
        $this->db->select('*');
        $this->db->join('tbl_narasumber_new', 'tbl_narasumber_new.id_narasumber = pelatihan_narasumber.id_narasumber');
        $this->db->join('tbl_pelatihan', 'tbl_pelatihan.id_pelatihan = pelatihan_narasumber.id_pelatihan');
        $this->db->join('tbl_tema_pelatihan', 'tbl_tema_pelatihan.id_tema = tbl_pelatihan.id_tema');
        $this->db->where('pelatihan_narasumber.approve','0');
        $this->db->where('pelatihan_narasumber.id_narasumber',$id);
        $this->db->from('pelatihan_narasumber');

        $query = $this->db->get();

        return $query->result();
    }

    public function countNotifPendingRequest($id)
    {
        $query = $this->db->query("SELECT (SELECT COUNT(*) FROM (SELECT * FROM  `pelatihan_narasumber` WHERE  `approve` =0 AND id_narasumber = '$id') AS ROW) AS ROW");
        $id_key = $query->row();

        if($id_key!=null){
            return $id_key->ROW;
        }else{
            return 0;
        }
    }

    public function getNotifCanceledRequest()
    {
        $this->db->select('*');
        $this->db->join('tbl_narasumber_new', 'tbl_narasumber_new.id_narasumber = pelatihan_narasumber.id_narasumber');
        $this->db->where('pelatihan_narasumber.approve','2');
        //$this->db->where('pelatihan_narasumber.read','0');
        $this->db->from('pelatihan_narasumber');

        $query = $this->db->get();

        return $query->result();
    }

    public function countNotifCanceledRequest()
    {
        $query = $this->db->query("SELECT (SELECT COUNT(*) FROM (SELECT * FROM  `pelatihan_narasumber` WHERE  `approve` =2) AS ROW) AS ROW");
        $id_key = $query->row();

        if($id_key!=null){
            return $id_key->ROW;
        }else{
            return 0;
        }
    }


    public function getSchedulePelatihanNarasumber($id)
    {    
        $now = date('Y-m-d');

        $this->db->select('*');
        $this->db->join('tbl_pelatihan', 'tbl_pelatihan.id_pelatihan = pelatihan_narasumber.id_pelatihan');
        $this->db->join('tbl_lpp', 'tbl_lpp.id_lpp = tbl_pelatihan.id_lpp');
        $this->db->join('tbl_tema_pelatihan', 'tbl_tema_pelatihan.id_tema = tbl_pelatihan.id_tema');
        $this->db->where('pelatihan_narasumber.approve','1');
        $this->db->where('pelatihan_narasumber.id_narasumber',$id);
        $this->db->where("tbl_pelatihan.sampai_tanggal >= '$now' ");
        $this->db->from('pelatihan_narasumber');

        $query = $this->db->get();

        return $query->result();
    }

    public function getArsipPelatihanNarasumber($id)
    {    
        $now = date('Y-m-d');

        $this->db->select('*');
        $this->db->join('tbl_pelatihan', 'tbl_pelatihan.id_pelatihan = pelatihan_narasumber.id_pelatihan');
        $this->db->join('tbl_lpp', 'tbl_lpp.id_lpp = tbl_pelatihan.id_lpp');
        $this->db->join('tbl_tema_pelatihan', 'tbl_tema_pelatihan.id_tema = tbl_pelatihan.id_tema');
        $this->db->where('pelatihan_narasumber.approve','1');
        $this->db->where('pelatihan_narasumber.id_narasumber',$id);
        $this->db->where("tbl_pelatihan.sampai_tanggal < '$now' ");
        $this->db->from('pelatihan_narasumber');

        $query = $this->db->get();

        return $query->result();
    }


    public function getAllRequestPelatihan()
    {
        $this->db->select('*');
        $this->db->join('tbl_pelatihan', 'tbl_pelatihan.id_pelatihan = pelatihan_narasumber.id_pelatihan');
        //$this->db->where('approve','1');
        $this->db->from('pelatihan_narasumber');

        $query = $this->db->get();

        return $query->result();
    }

    public function getAllRequestPelatihanById($id_narasumber)
    {
        $this->db->select('*');
        $this->db->join('tbl_pelatihan', 'tbl_pelatihan.id_pelatihan = pelatihan_narasumber.id_pelatihan');
        $this->db->join('tbl_lpp', 'tbl_lpp.id_lpp = tbl_pelatihan.id_lpp');
        $this->db->join('tbl_tema_pelatihan', 'tbl_pelatihan.id_tema = tbl_tema_pelatihan.id_tema');
        $this->db->where('pelatihan_narasumber.id_narasumber',$id_narasumber);
        $this->db->where('pelatihan_narasumber.approve','0');
        $this->db->from('pelatihan_narasumber');

        $query = $this->db->get();

        return $query->result();
    }

    public function getDetailRequestPelatihanById($id)
    {
        $this->db->select('*');
        $this->db->join('tbl_pelatihan', 'tbl_pelatihan.id_pelatihan = pelatihan_narasumber.id_pelatihan');
        $this->db->join('tbl_lpp', 'tbl_lpp.id_lpp = tbl_pelatihan.id_lpp');
        $this->db->join('tbl_tema_pelatihan', 'tbl_tema_pelatihan.id_tema = tbl_pelatihan.id_tema');
        $this->db->where('pelatihan_narasumber.id',$id);
        $this->db->from('pelatihan_narasumber');

        $query = $this->db->get();

        return $query->result();
    }

    public function update_status_pelatihan_narasumber($id)
    {
        //$id = $this->input->post('id');
        $approve = $this->input->post('submit');
        $alasan = $this->input->post('alasan_tolak');

        /*$data = array(
               'approve' => $approve
        );

        $this->db->where('id', $id);
        $this->db->update('pelatihan_narasumber', $data); */

        $status = "";

        if($approve==1){
            $myData = $this->getDetailRequestPelatihanById($id);
            foreach ($myData as $data):
                $id_narasumber = $data->id_narasumber;
                $tgl_pelatihan = $data->tgl_pelatihan;
                $jumlah_jp = $data->total_jam;
                $instansi = $data->instansi;
                $id_pelatihan = $data->id_pelatihan;

                $proses = $this->db->query("INSERT INTO tbl_kegiatan_natasumber VALUES('$instansi','$id_narasumber','','$tgl_pelatihan','$jumlah_jp','$id_pelatihan')");

                if($proses){
                    $status = "success";
                }else{
                    $status = "failed";
                }
            endforeach;

            $data = array(
                   'approve' => $approve
            );

            $this->db->where('id', $id);
            $this->db->update('pelatihan_narasumber', $data);

        }else if($approve==2){
            $data = array(
                   'approve' => $approve,
                   'alasan' => $alasan
            );

            $this->db->where('id', $id);
            $this->db->update('pelatihan_narasumber', $data);

            $status = "success";
        }

        return $status;
        
    }

 
    public function insert_new_narasumber()
    {
        $nama_lengkap = $this->input->post('nama_lengkap');
        $alamat = $this->input->post('alamat');
        $no_telp = $this->input->post('no_telp');
        $tempat_lahir = $this->input->post('tempat_lahir');
        $tgl_lahir = $this->input->post('tgl_lahir');
        $gelar = $this->input->post('gelar');
        $jenis_kelamin = $this->input->post('jenis_kelamin');
        $pendidikan = $this->input->post('pendidikan');
        $instansi = $this->input->post('instansi');
        $satuan_kerja = $this->input->post('satuan_kerja');
        $provinsi = $this->input->post('provinsi');
        $kota = $this->input->post('kota');
        $email = $this->input->post('email');
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        $no_sertifikat = $this->input->post('no_sertifikat');
        $hasil_evaluasi = $this->input->post('hasil_evaluasi');

        $jabatan_narasumber = $this->input->post('jabatan_narasumber');
        $web_narasumber = $this->input->post('web_narasumber');
        $pangkat_narasumber = $this->input->post('pangkat_narasumber');
        $kota_kantor = $this->input->post('kota_kantor');
        $alamat_kantor = $this->input->post('alamat_kantor');

        
            $pangkat = $this->input->post('pangkat');
            if(isset($pangkat)){
                $pangkat = $this->input->post('pangkat');
            }else{
                $pangkat = "";
            }

            $golongan = $this->input->post('golongan');
            if(isset($golongan)){
                $golongan = $this->input->post('golongan');
            }else{
                $golongan = "";
            }

            $nip = $this->input->post('nip');
            if(isset($nip)){
                $nip = $this->input->post('nip');
            }else{
                $nip = "";
            }
            
            $status_pegawai = $this->input->post('status_pegawai');
            if(isset($status_pegawai)){
                $status_pegawai = $this->input->post('status_pegawai');
            }else{
                $status_pegawai = "";
            }
            
            $jenis_pegawai = $this->input->post('jenis_pegawai');
            if(isset($jenis_pegawai)){
                $jenis_pegawai = $this->input->post('jenis_pegawai');
            }else{
                $jenis_pegawai = "";
            }
            
            $jabatan_narasumber = $this->input->post('jabatan_narasumber');
            if(isset($jabatan_narasumber)){
                $jabatan_narasumber = $this->input->post('jabatan_narasumber');
            }else{
                $jabatan_narasumber = "";
            }
            

        $pass = base64_encode(md5($password));
        $id = $this->getLastIDNarasumber();
        $id_narasumber = explode('TOT',$id)[1];
        $id_narasumber = $id_narasumber+1;
        $id_narasumber = "TOT".$id_narasumber;
        
        $data_input = array(
	    'id_narasumber' => $id_narasumber,
	    'nama_narasumber' => $nama_lengkap,
	    'alamat_narasumber' => $alamat,
	    'no_telp_narasumber' => $no_telp,
	    'tempat_lahir_narasumber' => $tempat_lahir,
	    'tanggal_lahir_narasumber' => $tgl_lahir,
	    'nip_narasumber' => $nip,
	    'gelar_narasumber' => $gelar,
	    'jeniskel_narasumber' => $jenis_kelamin,
	    'golongan_narasumber' => $golongan,
	    'pendidikan_narasumber' => $pendidikan,
	    'photo_narasumber' => '',
	    'pangkat_narasumber' => $pangkat,
	    'satker_narasumber' => $satuan_kerja,
	    'status_pegawai_narasumber' => $status_pegawai,
	    'jenis_status_pegawai' => $jenis_pegawai,
	    'jabatan_narasumber' => $jabatan_narasumber,
	    'provinsi_narasumber' => $provinsi,
	    'kota_narasumber' => $kota,
	    'email_narasumber' => $email,
	    'status_aktif_narasumber' => 1,
	    'username' => $username,
	    'password' => $pass,
	    'approve_narasumber' => 1,
	    'instansi_narsum' => $instansi
        );
        
        $proses = $this->db->insert('tbl_narasumber_new', $data_input);
        /*
        $proses = $this->db->query("INSERT INTO tbl_narasumber_new VALUES('',
            '$id_narasumber',
            '$nama_lengkap',
            '$alamat',
            '$no_telp',
            '$tempat_lahir',
            '$tgl_lahir',
            '$nip','$gelar',
            '$jenis_kelamin',
            '$golongan',
            '$pendidikan',
            '',
            '$pangkat',
            '$satuan_kerja',
            '$status_pegawai',
            '$jenis_pegawai',
            '$jabatan_narasumber',
            '$provinsi',
            '$kota',
            '$email',
            '$web_narasumber',
            '1',
            '$username',
            '$pass',
            '1',
            '$instansi',
            '$kota',
            '$no_telp',
            '$email',
            '',
            '',
            '',
            '$no_sertifikat',
            '$hasil_evaluasi',
            '$alamat_kantor',
            '$kota_kantor',
            '',
            '')");
        */
        if($proses){
            return true;
        }else{
            return false;
        }

    }
    public function getLastIDNarasumber()
    {
        $query = $this->db->query("SELECT id_narasumber FROM tbl_narasumber_new ORDER BY id_narasumber DESC LIMIT 0,1");
        $id_key = $query->row();

        return $id_key->id_narasumber;
    }
    public function getNameNarasumberByID($id)
    {
        $query = $this->db->query("SELECT nama_narasumber FROM tbl_narasumber_new WHERE id_narasumber='$id' ");
        $id_key = $query->row();
        
        if($id_key)
        return $id_key->nama_narasumber;
    }
    
    public function getNameNarasumberByUsername($username)
    {
        $query = $this->db->query("SELECT nama_narasumber FROM tbl_narasumber_new WHERE username='$username' ");
        $id_key = $query->row();
        
        if($id_key)
        return $id_key->nama_narasumber;
    }
    public function getKlasemenNarasumber($start,$limit)
    {
        /*-----------------------------query nya ini-------------------
        SELECT id_narasumber,nama_narasumber,(SELECT SUM(jumlah_jp) AS total_jp FROM tbl_kegiatan_natasumber WHERE tbl_kegiatan_natasumber.id_narasumber = tbl_narasumber_new.id_narasumber) FROM `tbl_narasumber_new`
        ORDER BY `(SELECT SUM(jumlah_jp) AS total_jp FROM tbl_kegiatan_natasumber WHERE tbl_kegiatan_natasumber.id_narasumber = tbl_narasumber_new.id_narasumber)`  DESC*/
        /*------------------------------------------------------------*/

        $sql = "SELECT id_narasumber,nama_narasumber,no_telp,lokasi,email,instansi_narsum AS instansi,(SELECT SUM(jumlah_jp) AS total_jp FROM tbl_kegiatan_natasumber WHERE tbl_kegiatan_natasumber.id_narasumber = tbl_narasumber_new.id_narasumber) AS total_jp FROM `tbl_narasumber_new`
        ORDER BY `total_jp` DESC LIMIT $start,$limit ";

        $query = $this->db->query($sql);
        return $query->result();
        
    }
    public function getDetailKegiatanNarasumber($id,$condition)
    {
        /*$this->db->select('*');
        $this->db->join('tbl_kegiatan_natasumber', 'tbl_narasumber_new.id_narasumber = tbl_kegiatan_natasumber.id_narasumber');
        $this->db->where('tbl_kegiatan_natasumber.id_narasumber',$id);
        $this->db->order_by('tbl_kegiatan_natasumber.tanggal','DESC');
        $this->db->from('tbl_narasumber_new');*/
        $condition1 = explode('ORDER',$condition);
        $condition2 = explode('GROUP',$condition1[0]);
        $sql = "SELECT * FROM tbl_narasumber_new,tbl_kegiatan_natasumber WHERE `tbl_narasumber_new`.`id_narasumber` = `tbl_kegiatan_natasumber`.`id_narasumber` AND `tbl_kegiatan_natasumber`.`id_narasumber` = '$id' $condition2[0]";

        $query = $this->db->query($sql);
        return $query->result();
    }
    public function getDetailNarasumber($id)
    {
        /*$this->db->select('*');
        $this->db->join('tbl_kegiatan_natasumber', 'tbl_narasumber_new.id_narasumber = tbl_kegiatan_natasumber.id_narasumber');
        $this->db->where('tbl_kegiatan_natasumber.id_narasumber',$id);
        $this->db->order_by('tbl_kegiatan_natasumber.tanggal','DESC');
        $this->db->from('tbl_narasumber_new');*/
        $sql = "SELECT * FROM tbl_narasumber_new WHERE id_narasumber = '$id' ";

        $query = $this->db->query($sql);
        return $query->result();
    }
    public function update_data_narasumber()
    {
        $id_narasumber = $this->input->post('id_narasumber');
        $nama_narasumber = $this->input->post('nama_narasumber');
        $alamat_narasumber = $this->input->post('alamat_narasumber');
        $no_telp = $this->input->post('no_telp_narasumber');
        $tempat_lahir = $this->input->post('tempat_lahir_narasumber');
        $tgl_lahir = $this->input->post('tgl_lahir_narasumber');
        $nip = $this->input->post('nip_narasumber');
        $gelar = $this->input->post('gelar_narasumber');
        $golongan = $this->input->post('golongan_narasumber');
        $jenis_kelamin = $this->input->post('jenis_kelamin_narasumber');
        $pendidikan = $this->input->post('pendidikan_narasumber');
        $instansi = $this->input->post('instansi_narasumber');
        $satker = $this->input->post('satuan_kerja_narasumber');
        $status_pegawai = $this->input->post('status_pegawai');
        $provinsi = $this->input->post('provinsi');
        $kota = $this->input->post('kota_narasumber');
        $email = $this->input->post('email_narasumber');
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        $no_sertifikat = $this->input->post('no_sertifikat_narasumber');
        $sertifikat_tot_narasumber = $this->input->post('sertifikat_tot_narasumber');
        $hasil_evaluasi = $this->input->post('hasil_evaluasi');
        $status_aktif_narasumber = $this->input->post('status_aktif_narasumber');
        $tot_yang_diikuti = $this->input->post('tot_yang_diikuti');

        $jabatan_narasumber = $this->input->post('jabatan_narasumber');
        $web_narasumber = $this->input->post('web_narasumber');
        $pangkat_narasumber = $this->input->post('pangkat_narasumber');
        $kota_kantor = $this->input->post('kota_kantor');
        $alamat_kantor = $this->input->post('alamat_kantor');

        $deskripsi_pengalaman = $this->input->post('deskripsi_pengalaman');

        $lokasi = $this->input->post('lokasi_narasumber');

        $file_dokumen = array();
        $image_name = array();

        $cv = "cv";
        

        if($cv!=null){
            $config['upload_path'] = '././assets/upload/narasumber/';
            $config['allowed_types'] = 'png|jpg|pdf|docx|doc';
            $config['overwrite'] = true;


            $this->load->library('upload', $config);
            $this->upload->overwrite = true;
            $this->upload->initialize($config);


            if ( ! $this->upload->do_upload($cv))
            {
                $msg = $this->upload->display_errors('', '');
                $status = $msg;
                $status = "not_upload";

                //print_r($msg);
            }
            else
            {
                $data_cv = $this->upload->data();
                $status = "success";

                $upload_cv_narasumber = $data_cv['file_name'];

                $proses = $this->db->query("UPDATE tbl_narasumber_new SET 
                nama_narasumber = '$nama_narasumber',
                alamat_narasumber = '$alamat_narasumber',
                no_telp_narasumber = '$no_telp',
                tempat_lahir_narasumber = '$tempat_lahir',
                tanggal_lahir_narasumber = '$tgl_lahir',
                nip_narasumber = '$nip',
                gelar_narasumber = '$gelar',
                jeniskel_narasumber = '$jenis_kelamin',
                golongan_narasumber = '$golongan',
                pendidikan_narasumber = '$pendidikan',
                instansi_narsum = '$instansi',
                pangkat_narasumber = '$pangkat_narasumber',
                satker_narasumber = '$satker',
                status_pegawai_narasumber = '$status_pegawai',
                provinsi_narasumber = '$provinsi',
                kota_narasumber = '$kota',
                email_narasumber = '$email',
                
                hasil_evaluasi = '$hasil_evaluasi',
                status_aktif_narasumber = '$status_aktif_narasumber',
                tot_yang_diikuti = '$tot_yang_diikuti',
                web_narasumber = '$web_narasumber',
                jabatan_narasumber = '$jabatan_narasumber',
                alamat_kantor = '$alamat_kantor',
                kota_kantor = '$kota_kantor',
                deskripsi_pengalaman = '$deskripsi_pengalaman',
                upload_cv_narasumber = '$upload_cv_narasumber'
                WHERE
                id_narasumber = '$id_narasumber'
                ");
            }
        }else{

        }

        if($this->input->post('isImage')=="on"){
            
            $proses = $this->db->query("UPDATE tbl_narasumber_new SET 
            nama_narasumber = '$nama_narasumber',
            alamat_narasumber = '$alamat_narasumber',
            no_telp_narasumber = '$no_telp',
            tempat_lahir_narasumber = '$tempat_lahir',
            tanggal_lahir_narasumber = '$tgl_lahir',
            nip_narasumber = '$nip',
            gelar_narasumber = '$gelar',
            jeniskel_narasumber = '$jenis_kelamin',
            golongan_narasumber = '$golongan',
            pendidikan_narasumber = '$pendidikan',
            instansi_narsum = '$instansi',
            pangkat_narasumber = '$pangkat_narasumber',
            satker_narasumber = '$satker',
            status_pegawai_narasumber = '$status_pegawai',
            provinsi_narasumber = '$provinsi',
            kota_narasumber = '$kota',
            email_narasumber = '$email',
            username = '$username',
            hasil_evaluasi = '$hasil_evaluasi',
            status_aktif_narasumber = '$status_aktif_narasumber',
            tot_yang_diikuti = '$tot_yang_diikuti',
            web_narasumber = '$web_narasumber',
            jabatan_narasumber = '$jabatan_narasumber',
            alamat_kantor = '$alamat_kantor',
            kota_kantor = '$kota_kantor',
            deskripsi_pengalaman = '$deskripsi_pengalaman',
            lokasi = '$lokasi'
            WHERE
            id_narasumber = '$id_narasumber'
            ");

        }else{
            $config['upload_path'] = '././assets/upload/narasumber/';
            $config['allowed_types'] = 'png|jpg|pdf';
            $config['overwrite'] = true;


            $this->load->library('upload', $config);
            $this->upload->overwrite = true;
            $this->upload->initialize($config);


            if ( ! $this->upload->do_upload())
            {
                $msg = $this->upload->display_errors('', '');
                $status = $msg;
                $status = "not_upload";

                //print_r($msg);
            }
            else
            {
                $data_photo = $this->upload->data();
                $status = "success";

                $photo_name = $data_photo['file_name'];

                $proses = $this->db->query("UPDATE tbl_narasumber_new SET 
                nama_narasumber = '$nama_narasumber',
                alamat_narasumber = '$alamat_narasumber',
                no_telp_narasumber = '$no_telp',
                tempat_lahir_narasumber = '$tempat_lahir',
                tanggal_lahir_narasumber = '$tgl_lahir',
                nip_narasumber = '$nip',
                gelar_narasumber = '$gelar',
                jeniskel_narasumber = '$jenis_kelamin',
                golongan_narasumber = '$golongan',
                pendidikan_narasumber = '$pendidikan',
                instansi_narsum = '$instansi',
                pangkat_narasumber = '$pangkat_narasumber',
                satker_narasumber = '$satker',
                status_pegawai_narasumber = '$status_pegawai',
                provinsi_narasumber = '$provinsi',
                kota_narasumber = '$kota',
                email_narasumber = '$email',
                username = '$username',
                hasil_evaluasi = '$hasil_evaluasi',
                status_aktif_narasumber = '$status_aktif_narasumber',
                tot_yang_diikuti = '$tot_yang_diikuti',
                web_narasumber = '$web_narasumber',
                jabatan_narasumber = '$jabatan_narasumber',
                alamat_kantor = '$alamat_kantor',
                kota_kantor = '$kota_kantor',
                deskripsi_pengalaman = '$deskripsi_pengalaman',
                photo_narasumber = '$photo_name'
                WHERE
                id_narasumber = '$id_narasumber'
                ");

                //print_r($data_photo);
            }
        }

        

        /*
        $this->upload->initialize(array(
            "upload_path"   => '././assets/upload/narasumber/',
            "overwrite" => TRUE,
            "encrypt_name" => TRUE,
            "remove_spaces" => TRUE,
            "allowed_types" => 'gif|jpg|png|doc|docx|pdf'
        ));

        $this->upload->do_multi_upload("files");
        $upload_data = $this->upload->get_multi_upload_data();
        foreach ($upload_data as $key => $value) {
            $image_name[] = $value['file_name'];
        };

        print_r($upload_data);

       /* $file_dokumen = array(
            "photo_name" => $image_name[0],
            "upload_cv_narasumber" => $image_name[1],
        );

        if(isset($file_dokumen['photo_name'])){
            $photo_name = $file_dokumen['photo_name'];
        }else{
            $photo_name = " ";
        }

        if(isset($file_dokumen['upload_cv_narasumber'])){
            $upload_cv_narasumber = $file_dokumen['upload_cv_narasumber'];
        }else{
            $upload_cv_narasumber = " ";
        }

        */
        /**/

        
        
        
        

        
        return $status;
    }

    public function update_data_narasumber_lkpp($id)
    {
        $id_narasumber = $this->input->post('id_narasumber');
        $nama_narasumber = $this->input->post('nama_narasumber');
        $alamat_narasumber = $this->input->post('alamat_narasumber');
        $no_telp = $this->input->post('no_telp_narasumber');
        $tempat_lahir = $this->input->post('tempat_lahir_narasumber');
        $tgl_lahir = $this->input->post('tgl_lahir_narasumber');
        $nip = $this->input->post('nip_narasumber');
        $gelar = $this->input->post('gelar_narasumber');
        $golongan = $this->input->post('golongan_narasumber');
        $jenis_kelamin = $this->input->post('jenis_kelamin_narasumber');
        $pendidikan = $this->input->post('pendidikan_narasumber');
        $instansi = $this->input->post('instansi_narasumber');
        $satker = $this->input->post('satuan_kerja_narasumber');
        $status_pegawai = $this->input->post('status_pegawai');
        $provinsi = $this->input->post('provinsi');
        $kota = $this->input->post('kota_narasumber');
        $email = $this->input->post('email_narasumber');
        
        if($this->input->post('username'))
	    $username = $this->input->post('username');
        
        if($this->input->post('password'))
	    $password = base64_encode(md5($this->input->post('password')));
        $no_sertifikat = $this->input->post('no_sertifikat_narasumber');
        $sertifikat_tot_narasumber = $this->input->post('sertifikat_tot_narasumber');
        $hasil_evaluasi = $this->input->post('hasil_evaluasi');
        $status_aktif_narasumber = $this->input->post('status_aktif_narasumber');
        $tot_yang_diikuti = $this->input->post('tot_yang_diikuti');

        $jabatan_narasumber = $this->input->post('jabatan_narasumber');
        $web_narasumber = $this->input->post('web_narasumber');
        $pangkat_narasumber = $this->input->post('pangkat_narasumber');
        $kota_kantor = $this->input->post('kota_kantor');
        $alamat_kantor = $this->input->post('alamat_kantor');

        $deskripsi_pengalaman = $this->input->post('deskripsi_pengalaman');

         $password = base64_encode(md5($password));
         $config['upload_path'] = '././assets/upload/narasumber/';
        $config['allowed_types'] = 'png|jpg|pdf';
        $config['overwrite'] = true;


        $this->load->library('upload', $config);
        $this->upload->overwrite = true;
        $this->upload->initialize($config);


        if ( ! $this->upload->do_upload())
        {
            $msg = $this->upload->display_errors('', '');
            $status = $msg;
            $proses = $this->db->query("UPDATE tbl_narasumber_new SET 
            nama_narasumber = '$nama_narasumber',
            alamat_narasumber = '$alamat_narasumber',
            no_telp_narasumber = '$no_telp',
            tempat_lahir_narasumber = '$tempat_lahir',
            tanggal_lahir_narasumber = '$tgl_lahir',
            nip_narasumber = '$nip',
            gelar_narasumber = '$gelar',
            jeniskel_narasumber = '$jenis_kelamin',
            golongan_narasumber = '$golongan',
            pendidikan_narasumber = '$pendidikan',
            instansi_narsum = '$instansi',
            pangkat_narasumber = '$pangkat_narasumber',
            satker_narasumber = '$satker',
            status_pegawai_narasumber = '$status_pegawai',
            provinsi_narasumber = '$provinsi',
            kota_narasumber = '$kota',
            email_narasumber = '$email',
            username = '$username',
            password = '$password',
            hasil_evaluasi = '$hasil_evaluasi',
            status_aktif_narasumber = '$status_aktif_narasumber',
            tot_yang_diikuti = '$tot_yang_diikuti',
            web_narasumber = '$web_narasumber',
            jabatan_narasumber = '$jabatan_narasumber',
            alamat_kantor = '$alamat_kantor',
            kota_kantor = '$kota_kantor',
            deskripsi_pengalaman = '$deskripsi_pengalaman'
            WHERE
            id_narasumber = '$id_narasumber'
            ");

            if($proses){
                return "success";
            }else{
                return "failed";
            }
        }
        else
        {
            $data_photo = $this->upload->data();
            $status = "success";

            $photo_name = $data_photo['file_name'];

            $proses = $this->db->query("UPDATE tbl_narasumber_new SET 
            nama_narasumber = '$nama_narasumber',
            alamat_narasumber = '$alamat_narasumber',
            no_telp_narasumber = '$no_telp',
            tempat_lahir_narasumber = '$tempat_lahir',
            tanggal_lahir_narasumber = '$tgl_lahir',
            nip_narasumber = '$nip',
            gelar_narasumber = '$gelar',
            jeniskel_narasumber = '$jenis_kelamin',
            golongan_narasumber = '$golongan',
            pendidikan_narasumber = '$pendidikan',
            instansi_narsum = '$instansi',
            pangkat_narasumber = '$pangkat_narasumber',
            satker_narasumber = '$satker',
            status_pegawai_narasumber = '$status_pegawai',
            provinsi_narasumber = '$provinsi',
            kota_narasumber = '$kota',
            email_narasumber = '$email',
            username = '$username',
            password = '$password',
            hasil_evaluasi = '$hasil_evaluasi',
            status_aktif_narasumber = '$status_aktif_narasumber',
            tot_yang_diikuti = '$tot_yang_diikuti',
            web_narasumber = '$web_narasumber',
            jabatan_narasumber = '$jabatan_narasumber',
            alamat_kantor = '$alamat_kantor',
            kota_kantor = '$kota_kantor',
            deskripsi_pengalaman = '$deskripsi_pengalaman',
            photo_narasumber = '$photo_name'
            WHERE
            id_narasumber = '$id_narasumber'
            ");

            if($proses){
                return "success";
            }else{
                return "failed";
            }
        }

        
    }
    public function getPasswordLama($id)
    {
        $query = $this->db->query("SELECT password from tbl_narasumber_new WHERE id_narasumber = '$id' ");
        $id_key = $query->row();

        if($id_key!=null){
            return $id_key->password;
        }else{
            return 0;
        }
    }
    public function getPasswordLamaLkpp($id)
    {
        $query = $this->db->query("SELECT password from tbl_lkpp WHERE id_lkpp = '$id' ");
        $id_key = $query->row();

        if($id_key!=null){
            return $id_key->password;
        }else{
            return 0;
        }
    }


    /*public function record_count_klasemen()
    {
        $sql = "SELECT count(*)
From (SELECT (SELECT SUM(jumlah_jp) AS total_jp FROM tbl_kegiatan_natasumber WHERE tbl_kegiatan_natasumber.id_narasumber = tbl_narasumber_new.id_narasumber) AS total_jp FROM `tbl_narasumber_new`) as row";
    
        $id_key = $sql->row();

        if($id_key!=null){
            return $id_key->count(*);
        }else{
            return 0;
        }
    }*/
}