<?php
class Model_evaluasi extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }
    
    public function getEvaluasiPenyelenggara($id_lpp,$id_pelatihan)
    {
        $this->db->select('*');
        $this->db->from('evaluasi_penyelenggara');
        $this->db->where('id_lpp',$id_lpp);
        $this->db->where('id_pelatihan',$id_pelatihan);
        $this->db->order_by('id_evaluasi_penyelenggara','DESC');

        $query = $this->db->get();

        return $query->result();
    }
    public function getEvaluasiPenyelenggaraTanpaLpp($id_pelatihan)
    {
        $this->db->select('*');
        $this->db->join('tbl_lpp', 'tbl_lpp.id_lpp = evaluasi_penyelenggara .id_lpp');
        $this->db->from('evaluasi_penyelenggara');
        $this->db->where('id_pelatihan',$id_pelatihan);
        $this->db->order_by('id_evaluasi_penyelenggara','DESC');

        $query = $this->db->get();

        return $query->result();
    }
    public function checkEvaluasiPenyelenggara($id_lpp,$id_pelatihan)
    {
        $this->db->select('*');
        $this->db->from('evaluasi_penyelenggara');
        $this->db->where('id_lpp',$id_lpp);
        $this->db->where('id_pelatihan',$id_pelatihan);
        $this->db->order_by('id_evaluasi_penyelenggara','DESC');

        $query = $this->db->get();
        $rowcount = $query->num_rows();
        
        return $rowcount;
    }

    public function getEvaluasiNarasumber($id_pelatihan)
    {
        $this->db->select('*');
        $this->db->from('evaluasi_narasumber');
        $this->db->join('tbl_pelatihan', 'tbl_pelatihan.id_pelatihan = evaluasi_narasumber .id_pelatihan');
        $this->db->join('tbl_tema_pelatihan', 'tbl_tema_pelatihan.id_tema = tbl_pelatihan .id_tema');
        $this->db->join('tbl_narasumber_new', 'tbl_narasumber_new.id_narasumber = evaluasi_narasumber .id_narasumber');
        $this->db->where('evaluasi_narasumber.id_pelatihan',$id_pelatihan);
        $this->db->order_by('evaluasi_narasumber.id_evaluasi_narasumber','DESC');

        $query = $this->db->get();

        return $query->result();
    }
    public function checkEvaluasiNarasumber($id_narasumber,$id_pelatihan)
    {
        $this->db->select('*');
        $this->db->from('evaluasi_narasumber');
        $this->db->where('id_narasumber',$id_narasumber);
        $this->db->where('id_pelatihan',$id_pelatihan);
        $this->db->order_by('id_evaluasi_narasumber','DESC');

        $query = $this->db->get();
        $rowcount = $query->num_rows();
        
        return $rowcount;
    }

    public function proses_evaluasi_penyelenggara()
    {
        $lpp = $this->input->post('id_lpp');
        $pelatihan = $this->input->post('id_pelatihan');
        $kenyamanan = $this->input->post('kenyamanan');
        $kritik = $this->input->post('kritik');
        $perlengkapan = $this->input->post('perlengkapan');
        $ketersediaan = $this->input->post('ketersediaan');

        $config['upload_path'] = '././assets/upload/evaluasi_penyelenggara/';
        $config['allowed_types'] = 'gif|jpg|png|xls|xlsx|doc|docx|pdf';
        $config['max_size'] = '10000';
        $config['max_width']  = '1024';
        $config['max_height']  = '1024';
        $config['overwrite'] = true;
        

        $this->load->library('upload', $config);
        $this->upload->overwrite = true;
        $this->upload->initialize($config);


        if ( ! $this->upload->do_upload())
        {
            /*$msg = $this->upload->display_errors('', '');
            $status = $msg;*/
            $status = "not_upload";
            $file = " ";
        }
        else
        {
            $data = $this->upload->data();
            $file = $data['file_name'];
            $status = "success";
        }


        $proses = $this->db->query("INSERT INTO evaluasi_penyelenggara VALUES ('','$lpp','$pelatihan','$kenyamanan','$kritik','$perlengkapan','$ketersediaan','$file')");

        if($proses){
            return true;
        }else{
            return false;
        }
    }

    public function proses_evaluasi_narasumber()
    {
        $narasumber = $this->input->post('id_narasumber');
        $pelatihan = $this->input->post('id_pelatihan');
        $materi = $this->input->post('materi');
        $contoh = $this->input->post('contoh');
        $aktif = $this->input->post('aktif');
        $sistematis = $this->input->post('sistematis');
        $kritik = $this->input->post('kritik');

        $config['upload_path'] = '././assets/upload/evaluasi_narasumber/';
        $config['allowed_types'] = 'gif|jpg|png|xls|xlsx|doc|docx|pdf';
        $config['max_size'] = '10000';
        $config['max_width']  = '1024';
        $config['max_height']  = '1024';
        $config['overwrite'] = true;
        

        $this->load->library('upload', $config);
        $this->upload->overwrite = true;
        $this->upload->initialize($config);


        if ( ! $this->upload->do_upload())
        {
            /*$msg = $this->upload->display_errors('', '');
            $status = $msg;*/
            $status = "not_upload";
            $file = " ";
        }
        else
        {
            $data = $this->upload->data();
            $file = $data['file_name'];
            $status = "success";
        }


        $proses = $this->db->query("INSERT INTO evaluasi_narasumber VALUES ('','$narasumber','$pelatihan','$materi','$sistematis','$kritik','$contoh','$aktif','$file')");

        if($proses){
            return true;
        }else{
            return false;
        }
    }

}