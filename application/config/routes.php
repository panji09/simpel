<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

//home
$route['home'] = 'home/home';
// $route['home/(:any)'] = 'home/$1';

/*pages*/
$route['news'] = 'home/pages/news';
$route['news/(:any)'] = 'home/pages/news/$1';

$route['gallery'] = 'home/pages/gallery';
$route['gallery/(:any)'] = 'home/pages/gallery/$1';

$route['event'] = 'home/pages/event';
$route['event/(:any)'] = 'home/pages/event/$1';
$route['contact'] = 'home/pages/contact';
$route['about'] = 'home/pages/about';
$route['faq'] = 'home/pages/faq';
$route['regulatory_training'] = 'home/pages/regulatory_training';
$route['how_to'] = 'home/pages/how_to';
/*./pages*/

/*user*/
$route['institution'] = 'home/user/institution';
$route['instructor'] = 'home/user/instructor';
/*./user*/

/*training*/
$route['schedule'] = 'home/training/schedule';
$route['schedule/(:any)'] = 'home/training/schedule/$1';
/*./training*/

/*training*/
$route['training'] = 'home/training';
$route['training/(:any)'] = 'home/training/$1';
/*./training*/

/*dashboard*/
$route['dashboard'] = 'dashboard/dashboard';
$route['dashboard/(:any)'] = 'dashboard/$1';

$route['lpp'] = 'lpp/lpp';
$route['lpp/(:any)'] = 'lpp/$1';

$route['narasumber'] = 'narasumber/narasumber';
$route['narasumber/(:any)'] = 'narasumber/$1';

$route['peserta'] = 'peserta/participant';
$route['peserta/(:any)'] = 'peserta/$1';

$route['fasilitator'] = 'fasilitator/fasilitator';
$route['fasilitator/(:any)'] = 'fasilitator/$1';
/*./dashboard*/

$route['pengaduan'] = 'pengaduan/pengaduan';
$route['pengaduan/(:any)'] = 'pengaduan/$1';

$route['superadmin'] = 'superadmin/admin';
$route['admin'] = 'admin/admin';


$route['default_controller'] = "home/home";
$route['404_override'] = '';


/* End of file routes.php */
/* Location: ./application/config/routes.php */