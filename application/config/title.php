<?php
    $config['title'] = 'SIMPEL';
    $config['title_short'] = 'SIMPEL';
    $config['title_long'] = 'Sistem Informasi Manajemen Pelatihan';
    $config['title_footer'] = 'Dikembangkan oleh Direktorat Pelatihan Kompetensi Deputi PPSDM - Direktorat Pelatihan Kompetensi. &copy; Copyright '.(date('Y')==2015 ? date('Y') : '2015 - '.date('Y')).' LKPP';
?>