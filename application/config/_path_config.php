<?php
    $config['connect_api_url']	= 'http://simpel.ruangpanji.com/v2/connect/index.php/api';
    $config['connect_url']	= 'http://simpel.ruangpanji.com/v2/connect';
    $config['simpel_api_url']	= 'http://simpel.ruangpanji.com/v2/index.php/api';
    $config['lms_api_url']	= 'http://pustakakata.com/index.php/api';
    
    $config['connect_api_base_url'] = 'http://simpel.ruangpanji.com/v2/connect';
    $config['base_url'] = $this->config['base_url'];
    $config['plugin'] = $config['base_url']."/media/plugin";
    $config['upload'] = $config['base_url']."/media/upload";
    $config['upload_dir'] = getcwd()."/media/upload";
    
    //path home
    $config['home_css'] = $config['base_url']."/media/home/css";
    $config['home_js'] = $config['base_url']."/media/home/js";
    $config['home_img'] = $config['base_url']."/media/home/images";
    $config['home_font'] = $config['base_url']."/media/home/font";
    
    //path admin
    $config['admin_css'] = $config['base_url']."/media/admin/css";
    $config['admin_js'] = $config['base_url']."/media/admin/scripts";
    $config['admin_img'] = $config['base_url']."/media/admin/img";
    
    //path login
    $config['login_css'] = $config['base_url']."/media/login/css";
    $config['login_js'] = $config['base_url']."/media/login/scripts";
    $config['login_img'] = $config['base_url']."/media/login/img";
    
?>
