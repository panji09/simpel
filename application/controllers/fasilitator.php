<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Fasilitator extends CI_Controller {

	private $id = "";
    private $conditionQuery="";

	public function __construct()
    {
        parent::__construct();
        $this->load->model('model_pelatihan');
        $this->load->model('model_lpp');
        $this->load->model('model_narasumber');
        $this->load->model('model_request_pelatihan');
        $this->load->model('model_rencana_kegiatan');
        $this->load->model('model_berita');
        $this->load->model('model_peserta');
        $this->load->model('model_evaluasi');
        $this->load->library("pagination");
        $this->load->helper(array('form', 'url'));
        $this->load->library('upload');
        $this->load->helper('tgl_indonesia');
        $this->load->library('email');
        $this->load->library('csvreader');
        $this->load->library('datatables');
        $id = $this->session->userdata('id');

        $this->load->model('app_model');
        if( ($this->session->userdata('logged_in')!=1) || ($this->session->userdata('role')!='fasilitator') )
        {
        	$this->app_model->logout();
            redirect('admin/login');
        }
        
        session_start();
        
        $_SESSION['KCFINDER'] = array(
	    'disabled' => false,
	    'access' => array(

	      'files' => array(
		  'upload' => false,
		  'delete' => true,
		  'copy'   => true,
		  'move'   => true,
		  'rename' => true
	      ),

	      'dirs' => array(
		  'create' => true,
		  'delete' => true,
		  'rename' => true
	      )
	  )
	);
    }
    public function index()
    {
    	$data['username'] = $this->session->userdata('username');
		$data['role'] = $this->session->userdata('role');
		$data['id_user'] = $this->session->userdata('id');
		$data['countNotifNarasumber'] = $this->model_pelatihan->countNotifNarasumberFasilitator($this->session->userdata('id'));
        $data['total_request'] = $this->model_pelatihan->newRequestFasilitator($this->session->userdata('id'));

        /*echo "<br><br><br><br><br><br><br> adsadasdsad ".$this->session->userdata('id');*/

        $data['notifikasi_narasumber'] = $this->model_pelatihan->getNotifNarasumberFasilitator($this->session->userdata('id'));
        
		$data['jumlah_pelatihan'] = $this->model_pelatihan->countPelatihanByIdPelatihanIdFasilitator($this->session->userdata('id'));
		$data['jumlah_arsip_pelatihan'] = $this->model_pelatihan->countArsipPelatihanByIdPelatihanIdFasilitator($this->session->userdata('id'));

        $data['notifAgendaBaru'] = $this->model_rencana_kegiatan->countAllAgenda('0');
        $data['notifScheduleBaru'] = $this->model_pelatihan->countScheduleToBeApproved();
        $data['notifMintaFasilitator'] = $this->model_pelatihan->countFasilitatoriPelatihan();
        $data['username'] = $this->session->userdata('username');
        $data['role'] = $this->session->userdata('role');

        $data['notifAllLPP'] = $this->model_lpp->countAllLpp();
        $data['notifAllNarasumber'] = $this->model_narasumber->countAllNarasumber();
        $data['notifLPPbaru'] = $this->model_lpp->countLPPBaru();
        $data['notifAgendaBaru'] = $this->model_rencana_kegiatan->countAllAgenda('0');
        $data['notifScheduleBaru'] = $this->model_pelatihan->countScheduleToBeApproved();
        $data['notifMintaFasilitator'] = $this->model_pelatihan->countFasilitatoriPelatihan();
        

        $limit = $this->input->post('limit');

        if($this->uri->segment(3)!=null){
            $data['no_from_url'] = $this->uri->segment(3);
        }else{
            $data['no_from_url'] = 0;
        }
        $this->conditionQuery = $this->model_narasumber->getCondition();
        $data['conditionQuery'] = $this->conditionQuery;
        
        $data['narasumber'] = $this->model_narasumber->getNarasumber();
        $data['klasemen_narasumber'] = $this->model_narasumber->getKlasemenNarasumber(0, 1000);

        $data['tahun'] = date('Y');

		$this->load->view('layout_admin/header',$data);
		$this->load->view('fasilitator/index',$data);
		$this->load->view('layout_admin/footer');
    }
    
    public function file_explorer(){
	$data['username'] = $this->session->userdata('username');
	$data['role'] = $this->session->userdata('role');
	$data['id_user'] = $this->session->userdata('id');
	$id = $this->session->userdata('id');

	$data['username'] = $this->session->userdata('username');
	$data['role'] = $this->session->userdata('role');
	
	$data['dataFoto'] = $this->model_pelatihan->get_all_foto();
	$data['notifAgendaBaru'] = $this->model_rencana_kegiatan->countAllAgenda('0');
	$data['notifScheduleBaru'] = $this->model_pelatihan->countScheduleToBeApproved();
	$data['notifMintaFasilitator'] = $this->model_pelatihan->countFasilitatoriPelatihan();
	$data['countNotifNarasumber'] = $this->model_pelatihan->countNotifNarasumberFasilitator($this->session->userdata('id'));
	$data['total_request'] = $this->model_pelatihan->newRequestFasilitator($this->session->userdata('id'));
	
	$this->load->view('layout_admin/header',$data);
	$this->load->view('lkpp/file_explorer');
	$this->load->view('layout_admin/footer');

    }
    
    public function detail_kegiatan_narasumber($id)
    {
        $data['username'] = $this->session->userdata('username');
        $data['role'] = $this->session->userdata('role');
        $data['id_user'] = $this->session->userdata('id');
        $data['countNotifNarasumber'] = $this->model_pelatihan->countNotifNarasumberFasilitator($this->session->userdata('id'));
        $data['total_request'] = $this->model_pelatihan->newRequestFasilitator($this->session->userdata('id'));

        /*echo "<br><br><br><br><br><br><br> adsadasdsad ".$this->session->userdata('id');*/

        $data['notifikasi_narasumber'] = $this->model_pelatihan->getNotifNarasumberFasilitator($this->session->userdata('id'));
        
        $data['jumlah_pelatihan'] = $this->model_pelatihan->countPelatihanByIdPelatihanIdFasilitator($this->session->userdata('id'));
        $data['jumlah_arsip_pelatihan'] = $this->model_pelatihan->countArsipPelatihanByIdPelatihanIdFasilitator($this->session->userdata('id'));
        //$id = $this->input->post('id_narasumber');
        $data['id_narasumber'] = $id;

        $condition = $this->input->post('condition');

        $data['dari_tanggal'] = $this->input->post('dari_tanggal');
        $data['sampai_tanggal'] = $this->input->post('sampai_tanggal');

        $dari_tanggal = $data['dari_tanggal'];
        $sampai_tanggal = $data['sampai_tanggal'];

        if($dari_tanggal!=null){
            $dari_tanggal = $this->input->post('dari_tanggal');
            $condition .= " AND tbl_kegiatan_natasumber.tanggal >= '$dari_tanggal' ";
        }
        if($sampai_tanggal!=null){
            $sampai_tanggal = $this->input->post('sampai_tanggal');
            $condition .= " AND tbl_kegiatan_natasumber.tanggal <= '$sampai_tanggal' ";
        }

        
        $data['detail_kegiatan_narasumber'] = $this->model_narasumber->getDetailKegiatanNarasumber($id,$condition);
        $data['detail_narasumber'] = $this->model_narasumber->getDetailNarasumber($id);
        $data['conditionQuery'] = $condition;

        $this->load->view('layout_admin/header',$data);
        $this->load->view('fasilitator/detail_kegiatan_narasumber',$data);
        $this->load->view('layout_admin/footer');
    }
    public function findNarasumber($page = 0)
    {
        $data['username'] = $this->session->userdata('username');
        $data['role'] = $this->session->userdata('role');
        $data['id_user'] = $this->session->userdata('id');
        $data['countNotifNarasumber'] = $this->model_pelatihan->countNotifNarasumberFasilitator($this->session->userdata('id'));
        $data['total_request'] = $this->model_pelatihan->newRequestFasilitator($this->session->userdata('id'));

         $data['total_request'] = $this->model_pelatihan->newRequestFasilitator($this->session->userdata('id'));

        /*echo "<br><br><br><br><br><br><br> adsadasdsad ".$this->session->userdata('id');*/

        $data['notifikasi_narasumber'] = $this->model_pelatihan->getNotifNarasumberFasilitator($this->session->userdata('id'));
        
        $data['jumlah_pelatihan'] = $this->model_pelatihan->countPelatihanByIdPelatihanIdFasilitator($this->session->userdata('id'));
        $data['jumlah_arsip_pelatihan'] = $this->model_pelatihan->countArsipPelatihanByIdPelatihanIdFasilitator($this->session->userdata('id'));

        $data['notifAllLPP'] = $this->model_lpp->countAllLpp();
        $data['notifAllNarasumber'] = $this->model_narasumber->countAllNarasumber();
        $data['notifLPPbaru'] = $this->model_lpp->countLPPBaru();
        $data['notifAgendaBaru'] = $this->model_rencana_kegiatan->countAllAgenda('0');
        $data['notifScheduleBaru'] = $this->model_pelatihan->countScheduleToBeApproved();
        $data['notifMintaFasilitator'] = $this->model_pelatihan->countFasilitatoriPelatihan();

        
        //$data['klasemen_narasumber'] = $this->model_narasumber->findNarasumber();
        $data['narasumber'] = $this->model_narasumber->getNarasumber();

        $data['narasumber'] = $this->input->post('id_narasumber');
        $data['dari_tanggal'] = $this->input->post('dari_tanggal');
        $data['sampai_tanggal'] = $this->input->post('sampai_tanggal');
        $data['catatan'] = $this->input->post('catatan');
        $data['instansi'] = $this->input->post('instansi');
        $data['lokasi'] = $this->input->post('lokasi');
        $data['JP'] = $this->input->post('JP');

                $config = array();
        $config["base_url"] = base_url() . "lkpp/findNarasumber/limit";
        //HITUNG JUMLAH ROW DI TABLE BERITA//
        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;

        if($this->uri->segment(4)!=null){
            $data['no_from_url'] = $this->uri->segment(4);
        }else{
            $data['no_from_url'] = 0;
        }

        $data['narasumber'] = $this->model_narasumber->getNarasumber();
        $data['klasemen_narasumber'] = $this->model_narasumber->findNarasumber(10, $page);

        $this->conditionQuery = $this->model_narasumber->getCondition();
        $data['conditionQuery'] = $this->conditionQuery;
        //echo $this->condition."shuloo";

        $config["total_rows"] = $this->model_narasumber->getRowFromfindNarasumber();
        $config["per_page"] = 10;
        $config["uri_segment"] = 4;
        $this->pagination->initialize($config);
        $data["links"] = $this->pagination->create_links();

        $data['tahun'] = date('Y');

        $this->load->view('layout_admin/header',$data);
        $this->load->view('fasilitator/index',$data);
        $this->load->view('layout_admin/footer');
    }
    public function setAktif($id,$id_pelatihan)
    {
    	$proses = $this->model_pelatihan->setAktifFasilitator($id);
    	redirect('fasilitator/input_narasumber/'.$id_pelatihan);
    }

    public function pelatihan($id)
    {
    	$data['username'] = $this->session->userdata('username');
		$data['role'] = $this->session->userdata('role');
		$data['id_user'] = $this->session->userdata('id');
		$data['countNotifNarasumber'] = $this->model_pelatihan->countNotifNarasumberFasilitator($this->session->userdata('id'));

		$data['id'] = $id;

		$data['pelatihan'] = $this->model_pelatihan->getPelatihanByIdPelatihanIdFasilitator($id);
        $data['total_request'] = $this->model_pelatihan->newRequestFasilitator($this->session->userdata('id'));

        $data['row_narasumber'] = $this->model_pelatihan->countRowTable('pelatihan_narasumber','id_pelatihan',$id);
        $data['row_peserta'] = $this->model_pelatihan->countRowTable('tbl_peserta','id_pelatihan',$id);
        $data['row_eval_penyelenggara'] = $this->model_pelatihan->countRowTable('evaluasi_penyelenggara','id_pelatihan',$id);
        $data['row_eval_narasumber'] = $this->model_pelatihan->countRowTable('evaluasi_narasumber','id_pelatihan',$id);

		$this->load->view('layout_admin/header',$data);
		$this->load->view('fasilitator/pelatihan');
		$this->load->view('layout_admin/footer');
    }

    public function arsip_pelatihan($id)
    {
    	$data['username'] = $this->session->userdata('username');
		$data['role'] = $this->session->userdata('role');
		$data['id_user'] = $this->session->userdata('id');
		$data['countNotifNarasumber'] = $this->model_pelatihan->countNotifNarasumberFasilitator($this->session->userdata('id'));


		$data['pelatihan'] = $this->model_pelatihan->getPelatihanByIdPelatihanIdFasilitatorArsip($id);
        $data['total_request'] = $this->model_pelatihan->newRequestFasilitator($this->session->userdata('id'));

		$this->load->view('layout_admin/header',$data);
		$this->load->view('fasilitator/arsip_pelatihan');
		$this->load->view('layout_admin/footer');
    }
    
    function ajax_pelatihan_narasumber($tahun=null, $periode=1){
	
	if($periode == 1){
	    $this->datatables->select('
	      null as no,

	      a.id_narasumber as id_tot,
	      b.nama_narasumber as nama,
	      a.instansi as instansi,
	      sum(if(month(a.tanggal) = 1, jumlah_jp,0)) as bln1,

	      SUM(IF(month(a.tanggal) = 1 and WEEK(a.tanggal, 5) - WEEK(DATE_SUB(a.tanggal, INTERVAL DAYOFMONTH(a.tanggal) - 1 DAY), 5) + 1 = 1, jumlah_jp, 0)) as bln1_1,
	      SUM(IF(month(a.tanggal) = 1 and WEEK(a.tanggal, 5) - WEEK(DATE_SUB(a.tanggal, INTERVAL DAYOFMONTH(a.tanggal) - 1 DAY), 5) + 1 = 2, jumlah_jp, 0)) as bln1_2,
	      SUM(IF(month(a.tanggal) = 1 and WEEK(a.tanggal, 5) - WEEK(DATE_SUB(a.tanggal, INTERVAL DAYOFMONTH(a.tanggal) - 1 DAY), 5) + 1 = 3, jumlah_jp, 0)) as bln1_3,
	      SUM(IF(month(a.tanggal) = 1 and WEEK(a.tanggal, 5) - WEEK(DATE_SUB(a.tanggal, INTERVAL DAYOFMONTH(a.tanggal) - 1 DAY), 5) + 1 = 4, jumlah_jp, 0)) as bln1_4,
	      SUM(IF(month(a.tanggal) = 1 and WEEK(a.tanggal, 5) - WEEK(DATE_SUB(a.tanggal, INTERVAL DAYOFMONTH(a.tanggal) - 1 DAY), 5) + 1 = 5, jumlah_jp, 0)) as bln1_5,

	      sum(if(month(a.tanggal) = 2, jumlah_jp,0)) as bln2,

	      SUM(IF(month(a.tanggal) = 2 and WEEK(a.tanggal, 5) - WEEK(DATE_SUB(a.tanggal, INTERVAL DAYOFMONTH(a.tanggal) - 1 DAY), 5) + 1 = 1, jumlah_jp, 0)) as bln2_1,
	      SUM(IF(month(a.tanggal) = 2 and WEEK(a.tanggal, 5) - WEEK(DATE_SUB(a.tanggal, INTERVAL DAYOFMONTH(a.tanggal) - 1 DAY), 5) + 1 = 2, jumlah_jp, 0)) as bln2_2,
	      SUM(IF(month(a.tanggal) = 2 and WEEK(a.tanggal, 5) - WEEK(DATE_SUB(a.tanggal, INTERVAL DAYOFMONTH(a.tanggal) - 1 DAY), 5) + 1 = 3, jumlah_jp, 0)) as bln2_3,
	      SUM(IF(month(a.tanggal) = 2 and WEEK(a.tanggal, 5) - WEEK(DATE_SUB(a.tanggal, INTERVAL DAYOFMONTH(a.tanggal) - 1 DAY), 5) + 1 = 4, jumlah_jp, 0)) as bln2_4,
	      SUM(IF(month(a.tanggal) = 2 and WEEK(a.tanggal, 5) - WEEK(DATE_SUB(a.tanggal, INTERVAL DAYOFMONTH(a.tanggal) - 1 DAY), 5) + 1 = 5, jumlah_jp, 0)) as bln2_5,

	      sum(if(month(a.tanggal) = 3, jumlah_jp,0)) as bln3,

	      SUM(IF(month(a.tanggal) = 3 and WEEK(a.tanggal, 5) - WEEK(DATE_SUB(a.tanggal, INTERVAL DAYOFMONTH(a.tanggal) - 1 DAY), 5) + 1 = 1, jumlah_jp, 0)) as bln3_1,
	      SUM(IF(month(a.tanggal) = 3 and WEEK(a.tanggal, 5) - WEEK(DATE_SUB(a.tanggal, INTERVAL DAYOFMONTH(a.tanggal) - 1 DAY), 5) + 1 = 2, jumlah_jp, 0)) as bln3_2,
	      SUM(IF(month(a.tanggal) = 3 and WEEK(a.tanggal, 5) - WEEK(DATE_SUB(a.tanggal, INTERVAL DAYOFMONTH(a.tanggal) - 1 DAY), 5) + 1 = 3, jumlah_jp, 0)) as bln3_3,
	      SUM(IF(month(a.tanggal) = 3 and WEEK(a.tanggal, 5) - WEEK(DATE_SUB(a.tanggal, INTERVAL DAYOFMONTH(a.tanggal) - 1 DAY), 5) + 1 = 4, jumlah_jp, 0)) as bln3_4,
	      SUM(IF(month(a.tanggal) = 3 and WEEK(a.tanggal, 5) - WEEK(DATE_SUB(a.tanggal, INTERVAL DAYOFMONTH(a.tanggal) - 1 DAY), 5) + 1 = 5, jumlah_jp, 0)) as bln3_5,

	      sum(if(month(a.tanggal) = 4, jumlah_jp,0)) as bln4,

	      SUM(IF(month(a.tanggal) = 4 and WEEK(a.tanggal, 5) - WEEK(DATE_SUB(a.tanggal, INTERVAL DAYOFMONTH(a.tanggal) - 1 DAY), 5) + 1 = 1, jumlah_jp, 0)) as bln4_1,
	      SUM(IF(month(a.tanggal) = 4 and WEEK(a.tanggal, 5) - WEEK(DATE_SUB(a.tanggal, INTERVAL DAYOFMONTH(a.tanggal) - 1 DAY), 5) + 1 = 2, jumlah_jp, 0)) as bln4_2,
	      SUM(IF(month(a.tanggal) = 4 and WEEK(a.tanggal, 5) - WEEK(DATE_SUB(a.tanggal, INTERVAL DAYOFMONTH(a.tanggal) - 1 DAY), 5) + 1 = 3, jumlah_jp, 0)) as bln4_3,
	      SUM(IF(month(a.tanggal) = 4 and WEEK(a.tanggal, 5) - WEEK(DATE_SUB(a.tanggal, INTERVAL DAYOFMONTH(a.tanggal) - 1 DAY), 5) + 1 = 4, jumlah_jp, 0)) as bln4_4,
	      SUM(IF(month(a.tanggal) = 4 and WEEK(a.tanggal, 5) - WEEK(DATE_SUB(a.tanggal, INTERVAL DAYOFMONTH(a.tanggal) - 1 DAY), 5) + 1 = 5, jumlah_jp, 0)) as bln4_5,

	      sum(if(month(a.tanggal) = 5, jumlah_jp,0)) as bln5,

	      SUM(IF(month(a.tanggal) = 5 and WEEK(a.tanggal, 5) - WEEK(DATE_SUB(a.tanggal, INTERVAL DAYOFMONTH(a.tanggal) - 1 DAY), 5) + 1 = 1, jumlah_jp, 0)) as bln5_1,
	      SUM(IF(month(a.tanggal) = 5 and WEEK(a.tanggal, 5) - WEEK(DATE_SUB(a.tanggal, INTERVAL DAYOFMONTH(a.tanggal) - 1 DAY), 5) + 1 = 2, jumlah_jp, 0)) as bln5_2,
	      SUM(IF(month(a.tanggal) = 5 and WEEK(a.tanggal, 5) - WEEK(DATE_SUB(a.tanggal, INTERVAL DAYOFMONTH(a.tanggal) - 1 DAY), 5) + 1 = 3, jumlah_jp, 0)) as bln5_3,
	      SUM(IF(month(a.tanggal) = 5 and WEEK(a.tanggal, 5) - WEEK(DATE_SUB(a.tanggal, INTERVAL DAYOFMONTH(a.tanggal) - 1 DAY), 5) + 1 = 4, jumlah_jp, 0)) as bln5_4,
	      SUM(IF(month(a.tanggal) = 5 and WEEK(a.tanggal, 5) - WEEK(DATE_SUB(a.tanggal, INTERVAL DAYOFMONTH(a.tanggal) - 1 DAY), 5) + 1 = 5, jumlah_jp, 0)) as bln5_5,

	      sum(if(month(a.tanggal) = 6, jumlah_jp,0)) as bln6,

	      SUM(IF(month(a.tanggal) = 6 and WEEK(a.tanggal, 5) - WEEK(DATE_SUB(a.tanggal, INTERVAL DAYOFMONTH(a.tanggal) - 1 DAY), 5) + 1 = 1, jumlah_jp, 0)) as bln6_1,
	      SUM(IF(month(a.tanggal) = 6 and WEEK(a.tanggal, 5) - WEEK(DATE_SUB(a.tanggal, INTERVAL DAYOFMONTH(a.tanggal) - 1 DAY), 5) + 1 = 2, jumlah_jp, 0)) as bln6_2,
	      SUM(IF(month(a.tanggal) = 6 and WEEK(a.tanggal, 5) - WEEK(DATE_SUB(a.tanggal, INTERVAL DAYOFMONTH(a.tanggal) - 1 DAY), 5) + 1 = 3, jumlah_jp, 0)) as bln6_3,
	      SUM(IF(month(a.tanggal) = 6 and WEEK(a.tanggal, 5) - WEEK(DATE_SUB(a.tanggal, INTERVAL DAYOFMONTH(a.tanggal) - 1 DAY), 5) + 1 = 4, jumlah_jp, 0)) as bln6_4,
	      SUM(IF(month(a.tanggal) = 6 and WEEK(a.tanggal, 5) - WEEK(DATE_SUB(a.tanggal, INTERVAL DAYOFMONTH(a.tanggal) - 1 DAY), 5) + 1 = 5, jumlah_jp, 0)) as bln6_5,

	      sum(jumlah_jp) as total
	    ',false);
	}elseif($periode == 2){
	    $this->datatables->select('
	      null as no,

	      a.id_narasumber as id_tot,
	      b.nama_narasumber as nama,
	      a.instansi as instansi,
	      sum(if(month(a.tanggal) = 7, jumlah_jp,0)) as bln7,

	      SUM(IF(month(a.tanggal) = 7 and WEEK(a.tanggal, 5) - WEEK(DATE_SUB(a.tanggal, INTERVAL DAYOFMONTH(a.tanggal) - 1 DAY), 5) + 1 = 1, jumlah_jp, 0)) as bln7_1,
	      SUM(IF(month(a.tanggal) = 7 and WEEK(a.tanggal, 5) - WEEK(DATE_SUB(a.tanggal, INTERVAL DAYOFMONTH(a.tanggal) - 1 DAY), 5) + 1 = 2, jumlah_jp, 0)) as bln7_2,
	      SUM(IF(month(a.tanggal) = 7 and WEEK(a.tanggal, 5) - WEEK(DATE_SUB(a.tanggal, INTERVAL DAYOFMONTH(a.tanggal) - 1 DAY), 5) + 1 = 3, jumlah_jp, 0)) as bln7_3,
	      SUM(IF(month(a.tanggal) = 7 and WEEK(a.tanggal, 5) - WEEK(DATE_SUB(a.tanggal, INTERVAL DAYOFMONTH(a.tanggal) - 1 DAY), 5) + 1 = 4, jumlah_jp, 0)) as bln7_4,
	      SUM(IF(month(a.tanggal) = 7 and WEEK(a.tanggal, 5) - WEEK(DATE_SUB(a.tanggal, INTERVAL DAYOFMONTH(a.tanggal) - 1 DAY), 5) + 1 = 5, jumlah_jp, 0)) as bln7_5,

	      sum(if(month(a.tanggal) = 8, jumlah_jp,0)) as bln8,

	      SUM(IF(month(a.tanggal) = 8 and WEEK(a.tanggal, 5) - WEEK(DATE_SUB(a.tanggal, INTERVAL DAYOFMONTH(a.tanggal) - 1 DAY), 5) + 1 = 1, jumlah_jp, 0)) as bln8_1,
	      SUM(IF(month(a.tanggal) = 8 and WEEK(a.tanggal, 5) - WEEK(DATE_SUB(a.tanggal, INTERVAL DAYOFMONTH(a.tanggal) - 1 DAY), 5) + 1 = 2, jumlah_jp, 0)) as bln8_2,
	      SUM(IF(month(a.tanggal) = 8 and WEEK(a.tanggal, 5) - WEEK(DATE_SUB(a.tanggal, INTERVAL DAYOFMONTH(a.tanggal) - 1 DAY), 5) + 1 = 3, jumlah_jp, 0)) as bln8_3,
	      SUM(IF(month(a.tanggal) = 8 and WEEK(a.tanggal, 5) - WEEK(DATE_SUB(a.tanggal, INTERVAL DAYOFMONTH(a.tanggal) - 1 DAY), 5) + 1 = 4, jumlah_jp, 0)) as bln8_4,
	      SUM(IF(month(a.tanggal) = 8 and WEEK(a.tanggal, 5) - WEEK(DATE_SUB(a.tanggal, INTERVAL DAYOFMONTH(a.tanggal) - 1 DAY), 5) + 1 = 5, jumlah_jp, 0)) as bln8_5,

	      sum(if(month(a.tanggal) = 9, jumlah_jp,0)) as bln9,

	      SUM(IF(month(a.tanggal) = 9 and WEEK(a.tanggal, 5) - WEEK(DATE_SUB(a.tanggal, INTERVAL DAYOFMONTH(a.tanggal) - 1 DAY), 5) + 1 = 1, jumlah_jp, 0)) as bln9_1,
	      SUM(IF(month(a.tanggal) = 9 and WEEK(a.tanggal, 5) - WEEK(DATE_SUB(a.tanggal, INTERVAL DAYOFMONTH(a.tanggal) - 1 DAY), 5) + 1 = 2, jumlah_jp, 0)) as bln9_2,
	      SUM(IF(month(a.tanggal) = 9 and WEEK(a.tanggal, 5) - WEEK(DATE_SUB(a.tanggal, INTERVAL DAYOFMONTH(a.tanggal) - 1 DAY), 5) + 1 = 3, jumlah_jp, 0)) as bln9_3,
	      SUM(IF(month(a.tanggal) = 9 and WEEK(a.tanggal, 5) - WEEK(DATE_SUB(a.tanggal, INTERVAL DAYOFMONTH(a.tanggal) - 1 DAY), 5) + 1 = 4, jumlah_jp, 0)) as bln9_4,
	      SUM(IF(month(a.tanggal) = 9 and WEEK(a.tanggal, 5) - WEEK(DATE_SUB(a.tanggal, INTERVAL DAYOFMONTH(a.tanggal) - 1 DAY), 5) + 1 = 5, jumlah_jp, 0)) as bln9_5,

	      sum(if(month(a.tanggal) = 10, jumlah_jp,0)) as bln10,

	      SUM(IF(month(a.tanggal) = 10 and WEEK(a.tanggal, 5) - WEEK(DATE_SUB(a.tanggal, INTERVAL DAYOFMONTH(a.tanggal) - 1 DAY), 5) + 1 = 1, jumlah_jp, 0)) as bln10_1,
	      SUM(IF(month(a.tanggal) = 10 and WEEK(a.tanggal, 5) - WEEK(DATE_SUB(a.tanggal, INTERVAL DAYOFMONTH(a.tanggal) - 1 DAY), 5) + 1 = 2, jumlah_jp, 0)) as bln10_2,
	      SUM(IF(month(a.tanggal) = 10 and WEEK(a.tanggal, 5) - WEEK(DATE_SUB(a.tanggal, INTERVAL DAYOFMONTH(a.tanggal) - 1 DAY), 5) + 1 = 3, jumlah_jp, 0)) as bln10_3,
	      SUM(IF(month(a.tanggal) = 10 and WEEK(a.tanggal, 5) - WEEK(DATE_SUB(a.tanggal, INTERVAL DAYOFMONTH(a.tanggal) - 1 DAY), 5) + 1 = 4, jumlah_jp, 0)) as bln10_4,
	      SUM(IF(month(a.tanggal) = 10 and WEEK(a.tanggal, 5) - WEEK(DATE_SUB(a.tanggal, INTERVAL DAYOFMONTH(a.tanggal) - 1 DAY), 5) + 1 = 5, jumlah_jp, 0)) as bln10_5,

	      sum(if(month(a.tanggal) = 11, jumlah_jp,0)) as bln11,

	      SUM(IF(month(a.tanggal) = 11 and WEEK(a.tanggal, 5) - WEEK(DATE_SUB(a.tanggal, INTERVAL DAYOFMONTH(a.tanggal) - 1 DAY), 5) + 1 = 1, jumlah_jp, 0)) as bln11_1,
	      SUM(IF(month(a.tanggal) = 11 and WEEK(a.tanggal, 5) - WEEK(DATE_SUB(a.tanggal, INTERVAL DAYOFMONTH(a.tanggal) - 1 DAY), 5) + 1 = 2, jumlah_jp, 0)) as bln11_2,
	      SUM(IF(month(a.tanggal) = 11 and WEEK(a.tanggal, 5) - WEEK(DATE_SUB(a.tanggal, INTERVAL DAYOFMONTH(a.tanggal) - 1 DAY), 5) + 1 = 3, jumlah_jp, 0)) as bln11_3,
	      SUM(IF(month(a.tanggal) = 11 and WEEK(a.tanggal, 5) - WEEK(DATE_SUB(a.tanggal, INTERVAL DAYOFMONTH(a.tanggal) - 1 DAY), 5) + 1 = 4, jumlah_jp, 0)) as bln11_4,
	      SUM(IF(month(a.tanggal) = 11 and WEEK(a.tanggal, 5) - WEEK(DATE_SUB(a.tanggal, INTERVAL DAYOFMONTH(a.tanggal) - 1 DAY), 5) + 1 = 5, jumlah_jp, 0)) as bln11_5,

	      sum(if(month(a.tanggal) = 12, jumlah_jp,0)) as bln12,

	      SUM(IF(month(a.tanggal) = 12 and WEEK(a.tanggal, 5) - WEEK(DATE_SUB(a.tanggal, INTERVAL DAYOFMONTH(a.tanggal) - 1 DAY), 5) + 1 = 1, jumlah_jp, 0)) as bln12_1,
	      SUM(IF(month(a.tanggal) = 12 and WEEK(a.tanggal, 5) - WEEK(DATE_SUB(a.tanggal, INTERVAL DAYOFMONTH(a.tanggal) - 1 DAY), 5) + 1 = 2, jumlah_jp, 0)) as bln12_2,
	      SUM(IF(month(a.tanggal) = 12 and WEEK(a.tanggal, 5) - WEEK(DATE_SUB(a.tanggal, INTERVAL DAYOFMONTH(a.tanggal) - 1 DAY), 5) + 1 = 3, jumlah_jp, 0)) as bln12_3,
	      SUM(IF(month(a.tanggal) = 12 and WEEK(a.tanggal, 5) - WEEK(DATE_SUB(a.tanggal, INTERVAL DAYOFMONTH(a.tanggal) - 1 DAY), 5) + 1 = 4, jumlah_jp, 0)) as bln12_4,
	      SUM(IF(month(a.tanggal) = 12 and WEEK(a.tanggal, 5) - WEEK(DATE_SUB(a.tanggal, INTERVAL DAYOFMONTH(a.tanggal) - 1 DAY), 5) + 1 = 5, jumlah_jp, 0)) as bln12_5,
	      sum(jumlah_jp) as total
	    ',false);
	}
	
	
	$this->datatables->from('tbl_kegiatan_natasumber as a,  tbl_narasumber_new as b');
	$this->datatables->where('a.id_narasumber = b.id_narasumber');
	$this->datatables->where('year(a.tanggal)', $tahun);
	
	
	$this->datatables->group_by('a.id_narasumber');
	
	$this->datatables->edit_column('bln1','<strong>$1</strong>','bln1');
	$this->datatables->edit_column('bln2','<strong>$1</strong>','bln2');
	$this->datatables->edit_column('bln3','<strong>$1</strong>','bln3');
	$this->datatables->edit_column('bln4','<strong>$1</strong>','bln4');
	$this->datatables->edit_column('bln5','<strong>$1</strong>','bln5');
	$this->datatables->edit_column('bln6','<strong>$1</strong>','bln6');
	$this->datatables->edit_column('bln7','<strong>$1</strong>','bln7');
	$this->datatables->edit_column('bln8','<strong>$1</strong>','bln8');
	$this->datatables->edit_column('bln9','<strong>$1</strong>','bln9');
	$this->datatables->edit_column('bln10','<strong>$1</strong>','bln10');
	$this->datatables->edit_column('bln11','<strong>$1</strong>','bln11');
	$this->datatables->edit_column('bln12','<strong>$1</strong>','bln12');
	$this->datatables->edit_column('total','<strong>$1</strong>','total');
	
	
	echo $this->datatables->generate();
    }
    function post_pelatihan_narasumber($id){
	$this->session->set_flashdata('tahun',$this->input->post('tahun'));
	$this->session->set_flashdata('periode',$this->input->post('periode'));
	
	redirect('fasilitator/pelatihan_narasumber/'.$id);
    }
    function pelatihan_narasumber($id){
	$data['username'] = $this->session->userdata('username');
		$data['role'] = $this->session->userdata('role');
		$data['id_user'] = $this->session->userdata('id');
		$data['countNotifNarasumber'] = $this->model_pelatihan->countNotifNarasumberFasilitator($this->session->userdata('id'));


		$data['pelatihan'] = $this->model_pelatihan->getPelatihanByIdPelatihanIdFasilitatorArsip($id);
        $data['total_request'] = $this->model_pelatihan->newRequestFasilitator($this->session->userdata('id'));

		$this->load->view('layout_admin/header',$data);
		$this->load->view('fasilitator/pelatihan_narasumber');
		$this->load->view('layout_admin/footer');
    }
    
    public function proses_arsip_pelatihan($id_fasilitator,$id)
    {
    	$catatan = $this->input->post('catatan');
    	if(isset($catatan)){
    		$selesai = 1;
    	}else{
    		$selesai = 0;
    	}

    	$this->db->query("UPDATE tbl_pelatihan SET arsip ='1' WHERE id_pelatihan ='$id' ");	

    	$this->db->select('*');
        $this->db->where('tbl_pelatihan.id_pelatihan',$id);
        $this->db->from('tbl_pelatihan');
        $query = $this->db->get();
        $data_pelatihan = $query->result();

        foreach ($data_pelatihan as $data):
            $proses = $this->db->query("INSERT INTO tbl_pelatihan_arsip_fasilitator VALUES
                ('',
                '$data->id_tema',
                '$data->id_pelatihan',
                '$data->isi_pelatihan',
                '$data->contact_nama',
                '$data->contact_hp',
                '$data->tempat_pelatihan',
                '$data->mulai_tanggal',
                '$data->sampai_tanggal',
                '$data->instansi',
                '$data->created_by',
                '$data->id_lpp',
                '$data->id_request',
                '0',
                '0',
                '$now',
                '$data->jumlah_peserta',
                '$data->surat_permohonan',
                '$data->tgl_diapprove',
                '$data->alasan_ditolak',
                '$selesai',
                '$catatan',
                '$data->total_jp')");
        endforeach;


        redirect('fasilitator/pelatihan/'.$id_fasilitator);
    }

    public function detail_pelatihan($id)
    {
    	$data['username'] = $this->session->userdata('username');
		$data['role'] = $this->session->userdata('role');
		$data['id_user'] = $this->session->userdata('id');
		$data['countNotifNarasumber'] = $this->model_pelatihan->countNotifNarasumberFasilitator($this->session->userdata('id'));

		
        
        $data['row_narasumber'] = $this->model_pelatihan->countRowTable('pelatihan_narasumber','id_pelatihan',$id);
        $data['row_peserta'] = $this->model_pelatihan->countRowTable('tbl_peserta','id_pelatihan',$id);
        $data['row_eval_penyelenggara'] = $this->model_pelatihan->countRowTable('evaluasi_penyelenggara','id_pelatihan',$id);
        $data['row_eval_narasumber'] = $this->model_pelatihan->countRowTable('evaluasi_narasumber','id_pelatihan',$id);
        
		$data['id'] = $id;

		$data['detail'] = $this->model_pelatihan->getPelatihanByIdPelatihan($id);
        $data['total_request'] = $this->model_pelatihan->newRequestFasilitator($this->session->userdata('id'));
        
        $this->model_pelatihan->setRequestRead($id,'1');

		$this->load->view('layout_admin/header',$data);
		$this->load->view('fasilitator/menu_detail_pelatihan');
		$this->load->view('lkpp/detail_pelatihan',$data);
		$this->load->view('layout_admin/footer');
    }

    public function input_peserta($id)
    {
    	$data['username'] = $this->session->userdata('username');
		$data['role'] = $this->session->userdata('role');
		$data['id_user'] = $this->session->userdata('id');
		$data['countNotifNarasumber'] = $this->model_pelatihan->countNotifNarasumberFasilitator($this->session->userdata('id'));
		
        $data['row_narasumber'] = $this->model_pelatihan->countRowTable('pelatihan_narasumber','id_pelatihan',$id);
        $data['row_peserta'] = $this->model_pelatihan->countRowTable('tbl_peserta','id_pelatihan',$id);
        $data['row_eval_penyelenggara'] = $this->model_pelatihan->countRowTable('evaluasi_penyelenggara','id_pelatihan',$id);
        $data['row_eval_narasumber'] = $this->model_pelatihan->countRowTable('evaluasi_narasumber','id_pelatihan',$id);
        

		
		$data['id'] = $id;

		$data['judul_pelatihan'] = $this->model_pelatihan->getJudulPelatihan($id);
		$data['detail'] = $this->model_pelatihan->getTemaPelatihanByIdRequest($id);
		$data['semua_peserta'] = $this->model_peserta->getAllPesertaByIdPelatihan($id);
        $data['total_request'] = $this->model_pelatihan->newRequestFasilitator($this->session->userdata('id'));

		$this->load->view('layout_admin/header',$data);
		$this->load->view('fasilitator/menu_detail_pelatihan');
		/*$this->load->view('lpp/input_peserta',$data);*/		
		$this->load->view('fasilitator/input_peserta',$data);
		$this->load->view('layout_admin/footer');
    }
    public function input_peserta_manual($id)
    {
		$data['username'] = $this->session->userdata('username');
		$data['role'] = $this->session->userdata('role');
		$data['id_user'] = $this->session->userdata('id');
		$data['countNotifNarasumber'] = $this->model_pelatihan->countNotifNarasumberFasilitator($this->session->userdata('id'));


		
        $data['row_narasumber'] = $this->model_pelatihan->countRowTable('pelatihan_narasumber','id_pelatihan',$id);
        $data['row_peserta'] = $this->model_pelatihan->countRowTable('tbl_peserta','id_pelatihan',$id);
        $data['row_eval_penyelenggara'] = $this->model_pelatihan->countRowTable('evaluasi_penyelenggara','id_pelatihan',$id);
        $data['row_eval_narasumber'] = $this->model_pelatihan->countRowTable('evaluasi_narasumber','id_pelatihan',$id);
        
		$data['id'] = $id;

		$data['judul_pelatihan'] = $this->model_pelatihan->getJudulPelatihan($id);
		$data['detail'] = $this->model_pelatihan->getTemaPelatihanByIdRequest($id);
		$data['semua_peserta'] = $this->model_peserta->getAllPesertaByIdPelatihan($id);
        $data['total_request'] = $this->model_pelatihan->newRequestFasilitator($this->session->userdata('id'));

		$this->load->view('layout_admin/header',$data);
		$this->load->view('fasilitator/menu_detail_pelatihan');
		$this->load->view('fasilitator/input_peserta_manual',$data);
		$this->load->view('layout_admin/footer');
    }
    public function detail_peserta($id,$id_pelatihan)
    {
    	$data['username'] = $this->session->userdata('username');
		$data['role'] = $this->session->userdata('role');
		$data['id_user'] = $this->session->userdata('id');
		$data['countNotifNarasumber'] = $this->model_pelatihan->countNotifNarasumberFasilitator($this->session->userdata('id'));


        $data['row_narasumber'] = $this->model_pelatihan->countRowTable('pelatihan_narasumber','id_pelatihan',$id);
        $data['row_peserta'] = $this->model_pelatihan->countRowTable('tbl_peserta','id_pelatihan',$id);
        $data['row_eval_penyelenggara'] = $this->model_pelatihan->countRowTable('evaluasi_penyelenggara','id_pelatihan',$id);
        $data['row_eval_narasumber'] = $this->model_pelatihan->countRowTable('evaluasi_narasumber','id_pelatihan',$id);
        
		
		$data['id'] = $id_pelatihan;

		$data['judul_pelatihan'] = $this->model_pelatihan->getJudulPelatihan($id_pelatihan);
		$data['detail'] = $this->model_pelatihan->getTemaPelatihanByIdRequest($id_pelatihan);
		$data['detail_peserta'] = $this->model_peserta->getDetailPeserta($id);
        $data['total_request'] = $this->model_pelatihan->newRequestFasilitator($this->session->userdata('id'));

		$this->load->view('layout_admin/header',$data);
		$this->load->view('fasilitator/menu_detail_pelatihan');
		/*$this->load->view('lpp/input_peserta',$data);*/		
		$this->load->view('fasilitator/detail_peserta',$data);
		$this->load->view('layout_admin/footer');
    }
    public function proses_input_peserta($id)
    {
    	$data = $this->model_pelatihan->insert_peserta($id);
			if($data){
				//$data['status'] = "success";
				$this->session->set_flashdata('status', '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Narasumber berhasil ditambahkan</div>');

				
				redirect('fasilitator/input_peserta/'.$id,$data);
			}
    }
    public function upload_csv_peserta($id)
    {
    			$config['upload_path'] = 'assets/csv';
		        //$config['allowed_types'] = 'csv';
		        $config['allowed_types'] = 'csv|xls|xlsx|csvexcel';
		        //text/comma-separated-values|application/csv|application/excel|application/vnd.ms-excel|application/vnd.msexcel|text/anytext
		        $config['max_size'] = '100';
		        $config['max_width']  = '1024';
		        $config['max_height']  = '768';

		        $this->load->library('upload', $config);
                $this->upload->initialize($config);

		        if ( ! $this->upload->do_upload('csvfile'))
		        {
		            $error = array('error' => $this->upload->display_errors());
		            var_dump($error);
		        }
		        else
		        {
		            $upload_data = $this->upload->data();
		            
		            // start to read the CSV file
		            $this->load->library('csvreader');
		            $file_path = $upload_data['full_path'];
		            $csv_data = $this->csvreader->parse_file($file_path); 
		            $csv_fields = $this->csvreader->get_fields();
		            //echo "hae";
		                if($csv_fields) // match to one of database table
		                {
		                    foreach($csv_data as $row){
                                $id_peserta = $row['ID_PESERTA'];
                                $id_pelatihan = $id;
                                $nama_lengkap = $row['NAMA_LENGKAP'];
                                $gelar_akademis = $row['GELAR_AKADEMIS'];
                                $jenis_kelamin = $row['JENIS_KELAMIN'];
                                $NIP = $row['NIP'];
                                $no_ktp = $row['NO_KTP'];
                                $tempat_lahir = $row['TEMPAT_LAHIR'];
                                $tgl_lahir = $row['TGL_LAHIR'];
                                $pendidikan_terakhir = $row['PENDIDIKAN_TERAKHIR'];
                                $status_kepegawaian = $row['STATUS_KEPEGAWAIAN'];
                                $instansi = $row['INSTANSI'];
                                $lembaga = $row['LEMBAGA'];
                                $domisili_kabupaten = $row['DOMISILI_KABUPATEN'];
                                $domisili_provinsi = $row['DOMISILI_PROVINSI'];
                                $no_telepon = $row['NO_TELEPON'];
                                $alamat_email = $row['ALAMAT_EMAIL'];
                                $pengalaman_kerja_bidang_pengadaan_barang_pemerintah = $row['PENGALAMAN_KERJA_BIDANG_PENGADAAN_BARANG_PEMERINTAH'];
                                $pernah_mengikuti_pelatihan_barang_jasa = $row['PERNAH_MENGIKUTI_PELATIHAN_BARANG_JASA'];
                                $berapa_kali = $row['BERAPA_KALI'];
                                $alasan_mengikuti = $row['ALASAN_MENGIKUTI'];

                                //insert ke table peserta
						        $proses_tbl_peserta = $this->db->query("INSERT INTO tbl_peserta VALUES('$id_peserta','$nama_lengkap','$gelar_akademis','$jenis_kelamin','$NIP','$no_ktp','$tempat_lahir','$tgl_lahir','$pendidikan_terakhir','$status_kepegawaian','$instansi','$lembaga','$domisili_kabupaten','$domisili_provinsi','$no_telepon','$alamat_email','$pengalaman_kerja_bidang_pengadaan_barang_pemerintah','$pernah_mengikuti_pelatihan_barang_jasa','$berapa_kali','$alasan_mengikuti','$id_pelatihan','','')");

						        //$proses_peserta_pelatihan = $this->db->query("INSERT INTO pelatihan_peserta VALUES('','$id_peserta','$id_pelatihan','','')");
		                    }

		                    if(($proses_tbl_peserta)){
									//$data['status'] = "success";
									$this->session->set_flashdata('status', '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Peserta berhasil ditambahkan</div>');

									
									redirect('fasilitator/input_peserta/'.$id);
								}
		                }
		            //}
		            
		            /*$data = array(
		                'upload_data' => $upload_data,
		                'csv_data' => $csv_data,
		            );*/
		            
		            //$this->load->view('upload_success', $data);
		        }
    }


    public function input_narasumber($id)
    {
    	$data['username'] = $this->session->userdata('username');
		$data['role'] = $this->session->userdata('role');
		$data['id_user'] = $this->session->userdata('id');
		$data['countNotifNarasumber'] = $this->model_pelatihan->countNotifNarasumberFasilitator($this->session->userdata('id'));


        $data['row_narasumber'] = $this->model_pelatihan->countRowTable('pelatihan_narasumber','id_pelatihan',$id);
        $data['row_peserta'] = $this->model_pelatihan->countRowTable('tbl_peserta','id_pelatihan',$id);
        $data['row_eval_penyelenggara'] = $this->model_pelatihan->countRowTable('evaluasi_penyelenggara','id_pelatihan',$id);
        $data['row_eval_narasumber'] = $this->model_pelatihan->countRowTable('evaluasi_narasumber','id_pelatihan',$id);
        
		$data['id'] = $id;

		$data['detail'] = $this->model_pelatihan->getPelatihanByIdPelatihan($id);

		$dari_tanggal = $this->model_pelatihan->getTanggalDariPelatihanByIdPelatihan($id);
		$sampai_tanggal = $this->model_pelatihan->getTanggalSampaiPelatihanByIdPelatihan($id);

		$data['narasumber'] = $this->model_narasumber->getNarasumberAvailable($dari_tanggal,$sampai_tanggal);

		$data['semua_narasumber_temporary'] = $this->model_narasumber->getAllTemporaryNarasumberByRealIdPelatihan($id);
		$data['semua_narasumber'] = $this->model_narasumber->getAllNarasumberByRealIdPelatihan($id);

        $data['total_jp_row'] = $this->model_narasumber->countTotalJPByRealIdPelatihan($id);
        $data['total_jp_row_approve'] = $this->model_narasumber->countTotalJPByRealIdPelatihan($id);

        $data['total_request'] = $this->model_pelatihan->newRequestFasilitator($this->session->userdata('id'));

		$this->load->view('layout_admin/header',$data);
		$this->load->view('fasilitator/menu_detail_pelatihan');
		$this->load->view('fasilitator/input_narasumber',$data);
		$this->load->view('layout_admin/footer');
    }
    public function proses_insert_narasumber($id)
    {
    	

    	$this->form_validation->set_rules('tgl_pelatihan', 'tgl_pelatihan', 'required');

		if ($this->form_validation->run() == FALSE)
		{
			$this->session->set_flashdata('status', '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.validation_errors().'</div>');
				redirect('fasilitator/request_narasumber/'.$id,$data);
		}else{
            $tgl_pelatihan = $this->input->post('tgl_pelatihan');
            if($tgl_pelatihan=="-"){
                $this->session->set_flashdata('status', '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>tanggal pelatihan tidak boleh kosong</div>');
                redirect('fasilitator/request_narasumber/'.$id,$data);
            }else{
                $data = $this->model_narasumber->input_narasumber_temporary();
                if($data){
                    //$data['status'] = "success";
                    $this->session->set_flashdata('status', '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Narasumber berhasil ditambahkan</div>');

                    
                    redirect('fasilitator/input_narasumber/'.$id,$data);
                }else{
                    echo $data;
                }
            }
			
		}
    }
    public function eksekusi_narasumber($id,$id_narasumber)
    {
    	$insert_data =  $this->db->query("INSERT INTO pelatihan_narasumber(id_pelatihan,tgl_pelatihan,dari_jam,sampai_jam,id_narasumber,total_jam,approve,alasan,diganti_sama_lkpp,parent_id,aktif,materi_pelatihan) SELECT id_pelatihan,tgl_pelatihan,dari_jam,sampai_jam,id_narasumber,total_jam,approve,alasan,diganti_sama_lkpp,parent_id,aktif,materi_pelatihan FROM pelatihan_narasumber_temporary WHERE id='$id_narasumber'");
    	if($insert_data){
    		$delete_data = $this->db->query("DELETE FROM pelatihan_narasumber_temporary WHERE id='$id_narasumber'");

    		if($delete_data){
    			redirect('fasilitator/input_narasumber/'.$id,$data);
    		}else{

    		}
    	}else{
    		
    	}

    }

    public function delete_temporary_narasumber($id,$id_narasumber)
    {
    	$insert_data =  $this->db->query("DELETE FROM pelatihan_narasumber_temporary WHERE id='$id_narasumber'");
    	redirect('fasilitator/input_narasumber/'.$id,$data);

    }

    public function request_narasumber($id)
    {
    	$data['username'] = $this->session->userdata('username');
		$data['role'] = $this->session->userdata('role');
		$data['id_user'] = $this->session->userdata('id');
		$data['countNotifNarasumber'] = $this->model_pelatihan->countNotifNarasumberFasilitator($this->session->userdata('id'));

        $data['row_narasumber'] = $this->model_pelatihan->countRowTable('pelatihan_narasumber','id_pelatihan',$id);
        $data['row_peserta'] = $this->model_pelatihan->countRowTable('tbl_peserta','id_pelatihan',$id);
        $data['row_eval_penyelenggara'] = $this->model_pelatihan->countRowTable('evaluasi_penyelenggara','id_pelatihan',$id);
        $data['row_eval_narasumber'] = $this->model_pelatihan->countRowTable('evaluasi_narasumber','id_pelatihan',$id);
        

		$data['id'] = $id;

		$data['detail'] = $this->model_pelatihan->getPelatihanByIdPelatihan($id);

		$dari_tanggal = $this->model_pelatihan->getTanggalDariPelatihanByIdPelatihan($id);
		$sampai_tanggal = $this->model_pelatihan->getTanggalSampaiPelatihanByIdPelatihan($id);

		$data['narasumber'] = $this->model_narasumber->getAbsolutelyAllNarasumber();
		$data['semua_narasumber'] = $this->model_narasumber->getAllNarasumberByRealIdPelatihan($id);
        $data['total_request'] = $this->model_pelatihan->newRequestFasilitator($this->session->userdata('id'));

        $data['array_date'] = $this->createDateRangeArray($dari_tanggal,$sampai_tanggal);
        $data['total_jp_row'] = $this->model_narasumber->countTotalJPByRealIdPelatihanNotApprove($id);
        $data['total_jp_row_approve'] = $this->model_narasumber->countTotalJPByRealIdPelatihan($id);

        //print_r($data['array_date']);
        /*foreach ($this->model_narasumber->getAllNarasumberByRealIdPelatihanTidakDitolak($id) as $data_detail) {

            //echo "<br>".$data_detail->tgl_pelatihan."<br>";
            if(($key = array_search($data_detail->tgl_pelatihan, $data['array_date'])) !== false) {
                unset($data['array_date'][$key]);
            }

        }*/
        //print_r($data['array_date']);
        /*echo key($data['array_date']);*/

		$this->load->view('layout_admin/header',$data);
		$this->load->view('fasilitator/menu_detail_pelatihan');
		$this->load->view('fasilitator/request_narasumber',$data);
		$this->load->view('layout_admin/footer');
    }
    public function new_request($id)
    {
    	$data['username'] = $this->session->userdata('username');
		$data['role'] = $this->session->userdata('role');
		$data['id_user'] = $this->session->userdata('id');
		$data['countNotifNarasumber'] = $this->model_pelatihan->countNotifNarasumberFasilitator($this->session->userdata('id'));


		$data['pelatihan'] = $this->model_pelatihan->getNewPelatihanByIdPelatihanIdFasilitator($id);
        $data['total_request'] = $this->model_pelatihan->newRequestFasilitator($this->session->userdata('id'));


		$this->load->view('layout_admin/header',$data);
		$this->load->view('fasilitator/new_request');
		$this->load->view('layout_admin/footer');
    }

    public function delete_peserta($id,$id_pelatihan)
    {
    	$data = $this->model_peserta->delete('tbl_peserta','id_peserta',$id);
    	if($data){
				$this->session->set_flashdata('status', '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Data Berhasil di Hapus</div>');
				redirect('fasilitator/input_peserta/'.$id_pelatihan,$data);
			}else{
				$this->session->set_flashdata('status', '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$data.'</div>');
				redirect('fasilitator/input_peserta/'.$id_pelatihan,$data);
			}
    }

    public function proses_update_peserta($id_pelatihan)
    {
    	$data = $this->model_peserta->updatePeserta($id_pelatihan);
			if($data){
				//$data['status'] = "success";
				$this->session->set_flashdata('status', '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Narasumber berhasil ditambahkan</div>');

				
				redirect('fasilitator/input_peserta/'.$id_pelatihan,$data);
			}
    }
    public function evaluasi_penyelenggara($id_pelatihan)
	{
		$data['username'] = $this->session->userdata('username');
		$data['role'] = $this->session->userdata('role');
		$data['id_user'] = $this->session->userdata('id');
		$data['countNotifNarasumber'] = $this->model_pelatihan->countNotifNarasumberFasilitator($this->session->userdata('id'));

        $data['row_narasumber'] = $this->model_pelatihan->countRowTable('pelatihan_narasumber','id_pelatihan',$id_pelatihan);
        $data['row_peserta'] = $this->model_pelatihan->countRowTable('tbl_peserta','id_pelatihan',$id_pelatihan);
        $data['row_eval_penyelenggara'] = $this->model_pelatihan->countRowTable('evaluasi_penyelenggara','id_pelatihan',$id_pelatihan);
        $data['row_eval_narasumber'] = $this->model_pelatihan->countRowTable('evaluasi_narasumber','id_pelatihan',$id_pelatihan);
        
		$id_lpp = $this->session->userdata('id');
		$data['nama_lpp'] = $this->model_lpp->getLppName($this->session->userdata('id'));
		$data['id_lpp'] = $this->session->userdata('id');
		
		$data['detail'] = $this->model_pelatihan->getPelatihanByIdPelatihan($id_pelatihan);
        $data['total_request'] = $this->model_pelatihan->newRequestFasilitator($this->session->userdata('id'));
		
		$data['id'] = $id_pelatihan;

		$this->load->view('layout_admin/header',$data);
		$this->load->view('fasilitator/menu_detail_pelatihan');

			$data['dataEvaluasi'] = $this->model_evaluasi->getEvaluasiPenyelenggaraTanpaLpp($id_pelatihan);
			$this->load->view('fasilitator/evaluasi_penyelenggara',$data);
		
		$this->load->view('layout_admin/footer');
	}

	public function evaluasi_narasumber($id_pelatihan)
	{
		$data['username'] = $this->session->userdata('username');
		$data['role'] = $this->session->userdata('role');
		$data['id_user'] = $this->session->userdata('id');
		$data['countNotifNarasumber'] = $this->model_pelatihan->countNotifNarasumberFasilitator($this->session->userdata('id'));

        $data['row_narasumber'] = $this->model_pelatihan->countRowTable('pelatihan_narasumber','id_pelatihan',$id_pelatihan);
        $data['row_peserta'] = $this->model_pelatihan->countRowTable('tbl_peserta','id_pelatihan',$id_pelatihan);
        $data['row_eval_penyelenggara'] = $this->model_pelatihan->countRowTable('evaluasi_penyelenggara','id_pelatihan',$id_pelatihan);
        $data['row_eval_narasumber'] = $this->model_pelatihan->countRowTable('evaluasi_narasumber','id_pelatihan',$id_pelatihan);
        
		$id_lpp = $this->session->userdata('id');
		$data['nama_lpp'] = $this->model_lpp->getLppName($this->session->userdata('id'));
		$data['id_lpp'] = $this->session->userdata('id');
		
		$data['detail'] = $this->model_pelatihan->getPelatihanByIdPelatihan($id_pelatihan);
        $data['total_request'] = $this->model_pelatihan->newRequestFasilitator($this->session->userdata('id'));
		
		$data['id'] = $id_pelatihan;
		
		$data['detail'] = $this->model_pelatihan->getPelatihanByIdPelatihan($id_pelatihan);
		$data['dataEvaluasi'] = $this->model_evaluasi->getEvaluasiNarasumber($id_pelatihan);

		$data['id'] = $id_pelatihan;
		$data['akreditasi'] = $this->model_lpp->getAkreditasiLpp($this->session->userdata('id'));

		$this->load->view('layout_admin/header',$data);
		$this->load->view('fasilitator/menu_detail_pelatihan');
		$this->load->view('fasilitator/evaluasi_narasumber',$data);
		$this->load->view('layout_admin/footer');
	}
    public function batalkan_narasumber($id_pelatihan,$id_narasumber)
    {
        $delete1 = $this->db->query("DELETE 
            FROM pelatihan_narasumber 
            WHERE id_pelatihan = '$id_pelatihan' 
            AND id_narasumber='$id_narasumber'
            ");
        if($delete1){
            $delete2 = $this->db->query("DELETE 
            FROM tbl_kegiatan_natasumber 
            WHERE id_pelatihan = '$id_pelatihan' 
            AND id_narasumber='$id_narasumber'");

                if($delete2){
                    $this->session->set_flashdata('status', '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        Narasumber berhasil Dibatalkan</div>');
                    redirect(base_url().'fasilitator/input_narasumber/'.$id_pelatihan);
                }
        }
        
    }

    function createDateRangeArray($strDateFrom,$strDateTo)
    {
       
        $aryRange=array();

        $iDateFrom=mktime(1,0,0,substr($strDateFrom,5,2),     substr($strDateFrom,8,2),substr($strDateFrom,0,4));
        $iDateTo=mktime(1,0,0,substr($strDateTo,5,2),     substr($strDateTo,8,2),substr($strDateTo,0,4));

        if ($iDateTo>=$iDateFrom)
        {
            array_push($aryRange,date('Y-m-d',$iDateFrom)); // first entry
            while ($iDateFrom<$iDateTo)
            {
                $iDateFrom+=86400; // add 24 hours
                array_push($aryRange,date('Y-m-d',$iDateFrom));
            }
        }
        return $aryRange;
    }


    public function print_pelatihan($id_pelatihan)
    {
            $data['id_pelatihan'] = $id_pelatihan;
        
            $data['detail'] = $this->model_pelatihan->getPelatihanByIdPelatihan($id_pelatihan);
            $data['data_fasilitator'] = $this->model_pelatihan->get_fasilitator_by_pelatihan($id_pelatihan);
            $data['data_narasumber'] = $this->model_narasumber->getAllNarasumberByRealIdPelatihan($id_pelatihan);
            $data['data_peserta'] = $this->model_peserta->getAllPesertaByIdPelatihan($id_pelatihan);
            $data['data_evaluasi'] = $this->model_evaluasi->getEvaluasiPenyelenggaraTanpaLpp($id_pelatihan);
            $data['data_evaluasi_narasumber'] = $this->model_evaluasi->getEvaluasiNarasumber($id_pelatihan);

            $html = $this->load->view('fasilitator/report',$data,TRUE);

            $data['laporan'] = "detail_pelatihan";
            
            //echo $html;

            $this->load->library('mpdf/mpdf');
            $this->mpdf=new mPDF('c','A4','','');
            $this->mpdf->WriteHTML($html,2);
            $mpdf->useDefaultCSS2 = true;
            $this->mpdf->Output('LAPORAN PELATIHAN.pdf','I');
    }
}