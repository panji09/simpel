<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Satker extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
    {
        parent::__construct();
        $this->load->model('model_pelatihan');
        $this->load->model('model_request_form');
        if( ($this->session->userdata('logged_in')!=1) )
        {
            redirect('admin/login');
        }
    }
	public function index()
	{
		$data['username'] = $this->session->userdata('username');
		$data['role'] = $this->session->userdata('role');

		$data['tema_pelatihan'] = $this->model_pelatihan->getAllTemaPelatihan();
		$data['totalApproved'] = $this->model_request_form->countApprovedRequest();
		$data['totalCanceled'] = $this->model_request_form->countCanceledRequest();
		$data['totalPending'] = $this->model_request_form->countPendingRequest();

		$data['notiftotalApproved'] = $this->model_request_form->countNotifApprovedRequest();
		$data['notiflisttotalApproved'] = $this->model_request_form->getNotifApprovedRequest();

		$data['notiftotalPending'] = $this->model_request_form->countNotifPendingRequest();
		$data['notiflisttotalPending'] = $this->model_request_form->getNotifPendingRequest();

		$data['notiftotalCanceled'] = $this->model_request_form->countNotifCanceledRequest();
		$data['notiflisttotalCanceled'] = $this->model_request_form->getNotifCanceledRequest();

		$this->load->view('layout_admin/header',$data);
		$this->load->view('satker/index');
		$this->load->view('layout_admin/footer');
	}
	public function allrequest($stat)
	{
		$data['username'] = $this->session->userdata('username');
		$data['role'] = $this->session->userdata('role');

		$data['tema_pelatihan'] = $this->model_pelatihan->getAllTemaPelatihan();
		$data['totalApproved'] = $this->model_request_form->countApprovedRequest();
		$data['totalCanceled'] = $this->model_request_form->countCanceledRequest();
		$data['totalPending'] = $this->model_request_form->countPendingRequest();

		$data['notiftotalApproved'] = $this->model_request_form->countNotifApprovedRequest();
		$data['notiflisttotalApproved'] = $this->model_request_form->getNotifApprovedRequest();

		$data['notiftotalPending'] = $this->model_request_form->countNotifPendingRequest();
		$data['notiflisttotalPending'] = $this->model_request_form->getNotifPendingRequest();

		$data['notiftotalCanceled'] = $this->model_request_form->countNotifCanceledRequest();
		$data['notiflisttotalCanceled'] = $this->model_request_form->getNotifCanceledRequest();

		$data['allRequest'] = $this->model_request_form->getAllRequestWhere($stat);

		$data['stat'] = $stat;

		$this->load->view('layout_admin/header',$data);
		$this->load->view('satker/all',$data);
		$this->load->view('layout_admin/footer');
	}
	public function insertPelatihan()
	{

		$this->form_validation->set_rules('tema_pelatihan', 'tema_pelatihan', 'required');
		$this->form_validation->set_rules('dari_tanggal', 'dari_tanggal', 'required');
		$this->form_validation->set_rules('sampai_tanggal', 'sampai_tanggal', 'required');

		if ($this->form_validation->run() == FALSE)
		{
			echo validation_errors();
			//redirect('satker');
		}else{
			$data = $this->model_request_form->addNewPelatihan();
			if($data){
				//$data['status'] = "success";
				$this->session->set_flashdata('status', '<div class="alert alert-success">Request data telah dikirim kepada LKPP</div>');
				redirect('satker/index',$data);
			}else{
				//$data['status'] = "failed";
				$this->session->set_flashdata('status', '<div class="alert alert-danger">gagal memasukan data, coba beberapa saat lagi</div>');
				redirect('satker/index',$data);
			}
		}
		
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */