<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Profil extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        $this->load->model('model_pelatihan');
        $this->load->model('model_lpp');
        $this->load->model('model_narasumber');
        $this->load->model('model_peserta');
        $this->load->model('model_request_pelatihan');
        $this->load->model('model_berita');
        $this->load->helper('tgl_indonesia');
        $this->load->library('MY_Upload');
    }
	public function visi()
    {
        $data['headerPengumuman'] = $this->model_berita->getheaderPengumuman();
        $this->load->view('frontend/header',$data);
        $this->load->view('frontend/visi');
        $this->load->view('frontend/footer');
    }
    public function misi()
    {
        $data['headerPengumuman'] = $this->model_berita->getheaderPengumuman();
        $this->load->view('frontend/header',$data);
        $this->load->view('frontend/misi');
        $this->load->view('frontend/footer');
    }
    public function moto()
    {
        $data['headerPengumuman'] = $this->model_berita->getheaderPengumuman();
        $this->load->view('frontend/header',$data);
        $this->load->view('frontend/moto');
        $this->load->view('frontend/footer');
    }
    public function maklumat()
    {
        $data['headerPengumuman'] = $this->model_berita->getheaderPengumuman();
        $this->load->view('frontend/header',$data);
        $this->load->view('frontend/maklumat');
        $this->load->view('frontend/footer');
    }
    public function sop()
    {
        $this->load->view('frontend/header',$data);
        $this->load->view('frontend/sop');
        $this->load->view('frontend/footer');
    }
    public function standar_pelayanan()
    {
        $data['headerPengumuman'] = $this->model_berita->getheaderPengumuman();
        $this->load->view('frontend/header',$data);
        $this->load->view('frontend/standar_pelayanan');
        $this->load->view('frontend/footer');
    }
    public function layanan()
    {
        $data['headerPengumuman'] = $this->model_berita->getheaderPengumuman();
        $this->load->view('frontend/header',$data);
        $this->load->view('frontend/layanan');
        $this->load->view('frontend/footer');
    }

}