<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Faq extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        $this->load->model('model_pelatihan');
        $this->load->model('model_lpp');
        $this->load->model('model_narasumber');
        $this->load->model('model_peserta');
        $this->load->model('model_request_pelatihan');
        $this->load->model('model_berita');
        $this->load->helper('tgl_indonesia');
        $this->load->library('MY_Upload');
    }
	public function index()
    {

        $data['headerPengumuman'] = $this->model_berita->getheaderPengumuman();

        $this->load->view('frontend/header',$data);
        $this->load->view('frontend/faq');
        $this->load->view('frontend/footer');
    }

}