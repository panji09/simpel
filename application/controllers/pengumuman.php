<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pengumuman extends CI_Controller {
	public function __construct()
    {
        parent::__construct();
        $this->load->model('model_berita');
        $this->load->model('model_narasumber');
        $this->load->model('model_pelatihan');
        $this->load->library("pagination");
        $this->load->helper('tgl_indonesia');
    }
    public function detail($id_pengumuman)
    {
    	$data['dataBerita'] = $this->model_berita->getDetailPengumuman($id_pengumuman);
		$data['headerBerita'] = $this->model_berita->getheaderBerita();
        $data['headerPengumuman'] = $this->model_berita->getheaderPengumuman();

		$data['dataFoto'] = $this->model_pelatihan->get_foto();
		$data['dataPengumuman'] = $this->model_berita->getPengumuman();
		$this->load->view('frontend/header',$data);
		$this->load->view('frontend/detail_pengumuman');
		$this->load->view('frontend/footer');
    }
} 