<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Main_controller extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
    {
        parent::__construct();
        $this->load->model('model_berita');
        $this->load->model('model_narasumber');
        $this->load->model('model_pelatihan');
        $this->load->library("pagination");
        $this->load->helper('tgl_indonesia');
    }

	public function index()
	{
		$config = array();
        $config["base_url"] = base_url() . "lkpp/berita/page";
        //HITUNG JUMLAH ROW DI TABLE BERITA//
        $config["total_rows"] = $this->model_narasumber->record_count('tbl_berita');
        $config["per_page"] = 5;
        $config["uri_segment"] = 3;
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $data["links"] = $this->pagination->create_links();
        $data['page'] = "berita";
		$data['dataBerita'] = $this->model_berita->getAllBerita($config["per_page"], $page);
		$data['headerBerita'] = $this->model_berita->getheaderBerita();
        $data['headerPengumuman'] = $this->model_berita->getheaderPengumuman();

		$data['dataFoto'] = $this->model_pelatihan->get_foto();
		$data['dataPengumuman'] = $this->model_berita->getPengumuman();

		$this->load->view('frontend/header',$data);
		$this->load->view('frontend/index');
		$this->load->view('frontend/footer');
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */