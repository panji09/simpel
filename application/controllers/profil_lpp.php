<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Profil_lpp extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        $this->load->model('model_pelatihan');
        $this->load->model('model_lpp');
        $this->load->model('model_narasumber');
        $this->load->model('model_peserta');
        $this->load->model('model_request_pelatihan');
        $this->load->model('model_berita');
        $this->load->helper('tgl_indonesia');
        $this->load->library('MY_Upload');
    }
	public function data($id)
    {

        $data['headerPengumuman'] = $this->model_berita->getheaderPengumuman();
        $data['profil_lpp'] = $this->model_lpp->getDetailLpp($id);

        $this->load->view('frontend/header',$data);
        $this->load->view('frontend/profil_lpp');
        $this->load->view('frontend/footer');
    }

}