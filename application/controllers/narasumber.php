<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Narasumber extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
    {
        parent::__construct();
        $this->load->model('model_pelatihan');
        $this->load->model('model_lpp');
        $this->load->model('model_narasumber');
        $this->load->model('model_request_pelatihan');
        $this->load->model('model_detail_request_pelatihan');
        $this->load->model('model_rencana_kegiatan');
        $this->load->model('app_model');
        if( ($this->session->userdata('logged_in')!=1) || ($this->session->userdata('role')!='narasumber') )
        {
        	$this->app_model->logout();
            redirect('admin/login');
        }
        
        session_start();
        
        $_SESSION['KCFINDER'] = array(
	    'disabled' => false,
	    'access' => array(

	      'files' => array(
		  'upload' => false,
		  'delete' => true,
		  'copy'   => true,
		  'move'   => true,
		  'rename' => true
	      ),

	      'dirs' => array(
		  'create' => true,
		  'delete' => true,
		  'rename' => true
	      )
	  )
	);
    }
    public function index()
    {
    	$data['username'] = $this->session->userdata('username');
		$data['role'] = $this->session->userdata('role');
		$data['id_user'] = $this->session->userdata('id');
		$data['detail'] = $this->model_narasumber->getDetailNarasumber($this->session->userdata('id'));

		$data['notiftotalPending'] = $this->model_narasumber->countNotifPendingRequest($this->session->userdata('id'));
		$data['notiflisttotalPending'] = $this->model_narasumber->getNotifPendingRequest($this->session->userdata('id'));

		$data['notiftotalApproved'] = $this->model_narasumber->countNotifApprovedRequest();
		$data['notiflisttotalApproved'] = $this->model_narasumber->getNotifApprovedRequest();

		$data['notiftotalCanceled'] = $this->model_narasumber->countNotifCanceledRequest();
		$data['notiflisttotalCanceled'] = $this->model_narasumber->getNotifCanceledRequest();

		$data['nama_narasumber'] = $this->model_narasumber->getNameNarasumberByUsername($this->session->userdata('username'));

		$data['JPpribadi'] = $this->model_narasumber->getJPagendapribadi($this->session->userdata('id'));
		$data['JPlkpp'] = $this->model_narasumber->getJPlkpp($this->session->userdata('id'));

		$data['detail_kegiatan_narasumber'] = $this->model_narasumber->getDetailKegiatanNarasumber($this->session->userdata('id'),"");

		$this->load->view('layout_admin/header',$data);
		$this->load->view('narasumber/index');
		$this->load->view('layout_admin/footer');
    }
    
    public function file_explorer(){
	$data['username'] = $this->session->userdata('username');
		$data['role'] = $this->session->userdata('role');
		$data['id_user'] = $this->session->userdata('id');
		$data['detail'] = $this->model_narasumber->getDetailNarasumber($this->session->userdata('id'));

		$data['notiftotalPending'] = $this->model_narasumber->countNotifPendingRequest($this->session->userdata('id'));
		$data['notiflisttotalPending'] = $this->model_narasumber->getNotifPendingRequest($this->session->userdata('id'));

		$data['notiftotalApproved'] = $this->model_narasumber->countNotifApprovedRequest();
		$data['notiflisttotalApproved'] = $this->model_narasumber->getNotifApprovedRequest();

		$data['notiftotalCanceled'] = $this->model_narasumber->countNotifCanceledRequest();
		$data['notiflisttotalCanceled'] = $this->model_narasumber->getNotifCanceledRequest();

		$data['nama_narasumber'] = $this->model_narasumber->getNameNarasumberByID($this->session->userdata('username'));

		$data['JPpribadi'] = $this->model_narasumber->getJPagendapribadi($this->session->userdata('id'));
		$data['JPlkpp'] = $this->model_narasumber->getJPlkpp($this->session->userdata('id'));

		$data['detail_kegiatan_narasumber'] = $this->model_narasumber->getDetailKegiatanNarasumber($this->session->userdata('id'),"");

	
	$this->load->view('layout_admin/header',$data);
	$this->load->view('lkpp/file_explorer');
	$this->load->view('layout_admin/footer');

    }
    
    public function detail_kegiatan_narasumber($id)
    {
    	$data['username'] = $this->session->userdata('username');
		$data['role'] = $this->session->userdata('role');
		$data['id_user'] = $this->session->userdata('id');
		$data['detail'] = $this->model_narasumber->getDetailNarasumber($this->session->userdata('id'));

		$data['notiftotalPending'] = $this->model_narasumber->countNotifPendingRequest($this->session->userdata('id'));
		$data['notiflisttotalPending'] = $this->model_narasumber->getNotifPendingRequest($this->session->userdata('id'));

		$data['notiftotalApproved'] = $this->model_narasumber->countNotifApprovedRequest();
		$data['notiflisttotalApproved'] = $this->model_narasumber->getNotifApprovedRequest();

		$data['notiftotalCanceled'] = $this->model_narasumber->countNotifCanceledRequest();
		$data['notiflisttotalCanceled'] = $this->model_narasumber->getNotifCanceledRequest();

		$data['nama_narasumber'] = $this->model_narasumber->getNameNarasumberByID($this->session->userdata('username'));

		$data['JPpribadi'] = $this->model_narasumber->getJPagendapribadi($this->session->userdata('id'));
		$data['JPlkpp'] = $this->model_narasumber->getJPlkpp($this->session->userdata('id'));
		
    	$condition = $this->input->post('condition');

		$data['dari_tanggal'] = $this->input->post('dari_tanggal');
		$data['sampai_tanggal'] = $this->input->post('sampai_tanggal');

		$dari_tanggal = $data['dari_tanggal'];
		$sampai_tanggal = $data['sampai_tanggal'];

		if($dari_tanggal!=null){
			$dari_tanggal = $this->input->post('dari_tanggal');
			$condition .= " AND tbl_kegiatan_natasumber.tanggal >= '$dari_tanggal' ";
		}
		if($sampai_tanggal!=null){
			$sampai_tanggal = $this->input->post('sampai_tanggal');
			$condition .= " AND tbl_kegiatan_natasumber.tanggal <= '$sampai_tanggal' ";
		}

		
		$data['detail_kegiatan_narasumber'] = $this->model_narasumber->getDetailKegiatanNarasumber($id,$condition);
		$data['detail_narasumber'] = $this->model_narasumber->getDetailNarasumber($id);
		$data['conditionQuery'] = $condition;

		$this->load->view('layout_admin/header',$data);
		$this->load->view('narasumber/index');
		$this->load->view('layout_admin/footer');

    }

    public function list_narasumber()
    {
    	$data['jadwalPelatihanNarasumber'] = $this->model_narasumber->getAllPelatihanNarasumber();
		$data['id_user'] = $this->session->userdata('id');

    	$data['notiftotalPending'] = $this->model_narasumber->countNotifPendingRequest($this->session->userdata('id'));
		$data['notiflisttotalPending'] = $this->model_narasumber->getNotifPendingRequest($this->session->userdata('id'));

		$data['notiftotalApprove'] = $this->model_narasumber->countNotifApprovedRequest();
		$data['notiftotalApprove'] = $this->model_narasumber->getNotifApprovedRequest();

		$data['notiftotalCancel'] = $this->model_narasumber->countNotifCanceledRequest();
		$data['notiftotalCancel'] = $this->model_narasumber->getNotifCanceledRequest();

		$data['nama_narasumber'] = $this->model_narasumber->getNameNarasumberByID($this->session->userdata('username'));

		$this->load->view('layout_admin/header',$data);
		$this->load->view('narasumber/index');
		$this->load->view('layout_admin/footer');
    }

    public function schedule_narasumber()
    {
    	$data['username'] = $this->session->userdata('username');
		$data['role'] = $this->session->userdata('role');
		$data['id_user'] = $this->session->userdata('id');
		$data['detail'] = $this->model_narasumber->getDetailNarasumber($this->session->userdata('id'));

		$data['notiftotalPending'] = $this->model_narasumber->countNotifPendingRequest($this->session->userdata('id'));
		$data['notiflisttotalPending'] = $this->model_narasumber->getNotifPendingRequest($this->session->userdata('id'));

		$data['notiftotalApproved'] = $this->model_narasumber->countNotifApprovedRequest();
		$data['notiflisttotalApproved'] = $this->model_narasumber->getNotifApprovedRequest();

		$data['notiftotalCanceled'] = $this->model_narasumber->countNotifCanceledRequest();
		$data['notiflisttotalCanceled'] = $this->model_narasumber->getNotifCanceledRequest();

		$data['nama_narasumber'] = $this->model_narasumber->getNameNarasumberByID($this->session->userdata('username'));
    	//$data['schedulePelatihanNarasumber'] = $this->model_narasumber->getAllPelatihanNarasumber();

		$data['jadwal_pelatihan_narasumber'] = $this->model_narasumber->getSchedulePelatihanNarasumber($this->session->userdata('id'));

		$this->load->view('layout_admin/header',$data);
		$this->load->view('narasumber/schedule_narasumber');
		$this->load->view('layout_admin/footer');
    }

    public function detail_schedule_narasumber($id_pelatihan)
    {
    	$data['username'] = $this->session->userdata('username');
		$data['role'] = $this->session->userdata('role');
		$data['id_user'] = $this->session->userdata('id');
		$data['detail'] = $this->model_narasumber->getDetailNarasumber($this->session->userdata('id'));

		//$data['tema_pelatihan'] = $this->model_pelatihan->getAllTemaPelatihan();

		$data['notiftotalPending'] = $this->model_narasumber->countNotifPendingRequest($this->session->userdata('id'));
		$data['notiflisttotalPending'] = $this->model_narasumber->getNotifPendingRequest($this->session->userdata('id'));

		$data['notiftotalApproved'] = $this->model_narasumber->countNotifApprovedRequest();
		$data['notiflisttotalApproved'] = $this->model_narasumber->getNotifApprovedRequest();

		$data['notiftotalCanceled'] = $this->model_narasumber->countNotifCanceledRequest();
		$data['notiflisttotalCanceled'] = $this->model_narasumber->getNotifCanceledRequest();

		$data['detailAll'] = $this->model_pelatihan->getDetaillPelatihan($id_pelatihan);

		$data['nama_narasumber'] = $this->model_narasumber->getNameNarasumberByID($this->session->userdata('username'));
		$data['semua_narasumber'] = $this->model_narasumber->getAllNarasumberByRealIdPelatihan($id_pelatihan);

		$this->load->view('layout_admin/header',$data);
		$this->load->view('narasumber/detail_schedule_narasumber');
		$this->load->view('layout_admin/footer');
    }

    public function all_request_pelatihan()
    {
    	$data['username'] = $this->session->userdata('username');
		$data['role'] = $this->session->userdata('role');
		$id = $this->session->userdata('id');
		$data['id_user'] = $this->session->userdata('id');
		$data['detail'] = $this->model_narasumber->getDetailNarasumber($this->session->userdata('id'));

		$data['notiftotalPending'] = $this->model_narasumber->countNotifPendingRequest($this->session->userdata('id'));
		$data['notiflisttotalPending'] = $this->model_narasumber->getNotifPendingRequest($this->session->userdata('id'));

		$data['notiftotalApproved'] = $this->model_narasumber->countNotifApprovedRequest();
		$data['notiflisttotalApproved'] = $this->model_narasumber->getNotifApprovedRequest();

		$data['notiftotalCanceled'] = $this->model_narasumber->countNotifCanceledRequest();
		$data['notiflisttotalCanceled'] = $this->model_narasumber->getNotifCanceledRequest();

		$data['all_request'] = $this->model_narasumber->getAllRequestPelatihanById($id);

		$data['nama_narasumber'] = $this->model_narasumber->getNameNarasumberByID($this->session->userdata('username'));

		$this->load->view('layout_admin/header',$data);
		$this->load->view('narasumber/all_request_pelatihan_narasumber');
		$this->load->view('layout_admin/footer');
    }

    public function detail_all_request_pelatihan($id,$id_pelatihan)
	{
	    
	    $data['username'] = $this->session->userdata('username');
		$data['role'] = $this->session->userdata('role');
		$data['id_user'] = $this->session->userdata('id');
		$data['detail'] = $this->model_narasumber->getDetailNarasumber($this->session->userdata('id'));

		//$data['tema_pelatihan'] = $this->model_pelatihan->getAllTemaPelatihan();

		$data['notiftotalPending'] = $this->model_narasumber->countNotifPendingRequest($this->session->userdata('id'));
		$data['notiflisttotalPending'] = $this->model_narasumber->getNotifPendingRequest($this->session->userdata('id'));

		$data['notiftotalApproved'] = $this->model_narasumber->countNotifApprovedRequest();
		$data['notiflisttotalApproved'] = $this->model_narasumber->getNotifApprovedRequest();

		$data['notiftotalCanceled'] = $this->model_narasumber->countNotifCanceledRequest();
		$data['notiflisttotalCanceled'] = $this->model_narasumber->getNotifCanceledRequest();

		$data['detailAll'] = $this->model_narasumber->getDetailRequestPelatihanById($id);

		$data['nama_narasumber'] = $this->model_narasumber->getNameNarasumberByID($this->session->userdata('username'));
		$data['semua_narasumber'] = $this->model_narasumber->getAllNarasumberByRealIdPelatihan($id_pelatihan);

		$this->load->view('layout_admin/header',$data);
		$this->load->view('narasumber/detail_request_pelatihan');
		$this->load->view('layout_admin/footer');
	}

	public function approve_request_pelatihan($id)
	{
        $this->form_validation->set_rules('alasan_tolak', 'alasan_tolak', 'required');
        $alasan = $this->input->post('alasan_tolak');
        $submit = $this->input->post('submit');


        if($submit==2){
        	if ($this->form_validation->run() == FALSE)
			{
				$this->session->set_flashdata('status', '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.validation_errors().'</div>');
					redirect('narasumber/all_request_pelatihan',$data);
			}else{
				$data = $this->model_narasumber->update_status_pelatihan_narasumber($id);
				if($data=="success"){
					//$data['status'] = "success";
					$this->session->set_flashdata('status', '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Pelatihan telah ditambahkan ke jadwal anda</div>');
					redirect('narasumber/all_request_pelatihan',$data);
				}else{
					//$data['status'] = "failed";
					var_dump($data);
					$this->session->set_flashdata('status', '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>gagal memasukan data, coba beberapa saat lagi'.var_dump($data).'</div>');
					redirect('narasumber/all_request_pelatihan',$data);
				}
			}
        }else{
        	$data = $this->model_narasumber->update_status_pelatihan_narasumber($id);
				if($data=="success"){
					//$data['status'] = "success";
					$this->session->set_flashdata('status', '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Pelatihan telah ditambahkan ke jadwal anda</div>');
					redirect('narasumber/all_request_pelatihan',$data);
				}else{
					//$data['status'] = "failed";
					var_dump($data);
					$this->session->set_flashdata('status', '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>gagal memasukan data, coba beberapa saat lagi'.var_dump($data).'</div>');
					redirect('narasumber/all_request_pelatihan',$data);
				}
        }
		

	    
	}
	public function rencana_kegiatan()
	{
		$data['username'] = $this->session->userdata('username');
		$data['role'] = $this->session->userdata('role');
		$data['id_user'] = $this->session->userdata('id');
		$data['detail'] = $this->model_narasumber->getDetailNarasumber($this->session->userdata('id'));

		$data['notiftotalPending'] = $this->model_narasumber->countNotifPendingRequest($this->session->userdata('id'));
		$data['notiflisttotalPending'] = $this->model_narasumber->getNotifPendingRequest($this->session->userdata('id'));

		$data['notiftotalApproved'] = $this->model_narasumber->countNotifApprovedRequest();
		$data['notiflisttotalApproved'] = $this->model_narasumber->getNotifApprovedRequest();

		$data['notiftotalCanceled'] = $this->model_narasumber->countNotifCanceledRequest();
		$data['notiflisttotalCanceled'] = $this->model_narasumber->getNotifCanceledRequest();

		$data['renKegiatan'] = $this->model_rencana_kegiatan->getRencanaKegiatan($this->session->userdata('id'));

		$data['nama_narasumber'] = $this->model_narasumber->getNameNarasumberByID($this->session->userdata('username'));

		$this->load->view('layout_admin/header',$data);
		$this->load->view('narasumber/rencana_kegiatan');
		$this->load->view('layout_admin/footer');
	}
	public function the_rencana_kegiatan()
    {
    	$data['username'] = $this->session->userdata('username');
		$data['role'] = $this->session->userdata('role');
		$data['id_user'] = $this->session->userdata('id');
		$data['detail'] = $this->model_narasumber->getDetailNarasumber($this->session->userdata('id'));

		//$data['tema_pelatihan'] = $this->model_pelatihan->getAllTemaPelatihan();

		$data['notiftotalPending'] = $this->model_request_pelatihan->countNotifPendingRequest();
		$data['notiflisttotalPending'] = $this->model_request_pelatihan->getNotifPendingRequest();

		$data['notiftotalApproved'] = $this->model_request_pelatihan->countNotifApprovedRequest();
		$data['notiflisttotalApproved'] = $this->model_request_pelatihan->getNotifApprovedRequest();

		$data['notiftotalCanceled'] = $this->model_request_pelatihan->countNotifCanceledRequest();
		$data['notiflisttotalCanceled'] = $this->model_request_pelatihan->getNotifCanceledRequest();

		$data['nama_narasumber'] = $this->model_narasumber->getNameNarasumberByID($this->session->userdata('username'));

		$this->load->view('layout_admin/header',$data);
		$this->load->view('narasumber/add_rencana');
		$this->load->view('layout_admin/footer');
    }

  
    public function add_rencana_kegiatan()
    {
    	$this->load->library('form_validation');
		$this->form_validation->set_rules('dari_tanggal', 'Dari Tanggal', 'required');
		$this->form_validation->set_rules('sampai_tanggal', 'Sampai Tanggal', 'required');
		$this->form_validation->set_rules('jumlah_jp', 'Jumlah JP', 'required');
		$this->form_validation->set_rules('keterangan', 'keterangan', 'required');
		$data['id_user'] = $this->session->userdata('id');

		if ($this->form_validation->run() == FALSE)
		{
			$this->session->set_flashdata('status', '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.validation_errors().'</div>');
				redirect('narasumber/the_rencana_kegiatan',$data);
		}else{
			$data = $this->model_rencana_kegiatan->addRencanaKegiatan();
			if($data){
				//$data['status'] = "success";
				$this->session->set_flashdata('status', '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Gagal Insert Data, Coba Beberapa Saat Lagi</div>');
				redirect('narasumber/the_rencana_kegiatan',$data);
			}else{
				//$data['status'] = "failed";
				$this->session->set_flashdata('status', '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Insert Data Berhasil</div>');
				redirect('narasumber/the_rencana_kegiatan',$data);
			}
			//var_dump($data);
		}
    }

    public function edit_rencana_kegiatan($id_rencana)
	{
	    $data['username'] = $this->session->userdata('username');
		$data['role'] = $this->session->userdata('role');
		$data['id_user'] = $this->session->userdata('id');

		//$data['tema_pelatihan'] = $this->model_pelatihan->getAllTemaPelatihan();

		$data['notiftotalPending'] = $this->model_request_pelatihan->countNotifPendingRequest();
		$data['notiflisttotalPending'] = $this->model_request_pelatihan->getNotifPendingRequest();

		$data['notiftotalApproved'] = $this->model_request_pelatihan->countNotifApprovedRequest();
		$data['notiflisttotalApproved'] = $this->model_request_pelatihan->getNotifApprovedRequest();

		$data['notiftotalCanceled'] = $this->model_request_pelatihan->countNotifCanceledRequest();
		$data['notiflisttotalCanceled'] = $this->model_request_pelatihan->getNotifCanceledRequest();

		$data['editRencana'] = $this->model_rencana_kegiatan->getRencanaKegiatanEdit($id_rencana);

		$data['nama_narasumber'] = $this->model_narasumber->getNameNarasumberByID($this->session->userdata('username'));

		$this->load->view('layout_admin/header',$data);
		$this->load->view('narasumber/edit_rencana');
		$this->load->view('layout_admin/footer');
	}
	public function proses_edit_rencana()
	{
	    $this->load->model('model_rencana_kegiatan','',TRUE);
	    $this->model_rencana_kegiatan->editRencanaKegiatan();
	    redirect('narasumber','refresh');
	}

    public function proses_hapus($id_rencana)
	{
	    //$this->load->model('model_movie','',TRUE);    
	    $this->model_rencana_kegiatan->deleteRencanaKegiatan($id_rencana);    
	    redirect('narasumber','refresh');
	}

	public function profil($id)
	{
		$data['username'] = $this->session->userdata('username');
		$data['role'] = $this->session->userdata('role');
		$data['id_user'] = $this->session->userdata('id');

		$data['tema_pelatihan'] = $this->model_pelatihan->getAllTemaPelatihan();

		$data['notiftotalPending'] = $this->model_request_pelatihan->countNotifPendingRequest();
		$data['notiflisttotalPending'] = $this->model_request_pelatihan->getNotifPendingRequest();

		$data['notiftotalApproved'] = $this->model_request_pelatihan->countNotifApprovedRequest();
		$data['notiflisttotalApproved'] = $this->model_request_pelatihan->getNotifApprovedRequest();

		$data['notiftotalCanceled'] = $this->model_request_pelatihan->countNotifCanceledRequest();
		$data['notiflisttotalCanceled'] = $this->model_request_pelatihan->getNotifCanceledRequest();

		$data['id'] = $id;

		$data['detail'] = $this->model_narasumber->getDetailNarasumber($id);
		$data['nama_narasumber'] = $this->model_narasumber->getNameNarasumberByID($this->session->userdata('username'));
		
		//update status read jadi 1 di table request pelatihan
		//$this->model_request_pelatihan->setRead(1,$id);

		$this->load->view('layout_admin/header',$data);
		$this->load->view('narasumber/detail_narasumber',$data);
		$this->load->view('layout_admin/footer');
	}
	public function update_data_narasumber()
    {
    	$this->form_validation->set_rules('nama_narasumber', 'nama_narasumber', 'required');
		$this->form_validation->set_rules('alamat_narasumber', 'alamat_narasumber', 'required');
		$this->form_validation->set_rules('no_telp_narasumber', 'no_telp_narasumber', 'required');
		$this->form_validation->set_rules('username', 'username', 'required');
        $this->form_validation->set_rules('tempat_lahir_narasumber', 'tempat_lahir_narasumber', 'required');
        $this->form_validation->set_rules('tgl_lahir_narasumber', 'tgl_lahir_narasumber', 'required');
        $this->form_validation->set_rules('nip_narasumber', 'nip_narasumber', 'required');
        $this->form_validation->set_rules('gelar_narasumber', 'gelar_narasumber', 'required');
        $this->form_validation->set_rules('golongan_narasumber', 'golongan_narasumber', 'required');
        $this->form_validation->set_rules('jenis_kelamin_narasumber', 'jenis_kelamin_narasumber', 'required');
        $this->form_validation->set_rules('pendidikan_narasumber', 'pendidikan_narasumber', 'required');
        $this->form_validation->set_rules('instansi_narasumber', 'instansi_narasumber', 'required');
        $this->form_validation->set_rules('pangkat_narasumber', 'pangkat_narasumber', 'required');
        $this->form_validation->set_rules('satuan_kerja_narasumber', 'satuan_kerja_narasumber', 'required');
        $this->form_validation->set_rules('status_pegawai', 'status_pegawai', 'required');
        $this->form_validation->set_rules('provinsi', 'provinsi', 'required');
        $this->form_validation->set_rules('kota_narasumber', 'kota_narasumber', 'required');
        $this->form_validation->set_rules('email_narasumber', 'email_narasumber', 'required');
        /*$this->form_validation->set_rules('no_sertifikat_narasumber', 'no_sertifikat_narasumber', 'required');*/
        $this->form_validation->set_rules('hasil_evaluasi', 'hasil_evaluasi', 'required');

		$id_narasumber = $this->input->post('id_narasumber');

		$from = $this->input->post('from');

		//$data = $this->model_narasumber->update_data_narasumber();

		/*if ($this->form_validation->run() == FALSE)
		{
			$this->session->set_flashdata('status', '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.validation_errors().'</div>');
				redirect('narasumber/profil/'.$id_narasumber,$data);
				
		}else{*/
			$data = $this->model_narasumber->update_data_narasumber();
			if($data == "success"){
				//$data['status'] = "success";
				$this->session->set_flashdata('status', '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Data & Photo Berhasil di Update</div>');
				redirect('narasumber/profil/'.$id_narasumber,$data);
			}else if($data == "not_upload"){
				//$data['status'] = "success";
				$this->session->set_flashdata('status', '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Data Berhasil di Update</div>');
				redirect('narasumber/profil/'.$id_narasumber,$data);
			}else{
				//echo $data;
			}
		//}
    }
    public function arsip_pelatihan()
    {
		$data['username'] = $this->session->userdata('username');
		$data['role'] = $this->session->userdata('role');
		$id = $this->session->userdata('id');
		$data['id_user'] = $this->session->userdata('id');
		$data['detail'] = $this->model_narasumber->getDetailNarasumber($this->session->userdata('id'));

		$data['notiftotalPending'] = $this->model_narasumber->countNotifPendingRequest($this->session->userdata('id'));
		$data['notiflisttotalPending'] = $this->model_narasumber->getNotifPendingRequest($this->session->userdata('id'));

		$data['notiftotalApproved'] = $this->model_narasumber->countNotifApprovedRequest();
		$data['notiflisttotalApproved'] = $this->model_narasumber->getNotifApprovedRequest();

		$data['notiftotalCanceled'] = $this->model_narasumber->countNotifCanceledRequest();
		$data['notiflisttotalCanceled'] = $this->model_narasumber->getNotifCanceledRequest();

		$data['all_request'] = $this->model_narasumber->getAllRequestPelatihanById($id);

		$data['nama_narasumber'] = $this->model_narasumber->getNameNarasumberByID($this->session->userdata('username'));

		$data['tema_pelatihan'] = $this->model_pelatihan->getAllTemaPelatihan();
		$data['menu'] = "arsip";
		$data['semua_pelatihan'] = $this->model_narasumber->getArsipPelatihanNarasumber($id);

		$this->load->view('layout_admin/header',$data);
		$this->load->view('narasumber/arsip_schedule',$data);
		$this->load->view('layout_admin/footer');
    }
    public function ganti_password()
    {
    	$data['username'] = $this->session->userdata('username');
		$data['role'] = $this->session->userdata('role');
		$data['id_user'] = $this->session->userdata('id');
		$id = $this->session->userdata('id');

		$data['tema_pelatihan'] = $this->model_pelatihan->getAllTemaPelatihan();

		$data['notiftotalPending'] = $this->model_request_pelatihan->countNotifPendingRequest();
		$data['notiflisttotalPending'] = $this->model_request_pelatihan->getNotifPendingRequest();

		$data['notiftotalApproved'] = $this->model_request_pelatihan->countNotifApprovedRequest();
		$data['notiflisttotalApproved'] = $this->model_request_pelatihan->getNotifApprovedRequest();

		$data['notiftotalCanceled'] = $this->model_request_pelatihan->countNotifCanceledRequest();
		$data['notiflisttotalCanceled'] = $this->model_request_pelatihan->getNotifCanceledRequest();

		$data['id'] = $id;

		$data['detail'] = $this->model_narasumber->getDetailNarasumber($id);
		$data['nama_narasumber'] = $this->model_narasumber->getNameNarasumberByID($this->session->userdata('username'));
		
		//update status read jadi 1 di table request pelatihan
		//$this->model_request_pelatihan->setRead(1,$id);

		$this->load->view('layout_admin/header',$data);
		$this->load->view('narasumber/ganti_password',$data);
		$this->load->view('layout_admin/footer');
    }

    public function ganti_password_narasumber()
    {
    	$id_narasumber = $this->session->userdata('id');

    	$password_lama = $this->input->post('password_lama');
    	$password_baru = $this->input->post('password_baru');
    	$password_konfirmasi = $this->input->post('password_konfirmasi');

        $this->form_validation->set_rules('password_lama', 'password_lama', 'required');
        $this->form_validation->set_rules('password_baru', 'password_baru', 'required');
        $this->form_validation->set_rules('password_konfirmasi', 'password_konfirmasi', 'required');

    	if ($this->form_validation->run() == FALSE)
		{
			$this->session->set_flashdata('status', '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.validation_errors().'</div>');
				redirect('narasumber/ganti_password',$data);
				
		}else{
	    	if(base64_encode(md5($password_lama))!=$this->model_narasumber->getPasswordLama($id_narasumber)){
	    		$this->session->set_flashdata('status', '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>password sebelumnya salah</div>');
						redirect('narasumber/ganti_password',$data);
	    	}else{
	    		if($password_baru!=$password_konfirmasi){
	    			$this->session->set_flashdata('status', '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>password baru tidak sesuai</div>');
	    			redirect('narasumber/ganti_password',$data);
	    		}else{
	    			$pass = base64_encode(md5($password_konfirmasi));
	    			$proses = $this->db->query("UPDATE 
	    				tbl_narasumber_new SET 
			            password = '$pass'
			            WHERE
			            id_narasumber = '$id_narasumber'
		            ");
	    			$this->session->set_flashdata('status', '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>password berhasil diubah</div>');
	    			redirect('narasumber/ganti_password',$data);
	    		}
	    	}
	    }
    }
}

