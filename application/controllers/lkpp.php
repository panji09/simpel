<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Lkpp extends CI_Controller {

	protected $where='';
    protected $search_data='';
    protected $value='';
    protected $sql='';
    private $conditionQuery="";

    private $queryfindPelatihan;

	public function __construct()
    {
		parent::__construct();
		$this->load->model('model_pelatihan');
		$this->load->model('model_lpp');
		$this->load->model('model_narasumber');
		$this->load->model('model_request_pelatihan');
		$this->load->model('model_rencana_kegiatan');
		$this->load->model('model_berita');
		$this->load->model('model_evaluasi');
		$this->load->model('model_peserta');
		$this->load->library("pagination");
		$this->load->helper(array('form', 'url'));
		$this->load->library('upload');
		$this->load->helper('tgl_indonesia');
		$this->load->library('email');
		$this->load->library('csvreader');
		$this->load->model('app_model');
        if( ($this->session->userdata('logged_in')!=1) || ($this->session->userdata('role')!='lkpp') )
        {
        	$this->app_model->logout();
            redirect('admin/login');
        }
        
        session_start();
        
        $_SESSION['KCFINDER'] = array(
	    'disabled' => false,
	    'access' => array(

	      'files' => array(
		  'upload' => true,
		  'delete' => true,
		  'copy'   => true,
		  'move'   => true,
		  'rename' => true
	      ),

	      'dirs' => array(
		  'create' => true,
		  'delete' => true,
		  'rename' => true
	      )
	  )
	);
    }
    public function index()
    {
    
		$data['notifAgendaBaru'] = $this->model_rencana_kegiatan->countAllAgenda('0');
		$data['notifScheduleBaru'] = $this->model_pelatihan->countScheduleToBeApproved();
		$data['notifMintaFasilitator'] = $this->model_pelatihan->countFasilitatoriPelatihan();
		$data['username'] = $this->session->userdata('username');
		$data['role'] = $this->session->userdata('role');

		$data['notifAllLPP'] = $this->main_model->count_row('tbl_lpp',array('approve'=>1));
		$data['notifAllNarasumber'] = $this->model_narasumber->countAllNarasumber();
		$data['notifLPPbaru'] = $this->model_lpp->countLPPBaru();
		$data['notifAgendaBaru'] = $this->model_rencana_kegiatan->countAllAgenda('0');
		$data['notifScheduleBaru'] = $this->model_pelatihan->countScheduleToBeApproved();
		$data['notifMintaFasilitator'] = $this->model_pelatihan->countFasilitatoriPelatihan();
		
		$data['notifLPPditolak'] = $this->db->query("SELECT * FROM tbl_lpp WHERE approve = 2")->num_rows();

		$limit = $this->input->post('limit');

		$config = array();
		$config["base_url"] = base_url() . "lkpp/klasemen/limit";
		//HITUNG JUMLAH ROW DI TABLE BERITA//
		$config["total_rows"] = $this->model_narasumber->record_count('tbl_narasumber_new');
		$config["per_page"] = 10;
		$config["uri_segment"] = 3;
		$this->pagination->initialize($config);
		$data["links"] = $this->pagination->create_links();

		if($this->uri->segment(3)!=null){
			$data['no_from_url'] = $this->uri->segment(3);
		}else{
			$data['no_from_url'] = 0;
		}

		$this->conditionQuery = $this->model_narasumber->getCondition();
		$data['conditionQuery'] = $this->conditionQuery;
		
		$data['narasumber'] = $this->model_narasumber->getNarasumber();
		$data['klasemen_narasumber'] = $this->model_narasumber->getKlasemenNarasumber(0, 1000);

		$data['tahun'] = date('Y');

		$this->load->view('layout_admin/header',$data);
		$this->load->view('lkpp/index');
		$this->load->view('layout_admin/footer');

		/*if($this->session->userdata('role')!='lkpp'){
			echo "haiah";
		}else{
			$this->session->userdata('role');
		}*/
    }
    
    public function file_explorer(){
	$data['username'] = $this->session->userdata('username');
	$data['role'] = $this->session->userdata('role');
	$data['id_user'] = $this->session->userdata('id');
	$id = $this->session->userdata('id');

	$data['username'] = $this->session->userdata('username');
	$data['role'] = $this->session->userdata('role');
	
	$data['dataFoto'] = $this->model_pelatihan->get_all_foto();
	$data['notifAgendaBaru'] = $this->model_rencana_kegiatan->countAllAgenda('0');
	$data['notifScheduleBaru'] = $this->model_pelatihan->countScheduleToBeApproved();
	$data['notifMintaFasilitator'] = $this->model_pelatihan->countFasilitatoriPelatihan();
	      
	$this->load->view('layout_admin/header',$data);
	$this->load->view('lkpp/file_explorer');
	$this->load->view('layout_admin/footer');

    }
    
    public function klasemen($page)
    {
		$data['notifAgendaBaru'] = $this->model_rencana_kegiatan->countAllAgenda('0');
		$data['notifScheduleBaru'] = $this->model_pelatihan->countScheduleToBeApproved();
		$data['notifMintaFasilitator'] = $this->model_pelatihan->countFasilitatoriPelatihan();
    	$data['username'] = $this->session->userdata('username');
		$data['role'] = $this->session->userdata('role');

		

		$limit = $this->input->post('limit');

		$config = array();
$config["base_url"] = base_url() . "lkpp/klasemen/limit";
//HITUNG JUMLAH ROW DI TABLE BERITA//
$config["total_rows"] = $this->model_narasumber->record_count('tbl_narasumber_new');
$config["per_page"] = 10;
$config["uri_segment"] = 4;
$this->pagination->initialize($config);
$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
$data["links"] = $this->pagination->create_links();

if($this->uri->segment(4)!=null){
	$data['no_from_url'] = $this->uri->segment(4);
    	}else{
    		$data['no_from_url'] = 0;
    	}
		$data['klasemen_narasumber'] = $this->model_narasumber->getKlasemenNarasumber($config["per_page"], $page);

		$this->load->view('layout_admin/header',$data);
		$this->load->view('lkpp/index');
		$this->load->view('layout_admin/footer');
    }
    public function detail_narasumber($id)
    {
		$data['notifAgendaBaru'] = $this->model_rencana_kegiatan->countAllAgenda('0');
		$data['notifScheduleBaru'] = $this->model_pelatihan->countScheduleToBeApproved();
		$data['notifMintaFasilitator'] = $this->model_pelatihan->countFasilitatoriPelatihan();
    	$data['username'] = $this->session->userdata('username');
		$data['role'] = $this->session->userdata('role');

		$data['tema_pelatihan'] = $this->model_pelatihan->getAllTemaPelatihan();

		$data['notiftotalPending'] = $this->model_request_pelatihan->countNotifPendingRequest();
		$data['notiflisttotalPending'] = $this->model_request_pelatihan->getNotifPendingRequest();

		$data['notiftotalApproved'] = $this->model_request_pelatihan->countNotifApprovedRequest();
		$data['notiflisttotalApproved'] = $this->model_request_pelatihan->getNotifApprovedRequest();

		$data['notiftotalCanceled'] = $this->model_request_pelatihan->countNotifCanceledRequest();
		$data['notiflisttotalCanceled'] = $this->model_request_pelatihan->getNotifCanceledRequest();

		$data['id'] = $id;

		$data['detail'] = $this->model_narasumber->getDetailNarasumber($id);
		
		//update status read jadi 1 di table request pelatihan
		//$this->model_request_pelatihan->setRead(1,$id);

		$this->load->view('layout_admin/header',$data);
		$this->load->view('lkpp/detail_narasumber',$data);
		$this->load->view('layout_admin/footer');
    }
    public function detail_lpp($id)
    {
		$data['notifAgendaBaru'] = $this->model_rencana_kegiatan->countAllAgenda('0');
		$data['notifScheduleBaru'] = $this->model_pelatihan->countScheduleToBeApproved();
		$data['notifMintaFasilitator'] = $this->model_pelatihan->countFasilitatoriPelatihan();
    	$data['username'] = $this->session->userdata('username');
		$data['role'] = $this->session->userdata('role');

		$data['tema_pelatihan'] = $this->model_pelatihan->getAllTemaPelatihan();

		$data['notiftotalPending'] = $this->model_request_pelatihan->countNotifPendingRequest();
		$data['notiflisttotalPending'] = $this->model_request_pelatihan->getNotifPendingRequest();

		$data['notiftotalApproved'] = $this->model_request_pelatihan->countNotifApprovedRequest();
		$data['notiflisttotalApproved'] = $this->model_request_pelatihan->getNotifApprovedRequest();

		$data['notiftotalCanceled'] = $this->model_request_pelatihan->countNotifCanceledRequest();
		$data['notiflisttotalCanceled'] = $this->model_request_pelatihan->getNotifCanceledRequest();

		$data['id'] = $id;

		$data['detail'] = $this->model_lpp->getDetailLpp($id);


		
		//update status read jadi 1 di table request pelatihan
		//$this->model_request_pelatihan->setRead(1,$id);

		$this->load->view('layout_admin/header',$data);
		$this->load->view('lkpp/detail_lpp',$data);
		$this->load->view('layout_admin/footer');
    }

    public function verifikasi($jenis,$param,$id)
    {
    	$data = $this->model_lpp->verifikasi($jenis,$param,$id);
    	if($data == "success"){
				//$data['status'] = "success";
				$this->session->set_flashdata('status', '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Verifikasi Berhasil</div>');
				redirect('lkpp/detail_lpp/'.$id,$data);
			}else{
				$this->session->set_flashdata('status', '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$data.'</div>');
				redirect('lkpp/detail_lpp/'.$id,$data);
				//var_dump($data);
				//echo $data['file_name'];
			}
    }

    public function delete_lpp($id)
    {
		$data['notifAgendaBaru'] = $this->model_rencana_kegiatan->countAllAgenda('0');
		$data['notifScheduleBaru'] = $this->model_pelatihan->countScheduleToBeApproved();
		$data['notifMintaFasilitator'] = $this->model_pelatihan->countFasilitatoriPelatihan();
    	$data = $this->model_lpp->delete('tbl_lpp','id_lpp',$id);
		if($data == "success"){
				//$data['status'] = "success";
				$this->session->set_flashdata('status', '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Data Berhasil di Hapus</div>');
				redirect('lkpp/lpp/all',$data);
			}else{
				$this->session->set_flashdata('status', '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$data.'</div>');
				redirect('lkpp/lpp/all',$data);
				//var_dump($data);
				//echo $data['file_name'];
			}

    }
    public function delete_pelatihan($id)
    {
		$data['notifAgendaBaru'] = $this->model_rencana_kegiatan->countAllAgenda('0');
		$data['notifScheduleBaru'] = $this->model_pelatihan->countScheduleToBeApproved();
		$data['notifMintaFasilitator'] = $this->model_pelatihan->countFasilitatoriPelatihan();
    	$data = $this->model_lpp->delete('tbl_pelatihan','id_pelatihan',$id);
		if($data == "success"){
				//$data['status'] = "success";
				$this->session->set_flashdata('status', '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Data Berhasil di Hapus</div>');
				redirect('lkpp/pelatihan',$data);
			}else{
				$this->session->set_flashdata('status', '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$data.'</div>');
				redirect('lkpp/pelatihan',$data);
				//var_dump($data);
				//echo $data['file_name'];
			}

    }
    public function delete_fasilitator($id)
    {
    	$data = $this->model_lpp->delete('tbl_fasilitator','id_fasilitator',$id);
		if($data == "success"){
				//$data['status'] = "success";
				$this->session->set_flashdata('status', '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Data Berhasil di Hapus</div>');
				redirect('lkpp/new_fasilitator',$data);
			}else{
				$this->session->set_flashdata('status', '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$data.'</div>');
				redirect('lkpp/new_fasilitator',$data);
				//var_dump($data);
				//echo $data['file_name'];
			}

    }
    public function delete_berita($id)
    {
		$data['notifAgendaBaru'] = $this->model_rencana_kegiatan->countAllAgenda('0');
		$data['notifScheduleBaru'] = $this->model_pelatihan->countScheduleToBeApproved();
		$data['notifMintaFasilitator'] = $this->model_pelatihan->countFasilitatoriPelatihan();
    	$data = $this->model_lpp->delete('tbl_berita','id_berita',$id);
		if($data == "success"){
				//$data['status'] = "success";
				$this->session->set_flashdata('status', '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Data Berhasil di Hapus</div>');
				redirect('lkpp/berita',$data);
			}else{
				$this->session->set_flashdata('status', '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$data.'</div>');
				redirect('lkpp/berita',$data);
				//var_dump($data);
				//echo $data['file_name'];
			}    
	}
	public function proses_edit_berita($id)
	{
		
			$data = $this->model_berita->update_berita($id);
			if($data == "success"){
				//$data['status'] = "success";
				$this->session->set_flashdata('status', '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Tema Pelatihan berhasil ditambahkan</div>');
				redirect('lkpp/berita',$data);
			}else{
				$this->session->set_flashdata('status', '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$data.'</div>');
				redirect('lkpp/berita',$data);
				//var_dump($data);
				//echo $data['file_name'];
			}
	}
    public function delete_narasumber($id)
    {
		$data['notifAgendaBaru'] = $this->model_rencana_kegiatan->countAllAgenda('0');
		$data['notifScheduleBaru'] = $this->model_pelatihan->countScheduleToBeApproved();
		$data['notifMintaFasilitator'] = $this->model_pelatihan->countFasilitatoriPelatihan();
    	$data = $this->model_lpp->delete('tbl_narasumber_new','id_narasumber',$id);
		if($data == "success"){
				//$data['status'] = "success";
				$this->session->set_flashdata('status', '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Data Berhasil di Hapus</div>');
				redirect('lkpp/narasumber',$data);
			}else{
				$this->session->set_flashdata('status', '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$data.'</div>');
				redirect('lkpp/narasumber',$data);
				//var_dump($data);
				//echo $data['file_name'];
			}

    }

    public function update_data_narasumber()
    {
		$data['notifAgendaBaru'] = $this->model_rencana_kegiatan->countAllAgenda('0');
		$data['notifScheduleBaru'] = $this->model_pelatihan->countScheduleToBeApproved();
		$data['notifMintaFasilitator'] = $this->model_pelatihan->countFasilitatoriPelatihan();
    	//$this->form_validation->set_rules('nama_narasumber', 'nama_narasumber', 'required');
		//$this->form_validation->set_rules('alamat_narasumber', 'alamat_narasumber', 'required');
		/*
		$this->form_validation->set_rules('no_telp_narasumber', 'no_telp_narasumber', 'required');*/
		//$this->form_validation->set_rules('username', 'username', 'required');
		/*
		$this->form_validation->set_rules('tempat_lahir_narasumber', 'tempat_lahir_narasumber', 'required');
		$this->form_validation->set_rules('tgl_lahir_narasumber', 'tgl_lahir_narasumber', 'required');
		$this->form_validation->set_rules('nip_narasumber', 'nip_narasumber', 'required');
		$this->form_validation->set_rules('gelar_narasumber', 'gelar_narasumber', 'required');
		$this->form_validation->set_rules('golongan_narasumber', 'golongan_narasumber', 'required');
		$this->form_validation->set_rules('jenis_kelamin_narasumber', 'jenis_kelamin_narasumber', 'required');
		$this->form_validation->set_rules('pendidikan_narasumber', 'pendidikan_narasumber', 'required');
		$this->form_validation->set_rules('instansi_narasumber', 'instansi_narasumber', 'required');
		$this->form_validation->set_rules('pangkat_narasumber', 'pangkat_narasumber', 'required');
		$this->form_validation->set_rules('satuan_kerja_narasumber', 'satuan_kerja_narasumber', 'required');
		$this->form_validation->set_rules('status_pegawai', 'status_pegawai', 'required');
		$this->form_validation->set_rules('provinsi', 'provinsi', 'required');
		$this->form_validation->set_rules('kota_narasumber', 'kota_narasumber', 'required');
		$this->form_validation->set_rules('email_narasumber', 'email_narasumber', 'required');
		$this->form_validation->set_rules('no_sertifikat_narasumber', 'no_sertifikat_narasumber', 'required');
		$this->form_validation->set_rules('hasil_evaluasi', 'hasil_evaluasi', 'required');*/

		$id_narasumber = $this->input->post('id_narasumber');

		$from = $this->input->post('from');

		//$data = $this->model_narasumber->update_data_narasumber();

		/*if ($this->form_validation->run() == FALSE)
		{
			$this->session->set_flashdata('status', '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.validation_errors().'</div>');
				redirect('lkpp/detail_narasumber/'.$id_narasumber,$data);
				redirect('lkpp/detail_narasumber/'.$id_narasumber,$data);
				
		}else{*/
			$data = $this->model_narasumber->update_data_narasumber_lkpp($id_narasumber);
			if($data == "success"){
				//$data['status'] = "success";
				$this->session->set_flashdata('status', '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Data & Photo Berhasil di Update</div>');
				redirect('lkpp/detail_narasumber/'.$id_narasumber,$data);
				redirect('lkpp/detail_narasumber/'.$id_narasumber,$data);
			}else if($data == "not_upload"){
				//$data['status'] = "success";
				$this->session->set_flashdata('status', '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Data Berhasil di Update</div>');
				redirect('lkpp/detail_narasumber/'.$id_narasumber,$data);
				redirect('lkpp/detail_narasumber/'.$id_narasumber,$data);
			}else{
				//echo $data;
			}
			echo "<script>window.location.reload(true); </script>";
		//}
    }
    public function detail_request($id)
    {
		$data['notifAgendaBaru'] = $this->model_rencana_kegiatan->countAllAgenda('0');
		$data['notifScheduleBaru'] = $this->model_pelatihan->countScheduleToBeApproved();
		$data['notifMintaFasilitator'] = $this->model_pelatihan->countFasilitatoriPelatihan();
    	$data['username'] = $this->session->userdata('username');
		$data['role'] = $this->session->userdata('role');

		$data['id_request'] = $id;

		$data['notiftotalPending'] = $this->model_request_pelatihan->countNotifPendingRequest();
		$data['notiflisttotalPending'] = $this->model_request_pelatihan->getNotifPendingRequest();

		$data['detail'] = $this->model_request_pelatihan->getDetailRequestById($id);

		$this->load->view('layout_admin/header',$data);
		$this->load->view('lkpp/detail_request');
		$this->load->view('layout_admin/footer');
    }
    public function pelatihan()
    {
		$data['notifAgendaBaru'] = $this->model_rencana_kegiatan->countAllAgenda('0');
		$data['notifScheduleBaru'] = $this->model_pelatihan->countScheduleToBeApproved();
		$data['notifMintaFasilitator'] = $this->model_pelatihan->countFasilitatoriPelatihan();
    	$data['username'] = $this->session->userdata('username');
		$data['role'] = $this->session->userdata('role');

		$data['notiftotalPending'] = $this->model_request_pelatihan->countNotifPendingRequest();
		$data['notiflisttotalPending'] = $this->model_request_pelatihan->getNotifPendingRequest();

		$data['semua_lpp'] = $this->model_lpp->getAllLpp();
		$data['pelatihan'] = $this->model_pelatihan->getAllPelatihan();
		$data['tema_pelatihan'] = $this->model_pelatihan->getAllTemaPelatihan();

		$data['berlangsung'] = "berlangsung";

		$data['dari_tanggal'] = "";
        $data['sampai_tanggal'] = "";
        $data['id_lpp'] = "";
        $data['id_pelatihan'] = "";
        $data['provinsi'] = "";

		$data['report'] = "no";



    	$this->load->view('layout_admin/header',$data);
		$this->load->view('lkpp/pelatihan');
		$this->load->view('layout_admin/footer');
    }
    public function find_pelatihan()
    {
		$data['notifAgendaBaru'] = $this->model_rencana_kegiatan->countAllAgenda('0');
		$data['notifScheduleBaru'] = $this->model_pelatihan->countScheduleToBeApproved();
		$data['notifMintaFasilitator'] = $this->model_pelatihan->countFasilitatoriPelatihan();
    	$data['username'] = $this->session->userdata('username');
		$data['role'] = $this->session->userdata('role');

		$data['notiftotalPending'] = $this->model_request_pelatihan->countNotifPendingRequest();
		$data['notiflisttotalPending'] = $this->model_request_pelatihan->getNotifPendingRequest();

		$data['semua_lpp'] = $this->model_lpp->getAllLpp();
		$data['pelatihan'] = $this->model_pelatihan->findPelatihan();
		$data['tema_pelatihan'] = $this->model_pelatihan->getAllTemaPelatihan();


		$data['dari_tanggal'] = $this->input->post('dari_tanggal');
        $data['sampai_tanggal'] = $this->input->post('sampai_tanggal');
        $data['id_lpp'] = $this->input->post('lpp');
        $data['id_pelatihan'] = $this->input->post('pelatihan');
        $data['provinsi'] = $this->input->post('provinsi');

		$data['report'] = "yes";

    	$this->load->view('layout_admin/header',$data);
		$this->load->view('lkpp/pelatihan');
		$this->load->view('layout_admin/footer');
    }
    public function arsip_pelatihan()
    {
		$data['notifAgendaBaru'] = $this->model_rencana_kegiatan->countAllAgenda('0');
		$data['notifScheduleBaru'] = $this->model_pelatihan->countScheduleToBeApproved();
		$data['notifMintaFasilitator'] = $this->model_pelatihan->countFasilitatoriPelatihan();
    	$data['username'] = $this->session->userdata('username');
		$data['role'] = $this->session->userdata('role');

		$data['notiftotalPending'] = $this->model_request_pelatihan->countNotifPendingRequest();
		$data['notiflisttotalPending'] = $this->model_request_pelatihan->getNotifPendingRequest();

		$data['dari_tanggal'] = "";
        $data['sampai_tanggal'] = "";
        $data['id_lpp'] = "";
        $data['id_pelatihan'] = "";
        $data['provinsi'] = "";

		$data['report'] = "no";
		
		$data['arsip'] = "arsip";
		$data['pelatihan'] = $this->model_pelatihan->getArsipPelatihan();

    	$this->load->view('layout_admin/header',$data);
		$this->load->view('lkpp/pelatihan');
		$this->load->view('layout_admin/footer');
    }
    public function pelatihan_ditolak()
    {
		$data['notifAgendaBaru'] = $this->model_rencana_kegiatan->countAllAgenda('0');
		$data['notifScheduleBaru'] = $this->model_pelatihan->countScheduleToBeApproved();
		$data['notifMintaFasilitator'] = $this->model_pelatihan->countFasilitatoriPelatihan();
    	$data['username'] = $this->session->userdata('username');
		$data['role'] = $this->session->userdata('role');

		$data['notiftotalPending'] = $this->model_request_pelatihan->countNotifPendingRequest();
		$data['notiflisttotalPending'] = $this->model_request_pelatihan->getNotifPendingRequest();

		$data['dari_tanggal'] = "";
        $data['sampai_tanggal'] = "";
        $data['id_lpp'] = "";
        $data['id_pelatihan'] = "";
        $data['provinsi'] = "";

		$data['report'] = "no";

		$data['ditolak'] = "ditolak";
		$data['pelatihan'] = $this->model_pelatihan->getPelatihanDitolak();

    	$this->load->view('layout_admin/header',$data);
		$this->load->view('lkpp/pelatihan');
		$this->load->view('layout_admin/footer');
    }
    public function tambah_pelatihan()
    {
		$data['notifAgendaBaru'] = $this->model_rencana_kegiatan->countAllAgenda('0');
		$data['notifScheduleBaru'] = $this->model_pelatihan->countScheduleToBeApproved();
		$data['notifMintaFasilitator'] = $this->model_pelatihan->countFasilitatoriPelatihan();
    	$data['username'] = $this->session->userdata('username');
		$data['role'] = $this->session->userdata('role');

		$data['notiftotalPending'] = $this->model_request_pelatihan->countNotifPendingRequest();
		$data['notiflisttotalPending'] = $this->model_request_pelatihan->getNotifPendingRequest();

		$data['tema_pelatihan'] = $this->model_pelatihan->getAllTemaPelatihan();

		$data['lpp'] = $this->model_lpp->getAllLpp();

    	$this->load->view('layout_admin/header',$data);
		$this->load->view('lkpp/tambah_pelatihan');
		$this->load->view('layout_admin/footer');
    }
    public function tambah_fasilitator()
    {
		$data['notifAgendaBaru'] = $this->model_rencana_kegiatan->countAllAgenda('0');
		$data['notifScheduleBaru'] = $this->model_pelatihan->countScheduleToBeApproved();
		$data['notifMintaFasilitator'] = $this->model_pelatihan->countFasilitatoriPelatihan();
    	$data['username'] = $this->session->userdata('username');
		$data['role'] = $this->session->userdata('role');

		$data['notiftotalPending'] = $this->model_request_pelatihan->countNotifPendingRequest();
		$data['notiflisttotalPending'] = $this->model_request_pelatihan->getNotifPendingRequest();

		$data['tema_pelatihan'] = $this->model_pelatihan->getAllTemaPelatihan();

		$data['lpp'] = $this->model_lpp->getAllLpp();

    	$this->load->view('layout_admin/header',$data);
		$this->load->view('lkpp/tambah_fasilitator');
		$this->load->view('layout_admin/footer');
    }
    public function hubungkan($id_pelatihan)
    {
		$data['notifAgendaBaru'] = $this->model_rencana_kegiatan->countAllAgenda('0');
		$data['notifScheduleBaru'] = $this->model_pelatihan->countScheduleToBeApproved();
		$data['notifMintaFasilitator'] = $this->model_pelatihan->countFasilitatoriPelatihan();
    	$data['username'] = $this->session->userdata('username');
		$data['role'] = $this->session->userdata('role');

		$data['notiftotalPending'] = $this->model_request_pelatihan->countNotifPendingRequest();
		$data['notiflisttotalPending'] = $this->model_request_pelatihan->getNotifPendingRequest();

		$data['pelatihan'] = $this->model_pelatihan->getPelatihanNarasumberById($id_pelatihan);

		$data['narasumber'] = $this->model_narasumber->getAbsolutelyAllNarasumber();

    	$this->load->view('layout_admin/header',$data);
		$this->load->view('lkpp/hubungkan');
		$this->load->view('layout_admin/footer');
    }
    public function insertPelatihan()
	{
		$action = $this->input->post('submit');
		$id_request = $this->input->post('id_request');
		$keterangan_ditolak = $this->input->post('keterangan_ditolak');
		$data['notifAgendaBaru'] = $this->model_rencana_kegiatan->countAllAgenda('0');
		$data['notifScheduleBaru'] = $this->model_pelatihan->countScheduleToBeApproved();
		$data['notifMintaFasilitator'] = $this->model_pelatihan->countFasilitatoriPelatihan();

		if($action=="submit"){
			$this->form_validation->set_rules('isi_pelatihan', 'isi_pelatihan', 'required');
			$this->form_validation->set_rules('tempat_pelatihan', 'tempat_pelatihan', 'required');

			if ($this->form_validation->run() == FALSE)
			{
				echo validation_errors();
				//redirect('satker');
			}else{
				$data = $this->model_pelatihan->addNewPelatihan();
				if($data){
					//$data['status'] = "success";
					$this->session->set_flashdata('status', '<div class="alert alert-success">Pelatihan Berhasil Dibuat</div>');
					redirect('lkpp/pelatihan',$data);
				}else{
					//$data['status'] = "failed";
					$this->session->set_flashdata('status', '<div class="alert alert-danger">gagal memasukan data, coba beberapa saat lagi</div>');
					redirect('lkpp/pelatihan',$data);
				}
			}
		}else{
			$data = $this->model_pelatihan->cancelPelatihan($id_request,$keterangan_ditolak);
				if($data){
					//$data['status'] = "success";
					$this->session->set_flashdata('status', '<div class="alert alert-success">Pelatihan Ditolak</div>');
					redirect('lkpp/pelatihan',$data);
				}else{
					//$data['status'] = "failed";
					$this->session->set_flashdata('status', '<div class="alert alert-danger">gagal memasukan data, coba beberapa saat lagi</div>');
					redirect('lkpp/pelatihan',$data);
				}
		}
		
		
	}
	public function addNarasumberToPelatihan()
	{
		$dari_jam = $this->input->post('dari_jam');
		$sampai_jam = $this->input->post('sampai_jam');
		$narasumber = $this->input->post('narasumber');

		echo $dari_jam."-".$sampai_jam." oleh ".$narasumber;
	}
	public function proses_request_pelatihan()
	{
		$data = $this->model_request_pelatihan->approveRequest();
			if($data=="success_approve"){
				//$data['status'] = "success";
				$this->session->set_flashdata('status', '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Request telah diterima, pelatihan berhasil di buat</div>');
				redirect('lkpp/index',$data);
			}else if($data=="success_tolak"){
				//$data['status'] = "success";
				$this->session->set_flashdata('status', '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Request telah ditolak, pelatihan berhasil ditolak</div>');
				redirect('lkpp/index',$data);
			}else{
				//$data['status'] = "failed";
				$this->session->set_flashdata('status', '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>gagal memasukan data, coba beberapa saat lagi</div>');
				redirect('lkpp/index',$data);
			}
	}
	

	public function lpp($param)
	{
		$data['username'] = $this->session->userdata('username');
		$data['role'] = $this->session->userdata('role');
		$data['notifAgendaBaru'] = $this->model_rencana_kegiatan->countAllAgenda('0');
		$data['notifScheduleBaru'] = $this->model_pelatihan->countScheduleToBeApproved();
		$data['notifMintaFasilitator'] = $this->model_pelatihan->countFasilitatoriPelatihan();
		$data['param'] = $param;

		$data['akreditasi'] = $this->main_model->get_query('SELECT hasil_akreditasi FROM tbl_lpp GROUP BY hasil_akreditasi');

		if($param=="all"){
			$search = $this->input->post('search');
			$hasil_akreditasi = $this->input->post('hasil_akreditasi');
			$query = "SELECT * FROM tbl_lpp WHERE nama_lpp LIKE '%$search%'";
			if($hasil_akreditasi=="Terdaftar"){
					$query .= "AND hasil_akreditasi=''";
			}else if($hasil_akreditasi=="all"){
					$query .= "";
			}else{
				$query .= "AND hasil_akreditasi='$hasil_akreditasi'";
			}

			$data['semua_lpp'] = $this->main_model->get_query($query);
		}else if($param=="tolak"){
			$data['semua_lpp'] = $this->main_model->get_list_where('tbl_lpp',array('approve'=>2));
		}else{
			$data['param'] = "baru";
			$data['semua_lpp'] = $this->model_lpp->getLppBaru();
		}
		

		$this->load->view('layout_admin/header',$data);
		$this->load->view('lkpp/lpp_all',$data);
		$this->load->view('layout_admin/footer');
	}
	public function hapus_lpp($param,$id)
	{
		$query = $this->main_model->delete('tbl_lpp','id_lpp',$id);
		redirect('lkpp/lpp/'.$param,$data);
	}

	public function tambah_lpp()
	{
		$data['username'] = $this->session->userdata('username');
		$data['role'] = $this->session->userdata('role');
		$data['notifAgendaBaru'] = $this->model_rencana_kegiatan->countAllAgenda('0');
		$data['notifScheduleBaru'] = $this->model_pelatihan->countScheduleToBeApproved();
		$data['notifMintaFasilitator'] = $this->model_pelatihan->countFasilitatoriPelatihan();

		

		$this->load->view('layout_admin/header',$data);
		$this->load->view('lkpp/tambah_lpp');
		$this->load->view('layout_admin/footer');
	}
	public function proses_tambah_lpp()
	{
		$this->form_validation->set_rules('nama_lpp', 'nama_lpp', 'required');
		$this->form_validation->set_rules('alamat_lpp', 'alamat_lpp', 'required');
		$this->form_validation->set_rules('no_telp_lpp', 'no_telp_lpp', 'required');
		$this->form_validation->set_rules('username', 'username', 'required');
		$this->form_validation->set_rules('password', 'password', 'required');
		$this->form_validation->set_rules('no_akreditasi_lpp', 'no_akreditasi_lpp', 'required');
		$this->form_validation->set_rules('hasil_akreditasi', 'hasil_akreditasi', 'required');
		$this->form_validation->set_rules('satker', 'satker', 'required');
		$this->form_validation->set_rules('file_akreditasi', 'file_akreditasi', 'required');

		$from = $this->input->post('from');

		if ($this->form_validation->run() == FALSE)
		{
			$this->session->set_flashdata('status', '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.validation_errors().'</div>');
				if($from=="lkpp"){
					redirect('lkpp/tambah_lpp',$data);
				}else if($from=="home"){
					redirect('lpp/daftar',$data);
				}
				
		}else{
			$data = $this->model_lpp->insert_lpp();
			if($data){
				//$data['status'] = "success";
				$this->session->set_flashdata('status', '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Pendaftaran LPP baru berhasil (belum diverifikasi)</div>');
				if($from=="lkpp"){
					redirect('lkpp/tambah_lpp',$data);
				}else if($from=="home"){
					redirect('lpp/daftar',$data);
				}
			}
		}
	}
	public function aktifkan_lpp($id)
	{
		$data = $this->model_lpp->aktifkan_lpp($id);
			if($data){
				//$data['status'] = "success";
				$this->session->set_flashdata('status', '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Data & Photo Berhasil di Update</div>');
				redirect('lkpp/lpp/all',$data);
			}else{
				$this->session->set_flashdata('status', '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Gagal, coba lagi</div>');
				redirect('lkpp/lpp/all',$data);
			}
	}
	public function proses_update_lpp()
    {
		$data['notifAgendaBaru'] = $this->model_rencana_kegiatan->countAllAgenda('0');
		$data['notifScheduleBaru'] = $this->model_pelatihan->countScheduleToBeApproved();
		$data['notifMintaFasilitator'] = $this->model_pelatihan->countFasilitatoriPelatihan();
    	/*$this->form_validation->set_rules('nama_lpp', 'nama_lpp', 'required');
		$this->form_validation->set_rules('alamat_lpp', 'alamat_lpp', 'required');
		$this->form_validation->set_rules('no_telp_lpp', 'no_telp_lpp', 'required');
		$this->form_validation->set_rules('username', 'username', 'required');
		$this->form_validation->set_rules('password', 'password', 'required');
		$this->form_validation->set_rules('no_akreditasi_lpp', 'no_akreditasi_lpp', 'required');
		$this->form_validation->set_rules('hasil_akreditasi', 'hasil_akreditasi', 'required');*/

		$id_lpp = $this->input->post('id_lpp');

		$from = $this->input->post('from');
		$data = $this->model_lpp->update_data_lpp_lkpp();
			if($data == "success"){
				//$data['status'] = "success";
				$this->session->set_flashdata('status', '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Data & Photo Berhasil di Update</div>');
				redirect('lkpp/detail_lpp/'.$id_lpp,$data);
			}else if($data == "not_upload"){
				//$data['status'] = "success";
				$this->session->set_flashdata('status', '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Data Berhasil di Update</div>');
				redirect('lkpp/detail_lpp/'.$id_lpp,$data);
			}else{
				//echo $data;
			}

		//$data = $this->model_narasumber->update_data_narasumber();

		/*if ($this->form_validation->run() == FALSE)
		{
			$this->session->set_flashdata('status', '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.validation_errors().'</div>');
				redirect('lkpp/detail_lpp/'.$id_lpp,$data);
				
		}else{
			
		}*/
    }


	public function narasumber()
	{
		$data['notifAgendaBaru'] = $this->model_rencana_kegiatan->countAllAgenda('0');
		$data['notifScheduleBaru'] = $this->model_pelatihan->countScheduleToBeApproved();
		$data['notifMintaFasilitator'] = $this->model_pelatihan->countFasilitatoriPelatihan();
		$data['username'] = $this->session->userdata('username');
		$data['role'] = $this->session->userdata('role');

		$data['tema_pelatihan'] = $this->model_pelatihan->getAllTemaPelatihan();

		$data['notiftotalPending'] = $this->model_request_pelatihan->countNotifPendingRequest();
		$data['notiflisttotalPending'] = $this->model_request_pelatihan->getNotifPendingRequest();

		$data['getAllRequest'] = $this->model_request_pelatihan->getAllRequest();
		$data['notifAllRequest'] = $this->model_request_pelatihan->countAllRequest();

		$data['notifAllLPP'] = $this->model_lpp->countAllLpp();

		/*$config = array();
$config["base_url"] = base_url() . "lkpp/narasumber/page";
//HITUNG JUMLAH ROW DI TABLE BERITA//
$config["total_rows"] = $this->model_narasumber->record_count('tbl_narasumber_new');
$config["per_page"] = 10;
$config["uri_segment"] = 4;
$this->pagination->initialize($config);
$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
$data["links"] = $this->pagination->create_links();
$data['page'] = "narasumber";
if($this->uri->segment(4)!=null){
	$data['no_from_url'] = $this->uri->segment(4);
    	}else{
    		$data['no_from_url'] = 1;
    	}*/
    	if($this->uri->segment(4)!=null){
	$data['no_from_url'] = $this->uri->segment(4);
    	}else{
    		$data['no_from_url'] = 1;
    	}
    	
		$data['notifAllNarasumber'] = $this->model_narasumber->countAllNarasumber();
		//$data['semua_narasumber'] = $this->model_narasumber->getAllNarasumber($config["per_page"], $page);
		$data['semua_narasumber'] = $this->model_narasumber->getAbsolutelyAllNarasumber();
		$data['narasumber'] = $this->model_narasumber->getNarasumber();

		$this->load->view('layout_admin/header',$data);
		$this->load->view('lkpp/narasumber_all',$data);
		$this->load->view('layout_admin/footer');
	}
	public function findDataNarasumber()
	{
		$data['username'] = $this->session->userdata('username');
		$data['role'] = $this->session->userdata('role');
		$data['notifAgendaBaru'] = $this->model_rencana_kegiatan->countAllAgenda('0');
		$data['notifScheduleBaru'] = $this->model_pelatihan->countScheduleToBeApproved();
		$data['notifMintaFasilitator'] = $this->model_pelatihan->countFasilitatoriPelatihan();

		$data['tema_pelatihan'] = $this->model_pelatihan->getAllTemaPelatihan();

		$data['notiftotalPending'] = $this->model_request_pelatihan->countNotifPendingRequest();
		$data['notiflisttotalPending'] = $this->model_request_pelatihan->getNotifPendingRequest();

		$data['getAllRequest'] = $this->model_request_pelatihan->getAllRequest();
		$data['notifAllRequest'] = $this->model_request_pelatihan->countAllRequest();

		$data['notifAllLPP'] = $this->model_lpp->countAllLpp();
		$data['notifAllNarasumber'] = $this->model_narasumber->countAllNarasumber();
		//$data['semua_narasumber'] = $this->model_narasumber->getAllNarasumber($config["per_page"], $page);
		if($this->uri->segment(4)!=null){
	$data['no_from_url'] = $this->uri->segment(4);
    	}else{
    		$data['no_from_url'] = 1;
    	}

		$id = $this->input->post('id_narasumber');
		$lokasi = $this->input->post('lokasi');

		$data['semua_narasumber'] = $this->model_narasumber->getNarasumberById($id,$lokasi);
		$data['narasumber'] = $this->model_narasumber->getNarasumber();

		$this->load->view('layout_admin/header',$data);
		$this->load->view('lkpp/narasumber_all',$data);
		$this->load->view('layout_admin/footer');
	}

	public function tambah_narasumber()
	{
		$data['username'] = $this->session->userdata('username');
		$data['role'] = $this->session->userdata('role');
		$data['notifAgendaBaru'] = $this->model_rencana_kegiatan->countAllAgenda('0');
		$data['notifScheduleBaru'] = $this->model_pelatihan->countScheduleToBeApproved();
		$data['notifMintaFasilitator'] = $this->model_pelatihan->countFasilitatoriPelatihan();

		

		$this->load->view('layout_admin/header',$data);
		$this->load->view('lkpp/tambah_narasumber');
		$this->load->view('layout_admin/footer');
	}
	public function proses_tambah_narasumber()
	{
		$this->form_validation->set_rules('nama_lengkap', 'nama_lengkap', 'required');
		//$this->form_validation->set_rules('alamat', 'alamat', 'required');
		$this->form_validation->set_rules('no_telp', 'no_telp', 'required');
		
		//$this->form_validation->set_rules('tempat_lahir', 'tempat_lahir', 'required');
		//$this->form_validation->set_rules('tgl_lahir', 'tgl_lahir', 'required');
		if(strtolower($this->input->post('status_pegawai')) == 'asn')
		    $this->form_validation->set_rules('nip', 'nip', 'required');
		
		//$this->form_validation->set_rules('gelar', 'gelar', 'required');
		//$this->form_validation->set_rules('golongan', 'golongan', 'required');
		//$this->form_validation->set_rules('jenis_kelamin', 'jenis_kelamin', 'required');
		
		$this->form_validation->set_rules('pendidikan', 'pendidikan', 'required');
		$this->form_validation->set_rules('instansi', 'instansi', 'required');
		
		//$this->form_validation->set_rules('pangkat', 'pangkat', 'required');
		
		$this->form_validation->set_rules('satuan_kerja', 'satuan_kerja', 'required');
		$this->form_validation->set_rules('status_pegawai', 'status_pegawai', 'required');
		$this->form_validation->set_rules('provinsi', 'provinsi', 'required');
		$this->form_validation->set_rules('kota', 'kota', 'required');
		$this->form_validation->set_rules('email', 'email', 'required');
		$this->form_validation->set_rules('username', 'username', 'required');
		$this->form_validation->set_rules('password', 'password', 'required');
		/*$this->form_validation->set_rules('no_sertifikat', 'no_sertifikat', 'required');*/

		if ($this->form_validation->run() == FALSE)
		{
			$this->session->set_flashdata('status', '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.validation_errors().'</div>');
				redirect('lkpp/tambah_narasumber',$data);
		}else{
			$data = $this->model_narasumber->insert_new_narasumber();
			if($data){
				//$data['status'] = "success";
				$this->session->set_flashdata('status', '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Narasumber berhasil ditambahkan</div>');
				redirect('lkpp/tambah_narasumber',$data);
			}
		}
	}
	public function proses_tambah_Fasilitator()
	{
		/*$this->form_validation->set_rules('nama_lengkap', 'nama_lengkap', 'required');
		$this->form_validation->set_rules('alamat', 'alamat', 'required');
		$this->form_validation->set_rules('no_telp', 'no_telp', 'required');
		$this->form_validation->set_rules('tempat_lahir', 'tempat_lahir', 'required');
		$this->form_validation->set_rules('tgl_lahir', 'tgl_lahir', 'required');
		$this->form_validation->set_rules('nip', 'nip', 'required');
		$this->form_validation->set_rules('gelar', 'gelar', 'required');
		$this->form_validation->set_rules('golongan', 'golongan', 'required');
		$this->form_validation->set_rules('jenis_kelamin', 'jenis_kelamin', 'required');
		$this->form_validation->set_rules('pendidikan', 'pendidikan', 'required');
		$this->form_validation->set_rules('instansi', 'instansi', 'required');
		$this->form_validation->set_rules('pangkat', 'pangkat', 'required');
		$this->form_validation->set_rules('satuan_kerja', 'satuan_kerja', 'required');
		$this->form_validation->set_rules('status_pegawai', 'status_pegawai', 'required');
		$this->form_validation->set_rules('provinsi', 'provinsi', 'required');
		$this->form_validation->set_rules('kota', 'kota', 'required');
		$this->form_validation->set_rules('email', 'email', 'required');*/
		/*$this->form_validation->set_rules('no_sertifikat', 'no_sertifikat', 'required');*/
		$this->form_validation->set_rules('username', 'username', 'required');
		$this->form_validation->set_rules('password', 'password', 'required');

		if ($this->form_validation->run() == FALSE)
		{
			$this->session->set_flashdata('status', '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.validation_errors().'</div>');
				redirect('lkpp/new_fasilitator',$data);
		}else{
			$data = $this->model_pelatihan->insert_new_fasilitator();
			if($data){
				//$data['status'] = "success";
				$this->session->set_flashdata('status', '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>fasilitator berhasil ditambahkan</div>');
				redirect('lkpp/new_fasilitator',$data);
			}
		}
	}

	public function proses_update_fasilitator()
	{
		$id = $this->input->post('id_fasilitator');
		$data = $this->model_pelatihan->update_fasilitator($id);
			if($data){
				//$data['status'] = "success";
				$this->session->set_flashdata('status', '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>fasilitator berhasil ditambahkan</div>');
				redirect('lkpp/new_fasilitator',$data);
			}
	}
	
	public function findNarasumber($page = 0)
	{
		$data['username'] = $this->session->userdata('username');
		$data['role'] = $this->session->userdata('role');
		$data['notifAgendaBaru'] = $this->model_rencana_kegiatan->countAllAgenda('0');
		$data['notifScheduleBaru'] = $this->model_pelatihan->countScheduleToBeApproved();
		$data['notifMintaFasilitator'] = $this->model_pelatihan->countFasilitatoriPelatihan();
    	$data['username'] = $this->session->userdata('username');
		$data['role'] = $this->session->userdata('role');

		$data['notifAllLPP'] = $this->model_lpp->countAllLpp();
		$data['notifAllNarasumber'] = $this->model_narasumber->countAllNarasumber();
		$data['notifLPPbaru'] = $this->model_lpp->countLPPBaru();
		$data['notifAgendaBaru'] = $this->model_rencana_kegiatan->countAllAgenda('0');
		$data['notifScheduleBaru'] = $this->model_pelatihan->countScheduleToBeApproved();
		$data['notifMintaFasilitator'] = $this->model_pelatihan->countFasilitatoriPelatihan();

		
		//$data['klasemen_narasumber'] = $this->model_narasumber->findNarasumber();
		$data['narasumber'] = $this->model_narasumber->getNarasumber();

		$data['narasumber'] = $this->input->post('id_narasumber');
		$data['dari_tanggal'] = $this->input->post('dari_tanggal');
		$data['sampai_tanggal'] = $this->input->post('sampai_tanggal');
		$data['catatan'] = $this->input->post('catatan');
		$data['instansi'] = $this->input->post('instansi');
		$data['lokasi'] = $this->input->post('lokasi');
		$data['JP'] = $this->input->post('JP');

				$config = array();
		$config["base_url"] = base_url() . "lkpp/findNarasumber/limit";
		//HITUNG JUMLAH ROW DI TABLE BERITA//
		$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;

		if($this->uri->segment(4)!=null){
			$data['no_from_url'] = $this->uri->segment(4);
    	}else{
    		$data['no_from_url'] = 0;
    	}

    	$data['narasumber'] = $this->model_narasumber->getNarasumber();
		$data['klasemen_narasumber'] = $this->model_narasumber->findNarasumber(10, $page);

		$this->conditionQuery = $this->model_narasumber->getCondition();
		$data['conditionQuery'] = $this->conditionQuery;
		//echo $this->condition."shuloo";

		$config["total_rows"] = $this->model_narasumber->getRowFromfindNarasumber();
		$config["per_page"] = 10;
		$config["uri_segment"] = 4;
		$this->pagination->initialize($config);
		$data["links"] = $this->pagination->create_links();

		$data['tahun'] = date('Y');

		$this->load->view('layout_admin/header',$data);
		$this->load->view('lkpp/index',$data);
		$this->load->view('layout_admin/footer');
	}
	public function detail_pelatihan($id)
    {
		$data['notifAgendaBaru'] = $this->model_rencana_kegiatan->countAllAgenda('0');
		$data['notifScheduleBaru'] = $this->model_pelatihan->countScheduleToBeApproved();
		$data['notifMintaFasilitator'] = $this->model_pelatihan->countFasilitatoriPelatihan();
    	$data['username'] = $this->session->userdata('username');
		$data['role'] = $this->session->userdata('role');

        $data['row_narasumber'] = $this->model_pelatihan->countRowTable('pelatihan_narasumber','id_pelatihan',$id);
        $data['row_peserta'] = $this->model_pelatihan->countRowTable('tbl_peserta','id_pelatihan',$id);
        $data['row_eval_penyelenggara'] = $this->model_pelatihan->countRowTable('evaluasi_penyelenggara','id_pelatihan',$id);
        $data['row_eval_narasumber'] = $this->model_pelatihan->countRowTable('evaluasi_narasumber','id_pelatihan',$id);
        $data['row_fasilitator'] = $this->model_pelatihan->countRowTable('pelatihan_fasilitator','id_pelatihan',$id);
        
		$data['tema_pelatihan'] = $this->model_pelatihan->getAllTemaPelatihan();

		$data['notiftotalPending'] = $this->model_request_pelatihan->countNotifPendingRequest();
		$data['notiflisttotalPending'] = $this->model_request_pelatihan->getNotifPendingRequest();

		$data['notiftotalApproved'] = $this->model_request_pelatihan->countNotifApprovedRequest();
		$data['notiflisttotalApproved'] = $this->model_request_pelatihan->getNotifApprovedRequest();

		$data['notiftotalCanceled'] = $this->model_request_pelatihan->countNotifCanceledRequest();
		$data['notiflisttotalCanceled'] = $this->model_request_pelatihan->getNotifCanceledRequest();

		$data['id'] = $id;
		$id_lpp = $this->model_lpp->getIdLppFromIdPelatihan($id);
		$data['akreditasi'] = $this->model_lpp->getAkreditasiLpp($id_lpp);

		$data['detail'] = $this->model_pelatihan->getPelatihanByIdPelatihan($id);
		
		//update status read jadi 1 di table request pelatihan
		//$this->model_request_pelatihan->setRead(1,$id);

		$this->load->view('layout_admin/header',$data);
		$this->load->view('lkpp/menu_detail_pelatihan');
		$this->load->view('lkpp/detail_pelatihan',$data);
		$this->load->view('layout_admin/footer');
    }
    public function input_peserta($id)
    {
		$data['notifAgendaBaru'] = $this->model_rencana_kegiatan->countAllAgenda('0');
		$data['notifScheduleBaru'] = $this->model_pelatihan->countScheduleToBeApproved();
		$data['notifMintaFasilitator'] = $this->model_pelatihan->countFasilitatoriPelatihan();
    	$data['username'] = $this->session->userdata('username');
		$data['role'] = $this->session->userdata('role');

		$data['tema_pelatihan'] = $this->model_pelatihan->getAllTemaPelatihan();

		$data['notiftotalPending'] = $this->model_request_pelatihan->countNotifPendingRequest();
		$data['notiflisttotalPending'] = $this->model_request_pelatihan->getNotifPendingRequest();

		$data['notiftotalApproved'] = $this->model_request_pelatihan->countNotifApprovedRequest();
		$data['notiflisttotalApproved'] = $this->model_request_pelatihan->getNotifApprovedRequest();

		$data['notiftotalCanceled'] = $this->model_request_pelatihan->countNotifCanceledRequest();
		$data['notiflisttotalCanceled'] = $this->model_request_pelatihan->getNotifCanceledRequest();

		$data['id'] = $id;

		$data['detail'] = $this->model_pelatihan->getTemaPelatihanByIdRequest($id);
		$data['semua_peserta'] = $this->model_peserta->getAllPesertaByIdPelatihan($id);

		$this->load->view('layout_admin/header',$data);
		$this->load->view('lkpp/menu_detail_pelatihan');
		/*$this->load->view('lpp/input_peserta',$data);*/		
		$this->load->view('lkpp/input_peserta_upload',$data);
		$this->load->view('layout_admin/footer');
    }
    public function lihat_peserta($id)
    {
		$data['notifAgendaBaru'] = $this->model_rencana_kegiatan->countAllAgenda('0');
		$data['notifScheduleBaru'] = $this->model_pelatihan->countScheduleToBeApproved();
		$data['notifMintaFasilitator'] = $this->model_pelatihan->countFasilitatoriPelatihan();
    	$data['username'] = $this->session->userdata('username');
		$data['role'] = $this->session->userdata('role');

        $data['row_narasumber'] = $this->model_pelatihan->countRowTable('pelatihan_narasumber','id_pelatihan',$id);
        $data['row_peserta'] = $this->model_pelatihan->countRowTable('tbl_peserta','id_pelatihan',$id);
        $data['row_eval_penyelenggara'] = $this->model_pelatihan->countRowTable('evaluasi_penyelenggara','id_pelatihan',$id);
        $data['row_eval_narasumber'] = $this->model_pelatihan->countRowTable('evaluasi_narasumber','id_pelatihan',$id);
        $data['row_fasilitator'] = $this->model_pelatihan->countRowTable('pelatihan_fasilitator','id_pelatihan',$id);
        
		$data['tema_pelatihan'] = $this->model_pelatihan->getAllTemaPelatihan();

		$data['notiftotalPending'] = $this->model_request_pelatihan->countNotifPendingRequest();
		$data['notiflisttotalPending'] = $this->model_request_pelatihan->getNotifPendingRequest();

		$data['notiftotalApproved'] = $this->model_request_pelatihan->countNotifApprovedRequest();
		$data['notiflisttotalApproved'] = $this->model_request_pelatihan->getNotifApprovedRequest();

		$data['notiftotalCanceled'] = $this->model_request_pelatihan->countNotifCanceledRequest();
		$data['notiflisttotalCanceled'] = $this->model_request_pelatihan->getNotifCanceledRequest();

		$data['id'] = $id;
		$id_lpp = $this->model_lpp->getIdLppFromIdPelatihan($id);
		$data['akreditasi'] = $this->model_lpp->getAkreditasiLpp($id_lpp);
		
		$data['judul_pelatihan'] = $this->model_pelatihan->getJudulPelatihan($id);

		$data['detail'] = $this->model_pelatihan->getTemaPelatihanByIdRequest($id);
		$data['semua_peserta'] = $this->model_peserta->getAllPesertaByIdPelatihan($id);

		$this->load->view('layout_admin/header',$data);
		$this->load->view('lkpp/menu_detail_pelatihan');
		/*$this->load->view('lpp/input_peserta',$data);*/		
		$this->load->view('lkpp/lihat_peserta',$data);
		$this->load->view('layout_admin/footer');
    }
    public function fasilitator($id)
    {
		$data['notifAgendaBaru'] = $this->model_rencana_kegiatan->countAllAgenda('0');
		$data['notifScheduleBaru'] = $this->model_pelatihan->countScheduleToBeApproved();
		$data['notifMintaFasilitator'] = $this->model_pelatihan->countFasilitatoriPelatihan();
    	$data['username'] = $this->session->userdata('username');
		$data['role'] = $this->session->userdata('role');

        $data['row_narasumber'] = $this->model_pelatihan->countRowTable('pelatihan_narasumber','id_pelatihan',$id);
        $data['row_peserta'] = $this->model_pelatihan->countRowTable('tbl_peserta','id_pelatihan',$id);
        $data['row_eval_penyelenggara'] = $this->model_pelatihan->countRowTable('evaluasi_penyelenggara','id_pelatihan',$id);
        $data['row_eval_narasumber'] = $this->model_pelatihan->countRowTable('evaluasi_narasumber','id_pelatihan',$id);
        $data['row_fasilitator'] = $this->model_pelatihan->countRowTable('pelatihan_fasilitator','id_pelatihan',$id);
        
		$data['tema_pelatihan'] = $this->model_pelatihan->getAllTemaPelatihan();

		$data['notiftotalPending'] = $this->model_request_pelatihan->countNotifPendingRequest();
		$data['notiflisttotalPending'] = $this->model_request_pelatihan->getNotifPendingRequest();

		$data['notiftotalApproved'] = $this->model_request_pelatihan->countNotifApprovedRequest();
		$data['notiflisttotalApproved'] = $this->model_request_pelatihan->getNotifApprovedRequest();

		$data['notiftotalCanceled'] = $this->model_request_pelatihan->countNotifCanceledRequest();
		$data['notiflisttotalCanceled'] = $this->model_request_pelatihan->getNotifCanceledRequest();

		$data['id'] = $id;
		$id_lpp = $this->model_lpp->getIdLppFromIdPelatihan($id);
		$data['akreditasi'] = $this->model_lpp->getAkreditasiLpp($id_lpp);

		$data['detail'] = $this->model_pelatihan->getTemaPelatihanByIdRequest($id);
		$data['semua_peserta'] = $this->model_peserta->getAllPesertaByIdPelatihan($id);

		$data['fasilitator'] = $this->model_pelatihan->getFasilitator();

		$row = $this->model_pelatihan->count_fasilitator_by_pelatihan($id);

		$data['row_fasilitator'] = $row;

		if($row!=0){
			$data['data_fasilitator'] = $this->model_pelatihan->get_fasilitator_by_pelatihan($id);
		}

		$this->load->view('layout_admin/header',$data);
		$this->load->view('lkpp/menu_detail_pelatihan');
		/*$this->load->view('lpp/input_peserta',$data);*/		
		$this->load->view('lkpp/fasilitator',$data);
		$this->load->view('layout_admin/footer');
    }
    public function lihat_fasilitator($id)
    {
		$data['notifAgendaBaru'] = $this->model_rencana_kegiatan->countAllAgenda('0');
		$data['notifScheduleBaru'] = $this->model_pelatihan->countScheduleToBeApproved();
		$data['notifMintaFasilitator'] = $this->model_pelatihan->countFasilitatoriPelatihan();
    	$data['username'] = $this->session->userdata('username');
		$data['role'] = $this->session->userdata('role');

        $data['row_narasumber'] = $this->model_pelatihan->countRowTable('pelatihan_narasumber','id_pelatihan',$id);
        $data['row_peserta'] = $this->model_pelatihan->countRowTable('tbl_peserta','id_pelatihan',$id);
        $data['row_eval_penyelenggara'] = $this->model_pelatihan->countRowTable('evaluasi_penyelenggara','id_pelatihan',$id);
        $data['row_eval_narasumber'] = $this->model_pelatihan->countRowTable('evaluasi_narasumber','id_pelatihan',$id);
        $data['row_fasilitator'] = $this->model_pelatihan->countRowTable('pelatihan_fasilitator','id_pelatihan',$id);
        
		$data['tema_pelatihan'] = $this->model_pelatihan->getAllTemaPelatihan();

		$data['notiftotalPending'] = $this->model_request_pelatihan->countNotifPendingRequest();
		$data['notiflisttotalPending'] = $this->model_request_pelatihan->getNotifPendingRequest();

		$data['notiftotalApproved'] = $this->model_request_pelatihan->countNotifApprovedRequest();
		$data['notiflisttotalApproved'] = $this->model_request_pelatihan->getNotifApprovedRequest();

		$data['notiftotalCanceled'] = $this->model_request_pelatihan->countNotifCanceledRequest();
		$data['notiflisttotalCanceled'] = $this->model_request_pelatihan->getNotifCanceledRequest();

		$data['id'] = $id;

		$data['detail'] = $this->model_pelatihan->getTemaPelatihanByIdRequest($id);
		$data['semua_peserta'] = $this->model_peserta->getAllPesertaByIdPelatihan($id);

		$data['fasilitator'] = $this->model_pelatihan->getFasilitator();

		$row = $this->model_pelatihan->count_fasilitator_by_pelatihan($id);

		$data['row_fasilitator'] = $row;

		if($row!=0){
			$data['data_fasilitator'] = $this->model_pelatihan->get_fasilitator_by_pelatihan($id);
		}

		$this->load->view('layout_admin/header',$data);
		$this->load->view('lkpp/menu_detail_pelatihan');
		/*$this->load->view('lpp/input_peserta',$data);*/		
		$this->load->view('lkpp/lihat_fasilitator',$data);
		$this->load->view('layout_admin/footer');
    }
    public function proses_input_fasilitator()
    {
		$data['notifAgendaBaru'] = $this->model_rencana_kegiatan->countAllAgenda('0');
		$data['notifScheduleBaru'] = $this->model_pelatihan->countScheduleToBeApproved();
		$data['notifMintaFasilitator'] = $this->model_pelatihan->countFasilitatoriPelatihan();
    	$data = $this->model_pelatihan->insert_narasumber_fasilitator();
    	$id = $this->input->post('id_pelatihan');

		if($data){
			
			$this->session->set_flashdata('status', '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>fasilitator berhasil ditambahkan</div>');

			redirect('lkpp/fasilitator/'.$id,$data);
		}
    }

    public function upload_csv_peserta($id)
    {
		$data['notifAgendaBaru'] = $this->model_rencana_kegiatan->countAllAgenda('0');
		$data['notifScheduleBaru'] = $this->model_pelatihan->countScheduleToBeApproved();
		$data['notifMintaFasilitator'] = $this->model_pelatihan->countFasilitatoriPelatihan();
    			$config['upload_path'] = 'assets/csv';
		//$config['allowed_types'] = 'csv';
		$config['allowed_types'] = 'csv|xls|xlsx|csvexcel';
		//text/comma-separated-values|application/csv|application/excel|application/vnd.ms-excel|application/vnd.msexcel|text/anytext
		$config['max_size'] = '100';
		$config['max_width']  = '1024';
		$config['max_height']  = '768';

		$this->load->library('upload', $config);
$this->upload->initialize($config);

		if ( ! $this->upload->do_upload('csvfile'))
		{
		    $error = array('error' => $this->upload->display_errors());
		    var_dump($error);
		}
		else
		{
		    $upload_data = $this->upload->data();
		    
		    // start to read the CSV file
		    $this->load->library('csvreader');
		    $file_path = $upload_data['full_path'];
		    $csv_data = $this->csvreader->parse_file($file_path); 
		    $csv_fields = $this->csvreader->get_fields();
		    //echo "hae";
		if($csv_fields) // match to one of database table
		{
		    foreach($csv_data as $row){
$id_peserta = $row['ID_PESERTA'];
$id_pelatihan = $id;
$nama_lengkap = $row['NAMA_LENGKAP'];
$gelar_akademis = $row['GELAR_AKADEMIS'];
$jenis_kelamin = $row['JENIS_KELAMIN'];
$NIP = $row['NIP'];
$no_ktp = $row['NO_KTP'];
$tempat_lahir = $row['TEMPAT_LAHIR'];
$tgl_lahir = $row['TGL_LAHIR'];
$pendidikan_terakhir = $row['PENDIDIKAN_TERAKHIR'];
$status_kepegawaian = $row['STATUS_KEPEGAWAIAN'];
$instansi = $row['INSTANSI'];
$lembaga = $row['LEMBAGA'];
$domisili_kabupaten = $row['DOMISILI_KABUPATEN'];
$domisili_provinsi = $row['DOMISILI_PROVINSI'];
$no_telepon = $row['NO_TELEPON'];
$alamat_email = $row['ALAMAT_EMAIL'];
$pengalaman_kerja_bidang_pengadaan_barang_pemerintah = $row['PENGALAMAN_KERJA_BIDANG_PENGADAAN_BARANG_PEMERINTAH'];
$pernah_mengikuti_pelatihan_barang_jasa = $row['PERNAH_MENGIKUTI_PELATIHAN_BARANG_JASA'];
$berapa_kali = $row['BERAPA_KALI'];
$alasan_mengikuti = $row['ALASAN_MENGIKUTI'];

//insert ke table peserta
						$proses_tbl_peserta = $this->db->query("INSERT INTO tbl_peserta VALUES('$id_peserta','$nama_lengkap','$gelar_akademis','$jenis_kelamin','$NIP','$no_ktp','$tempat_lahir','$tgl_lahir','$pendidikan_terakhir','$status_kepegawaian','$instansi','$lembaga','$domisili_kabupaten','$domisili_provinsi','$no_telepon','$alamat_email','$pengalaman_kerja_bidang_pengadaan_barang_pemerintah','$pernah_mengikuti_pelatihan_barang_jasa','$berapa_kali','$alasan_mengikuti','$id_pelatihan','','')");

						//$proses_peserta_pelatihan = $this->db->query("INSERT INTO pelatihan_peserta VALUES('','$id_peserta','$id_pelatihan','','')");
		    }

		    if(($proses_tbl_peserta)){
									//$data['status'] = "success";
									$this->session->set_flashdata('status', '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Peserta berhasil ditambahkan</div>');

									
									redirect('lkpp/input_peserta/'.$id);
								}
		}
		    //}
		    
		    /*$data = array(
		'upload_data' => $upload_data,
		'csv_data' => $csv_data,
		    );*/
		    
		    //$this->load->view('upload_success', $data);
		}
    }
    public function proses_insert_peserta()
    {
		$data['notifAgendaBaru'] = $this->model_rencana_kegiatan->countAllAgenda('0');
		$data['notifScheduleBaru'] = $this->model_pelatihan->countScheduleToBeApproved();
		$data['notifMintaFasilitator'] = $this->model_pelatihan->countFasilitatoriPelatihan();
    	$proses = $this->model_peserta->input_peserta();
    	if($proses){
    		$result = array('result' => 'success');
    	}else{
    		$result = array('result' => 'failed');
    	}

		echo json_encode($result);
    }
    public function input_narasumber($id)
    {
		$data['notifAgendaBaru'] = $this->model_rencana_kegiatan->countAllAgenda('0');
		$data['notifScheduleBaru'] = $this->model_pelatihan->countScheduleToBeApproved();
		$data['notifMintaFasilitator'] = $this->model_pelatihan->countFasilitatoriPelatihan();
    	$data['username'] = $this->session->userdata('username');
		$data['role'] = $this->session->userdata('role');

        $data['row_narasumber'] = $this->model_pelatihan->countRowTable('pelatihan_narasumber','id_pelatihan',$id);
        $data['row_peserta'] = $this->model_pelatihan->countRowTable('tbl_peserta','id_pelatihan',$id);
        $data['row_eval_penyelenggara'] = $this->model_pelatihan->countRowTable('evaluasi_penyelenggara','id_pelatihan',$id);
        $data['row_eval_narasumber'] = $this->model_pelatihan->countRowTable('evaluasi_narasumber','id_pelatihan',$id);
        $data['row_fasilitator'] = $this->model_pelatihan->countRowTable('pelatihan_fasilitator','id_pelatihan',$id);
        
		$data['tema_pelatihan'] = $this->model_pelatihan->getAllTemaPelatihan();

		$data['notiftotalPending'] = $this->model_request_pelatihan->countNotifPendingRequest();
		$data['notiflisttotalPending'] = $this->model_request_pelatihan->getNotifPendingRequest();

		$data['notiftotalApproved'] = $this->model_request_pelatihan->countNotifApprovedRequest();
		$data['notiflisttotalApproved'] = $this->model_request_pelatihan->getNotifApprovedRequest();

		$data['notiftotalCanceled'] = $this->model_request_pelatihan->countNotifCanceledRequest();
		$data['notiflisttotalCanceled'] = $this->model_request_pelatihan->getNotifCanceledRequest();

		$data['id'] = $id;

		$data['detail'] = $this->model_pelatihan->getPelatihanByIdPelatihan($id);

		$dari_tanggal = $this->model_pelatihan->getTanggalDariPelatihanByIdPelatihan($id);
		$sampai_tanggal = $this->model_pelatihan->getTanggalSampaiPelatihanByIdPelatihan($id);

		$data['narasumber'] = $this->model_narasumber->getNarasumberAvailable($dari_tanggal,$sampai_tanggal);
		$data['semua_narasumber'] = $this->model_narasumber->getAllNarasumberByRealIdPelatihan($id);

		$this->load->view('layout_admin/header',$data);
		$this->load->view('lkpp/menu_detail_pelatihan');
		$this->load->view('lkpp/input_narasumber',$data);
		$this->load->view('layout_admin/footer');
    }
    public function lihat_narasumber($id)
    {
		$data['notifAgendaBaru'] = $this->model_rencana_kegiatan->countAllAgenda('0');
		$data['notifScheduleBaru'] = $this->model_pelatihan->countScheduleToBeApproved();
		$data['notifMintaFasilitator'] = $this->model_pelatihan->countFasilitatoriPelatihan();
    	$data['username'] = $this->session->userdata('username');
		$data['role'] = $this->session->userdata('role');

        $data['row_narasumber'] = $this->model_pelatihan->countRowTable('pelatihan_narasumber','id_pelatihan',$id);
        $data['row_peserta'] = $this->model_pelatihan->countRowTable('tbl_peserta','id_pelatihan',$id);
        $data['row_eval_penyelenggara'] = $this->model_pelatihan->countRowTable('evaluasi_penyelenggara','id_pelatihan',$id);
        $data['row_eval_narasumber'] = $this->model_pelatihan->countRowTable('evaluasi_narasumber','id_pelatihan',$id);
        $data['row_fasilitator'] = $this->model_pelatihan->countRowTable('pelatihan_fasilitator','id_pelatihan',$id);
        
		$data['tema_pelatihan'] = $this->model_pelatihan->getAllTemaPelatihan();

		$data['notiftotalPending'] = $this->model_request_pelatihan->countNotifPendingRequest();
		$data['notiflisttotalPending'] = $this->model_request_pelatihan->getNotifPendingRequest();

		$data['notiftotalApproved'] = $this->model_request_pelatihan->countNotifApprovedRequest();
		$data['notiflisttotalApproved'] = $this->model_request_pelatihan->getNotifApprovedRequest();

		$data['notiftotalCanceled'] = $this->model_request_pelatihan->countNotifCanceledRequest();
		$data['notiflisttotalCanceled'] = $this->model_request_pelatihan->getNotifCanceledRequest();

		$data['id'] = $id;
		$id_lpp = $this->model_lpp->getIdLppFromIdPelatihan($id);
		$data['akreditasi'] = $this->model_lpp->getAkreditasiLpp($id_lpp);

		$data['detail'] = $this->model_pelatihan->getPelatihanByIdPelatihan($id);

		$dari_tanggal = $this->model_pelatihan->getTanggalDariPelatihanByIdPelatihan($id);
		$sampai_tanggal = $this->model_pelatihan->getTanggalSampaiPelatihanByIdPelatihan($id);

		$data['narasumber'] = $this->model_narasumber->getNarasumberAvailable($dari_tanggal,$sampai_tanggal);
		$data['semua_narasumber'] = $this->model_narasumber->getAllNarasumberByRealIdPelatihan($id);

		$this->load->view('layout_admin/header',$data);
		$this->load->view('lkpp/menu_detail_pelatihan');
		$this->load->view('lkpp/lihat_narasumber',$data);
		$this->load->view('layout_admin/footer');
    }
    public function proses_insert_narasumber($id)
    {
		$data['notifAgendaBaru'] = $this->model_rencana_kegiatan->countAllAgenda('0');
		$data['notifScheduleBaru'] = $this->model_pelatihan->countScheduleToBeApproved();
		$data['notifMintaFasilitator'] = $this->model_pelatihan->countFasilitatoriPelatihan();
    	/*$proses = $this->model_narasumber->input_narasumber();
    	if($proses){
    		$result = array('result' => 'success');
    	}else{
    		$result = array('result' => 'failed');
    	}*/

    	$this->form_validation->set_rules('tgl_pelatihan', 'tgl_pelatihan', 'required');

		if ($this->form_validation->run() == FALSE)
		{
			$this->session->set_flashdata('status', '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.validation_errors().'</div>');
				redirect('lkpp/input_narasumber/'.$id,$data);
		}else{
			$data = $this->model_narasumber->input_narasumber();
			if($data){
				//$data['status'] = "success";
				$this->session->set_flashdata('status', '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Narasumber berhasil ditambahkan</div>');

				
				redirect('lkpp/input_narasumber/'.$id,$data);
			}
		}
    }
    public function request_narasumber($id)
    {
		$data['notifAgendaBaru'] = $this->model_rencana_kegiatan->countAllAgenda('0');
		$data['notifScheduleBaru'] = $this->model_pelatihan->countScheduleToBeApproved();
		$data['notifMintaFasilitator'] = $this->model_pelatihan->countFasilitatoriPelatihan();
    	$data['username'] = $this->session->userdata('username');
		$data['role'] = $this->session->userdata('role');

		$data['tema_pelatihan'] = $this->model_pelatihan->getAllTemaPelatihan();

		$data['notiftotalPending'] = $this->model_request_pelatihan->countNotifPendingRequest();
		$data['notiflisttotalPending'] = $this->model_request_pelatihan->getNotifPendingRequest();

		$data['notiftotalApproved'] = $this->model_request_pelatihan->countNotifApprovedRequest();
		$data['notiflisttotalApproved'] = $this->model_request_pelatihan->getNotifApprovedRequest();

		$data['notiftotalCanceled'] = $this->model_request_pelatihan->countNotifCanceledRequest();
		$data['notiflisttotalCanceled'] = $this->model_request_pelatihan->getNotifCanceledRequest();

		$data['id'] = $id;

		$data['detail'] = $this->model_pelatihan->getPelatihanByIdPelatihan($id);

		$dari_tanggal = $this->model_pelatihan->getTanggalDariPelatihanByIdPelatihan($id);
		$sampai_tanggal = $this->model_pelatihan->getTanggalSampaiPelatihanByIdPelatihan($id);

		$data['narasumber'] = $this->model_narasumber->getNarasumberAvailable($dari_tanggal,$sampai_tanggal);
		$data['semua_narasumber'] = $this->model_narasumber->getAllNarasumberByRealIdPelatihan($id);

		$this->load->view('layout_admin/header',$data);
		$this->load->view('lkpp/menu_detail_pelatihan');
		$this->load->view('lkpp/request_narasumber',$data);
		$this->load->view('layout_admin/footer');
    }
	public function detail_kegiatan_narasumber($id)
	{
		$data['username'] = $this->session->userdata('username');
		$data['role'] = $this->session->userdata('role');
		$data['notifAgendaBaru'] = $this->model_rencana_kegiatan->countAllAgenda('0');
		$data['notifScheduleBaru'] = $this->model_pelatihan->countScheduleToBeApproved();
		$data['notifMintaFasilitator'] = $this->model_pelatihan->countFasilitatoriPelatihan();

		//$id = $this->input->post('id_narasumber');
		$data['id_narasumber'] = $id;

		$condition = $this->input->post('condition');

		$data['dari_tanggal'] = $this->input->post('dari_tanggal');
		$data['sampai_tanggal'] = $this->input->post('sampai_tanggal');

		$dari_tanggal = $data['dari_tanggal'];
		$sampai_tanggal = $data['sampai_tanggal'];

		if($dari_tanggal!=null){
			$dari_tanggal = $this->input->post('dari_tanggal');
			$condition .= " AND tbl_kegiatan_natasumber.tanggal >= '$dari_tanggal' ";
		}
		if($sampai_tanggal!=null){
			$sampai_tanggal = $this->input->post('sampai_tanggal');
			$condition .= " AND tbl_kegiatan_natasumber.tanggal <= '$sampai_tanggal' ";
		}

		
		$data['detail_kegiatan_narasumber'] = $this->model_narasumber->getDetailKegiatanNarasumber($id,$condition);
		$data['detail_narasumber'] = $this->model_narasumber->getDetailNarasumber($id);
		$data['conditionQuery'] = $condition;

		$this->load->view('layout_admin/header',$data);
		$this->load->view('lkpp/detail_kegiatan_narasumber',$data);
		$this->load->view('layout_admin/footer');
	}

	public function berita()
    {
		$data['notifAgendaBaru'] = $this->model_rencana_kegiatan->countAllAgenda('0');
		$data['notifScheduleBaru'] = $this->model_pelatihan->countScheduleToBeApproved();
		$data['notifMintaFasilitator'] = $this->model_pelatihan->countFasilitatoriPelatihan();
    	$data['username'] = $this->session->userdata('username');
		$data['role'] = $this->session->userdata('role');

		
		$data['dataBerita'] = $this->model_berita->getSemuaBerita();

		$this->load->view('layout_admin/header',$data);
		$this->load->view('lkpp/berita');
		$this->load->view('layout_admin/footer');
    }

    public function detail_berita($id)
    {
		$data['notifAgendaBaru'] = $this->model_rencana_kegiatan->countAllAgenda('0');
		$data['notifScheduleBaru'] = $this->model_pelatihan->countScheduleToBeApproved();
		$data['notifMintaFasilitator'] = $this->model_pelatihan->countFasilitatoriPelatihan();
    	$data['username'] = $this->session->userdata('username');
		$data['role'] = $this->session->userdata('role');

		$data['dataBerita'] = $this->model_berita->getDetailBerita($id);

		$this->load->view('layout_admin/header',$data);
		$this->load->view('lkpp/detail_berita',$data);
		$this->load->view('layout_admin/footer');
    }

    public function tambah_berita()
	{
		$data['username'] = $this->session->userdata('username');
		$data['role'] = $this->session->userdata('role');
		$data['notifAgendaBaru'] = $this->model_rencana_kegiatan->countAllAgenda('0');
		$data['notifScheduleBaru'] = $this->model_pelatihan->countScheduleToBeApproved();
		$data['notifMintaFasilitator'] = $this->model_pelatihan->countFasilitatoriPelatihan();

		

		$this->load->view('layout_admin/header',$data);
		$this->load->view('lkpp/tambah_berita');
		$this->load->view('layout_admin/footer');
	}
	public function proses_tambah_berita()
	{
		$this->form_validation->set_rules('judul_berita', 'judul_berita', 'required');
		$this->form_validation->set_rules('tgl_berita', 'tgl_berita', 'required');
		$this->form_validation->set_rules('isi_berita', 'isi_berita', 'required');
		//$this->form_validation->set_rules('photo_berita', 'photo_berita', 'required');

		if ($this->form_validation->run() == FALSE)
		{
			$this->session->set_flashdata('status', '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.validation_errors().'</div>');
				redirect('lkpp/tambah_berita',$data);
		}else{
			$data = $this->model_berita->insert_new_berita();
			if($data == "success"){
				//$data['status'] = "success";
				$this->session->set_flashdata('status', '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Berita berhasil ditambahkan</div>');
				redirect('lkpp/berita',$data);
			}else{
				$this->session->set_flashdata('status', '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$data.'</div>');
				redirect('lkpp/berita',$data);
				//var_dump($data);
				//echo $data['file_name'];
			}
		}
	}
	public function tema_pelatihan()
	{
		$data['username'] = $this->session->userdata('username');
		$data['role'] = $this->session->userdata('role');
		$data['notifAgendaBaru'] = $this->model_rencana_kegiatan->countAllAgenda('0');
		$data['notifScheduleBaru'] = $this->model_pelatihan->countScheduleToBeApproved();
		$data['notifMintaFasilitator'] = $this->model_pelatihan->countFasilitatoriPelatihan();

		

		$config = array();
$config["base_url"] = base_url() . "lkpp/tema_pelatihan/limit";
//HITUNG JUMLAH ROW DI TABLE BERITA//
/*$config["total_rows"] = $this->model_narasumber->record_count('tbl_tema_pelatihan');
$config["per_page"] = 10;
$config["uri_segment"] = 3;
$this->pagination->initialize($config);
$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
$data["links"] = $this->pagination->create_links();
$data['page'] = "tema_pelatihan";
		$data['dataTemaPelatihan'] = $this->model_pelatihan->getAllTemaPelatihanLimit($config["per_page"], $page);*/
		$data['dataTemaPelatihan'] = $this->model_pelatihan->getAllTemaPelatihanAll();

		$this->load->view('layout_admin/header',$data);
		$this->load->view('lkpp/tema_pelatihan');
		$this->load->view('layout_admin/footer');
	}
	public function tambah_tema_pelatihan()
	{
		$data['username'] = $this->session->userdata('username');
		$data['role'] = $this->session->userdata('role');
		$data['notifAgendaBaru'] = $this->model_rencana_kegiatan->countAllAgenda('0');
		$data['notifScheduleBaru'] = $this->model_pelatihan->countScheduleToBeApproved();
		$data['notifMintaFasilitator'] = $this->model_pelatihan->countFasilitatoriPelatihan();

		

		$this->load->view('layout_admin/header',$data);
		$this->load->view('lkpp/tambah_tema_pelatihan');
		$this->load->view('layout_admin/footer');
	}
	public function proses_tema_pelatihan()
	{
		$this->form_validation->set_rules('judul_tema', 'judul_tema', 'required');
		$this->form_validation->set_rules('keterangan_tema', 'keterangan_tema', 'required');
		//$this->form_validation->set_rules('photo_berita', 'photo_berita', 'required');

		if ($this->form_validation->run() == FALSE)
		{
			$this->session->set_flashdata('status', '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.validation_errors().'</div>');
				redirect('lkpp/tema_pelatihan',$data);
		}else{
			$data = $this->model_pelatihan->insert_new_tema_pelatihan();
			if($data == "success"){
				//$data['status'] = "success";
				$this->session->set_flashdata('status', '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Tema Pelatihan berhasil ditambahkan</div>');
				redirect('lkpp/tema_pelatihan',$data);
			}else{
				$this->session->set_flashdata('status', '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$data.'</div>');
				redirect('lkpp/tema_pelatihan',$data);
				//var_dump($data);
				//echo $data['file_name'];
			}
		}
	}
	public function edit_tema_pelatihan($id)
	{
		$data['username'] = $this->session->userdata('username');
		$data['role'] = $this->session->userdata('role');

		$data['tema_pelatihan'] = $this->model_pelatihan->getAllTemaPelatihan();

		$data['notiftotalPending'] = $this->model_request_pelatihan->countNotifPendingRequest();
		$data['notiflisttotalPending'] = $this->model_request_pelatihan->getNotifPendingRequest();

		$data['getAllRequest'] = $this->model_request_pelatihan->getAllRequest();
		$data['notifAllRequest'] = $this->model_request_pelatihan->countAllRequest();

		$data['notifAllLPP'] = $this->model_lpp->countAllLpp();

		$data['dataTemaPelatihan'] = $this->model_pelatihan->getTemaPelatihanById($id);
		
		$data['notifAllNarasumber'] = $this->model_narasumber->countAllNarasumber();
		$data['notifAgendaBaru'] = $this->model_rencana_kegiatan->countAllAgenda('0');
		$data['notifScheduleBaru'] = $this->model_pelatihan->countScheduleToBeApproved();
		$data['notifMintaFasilitator'] = $this->model_pelatihan->countFasilitatoriPelatihan();

		$this->load->view('layout_admin/header',$data);
		$this->load->view('lkpp/edit_tema_pelatihan',$data);
		$this->load->view('layout_admin/footer');
	}
	public function proses_update_tema_pelatihan()
	{
		$this->form_validation->set_rules('judul_tema', 'judul_tema', 'required');
		$this->form_validation->set_rules('keterangan_tema', 'keterangan_tema', 'required');
		//$this->form_validation->set_rules('photo_berita', 'photo_berita', 'required');

		if ($this->form_validation->run() == FALSE)
		{
			$this->session->set_flashdata('status', '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.validation_errors().'</div>');
				redirect('lkpp/tema_pelatihan',$data);
		}else{
			$data = $this->model_pelatihan->update_tema_pelatihan();
			if($data == "success"){
				//$data['status'] = "success";
				$this->session->set_flashdata('status', '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Tema Pelatihan berhasil ditambahkan</div>');
				redirect('lkpp/tema_pelatihan',$data);
			}else{
				$this->session->set_flashdata('status', '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$data.'</div>');
				redirect('lkpp/tema_pelatihan',$data);
				//var_dump($data);
				//echo $data['file_name'];
			}
		}
	}
	public function delete_tema_pelatihan($id)
	{
		$data = $this->model_pelatihan->delete_tema_pelatihan($id);
		if($data == "success"){
				//$data['status'] = "success";
				$this->session->set_flashdata('status', '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Data Berhasil di Hapus</div>');
				redirect('lkpp/tema_pelatihan',$data);
			}else{
				$this->session->set_flashdata('status', '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$data.'</div>');
				redirect('lkpp/tema_pelatihan',$data);
				//var_dump($data);
				//echo $data['file_name'];
			}
	}

	

	public function verifikasi_lpp($id_lpp)
	{
		$data = $this->model_lpp->verifikasi_lpp($id_lpp);
		if($data == "success"){
				//$data['status'] = "success";
				$this->session->set_flashdata('status', '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>LPP telah di Verifikasi</div>');
				redirect('lkpp/lpp/all',$data);
			}else{
				$this->session->set_flashdata('status', '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$data.'</div>');
				redirect('lkpp/lpp/all',$data);
				//var_dump($data);
				//echo $data['file_name'];
			}
	}

	public function debug()
	{

				$config = Array(
				    'protocol' => 'smtp',
				    'smtp_host' => 'ssl://smtp.gmail.com',
				    'smtp_port' => 465,
				    'smtp_user' => 'swara.ogawa@gmail.com',
				    'smtp_pass' => '',
				    'mailtype'  => 'html', 
				    'charset'   => 'iso-8859-1'
				);
				$this->load->library('email', $config);
				$this->email->set_newline("\r\n");

				$this->load->library('email', $config);
				$this->email->set_newline("\r\n");

				$this->email->from('swara.ogawa@gmail.com', 'Aditya Eka Putra');
				$this->email->to('Recieving Email address'); 

				$this->email->subject('Email Subject');
				$this->email->message('Email Message');
				
				$result = $this->email->send(); 

				echo $this->email->print_debugger();
	}
	public function getlppsatker($satker)
	{
		$lpp = array();
		$lpp = $this->model_lpp->get_lpp_by_satker($satker);
		echo json_encode($lpp);
	}
	public function approve_rencana_kegiatan()
	{
		$data['username'] = $this->session->userdata('username');
		$data['role'] = $this->session->userdata('role');
		$data['notifAgendaBaru'] = $this->model_rencana_kegiatan->countAllAgenda('0');
		$data['notifScheduleBaru'] = $this->model_pelatihan->countScheduleToBeApproved();
		$data['notifMintaFasilitator'] = $this->model_pelatihan->countFasilitatoriPelatihan();

		

		$config = array();
		$config["base_url"] = base_url() . "lkpp/approve_rencana_kegiatan/page";

		$data['dataBerita'] = $this->model_rencana_kegiatan->getAllAgenda('0');

		$this->load->view('layout_admin/header',$data);
		$this->load->view('lkpp/approve_rencana_kegiatan',$data);
		$this->load->view('layout_admin/footer');
	}
	public function tolak_agenda($id_rencana)
	{
		$this->form_validation->set_rules('alasan_ditolak', 'alasan_ditolak', 'required');

		if ($this->form_validation->run() == FALSE)
		{
			$this->session->set_flashdata('status', '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Keterangan Harus Diisi</div>');
    	}else{
    		$alasan = $this->input->post('alasan_ditolak');
			
			$data = array(
	            'approved' => 2,
	            'alasan_ditolak' => $alasan
	        ); 

			$this->db->where('id_rencana',$id_rencana);
	        $this->db->update('rencana_kegiatan', $data); 
    	}
		redirect('lkpp/approve_rencana_kegiatan',$data);
	}
	public function arsip_rencana_kegiatan()
	{
		$data['username'] = $this->session->userdata('username');
		$data['role'] = $this->session->userdata('role');

		

		$config = array();
		$config["base_url"] = base_url() . "lkpp/approve_rencana_kegiatan/page";

		$data['dataBerita'] = $this->model_rencana_kegiatan->getAllAgenda('1');
		$data['notifAgendaBaru'] = $this->model_rencana_kegiatan->countAllAgenda('0');
		$data['notifScheduleBaru'] = $this->model_pelatihan->countScheduleToBeApproved();
		$data['notifMintaFasilitator'] = $this->model_pelatihan->countFasilitatoriPelatihan();

		$this->load->view('layout_admin/header',$data);
		$this->load->view('lkpp/approve_rencana_kegiatan',$data);
		$this->load->view('layout_admin/footer');
	}
	public function detail_agenda($id)
	{
		$data['username'] = $this->session->userdata('username');
		$data['role'] = $this->session->userdata('role');

		$data['dataAgenda'] = $this->model_rencana_kegiatan->getDetailAgenda($id);
		$data['notifAgendaBaru'] = $this->model_rencana_kegiatan->countAllAgenda('0');
		$data['notifScheduleBaru'] = $this->model_pelatihan->countScheduleToBeApproved();
		$data['notifMintaFasilitator'] = $this->model_pelatihan->countFasilitatoriPelatihan();

		$this->load->view('layout_admin/header',$data);
		$this->load->view('lkpp/detail_rencana_kegiatan',$data);
		$this->load->view('layout_admin/footer');
	}
	public function approve_agenda($id)
	{
		$data = $this->model_rencana_kegiatan->approve($id);
		if($data == "success"){
				//$data['status'] = "success";
				$this->session->set_flashdata('status', '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Data Berhasil di Hapus</div>');
				redirect('lkpp/approve_rencana_kegiatan',$data);
			}else{
				$this->session->set_flashdata('status', '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$data.'</div>');
				redirect('lkpp/approve_rencana_kegiatan',$data);
				//var_dump($data);
				//echo $data['file_name'];
			}
	}
	public function new_fasilitator()
	{
		$data['username'] = $this->session->userdata('username');
		$data['role'] = $this->session->userdata('role');
		$data['notifAgendaBaru'] = $this->model_rencana_kegiatan->countAllAgenda('0');
		$data['notifScheduleBaru'] = $this->model_pelatihan->countScheduleToBeApproved();
		$data['notifMintaFasilitator'] = $this->model_pelatihan->countFasilitatoriPelatihan();

		

		$config = array();
		$config["base_url"] = base_url() . "lkpp/approve_rencana_kegiatan/page";

		$data['dataBerita'] = $this->model_pelatihan->getFasilitator();

		$this->load->view('layout_admin/header',$data);
		$this->load->view('lkpp/new_fasilitator',$data);
		$this->load->view('layout_admin/footer');
	}
	public function detail_fasilitator($id)
	{
		$data['username'] = $this->session->userdata('username');
		$data['role'] = $this->session->userdata('role');
		$data['notifAgendaBaru'] = $this->model_rencana_kegiatan->countAllAgenda('0');
		$data['notifScheduleBaru'] = $this->model_pelatihan->countScheduleToBeApproved();
		$data['notifMintaFasilitator'] = $this->model_pelatihan->countFasilitatoriPelatihan();

		

		$config = array();
		$config["base_url"] = base_url() . "lkpp/approve_rencana_kegiatan/page";

		$data['dataBerita'] = $this->model_pelatihan->getFasilitatorByID($id);

		$this->load->view('layout_admin/header',$data);
		$this->load->view('lkpp/detail_fasilitator',$data);
		$this->load->view('layout_admin/footer');
	}
	public function add_fasilitator()
	{
		$data = $this->model_pelatihan->add_fasilitator();
		if($data == "success"){
				//$data['status'] = "success";
				$this->session->set_flashdata('status', '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Data Berhasil di Hapus</div>');
				redirect('lkpp/new_fasilitator',$data);
			}else{
				$this->session->set_flashdata('status', '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$data.'</div>');
				redirect('lkpp/new_fasilitator',$data);
				//var_dump($data);
				//echo $data['file_name'];
			}
	}
	public function downloadfile($link)
	{
		$file = urldecode($link);
		redirect('assets/upload/bukti_kegiatan/'.$file);
	}
	public function upload_materi()
	{
		$data['username'] = $this->session->userdata('username');
		$data['role'] = $this->session->userdata('role');
		
		$data['dataMateri'] = $this->model_pelatihan->get_materi();
		$data['notifAgendaBaru'] = $this->model_rencana_kegiatan->countAllAgenda('0');
		$data['notifScheduleBaru'] = $this->model_pelatihan->countScheduleToBeApproved();
		$data['notifMintaFasilitator'] = $this->model_pelatihan->countFasilitatoriPelatihan();

		$this->load->view('layout_admin/header',$data);
		$this->load->view('lkpp/upload_materi');
		$this->load->view('layout_admin/footer');
	}
	public function proses_tambah_materi()
	{
			$data = $this->model_pelatihan->insert_new_materi();
			if($data == "success"){
				//$data['status'] = "success";
				$this->session->set_flashdata('status', '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Berita berhasil ditambahkan</div>');
				redirect('lkpp/upload_materi',$data);
			}else{
				$this->session->set_flashdata('status', '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$data.'</div>');
				redirect('lkpp/upload_materi',$data);
				//var_dump($data);
				//echo $data['file_name'];
			}
		}
	public function approve_schedule()
	{
		$data['username'] = $this->session->userdata('username');
		$data['role'] = $this->session->userdata('role');
		$data['notifAgendaBaru'] = $this->model_rencana_kegiatan->countAllAgenda('0');
		$data['notifScheduleBaru'] = $this->model_pelatihan->countScheduleToBeApproved();
		$data['notifMintaFasilitator'] = $this->model_pelatihan->countFasilitatoriPelatihan();
		
		$data['dataMateri'] = $this->model_pelatihan->get_materi();

		$data['semua_pelatihan'] = $this->model_pelatihan->getAllSchedulePelatihan();

		$this->load->view('layout_admin/header',$data);
		$this->load->view('lkpp/approve_schedule');
		$this->load->view('layout_admin/footer');
	}
	public function fasilitatori_pelatihan()
	{
		$data['username'] = $this->session->userdata('username');
		$data['role'] = $this->session->userdata('role');
		$data['notifAgendaBaru'] = $this->model_rencana_kegiatan->countAllAgenda('0');
		$data['notifScheduleBaru'] = $this->model_pelatihan->countScheduleToBeApproved();
		$data['notifMintaFasilitator'] = $this->model_pelatihan->countFasilitatoriPelatihan();
		
		$data['dataMateri'] = $this->model_pelatihan->get_materi();

		$data['semua_pelatihan'] = $this->model_pelatihan->getFasilitatoriPelatihan();

		$this->load->view('layout_admin/header',$data);
		$this->load->view('lkpp/fasilitatori_pelatihan');
		$this->load->view('layout_admin/footer');
	}
	public function pilih_fasilitator($id_pelatihan)
	{
		$data['username'] = $this->session->userdata('username');
		$data['role'] = $this->session->userdata('role');
		$data['notifAgendaBaru'] = $this->model_rencana_kegiatan->countAllAgenda('0');
		$data['notifScheduleBaru'] = $this->model_pelatihan->countScheduleToBeApproved();
		$data['notifMintaFasilitator'] = $this->model_pelatihan->countFasilitatoriPelatihan();
		
		$data['dataMateri'] = $this->model_pelatihan->get_materi();

		$data['semua_pelatihan'] = $this->model_pelatihan->getFasilitatoriPelatihan();
		$data['detail'] = $this->model_pelatihan->getPelatihanByIdPelatihan($id_pelatihan);
		$data['fasilitator'] = $this->model_pelatihan->getFasilitator();

		$data['id_pelatihan'] = $id_pelatihan;

		$this->load->view('layout_admin/header',$data);
		$this->load->view('lkpp/pilih_fasilitator');
		$this->load->view('layout_admin/footer');
	}
	public function proses_pilih_fasilitator($id_pelatihan)
	{
		$data = $this->model_pelatihan->insert_narasumber_fasilitator2($id_pelatihan);

		if($data){
			
			$this->session->set_flashdata('status', '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>fasilitator berhasil ditambahkan</div>');

			redirect('lkpp/fasilitatori_pelatihan/',$data);
		}
	}

	public function proses_approve_schedule($id)
	{
		$data = $this->model_pelatihan->approveSchedule($id);
			if($data=="success_approve"){
				//$data['status'] = "success";
				$this->session->set_flashdata('status', '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Request telah diterima, pelatihan berhasil di buat</div>');
				redirect('lkpp/approve_schedule',$data);
			}else{
				//$data['status'] = "failed";
				$this->session->set_flashdata('status', '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>gagal memasukan data, coba beberapa saat lagi</div>');
				redirect('lkpp/approve_schedule',$data);
			}
	}
	public function proses_tolak_schedule($id)
	{
		$alasan_ditolak = $this->input->post('alasan_ditolak');
		$this->form_validation->set_rules('alasan_ditolak', 'alasan_ditolak', 'required');


			if ($this->form_validation->run() == FALSE)
			{
				$this->session->set_flashdata('status', '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Alasan Tidak Boleh Kosong</div>');
				redirect('lkpp/approve_schedule');
			}else{
				$data = $this->model_pelatihan->tolakSchedule($id,$alasan_ditolak);
					if($data=="success_approve"){
						//$data['status'] = "success";
						$this->session->set_flashdata('status', '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Request telah diterima, pelatihan berhasil di buat</div>');
						redirect('lkpp/approve_schedule',$data);
					}else{
						//$data['status'] = "failed";
						$this->session->set_flashdata('status', '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>gagal memasukan data, coba beberapa saat lagi</div>');
						redirect('lkpp/approve_schedule',$data);
					}
			}
	}
	public function evaluasi_penyelenggara($id_pelatihan)
	{
		$data['notifAgendaBaru'] = $this->model_rencana_kegiatan->countAllAgenda('0');
		$data['notifScheduleBaru'] = $this->model_pelatihan->countScheduleToBeApproved();
		$data['notifMintaFasilitator'] = $this->model_pelatihan->countFasilitatoriPelatihan();
		$data['username'] = $this->session->userdata('username');
		$data['role'] = $this->session->userdata('role');
		$data['id_user'] = $this->session->userdata('id');
		$id_lpp = $this->session->userdata('id');
		$data['nama_lpp'] = $this->model_lpp->getLppName($this->session->userdata('id'));
		$data['id_lpp'] = $this->session->userdata('id');
		
        $data['row_narasumber'] = $this->model_pelatihan->countRowTable('pelatihan_narasumber','id_pelatihan',$id_pelatihan);
        $data['row_peserta'] = $this->model_pelatihan->countRowTable('tbl_peserta','id_pelatihan',$id_pelatihan);
        $data['row_eval_penyelenggara'] = $this->model_pelatihan->countRowTable('evaluasi_penyelenggara','id_pelatihan',$id_pelatihan);
        $data['row_eval_narasumber'] = $this->model_pelatihan->countRowTable('evaluasi_narasumber','id_pelatihan',$id_pelatihan);
        $data['row_fasilitator'] = $this->model_pelatihan->countRowTable('pelatihan_fasilitator','id_pelatihan',$id_pelatihan);
        
		$data['detail'] = $this->model_pelatihan->getPelatihanByIdPelatihan($id_pelatihan);
        $data['total_request'] = $this->model_pelatihan->newRequestFasilitator($this->session->userdata('id'));
		
		$data['id'] = $id_pelatihan;
		$id_lpp = $this->model_lpp->getIdLppFromIdPelatihan($id_pelatihan);
		$data['akreditasi'] = $this->model_lpp->getAkreditasiLpp($id_lpp);

		$this->load->view('layout_admin/header',$data);
		$this->load->view('lkpp/menu_detail_pelatihan');

			$data['dataEvaluasi'] = $this->model_evaluasi->getEvaluasiPenyelenggaraTanpaLpp($id_pelatihan);
			$this->load->view('fasilitator/evaluasi_penyelenggara',$data);
		
		$this->load->view('layout_admin/footer');
	}

	public function evaluasi_narasumber($id_pelatihan)
	{
		$data['notifAgendaBaru'] = $this->model_rencana_kegiatan->countAllAgenda('0');
		$data['notifScheduleBaru'] = $this->model_pelatihan->countScheduleToBeApproved();
		$data['notifMintaFasilitator'] = $this->model_pelatihan->countFasilitatoriPelatihan();
		$data['username'] = $this->session->userdata('username');
		$data['role'] = $this->session->userdata('role');
		$data['id_user'] = $this->session->userdata('id');
		//$id_lpp = $this->session->userdata('id');
		$data['nama_lpp'] = $this->model_lpp->getLppName($this->session->userdata('id'));
		//$data['id_lpp'] = $this->session->userdata('id');
		
        $data['row_narasumber'] = $this->model_pelatihan->countRowTable('pelatihan_narasumber','id_pelatihan',$id_pelatihan);
        $data['row_peserta'] = $this->model_pelatihan->countRowTable('tbl_peserta','id_pelatihan',$id_pelatihan);
        $data['row_eval_penyelenggara'] = $this->model_pelatihan->countRowTable('evaluasi_penyelenggara','id_pelatihan',$id_pelatihan);
        $data['row_eval_narasumber'] = $this->model_pelatihan->countRowTable('evaluasi_narasumber','id_pelatihan',$id_pelatihan);
        $data['row_fasilitator'] = $this->model_pelatihan->countRowTable('pelatihan_fasilitator','id_pelatihan',$id_pelatihan);
        
		$data['detail'] = $this->model_pelatihan->getPelatihanByIdPelatihan($id_pelatihan);
        $data['total_request'] = $this->model_pelatihan->newRequestFasilitator($this->session->userdata('id'));
		
		$data['id'] = $id_pelatihan;
		$id_lpp = $this->model_lpp->getIdLppFromIdPelatihan($id_pelatihan);
		$data['akreditasi'] = $this->model_lpp->getAkreditasiLpp($id_lpp);
		
		$data['detail'] = $this->model_pelatihan->getPelatihanByIdPelatihan($id_pelatihan);
		$data['dataEvaluasi'] = $this->model_evaluasi->getEvaluasiNarasumber($id_pelatihan);

		$data['id'] = $id_pelatihan;

		$this->load->view('layout_admin/header',$data);
		$this->load->view('lkpp/menu_detail_pelatihan');
		$this->load->view('fasilitator/evaluasi_narasumber',$data);
		$this->load->view('layout_admin/footer');
	}

	public function upload_dokumen()
	{
		$data['username'] = $this->session->userdata('username');
		$data['role'] = $this->session->userdata('role');
		
		$data['dataDokumen'] = $this->model_pelatihan->get_dokumen();
		$data['notifAgendaBaru'] = $this->model_rencana_kegiatan->countAllAgenda('0');
		$data['notifScheduleBaru'] = $this->model_pelatihan->countScheduleToBeApproved();
		$data['notifMintaFasilitator'] = $this->model_pelatihan->countFasilitatoriPelatihan();

		$this->load->view('layout_admin/header',$data);
		$this->load->view('lkpp/upload_dokumen');
		$this->load->view('layout_admin/footer');
	}

	public function upload_regulasi()
	{
		$data['username'] = $this->session->userdata('username');
		$data['role'] = $this->session->userdata('role');
		
		$data['dataDokumen'] = $this->main_model->get_list_all('tbl_regulasi');
		$data['notifAgendaBaru'] = $this->model_rencana_kegiatan->countAllAgenda('0');
		$data['notifScheduleBaru'] = $this->model_pelatihan->countScheduleToBeApproved();
		$data['notifMintaFasilitator'] = $this->model_pelatihan->countFasilitatoriPelatihan();

		$this->load->view('layout_admin/header',$data);
		$this->load->view('lkpp/upload_regulasi');
		$this->load->view('layout_admin/footer');
	}

	public function edit_dokumen($id)
	{
		$data['username'] = $this->session->userdata('username');
		$data['role'] = $this->session->userdata('role');
		
		$data['dataDokumen'] = $this->model_pelatihan->get_dokumen_where($id);
		$data['notifAgendaBaru'] = $this->model_rencana_kegiatan->countAllAgenda('0');
		$data['notifScheduleBaru'] = $this->model_pelatihan->countScheduleToBeApproved();
		$data['notifMintaFasilitator'] = $this->model_pelatihan->countFasilitatoriPelatihan();

		$this->load->view('layout_admin/header',$data);
		$this->load->view('lkpp/edit_dokumen');
		$this->load->view('layout_admin/footer');
	}

	public function edit_regulasi($id)
	{
		$data['username'] = $this->session->userdata('username');
		$data['role'] = $this->session->userdata('role');
		
		$data['dataDokumen'] = $this->main_model->get_list_where('tbl_regulasi',array('id_dokumen'=>$id));
		$data['notifAgendaBaru'] = $this->model_rencana_kegiatan->countAllAgenda('0');
		$data['notifScheduleBaru'] = $this->model_pelatihan->countScheduleToBeApproved();
		$data['notifMintaFasilitator'] = $this->model_pelatihan->countFasilitatoriPelatihan();

		$this->load->view('layout_admin/header',$data);
		$this->load->view('lkpp/edit_regulasi');
		$this->load->view('layout_admin/footer');
	}
	public function proses_tambah_dokumen()
	{
			$data = $this->model_pelatihan->insert_new_dokumen();
			if($data == "success"){
				//$data['status'] = "success";
				$this->session->set_flashdata('status', '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>dokumen berhasil ditambahkan</div>');
				redirect('lkpp/upload_dokumen',$data);
			}else{
				$this->session->set_flashdata('status', '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$data.'</div>');
				redirect('lkpp/upload_dokumen',$data);
				//var_dump($data);
				//echo $data['file_name'];
			}
	}
	public function proses_tambah_regulasi()
	{
			$data = $this->model_pelatihan->insert_new_regulasi();
			if($data == "success"){
				//$data['status'] = "success";
				$this->session->set_flashdata('status', '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>dokumen berhasil ditambahkan</div>');
				redirect('lkpp/upload_regulasi',$data);
			}else{
				$this->session->set_flashdata('status', '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$data.'</div>');
				redirect('lkpp/upload_regulasi',$data);
				//var_dump($data);
				//echo $data['file_name'];
			}
	}
	public function proses_edit_dokumen()
	{
			$data = $this->model_pelatihan->update_dokumen();
			if($data == "success"){
				//$data['status'] = "success";
				$this->session->set_flashdata('status', '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>dokumen berhasil ditambahkan</div>');
				redirect('lkpp/upload_dokumen',$data);
			}else{
				$this->session->set_flashdata('status', '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$data.'</div>');
				redirect('lkpp/upload_dokumen',$data);
				//var_dump($data);
				//echo $data['file_name'];
			}
	}
	public function proses_edit_regulasi()
	{
			$data = $this->model_pelatihan->update_regulasi();
			if($data == "success"){
				//$data['status'] = "success";
				$this->session->set_flashdata('status', '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>dokumen berhasil ditambahkan</div>');
				redirect('lkpp/upload_regulasi',$data);
			}else{
				$this->session->set_flashdata('status', '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$data.'</div>');
				redirect('lkpp/upload_regulasi',$data);
				//var_dump($data);
				//echo $data['file_name'];
			}
	}

	public function galeri()
	{
		$data['username'] = $this->session->userdata('username');
		$data['role'] = $this->session->userdata('role');
		
		$data['dataFoto'] = $this->model_pelatihan->get_all_foto();
		$data['notifAgendaBaru'] = $this->model_rencana_kegiatan->countAllAgenda('0');
		$data['notifScheduleBaru'] = $this->model_pelatihan->countScheduleToBeApproved();
		$data['notifMintaFasilitator'] = $this->model_pelatihan->countFasilitatoriPelatihan();
		$data['kategori'] = $this->model_pelatihan->get_kategori();

		$this->load->view('layout_admin/header',$data);
		$this->load->view('lkpp/galeri_foto');
		$this->load->view('layout_admin/footer');
	}

	public function proses_tambah_foto()
	{
			$data = $this->model_pelatihan->insert_new_foto();
			if($data == "success"){
				//$data['status'] = "success";
				$this->session->set_flashdata('status', '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>dokumen berhasil ditambahkan</div>');
				redirect('lkpp/galeri',$data);
			}else{
				$this->session->set_flashdata('status', '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$data.'</div>');
				redirect('lkpp/galeri',$data);
				//var_dump($data);
				//echo $data['file_name'];
			}
	}

	public function proses_tambah_kategori()
	{
		$kategori = $this->input->post('kategori_foto');
		$this->db->query("INSERT INTO kategori_galeri VALUES('','$kategori')");
		redirect('lkpp/galeri/');
	}
	public function delete_foto($id)
	{
		$this->db->query("DELETE FROM galeri_foto WHERE id_foto='$id'");
		redirect('lkpp/galeri/');
	}
	public function delete_materi($id)
	{
		$this->db->query("DELETE FROM tbl_library WHERE id_library='$id'");
		redirect('lkpp/upload_materi/');
	}

	public function delete_pengumuman($id)
	{
		$this->db->query("DELETE FROM tbl_pengumuman WHERE id_pengumuman='$id'");
		redirect('lkpp/pengumuman/all');
	}

	public function delete_dokumen($id)
	{
		$this->db->query("DELETE FROM tbl_dokumen WHERE id_dokumen='$id'");
		redirect('lkpp/upload_dokumen');
	}

	public function delete_regulasi($id)
	{
		$this->db->query("DELETE FROM tbl_regulasi WHERE id_dokumen='$id'");
		redirect('lkpp/upload_regulasi');
	}

	public function pengumuman($param,$id='')
    {
		$data['notifAgendaBaru'] = $this->model_rencana_kegiatan->countAllAgenda('0');
		$data['notifScheduleBaru'] = $this->model_pelatihan->countScheduleToBeApproved();
		$data['notifMintaFasilitator'] = $this->model_pelatihan->countFasilitatoriPelatihan();
    	$data['username'] = $this->session->userdata('username');
		$data['role'] = $this->session->userdata('role');

		
		$data['dataPengumuman'] = $this->model_berita->getAllPengumuman();
		$data['dataPengumumanById'] = $this->model_berita->getPengumumanById($id);

		$data['action'] = $param;

		$this->load->view('layout_admin/header',$data);
		$this->load->view('lkpp/pengumuman');
		$this->load->view('layout_admin/footer');
    }

    public function proses_tambah_pengumuman()
	{
			$data = $this->model_berita->insert_new_pengumuman();
			if($data == "success"){
				//$data['status'] = "success";
				$this->session->set_flashdata('status', '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>dokumen berhasil ditambahkan</div>');
				redirect('lkpp/pengumuman/all',$data);
			}else{
				$this->session->set_flashdata('status', '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$data.'</div>');
				redirect('lkpp/pengumuman/all',$data);
				//var_dump($data);
				//echo $data['file_name'];
			}
	}
	public function proses_edit_pengumuman($id)
	{
		$data = $this->model_berita->edit_pengumuman($id);
			if($data == "success"){
				//$data['status'] = "success";
				$this->session->set_flashdata('status', '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>dokumen berhasil ditambahkan</div>');
				redirect('lkpp/pengumuman/edit/'.$id,$data);
			}else{
				$this->session->set_flashdata('status', '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$data.'</div>');
				redirect('lkpp/pengumuman/edit/'.$id,$data);
				//var_dump($data);
				//echo $data['file_name'];
			}
	}


	public function edit_pengumuman($id)
    {
		$data['notifAgendaBaru'] = $this->model_rencana_kegiatan->countAllAgenda('0');
		$data['notifScheduleBaru'] = $this->model_pelatihan->countScheduleToBeApproved();
		$data['notifMintaFasilitator'] = $this->model_pelatihan->countFasilitatoriPelatihan();
    	$data['username'] = $this->session->userdata('username');
		$data['role'] = $this->session->userdata('role');

		
		$data['dataPengumuman'] = $this->model_berita->getPengumumanById($id);

		$this->load->view('layout_admin/header',$data);
		$this->load->view('lkpp/pengumuman');
		$this->load->view('layout_admin/footer');
    }

    public function report($id_pelatihan)
    {
    	$data['id_pelatihan'] = $id_pelatihan;
    	
		$data['detail'] = $this->model_pelatihan->getPelatihanByIdPelatihan($id_pelatihan);
		$data['data_fasilitator'] = $this->model_pelatihan->get_fasilitator_by_pelatihan($id_pelatihan);
		$data['data_narasumber'] = $this->model_narasumber->getAllNarasumberByRealIdPelatihan($id_pelatihan);
		$data['data_peserta'] = $this->model_peserta->getAllPesertaByIdPelatihan($id_pelatihan);
		$data['data_evaluasi'] = $this->model_evaluasi->getEvaluasiPenyelenggaraTanpaLpp($id_pelatihan);
		$data['data_evaluasi_narasumber'] = $this->model_evaluasi->getEvaluasiNarasumber($id_pelatihan);

			$data['laporan'] = "detail_pelatihan";
			
		$this->load->view('lkpp/tombol_report',$data);
		$this->load->view('lkpp/report',$data);

    }

    public function laporan_detail_pelatihan($id_pelatihan)
    {
    		$data['id_pelatihan'] = $id_pelatihan;
    	
			$data['detail'] = $this->model_pelatihan->getPelatihanByIdPelatihan($id_pelatihan);
			$data['data_fasilitator'] = $this->model_pelatihan->get_fasilitator_by_pelatihan($id_pelatihan);
			$data['data_narasumber'] = $this->model_narasumber->getAllNarasumberByRealIdPelatihan($id_pelatihan);
			$data['data_peserta'] = $this->model_peserta->getAllPesertaByIdPelatihan($id_pelatihan);
			$data['data_evaluasi'] = $this->model_evaluasi->getEvaluasiPenyelenggaraTanpaLpp($id_pelatihan);
			$data['data_evaluasi_narasumber'] = $this->model_evaluasi->getEvaluasiNarasumber($id_pelatihan);

			$html = $this->load->view('lkpp/report',$data,TRUE);

			$data['laporan'] = "detail_pelatihan";
  
			$this->load->library('mpdf/mpdf');
			$this->mpdf=new mPDF('c','A4','','');
			$this->mpdf->WriteHTML($html,2);
			$mpdf->useDefaultCSS2 = true;
			$this->mpdf->Output('LAPORAN PELATIHAN.pdf','I');
    }

    public function reportSemuaPelatihan()
    {
    	$data['laporan'] = "seluruh_pelatihan";
		$data['pelatihan'] = $this->model_pelatihan->findPelatihan();

		$this->queryfindPelatihan = $this->model_pelatihan->findPelatihan();

		$html = $this->load->view('lkpp/report_seluruh_pelatihan',$data,TRUE);
  
			$this->load->library('mpdf/mpdf');
			$this->mpdf=new mPDF('c','A4','','');
			$this->mpdf->WriteHTML($html,2);
			$mpdf->useDefaultCSS2 = true;
			$this->mpdf->Output('LAPORAN SELURUH PELATIHAN.pdf','I');
    }

    public function laporan_seluruh_pelatihan()
    {
    		
			
    }

     public function ganti_password()
    {
		$data['username'] = $this->session->userdata('username');
		$data['role'] = $this->session->userdata('role');
		$data['id_user'] = $this->session->userdata('id');
		$id = $this->session->userdata('id');

		$data['username'] = $this->session->userdata('username');
		$data['role'] = $this->session->userdata('role');
		
		$data['dataFoto'] = $this->model_pelatihan->get_all_foto();
		$data['notifAgendaBaru'] = $this->model_rencana_kegiatan->countAllAgenda('0');
		$data['notifScheduleBaru'] = $this->model_pelatihan->countScheduleToBeApproved();
		$data['notifMintaFasilitator'] = $this->model_pelatihan->countFasilitatoriPelatihan();
		
		//update status read jadi 1 di table request pelatihan
		//$this->model_request_pelatihan->setRead(1,$id);

		$this->load->view('layout_admin/header',$data);
		$this->load->view('lkpp/ganti_password',$data);
		$this->load->view('layout_admin/footer');
    }

    public function ganti_password_lkpp()
    {
    	$id_lkpp = $this->session->userdata('id');

    	$password_lama = $this->input->post('password_lama');
    	$password_baru = $this->input->post('password_baru');
    	$password_konfirmasi = $this->input->post('password_konfirmasi');

        $this->form_validation->set_rules('password_lama', 'password_lama', 'required');
        $this->form_validation->set_rules('password_baru', 'password_baru', 'required');
        $this->form_validation->set_rules('password_konfirmasi', 'password_konfirmasi', 'required');

    	if ($this->form_validation->run() == FALSE)
		{
			$this->session->set_flashdata('status', '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.validation_errors().'</div>');
				redirect('lkpp/ganti_password',$data);
				
		}else{
	    	if(base64_encode(md5($password_lama))!=$this->model_narasumber->getPasswordLamaLkpp($id_lkpp)){
	    		$this->session->set_flashdata('status', '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>password sebelumnya salah</div>');
						redirect('lkpp/ganti_password',$data);
	    	}else{
	    		if($password_baru!=$password_konfirmasi){
	    			$this->session->set_flashdata('status', '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>password baru tidak sesuai</div>');
	    			redirect('lkpp/ganti_password',$data);
	    		}else{
	    			$pass = base64_encode(md5($password_konfirmasi));
	    			$proses = $this->db->query("UPDATE 
	    				tbl_lkpp SET 
			            password = '$pass'
			            WHERE
			            id_lkpp = '$id_lkpp'
		            ");
	    			$this->session->set_flashdata('status', '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>password berhasil diubah</div>');
	    			redirect('lkpp/ganti_password',$data);
	    		}
	    	}
	    }
    }

    public function batalkan_lpp($id)
    {

    	$proses = $this->db->query("UPDATE tbl_lpp SET approve = '2' WHERE id_lpp = '$id' ");
        $alasan = $this->input->post('alasan_ditolak');
        $message = "Mohon maaf pendaftaran anda ditolak karena, ";
        $message .= $alasan;

        if($proses){
        	$email = $this->model_lpp->getEmailLpp($id);

            $mail             = new PHPMailer();

	        $mail->IsSMTP();
	        $mail->SMTPAuth   = true;                  
	        $mail->SMTPSecure = "";                 
	        $mail->Host       = "mail.lkpp.go.id";      
	        $mail->Port       = 25;                   

	        $mail->Username   = "simpel@lkpp.go.id";  
	        $mail->Password   = "jeruks1988";           
	        $mail->AddReplyTo($email,$email);
	        $mail->AddAddress($email, $email);
	        $mail->From       = "simpel@lkpp.go.id";
	        $mail->FromName   = "SIMPEL LKPP";

	        $mail->Subject    = "Pemberitahuan Pendaftaran LPP";

	        $mail->Body       = "Hi, <br>".$message."<br>";             
	        $mail->AltBody    = ""; 
	        $mail->WordWrap   = 50; 

	        $mail->MsgHTML($message);

	        $mail->IsHTML(true); 

	        if(!$mail->Send()) {
	          echo "Mailer Error: " . $mail->ErrorInfo;
	        } else {
	            redirect('kontak/');
	        }
        }else{
            return false;
        }

    	
    }
}