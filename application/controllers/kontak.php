<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Kontak extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        $this->load->model('model_pelatihan');
        $this->load->model('model_lpp');
        $this->load->model('model_narasumber');
        $this->load->model('model_peserta');
        $this->load->model('model_request_pelatihan');
        $this->load->model('model_berita');
        $this->load->helper('tgl_indonesia');
        $this->load->library('MY_Upload');
        $this->load->library('email');
        $this->load->library('MY_PHPMailer');
    }
	public function index()
    {
        $data['headerPengumuman'] = $this->model_berita->getheaderPengumuman();
        $this->load->view('frontend/header',$data);
        $this->load->view('frontend/hubungi_kami');
        $this->load->view('frontend/footer');
    }
    public function mail()
    {
        $nama = $this->input->post('nama');
        $email = $this->input->post('email');
        $subjek = $this->input->post('subjek');
        $message = $this->input->post('message');

        $mail             = new PHPMailer();

        //$body             = $mail->getFile('contents.html');
        //$body             = eregi_replace("[\]",'',$body);

        $mail->IsSMTP();
        $mail->SMTPAuth   = true;                  // enable SMTP authentication
        $mail->SMTPSecure = "";                 // sets the prefix to the servier
        $mail->Host       = "mail.lkpp.go.id";      // sets GMAIL as the SMTP server
        $mail->Port       = 25;                   // set the SMTP port for the GMAIL server

        $mail->Username   = "simpel@lkpp.go.id";  // GMAIL username
        $mail->Password   = "jeruks1988";            // GMAIL password
        $mail->AddReplyTo("simpel@lkpp.go.id","Kontak EMAIL ");
        $mail->AddAddress("simpel@lkpp.go.id", "SIMPEL LKPP");
        $mail->From       = $email;
        $mail->FromName   = $nama;

        $mail->Subject    = $subjek;

        $mail->Body       = "Hi, <br>".$message."<br>";                      //HTML Body
        $mail->AltBody    = ""; // optional, comment out and test
        $mail->WordWrap   = 50; // set word wrap

        $mail->MsgHTML($message);

        //$mail->AddAttachment("images/phpmailer.gif");             // attachment

        $mail->IsHTML(true); // send as HTML

        if(!$mail->Send()) {
          echo "Mailer Error: " . $mail->ErrorInfo;
        } else {
            redirect('kontak/');
        }
    }

}