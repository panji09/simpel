<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Galeri extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        $this->load->model('model_pelatihan');
        $this->load->model('model_lpp');
        $this->load->model('model_narasumber');
        $this->load->model('model_peserta');
        $this->load->model('model_request_pelatihan');
        $this->load->model('model_berita');
        $this->load->helper('tgl_indonesia');
        $this->load->library('MY_Upload');
    }
	public function index()
    {
        $data['headerPengumuman'] = $this->model_berita->getheaderPengumuman();
        $data['galeri_kategori'] = $this->model_pelatihan->get_foto_kategori();
        $data['dataPengumuman'] = $this->model_berita->getPengumuman();

        $this->load->view('frontend/header',$data);
        $this->load->view('frontend/galeri');
        $this->load->view('frontend/footer');
    }
    public function detail($id)
    {
        $data['headerPengumuman'] = $this->model_berita->getheaderPengumuman();
        $data['galeri_kategori'] = $this->model_pelatihan->get_fotoByID($id);
        $data['dataPengumuman'] = $this->model_berita->getPengumuman();
        
        $this->load->view('frontend/header',$data);
        $this->load->view('frontend/galeri_detail');
        $this->load->view('frontend/footer');
    }

}