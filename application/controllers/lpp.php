<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Lpp extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
    {
        parent::__construct();
        $this->load->model('model_pelatihan');
        $this->load->model('model_lpp');
        $this->load->model('model_narasumber');
        $this->load->model('model_peserta');
        $this->load->model('model_request_pelatihan');
        $this->load->model('model_evaluasi');
        $this->load->helper('tgl_indonesia');
        $this->load->model('app_model');
        if( ($this->session->userdata('logged_in')!=1) || ($this->session->userdata('role')!='lpp') )
        {
        	$this->app_model->logout();
            redirect('admin/login');
        }
        
         session_start();
        
        $_SESSION['KCFINDER'] = array(
	    'disabled' => false,
	    'access' => array(

	      'files' => array(
		  'upload' => false,
		  'delete' => true,
		  'copy'   => true,
		  'move'   => true,
		  'rename' => true
	      ),

	      'dirs' => array(
		  'create' => true,
		  'delete' => true,
		  'rename' => true
	      )
	  )
	);
    }
    public function index()
    {
		$data['notifikasi_row'] = $this->model_pelatihan->countNotifNarasumber($this->session->userdata('id'));
        $data['notifikasi_schedule'] = $this->model_pelatihan->countpelatihanbaru($this->session->userdata('id'));
        $data['notifikasi_ditolak'] = $this->model_pelatihan->countpelatihanditolak($this->session->userdata('id'));

    	$data['username'] = $this->session->userdata('username');
		$data['role'] = $this->session->userdata('role');
		$id_lpp = $this->session->userdata('id');

		$data['jumlah_pelatihan'] = $this->model_pelatihan->countAllPelatihanByIdLpp($id_lpp);
		$data['jumlah_request'] = $this->model_request_pelatihan->countAllRequestByIdLpp($id_lpp);
		$data['jumlah_narasumber'] = $this->model_narasumber->countAllNarasumber();

		$data['notifikasi_narasumber'] = $this->model_pelatihan->getNotifNarasumber($this->session->userdata('id'));

		$this->load->view('layout_admin/header',$data);
		$this->load->view('lpp/index');
		$this->load->view('layout_admin/footer');
    }
    
    public function file_explorer(){
	$data['notifikasi_row'] = $this->model_pelatihan->countNotifNarasumber($this->session->userdata('id'));
        $data['notifikasi_schedule'] = $this->model_pelatihan->countpelatihanbaru($this->session->userdata('id'));
        $data['notifikasi_ditolak'] = $this->model_pelatihan->countpelatihanditolak($this->session->userdata('id'));

    	$data['username'] = $this->session->userdata('username');
	$data['role'] = $this->session->userdata('role');
	$id_lpp = $this->session->userdata('id');

	$data['jumlah_pelatihan'] = $this->model_pelatihan->countAllPelatihanByIdLpp($id_lpp);
	$data['jumlah_request'] = $this->model_request_pelatihan->countAllRequestByIdLpp($id_lpp);
	$data['jumlah_narasumber'] = $this->model_narasumber->countAllNarasumber();

	$data['notifikasi_narasumber'] = $this->model_pelatihan->getNotifNarasumber($this->session->userdata('id'));
	
	$this->load->view('layout_admin/header',$data);
	$this->load->view('lkpp/file_explorer');
	$this->load->view('layout_admin/footer');

    }
    
    public function request_pelatihan()
    {
		$data['notifikasi_row'] = $this->model_pelatihan->countNotifNarasumber($this->session->userdata('id'));
        $data['notifikasi_schedule'] = $this->model_pelatihan->countpelatihanbaru($this->session->userdata('id'));
        $data['notifikasi_ditolak'] = $this->model_pelatihan->countpelatihanditolak($this->session->userdata('id'));
    	$data['username'] = $this->session->userdata('username');
		$data['role'] = $this->session->userdata('role');

		$data['tema_pelatihan'] = $this->model_pelatihan->getAllTemaPelatihan();

		$this->load->view('layout_admin/header',$data);
		$this->load->view('lpp/request_pelatihan');
		$this->load->view('layout_admin/footer');
    }
    /*public function proses_request_pelatihan()
    {
		$data['notifikasi_row'] = $this->model_pelatihan->countNotifNarasumber($this->session->userdata('id'));
        $data['notifikasi_schedule'] = $this->model_pelatihan->countpelatihanbaru($this->session->userdata('id'));
        $data['notifikasi_ditolak'] = $this->model_pelatihan->countpelatihanditolak($this->session->userdata('id'));
    	$this->form_validation->set_rules('tema_pelatihan', 'tema_pelatihan', 'required');
		$this->form_validation->set_rules('dari_tanggal', 'dari_tanggal', 'required');
		$this->form_validation->set_rules('sampai_tanggal', 'sampai_tanggal', 'required');
		$this->form_validation->set_rules('tempat_pelatihan', 'tempat_pelatihan', 'required');
		$this->form_validation->set_rules('isi_pelatihan', 'isi_pelatihan', 'required');

		if ($this->form_validation->run() == FALSE)
		{
			$this->session->set_flashdata('status', '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.validation_errors().'</div>');
				redirect('lpp/request_pelatihan',$data);
		}else{
			$data = $this->model_request_pelatihan->addNewRequest();
			if($data){
				//$data['status'] = "success";
				$this->session->set_flashdata('status', '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Request data telah dikirim kepada lpp</div>');
				redirect('lpp/request_pelatihan',$data);
			}else{
				//$data['status'] = "failed";
				$this->session->set_flashdata('status', '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>gagal memasukan data, coba beberapa saat lagi</div>');
				redirect('lpp/request_pelatihan',$data);
			}
		}
    }*/
    

    public function detail_pelatihan($id)
    {
		$data['notifikasi_row'] = $this->model_pelatihan->countNotifNarasumber($this->session->userdata('id'));
        $data['notifikasi_schedule'] = $this->model_pelatihan->countpelatihanbaru($this->session->userdata('id'));
        $data['notifikasi_ditolak'] = $this->model_pelatihan->countpelatihanditolak($this->session->userdata('id'));

        $data['row_narasumber'] = $this->model_pelatihan->countRowTable('pelatihan_narasumber','id_pelatihan',$id);
        $data['row_peserta'] = $this->model_pelatihan->countRowTable('tbl_peserta','id_pelatihan',$id);
        $data['row_eval_penyelenggara'] = $this->model_pelatihan->countRowTable('evaluasi_penyelenggara','id_pelatihan',$id);
        $data['row_eval_narasumber'] = $this->model_pelatihan->countRowTable('evaluasi_narasumber','id_pelatihan',$id);

    	$data['username'] = $this->session->userdata('username');
		$data['role'] = $this->session->userdata('role');

		$data['tema_pelatihan'] = $this->model_pelatihan->getAllTemaPelatihan();

		$data['id'] = $id;

		$data['detail'] = $this->model_pelatihan->getPelatihanByIdPelatihan($id);
		
		$id_pelatihan = $this->model_lpp->getIdLppFromIdPelatihan($id);
		$data['akreditasi'] = $this->model_lpp->getAkreditasiLpp($id_pelatihan);
		
		//update status read jadi 1 di table request pelatihan
		$this->model_request_pelatihan->setRead(1,$id);

		$this->load->view('layout_admin/header',$data);
		$this->load->view('lpp/menu_detail_pelatihan');
		$this->load->view('lpp/detail_pelatihan',$data);
		$this->load->view('layout_admin/footer');
    }
    public function detail_schedule($id)
    {
		//update status read jadi 1 di table request pelatihan
		$this->model_request_pelatihan->setRead(1,$id);
		
		$data['notifikasi_row'] = $this->model_pelatihan->countNotifNarasumber($this->session->userdata('id'));
        $data['notifikasi_schedule'] = $this->model_pelatihan->countpelatihanbaru($this->session->userdata('id'));
        $data['notifikasi_ditolak'] = $this->model_pelatihan->countpelatihanditolak($this->session->userdata('id'));
    	$data['username'] = $this->session->userdata('username');
		$data['role'] = $this->session->userdata('role');

		$data['tema_pelatihan'] = $this->model_pelatihan->getAllTemaPelatihan();

		$data['id'] = $id;

		$data['detail'] = $this->model_pelatihan->getScheduleByIdPelatihan($id);
		
		$id_pelatihan = $this->model_lpp->getIdLppFromIdPelatihan($id);
		$data['akreditasi'] = $this->model_lpp->getAkreditasiLpp($id_pelatihan);
		

		$this->load->view('layout_admin/header',$data);
		$this->load->view('lpp/detail_schedule',$data);
		$this->load->view('layout_admin/footer');
    }
    public function detail_pelatihan_2($id)
    {
		$data['notifikasi_row'] = $this->model_pelatihan->countNotifNarasumber($this->session->userdata('id'));
        $data['notifikasi_schedule'] = $this->model_pelatihan->countpelatihanbaru($this->session->userdata('id'));
        $data['notifikasi_ditolak'] = $this->model_pelatihan->countpelatihanditolak($this->session->userdata('id'));
    	$data['username'] = $this->session->userdata('username');
		$data['role'] = $this->session->userdata('role');

		$data['tema_pelatihan'] = $this->model_pelatihan->getAllTemaPelatihan();

		$data['id'] = $id;

		$data['detail'] = $this->model_request_pelatihan->getDetailRequestById($id);
		$data['stat'] = "tidak/belum diterima";
		
		//update status read jadi 1 di table request pelatihan
		//$this->model_request_pelatihan->setRead(1,$id);

		$this->load->view('layout_admin/header',$data);
		$this->load->view('lpp/detail_pelatihan',$data);
		$this->load->view('layout_admin/footer');
    }

    public function input_peserta($id)
    {
		$data['notifikasi_row'] = $this->model_pelatihan->countNotifNarasumber($this->session->userdata('id'));
        $data['notifikasi_schedule'] = $this->model_pelatihan->countpelatihanbaru($this->session->userdata('id'));
        $data['notifikasi_ditolak'] = $this->model_pelatihan->countpelatihanditolak($this->session->userdata('id'));
        
        $data['row_narasumber'] = $this->model_pelatihan->countRowTable('pelatihan_narasumber','id_pelatihan',$id);
        $data['row_peserta'] = $this->model_pelatihan->countRowTable('tbl_peserta','id_pelatihan',$id);
        $data['row_eval_penyelenggara'] = $this->model_pelatihan->countRowTable('evaluasi_penyelenggara','id_pelatihan',$id);
        $data['row_eval_narasumber'] = $this->model_pelatihan->countRowTable('evaluasi_narasumber','id_pelatihan',$id);

    	$data['username'] = $this->session->userdata('username');
		$data['role'] = $this->session->userdata('role');

		$data['tema_pelatihan'] = $this->model_pelatihan->getAllTemaPelatihan();

		$data['id'] = $id;

		$data['detail'] = $this->model_pelatihan->getTemaPelatihanByIdRequest($id);
		
		$data['judul_pelatihan'] = $this->model_pelatihan->getJudulPelatihan($id);

		$data['semua_peserta'] = $this->model_peserta->getAllPesertaByIdPelatihan($id);

		$id_pelatihan = $this->model_lpp->getIdLppFromIdPelatihan($id);
		$data['akreditasi'] = $this->model_lpp->getAkreditasiLpp($id_pelatihan);

		$this->load->view('layout_admin/header',$data);
		$this->load->view('lpp/menu_detail_pelatihan');
		$this->load->view('lpp/input_peserta',$data);
		$this->load->view('layout_admin/footer');
    }
    public function input_peserta_manual($id)
    {
		$data['notifikasi_row'] = $this->model_pelatihan->countNotifNarasumber($this->session->userdata('id'));
        $data['notifikasi_schedule'] = $this->model_pelatihan->countpelatihanbaru($this->session->userdata('id'));
        $data['notifikasi_ditolak'] = $this->model_pelatihan->countpelatihanditolak($this->session->userdata('id'));
        
        $data['row_narasumber'] = $this->model_pelatihan->countRowTable('pelatihan_narasumber','id_pelatihan',$id);
        $data['row_peserta'] = $this->model_pelatihan->countRowTable('tbl_peserta','id_pelatihan',$id);
        $data['row_eval_penyelenggara'] = $this->model_pelatihan->countRowTable('evaluasi_penyelenggara','id_pelatihan',$id);
        $data['row_eval_narasumber'] = $this->model_pelatihan->countRowTable('evaluasi_narasumber','id_pelatihan',$id);

    	$data['username'] = $this->session->userdata('username');
		$data['role'] = $this->session->userdata('role');

		$data['tema_pelatihan'] = $this->model_pelatihan->getAllTemaPelatihan();

		$data['id'] = $id;

		$data['detail'] = $this->model_pelatihan->getTemaPelatihanByIdRequest($id);
		
		$data['semua_peserta'] = $this->model_peserta->getAllPesertaByIdPelatihan($id);

		$id_pelatihan = $this->model_lpp->getIdLppFromIdPelatihan($id);
		$data['akreditasi'] = $this->model_lpp->getAkreditasiLpp($id_pelatihan);

		$this->load->view('layout_admin/header',$data);
		$this->load->view('lpp/menu_detail_pelatihan');
		$this->load->view('lpp/input_peserta_manual',$data);
		$this->load->view('layout_admin/footer');
    }
    public function proses_insert_peserta()
    {
		$data['notifikasi_row'] = $this->model_pelatihan->countNotifNarasumber($this->session->userdata('id'));
        $data['notifikasi_schedule'] = $this->model_pelatihan->countpelatihanbaru($this->session->userdata('id'));
        $data['notifikasi_ditolak'] = $this->model_pelatihan->countpelatihanditolak($this->session->userdata('id'));
    	$proses = $this->model_peserta->input_peserta();
    	if($proses){
    		$result = array('result' => 'success');
    	}else{
    		$result = array('result' => 'failed');
    	}

		echo json_encode($result);
    }
    public function input_narasumber($id)
    {
		$data['notifikasi_row'] = $this->model_pelatihan->countNotifNarasumber($this->session->userdata('id'));
        $data['notifikasi_schedule'] = $this->model_pelatihan->countpelatihanbaru($this->session->userdata('id'));
        $data['notifikasi_ditolak'] = $this->model_pelatihan->countpelatihanditolak($this->session->userdata('id'));
        
        $data['row_narasumber'] = $this->model_pelatihan->countRowTable('pelatihan_narasumber','id_pelatihan',$id);
        $data['row_peserta'] = $this->model_pelatihan->countRowTable('tbl_peserta','id_pelatihan',$id);
        $data['row_eval_penyelenggara'] = $this->model_pelatihan->countRowTable('evaluasi_penyelenggara','id_pelatihan',$id);
        $data['row_eval_narasumber'] = $this->model_pelatihan->countRowTable('evaluasi_narasumber','id_pelatihan',$id);

    	$data['username'] = $this->session->userdata('username');
		$data['role'] = $this->session->userdata('role');

		$data['tema_pelatihan'] = $this->model_pelatihan->getAllTemaPelatihan();

		$data['id'] = $id;

		$data['detail'] = $this->model_pelatihan->getPelatihanByIdPelatihan($id);

		$dari_tanggal = $this->model_pelatihan->getTanggalDariPelatihanByIdPelatihan($id);
		$sampai_tanggal = $this->model_pelatihan->getTanggalSampaiPelatihanByIdPelatihan($id);

		//$data['narasumber'] = $this->model_narasumber->getNarasumberAvailable($dari_tanggal,$sampai_tanggal);
		$data['narasumber'] = $this->model_narasumber->getAbsolutelyAllNarasumber();
		$data['semua_narasumber'] = $this->model_narasumber->getAllNarasumberByRealIdPelatihan($id);

		$id_pelatihan = $this->model_lpp->getIdLppFromIdPelatihan($id);
		$data['akreditasi'] = $this->model_lpp->getAkreditasiLpp($id_pelatihan);



		$this->load->view('layout_admin/header',$data);
		$this->load->view('lpp/menu_detail_pelatihan');
		$this->load->view('lpp/input_narasumber',$data);
		$this->load->view('layout_admin/footer');
    }
    public function setAktif($id,$id_pelatihan)
    {
		$data['notifikasi_row'] = $this->model_pelatihan->countNotifNarasumber($this->session->userdata('id'));
        $data['notifikasi_schedule'] = $this->model_pelatihan->countpelatihanbaru($this->session->userdata('id'));
        $data['notifikasi_ditolak'] = $this->model_pelatihan->countpelatihanditolak($this->session->userdata('id'));
    	$proses = $this->model_pelatihan->setAktif($id);
    	redirect('lpp/lihat_narasumber/'.$id_pelatihan);
    }
    public function proses_insert_narasumber($id)
    {
		$data['notifikasi_row'] = $this->model_pelatihan->countNotifNarasumber($this->session->userdata('id'));
        $data['notifikasi_schedule'] = $this->model_pelatihan->countpelatihanbaru($this->session->userdata('id'));
        $data['notifikasi_ditolak'] = $this->model_pelatihan->countpelatihanditolak($this->session->userdata('id'));
    	/*$proses = $this->model_narasumber->input_narasumber();
    	if($proses){
    		$result = array('result' => 'success');
    	}else{
    		$result = array('result' => 'failed');
    	}*/

    	$this->form_validation->set_rules('tgl_pelatihan', 'tgl_pelatihan', 'required');

		if ($this->form_validation->run() == FALSE)
		{
			$this->session->set_flashdata('status', '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.validation_errors().'</div>');
				redirect('lpp/input_narasumber/'.$id,$data);
		}else{
			$data = $this->model_narasumber->input_narasumber();
			if($data){
				//$data['status'] = "success";
				$this->session->set_flashdata('status', '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Narasumber berhasil ditambahkan</div>');

				
				redirect('lpp/input_narasumber/'.$id,$data);
			}
		}
    }
    public function schedule_pelatihan()
    {
		$data['notifikasi_row'] = $this->model_pelatihan->countNotifNarasumber($this->session->userdata('id'));
        $data['notifikasi_schedule'] = $this->model_pelatihan->countpelatihanbaru($this->session->userdata('id'));
        $data['notifikasi_ditolak'] = $this->model_pelatihan->countpelatihanditolak($this->session->userdata('id'));
    	$data['username'] = $this->session->userdata('username');
		$data['role'] = $this->session->userdata('role');
		$id_lpp = $this->session->userdata('id');

		$data['tema_pelatihan'] = $this->model_pelatihan->getAllTemaPelatihan();
		$data['menu'] = "schedule";
		$data['semua_pelatihan'] = $this->model_pelatihan->getAllSchedulePelatihanByIdLpp($id_lpp);

		$this->load->view('layout_admin/header',$data);
		$this->load->view('lpp/semua_pelatihan',$data);
		$this->load->view('layout_admin/footer');
    }
    public function semua_pelatihan()
    {
		$data['notifikasi_row'] = $this->model_pelatihan->countNotifNarasumber($this->session->userdata('id'));
        $data['notifikasi_schedule'] = $this->model_pelatihan->countpelatihanbaru($this->session->userdata('id'));
        $data['notifikasi_ditolak'] = $this->model_pelatihan->countpelatihanditolak($this->session->userdata('id'));
    	$data['username'] = $this->session->userdata('username');
		$data['role'] = $this->session->userdata('role');
		$id_lpp = $this->session->userdata('id');

		$data['tema_pelatihan'] = $this->model_pelatihan->getAllTemaPelatihan();
		$data['menu'] = "pelatihan";
		$data['semua_pelatihan'] = $this->model_pelatihan->getAllPelatihanByIdLpp($id_lpp);

		$this->load->view('layout_admin/header',$data);
		$this->load->view('lpp/semua_pelatihan',$data);
		$this->load->view('layout_admin/footer');
    }
    public function arsip_pelatihan()
    {
		$data['notifikasi_row'] = $this->model_pelatihan->countNotifNarasumber($this->session->userdata('id'));
        $data['notifikasi_schedule'] = $this->model_pelatihan->countpelatihanbaru($this->session->userdata('id'));
        $data['notifikasi_ditolak'] = $this->model_pelatihan->countpelatihanditolak($this->session->userdata('id'));
    	$data['username'] = $this->session->userdata('username');
		$data['role'] = $this->session->userdata('role');
		$id_lpp = $this->session->userdata('id');

		$data['tema_pelatihan'] = $this->model_pelatihan->getAllTemaPelatihan();
		$data['menu'] = "arsip";
		$data['semua_pelatihan'] = $this->model_pelatihan->getArsipPelatihanByIdLpp($id_lpp);

		$this->load->view('layout_admin/header',$data);
		$this->load->view('lpp/semua_pelatihan',$data);
		$this->load->view('layout_admin/footer');
    }
    public function pelatihan_ditolak()
    {
		$data['notifikasi_row'] = $this->model_pelatihan->countNotifNarasumber($this->session->userdata('id'));
        $data['notifikasi_schedule'] = $this->model_pelatihan->countpelatihanbaru($this->session->userdata('id'));
        $data['notifikasi_ditolak'] = $this->model_pelatihan->countpelatihanditolak($this->session->userdata('id'));
    	$data['username'] = $this->session->userdata('username');
		$data['role'] = $this->session->userdata('role');
		$id_lpp = $this->session->userdata('id');

		$data['tema_pelatihan'] = $this->model_pelatihan->getAllTemaPelatihan();
		$data['menu'] = "arsip";
		$data['semua_pelatihan'] = $this->model_pelatihan->getAllPelatihanDitolakByIdLpp($id_lpp);

		$data['ditolak'] = "ditolak";

		$tolak = $this->model_request_pelatihan->setReadAll(1);

		if($tolak){
			$this->load->view('layout_admin/header',$data);
			$this->load->view('lpp/semua_pelatihan',$data);
			$this->load->view('layout_admin/footer');
		}
		
    }
    public function daftar()
    {
		$data['notifikasi_row'] = $this->model_pelatihan->countNotifNarasumber($this->session->userdata('id'));
        $data['notifikasi_schedule'] = $this->model_pelatihan->countpelatihanbaru($this->session->userdata('id'));
        $data['notifikasi_ditolak'] = $this->model_pelatihan->countpelatihanditolak($this->session->userdata('id'));
    	$this->load->view('frontend/header');
		$this->load->view('frontend/daftar_lpp');
		$this->load->view('frontend/footer');
    }
    public function upload_csv_peserta($id)
    {
		$data['notifikasi_row'] = $this->model_pelatihan->countNotifNarasumber($this->session->userdata('id'));
        $data['notifikasi_schedule'] = $this->model_pelatihan->countpelatihanbaru($this->session->userdata('id'));
        $data['notifikasi_ditolak'] = $this->model_pelatihan->countpelatihanditolak($this->session->userdata('id'));
    			$config['upload_path'] = 'assets/csv';
		        //$config['allowed_types'] = 'csv';
		        $config['allowed_types'] = 'csv|xls|xlsx|csvexcel';
		        //text/comma-separated-values|application/csv|application/excel|application/vnd.ms-excel|application/vnd.msexcel|text/anytext
		        $config['max_size'] = '100';
		        $config['max_width']  = '1024';
		        $config['max_height']  = '768';

		        $this->load->library('upload', $config);
                $this->upload->initialize($config);

		        if ( ! $this->upload->do_upload('csvfile'))
		        {
		            $error = array('error' => $this->upload->display_errors());
		            //var_dump($error);
		            $this->session->set_flashdata('status', '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> File CSV Tidak Boleh Kosong</div>');
						redirect('lpp/input_peserta/'.$id);
		        }
		        else
		        {
		            $upload_data = $this->upload->data();
		            
		            // start to read the CSV file
		            $this->load->library('csvreader');
		            $file_path = $upload_data['full_path'];
		            $csv_data = $this->csvreader->parse_file($file_path); 
		            $csv_fields = $this->csvreader->get_fields();
		            //echo "hae";
		                if($csv_fields) // match to one of database table
		                {
		                    foreach($csv_data as $row){
                                $id_peserta = $row['ID_PESERTA'];
                                $id_pelatihan = $id;
                                $nama_lengkap = $row['NAMA_LENGKAP'];
                                $gelar_akademis = $row['GELAR_AKADEMIS'];
                                $jenis_kelamin = $row['JENIS_KELAMIN'];
                                $NIP = $row['NIP'];
                                $no_ktp = $row['NO_KTP'];
                                $tempat_lahir = $row['TEMPAT_LAHIR'];
                                $tgl_lahir = $row['TGL_LAHIR'];
                                $pendidikan_terakhir = $row['PENDIDIKAN_TERAKHIR'];
                                $status_kepegawaian = $row['STATUS_KEPEGAWAIAN'];
                                $instansi = $row['INSTANSI'];
                                $lembaga = $row['LEMBAGA'];
                                $domisili_kabupaten = $row['DOMISILI_KABUPATEN'];
                                $domisili_provinsi = $row['DOMISILI_PROVINSI'];
                                $no_telepon = $row['NO_TELEPON'];
                                $alamat_email = $row['ALAMAT_EMAIL'];
                                $pengalaman_kerja_bidang_pengadaan_barang_pemerintah = $row['PENGALAMAN_KERJA_BIDANG_PENGADAAN_BARANG_PEMERINTAH'];
                                $pernah_mengikuti_pelatihan_barang_jasa = $row['PERNAH_MENGIKUTI_PELATIHAN_BARANG_JASA'];
                                $berapa_kali = $row['BERAPA_KALI'];
                                $alasan_mengikuti = $row['ALASAN_MENGIKUTI'];

                                //insert ke table peserta
						        $proses_tbl_peserta = $this->db->query("INSERT INTO tbl_peserta VALUES('$id_peserta','$nama_lengkap','$gelar_akademis','$jenis_kelamin','$NIP','$no_ktp','$tempat_lahir','$tgl_lahir','$pendidikan_terakhir','$status_kepegawaian','$instansi','$lembaga','$domisili_kabupaten','$domisili_provinsi','$no_telepon','$alamat_email','$pengalaman_kerja_bidang_pengadaan_barang_pemerintah','$pernah_mengikuti_pelatihan_barang_jasa','$berapa_kali','$alasan_mengikuti','$id_pelatihan','','')");

						        //$proses_peserta_pelatihan = $this->db->query("INSERT INTO pelatihan_peserta VALUES('','$id_peserta','$id_pelatihan','','')");
		                    }

		                    if(($proses_tbl_peserta)){
									//$data['status'] = "success";
									$this->session->set_flashdata('status', '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Peserta berhasil ditambahkan</div>');

									
									redirect('lpp/input_peserta/'.$id);
								}
		                }
		            //}
		            
		            /*$data = array(
		                'upload_data' => $upload_data,
		                'csv_data' => $csv_data,
		            );*/
		            
		            //$this->load->view('upload_success', $data);
		        }
    }
    public function lpp($id)
    {
		$data['notifikasi_row'] = $this->model_pelatihan->countNotifNarasumber($this->session->userdata('id'));
        $data['notifikasi_schedule'] = $this->model_pelatihan->countpelatihanbaru($this->session->userdata('id'));
        $data['notifikasi_ditolak'] = $this->model_pelatihan->countpelatihanditolak($this->session->userdata('id'));
    	$data['username'] = $this->session->userdata('username');
		$data['role'] = $this->session->userdata('role');

		$data['tema_pelatihan'] = $this->model_pelatihan->getAllTemaPelatihan();

		$data['id'] = $id;

		$data['detail'] = $this->model_pelatihan->getTemaPelatihanByIdRequest($id);
		$data['semua_peserta'] = $this->model_peserta->getAllPesertaByIdPelatihan($id);

		$data['lpp'] = $this->model_pelatihan->getlpp();

		$row = $this->model_pelatihan->count_lpp_by_pelatihan($id);

		$data['row_lpp'] = $row;
		
		$id_pelatihan = $this->model_lpp->getIdLppFromIdPelatihan($id);
		$data['akreditasi'] = $this->model_lpp->getAkreditasiLpp($id_pelatihan);

		if($row!=0){
			$data['data_lpp'] = $this->model_pelatihan->get_lpp_by_pelatihan($id);
		}

		$this->load->view('layout_admin/header',$data);
		$this->load->view('lpp/menu_detail_pelatihan');
		/*$this->load->view('lpp/input_peserta',$data);*/		
		$this->load->view('lpp/lpp',$data);
		$this->load->view('layout_admin/footer');
    }
    public function proses_input_lpp()
    {
		$data['notifikasi_row'] = $this->model_pelatihan->countNotifNarasumber($this->session->userdata('id'));
        $data['notifikasi_schedule'] = $this->model_pelatihan->countpelatihanbaru($this->session->userdata('id'));
        $data['notifikasi_ditolak'] = $this->model_pelatihan->countpelatihanditolak($this->session->userdata('id'));
    	$data = $this->model_pelatihan->insert_new_lpp();
    	$id = $this->input->post('id_pelatihan');

		if($data){
			
			$this->session->set_flashdata('status', '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>lpp berhasil ditambahkan</div>');

			redirect('lpp/lpp/'.$id,$data);
		}
    }


    public function lihat_narasumber($id)
    {
		$data['notifikasi_row'] = $this->model_pelatihan->countNotifNarasumber($this->session->userdata('id'));
        $data['notifikasi_schedule'] = $this->model_pelatihan->countpelatihanbaru($this->session->userdata('id'));
        $data['notifikasi_ditolak'] = $this->model_pelatihan->countpelatihanditolak($this->session->userdata('id'));

        $data['row_narasumber'] = $this->model_pelatihan->countRowTable('pelatihan_narasumber','id_pelatihan',$id);
        $data['row_peserta'] = $this->model_pelatihan->countRowTable('tbl_peserta','id_pelatihan',$id);
        $data['row_eval_penyelenggara'] = $this->model_pelatihan->countRowTable('evaluasi_penyelenggara','id_pelatihan',$id);
        $data['row_eval_narasumber'] = $this->model_pelatihan->countRowTable('evaluasi_narasumber','id_pelatihan',$id);

    	$data['username'] = $this->session->userdata('username');
		$data['role'] = $this->session->userdata('role');
		$data['id_user'] = $this->session->userdata('id');

		$data['id'] = $id;

		$data['detail'] = $this->model_pelatihan->getPelatihanByIdPelatihan($id);

		$dari_tanggal = $this->model_pelatihan->getTanggalDariPelatihanByIdPelatihan($id);
		$sampai_tanggal = $this->model_pelatihan->getTanggalSampaiPelatihanByIdPelatihan($id);

		//$data['narasumber'] = $this->model_narasumber->getNarasumberAvailable($dari_tanggal,$sampai_tanggal);
		$data['narasumber'] = $this->model_narasumber->getAbsolutelyAllNarasumber();
		$data['semua_narasumber'] = $this->model_narasumber->getAllNarasumberByRealIdPelatihan($id);

		$id_pelatihan = $this->model_lpp->getIdLppFromIdPelatihan($id);
		$data['akreditasi'] = $this->model_lpp->getAkreditasiLpp($id_pelatihan);

		$this->load->view('layout_admin/header',$data);
		$this->load->view('lpp/menu_detail_pelatihan');
		$this->load->view('lpp/lihat_narasumber',$data);
		$this->load->view('layout_admin/footer');
    }

    public function request_narasumber($id)
    {
		$data['notifikasi_row'] = $this->model_pelatihan->countNotifNarasumber($this->session->userdata('id'));
        $data['notifikasi_schedule'] = $this->model_pelatihan->countpelatihanbaru($this->session->userdata('id'));
        $data['notifikasi_ditolak'] = $this->model_pelatihan->countpelatihanditolak($this->session->userdata('id'));
    	$data['username'] = $this->session->userdata('username');
		$data['role'] = $this->session->userdata('role');
		$data['id_user'] = $this->session->userdata('id');

		$data['id'] = $id;

		$data['detail'] = $this->model_pelatihan->getPelatihanByIdPelatihan($id);

		$dari_tanggal = $this->model_pelatihan->getTanggalDariPelatihanByIdPelatihan($id);
		$sampai_tanggal = $this->model_pelatihan->getTanggalSampaiPelatihanByIdPelatihan($id);

		//$data['narasumber'] = $this->model_narasumber->getNarasumberAvailable($dari_tanggal,$sampai_tanggal);
		$data['narasumber'] = $this->model_narasumber->getAbsolutelyAllNarasumber();
		$data['semua_narasumber'] = $this->model_narasumber->getAllNarasumberByRealIdPelatihan($id);

		$id_pelatihan = $this->model_lpp->getIdLppFromIdPelatihan($id);
		$data['akreditasi'] = $this->model_lpp->getAkreditasiLpp($id_pelatihan);

		$this->load->view('layout_admin/header',$data);
		$this->load->view('lpp/menu_detail_pelatihan');
		$this->load->view('lpp/request_narasumber',$data);
		$this->load->view('layout_admin/footer');
    }
    public function tambah_pelatihan()
    {
		$data['notifikasi_row'] = $this->model_pelatihan->countNotifNarasumber($this->session->userdata('id'));
        $data['notifikasi_schedule'] = $this->model_pelatihan->countpelatihanbaru($this->session->userdata('id'));
        $data['notifikasi_ditolak'] = $this->model_pelatihan->countpelatihanditolak($this->session->userdata('id'));
    	$data['username'] = $this->session->userdata('username');
    	$data['id_user'] = $this->session->userdata('id');
		$data['role'] = $this->session->userdata('role');

		$data['notiftotalPending'] = $this->model_request_pelatihan->countNotifPendingRequest();
		$data['notiflisttotalPending'] = $this->model_request_pelatihan->getNotifPendingRequest();

		$data['tema_pelatihan'] = $this->model_pelatihan->getAllTemaPelatihan();

		$data['lpp'] = $this->model_lpp->getAllLpp();

		$data['nama_lpp'] = $this->model_lpp->getLppName($this->session->userdata('id'));
		$data['schedule_pelatihan'] =  $this->model_pelatihan->getActiveSchedulePelatihanByIdLpp($this->session->userdata('id'));

    	$this->load->view('layout_admin/header',$data);
		$this->load->view('lpp/tambah_pelatihan');
		$this->load->view('layout_admin/footer');
    }
    public function tambah_schedule()
    {
		$data['notifikasi_row'] = $this->model_pelatihan->countNotifNarasumber($this->session->userdata('id'));
        $data['notifikasi_schedule'] = $this->model_pelatihan->countpelatihanbaru($this->session->userdata('id'));
        $data['notifikasi_ditolak'] = $this->model_pelatihan->countpelatihanditolak($this->session->userdata('id'));
    	$data['username'] = $this->session->userdata('username');
    	$data['id_user'] = $this->session->userdata('id');
		$data['role'] = $this->session->userdata('role');

		$data['notiftotalPending'] = $this->model_request_pelatihan->countNotifPendingRequest();
		$data['notiflisttotalPending'] = $this->model_request_pelatihan->getNotifPendingRequest();

		$data['tema_pelatihan'] = $this->model_pelatihan->getAllTemaPelatihan();

		$data['lpp'] = $this->model_lpp->getAllLpp();

		$data['nama_lpp'] = $this->model_lpp->getLppName($this->session->userdata('id'));
		//$data['instansi'] = $this->model_lpp->getInstansiLpp($this->session->userdata('id'));

    	$this->load->view('layout_admin/header',$data);
		$this->load->view('lpp/tambah_schedule');
		$this->load->view('layout_admin/footer');
    }
    public function proses_request_pelatihan()
	{
		$data = $this->model_request_pelatihan->approveRequest();
			if($data=="success_approve"){
				//$data['status'] = "success";
				$this->session->set_flashdata('status', '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Request telah diterima, pelatihan berhasil di buat</div>');
				redirect('lpp/index',$data);
			}else if($data=="success_tolak"){
				//$data['status'] = "success";
				$this->session->set_flashdata('status', '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Request telah ditolak, pelatihan berhasil ditolak</div>');
				redirect('lpp/index',$data);
			}else{
				//$data['status'] = "failed";
				$this->session->set_flashdata('status', '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>gagal memasukan data, coba beberapa saat lagi</div>');
				redirect('lpp/index',$data);
			}
	}
	public function proses_input_peserta_manual($id)
	{
		/*$data = $this->model_request_pelatihan->proses_input_peserta_manual();
			if($data){
				//$data['status'] = "success";
				$this->session->set_flashdata('status', '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Request telah diterima, pelatihan berhasil di buat</div>');
				redirect('lpp/input_peserta',$data);
			}else{
				//$data['status'] = "failed";
				$this->session->set_flashdata('status', '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>gagal memasukan data, coba beberapa saat lagi</div>');
				redirect('lpp/input_peserta',$data);
			}*/
		$data = $this->model_pelatihan->insert_peserta($id);
			if($data){
				//$data['status'] = "success";
				$this->session->set_flashdata('status', '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Narasumber berhasil ditambahkan</div>');

				
				redirect('lpp/input_peserta/'.$id,$data);
			}
	}
	public function detail_peserta($id,$id_pelatihan)
    {
    	$data['notifikasi_row'] = $this->model_pelatihan->countNotifNarasumber($this->session->userdata('id'));
        $data['notifikasi_schedule'] = $this->model_pelatihan->countpelatihanbaru($this->session->userdata('id'));
        $data['notifikasi_ditolak'] = $this->model_pelatihan->countpelatihanditolak($this->session->userdata('id'));
    	$data['username'] = $this->session->userdata('username');
		$data['role'] = $this->session->userdata('role');
		
		$data['akreditasi'] = $this->model_lpp->getAkreditasiLpp($id_pelatihan);

		
        $data['row_narasumber'] = $this->model_pelatihan->countRowTable('pelatihan_narasumber','id_pelatihan',$id_pelatihan);
        $data['row_peserta'] = $this->model_pelatihan->countRowTable('tbl_peserta','id_pelatihan',$id_pelatihan);
        $data['row_eval_penyelenggara'] = $this->model_pelatihan->countRowTable('evaluasi_penyelenggara','id_pelatihan',$id_pelatihan);
        $data['row_eval_narasumber'] = $this->model_pelatihan->countRowTable('evaluasi_narasumber','id_pelatihan',$id_pelatihan);
        
		$data['id'] = $id_pelatihan;

		$data['judul_pelatihan'] = $this->model_pelatihan->getJudulPelatihan($id_pelatihan);
		$data['detail'] = $this->model_pelatihan->getTemaPelatihanByIdRequest($id_pelatihan);
		$data['detail_peserta'] = $this->model_peserta->getDetailPeserta($id);
        $data['total_request'] = $this->model_pelatihan->newRequestFasilitator($this->session->userdata('id'));

		$this->load->view('layout_admin/header',$data);
		$this->load->view('lpp/menu_detail_pelatihan');
		/*$this->load->view('lpp/input_peserta',$data);*/		
		$this->load->view('lpp/detail_peserta',$data);
		$this->load->view('layout_admin/footer');
    }
	public function delete_peserta($id,$id_pelatihan)
    {
    	$data = $this->model_peserta->delete('tbl_peserta','id_peserta',$id);
    	if($data){
				$this->session->set_flashdata('status', '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Data Berhasil di Hapus</div>');
				redirect('lpp/input_peserta/'.$id_pelatihan,$data);
			}else{
				$this->session->set_flashdata('status', '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$data.'</div>');
				redirect('lpp/input_peserta/'.$id_pelatihan,$data);
			}
    }
	public function delete_schedule($id)
    {
    	$data = $this->model_peserta->delete('tbl_schedule_pelatihan','id_pelatihan',$id);
    	if($data){
				$this->session->set_flashdata('status', '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Data Berhasil di Hapus</div>');
				redirect('lpp/schedule_pelatihan/',$data);
			}else{
				$this->session->set_flashdata('status', '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$data.'</div>');
				redirect('lpp/schedule_pelatihan/',$data);
			}
    }

    public function proses_update_peserta($id_pelatihan)
    {
    	$data = $this->model_peserta->updatePeserta($id_pelatihan);
			if($data){
				//$data['status'] = "success";
				$this->session->set_flashdata('status', '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Narasumber berhasil ditambahkan</div>');

				
				redirect('lpp/input_peserta/'.$id_pelatihan,$data);
			}
    }
	public function proses_request_schedule()
	{
		$data = $this->model_request_pelatihan->approveRequestSchedule();
		if($data=="success_approve"){
				//$data['status'] = "success";
				$this->session->set_flashdata('status', '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Request telah diterima, pelatihan berhasil di buat</div>');
				redirect('lpp/schedule_pelatihan',$data);
			}else if($data=="success_tolak"){
				//$data['status'] = "success";
				$this->session->set_flashdata('status', '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Request telah ditolak, pelatihan berhasil ditolak</div>');
				redirect('lpp/schedule_pelatihan',$data);
			}else{
				//$data['status'] = "failed";
				$this->session->set_flashdata('status', '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>gagal memasukan data, coba beberapa saat lagi '.$data.'</div>');
				redirect('lpp/schedule_pelatihan',$data);
			}
	}

	public function fasilitator($id)
    {
		$data['notifikasi_row'] = $this->model_pelatihan->countNotifNarasumber($this->session->userdata('id'));
        $data['notifikasi_schedule'] = $this->model_pelatihan->countpelatihanbaru($this->session->userdata('id'));
        $data['notifikasi_ditolak'] = $this->model_pelatihan->countpelatihanditolak($this->session->userdata('id'));
    	$data['username'] = $this->session->userdata('username');
		$data['role'] = $this->session->userdata('role');

		$data['tema_pelatihan'] = $this->model_pelatihan->getAllTemaPelatihan();

		$data['id'] = $id;

		$data['detail'] = $this->model_pelatihan->getTemaPelatihanByIdRequest($id);
		$data['semua_peserta'] = $this->model_peserta->getAllPesertaByIdPelatihan($id);

		$data['fasilitator'] = $this->model_pelatihan->getFasilitator();

		$row = $this->model_pelatihan->count_fasilitator_by_pelatihan($id);

		$data['row_fasilitator'] = $row;

		if($row!=0){
			$data['data_fasilitator'] = $this->model_pelatihan->get_fasilitator_by_pelatihan($id);
		}

		$id_pelatihan = $this->model_lpp->getIdLppFromIdPelatihan($id);
		$data['akreditasi'] = $this->model_lpp->getAkreditasiLpp($id_pelatihan);

		$this->load->view('layout_admin/header',$data);
		$this->load->view('lpp/menu_detail_pelatihan');
		/*$this->load->view('lpp/input_peserta',$data);*/		
		$this->load->view('lpp/fasilitator',$data);
		$this->load->view('layout_admin/footer');
    }
    public function proses_input_fasilitator()
    {
		$data['notifikasi_row'] = $this->model_pelatihan->countNotifNarasumber($this->session->userdata('id'));
        $data['notifikasi_schedule'] = $this->model_pelatihan->countpelatihanbaru($this->session->userdata('id'));
        $data['notifikasi_ditolak'] = $this->model_pelatihan->countpelatihanditolak($this->session->userdata('id'));
    	$data = $this->model_pelatihan->insert_narasumber_fasilitator();
    	$id = $this->input->post('id_pelatihan');

		if($data){
			
			$this->session->set_flashdata('status', '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>fasilitator berhasil ditambahkan</div>');

			redirect('lpp/fasilitator/'.$id,$data);
		}
    }

    public function proses_request_pelatihan_schedule()
    {
		$data['notifikasi_row'] = $this->model_pelatihan->countNotifNarasumber($this->session->userdata('id'));
        $data['notifikasi_schedule'] = $this->model_pelatihan->countpelatihanbaru($this->session->userdata('id'));
        $data['notifikasi_ditolak'] = $this->model_pelatihan->countpelatihanditolak($this->session->userdata('id'));
    	$data = $this->model_pelatihan->create_pelatihan_berdasarkan_schedule();

		if($data=="success_approve"){
			
			$this->session->set_flashdata('status', '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>fasilitator berhasil ditambahkan</div>');

			redirect('lpp/semua_pelatihan/',$data);
		}
    }
    public function profil()
    {
		$data['notifikasi_row'] = $this->model_pelatihan->countNotifNarasumber($this->session->userdata('id'));
        $data['notifikasi_schedule'] = $this->model_pelatihan->countpelatihanbaru($this->session->userdata('id'));
        $data['notifikasi_ditolak'] = $this->model_pelatihan->countpelatihanditolak($this->session->userdata('id'));
    	$data['username'] = $this->session->userdata('username');
		$data['role'] = $this->session->userdata('role');
		$id_lpp = $this->session->userdata('id');

		$data['profil_lpp'] = $this->model_lpp->getDetailLpp($this->session->userdata('id'));

		$this->load->view('layout_admin/header',$data);
		$this->load->view('lpp/profil',$data);
		$this->load->view('layout_admin/footer');
    }
    public function library()
	{
		$data['notifikasi_row'] = $this->model_pelatihan->countNotifNarasumber($this->session->userdata('id'));
        $data['notifikasi_schedule'] = $this->model_pelatihan->countpelatihanbaru($this->session->userdata('id'));
        $data['notifikasi_ditolak'] = $this->model_pelatihan->countpelatihanditolak($this->session->userdata('id'));
		$data['username'] = $this->session->userdata('username');
		$data['role'] = $this->session->userdata('role');
		
		$data['dataMateri'] = $this->model_pelatihan->get_materi_by('lpp');

		$this->load->view('layout_admin/header',$data);
		$this->load->view('lpp/library');
		$this->load->view('layout_admin/footer');
	}

	public function evaluasi_penyelenggara($id_pelatihan)
	{
		$data['notifikasi_row'] = $this->model_pelatihan->countNotifNarasumber($this->session->userdata('id'));
        $data['notifikasi_schedule'] = $this->model_pelatihan->countpelatihanbaru($this->session->userdata('id'));
        $data['notifikasi_ditolak'] = $this->model_pelatihan->countpelatihanditolak($this->session->userdata('id'));
        
        $data['row_narasumber'] = $this->model_pelatihan->countRowTable('pelatihan_narasumber','id_pelatihan',$id_pelatihan);
        $data['row_peserta'] = $this->model_pelatihan->countRowTable('tbl_peserta','id_pelatihan',$id_pelatihan);
        $data['row_eval_penyelenggara'] = $this->model_pelatihan->countRowTable('evaluasi_penyelenggara','id_pelatihan',$id_pelatihan);
        $data['row_eval_narasumber'] = $this->model_pelatihan->countRowTable('evaluasi_narasumber','id_pelatihan',$id_pelatihan);

		$data['username'] = $this->session->userdata('username');
		$data['role'] = $this->session->userdata('role');
		$id_lpp = $this->session->userdata('id');
		$data['nama_lpp'] = $this->model_lpp->getLppName($this->session->userdata('id'));
		$data['id_lpp'] = $this->session->userdata('id');
		
		$data['detail'] = $this->model_pelatihan->getPelatihanByIdPelatihan($id_pelatihan);
		$cek = $this->model_evaluasi->checkEvaluasiPenyelenggara($id_lpp,$id_pelatihan);
		
		$data['id'] = $id_pelatihan;
		$data['akreditasi'] = $this->model_lpp->getAkreditasiLpp($this->session->userdata('id'));

		$this->load->view('layout_admin/header',$data);
		$this->load->view('lpp/menu_detail_pelatihan');
		if($cek!=0){
			$data['dataEvaluasi'] = $this->model_evaluasi->getEvaluasiPenyelenggara($id_lpp,$id_pelatihan);
			$this->load->view('lpp/hasil_evaluasi_penyelenggara',$data);
		}else{
			$this->load->view('lpp/evaluasi_penyelenggara');
		}
		
		$this->load->view('layout_admin/footer');
	}

	public function proses_evaluasi_penyelenggara()
	{
		$data = $this->model_evaluasi->proses_evaluasi_penyelenggara();
    	$id = $this->input->post('id_pelatihan');

		if($data){
			
			$this->session->set_flashdata('status', '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Evaluasi berhasil ditambahkan</div>');

			redirect('lpp/evaluasi_penyelenggara/'.$id,$data);
		}
	}

	public function proses_upload_csv_evaluasi_penyelenggara($id)
	{
				$config['upload_path'] = 'assets/csv';
		        //$config['allowed_types'] = 'csv';
		        $config['allowed_types'] = 'csv|xls|xlsx|csvexcel';
		        //text/comma-separated-values|application/csv|application/excel|application/vnd.ms-excel|application/vnd.msexcel|text/anytext
		        $config['max_size'] = '100';
		        $config['max_width']  = '1024';
		        $config['max_height']  = '768';

		        $this->load->library('upload', $config);
                $this->upload->initialize($config);

		        if ( ! $this->upload->do_upload('csvfile'))
		        {
		            $error = array('error' => $this->upload->display_errors());
		            var_dump($error);
		        }
		        else
		        {
		            $upload_data = $this->upload->data();
		            
		            // start to read the CSV file
		            $this->load->library('csvreader');
		            $file_path = $upload_data['full_path'];
		            $csv_data = $this->csvreader->parse_file($file_path); 
		            $csv_fields = $this->csvreader->get_fields();
		            //echo "hae";
		                if($csv_fields) // match to one of database table
		                {
		                    foreach($csv_data as $row){
                                $id_peserta = $row['ID_PESERTA'];
                                $id_pelatihan = $id;
                                $nama_lengkap = $row['NAMA_LENGKAP'];
                                $gelar_akademis = $row['GELAR_AKADEMIS'];
                                $jenis_kelamin = $row['JENIS_KELAMIN'];
                                $NIP = $row['NIP'];
                                $no_ktp = $row['NO_KTP'];
                                $tempat_lahir = $row['TEMPAT_LAHIR'];
                                $tgl_lahir = $row['TGL_LAHIR'];
                                $pendidikan_terakhir = $row['PENDIDIKAN_TERAKHIR'];
                                $status_kepegawaian = $row['STATUS_KEPEGAWAIAN'];
                                $instansi = $row['INSTANSI'];
                                $lembaga = $row['LEMBAGA'];
                                $domisili_kabupaten = $row['DOMISILI_KABUPATEN'];
                                $domisili_provinsi = $row['DOMISILI_PROVINSI'];
                                $no_telepon = $row['NO_TELEPON'];
                                $alamat_email = $row['ALAMAT_EMAIL'];
                                $pengalaman_kerja_bidang_pengadaan_barang_pemerintah = $row['PENGALAMAN_KERJA_BIDANG_PENGADAAN_BARANG_PEMERINTAH'];
                                $pernah_mengikuti_pelatihan_barang_jasa = $row['PERNAH_MENGIKUTI_PELATIHAN_BARANG_JASA'];
                                $berapa_kali = $row['BERAPA_KALI'];
                                $alasan_mengikuti = $row['ALASAN_MENGIKUTI'];

                                //insert ke table peserta
						        
		                    }

		                    if(($proses_tbl_peserta)){
									//$data['status'] = "success";
									$this->session->set_flashdata('status', '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Peserta berhasil ditambahkan</div>');

									
									redirect('lpp/evaluasi_penyelenggara/'.$id);
							}
		                }
		        }
	}
	public function evaluasi_narasumber($id_pelatihan)
	{
		$data['notifikasi_row'] = $this->model_pelatihan->countNotifNarasumber($this->session->userdata('id'));
        $data['notifikasi_schedule'] = $this->model_pelatihan->countpelatihanbaru($this->session->userdata('id'));
        $data['notifikasi_ditolak'] = $this->model_pelatihan->countpelatihanditolak($this->session->userdata('id'));
        
        $data['row_narasumber'] = $this->model_pelatihan->countRowTable('pelatihan_narasumber','id_pelatihan',$id_pelatihan);
        $data['row_peserta'] = $this->model_pelatihan->countRowTable('tbl_peserta','id_pelatihan',$id_pelatihan);
        $data['row_eval_penyelenggara'] = $this->model_pelatihan->countRowTable('evaluasi_penyelenggara','id_pelatihan',$id_pelatihan);
        $data['row_eval_narasumber'] = $this->model_pelatihan->countRowTable('evaluasi_narasumber','id_pelatihan',$id_pelatihan);

		$data['username'] = $this->session->userdata('username');
		$data['role'] = $this->session->userdata('role');
		$id_narasumber = $this->session->userdata('id');
		$data['nama_narasumber'] = $this->model_lpp->getLppName($this->session->userdata('id'));
		$data['id_narasumber'] = $this->session->userdata('id');
		$data['id_pelatihan'] = $id_pelatihan;

		$data['semua_narasumber'] = $this->model_narasumber->getAprovedNarasumberByRealIdPelatihan($id_pelatihan);
		
		$data['id'] = $id_pelatihan;
		$data['akreditasi'] = $this->model_lpp->getAkreditasiLpp($this->session->userdata('id'));

		$this->load->view('layout_admin/header',$data);
		$this->load->view('lpp/menu_detail_pelatihan');
		$this->load->view('lpp/evaluasi_narasumber');
		$this->load->view('layout_admin/footer');
	}
	public function insert_peserta_manual($id)
    {
        $id_peserta = $this->input->post('id_peserta');
        $id_pelatihan = $id;
        $nama_lengkap = $this->input->post('nama_lengkap');
        $gelar_akademis = $this->input->post('gelar_akademis');
        $jenis_kelamin = $this->input->post('jenis_kelamin');
        $NIP = $this->input->post('NIP');
        $no_ktp = $this->input->post('no_ktp');
        $tempat_lahir = $this->input->post('tempat_lahir');
        $tgl_lahir = $this->input->post('tgl_lahir');
        $pendidikan_terakhir = $this->input->post('pendidikan_terakhir');
        $status_kepegawaian = $this->input->post('status_kepegawaian');
        $instansi = $this->input->post('instansi');
        $lembaga = $this->input->post('lembaga');
        $domisili_kabupaten = $this->input->post('domisili_kabupaten');
        $domisili_provinsi = $this->input->post('domisili_provinsi');
        $no_telepon = $this->input->post('no_telepon');
        $alamat_email = $this->input->post('alamat_email');
        $pengalaman_kerja_bidang_pengadaan_barang_pemerintah = $this->input->post('pengalaman_kerja_bidang_pengadaan_barang_pemerintah');
        $pernah_mengikuti_pelatihan_barang_jasa = $this->input->post('pernah_mengikuti_pelatihan_barang_jasa');
        $berapa_kali = $this->input->post('berapa_kali');
        $alasan_mengikuti = $this->input->post('alasan_mengikuti');

        $proses_tbl_peserta = $this->db->query("INSERT INTO tbl_peserta VALUES('$id_peserta','$nama_lengkap','$gelar_akademis','$jenis_kelamin','$NIP','$no_ktp','$tempat_lahir','$tgl_lahir','$pendidikan_terakhir','$status_kepegawaian','$instansi','$lembaga','$domisili_kabupaten','$domisili_provinsi','$no_telepon','$alamat_email','$pengalaman_kerja_bidang_pengadaan_barang_pemerintah','$pernah_mengikuti_pelatihan_barang_jasa','$berapa_kali','$alasan_mengikuti','$id_pelatihan','','')");

        if($proses_tbl_peserta){
            return true;
        }else{
            return false;
        }

    }

	public function evaluasi_narasumber_hasil($id_pelatihan)
	{
		$data['notifikasi_row'] = $this->model_pelatihan->countNotifNarasumber($this->session->userdata('id'));
        $data['notifikasi_schedule'] = $this->model_pelatihan->countpelatihanbaru($this->session->userdata('id'));
        $data['notifikasi_ditolak'] = $this->model_pelatihan->countpelatihanditolak($this->session->userdata('id'));
        
        $data['row_narasumber'] = $this->model_pelatihan->countRowTable('pelatihan_narasumber','id_pelatihan',$id_pelatihan);
        $data['row_peserta'] = $this->model_pelatihan->countRowTable('tbl_peserta','id_pelatihan',$id_pelatihan);
        $data['row_eval_penyelenggara'] = $this->model_pelatihan->countRowTable('evaluasi_penyelenggara','id_pelatihan',$id_pelatihan);
        $data['row_eval_narasumber'] = $this->model_pelatihan->countRowTable('evaluasi_narasumber','id_pelatihan',$id_pelatihan);
        
		$data['username'] = $this->session->userdata('username');
		$data['role'] = $this->session->userdata('role');
		$id_lpp = $this->session->userdata('id');
		$data['nama_lpp'] = $this->model_lpp->getLppName($this->session->userdata('id'));
		$data['id_lpp'] = $this->session->userdata('id');
		
		$data['detail'] = $this->model_pelatihan->getPelatihanByIdPelatihan($id_pelatihan);
		$data['dataEvaluasi'] = $this->model_evaluasi->getEvaluasiNarasumber($id_pelatihan);

		$data['id'] = $id_pelatihan;
		$data['akreditasi'] = $this->model_lpp->getAkreditasiLpp($this->session->userdata('id'));

		$this->load->view('layout_admin/header',$data);
		$this->load->view('lpp/menu_detail_pelatihan');
		$this->load->view('lpp/hasil_evaluasi_narasumber',$data);
		$this->load->view('layout_admin/footer');
	}

	public function evaluasi_narasumber_input($id_pelatihan,$id_narasumber)	
	{
		$data['notifikasi_row'] = $this->model_pelatihan->countNotifNarasumber($this->session->userdata('id'));
        $data['notifikasi_schedule'] = $this->model_pelatihan->countpelatihanbaru($this->session->userdata('id'));
        $data['notifikasi_ditolak'] = $this->model_pelatihan->countpelatihanditolak($this->session->userdata('id'));
        
        $data['row_narasumber'] = $this->model_pelatihan->countRowTable('pelatihan_narasumber','id_pelatihan',$id_pelatihan);
        $data['row_peserta'] = $this->model_pelatihan->countRowTable('tbl_peserta','id_pelatihan',$id_pelatihan);
        $data['row_eval_penyelenggara'] = $this->model_pelatihan->countRowTable('evaluasi_penyelenggara','id_pelatihan',$id_pelatihan);
        $data['row_eval_narasumber'] = $this->model_pelatihan->countRowTable('evaluasi_narasumber','id_pelatihan',$id_pelatihan);

		$data['username'] = $this->session->userdata('username');
		$data['role'] = $this->session->userdata('role');
		$data['id_narasumber'] = $id_narasumber;

		$data['nama_narasumber'] = $this->model_narasumber->getNarasumberName($id_narasumber);
		$data['id_pelatihan'] = $id_pelatihan;

		
		$data['detail'] = $this->model_pelatihan->getPelatihanByIdPelatihan($id_pelatihan);
		$data['semua_narasumber'] = $this->model_narasumber->getAprovedNarasumberByRealIdPelatihan($id_pelatihan);
		
		$data['id'] = $id_pelatihan;
		$data['akreditasi'] = $this->model_lpp->getAkreditasiLpp($this->session->userdata('id'));

		$this->load->view('layout_admin/header',$data);
		$this->load->view('lpp/menu_detail_pelatihan');
		$this->load->view('lpp/evaluasi_narasumber_input');
		$this->load->view('layout_admin/footer');
	}

	public function proses_evaluasi_narasumber()
	{
		$data = $this->model_evaluasi->proses_evaluasi_narasumber();
    	$id = $this->input->post('id_pelatihan');

		if($data){
			
			$this->session->set_flashdata('status', '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Evaluasi berhasil ditambahkan</div>');

			redirect('lpp/evaluasi_narasumber/'.$id,$data);
		}
	}

	public function proses_update_lpp()
	{
		
			$data = $this->model_lpp->update_data_lpp();
			if($data){
				//$data['status'] = "success";
				$this->session->set_flashdata('status', '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Data Berhasil diupdate</div>');
				redirect('lpp/profil',$data);
			}
		
	}

	public function minta_fasilitator($id_pelatihan)
	{
		$this->db->query("UPDATE tbl_pelatihan SET minta_fasilitator ='1' WHERE id_pelatihan='$id_pelatihan' ");
		redirect('lpp/input_narasumber/'.$id_pelatihan);
	}

}