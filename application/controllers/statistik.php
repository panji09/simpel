<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Statistik extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        $this->load->model('model_pelatihan');
        $this->load->model('model_lpp');
        $this->load->model('model_narasumber');
        $this->load->model('model_peserta');
        $this->load->model('model_request_pelatihan');
        $this->load->model('model_berita');
        $this->load->helper('tgl_indonesia');
        $this->load->library('MY_Upload');
    }
    
    public function index()
    {
        $data['headerPengumuman'] = $this->model_berita->getheaderPengumuman();
        $data['narsum_berdasarkan_instansi'] = $this->main_model->get_query("SELECT instansi_narsum, COUNT(*) AS jumlah_narasumber FROM tbl_narasumber_new GROUP BY instansi_narsum");

        $data['narsum_berdasarkan_lokasi'] = $this->main_model->get_query("SELECT lokasi, COUNT(*) AS jumlah_narasumber FROM tbl_narasumber_new GROUP BY lokasi");

        $data['narsum_berdasarkan_gender'] = $this->main_model->get_query("SELECT jeniskel_narasumber, COUNT(*) AS jumlah_narasumber FROM tbl_narasumber_new GROUP BY jeniskel_narasumber");

        $data['pelatihan_berdasarkan_lpp'] = $this->main_model->get_query("SELECT tbl_lpp.nama_lpp,tbl_pelatihan.id_lpp, COUNT(*)  AS jumlah_pelatihan FROM tbl_pelatihan,tbl_lpp WHERE tbl_pelatihan.id_lpp = tbl_lpp.id_lpp GROUP BY tbl_pelatihan.id_lpp");



        $this->load->view('frontend/header',$data);
        $this->load->view('frontend/statistik');
        $this->load->view('frontend/footer');
    }

}