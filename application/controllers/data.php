<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Data extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        $this->load->model('model_pelatihan');
        $this->load->model('model_lpp');
        $this->load->model('model_narasumber');
        $this->load->model('model_peserta');
        $this->load->model('model_request_pelatihan');
        $this->load->model('model_berita');
        $this->load->helper('tgl_indonesia');
        $this->load->library('MY_Upload');
    }
	public function narasumber()
	{
        $data['headerPengumuman'] = $this->model_berita->getheaderPengumuman();
		$data['dataNarasumber'] = $this->narasumber_db->get_all(array('aktif'=>1))->result();

	   	$this->load->view('frontend/header',$data);
		$this->load->view('frontend/data_narasumber');
		$this->load->view('frontend/footer');
	}
    public function lpp()
    {
        $data['headerPengumuman'] = $this->model_berita->getheaderPengumuman();
        $data['dataLPP'] = $this->model_lpp->getLppTerakreditasi();
        $data['dataLPPTerdaftar'] = $this->model_lpp->getLppTerdaftar();
        $data['dataLPPBelumAkreditasi'] = $this->model_lpp->getLppBelumAkreditasi();
        
        //echo $this->db->last_query();
        $this->load->view('frontend/header',$data);
        $this->load->view('frontend/data_lpp');
        $this->load->view('frontend/footer');
    }
    public function find()
    {
        $data['headerPengumuman'] = $this->model_berita->getheaderPengumuman();

        $jenis = $this->input->post('jenis');
        $keyword = $this->input->post('keyword');

        if($jenis=="terakreditasi"){
            $data['dataLPP'] = $this->model_lpp->findLpp($keyword);
            $data['dataLPPTerdaftar'] = $this->model_lpp->getLppTerdaftar();
            $data['dataLPPBelumAkreditasi'] = $this->model_lpp->getLppBelumAkreditasi();
        }else if($jenis=="terdaftar"){
            $data['dataLPP'] = $this->model_lpp->getLppTerakreditasi();
            $data['dataLPPTerdaftar'] = $this->model_lpp->findLpp($keyword);
            $data['dataLPPBelumAkreditasi'] = $this->model_lpp->getLppBelumAkreditasi();
        }else if($jenis=="verifikasi"){
            $data['dataLPP'] = $this->model_lpp->getLppTerakreditasi();
            $data['dataLPPTerdaftar'] = $this->model_lpp->getLppTerdaftar();
            $data['dataLPPBelumAkreditasi'] = $this->model_lpp->findLpp($keyword);
        }
        

        $this->load->view('frontend/header',$data);
        $this->load->view('frontend/data_lpp');
        $this->load->view('frontend/footer');
    }

    public function find_narsum()
    {
        $nama_narasumber = $this->input->post('nama_narasumber');

        $data['headerPengumuman'] = $this->model_berita->getheaderPengumuman();
        $data['dataNarasumber'] = $this->narasumber_db->get_all(array('search_name' => $nama_narasumber, 'aktif' => 1))->result();

        $this->load->view('frontend/header',$data);
        $this->load->view('frontend/data_narasumber');
        $this->load->view('frontend/footer');
    }

    public function profil_narasumber($id)
    {
        $data['headerPengumuman'] = $this->model_berita->getheaderPengumuman();
        $data['dataNarasumber'] = $this->model_narasumber->getDetailNarasumber($id);

        $this->load->view('frontend/header',$data);
        $this->load->view('frontend/profil_narasumber');
        $this->load->view('frontend/footer');
    }

}