<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Jadwal extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        $this->load->model('model_pelatihan');
        $this->load->model('model_lpp');
        $this->load->model('model_narasumber');
        $this->load->model('model_peserta');
        $this->load->model('model_request_pelatihan');
        $this->load->model('model_berita');
        $this->load->helper('tgl_indonesia');
        $this->load->library('MY_Upload');
    }
	public function index()
	{
        $data['headerPengumuman'] = $this->model_berita->getheaderPengumuman();
		$data['pelatihan'] = $this->model_pelatihan->getAllPelatihan();

	   	$this->load->view('frontend/header',$data);
		$this->load->view('frontend/pelatihan');
		$this->load->view('frontend/footer');
	}

    public function get_narasumber($id)
    {
        $narasumber = $this->model_narasumber->getAprovedNarasumberByRealIdPelatihan($id);

        $data = "data :";
        $data += json_encode($narasumber);

        echo json_encode($narasumber);
    }

    public function get_lpp($id)
    {
        $lpp = $this->db->query("SELECT * FROM tbl_lpp WHERE id_lpp = '$id' ")->result();

        $data = "data :";
        $data += json_encode($lpp);

        echo json_encode($lpp);
    }


}