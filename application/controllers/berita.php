<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Berita extends CI_Controller {
	public function __construct()
    {
        parent::__construct();
        $this->load->model('model_berita');
        $this->load->model('model_narasumber');
        $this->load->model('model_pelatihan');
        $this->load->library("pagination");
        $this->load->helper('tgl_indonesia');
    }
	public function index()
	{
	   	$config = array();
        $config["base_url"] = base_url() . "berita/page";
        //HITUNG JUMLAH ROW DI TABLE BERITA//
        $config["total_rows"] = $this->model_narasumber->record_count('tbl_berita');
        $config["per_page"] = 5;
        $config["uri_segment"] = 3;
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $data["links"] = $this->pagination->create_links();
        $data['page'] = "berita";
		$data['dataBerita'] = $this->model_berita->getBerita($config["per_page"], $page);
        $data['headerPengumuman'] = $this->model_berita->getheaderPengumuman();

		$data['dataFoto'] = $this->model_pelatihan->get_foto();
		$data['dataPengumuman'] = $this->model_berita->getPengumuman();

		$this->load->view('frontend/header',$data);
		$this->load->view('frontend/berita');
		$this->load->view('frontend/footer');
	}
	public function page($page= 1)
	{
	   	$config = array();
        $config["base_url"] = base_url() . "berita/page";
        //HITUNG JUMLAH ROW DI TABLE BERITA//
        $config["total_rows"] = $this->model_narasumber->record_count('tbl_berita');
        $config["per_page"] = 5;
        $config["uri_segment"] = 3;
        $this->pagination->initialize($config);
        $data["links"] = $this->pagination->create_links();
        $data['page'] = "berita";
		$data['dataBerita'] = $this->model_berita->getBerita($config["per_page"], $page);
        $data['headerPengumuman'] = $this->model_berita->getheaderPengumuman();

		$data['dataFoto'] = $this->model_pelatihan->get_foto();
		$data['dataPengumuman'] = $this->model_berita->getPengumuman();

		$this->load->view('frontend/header',$data);
		$this->load->view('frontend/berita');
		$this->load->view('frontend/footer');
	}

	public function detail($judul)
	{
		$data['dataBerita'] = $this->model_berita->getDetailBerita($judul);
		$data['dataBeritaLain'] = $this->model_berita->getBeritaLainya($judul);
		$data['headerBerita'] = $this->model_berita->getheaderBerita($judul);
        $data['headerPengumuman'] = $this->model_berita->getheaderPengumuman();

		$data['dataFoto'] = $this->model_pelatihan->get_foto();
		$data['dataPengumuman'] = $this->model_berita->getPengumuman();
		$this->load->view('frontend/header',$data);
		$this->load->view('frontend/detail_berita');
		$this->load->view('frontend/footer');
	}

	public function find($page= 1)
	{
		$keyword = $this->input->post('keyword');
	   	$config = array();
        $config["base_url"] = base_url() . "berita/page";
        //HITUNG JUMLAH ROW DI TABLE BERITA//
        $config["total_rows"] = $this->model_narasumber->record_count('tbl_berita');
        $config["per_page"] = 5;
        $config["uri_segment"] = 3;
        $this->pagination->initialize($config);
        $data["links"] = $this->pagination->create_links();
        $data['page'] = "berita";
		$data['dataBerita'] = $this->model_berita->findBerita($config["per_page"], $page,$keyword);
        $data['headerPengumuman'] = $this->model_berita->getheaderPengumuman();

		$data['dataFoto'] = $this->model_pelatihan->get_foto();
		$data['dataPengumuman'] = $this->model_berita->getPengumuman();

		$this->load->view('frontend/header',$data);
		$this->load->view('frontend/berita');
		$this->load->view('frontend/footer');
	}

}