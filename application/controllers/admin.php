<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
    {
        parent::__construct();
        $this->load->model('app_model');
        /*if( ($this->session->userdata('logged_in')!=1) )
        {
            redirect('admin/login');
        }*/
    }
	public function index()
	{
		$this->load->view('login/header');
		$this->load->view('login');
		$this->load->view('login/footer');
	}
	public function login()
	{
		$form_data = $this->input->post('data');
		//var_dump($form_data);
		
        if (!empty($form_data))
        {
            if ($this->app_model->login($form_data['username'], $form_data['password'], $form_data['role']))
            {
                //redirect('administrator');
                $role = $this->session->userdata('role');
                if($role == "lkpp"){
                	redirect('lkpp');
                }else if($role == "lpp"){
                	redirect('lpp');
                }else if($role == "narasumber"){
                	redirect('narasumber');
                }else if($role == "fasilitator"){
                    redirect('fasilitator');
                }
            }
            else
            {
                //echo $this->db->last_query();
                //exit;
                redirect('admin/');
            }
        }

		$this->load->view('login/header');
		$this->load->view('login');
		$this->load->view('login/footer');
	} 
    public function logout()
    {
        $this->app_model->logout();
        $this->login();
    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */