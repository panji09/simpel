<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Unduh extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        $this->load->model('model_pelatihan');
        $this->load->model('model_lpp');
        $this->load->model('model_narasumber');
        $this->load->model('model_peserta');
        $this->load->model('model_request_pelatihan');
        $this->load->model('model_berita');
        $this->load->helper('tgl_indonesia');
        $this->load->library('MY_Upload');
    }
    public function index()
    {
        $data['dataDokumen'] = $this->model_pelatihan->get_dokumen();
        $data['headerPengumuman'] = $this->model_berita->getheaderPengumuman();
        $data['kategori_dokumen'] = $this->main_model->get_query('SELECT * FROM tbl_dokumen GROUP BY kategori_dokumen');

        $this->load->view('frontend/header',$data);
        $this->load->view('frontend/unduh');
        $this->load->view('frontend/footer');
    }
    public function kategori()
    {
        $kategori = $this->input->post('kategori');
        $data['dataDokumen'] = $this->main_model->get_list_where('tbl_dokumen',array('kategori_dokumen'=>$kategori));
        $data['kategori_dokumen'] = $this->main_model->get_query('SELECT * FROM tbl_dokumen GROUP BY kategori_dokumen');
        $data['headerPengumuman'] = $this->model_berita->getheaderPengumuman();

        $this->load->view('frontend/header',$data);
        $this->load->view('frontend/unduh');
        $this->load->view('frontend/footer');
    }

}