<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Daftar_lpp extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        $this->load->model('model_pelatihan');
        $this->load->model('model_lpp');
        $this->load->model('model_narasumber');
        $this->load->model('model_peserta');
        $this->load->model('model_request_pelatihan');
        $this->load->model('model_berita');
        $this->load->helper('tgl_indonesia');
        $this->load->library('MY_Upload');
    }
	public function index()
	{
        $data['headerPengumuman'] = $this->model_berita->getheaderPengumuman();
	   	$this->load->view('frontend/header',$data);
		$this->load->view('frontend/daftar_lpp');
		$this->load->view('frontend/footer');
	}
	
	function save_lpp($file_dokumen){
	    $data_lpp = array();
	    $data_lpp['nama_lpp'] = $this->input->post('nama_lpp');
	    $data_lpp['penanggungjawab'] = $this->input->post('penanggungjawab');
	    $data_lpp['jabatan'] = $this->input->post('jabatan');
	    $data_lpp['no_ktp'] = $this->input->post('no_ktp');

	    $data_lpp['program'] = $this->input->post('program');
	    $data_lpp['alamat_lpp'] = $this->input->post('alamat_lpp');
	    $data_lpp['no_telp_lpp'] = $this->input->post('no_telp_lpp');
	    $data_lpp['no_fax'] = $this->input->post('no_fax');
	    $data_lpp['email'] = $this->input->post('email');
	    $data_lpp['password'] = base64_encode(md5($this->input->post('password')));

	    $data_lpp['kontak_nama'] = $this->input->post('kontak_nama');
	    $data_lpp['kontak_tlp'] = $this->input->post('kontak_telp');
	    $data_lpp['kontak_email'] = $this->input->post('kontak_email');

	    $data_lpp['lampiran_ktp'] = $file_dokumen['lampiran_ktp'];

	    if(isset($file_dokumen['surat_ijin_operasional'])){
		$data_lpp['surat_ijin_operasional'] = $file_dokumen['surat_ijin_operasional'];
	    }

	    if(isset($file_dokumen['surat_ket_domisili'])){
		$data_lpp['surat_ket_domisili'] = $file_dokumen['surat_ket_domisili'];
	    }

	    if(isset($file_dokumen['akta_notaris'])){
		$data_lpp['akta_notaris'] = $file_dokumen['akta_notaris'];
	    }

	    if(isset($file_dokumen['mekanisme_keuangan'])){
		$data_lpp['mekanisme_keuangan'] = $file_dokumen['mekanisme_keuangan'];
	    }

	    if(isset($file_dokumen['sop'])){
		$data_lpp['sop'] = $file_dokumen['sop'];
	    }
	    
	    if(isset($file_dokumen['surat_komitmen'])){
		$data_lpp['surat_komitmen'] = $file_dokumen['surat_komitmen'];
	    }

	    if(isset($file_dokumen['tugas_pelaksana'])){
		$data_lpp['tugas_pelaksana'] = $file_dokumen['tugas_pelaksana'];
	    }
	    
	    $data_lpp['username'] = str_replace(" ","_",$data_lpp['nama_lpp']);
	    
	    return $this->lpp_db->save(null, $data_lpp);
	}
	
	function submit(){
	    $this->form_validation->set_rules('status', '<b>Status Kelembagaan</b>', 'required');
	    $this->form_validation->set_rules('nama_lpp', '<b>Nama LPP</b>', 'required');
	    $this->form_validation->set_rules('alamat_lpp', '<b>Alamat LPP</b>', 'required');
	    $this->form_validation->set_rules('penanggungjawab', '<b>Penanggungjawab</b>', 'required');
	    $this->form_validation->set_rules('jabatan', '<b>Jabatan</b>', 'required');
	    $this->form_validation->set_rules('no_ktp', '<b>No KTP</b>', 'required');
	    $this->form_validation->set_rules('program', '<b>Program</b>', 'required');
	    $this->form_validation->set_rules('alamat_lpp', '<b>Alamat LPP</b>', 'required');
	    $this->form_validation->set_rules('no_telp_lpp', '<b>No Telp</b>', 'required');
	    $this->form_validation->set_rules('no_fax', '<b>No Fax</b>', 'required');
	    $this->form_validation->set_rules('email', '<b>Email</b>', 'required');
	    $this->form_validation->set_rules('password', '<b>Password</b>', 'required');
	    $this->form_validation->set_rules('konfirmasi_password', '<b>Konfirmasi Password</b>', 'required');
	    $this->form_validation->set_rules('kontak_nama', '<b>Kontak Person Nama</b>', 'required');
	    $this->form_validation->set_rules('kontak_telp', '<b>Kontak Person Telepon</b>', 'required');
	    $this->form_validation->set_rules('kontak_email', '<b>Kontak Person Email</b>', 'required');
	    
	    if($this->input->post('status') == 'negeri'){
		
		if (empty($_FILES['files2']['name'][0])){
		    $this->form_validation->set_rules('files2[0]', '<b>Lampiran KTP</b>', 'required');
		}
		
		if (empty($_FILES['files2']['name'][1])){
		    $this->form_validation->set_rules('files2[1]', '<b>Mekanisme Keuangan</b>', 'required');
		}
		
		if (empty($_FILES['files2']['name'][2])){
		    $this->form_validation->set_rules('files2[2]', '<b>Sistem Manajemen Mutu</b>', 'required');
		}
		
		if (empty($_FILES['files2']['name'][3])){
		    $this->form_validation->set_rules('files2[3]', '<b>Surat Pernyataan Komitmen</b>', 'required');
		}
		
		if (empty($_FILES['files2']['name'][4])){
		    $this->form_validation->set_rules('files2[4]', '<b>Tugas dan Fungsi Pelaksana Ujian</b>', 'required');
		}
		
	    }else{
		//$files = $this->input->post('files');
		
		if (empty($_FILES['files']['name'][0])){
		    $this->form_validation->set_rules('files[0]', '<b>Lampiran KTP</b>', 'required');
		}
		
		if (empty($_FILES['files']['name'][1])){
		    $this->form_validation->set_rules('files[1]', '<b>Surat Ijin Operasional</b>', 'required');
		}
		
		if (empty($_FILES['files']['name'][2])){
		    $this->form_validation->set_rules('files[2]', '<b>Surat Keterangan domisili</b>', 'required');
		}
		
		if (empty($_FILES['files']['name'][3])){
		    $this->form_validation->set_rules('files[3]', '<b>Akta Notaris</b>', 'required');
		}
		
		if (empty($_FILES['files']['name'][4])){
		    $this->form_validation->set_rules('files[4]', '<b>Sistem Manajemen Mutu</b>', 'required');
		}
		
		if (empty($_FILES['files']['name'][5])){
		    $this->form_validation->set_rules('files[5]', '<b>Surat Pernyataan Komitmen</b>', 'required');
		}
		
	    }
	    
	    if ($this->form_validation->run() == FALSE)
	    {
		    $this->session->set_flashdata('form', $this->input->post());
		    $this->session->set_flashdata('status', '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.validation_errors().'</div>');
		    redirect('daftar_lpp');
			    
	    }else{
		    //form valid
		    
		    //check email exist and approve
		    if($this->lpp_db->email_exist($this->input->post('email'))){
			$this->session->set_flashdata('status', '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> Username/Email sudah digunakan!</div>');
			redirect('daftar_lpp'); return;
		    }
		    //check password
		    if($this->input->post('password') != $this->input->post('konfirmasi_password')){
			$this->session->set_flashdata('status', '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> Konfirmasi Password tidak sama!</div>');
			redirect('daftar_lpp'); return;
		    }
		    
		    //proses benernya
		    $file_dokumen = array();
		    $image_name = array();

		    $nama = $this->input->post('nama_lpp');
		    $name_dir = str_replace(" ","_",$nama);

		    $url = "/assets/dokumen_lpp/".$name_dir;

		    $thisdir = getcwd(); 
		    
		    //echo $thisdir.$url;
		    
		    $this->upload->initialize(array(
			  "upload_path"   => $thisdir.$url,
			  "overwrite" => TRUE,
			  "encrypt_name" => TRUE,
			  "remove_spaces" => TRUE,
			  "allowed_types" => 'gif|jpg|png|doc|docx|pdf'
		    ));
		    
		    mkdir($thisdir.$url, 0755);
		    $status = $this->input->post('status');
		    
		    if($status=="swasta"){
			//Perform upload.
			$this->upload->do_multi_upload("files");
			$upload_data = $this->upload->get_multi_upload_data();
			foreach ($upload_data as $key => $value) {
			    $image_name[] = $value['file_name'];
			}

			$file_dokumen = array(
			    "lampiran_ktp" => $image_name[0],
			    "surat_ijin_operasional" => $image_name[1],
			    "surat_ket_domisili" => $image_name[2],
			    "akta_notaris" => $image_name[3],
			    "sop" => $image_name[4],
			    "surat_komitmen" => $image_name[5],
			);

			if(
			  (isset($file_dokumen['lampiran_ktp'])) &&
			  (isset($file_dokumen['surat_ijin_operasional'])) &&
			  (isset($file_dokumen['surat_ket_domisili'])) &&
			  (isset($file_dokumen['akta_notaris'])) &&
			  (isset($file_dokumen['sop'])) &&
			  (isset($file_dokumen['surat_komitmen']))
			){
			    
			    
			    if($this->save_lpp($file_dokumen)){
				//sukses
				$this->session->set_flashdata('status', '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Anda telah berhasil melakukan pendaftaran LPP yang baru. Kami akan melakukan proses verifikasi dan kami akan menginformasikan melalui email untuk proses selanjutnya</div>');
				redirect('daftar_lpp');
				
			    }else{
				//fail
				$this->session->set_flashdata('status', '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Internal Server Error</div>');
				redirect('daftar_lpp');
			    }
			}else{
			    $this->session->set_flashdata('status', '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Dokumen Harus Diisi !</div>');
			    redirect('daftar_lpp');
			    /*print_r($file_dokumen);*/
			}
			  

		    }else if($status=="negeri"){
			//Perform upload.
			if(!$this->upload->do_multi_upload("files2")){
			    $error = array('error' => $this->upload->display_errors());

			    //print_r($error);
			}
			$upload_data = $this->upload->get_multi_upload_data();
			
			foreach ($upload_data as $key => $value) {
			    $image_name[] = $value['file_name'];
			}
			
			if($image_name)
			$file_dokumen = array(
			    "lampiran_ktp" => $image_name[0],
			    "mekanisme_keuangan" => $image_name[1],
			    "sop" => $image_name[2],
			    "surat_komitmen" => $image_name[3],
			    "tugas_pelaksana" => $image_name[4],
			);

			if(
			  (isset($file_dokumen['lampiran_ktp'])) &&
			  (isset($file_dokumen['mekanisme_keuangan'])) &&
			  (isset($file_dokumen['tugas_pelaksana'])) &&
			  (isset($file_dokumen['sop'])) &&
			  (isset($file_dokumen['surat_komitmen']))
			){
			    if($this->save_lpp($file_dokumen)){
				//sukses
				$this->session->set_flashdata('status', '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Anda telah berhasil melakukan pendaftaran LPP yang baru. Kami akan melakukan proses verifikasi dan kami akan menginformasikan melalui email untuk proses selanjutnya</div>');
				redirect('daftar_lpp');
				
			    }else{
				//fail
				$this->session->set_flashdata('status', '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Internal Server Error</div>');
				//echo $this->db->last_query();
				redirect('daftar_lpp');
			    }
			}else{
			    $this->session->set_flashdata('status', '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Dokumen Harus Diisi !</div>');
			    redirect('daftar_lpp');
				    /*print_r($file_dokumen);*/
			}
		    }
	    }
	}
	
	public function proses_tambah_lpp()
	{
		$file_dokumen = array();
		$image_name = array();

		$nama = $this->input->post('nama_lpp');
        $name_dir = str_replace(" ","_",$nama);

        $url = "/assets/dokumen_lpp/".$name_dir;

        $thisdir = getcwd(); 

        $this->upload->initialize(array(
		            "upload_path"   => $thisdir.$url,
				    "overwrite" => TRUE,
				    "encrypt_name" => TRUE,
				    "remove_spaces" => TRUE,
				    "allowed_types" => 'gif|jpg|png|doc|docx|pdf'
		        ));

        $status = $this->input->post('status');
        $password = $this->input->post('password');
        $konfirmasi_password = $this->input->post('konfirmasi_password');

        if($password == $konfirmasi_password){
        	if($status=="swasta"){
	        	if(!file_exists($thisdir.$url)){
		        	if(!mkdir($thisdir.$url, 0755)){
						//die('Failed to create folders...');
						$this->session->set_flashdata('status', '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Nama LPP yang anda daftarkan sudah ada !</div>');
						redirect('daftar_lpp',$data);
					}else{
						//echo "swasta";
				        //Perform upload.
				        $this->upload->do_multi_upload("files");
				        $upload_data = $this->upload->get_multi_upload_data();
				        foreach ($upload_data as $key => $value) {
					       	$image_name[] = $value['file_name'];
					    };

					    $file_dokumen = array(
						    "lampiran_ktp" => $image_name[0],
						    "surat_ijin_operasional" => $image_name[1],
						    "surat_ket_domisili" => $image_name[2],
						    "akta_notaris" => $image_name[3],
						    "sop" => $image_name[4],
						    "surat_komitmen" => $image_name[5],
						);

					    if( (isset($file_dokumen['lampiran_ktp'])) &&
				        	(isset($file_dokumen['surat_ijin_operasional'])) &&
				        	(isset($file_dokumen['surat_ket_domisili'])) &&
							(isset($file_dokumen['akta_notaris'])) &&
							(isset($file_dokumen['sop'])) &&
							(isset($file_dokumen['surat_komitmen']))
				         ){
				        	$this->insert_lpp_to_database($file_dokumen);
				        }else{
				        	$this->session->set_flashdata('status', '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Dokumen Harus Diisi !</div>');
							redirect('daftar_lpp',$data);
							/*print_r($file_dokumen);*/
				        }
						
					}
					
		        }else{
		        	$this->session->set_flashdata('status', '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Nama LPP yang anda daftarkan sudah ada !</div>');
					redirect('daftar_lpp',$data);
		        }
		        

	        }else if($status=="negeri"){
	        	if(!file_exists($thisdir.$url)){
		        	if(!mkdir($thisdir.$url, 0755)){
						//die('Failed to create folders...');
						$this->session->set_flashdata('status', '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Nama LPP yang anda daftarkan sudah ada !</div>');
						redirect('daftar_lpp',$data);
					}else{
						//echo "negeri";
				        //Perform upload.
				        $this->upload->do_multi_upload("files2");
				        $upload_data = $this->upload->get_multi_upload_data();
				        foreach ($upload_data as $key => $value) {
					       	$image_name[] = $value['file_name'];
					    };

					    $file_dokumen = array(
						    "lampiran_ktp" => $image_name[0],
						    "mekanisme_keuangan" => $image_name[1],
						    "sop" => $image_name[2],
						    "surat_komitmen" => $image_name[3],
						    "tugas_pelaksana" => $image_name[4],
						);

					    if( (isset($file_dokumen['lampiran_ktp'])) &&
				        	(isset($file_dokumen['mekanisme_keuangan'])) &&
							(isset($file_dokumen['tugas_pelaksana'])) &&
							(isset($file_dokumen['sop'])) &&
							(isset($file_dokumen['surat_komitmen']))
				         ){
				        	$this->insert_lpp_to_database($file_dokumen);
				        }else{
				        	$this->session->set_flashdata('status', '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Dokumen Harus Diisi !</div>');
							redirect('daftar_lpp',$data);
							/*print_r($file_dokumen);*/
				        }
					}
	        	}else{
	        		$this->session->set_flashdata('status', '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Nama LPP yang anda daftarkan sudah ada !</div>');
					redirect('daftar_lpp',$data);
	        	}

	        	
		        
	        }

	        //print_r($file_dokumen);
	        /**/
	        
        }else{
        	$this->session->set_flashdata('status', '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Password Tidak Sama !</div>');
				redirect('daftar_lpp',$data);
        }

        
			
		/*print_r($this->upload->get_multi_upload_data());
		echo "<br><br>";
		print_r($this->upload->display_errors());
        echo "<br><br>";
		print_r($file_dokumen);*/
		/*print_r($file_dokumen);
		echo $file_dokumen['lampiran_ktp'];*/
        





        //-----------------//


			        /*if($this->upload->do_multi_upload("files")){
			            //Code to run upon successful upload.
			            print_r($this->upload->get_multi_upload_data());
			            $upload_data = $this->upload->get_multi_upload_data();
				        foreach ($upload_data as $key => $value) {
				        	$image_name[] = $value['file_name'];
				        };

				        var_dump($value);
			        	var_dump($upload_data);
			        	var_dump($image_name);
			        }else{
			        	$invalid=$this->upload->display_errors();
			    		print_r($invalid);
			    		echo "gagal";
			        }*/

	}

	public function insert_lpp_to_database($file_dokumen)
	{
		$this->form_validation->set_rules('nama_lpp', 'Nama LPP', 'required');
		$this->form_validation->set_rules('alamat_lpp', 'Alamat LPP', 'required');
		$this->form_validation->set_rules('penanggungjawab', 'Penanggungjawab', 'required');
		$this->form_validation->set_rules('jabatan', 'Jabatan', 'required');
		$this->form_validation->set_rules('no_ktp', 'No KTP', 'required');
		$this->form_validation->set_rules('program', 'Program', 'required');
		$this->form_validation->set_rules('alamat_lpp', 'Alamat LPP', 'required');
		$this->form_validation->set_rules('no_fax', 'No Fax', 'required');
		$this->form_validation->set_rules('email', 'Email', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');
		$this->form_validation->set_rules('konfirmasi_password', 'Konfirmasi Password', 'required');
		$this->form_validation->set_rules('kontak_nama', 'Kontak Person Nama', 'required');
		$this->form_validation->set_rules('kontak_email', 'Kontak Person Email', 'required');
		$this->form_validation->set_rules('kontak_email', 'Kontak Person Email', 'required');
		$this->form_validation->set_rules('kontak_telp', 'Kontak Person Telepon', 'required');

		

		/*$this->form_validation->set_rules('files', 'Dokumen LPP', 'required');
		$this->form_validation->set_rules('files2', 'Dokumen LPP', 'required');*/

		if ($this->form_validation->run() == FALSE)
		{
			$this->session->set_flashdata('status', '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.validation_errors().'</div>');
				redirect('daftar_lpp',$data);
				
		}else{
			$data = $this->model_lpp->insert_lpp($file_dokumen);
			if($data){
				//$data['status'] = "success";
				$this->session->set_flashdata('status', '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Anda telah berhasil melakukan pendaftaran LPP yang baru. Kami akan melakukan proses verifikasi dan kami akan menginformasikan melalui email untuk proses selanjutnya</div>');
				redirect('daftar_lpp',$data);
			}
		}
	}

}