            <div id="page-wrapper">
            <div class="row">
            <div class="col-lg-12">
            <?php 
            $status = $this->session->flashdata('status');
            if(isset($status)){ echo $status; } 
            ?>
            <div class="panel panel-primary">
              <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-bar-chart-o"></i>Semua Perencanaan Pelatihan</h3>
              </div>
                      <table class="table table-hover table-striped tablesorter">
                <thead>
                  <tr>
                    <th class="header">No <i class="fa fa-sort"></i></th>
                    <th class="header">jenis Pelatihan <i class="fa fa-sort"></i></th>
                    <th class="header">Contact Person <i class="fa fa-sort"></i></th>
                    <th class="header">LPP <i class="fa fa-sort"></i></th>
                    <th class="header">Tempat Pelatihan <i class="fa fa-sort"></i></th>
                    <th class="header">Mulai Tanggal <i class="fa fa-sort"></i></th>
                    <th class="header">Sampai Tanggal <i class="fa fa-sort"></i></th>
                    <th class="header">Jumlah Peserta <i class="fa fa-sort"></i></th>
                    <th class="header">Surat Permohonan <i class="fa fa-sort"></i></th>
                    <th class="header">Status<i class="fa fa-sort"></i></th>
                    <th class="header">Action<i class="fa fa-sort"></i></th>
                  </tr>
                </thead>
                <tbody>
                <?php $no =1; foreach ($semua_pelatihan as $data): 
                if($data->approve_pelatihan==0){
                  $stat = "pending";
                }
                ?>
                  <tr>
                    <td><?php echo $no; ?></td>
                    <td><?php echo $data->judul_pelatihan; ?></td>
                    <td><?php echo $data->isi_pelatihan; ?></td>
                    <td><?php echo $data->nama_lpp; ?></td>
                    <td><?php echo $data->tempat_pelatihan; ?></td>
                    <td><?php echo $data->mulai_tanggal; ?></td>
                    <td><?php echo $data->sampai_tanggal; ?></td>
                    <td><?php echo $data->jumlah_peserta; ?></td>
                    <td><a href="<?php echo base_url(); ?>assets/upload/surat_permohonan/<?php echo $data->surat_permohonan; ?>"><?php echo substr($data->surat_permohonan,0,20); ?></a> </td>
                        <td>
                        <span style="color:red"><?php echo $stat; ?></span> 
                    <td>
                    <a href="<?php echo base_url().$role."/proses_approve_schedule/".$data->id_pelatihan; ?>"><button type="submit" class="btn btn-success" name="submit" value="kirim" id="delete" style="width:100%;">setujui</button></a><br><br>
                    
                    

                    <form method="POST" action="<?php echo base_url().$role."/proses_tolak_schedule/".$data->id_pelatihan; ?>">
                    <a href=""><button type="submit" class="btn btn-danger" name="submit" value="kirim" id="delete" style="width:100%;">tolak</button></a> <br>
                    <br>alasan ditolak <br> <textarea name="alasan_ditolak"></textarea> 

                    
                    </form>
                    </td>
                  </tr>
                <?php $no++; endforeach; ?>
                </tbody>
              </table>
               <div id="pager" class="pager" style="margin-top:-10px;">
                <form>
                  <img src="<?php echo base_url()?>assets/table_sorter/addons/pager/icons/first.png" class="first"/>
                  <img src="<?php echo base_url()?>assets/table_sorter/addons/pager/icons/prev.png" class="prev"/>
                  <input type="text" class="pagedisplay"/>
                  <img src="<?php echo base_url()?>assets/table_sorter/addons/pager/icons/next.png" class="next"/>
                  <img src="<?php echo base_url()?>assets/table_sorter/addons/pager/icons/last.png" class="last"/>
                  <select class="pagesize">
                    <option selected="selected"  value="10">10</option>
                    <option value="20">20</option>
                    <option value="30">30</option>
                    <option  value="40">40</option>
                  </select>
                </form>
              </div>