<script type="text/javascript">
function pilih_status(selected_value){
  if(selected_value=="asn"){
    //alert("asn");
    $('#jenis_pegawai').css("display", "block");
    $('#nip').css("display", "block");
    $('#jabatan').css("display", "block");
  }else{
    //alert("bukan asn");
    $('#nip').css("display", "none");
    $('#jabatan').css("display", "none");
    $('#jenis_pegawai').css("display", "none");
    $('#golongan').css("display", "none");
    $('#pangkat').css("display", "none");
  }
}

function pilih_jenis(selected_value){
  $('#golongan').css("display", "block");
    $('#pangkat').css("display", "block");
}
</script>
<style type="text/css">
  #jenis_pegawai{
    display: none;
  }
  #golongan{
    display: none;
  }
  #pangkat{
    display: none;
  }
  #nip{
    display: none;
  }
  #jabatan{
    display: none;
  }
</style>
        <div id="page-wrapper">
          <div class="row">
          <div class="col-lg-12">
            <div class="panel panel-primary">
              <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-bar-chart-o"></i> Tambah Fasilitator Baru</h3>
              </div>
              <?php 
              $status = $this->session->flashdata('status');
              if(isset($status)){ echo $status; } 
              ?>
              <div class="panel-body">
                <?php echo form_open(base_url().'lkpp/proses_tambah_Fasilitator'); ?>
                <div class="form-group">
		                <label>Nama</label>
                  <input type="text" class="form-control" name="nama_lengkap">
		            </div>
                <!-- <div class="form-group">
                    <label>Alamat</label>
                  <input type="text" class="form-control" name="alamat">
                </div>
                <div class="form-group">
                    <label>No Telp</label>
                  <input type="text" class="form-control" name="no_telp">
                </div>
                <div class="form-group">
                    <label>Tempat Lahir</label>
                  <input type="text" class="form-control" name="tempat_lahir">
                </div>
                <div class="form-group">
                    <label>Tanggal Lahir</label>
                  <input type="text" class="form-control" name="tgl_lahir" id="dari_tanggal">
                </div>
                <div class="form-group">
                    <label>Gelar</label>
                  <input type="text" class="form-control" name="gelar">
                </div>
                <div class="form-group">
                    <label>Jenis Kelamin</label>
                    <input type="radio" name="jenis_kelamin" value="Pria">Pria
                    <input type="radio" name="jenis_kelamin" value="Wanita">Wanita
                </div>
                <div class="form-group" id="nip">
                    <label>NIP</label>
                  <input type="text" class="form-control" name="nip">
                </div>
                <div class="form-group" id="jenis_pegawai">
                </div>
                <div class="form-group">
                    <label>Pendidikan</label>
                  <!-- <input type="text" class="form-control" name="pendidikan">
                  <select class="form-control" name="pendidikan">
                    <option value="S1">S1</option>
                    <option value="S2">S2</option>
                    <option value="S3">S3</option>
                  </select>
                </div> 
                <div class="form-group">
                    <label>Instansi</label>
                  <input type="text" class="form-control" name="instansi">
                </div>
                <div class="form-group">
                    <label>Provinsi</label>
                  <input type="text" class="form-control" name="provinsi">
                </div>
                <div class="form-group">
                    <label>Kota</label>
                  <input type="text" class="form-control" name="kota">
                </div>
                <div class="form-group">
                    <label>Email</label>
                  <input type="text" class="form-control" name="email">
                </div>-->
                <div class="form-group">
                    <label>Username</label>
                  <input type="text" class="form-control" name="username">
                </div>
                <div class="form-group">
                    <label>Password</label>
                  <input type="password" class="form-control" name="password">
                </div>
                <!-- <div class="form-group">
                    <label>No Sertifikat</label>
                  <input type="text" class="form-control" name="no_sertifikat">
                </div> -->
                <!-- 
                
                 -->
                 <button type="submit" class="btn btn-primary" name="submit" value="kirim">Tambah Fasilitator</button>
                <!-- <button type="submit" class="btn btn-danger" name="submit" value="cancel">Tolak Request</button> -->
                </form>
              </div>
            </div>
          </div>
        </div><!-- /.row -->