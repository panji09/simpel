<script type="text/javascript">
  $(document).ready(function(){
  
  $("#save").click(function(){
    ajax("save");
  });

  $("#add_new").click(function(){
    $(".entry-form").fadeIn("fast");  
  });
  
  $("#close").click(function(){
    $(".entry-form").fadeOut("fast"); 
  });
  
  $("#cancel").click(function(){
    $(".entry-form").fadeOut("fast"); 
  });
  
  $(".del").live("click",function(){
    ajax("delete",$(this).attr("id"));
  });

  function ajax(action,id){
    if(action =="save")
      data = $("#userinfo").serialize()+"&action="+action;
    else if(action == "delete"){
      data = "action="+action+"&item_id="+id;
    }

    $.ajax({
      type: "POST", 
      url: "<?php echo base_url(); ?>lkpp/addNarasumberToPelatihan", 
      data : data,
      dataType: "json",
      success: function(response){
        alert(response.msg);
        /*if(response.success == "1"){
          if(action == "save"){
            $(".entry-form").fadeOut("fast",function(){
              $(".table-list").append("<tr><td>"+response.fname+"</td><td>"+response.lname+"</td><td>"+response.email+"</td><td>"+response.phone+"</td><td><a href='#' id='"+response.row_id+"' class='del'>Delete</a></a></td></tr>"); 
              $(".table-list tr:last").effect("highlight", {
                color: '#4BADF5'
              }, 1000);
            }); 
          }else if(action == "delete"){
            var row_id = response.item_id;
            $("a[id='"+row_id+"']").closest("tr").effect("highlight", {
              color: '#4BADF5'
            }, 1000);
            $("a[id='"+row_id+"']").closest("tr").fadeOut();
          }
        }else{
          alert(response.msg);
        }*/
      },
      error: function(res){
        alert("Unexpected error! Try again."+res);
      }
    });
  }
});

</script>
<div id="page-wrapper">

        <div class="row">
          <div class="col-lg-12">
            <h2>Narasumber Pelatihan</h2>
            <button type="submit" class="btn btn-primary" name="submit" value="kirim" id="add_new">Tambah Narasumber</button>
            <div class="table-responsive">
              
              <table class="table table-bordered table-hover table-striped tablesorter">
                <thead>
                  <tr>
                    <th>Tema <i class="fa fa-sort"></i></th>
                    <th>Dari Jam <i class="fa fa-sort"></i></th>
                    <th>Sampai Jam <i class="fa fa-sort"></i></th>
                    <th>narasumber <i class="fa fa-sort"></i></th>
                    <th>approve <i class="fa fa-sort"></i></th>
                    <th>action <i class="fa fa-sort"></i></th>
                  </tr>
                </thead>
                <tbody>
                <?php foreach ($pelatihan as $data): ?>
                  <tr>
                    <td><?php echo $data->judul_pelatihan; ?></td>
                    <td><?php echo $data->dari_jam; ?></td>
                    <td><?php echo $data->sampai_jam; ?></td>
                    <td><?php echo $data->nama_narasumber; ?></td>
                    <td><?php echo $data->approve; ?></td>
                    <td><a href="<?php echo base_url(); ?>lkpp/hubungkan/<?php echo $data->id_pelatihan; ?>">tambah</a></td>
                  </tr>
                <?php endforeach; ?>
                </tbody>
              </table>
            </div>
          </div>

      </div><!-- /#page-wrapper -->



      <div class="entry-form">
      <form name="userinfo" id="userinfo"> 
      <table width="100%" border="0" cellpadding="4" cellspacing="0">
        <tr>
          <td colspan="2" align="right"><a href="#" id="close">Close</a></td>
        </tr>
        <tr>
          <td>Dari Jam</td>
          <td><!-- <input type="text" name="dari_jam"> -->
          <select name="dari_jam">
          <?php for($i=0;$i<24;$i++){ ?>
            <option value="<?php echo $i; ?>"><?php if($i<=9){echo "0".$i.".00";}else{echo $i.".00";} ?></option>
          <?php } ?>
          </select>
          </td>
        </tr>
        <tr>
          <td>Sampai Jam</td>
          <td><!-- <input type="text" name="sampai_jam"> -->
          <select name="sampai_jam">
          <?php for($i=0;$i<24;$i++){ ?>
            <option value="<?php echo $i; ?>"><?php if($i<=9){echo "0".$i.".00";}else{echo $i.".00";} ?></option>
          <?php } ?>
          </select>
          </td>
        </tr>
        <tr>
          <td>Narasumber</td>
          <td><!-- <input type="text" name="narasumber"> -->
          <select name="narasumber">
          <?php foreach ($narasumber as $data): ?>
            <option class="opt" value=""><?php echo $data->id_narasumber; ?></option>
          <?php endforeach; ?>
          </select>
          </td>
        </tr>
        <tr>
          <td align="right"></td>
          <td><input type="button" value="Save" id="save"><input type="button" value="cancel" id="cancel"></td>
        </tr>
      </table>
      </form>
    </div>