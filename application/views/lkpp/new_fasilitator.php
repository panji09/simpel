            
            <div id="page-wrapper">
            <!-- <form method="POST" action="<?php echo base_url();?>lkpp/add_fasilitator">
            <div style="border:1px solid #DADADA;padding:8px;">
                <tr>
                  <td></td>
                  <td>
                  <input type="text" name="nama_fasilitator" class="form-control"  style="font-size:12px;border:1px solid #DADADA;padding:3px 10px;float:left;width:180px;margin:0px 4px 0px 0px;">
                  </td>
                </tr>
                
            </div><br>
            </form>
 -->            
          <a href="<?php echo base_url(); ?>lkpp/tambah_fasilitator"><button type="submit" class="btn btn-primary" name="submit" value="kirim" id="edit">Tambah</button></a><br><br>
                
            <div class="row">
            <div class="col-lg-12">
            <div class="panel panel-primary">
              <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-bar-chart-o"></i></h3>
              </div>
                <table class="table table-hover table-striped tablesorter">
                <thead>
                  <tr>
                    <th class="header">no<i class="fa fa-sort"></i></th>
                    <th class="header">nama fasilitator<i class="fa fa-sort"></i></th>
                    <th class="header">username fasilitator<i class="fa fa-sort"></i></th>
                    <th class="header">action<i class="fa fa-sort"></i></th>
                  </tr>
                </thead>
                <tbody>
                <?php $no = 1; foreach ($dataBerita as $data): 
                ?>
                  <tr>
                    <td><?php echo $no; ?></td>
                    <td><?php echo $data->nama_fasilitator; ?></td>
                    <td><?php echo $data->username; ?></td>
                    <td>
                      <a href="<?php echo base_url(); ?>lkpp/detail_fasilitator/<?php echo $data->id_fasilitator; ?>"><button type="submit" class="btn btn-success" name="submit" value="kirim" id="delete" title="delete">Edit</button></a>
                      <a href="<?php echo base_url(); ?>lkpp/delete_fasilitator/<?php echo $data->id_fasilitator; ?>"><button type="submit" class="btn btn-danger" name="submit" value="kirim" id="delete" title="delete" onclick="return confirm('Anda Yakin Akan Menghapus Fasilitator ini ?')">Delete</button></a>
                    
                    </td>
                  </tr>
                <?php $no++; endforeach; ?>

                </tbody>
              </table>
              <div id="pager" class="pager" style="margin-top:0px;">
                <form>
                  <img src="<?php echo base_url()?>assets/table_sorter/addons/pager/icons/first.png" class="first"/>
                  <img src="<?php echo base_url()?>assets/table_sorter/addons/pager/icons/prev.png" class="prev"/>
                  <input type="text" class="pagedisplay"/>
                  <img src="<?php echo base_url()?>assets/table_sorter/addons/pager/icons/next.png" class="next"/>
                  <img src="<?php echo base_url()?>assets/table_sorter/addons/pager/icons/last.png" class="last"/>
                  <select class="pagesize">
                    <option selected="selected"  value="10">10</option>
                    <option value="20">20</option>
                    <option value="30">30</option>
                    <option  value="40">40</option>
                  </select>
                </form>
              </div>