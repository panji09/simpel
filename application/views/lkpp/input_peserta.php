              <script type="text/javascript">
  $(document).ready(function(){
  
  $("#save").click(function(){
    ajax("save");
  });

  $("#add_new").click(function(){
    $(".entry-form").fadeIn("fast");  
  });
  
  $("#close").click(function(){
    $(".entry-form").fadeOut("fast"); 
  });

  $("#cancel").click(function(){
    $(".entry-form").fadeOut("fast"); 
  });
  
  /*$("#cancel").click(function(){
    $(".entry-form").fadeOut("fast"); 
  });
  
  $(".del").live("click",function(){
    ajax("delete",$(this).attr("id"));
  });*/

  function ajax(action){
    $.ajax({
      type: "POST", 
      url: "<?php echo base_url(); ?>lpp/proses_insert_peserta", 
      data : $("#userinfo").serialize(),
      dataType: "json",
      success: function(response){
        if(response.result=="success"){
          $(".entry-form").fadeOut("fast",function(){
              location.reload();
          });
        }
      },
      error: function(xhr, status, error){
        //alert("Unexpected error! Try again."+res);
        console.log("Unexpected error! Try again. = "+xhr.responseText+" - "+status+" - "+error);
      }
    });
  }

    /*if(action =="save"){
      //data = $("#userinfo").serialize()+"&action="+action;
      myData = $("#userinfo").serialize();
    }else if(action == "delete"){
      myData = "action="+action+"&item_id="+id;
    }

    $.ajax({
      type: "POST", 
      url: "<?php echo base_url(); ?>lpp/proses_insert_peserta", 
      data : myData,
      dataType: "json",
      success: function(response){
        alert(response.msg);
        if(response.success == "1"){
          if(action == "save"){
            $(".entry-form").fadeOut("fast",function(){
              $(".table-list").append("<tr><td>"+response.fname+"</td><td>"+response.lname+"</td><td>"+response.email+"</td><td>"+response.phone+"</td><td><a href='#' id='"+response.row_id+"' class='del'>Delete</a></a></td></tr>"); 
              $(".table-list tr:last").effect("highlight", {
                color: '#4BADF5'
              }, 1000);
            }); 
          }else if(action == "delete"){
            var row_id = response.item_id;
            $("a[id='"+row_id+"']").closest("tr").effect("highlight", {
              color: '#4BADF5'
            }, 1000);
            $("a[id='"+row_id+"']").closest("tr").fadeOut();
          }
        }else{
          alert(response.msg);
        }
      },
      error: function(xhr, status, error){
        //alert("Unexpected error! Try again."+res);
        console.log("Unexpected error! Try again. = "+xhr+" - "+status+" - "+error);
      }
    });*/

});

</script>

              <div class="col-lg-12">
              <button type="submit" class="btn btn-primary" name="submit" value="kirim" id="add_new">Tambah Peserta</button>
              <table class="table table-hover table-striped tablesorter">
                <thead>
                  <tr>
                    <th class="header">Nama Peserta <i class="fa fa-sort"></i></th>
                    <th class="header">Judul Pelatihan <i class="fa fa-sort"></i></th>
                    <th class="header">Pre Test<i class="fa fa-sort"></i></th>
                    <th class="header">Post Test<i class="fa fa-sort"></i></th>
                    <th class="header">Action<i class="fa fa-sort"></i></th>
                  </tr>
                </thead>
                <tbody>
                <?php foreach ($semua_peserta as $data): ?>
                  <tr>
                    <td><?php echo $data->nama_lengkap; ?></td>
                    <td><?php echo $data->judul_pelatihan; ?></td>
                    <td><?php echo $data->pre_test; ?></td>
                    <td><?php echo $data->post_test; ?></td>
                    <td>
                    <button type="submit" class="btn btn-primary" name="submit" value="kirim" id="edit">edit</button>
                    <button type="submit" class="btn btn-danger" name="submit" value="kirim" id="delete">delete</button>
                    <button type="submit" class="btn btn-success" name="submit" value="kirim" id="delete">detail</button>
                    </td>
                  </tr>
                <?php endforeach; ?>
                </tbody>
              </table>

              <div class="entry-form">
              <form name="userinfo" id="userinfo"> 
              <table width="100%" border="0" cellpadding="4" cellspacing="0">
                <tr>
                  <td colspan="2" align="right"><a href="#" id="close">Close</a></td>
                </tr>
                <input type="hidden" name="id_request" value="<?php echo $id; ?>">
                <tr>
                  <td>Nama lengkap</td>
                  <td><!-- <input type="text" name="narasumber"> -->
                  <input type="text" name="nama_lengkap">
                  </td>
                </tr>
                <tr>
                  <td>Gelar Akademis</td>
                  <td><!-- <input type="text" name="narasumber"> -->
                  <input type="text" name="gelar_akademis">
                  </td>
                </tr>
                <tr>
                  <td>Jenis Kelamin</td>
                  <td><!-- <input type="text" name="narasumber"> -->
                  <select name="jenis_kelamin">
                    <option value="L">Laki Laki</option>
                    <option value="P">Perempuan</option>
                  </select>
                  </td>
                </tr>
                <tr>
                  <td>NIP</td>
                  <td><!-- <input type="text" name="narasumber"> -->
                  <input type="text" name="nip">
                  </td>
                </tr>
                <tr>
                  <td>NO KTP</td>
                  <td><!-- <input type="text" name="narasumber"> -->
                  <input type="text" name="no_ktp">
                  </td>
                </tr>
                <tr>
                  <td>Tempat Lahir</td>
                  <td><!-- <input type="text" name="narasumber"> -->
                  <input type="text" name="tempat_lahir">
                  </td>
                </tr>
                <tr>
                  <td>Tanggal Lahir</td>
                  <td><!-- <input type="text" name="narasumber"> -->
                  <input type="text" name="tanggal_lahir">
                  </td>
                </tr>
                <tr>
                  <td>Pendidikan Terakhir</td>
                  <td><!-- <input type="text" name="narasumber"> -->
                  <input type="text" name="pendidikan_terakhir">
                  </td>
                </tr>
                <tr>
                  <td>Status Kepegawaian</td>
                  <td><!-- <input type="text" name="narasumber"> -->
                  <input type="text" name="status_kepegawaian">
                  </td>
                </tr>
                <tr>
                  <td>Instansi</td>
                  <td><!-- <input type="text" name="narasumber"> -->
                  <input type="text" name="instansi">
                  </td>
                </tr>
                <tr>
                  <td>Lembaga</td>
                  <td><!-- <input type="text" name="narasumber"> -->
                  <input type="text" name="lembaga">
                  </td>
                </tr>
                <tr>
                  <td>Domisili Kabupaten</td>
                  <td><!-- <input type="text" name="narasumber"> -->
                  <input type="text" name="domisili_kabupaten">
                  </td>
                </tr>
                <tr>
                  <td>Domisili Provinsi</td>
                  <td><!-- <input type="text" name="narasumber"> -->
                  <input type="text" name="domisili_provinsi">
                  </td>
                </tr>
                <tr>
                  <td>No Telepon</td>
                  <td><!-- <input type="text" name="narasumber"> -->
                  <input type="text" name="no_telepon">
                  </td>
                </tr>
                <tr>
                  <td>Alamat Email</td>
                  <td><!-- <input type="text" name="narasumber"> -->
                  <input type="text" name="alamat_email">
                  </td>
                </tr>
                <tr>
                  <td>Pengalaman di bidang pengadaan barang / jasa pemerintah</td>
                  <td><!-- <input type="text" name="narasumber"> -->
                  <input type="radio" name="pengalaman" value="yes">ya
                  <input type="radio" name="pengalaman" value="no">tidak
                  </td>
                </tr>
                <tr>
                  <td>Pernah Mengikuti pelatihan barang / jasa </td>
                  <td><!-- <input type="text" name="narasumber"> -->
                  <input type="radio" name="pernah" value="yes" >ya
                  <input type="radio" name="pernah" value="yes" >tidak
                  </td>
                </tr>
                <tr>
                  <td>Berapa Kali</td>
                  <td><!-- <input type="text" name="narasumber"> -->
                  <input type="text" name="berapa_kali">
                  </td>
                </tr>
                <tr>
                  <td>Alasan Mengikuti</td>
                  <td><!-- <input type="text" name="narasumber"> -->
                  <input type="text" name="alasan_mengikuti">
                  </td>
                </tr>
                <tr>
                  <td align="right"></td>
                  <td><input type="button" value="Save" id="save"><input type="button" value="cancel" id="cancel"></td>
                </tr>
              </table>
              </form>
            </div>