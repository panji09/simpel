        <div id="page-wrapper">
          <div class="row">
          <div class="col-lg-12">
            <div class="panel panel-primary">
              <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-bar-chart-o"></i> Tambah Jenis Pelatihan</h3>
              </div>
              <?php 
              $status = $this->session->flashdata('status');
              if(isset($status)){ echo $status; } 
              ?>
              <div class="panel-body">
               <?php echo form_open_multipart('lkpp/proses_tema_pelatihan');?>
                <div class="form-group">
		                <label>Jenis Pelatihan</label><!-- <input type="text" class="form-control" name="narasumber"> -->
                  <input type="text" class="form-control" name="judul_tema">
		            </div>
                <div class="form-group">
                    <label>Keterangan</label><!-- <input type="text" class="form-control" name="narasumber"> -->
                    <textarea class="form-control" name="keterangan_tema" rows="6"></textarea>
                </div>
                 <button type="submit" class="btn btn-primary" name="submit" value="kirim">Tambah Jenis Pelatihan</button>
                <!-- <button type="submit" class="btn btn-danger" name="submit" value="cancel">Tolak Request</button> -->
                </form>
              </div>
            </div>
          </div>
        </div><!-- /.row -->