            <div id="page-wrapper">
            <?php 
            $status = $this->session->flashdata('status');
            if(isset($status)){ echo $status; } 
            ?>
            <a href="<?php echo base_url(); ?>lkpp/tambah_berita"><i class="fa fa-dashboard"></i> <button type="submit" class="btn btn-primary" name="submit" value="kirim" id="edit">Tambah Berita</button></a><br><br>
            <div class="row">
            <div class="col-lg-12">
            <div class="panel panel-primary">
              <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-bar-chart-o"></i>Semua Berita</h3>
              </div>
                <table class="table table-hover table-striped tablesorter">
                <thead>
                  <tr>
                    <th class="header">id berita<i class="fa fa-sort"></i></th>
                    <th class="header">judul berita<i class="fa fa-sort"></i></th>
                    <th class="header">foto berita <i class="fa fa-sort"></i></th>
                    <th class="header">tanggal berita <i class="fa fa-sort"></i></th>
                    <th class="header">isi berita<i class="fa fa-sort"></i></th>
                    <th class="header">action<i class="fa fa-sort"></i></th>
                  </tr>
                </thead>
                <tbody>
                <?php foreach ($dataBerita as $data): ?>
                  <tr>
                    <td><?php echo $data->id_berita; ?></td>
                    <td><?php echo $data->judul_berita; ?></td>
                    <td><img src="<?php echo base_url()."/assets/upload/berita/".$data->photo_berita; ?>" width="128px"></td>
                    <td><?php echo $data->tgl_berita; ?></td>
                    <td>
                    <?php 
                            $string = $data->isi_berita;
                            $string = strip_tags($string);

                    if (strlen($string) > 450) {

                        // truncate string
                        $stringCut = substr($string, 0, 450);

                        // make sure it ends in a word so assassinate doesn't become ass...
                        $string = substr($stringCut, 0, strrpos($stringCut, ' ')).'... <a class="read_more" href="'.base_url().'lkpp/berita/detail/'.$data->id_berita.'"></a>'; 
                    }
                    echo $string; 
                  ?>
                  </td>
                    <td>
                    <a href="<?php echo base_url(); ?>lkpp/detail_berita/<?php echo $data->id_berita; ?>"><button type="submit" class="btn btn-success" name="submit" value="kirim" id="delete" title="detail">detail</button></a>
                    <a href="<?php echo base_url().$role."/delete_berita/".$data->id_berita; ?>"><button type="submit" class="btn btn-danger" name="submit" value="kirim" id="delete" onclick="return confirm('Anda Yakin Akan Menghapus Berita ini')">delete</button></a>
                     </td>
                  </tr>
                <?php endforeach; ?>

                </tbody>
              </table>
              <div id="pager" class="pager">
                <form>
                  <img src="<?php echo base_url()?>assets/table_sorter/addons/pager/icons/first.png" class="first"/>
                  <img src="<?php echo base_url()?>assets/table_sorter/addons/pager/icons/prev.png" class="prev"/>
                  <input type="text" class="pagedisplay"/>
                  <img src="<?php echo base_url()?>assets/table_sorter/addons/pager/icons/next.png" class="next"/>
                  <img src="<?php echo base_url()?>assets/table_sorter/addons/pager/icons/last.png" class="last"/>
                  <select class="pagesize">
                    <option selected="selected"  value="10">10</option>
                    <option value="20">20</option>
                    <option value="30">30</option>
                    <option  value="40">40</option>
                  </select>
                </form>
              </div>