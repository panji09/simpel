

			<table class="table table-hover table-striped tablesorter">
                <thead>
                  <tr>
                    <th class="header">No <i class="fa fa-sort"></i></th>
                    <th class="header">NIP <i class="fa fa-sort"></i></th>
                    <th class="header">Nama Peserta <i class="fa fa-sort"></i></th>
                    <th class="header">Instansi <i class="fa fa-sort"></i></th>
                    <th class="header">Pre Test<i class="fa fa-sort"></i></th>
                    <th class="header">Post Test<i class="fa fa-sort"></i></th>
                  </tr>><!-- 
                    <th class="header">Action<i class="fa fa-sort"></i></th> -->
                  </tr>
                </thead>
                <tbody>
                <?php $no=1; foreach ($semua_peserta as $data): ?>
                  <tr>
                    <td><?php echo $no; ?></td>
                    <td><?php echo $data->NIP; ?></td>
                    <td><?php echo $data->nama_lengkap; ?></td>
                    <td><?php echo $data->instansi; ?></td>
                    <td><?php echo $data->pre_test; ?></td>
                    <td><?php echo $data->post_test; ?></td>
                    <!-- <td>
                    <button type="submit" class="btn btn-danger" name="submit" value="kirim" id="delete">delete</button>
                    <button type="submit" class="btn btn-success" name="submit" value="kirim" id="delete">detail</button>
                    </td> -->
                  </tr>
                <?php $no++; endforeach; ?>
                </tbody>
              </table>
              <div id="pager" class="pager" style="margin-top:-10px;">
                <form>
                  <img src="<?php echo base_url()?>assets/table_sorter/addons/pager/icons/first.png" class="first"/>
                  <img src="<?php echo base_url()?>assets/table_sorter/addons/pager/icons/prev.png" class="prev"/>
                  <input type="text" class="pagedisplay"/>
                  <img src="<?php echo base_url()?>assets/table_sorter/addons/pager/icons/next.png" class="next"/>
                  <img src="<?php echo base_url()?>assets/table_sorter/addons/pager/icons/last.png" class="last"/>
                  <select class="pagesize">
                    <option selected="selected"  value="10">10</option>
                    <option value="20">20</option>
                    <option value="30">30</option>
                    <option  value="40">40</option>
                  </select>
                </form>
              </div>