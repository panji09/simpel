<html>
<head>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()."assets/css/report.css"; ?>">
</head>

<body>

<br>
<br>
<br>
	<div id="page-header">
		<img src="http://localhost/simpel_new/assets/design/images/lkpp.jpg" width="200px">
	</div>
	<div id="page-body">
	<div id="judul_utama" align="center">
			<h1 align="center">Laporan Seluruh Pelatihan</h1>
		</div>
	<div id="content_box">
	<?php foreach ($pelatihan as $data_detail): ?>
	<h3>Pelatihan <?php echo $data_detail->judul_pelatihan; ?> oleh <?php echo $data_detail->nama_lpp; ?></h3>
		<table>
			<tr>
				<td class="td_front">Jenis Pelatihan</td>
				<td class="td_middle"> : </td>
				<td><?php echo $data_detail->judul_pelatihan; ?></td>
			</tr>
			<tr>
				<td class="td_front">LPP</td>
				<td class="td_middle"> : </td>
				<td><?php echo $data_detail->nama_lpp; ?></td>
			</tr>
			<tr>
				<td class="td_front">Instansi</td>
				<td class="td_middle"> : </td>
				<td><?php echo $data_detail->instansi; ?></td>
			</tr>
			<tr>
				<td class="td_front">Tempat Pelatihan</td>
				<td class="td_middle"> : </td>
				<td><?php echo $data_detail->tempat_pelatihan; ?></td>
			</tr>
			<tr>
				<td class="td_front">Contact Person</td>
				<td class="td_middle"> : </td>
				<td><?php echo $data_detail->contact_nama." - ".$data_detail->contact_hp; ?></td>
			</tr>
			<tr>
				<td class="td_front">Dari Tanggal</td>
				<td class="td_middle"> : </td>
				<td><?php echo tgl_indo($data_detail->mulai_tanggal); ?></td>
			</tr>
			<tr>
				<td class="td_front">Sampai Tanggal</td>
				<td class="td_middle"> : </td>
				<td><?php echo tgl_indo($data_detail->sampai_tanggal); ?></td>
			</tr>
		</table>
	<?php endforeach; ?>
		</div>

	</div>
</body>
</html>