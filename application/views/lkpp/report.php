<html>
<head>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()."assets/css/report.css"; ?>">
</head>

<body>

<br>
<br>
<br>
	<div id="page-header">
		<img src="http://localhost/simpel_new/assets/design/images/lkpp.jpg" width="200px">
	</div>
	<div id="page-body">
	<?php foreach ($detail as $data_detail): ?>
		<div id="judul_utama" align="center">
			<h1 align="center">Laporan Pelatihan <?php echo $data_detail->judul_pelatihan; ?></h1>
			<small>diselenggarakan pada tanggal : <?php echo tgl_indo($data_detail->mulai_tanggal)." s/d ".tgl_indo($data_detail->sampai_tanggal); ?> </small><br>
			<small>oleh : <?php echo $data_detail->nama_lpp; ?> </small>
		</div>
	<?php endforeach; ?>
	<div id="content_box">
	<h3>Detail Pelatihan</h3>
	<?php foreach ($detail as $data_detail): ?>
		<table>
			<tr>
				<td class="td_front">Jenis Pelatihan</td>
				<td class="td_middle"> : </td>
				<td><?php echo $data_detail->judul_pelatihan; ?></td>
			</tr>
			<tr>
				<td class="td_front">LPP</td>
				<td class="td_middle"> : </td>
				<td><?php echo $data_detail->nama_lpp; ?></td>
			</tr>
			<tr>
				<td class="td_front">Instansi</td>
				<td class="td_middle"> : </td>
				<td><?php echo $data_detail->instansi; ?></td>
			</tr>
			<tr>
				<td class="td_front">Tempat Pelatihan</td>
				<td class="td_middle"> : </td>
				<td><?php echo $data_detail->tempat_pelatihan; ?></td>
			</tr>
			<tr>
				<td class="td_front">Contact Person</td>
				<td class="td_middle"> : </td>
				<td><?php echo $data_detail->contact_nama." - ".$data_detail->contact_hp; ?></td>
			</tr>
			<tr>
				<td class="td_front">Dari Tanggal</td>
				<td class="td_middle"> : </td>
				<td><?php echo tgl_indo($data_detail->mulai_tanggal); ?></td>
			</tr>
			<tr>
				<td class="td_front">Sampai Tanggal</td>
				<td class="td_middle"> : </td>
				<td><?php echo tgl_indo($data_detail->sampai_tanggal); ?></td>
			</tr>
		</table>
	<?php endforeach; ?>
		</div>

		<div id="content_box">
	<?php foreach ($data_fasilitator as $fasilitator): ?>
		<h3>Fasilitator</h3>
		<table>
			<tr>
				<td class="td_front">Fasilitator</td>
				<td class="td_middle"> : </td>
				<td><?php echo $fasilitator->nama_fasilitator; ?></td>
			</tr>
		</table>
	<?php endforeach; ?>
		</div>

		<div id="content_box">
		<h3>Narasumber</h3>
		<table>
			<tr>
				<th>No</th>
				<th>Tanggal Pelatihan</th>
				<th>Dari Jam</th>
				<th>Sampai Jam</th>
				<th>Nama Narasumber</th>
				<th>Total Jam</th>
				<th>Status</th>
			</tr>
	<?php $no=1; foreach ($data_narasumber as $narasumber): ?>
			<tr>
				<td><?php echo $no; ?></td>
				<td><?php echo tgl_indo($narasumber->tgl_pelatihan); ?></td>
				<td><?php echo $narasumber->dari_jam; ?></td>
				<td><?php echo $narasumber->sampai_jam; ?></td>
				<td><?php echo $narasumber->nama_narasumber; ?></td>
				<td><?php echo $narasumber->total_jam; ?></td>
				<td><?php echo $narasumber->approve; ?></td>
			</tr>
	<?php $no++; endforeach; ?>		
		</table>
		</div>

		<div id="content_box">
		<h3>Peserta</h3>
		<table>
			<tr>
				<th>No</th>
				<th>NIP</th>
				<th>Nama Peserta</th>
				<th>Instansi</th>
				<th>Pre Test</th>
				<th>Post Test</th>
			</tr>
	<?php $no=1; foreach ($data_peserta as $peserta): ?>
			<tr>
				<td><?php echo $no; ?></td>
				<td><?php echo $peserta->NIP; ?></td>
				<td><?php echo $peserta->nama_lengkap; ?></td>
				<td><?php echo $peserta->instansi; ?></td>
				<td><?php echo $peserta->pre_test; ?></td>
				<td><?php echo $peserta->post_test; ?></td>
			</tr>
	<?php $no++; endforeach; ?>	
		</table>
		</div>

		<div id="content_box">
		<h3>Evaluasi Penyelenggara</h3>
	<?php $no=1; foreach ($data_evaluasi as $evaluasi): ?>
			<table>
				<tr>
					<td class="td_front">LPP</td>
					<td class="td_middle"> : </td>
					<td><?php echo $evaluasi->nama_lpp; ?></td>
				</tr>
				<tr>
					<td class="td_front">Kritik dan Saran</td>
					<td class="td_middle"> : </td>
					<td><?php echo $evaluasi->kritik_saran; ?></td>
				</tr>
				<tr>
					<td class="td_front">Kenyamanan ruang/tempat pelatihan</td>
					<td class="td_middle"> : </td>
					<td><?php echo $evaluasi->kenyamanan_ruang; ?></td>
				</tr>
				<tr>
					<td class="td_front">Perlengkapan pelatihan</td>
					<td class="td_middle"> : </td>
					<td><?php echo $evaluasi->perlengkapan; ?></td>
				</tr>
				<tr>
					<td class="td_front">Ketersediaan materi pelatihan </td>
					<td class="td_middle"> : </td>
					<td><?php echo $evaluasi->materi_pelatihan; ?></td>
				</tr>
				<tr>
					<td class="td_front">Hasil Evaluasi </td>
					<td class="td_middle"> : </td>
					<td><?php echo number_format((float)($evaluasi->kenyamanan_ruang+$evaluasi->perlengkapan+$evaluasi->materi_pelatihan)/3, 2, '.', ''); ?></td>
				</tr>
			</table>
	<?php $no++; endforeach; ?>	
			</div>

			<div id="content_box">
			<h3>Evaluasi Narasumber</h3>
			<table>
				<tr>
					<th>No</th>
					<th>Pelatihan</th>
					<th>Nama Narasumber</th>
					<th>Penguasaan Materi</th>
					<th>Memberikan Materi Secara Sistematis dan Mudah Dipahami</th>
					<th>Kritik dan Saran</th>
					<th>Memberi Contoh Menarik dan Mudah Diingat</th>
					<th>Mendorong Keaktifkan Peserta</th>
					<th>Hasil</th>
				</tr>
	<?php $no=1; foreach ($data_evaluasi_narasumber as $evaluasi_narasumber): ?>
				<tr>
					<td><?php echo $no; ?></td>
					<td><?php echo $evaluasi_narasumber->nama_narasumber; ?></td>
					<td><?php echo $evaluasi_narasumber->penguasaan_materi; ?></td>
					<td><?php echo $evaluasi_narasumber->mudah_dipahami; ?></td>
					<td><?php echo $evaluasi_narasumber->kritik_saran; ?></td>
					<td><?php echo $evaluasi_narasumber->mudah_diingat; ?></td>
					<td><?php echo $evaluasi_narasumber->mudah_diingat; ?></td>
					<td><?php echo $evaluasi_narasumber->aktif_dalam_kelas; ?></td>
					<td><?php  echo number_format((float)($evaluasi_narasumber->penguasaan_materi+$evaluasi_narasumber->mudah_dipahami+$evaluasi_narasumber->mudah_diingat+$evaluasi_narasumber->aktif_dalam_kelas)/4, 2, '.', '');  ?></td>
				</tr>
	<?php $no++; endforeach; ?>	
			</table>
			</div>

	</div>
	<!-- <div id="page-footer">
		<p>Footer text</p>
		<p>Page <span class="page-number"/> of <span class="page-count"/></p>
	</div>

	<div id="page-content">
		<p>Page 1.</p>
		
		<p style="page-break-after:always;"/>
		
		<p>Page 2.</p>
	</div> -->
</body>
</html>