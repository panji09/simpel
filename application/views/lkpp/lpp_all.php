            <div id="page-wrapper">
            <!-- <a href="<?php echo base_url(); ?>lkpp/tambah_lpp"><i class="fa fa-dashboard"></i> <button type="submit" class="btn btn-primary" name="submit" value="kirim" id="edit">Tambah LPP/Satker</button></a><br><br> -->
            <div class="row">

            <div class="col-lg-12">
            <?php echo form_open(base_url().'lkpp/lpp/all'); ?>
            <label style="float:left;">nama lpp &nbsp;: </label>
            <input type="text" name="search"  style="font-size:12px;border:1px solid #DADADA;padding:3px 10px;float:left;width:250px;margin:0px 5px 0px 0px;height:35px;" placeholder="Nama LPP">
            <br><br><label style="float:left;">akreditasi : </label>
            <select name="hasil_akreditasi"  style="font-size:12px;border:1px solid #DADADA;padding:3px 10px;float:left;width:250px;margin:0px 0px 0px 0px;height:35px;">
            <option value="Terdaftar">Terdaftar</option>
            <option value="all">all</option>
              <?php foreach ($akreditasi as $list) { 
                if($list->hasil_akreditasi!=""){ ?>
                <option value="<?php echo $list->hasil_akreditasi; ?>"><?php echo $list->hasil_akreditasi; ?></option>
              <?php } } ?>
            </select>
            <br><br><input type="submit" class="btn btn-success" name="submit" value="cari">
            <?php echo form_close(); ?>
            <br><br>
            <div class="panel panel-primary">
              <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-bar-chart-o"></i>Semua LPP</h3>
              </div>
                <table class="table table-hover table-striped tablesorter">
                <thead>
                  <tr>
                    <th class="header">No<i class="fa fa-sort"></i></th>
                    <th class="header">Nama LPP <i class="fa fa-sort"></i></th>
                    <th class="header">alamat LPP <i class="fa fa-sort"></i></th>
                    <th class="header">hasil akreditasi <i class="fa fa-sort"></i></th>
                    <th class="header">status <i class="fa fa-sort"></i></th>
                    <th class="header">Action<i class="fa fa-sort"></i></th>
                  </tr>
                </thead>
                <tbody>
                <?php $no =1; foreach ($semua_lpp as $data): 
                if($data->is_satker==1){
                	$satker = "iya";
                }else{
                	$satker = "bukan";
                }

                if($data->approve==1){
                  $stat = "Terdaftar";
                }else{
                  $stat = "Belum Terdaftar";
                }

                if($data->verified==1){
                    $verified = "<span style=\"color:green;\">verified</span>";
                }else{
                    $verified = "<span style=\"color:red;\">NOT verified</span>";
                }

                
                ?>
                  <tr>
                    <td><?php echo $no; ?></td>
                    <td><?php echo $data->nama_lpp; ?></td>
                    <td><?php echo $data->alamat_lpp; ?></td>
                    <td><?php echo $data->hasil_akreditasi; ?></td>
                    <td><?php if($data->hasil_akreditasi==null){ echo $stat; }else{ echo " "; }?></td>
                    <td>
                    <!-- <a href="<?php echo base_url(); ?>lkpp/verifikasi_lpp/<?php echo $data->id_lpp; ?>"><button type="submit" onclick="return confirm('Anda Yakin Mem-Verifikasi LPP ini ?')" class="btn btn-success" name="submit" value="kirim" id="delete">verifikasi</button></a> -->
                    <a href="<?php echo base_url(); ?>lkpp/detail_lpp/<?php echo $data->id_lpp; ?>"><button type="submit" class="btn btn-success" name="submit" value="kirim" id="delete"> detail </button></a>
                    <?php if($param!="baru"){ ?>
                    <a href="<?php echo base_url(); ?>lkpp/hapus_lpp/<?php echo $param."/".$data->id_lpp; ?>"><button type="submit" class="btn btn-danger" name="submit" value="kirim" id="delete" onclick="return confirm('Anda Yakin Akan Menghapus Data ini')">hapus</button></a>
                    <?php } ?>

                    <br><br>
                    <!-- <a href="<?php echo base_url(); ?>lkpp/delete_lpp/<?php echo $data->id_lpp; ?>"><button type="submit" class="btn btn-danger" name="submit" value="kirim" id="delete" onclick="return confirm('Anda Yakin Akan Menghapus LPP ini ?')">delete</button></a> -->
                    </td>
                  </tr>
                <?php $no++; endforeach; ?>
                </tbody>
              </table>

              <div id="pager" class="pager">
                <form>
                  <img src="<?php echo base_url()?>assets/table_sorter/addons/pager/icons/first.png" class="first"/>
                  <img src="<?php echo base_url()?>assets/table_sorter/addons/pager/icons/prev.png" class="prev"/>
                  <input type="text" class="pagedisplay"/>
                  <img src="<?php echo base_url()?>assets/table_sorter/addons/pager/icons/next.png" class="next"/>
                  <img src="<?php echo base_url()?>assets/table_sorter/addons/pager/icons/last.png" class="last"/>
                  <select class="pagesize">
                    <option selected="selected"  value="10">10</option>
                    <option value="20">20</option>
                    <option value="30">30</option>
                    <option  value="40">40</option>
                  </select>
                </form>
              </div>