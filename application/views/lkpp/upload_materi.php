 <div id="page-wrapper">
   <div class="row">
   <div class="col-lg-12">
     <div class="panel panel-primary">
<div class="panel-heading">
  <h3 class="panel-title"><i class="fa fa-bar-chart-o"></i> Tambah Materi Baru</h3>
</div>
<?php 
$status = $this->session->flashdata('status');
if(isset($status)){ echo $status; } 
?>
<div class="panel-body">
 <?php echo form_open_multipart('lkpp/proses_tambah_materi');?>
  <div class="form-group">
		  <label>Judul Materi</label><!-- <input type="text" class="form-control" name="narasumber"> -->
    <input type="text" class="form-control" name="judul_materi">
		     </div>
  <div class="form-group">
      <label>Kategori Materi</label><!-- <input type="text" class="form-control" name="narasumber"> -->
    <input type="text" class="form-control" name="kategori_materi">
  </div>
  <div class="form-group">
    <label>File Materi</label><!-- <input type="text" class="form-control" name="narasumber"> -->
    <input type="file" name="userfile" size="20" />
  </div>
  <div class="form-group">
    <label>Diperuntukan untuk</label><!-- <input type="text" class="form-control" name="narasumber"> -->
    <select name="role" class="form-control">
      <option value="all">Semua</option>
      <option value="lkpp">LKPP</option>
      <option value="lpp">LPP</option>
      <option value="fasilitator">Fasilitator</option>
      <option value="narasumber">Narasumber</option>
    </select>
  </div>
  <div class="form-group">
    <div id="files"></div>
  </div>
   <button type="submit" class="btn btn-primary" name="submit" value="kirim">Tambah Materi</button>
  <!-- <button type="submit" class="btn btn-danger" name="submit" value="cancel">Tolak Request</button> -->
  </form>
</div>
     </div>
     <table class="table table-hover table-striped tablesorter">
     <thead>
       <tr>
        <th class="header">id<i class="fa fa-sort"></i></th>
        <th class="header">judul materi<i class="fa fa-sort"></i></th>
        <th class="header">kategori materi <i class="fa fa-sort"></i></th>
        <th class="header">file materi <i class="fa fa-sort"></i></th>
        <th class="header">diperuntukan<i class="fa fa-sort"></i></th>
        <th class="header">action<i class="fa fa-sort"></i></th>
      </tr>
      </thead>
      <tbody>
      <?php $no = 1; foreach ($dataMateri as $data): ?>
        <tr>
          <td><?php echo $no; ?></td>
          <td><?php echo $data->judul_materi; ?></td>
          <td><?php echo $data->kategori_materi; ?></td>
          <td><a target="_blank" href="<?php echo base_url(); ?>assets/upload/materi/<?php echo $data->file_materi; ?>"> <?php echo $data->file_materi; ?></a></td>
          <td><?php echo $data->role; ?></td>
          <td><a href="<?php echo base_url(); ?>lkpp/delete_materi/<?php echo $data->id_library; ?>" onclick="return confirm('Anda Yakin Akan Menghapus Data ini')">delete</a></td>
      <?php $no++; endforeach; ?>
      </tbody>
      
    </table>
    <div id="pager" class="pager" style="margin-top:0px;">
                <form>
                  <img src="<?php echo base_url()?>assets/table_sorter/addons/pager/icons/first.png" class="first"/>
                  <img src="<?php echo base_url()?>assets/table_sorter/addons/pager/icons/prev.png" class="prev"/>
                  <input type="text" class="pagedisplay"/>
                  <img src="<?php echo base_url()?>assets/table_sorter/addons/pager/icons/next.png" class="next"/>
                  <img src="<?php echo base_url()?>assets/table_sorter/addons/pager/icons/last.png" class="last"/>
                  <select class="pagesize">
                    <option selected="selected"  value="10">10</option>
                    <option value="20">20</option>
                    <option value="30">30</option>
                    <option  value="40">40</option>
                  </select>
                </form>
              </div>
   </div>

 </div><!-- /.row -->

 