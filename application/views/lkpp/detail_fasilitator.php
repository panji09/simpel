<script type="text/javascript">
function pilih_status(selected_value){
  if(selected_value=="asn"){
    //alert("asn");
    $('#jenis_pegawai').css("display", "block");
    $('#nip').css("display", "block");
    $('#jabatan').css("display", "block");
  }else{
    //alert("bukan asn");
    $('#nip').css("display", "none");
    $('#jabatan').css("display", "none");
    $('#jenis_pegawai').css("display", "none");
    $('#golongan').css("display", "none");
    $('#pangkat').css("display", "none");
  }
}

function pilih_jenis(selected_value){
  $('#golongan').css("display", "block");
    $('#pangkat').css("display", "block");
}
</script>
<style type="text/css">
  #jenis_pegawai{
    display: none;
  }
  #golongan{
    display: none;
  }
  #pangkat{
    display: none;
  }
  #nip{
    display: none;
  }
  #jabatan{
    display: none;
  }
</style>
        <div id="page-wrapper">
          <div class="row">
          <div class="col-lg-12">
            <div class="panel panel-primary">
              <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-bar-chart-o"></i> Tambah Fasilitator Baru</h3>
              </div>
              <?php 
              $status = $this->session->flashdata('status');
              if(isset($status)){ echo $status; } 
              ?>
              <?php foreach ($dataBerita as $data): 
                ?>
              <div class="panel-body">
                <?php echo form_open(base_url().'lkpp/proses_update_Fasilitator'); ?>
                <input type="hidden" name="id_fasilitator" value="<?php echo $data->id_fasilitator; ?>">
                <div class="form-group">
		                <label>Nama lengkap</label>
                  <input type="text" class="form-control" name="nama_lengkap" value="<?php echo $data->nama_fasilitator; ?>">
		            </div>
                <!-- <div class="form-group">
                    <label>Alamat</label>
                  <input type="text" class="form-control" name="alamat" value="<?php echo $data->alamat_fasilitator; ?>">
                </div>
                <div class="form-group">
                    <label>No Telp</label>
                  <input type="text" class="form-control" name="no_telp" value="<?php echo $data->no_telp_fasilitator; ?>">
                </div>
                <div class="form-group">
                    <label>Tempat Lahir</label>
                  <input type="text" class="form-control" name="tempat_lahir" value="<?php echo $data->tempat_lahir_fasilitator; ?>">
                </div>
                <div class="form-group">
                    <label>Tanggal Lahir</label>
                  <input type="text" class="form-control" name="tgl_lahir" id="dari_tanggal" value="<?php echo $data->tanggal_lahir_fasilitator; ?>">
                </div>
                <div class="form-group">
                    <label>Gelar</label>
                  <input type="text" class="form-control" name="gelar" value="<?php echo $data->gelar_fasilitator; ?>">
                </div>
                <div class="form-group">
                    <label>Jenis Kelamin</label>
                    <input type="radio" name="jenis_kelamin" value="Pria">Pria
                    <input type="radio" name="jenis_kelamin" value="Wanita">Wanita
                </div>
                <div class="form-group" id="nip">
                    <label>NIP</label>
                  <input type="text" class="form-control" name="nip" value="<?php echo $data->nip_fasilitator; ?>">
                </div>
                <div class="form-group" id="jenis_pegawai">
                </div>
                <div class="form-group">
                    <label>Pendidikan</label>
                  <!-- <input type="text" class="form-control" name="pendidikan"> 
                  <select class="form-control" name="pendidikan">
                    <option value="S1">S1</option>
                    <option value="S2">S2</option>
                    <option value="S3">S3</option>
                  </select>
                </div>
                <div class="form-group">
                    <label>Instansi</label>
                  <input type="text" class="form-control" name="instansi" value="<?php echo $data->instansi_fasilitator; ?>">
                </div>
                <div class="form-group">
                    <label>Provinsi</label>
                  <input type="text" class="form-control" name="provinsi" value="<?php echo $data->provinsi_fasilitator; ?>">
                </div>
                <div class="form-group">
                    <label>Kota</label>
                  <input type="text" class="form-control" name="kota" value="<?php echo $data->kota_fasilitator; ?>">
                </div>
                <div class="form-group">
                    <label>Email</label>
                  <input type="text" class="form-control" name="email" value="<?php echo $data->email_fasilitator; ?>">
                </div> -->
                <div class="form-group">
                    <label>Username</label>
                  <input type="text" class="form-control" name="username" value="<?php echo $data->username; ?>">
                </div>
                <div class="form-group">
                    <label>Password</label>
                  <input type="password" class="form-control" name="password" value="<?php echo $data->password; ?>">
                </div>
                <!-- <div class="form-group">
                    <label>No Sertifikat</label>
                  <input type="text" class="form-control" name="no_sertifikat">
                </div> -->
                <!-- 
                
                 -->
                 <button type="submit" class="btn btn-primary" name="submit" value="kirim">Update Fasilitator</button>
                <!-- <button type="submit" class="btn btn-danger" name="submit" value="cancel">Tolak Request</button> -->
                </form>
                <?php endforeach; ?>
              </div>
            </div>
          </div>
        </div><!-- /.row -->