<br>
<?php foreach ($detail as $data): ?>
			<div class="col-lg-12">
            <div class="panel panel-primary">
              <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-bar-chart-o"></i><?php echo $data->judul_pelatihan; ?> </h3>
              </div>
              <div class="panel-body">
                <?php echo form_open(base_url().'lkpp/proses_request_pelatihan'); ?>
	                <input type="hidden" name="id_request" value="<?php echo $data->id_request; ?>">
                	<div class="form-group">
                		<label>Tema Pelatihan*</label>
		                <select class="form-control" name="tema_pelatihan"readonly>
		                  <option value="<?php echo $data->id_tema; ?>"><?php echo $data->judul_pelatihan; ?></option>
		                </select>
		            </div>
		            <div class="form-group">
		                <label>Dari tanggal*</label>
		                <input class="form-control" type="text" name="dari_tanggal" value="<?php echo $data->mulai_tanggal; ?>" readonly>
		            </div>
		            <div class="form-group">
		                <label>Sampai tanggal*</label>
		                <input class="form-control" type="text" name="sampai_tanggal" value="<?php echo $data->sampai_tanggal; ?>"readonly>
		            </div>
		            <div class="form-group">
		                <label>isi pelatihan*</label>
		                <textarea class="form-control" rows="4" name="isi_pelatihan"readonly><?php echo $data->isi_pelatihan; ?></textarea>
		             </div>
		             <div class="form-group">
		                <label>tempat pelatihan*</label>
		                <input class="form-control" type="text" id="tempat_pelatihan" name="tempat_pelatihan" value="<?php echo $data->tempat_pelatihan; ?>"readonly>
		            </div>
		            <div class="alert alert-warning"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>Alasan ditolak hanya diisi jika request pelatihan ingin ditolak</div>
		            <div class="form-group">
		                <label>alasan ditolak*</label>
		                <textarea class="form-control" rows="4" name="alasan_ditolak"></textarea>
		             </div>

		            <button type="submit" class="btn btn-primary" name="submit" value="approve">Approve Request</button>
		            <button type="submit" class="btn btn-danger" name="submit" value="cancel">Tolak Request</button>
                </form>
              </div>
            </div>
          </div>
        </div><!-- /.row -->
  	<?php endforeach; ?>