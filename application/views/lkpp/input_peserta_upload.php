<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Contoh CSV bisa di download di <a href="<?php echo base_url(); ?>assets/csv/contoh.rar">sini</a></div>
<?php echo form_open_multipart('lkpp/upload_csv_peserta/'.$id);?>
<input type="file" name="csvfile"><br>
<input type="submit" value="upload">
<?php echo form_close(); ?>

			<table class="table table-hover table-striped tablesorter">
                <thead>
                  <tr>
                    <th class="header">Nama Peserta <i class="fa fa-sort"></i></th>
                    <th class="header">Judul Pelatihan <i class="fa fa-sort"></i></th>
                    <th class="header">Pre Test<i class="fa fa-sort"></i></th>
                    <th class="header">Post Test<i class="fa fa-sort"></i></th>
                    <th class="header">Action<i class="fa fa-sort"></i></th>
                  </tr>
                </thead>
                <tbody>
                <?php foreach ($semua_peserta as $data): ?>
                  <tr>
                    <td><?php echo $data->nama_lengkap; ?></td>
                    <td><?php echo $data->gelar_akademis; ?></td>
                    <td><?php echo $data->pre_test; ?></td>
                    <td><?php echo $data->post_test; ?></td>
                    <td>
                    <button type="submit" class="btn btn-danger" name="submit" value="kirim" id="delete">delete</button>
                    <button type="submit" class="btn btn-success" name="submit" value="kirim" id="delete">detail</button>
                    </td>
                  </tr>
                <?php endforeach; ?>
                </tbody>
              </table>