        <div id="page-wrapper">
          <div class="row">
          <div class="col-lg-12">
            <div class="panel panel-primary">
              <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-bar-chart-o"></i> Tambah Berita Baru</h3>
              </div>
              <?php foreach ($dataBerita as $data): ?>
              <?php 
              $status = $this->session->flashdata('status');
              if(isset($status)){ echo $status; } 
              ?>
              <div class="panel-body">
               <?php echo form_open_multipart('lkpp/proses_edit_berita/'.$data->id_berita);?>
                <div class="form-group">
		                <label>Judul Berita</label><!-- <input type="text" class="form-control" name="narasumber"> -->
                  <input type="text" class="form-control" name="judul_berita" value="<?php echo $data->judul_berita; ?>">
		            </div>
                <div class="form-group">
                    <label>Tanggal Berita</label><!-- <input type="text" class="form-control" name="narasumber"> -->
                  <input type="text" class="form-control" name="tgl_berita" id="dari_tanggal" value="<?php echo $data->tgl_berita; ?>">
                </div>
                <div class="form-group">
                    <label>Berita</label><!-- <input type="text" class="form-control" name="narasumber"> -->
                    <script type="text/javascript" src="<?php echo base_url(); ?>assets/ckeditor/ckeditor.js"></script>
                    <textarea class="ckeditor" name="isi_berita"><?php echo $data->isi_berita; ?></textarea>
                </div>
                <div class="form-group">
                  <img src="<?php echo base_url()."/assets/upload/berita/".$data->photo_berita; ?>" width="128px">
                  <label>Photo Berita</label><!-- <input type="text" class="form-control" name="narasumber"> -->
                  <input type="file" name="userfile" size="20" />
                </div>
                <div class="form-group">
                    <label>Gunakan Foto Sebelumnya</label> <!-- <input type="text" class="form-control" name="narasumber"> -->
                    <?php if(isset($data->photo_berita)){?>
                    <input type="checkbox" name="cek_photo" checked> <span style="color:red;">*apabila foto diubah harap un-check tombol ini</span>
                    <?php } ?>
                </div>
                <div class="form-group">
                  <div id="files"></div>
                </div>
                 <button type="submit" class="btn btn-primary" name="submit" value="kirim">Update Berita</button>
                <!-- <button type="submit" class="btn btn-danger" name="submit" value="cancel">Tolak Request</button> -->
                </form>
                <?php endforeach; ?>
              </div>
            </div>
          </div>
        </div><!-- /.row -->