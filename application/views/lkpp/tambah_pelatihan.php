<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript">
 
function load_dropdown_content(selected_value){
	var html;
	var result;

	if(selected_value==1){
		 $('#instansi').css("visibility", "visible");
	}else{
		$('#instansi').css("visibility", "hidden");
	}

    /*var result = $.ajax({
        'url' : '<?php echo base_url(); ?>lkpp/getlppsatker/' + selected_value,
        'async' : false
    }).responseText;*/
	$.ajax({
        'url' : '<?php echo base_url(); ?>lkpp/getlppsatker/' + selected_value,
        'async' : false,
        beforeSend: function(){
            $("#id_lpp").html("please wait");
        },
        success: function(data)
        {
            result = data;
        }
    });
    //$('#id_lpp').append(result);
    var result = JSON.parse(result);

    for(var key in result) {
        html += "<option value=" + key  + ">" +result[key] + "</option>";
    }
    $('#id_lpp').html(html);

    //console.log(html);
}
</script>
<style type="text/css">
	#instansi{
		visibility: hidden;
	}
</style>

			<br>
			<div class="col-lg-12">
            <div class="panel panel-primary">
              <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-bar-chart-o"></i>Pelatihan Baru</h3>
              </div>
              <div class="panel-body">
                <?php echo form_open(base_url().'lkpp/proses_request_pelatihan'); ?>
                	<div class="form-group">
                		<label>Satker*</label>
		                <select class="form-control" name="satker" id="satker" onchange="javascript:load_dropdown_content(this.value)">
		                  <option value="0">bukan</option>
		                  <option value="1">satker</option>
		                </select>
		            </div>
		            <div class="form-group">
                		<label>LPP*</label>
		                <select class="form-control" name="id_lpp" id="id_lpp">

		                </select>
		            </div>
		            <div class="form-group" id="instansi">
                		<label>Instansi*</label>
		                <input type="text" name="instansi" class="form-control">
		            </div> 
                	<!-- <div class="form-group">
                		<label>LPP*</label>
		                <select class="form-control" name="id_lpp">
		                <?php foreach ($lpp as $data): ?>
		                  <option value="<?php echo $data->id_lpp; ?>"><?php echo $data->nama_lpp; ?></option>
		                  <?php endforeach; ?>
		                </select>
		            </div>
		            -->

		             <div class="form-group">
		                <label>tempat pelatihan*</label>
		                <input class="form-control" type="text" id="tempat_pelatihan" name="tempat_pelatihan" value="">
		            </div>
		            <div class="form-group">
		                <label>contact person*</label>
		                <textarea class="form-control" rows="4" name="isi_pelatihan"></textarea>
		             </div>
                	<div class="form-group">
                		<label>Tema Pelatihan*</label>
		                <select class="form-control" name="tema_pelatihan">
		                <?php foreach ($tema_pelatihan as $data): ?>
		                  <option value="<?php echo $data->id_tema; ?>"><?php echo $data->judul_pelatihan; ?></option>
		                  <?php endforeach; ?>
		                </select>
		            </div>
		            <div class="form-group">
		                <label>Dari tanggal*</label>
		                <input class="form-control" type="text" name="dari_tanggal" id="dari_tanggal" value="">
		            </div>
		            <div class="form-group">
		                <label>Sampai tanggal*</label>
		                <input class="form-control" type="text" name="sampai_tanggal" id="sampai_tanggal"  value="">
		            </div>

		            <button type="submit" class="btn btn-primary" name="submit" value="approve">Kirim</button>
                </form>
              </div>
            </div>
          </div>
        </div><!-- /.row -->
  	