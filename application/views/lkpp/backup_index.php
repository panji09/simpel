

      <div id="page-wrapper">

        <?php 
          $status = $this->session->flashdata('status');
          if(isset($status)){ echo $status; } 
        ?>
        <div class="row">

          <!-- <div class="col-lg-4">
            <div class="panel panel-info">
              <div class="panel-heading">
                <div class="row">
                  <div class="col-xs-6">
                    <i class="fa fa-tasks fa-5xb"></i>
                  </div>
                  <div class="col-xs-6 text-right">
                    <p class="announcement-heading"><?php echo $notifAllRequest; ?></p>
                    <p class="announcement-text">Total Request</p>
                  </div>
                </div>
              </div>
              <a href="#">
                <div class="panel-footer announcement-bottom">
                  <div class="row">
                    <div class="col-xs-6">
                      <a href="<?php echo base_url(); ?>lkpp/request/all">View All</a>
                    </div>
                    <div class="col-xs-6 text-right">
                      <i class="fa fa-arrow-circle-right"></i>
                    </div>
                  </div>
                </div>
              </a>
            </div>
          </div> -->

          <div class="col-lg-4">
            <div class="panel panel-info">
              <div class="panel-heading">
                <div class="row">
                  <div class="col-xs-6">
                    <i class="fa fa-tasks fa-5x"></i>
                  </div>
                  <div class="col-xs-6 text-right">
                    <p class="announcement-heading"><?php echo $notifAllLPP; ?></p>
                    <p class="announcement-text">Total LPP</p>
                  </div>
                </div>
              </div>
              <a href="#">
                <div class="panel-footer announcement-bottom">
                  <div class="row">
                    <div class="col-xs-6">
                      <a href="<?php echo base_url(); ?>lkpp/lpp/all">View All</a>
                    </div>
                    <div class="col-xs-6 text-right">
                      <i class="fa fa-arrow-circle-right"></i>
                    </div>
                  </div>
                </div>
              </a>
            </div>
          </div>

          <div class="col-lg-4">
            <div class="panel panel-info">
              <div class="panel-heading">
                <div class="row">
                  <div class="col-xs-6">
                    <i class="fa fa-tasks fa-5x"></i>
                  </div>
                  <div class="col-xs-6 text-right">
                    <p class="announcement-heading"><?php echo $notifAllNarasumber; ?></p>
                    <p class="announcement-text">Total Narasumber</p>
                  </div>
                </div>
              </div>
              <a href="#">
                <div class="panel-footer announcement-bottom">
                  <div class="row">
                    <div class="col-xs-6">
                      <a href="<?php echo base_url(); ?>lkpp/narasumber/all">View All</a>
                    </div>
                    <div class="col-xs-6 text-right">
                      <i class="fa fa-arrow-circle-right"></i>
                    </div>
                  </div>
                </div>
              </a>
            </div>
          </div>

          
          
        </div><!-- /.row -->

        <!-- <div class="row">
          <div class="col-lg-12">
            <div class="panel panel-primary">
              <div class="panel-heading">
                <?php 
                $status = $this->session->flashdata('status');
                if(isset($status)){ echo $status; } 
                ?>
                <h3 class="panel-title"><i class="fa fa-bar-chart-o"></i> Request Pelatihan Baru</h3>
              </div>
              
            </div>
          </div>
        </div> -->
        <!-- <form action="<?php echo base_url(); ?>lkpp" method="POST">
        <input class="form-control" type="text" placeholder="limit" style="width:190px;float:left" name="limit"><button type="submit" class="btn btn-primary" name="submit" value="kirim" id="edit">Kirim</button>
        </form> -->
        <br><br>
        <form method="POST" action="<?php echo base_url();?>lkpp/findnarasumber">
            <div style="border:1px solid #DADADA;padding:8px;">
                <tr>
                  <td><span style="float:left;margin:8px 4px 0px 0px;"> Narasumber </span></td>
                  <td>
                  <select name="id_narasumber" class="form-control" style="float:left;width:190px;margin:0px 4px 0px 0px;">
                  <option class="opt" value=""> -- All --</option>
                  <?php foreach ($narasumber as $data): ?>
                    <option class="opt" value="<?php echo $data->id_narasumber; ?>"><?php echo $data->nama_narasumber; ?></option>
                  <?php endforeach; ?>
                  </select>
                  </td>
                </tr>
                <tr>
                  <td></td>
                  <td>
                  <input type="text" name="dari_tanggal" id="dari_tanggal" placeholder="dari tanggal" class="form-control" style="float:left;width:190px;margin:0px 4px 0px 0px;" value="<?php if(isset($dari_tanggal)){ echo $dari_tanggal; } ?>">
                  </td>
                </tr>
                <tr>
                  <td></td>
                  <td>
                  <input type="text" name="sampai_tanggal" id="sampai_tanggal" placeholder="sampai tanggal" class="form-control" style="float:left;width:190px;margin:0px 4px 0px 0px;" value="<?php if(isset($sampai_tanggal)){ echo $sampai_tanggal; } ?>">
                  </td>
                </tr>
                <tr>
                  <td></td>
                  <td>
                  <input type="text" name="instansi" placeholder="instansi" class="form-control" style="float:left;width:190px;margin:0px 4px 0px 0px;" value="<?php if(isset($instansi)){ echo $instansi; } ?>">
                  </td>
                </tr>
                <tr>
                  <td></td>
                  <td>
                  <input type="text" name="catatan" placeholder="catatan" class="form-control" style="float:left;width:190px;margin:0px 4px 0px 0px;" value="<?php if(isset($catatan)){ echo $catatan; } ?>">
                  </td>
                </tr>
                <tr>
                  <td></td>
                  <td>
                  <input type="text" name="lokasi" placeholder="lokasi" class="form-control"  style="font-size:12px;border:1px solid #DADADA;padding:3px 10px;float:left;width:250px;margin:5px 0px 0px 80px;" value="<?php if(isset($lokasi)){ echo $lokasi; } ?>">
                  </td>
                </tr>
                <br>
                <tr>
                  <td></td>
                  <td>
                  <select name="JP" class="form-control"  style="font-size:12px;border:1px solid #DADADA;padding:3px 10px;float:left;width:250px;margin:5px 4px;">
                    <option>---------------------</option>  
                    <option value="terbanyak" <?php if(isset($JP)){ if($JP=="terbanyak") { echo "selected"; } } ?> >JP terbanyak</option>  
                    <option value="terkecil" <?php if(isset($JP)){ if($JP=="terkecil") { echo "selected"; } } ?> >JP terkecil</option>  
                  </select>
                  </td>
                </tr>
                <!-- <tr>
                  <td></td>
                  <td>
                  <input type="radio" name="JP" value="terbanyak" checked>JP terbanyak
                  </td>
                </tr>
                <tr>
                  <td></td>
                  <td>
                  <input type="radio"  name="JP" value="terkecil">JP terkecil
                  </td>
                </tr> -->
                <br><br><br><br>
                <button type="submit" class="btn btn-primary" name="submit" value="kirim" id="edit">Cari</button>
                <a href="<?php echo base_url(); ?>lkpp/">
                <button type="button" class="btn btn-primary">Reset</button></a>
            </div><br>
            </form>
        <div class="panel panel-primary">
              <div class="panel-heading">

                <h3 class="panel-title"><i class="fa fa-bar-chart-o"></i>Klasemen Narasumber</h3>
              </div>
                <table class="table table-hover table-striped tablesorter" class="tablesorter">
                <thead>
                  <tr>
                    <th class="header">Peringkat<i class="fa fa-sort"></i></th>
                    <th class="header">Nama Narasumber <i class="fa fa-sort"></i></th>
                    <th class="header">No Telp Narasumber <i class="fa fa-sort"></i></th>
                    <th class="header">Lokasi<i class="fa fa-sort"></i></th>
                    <th class="header">Instansi<i class="fa fa-sort"></i></th>
                    <th class="header">Email<i class="fa fa-sort"></i></th>
                    <th class="header">Total JP <i class="fa fa-sort"></i></th>
                    <th class="header">Action<i class="fa fa-sort"></i></th>
                  </tr>
                </thead>
                <tbody>
                <?php $no = $no_from_url+1; foreach ($klasemen_narasumber as $data): ?>
                  <tr>
                    <td><?php echo $no; ?></td>
                    <td><?php echo $data->nama_narasumber; ?></td>
                    <td><?php echo $data->no_telp; ?></td>
                    <td><?php echo $data->lokasi; ?></td>
                    <td><?php echo $data->instansi; ?></td>
                    <td><?php echo $data->email; ?></td>
                    <td><?php echo $data->total_jp; ?></td>
                    <td>
                    <form action="<?php echo base_url(); ?>lkpp/detail_kegiatan_narasumber/<?php echo $data->id_narasumber; ?>" method="POST">
                    <input type="hidden" name="id_narasumber" value="<?php echo $data->id_narasumber; ?>">
                    <input type="hidden" name="condition" value="<?php echo $conditionQuery; ?>">
                    <a href=""><button type="submit" class="btn btn-success" name="submit" value="kirim" id="delete">detail</button></a>
                    </form>
                    </td>
                  </tr>
                <?php $no++; endforeach; ?>

                </tbody>
              </table>
              <div id="pager" class="pager">
                <form>
                  <img src="<?php echo base_url()?>assets/table_sorter/addons/pager/icons/first.png" class="first"/>
                  <img src="<?php echo base_url()?>assets/table_sorter/addons/pager/icons/prev.png" class="prev"/>
                  <input type="text" class="pagedisplay"/>
                  <img src="<?php echo base_url()?>assets/table_sorter/addons/pager/icons/next.png" class="next"/>
                  <img src="<?php echo base_url()?>assets/table_sorter/addons/pager/icons/last.png" class="last"/>
                  <select class="pagesize">
                    <option selected="selected"  value="10">10</option>
                    <option value="20">20</option>
                    <option value="30">30</option>
                    <option  value="40">40</option>
                  </select>
                </form>
              </div>
              <?php //echo $links; ?>
      </div><!-- /#page-wrapper -->

    </div><!-- /#wrapper -->