            <div id="page-wrapper">
            <a href="<?php echo base_url(); ?>lkpp/tambah_tema_pelatihan"><i class="fa fa-dashboard"></i> <button type="submit" class="btn btn-primary" name="submit" value="kirim" id="edit">Tambah Jenis Pelatihan</button></a><br><br>
            <div class="row">
            <div class="col-lg-12">
            <?php 
              $status = $this->session->flashdata('status');
              if(isset($status)){ echo $status; } 
              ?>
            <div class="panel panel-primary">
              <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-bar-chart-o"></i>Semua Jenis Pelatihan</h3>
              </div>
                <table class="table table-hover table-striped tablesorter">
                <thead>
                  <tr>
                    <th class="header">No<i class="fa fa-sort"></i></th>
                    <th class="header">Jenis Pelatihan<i class="fa fa-sort"></i></th>
                    <th class="header">Keterangan <i class="fa fa-sort"></i></th>
                    <th class="header">action<i class="fa fa-sort"></i></th>
                  </tr>
                </thead>
                <tbody>
                <?php $no =1;  foreach ($dataTemaPelatihan as $data): ?>
                  <tr>
                    <td><?php echo $no; ?></td>
                    <td><?php echo $data->judul_pelatihan; ?></td>
                    <td><?php echo $data->keterangan; ?></td>
                    <td>
                    <a href="<?php echo base_url(); ?>lkpp/edit_tema_pelatihan/<?php echo $data->id_tema; ?>"><button type="submit" class="btn btn-success" name="submit" value="kirim" id="delete" title="delete">Edit</button></a>
                    <a href="<?php echo base_url(); ?>lkpp/delete_tema_pelatihan/<?php echo $data->id_tema; ?>"><button type="submit" class="btn btn-danger" name="submit" value="kirim" id="delete" title="delete" onclick="return confirm('Anda Yakin Akan Menghapus Tema ini ?')">Delete</button></a>
                    </td>
                  </tr>
                <?php $no++; endforeach; ?>

                </tbody>
              </table>
              <div id="pager" class="pager">
                <form>
                  <img src="<?php echo base_url()?>assets/table_sorter/addons/pager/icons/first.png" class="first"/>
                  <img src="<?php echo base_url()?>assets/table_sorter/addons/pager/icons/prev.png" class="prev"/>
                  <input type="text" class="pagedisplay"/>
                  <img src="<?php echo base_url()?>assets/table_sorter/addons/pager/icons/next.png" class="next"/>
                  <img src="<?php echo base_url()?>assets/table_sorter/addons/pager/icons/last.png" class="last"/>
                  <select class="pagesize">
                    <option selected="selected"  value="10">10</option>
                    <option value="20">20</option>
                    <option value="30">30</option>
                    <option  value="40">40</option>
                  </select>
                </form>
              </div>