 <div id="page-wrapper">
   <div class="row">
   <div class="col-lg-12">
     <div class="panel panel-primary">
<div class="panel-heading">
  <h3 class="panel-title"><i class="fa fa-bar-chart-o"></i> Tambah Dokumen Baru</h3>
</div>
<?php 
$status = $this->session->flashdata('status');
if(isset($status)){ echo $status; } 
?>
<div class="panel-body">
 <?php 
 if($action=="edit"){
  foreach ($dataPengumumanById as $data):
    echo form_open_multipart('lkpp/proses_edit_pengumuman/'.$data->id_pengumuman);
 ?>
  <div class="form-group">
      <label>Tanggal Pengumuman</label><!-- <input type="text" class="form-control" name="narasumber"> -->
    <input type="text" class="form-control" name="tgl_pengumuman" id="dari_tanggal" value="<?php echo $data->tgl_pengumuman; ?>">
         </div>
  <div class="form-group">
      <label>Isi Pengumuman</label><!-- <input type="text" class="form-control" name="narasumber"> -->
    <input type="text" class="form-control" name="judul_pengumuman" value="<?php echo $data->isi_pengumuman; ?>">
  </div>
  <div class="form-group">
      <label>Header</label><br>
      <input type="radio" name="header" value="1" <?php if($data->header==1) echo "checked"; ?>>ya<br>
      <input type="radio" name="header" value="0" <?php if($data->header==0) echo "checked"; ?>>tidak
  </div>
  <div class="form-group">
    <div id="files"></div>
  </div>
   <button type="submit" class="btn btn-primary" name="submit" value="kirim">Edit Pengumuman</button>
  <?php
  endforeach;
  }else{
    echo form_open_multipart('lkpp/proses_tambah_pengumuman');
  ?>
  <div class="form-group">
      <label>Tanggal Pengumuman</label><!-- <input type="text" class="form-control" name="narasumber"> -->
    <input type="text" class="form-control" name="tgl_pengumuman" id="dari_tanggal">
         </div>
  <div class="form-group">
      <label>Judul Pengumuman</label><!-- <input type="text" class="form-control" name="narasumber"> -->
    <input type="text" class="form-control" name="judul_pengumuman" id="dari_tanggal">
  </div>
  <div class="form-group">
      <label>Isi Pengumuman</label><!-- <input type="text" class="form-control" name="narasumber"> -->
   <script type="text/javascript" src="<?php echo base_url(); ?>assets/ckeditor/ckeditor.js"></script>
                    <textarea class="ckeditor" name="isi_pengumuman"></textarea>
  </div>
  <div class="form-group">
      <label>Header</label><br>
      <input type="radio" name="header" value="1">ya<br>
      <input type="radio" name="header" value="0">tidak
  </div>
  <div class="form-group">
    <label>File Pengumuman</label><!-- <input type="text" class="form-control" name="narasumber"> -->
    <input type="file" name="userfile" size="20" />
  </div>
   <button type="submit" class="btn btn-primary" name="submit" value="kirim">Tambah Pengumuman</button>
  <?php 
  }
  ?>
  <!-- <button type="submit" class="btn btn-danger" name="submit" value="cancel">Tolak Request</button> -->
  </form>
</div>
     </div>
     <table class="table table-hover table-striped tablesorter">
     <thead>
       <tr>
        <th class="header">id<i class="fa fa-sort"></i></th>
        <th class="header">Tanggal Pengumuman<i class="fa fa-sort"></i></th>
        <th class="header">Judul Pengumuman<i class="fa fa-sort"></i></th>
        <th class="header">Isi Pengumuman<i class="fa fa-sort"></i></th>
        <th class="header">Header<i class="fa fa-sort"></i></th>
        <th class="header">Action<i class="fa fa-sort"></i></th>
      </tr>
      </thead>
      <tbody>
      <?php $no = 1; foreach ($dataPengumuman as $data): 
      if($data->header==1){
        $data->header = "Ya";
      }else{
        $data->header = "Tidak";
      }
      ?>
        <tr>
          <td><?php echo $no; ?></td>
          <td><?php echo $data->tgl_pengumuman; ?></td>
          <td><?php echo $data->judul_pengumuman; ?></td>
          <td><?php echo $data->isi_pengumuman; ?></td>
          <td><?php echo $data->header; ?></td>
          <td>
            <a href="<?php echo base_url(); ?>lkpp/pengumuman/edit/<?php echo $data->id_pengumuman; ?>"><button type="submit" class="btn btn-success" name="submit" value="kirim" id="delete" title="detail">edit</button></a>
            <a href="<?php echo base_url().$role."/delete_pengumuman/".$data->id_pengumuman; ?>"><button type="submit" class="btn btn-danger" name="submit" value="kirim" id="delete" onclick="return confirm('Anda Yakin Akan Menghapus Berita ini')">delete</button></a>
          <td>
      <?php $no++; endforeach; ?>
      </tbody>
      
    </table>
    <div id="pager" class="pager" style="margin-top:0px;">
                <form>
                  <img src="<?php echo base_url()?>assets/table_sorter/addons/pager/icons/first.png" class="first"/>
                  <img src="<?php echo base_url()?>assets/table_sorter/addons/pager/icons/prev.png" class="prev"/>
                  <input type="text" class="pagedisplay"/>
                  <img src="<?php echo base_url()?>assets/table_sorter/addons/pager/icons/next.png" class="next"/>
                  <img src="<?php echo base_url()?>assets/table_sorter/addons/pager/icons/last.png" class="last"/>
                  <select class="pagesize">
                    <option selected="selected"  value="10">10</option>
                    <option value="20">20</option>
                    <option value="30">30</option>
                    <option  value="40">40</option>
                  </select>
                </form>
              </div>
   </div>

 </div><!-- /.row -->

 