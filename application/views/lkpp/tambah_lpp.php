<div id="page-wrapper">
<div class="row">
          <div class="col-lg-12">
            <div class="panel panel-primary">
              <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-bar-chart-o"></i>FORM PENDAFTARAN<br>
					LEMBAGA PENYELENGGARA PELATIHAN (LPP) DAN <br>
					VERIFIKASI PELAKSANA UJIAN<br></h3>
              </div>
              <?php 
              $status = $this->session->flashdata('status');
              if(isset($status)){ echo $status; } 
              ?>
              <div class="panel-body">
				      <?php 
			              $status = $this->session->flashdata('status');
			              if(isset($status)){ echo $status; } 
			              ?>
			              <div class="panel-body">
			                <?php echo form_open_multipart(base_url().'daftar_lpp/proses_tambah_lpp'); ?>
			                	<input type="hidden" name="from" value="home">
					             <div class="form-group">
					                <label>Nama Lembaga*</label>
					                <input class="form-control" type="text" id="nama_lpp" name="nama_lpp">
					            </div>
					            <div class="form-group">
					                <label>Penanggungjawab*</label>
					                <input class="form-control" type="text" id="nama_lpp" name="penanggungjawab">
					            </div>
					            <div class="form-group">
					                <label>No KTP*</label>
					                <input class="form-control" type="text" id="nama_lpp" name="no_ktp">
					            </div>
					            <div class="form-group">
					                <label>Lampiran KTP*</label>
					                <input class="form-control" type="file" id="nama_lpp" name="files[]" >
					            </div>
					            <div class="form-group">
					                <label>Program*</label>
					                <input class="form-control" type="text" id="nama_lpp" name="program">
					            </div>
					            <div class="form-group">
					                <label>Alamat/Sekertariat*</label>
					                <input class="form-control" type="text" id="alamat_lpp" name="alamat_lpp">
					            </div>
					            <div class="form-group">
					                <label>No Telp*</label>
					                <input class="form-control" type="text" id="no_telp_lpp" name="no_telp_lpp">
					            </div>
					            <div class="form-group">
					                <label>No Fax*</label>
					                <input class="form-control" type="text" id="nama_lpp" name="no_fax">
					            </div>
					            <div class="form-group">
					                <label>Email*</label>
					                <input class="form-control" type="text" id="nama_lpp" name="email">
					            </div>

					            <div class="form-group" style="margin:20px 0px;padding:20px;">
					            	<div class="form-group">
						                <label>Kontak Person*</label>
						            </div>
						            <div class="form-group" style="margin:10px;">
						                <label>Nama*</label>
						                <input class="form-control" type="text" id="nama_lpp" name="kontak_nama">
						            </div>
						            <div class="form-group" style="margin:10px;">
						                <label>Telp/HP*</label>
						                <input class="form-control" type="text" id="nama_lpp" name="kontak_telp">
						            </div>
						            <div class="form-group" style="margin:10px;">
						                <label>Email*</label>
						                <input class="form-control" type="text" id="nama_lpp" name="kontak_email">
						            </div>
					            </div>
					            <div class="form-group" style="margin:20px 0px;padding:20px;">
					            	<div class="form-group">
						                <label>Harap Melampirkan dokumen sbb *</label>
						            </div>
						            <div class="form-group" style="margin:10px;">
						                <label>1.	Surat Ijin Operasional *)</label>
						                <input class="form-control" type="file" id="nama_lpp" name="files[]">
						            </div>
						            <div class="form-group" style="margin:10px;">
						                <label>2.	Surat Keterangan domisili *)</label>
						                <input class="form-control" type="file" id="nama_lpp" name="files[]">
						            </div>
						            <div class="form-group" style="margin:10px;">
						                <label>3.	Akta Notaris *)</label>
						                <input class="form-control" type="file" id="nama_lpp" name="files[]">
						            </div>
						            <div class="form-group" style="margin:10px;">
						                <label>4.	Mekanisme Keuangan **)</label>
						                <input class="form-control" type="file" id="nama_lpp" name="files[]">
						            </div>
						            <div class="form-group" style="margin:10px;">
						                <label>5.	Sistem Manajemen Mutu (SOP) *) **)</label>
						                <input class="form-control" type="file" id="nama_lpp" name="files[]">
						            </div>
						            <div class="form-group" style="margin:10px;">
						                <label>6.	Surat Pernyataan Komitmen *) **)</label>
						                <input class="form-control" type="file" id="nama_lpp" name="files[]">
						            </div>
						            <div class="form-group" style="margin:10px;">
						                <label>7.	Tugas dan Fungsi Pelaksana Ujian **)</label>
						                <input class="form-control" type="file" id="nama_lpp" name="files[]">
						            </div>
					            </div>

					            <div class="form-group">
					                <label>*)    Khusus Lembaga Swasta dan Perguruan Tinggi Negeri/Swasta</label><br>
					                <label>**)  Khusus Instansi Pemerintah/ Perguruan Tinggi Negeri </label>
					            </div>
					            <!-- <div class="form-group">
					                <label>Username*</label>
					                <input class="form-control" type="text" id="username" name="username">
					            </div>
					            <div class="form-group">
					                <label>Password*</label>
					                <input class="form-control" type="password" id="password" name="password">
					            </div>
					            <div class="form-group">
					                <label>No Akreditasi LPP*</label>
					                <input class="form-control" type="text" id="no_akreditasi_lpp" name="no_akreditasi_lpp">
					            </div>
					            <div class="form-group">
					                <label>Hasil Akreditasi*</label>
					                <input class="form-control" type="text" id="hasil_akreditasi" name="hasil_akreditasi">
					            </div>
					            <div class="form-group">
					                <label>Satker*</label>
					                <input type="radio" name="satker" value="1">iya
					                <input type="radio" name="satker" value="0">bukan
					            </div>
					            <div class="form-group">
					                <label>File Akreditasi*</label>
					                <input class="form-control" type="text" id="file_akreditasi" name="file_akreditasi">
					            </div> -->
					            <input type="submit" class="btn btn-primary" name="submit" value="Daftar">
					            <!-- <button type="submit" class="btn btn-danger" name="submit" value="cancel">Tolak Request</button> -->
			                </form>
			              </div>
				  </div>
		      </div>

		      
		    </div>
		    <!-- / content body -->
		  </div>
		</div>
      
    </div>
</div>