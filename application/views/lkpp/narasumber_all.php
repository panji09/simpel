            <div id="page-wrapper">
            <form method="POST" action="<?php echo base_url();?>lkpp/findDataNarasumber">
            <div style="border:1px solid #DADADA;padding:8px;">
                <tr>
                  <td><span style="float:left;margin:8px 4px 0px 0px;"> Narasumber </span></td>
                  <td>
                  <select name="id_narasumber" class="form-control" style="float:left;width:190px;margin:0px 4px 0px 0px;">
                  <option class="opt" value=""> -- All --</option>
                  <?php foreach ($narasumber as $data): ?>
                    <option class="opt" value="<?php echo $data->id_narasumber; ?>"><?php echo $data->nama_narasumber; ?></option>
                  <?php endforeach; ?>
                  </select>
                  </td>
                  <td>
                   <span   style="padding:3px 10px;float:left;width:80px;margin:0px 0px 0px 0px;">Lokasi :</span>
                  </td>
                  <td> 
                    <input type="text" name="lokasi" placeholder="lokasi" class="form-control"  style="font-size:12px;border:1px solid #DADADA;padding:3px 10px;float:left;width:250px;margin:0px 0px 0px 0px;" value="<?php if(isset($lokasi)){ echo $lokasi; } ?>">
                  </td>
                </tr>
                <br><br><br><br>
                <button type="submit" class="btn btn-primary" name="submit" value="kirim" id="edit">Cari</button>
                <a href="<?php echo base_url(); ?>lkpp/">
                <button type="button" class="btn btn-primary">Reset</button></a>
            </div><br>
            </form>
            <a href="<?php echo base_url(); ?>lkpp/tambah_narasumber"><i class="fa fa-dashboard"></i> <button type="submit" class="btn btn-primary" name="submit" value="kirim" id="edit">Tambah Narasumber</button></a><br><br>
            <div class="row">
            <div class="col-lg-12">
            <div class="panel panel-primary">
              <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-bar-chart-o"></i>Semua Narasumber</h3>
              </div>
                <table class="table table-hover table-striped tablesorter">
                <thead>
                  <tr>
                    <th class="header">No<i class="fa fa-sort"></i></th>
                    <th class="header">Nama<i class="fa fa-sort"></i></th>
                    <th class="header">Lokasi<i class="fa fa-sort"></i></th>
                    <th class="header">Instansi <i class="fa fa-sort"></i></th>
                    <th class="header">Action<i class="fa fa-sort"></i></th>
                  </tr>
                </thead>
                <tbody>
                <?php $no = $no_from_url;  foreach ($semua_narasumber as $data): ?>
                  <tr>
                    <td><?php echo $no; ?></td>
                    <td><?php echo $data->nama_narasumber; ?></td>
                    <td><?php echo $data->lokasi; ?></td>
                    <td><?php echo $data->instansi_narsum; ?></td>
                    <td>
                    <a href="<?php echo base_url(); ?>lkpp/detail_narasumber/<?php echo $data->id_narasumber; ?>"><button type="submit" class="btn btn-success" name="submit" value="kirim" id="delete">detail</button></a><br><br>
                    <a href="<?php echo base_url(); ?>lkpp/delete_narasumber/<?php echo $data->id_narasumber; ?>"><button type="submit" class="btn btn-danger" name="submit" value="kirim" id="delete" onclick="return confirm('Anda Yakin Akan Menghapus narasumber ini ?')">delete</button></a>
                    </td>
                  </tr>
                <?php $no++; endforeach; ?>

                </tbody>
              </table>
              <div id="pager" class="pager">
                <form>
                  <img src="<?php echo base_url()?>assets/table_sorter/addons/pager/icons/first.png" class="first"/>
                  <img src="<?php echo base_url()?>assets/table_sorter/addons/pager/icons/prev.png" class="prev"/>
                  <input type="text" class="pagedisplay"/>
                  <img src="<?php echo base_url()?>assets/table_sorter/addons/pager/icons/next.png" class="next"/>
                  <img src="<?php echo base_url()?>assets/table_sorter/addons/pager/icons/last.png" class="last"/>
                  <select class="pagesize">
                    <option selected="selected"  value="10">10</option>
                    <option value="20">20</option>
                    <option value="30">30</option>
                    <option  value="40">40</option>
                  </select>
                </form>
              </div>