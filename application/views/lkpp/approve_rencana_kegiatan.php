            <div id="page-wrapper">
            
            <div class="row">
            <div class="col-lg-12">
            <div class="panel panel-primary">
              <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-bar-chart-o"></i></h3>
              </div>
                <table class="table table-hover table-striped tablesorter">
                <thead>
                  <tr>
                    <th class="header">No<i class="fa fa-sort"></i></th>
                    <th class="header">narasumber<i class="fa fa-sort"></i></th>
                    <th class="header">instansi<i class="fa fa-sort"></i></th>
                    <th class="header">dari tanggal <i class="fa fa-sort"></i></th>
                    <th class="header">sampai tanggal <i class="fa fa-sort"></i></th>
                    <th class="header">jumlah JP <i class="fa fa-sort"></i></th>
                    <th class="header">File <i class="fa fa-sort"></i></th>
                    <th class="header">keterangan agenda<i class="fa fa-sort"></i></th>
                    <th class="header">action<i class="fa fa-sort"></i></th>
                  </tr>
                </thead>
                <tbody>
                <?php 
                  $status = $this->session->flashdata('status');
                  if(isset($status)){ echo $status; } 

                $no = 1; foreach ($dataBerita as $data): 
                if( $data->approved==1){
                	$approved = "<span style =\"color:green;font-size:18px;\">Approved</span>";
                }else{
                  $approved = "";
                	//$approved = "<span style =\"color:darkred;font-size:18px;\">Belum Di Approve</span>";
                  //$file = str_replace(" ", "%20", $data->bukti_kegiatan);
                }
                ?>
                  <tr>
                    <td><?php echo $no; ?></td>
                    <td><?php echo $data->nama_narasumber; ?></td>
                    <td><?php echo $data->instansi; ?></td>
                    <td><?php echo $data->dari_tanggal; ?></td>
                    <td><?php echo $data->sampai_tanggal; ?></td>
                    <td><?php echo $data->jumlah_jp; ?></td>
                    <td><a href="<?php echo base_url(); ?>assets/upload/bukti_kegiatan/<?php echo urldecode($data->bukti_kegiatan); ?>"> <?php echo $data->bukti_kegiatan; ?></td>
                    <td><?php echo $data->keterangan; ?></td>
                    <td>
                    <?php echo $approved; if($data->approved==0){?>
                    <a href="<?php echo base_url(); ?>lkpp/approve_agenda/<?php echo $data->id_rencana; ?>">
                    <button type="submit" class="btn btn-success" name="submit" value="kirim" id="delete" title="detail">Approve</button>
                    <?php } ?>
                    </a>
                    <a href="<?php echo base_url(); ?>lkpp/detail_agenda/<?php echo $data->id_rencana; ?>"> <button type="submit" class="btn btn-success" name="submit" value="kirim" id="delete" title="detail">Detail</button></a>
                    <br><br>
                    <form method="POST" action="<?php echo base_url(); ?>lkpp/tolak_agenda/<?php echo $data->id_rencana; ?>">
                    <textarea name="alasan_ditolak"></textarea><br>
                    <a href="<?php echo base_url(); ?>lkpp/detail_agenda/<?php echo $data->id_rencana; ?>"> <button type="submit" class="btn btn-danger" name="submit" value="kirim" id="delete" title="detail">Tolak</button></a>
                    </form>
                    </td>
                  </tr>
                <?php $no++; endforeach; ?>

                </tbody>
              </table>
              <div id="pager" class="pager" style="margin-top:0px;">
                <form>
                  <img src="<?php echo base_url()?>assets/table_sorter/addons/pager/icons/first.png" class="first"/>
                  <img src="<?php echo base_url()?>assets/table_sorter/addons/pager/icons/prev.png" class="prev"/>
                  <input type="text" class="pagedisplay"/>
                  <img src="<?php echo base_url()?>assets/table_sorter/addons/pager/icons/next.png" class="next"/>
                  <img src="<?php echo base_url()?>assets/table_sorter/addons/pager/icons/last.png" class="last"/>
                  <select class="pagesize">
                    <option selected="selected"  value="10">10</option>
                    <option value="20">20</option>
                    <option value="30">30</option>
                    <option  value="40">40</option>
                  </select>
                </form>
              </div>