<?php foreach ($detail as $data) : ?>
<div id="page-wrapper">
<div class="row">
          <div class="col-lg-12">
            <div class="panel panel-primary">
              <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-bar-chart-o"></i> Detail LPP</h3>
              </div>
              <?php 
              $status = $this->session->flashdata('status');
              if(isset($status)){ echo $status; } 
              ?>
              <div class="panel-body">
                <?php echo form_open(base_url().'lkpp/proses_update_lpp'); ?>
			         <input type="hidden" name="id_lpp" value="<?php echo $data->id_lpp; ?>">
			         <input type="hidden" name="from" value="home">
		             <div class="form-group">
		                <label>Status Kelembagaan*</label><br>
		                <h5><?php if($data->surat_ijin_operasional!=" "){ echo "Swasta"; }else { echo "Negeri"; } ?></h5>
		            </div>
		             <div class="form-group">
		                <label>Nama Lembaga*</label>
		                <input class="form-control" type="text" id="nama_lpp" name="nama_lpp" value="<?php echo $data->nama_lpp; ?>">
		            </div>
		            <div class="form-group">
		                <label>Penanggung jawab*</label>
		                <input class="form-control" type="text" id="nama_lpp" name="penanggungjawab" value="<?php echo $data->penanggungjawab; ?>">
		            </div>
		            <div class="form-group">
		                <label>Jabatan*</label>
		                <input class="form-control" type="text" id="jabatan" name="jabatan" value="<?php echo $data->jabatan; ?>">
		            </div>
		            <div class="form-group">
		                <label>No KTP*</label>
		                <input class="form-control" type="text" id="nama_lpp" name="no_ktp" value="<?php echo $data->no_ktp; ?>">
		            </div>
		            <div class="form-group">
		                <label>Program*</label>
		                <input class="form-control" type="text" id="nama_lpp" name="program" value="<?php echo $data->program; ?>">
		            </div>
		            <div class="form-group">
		                <label>Alamat/Sekertariat*</label>
		                <input class="form-control" type="text" id="alamat_lpp" name="alamat_lpp" value="<?php echo $data->alamat_lpp; ?>">
		            </div>
		            <div class="form-group">
		                <label>No Telp*</label>
		                <input class="form-control" type="text" id="no_telp_lpp" name="no_telp_lpp" value="<?php echo $data->no_telp_lpp; ?>">
		            </div>
		            <div class="form-group">
		                <label>No Fax*</label>
		                <input class="form-control" type="text" id="nama_lpp" name="no_fax" value="<?php echo $data->no_fax; ?>">
		            </div>
		            <div class="form-group">
		                <label>Email*</label>
		                <input class="form-control" type="text" id="nama_lpp" name="email" value="<?php echo $data->email; ?>">
		            </div>
		            <div class="form-group">
		                <label>Website*</label>
		                <input class="form-control" type="text" id="website" name="website" value="<?php echo $data->website; ?>">
		            </div>

		            <div class="form-group" style="margin:20px 0px;padding:20px;">
		            	<div class="form-group">
			                <label>Kontak Person*</label>
			            </div>
			            <div class="form-group" style="margin:10px;">
			                <label>Nama*</label>
			                <input class="form-control" type="text" id="nama_lpp" name="kontak_nama" value="<?php echo $data->kontak_nama; ?>">
			            </div>
			            <div class="form-group" style="margin:10px;">
			                <label>Telp/HP*</label>
			                <input class="form-control" type="text" id="nama_lpp" name="kontak_telp" value="<?php echo $data->kontak_tlp; ?>">
			            </div>
			            <div class="form-group" style="margin:10px;">
			                <label>Email*</label>
			                <input class="form-control" type="text" id="nama_lpp" name="kontak_email" value="<?php echo $data->kontak_email; ?>">
			            </div>
		            </div>
		            <div class="form-group" style="margin:20px 0px;padding:20px;">
		            	<div class="form-group">
			                <label>File Lampiran</label>
			            </div>

			            
			            <table border="1">

			            <tr>
			            		<td style="padding:5px 10px"><label>	Lampiran KTP </label></td style="padding:5px 10px">
			            		<td style="padding:5px 10px"><a href="<?php echo base_url()."assets/dokumen_lpp/".str_replace(" ","_",$data->nama_lpp)."/".$data->lampiran_ktp; ?>"><?php echo $data->lampiran_ktp; ?></a></td style="padding:5px 10px">
			            		<td style="padding:5px 10px"></td style="padding:5px 10px">
			            		
			            	</tr>


			            	
			            <?php if(($data->surat_ijin_operasional)!=" "){ ?>
			            	<tr>
			            		<td style="padding:5px 10px"><label>	Surat Ijin Operasional </label></td style="padding:5px 10px">
			            		<td style="padding:5px 10px"><a href="<?php echo base_url()."assets/dokumen_lpp/".str_replace(" ","_",$data->nama_lpp)."/".$data->surat_ijin_operasional; ?>"><?php echo $data->surat_ijin_operasional; ?></a></td style="padding:5px 10px">
			            		<td style="padding:5px 10px"><?php  
			                		if($data->status_surat_ijin!=1){
			                ?>
			                	<a href="<?php echo base_url(); ?>lkpp/verifikasi/surat_ijin/1/<?php echo $id; ?>">
			                		<button type="button" class="btn btn-primary" name="submit" value="kirim">Verifikasi
			                		</button>
			                	</a>
					                <?php }else{ ?>
					                	
					                	<img src= "<?php echo base_url(); ?>assets/approved.png" title="Terverifikasi">
					                	&nbsp;
					                	<a href="<?php echo base_url(); ?>lkpp/verifikasi/surat_ijin/0/<?php echo $id; ?>">
					                		<button type="button" class="btn btn-danger" name="submit" value="kirim">Batalkan Verifikasi
					                		</button>
					                	</a>
					            </div>
					            <?php } ?>
					            </td style="padding:5px 10px">
			            		
			            	</tr>
			            <?php } ?>


			            <?php if(($data->surat_ket_domisili)!=" "){  ?>
			            	<tr>
			            		<td style="padding:5px 10px"><label>	Surat Keterangan domisili </label></td style="padding:5px 10px">
			            		<td style="padding:5px 10px"><a href="<?php echo base_url()."assets/dokumen_lpp/".str_replace(" ","_",$data->nama_lpp)."/".$data->surat_ket_domisili; ?>"><?php echo $data->surat_ket_domisili; ?></a></td style="padding:5px 10px">
			            		<td style="padding:5px 10px"><?php 
			                		if($data->status_domisili!=1){
			                ?>
			                
			                	<a href="<?php echo base_url(); ?>lkpp/verifikasi/domisili/1/<?php echo $id; ?>">
			                		<button type="button" class="btn btn-primary" name="submit" value="kirim">Verifikasi
			                		</button>
			                	</a>
				                <?php }else{ ?>
				                	
				                	<img src= "<?php echo base_url(); ?>assets/approved.png" title="Terverifikasi">
				                	&nbsp;
				                	<a href="<?php echo base_url(); ?>lkpp/verifikasi/domisili/0/<?php echo $id; ?>">
				                		<button type="button" class="btn btn-danger" name="submit" value="kirim">Batalkan Verifikasi
				                		</button>
				                	</a>
				                <?php } ?>
				                </td style="padding:5px 10px">
			            		
			            	</tr>
			            <?php } ?>

			            
			            <?php if(($data->akta_notaris)!=" "){  ?>
			            	<tr>
			            		<td style="padding:5px 10px"><label>	Akta Notaris </label></td style="padding:5px 10px">
			            		<td style="padding:5px 10px"><a href="<?php echo base_url()."assets/dokumen_lpp/".str_replace(" ","_",$data->nama_lpp)."/".$data->akta_notaris; ?>"><?php echo $data->akta_notaris; ?></a></td style="padding:5px 10px">
			            		<td style="padding:5px 10px"><?php 
			                		if($data->status_akta!=1){
			                ?>
			                
			                	<a href="<?php echo base_url(); ?>lkpp/verifikasi/akta/1/<?php echo $id; ?>">
			                		<button type="button" class="btn btn-primary" name="submit" value="kirim">Verifikasi
			                		</button>
			                	</a>
			                <?php }else{ ?>
			                	
			                	<img src= "<?php echo base_url(); ?>assets/approved.png" title="Terverifikasi">
			                	&nbsp;
			                	<a href="<?php echo base_url(); ?>lkpp/verifikasi/akta/0/<?php echo $id; ?>">
			                		<button type="button" class="btn btn-danger" name="submit" value="kirim">Batalkan Verifikasi
			                		</button>
			                	</a>
			                <?php } ?></td style="padding:5px 10px">
			            		
			            	</tr>
			            <?php } ?>

			            <?php if(($data->mekanisme_keuangan)!=" "){  ?>
			            	<tr>
			            		<td style="padding:5px 10px"><label>	Mekanisme Keuangan </label></td style="padding:5px 10px">
			                	
			                	<td style="padding:5px 10px"><a href="<?php echo base_url()."assets/dokumen_lpp/".str_replace(" ","_",$data->nama_lpp)."/".$data->mekanisme_keuangan; ?>"><?php echo $data->mekanisme_keuangan; ?></a></td>
			            		<td style="padding:5px 10px"><?php 
			                		if($data->status_mekanisme!=1){
				                ?>
				                
				                	<a href="<?php echo base_url(); ?>lkpp/verifikasi/mekanisme/1/<?php echo $id; ?>">
				                		<button type="button" class="btn btn-primary" name="submit" value="kirim">Verifikasi
				                		</button>
				                	</a>
				                <?php }else{ ?>
				                	
				                	<img src= "<?php echo base_url(); ?>assets/approved.png" title="Terverifikasi">
				                	&nbsp;
				                	<a href="<?php echo base_url(); ?>lkpp/verifikasi/mekanisme/0/<?php echo $id; ?>">
				                		<button type="button" class="btn btn-danger" name="submit" value="kirim">Batalkan Verifikasi
				                		</button>
				                	</a>
				            </div>
				            <?php } ?>
				            </td style="padding:5px 10px">
			            	</tr>
			            <?php } ?>

			            <?php if(($data->sop)!=" "){  ?>
			            	<tr>
			            		<td style="padding:5px 10px"><label>	Sistem Manajemen Mutu (SOP)  </label></td style="padding:5px 10px">
			            		<td style="padding:5px 10px"><a href="<?php echo base_url()."assets/dokumen_lpp/".str_replace(" ","_",$data->nama_lpp)."/".$data->sop; ?>"><?php echo $data->sop; ?></a></td style="padding:5px 10px">
			            		<td style="padding:5px 10px"><?php 
			                		if($data->status_sop!=1){
			                ?>
			                
			                	<a href="<?php echo base_url(); ?>lkpp/verifikasi/sop/1/<?php echo $id; ?>">
			                		<button type="button" class="btn btn-primary" name="submit" value="kirim">Verifikasi
			                		</button>
			                	</a>
			                <?php }else{ ?>
			                	
			                	<img src= "<?php echo base_url(); ?>assets/approved.png" title="Terverifikasi">
			                	&nbsp;
			                	<a href="<?php echo base_url(); ?>lkpp/verifikasi/sop/0/<?php echo $id; ?>">
			                		<button type="button" class="btn btn-danger" name="submit" value="kirim">Batalkan Verifikasi
			                		</button>
			                	</a>
			            </div>
			            <?php } ?>
			            </td style="padding:5px 10px">
			            		
			            	</tr>
			            <?php } ?>

			            <?php if(($data->surat_komitmen)!=" "){  ?>
			            	<tr>
			            		<td style="padding:5px 10px"><label>	Surat Pernyataan Komitmen  </label></td style="padding:5px 10px">
			            		<td style="padding:5px 10px"><a href="<?php echo base_url()."assets/dokumen_lpp/".str_replace(" ","_",$data->nama_lpp)."/".$data->surat_komitmen; ?>"><?php echo $data->surat_komitmen; ?></a></td style="padding:5px 10px">
			            		<td style="padding:5px 10px"><?php 
			                		if($data->status_komitmen!=1){
			                ?>
			                
			                	<a href="<?php echo base_url(); ?>lkpp/verifikasi/komitmen/1/<?php echo $id; ?>">
			                		<button type="button" class="btn btn-primary" name="submit" value="kirim">Verifikasi
			                		</button>
			                	</a>
			                <?php }else{ ?>
			                	
			                	<img src= "<?php echo base_url(); ?>assets/approved.png" title="Terverifikasi">
			                	&nbsp;
			                	<a href="<?php echo base_url(); ?>lkpp/verifikasi/komitmen/0/<?php echo $id; ?>">
			                		<button type="button" class="btn btn-danger" name="submit" value="kirim">Batalkan Verifikasi
			                		</button>
			                	</a>
			                <?php } ?></td style="padding:5px 10px">
			            		
			            	</tr>
			            <?php } ?>



			            <?php if(($data->tugas_pelaksana)!=" "){  ?>
			            	<tr>
			            		<td style="padding:5px 10px"><label>	Tugas dan Fungsi Pelaksana Ujian </label></td style="padding:5px 10px">
			            		<td style="padding:5px 10px"><a href="<?php echo base_url()."assets/dokumen_lpp/".str_replace(" ","_",$data->nama_lpp)."/".$data->tugas_pelaksana; ?>"><?php echo $data->tugas_pelaksana; ?></a></td style="padding:5px 10px">
			            		<td style="padding:5px 10px"><?php 
			                		if($data->status_tugas!=1){
			                ?>

			                	<a href="<?php echo base_url(); ?>lkpp/verifikasi/tugas/1/<?php echo $id; ?>">
			                		<button type="button" class="btn btn-primary" name="submit" value="kirim">Verifikasi
			                		</button>
			                	</a>
			                <?php }else{ ?>
			                
			                	<img src= "<?php echo base_url(); ?>assets/approved.png" title="Terverifikasi">
			                	&nbsp;
			                	<a href="<?php echo base_url(); ?>lkpp/verifikasi/tugas/0/<?php echo $id; ?>">
			                		<button type="button" class="btn btn-danger" name="submit" value="kirim">Batalkan Verifikasi
			                		</button>
			                	</a>
			                <?php } ?></td style="padding:5px 10px">
			            		
			            	</tr>
			            <?php } ?>


			            	
			            </table>

			            

		            
		            </div>

		             
		            <div class="form-group">
		                <label>Username*</label>
		                <input class="form-control" type="text" id="username" name="username" value="<?php echo $data->email; ?>" >
		            </div>
		            <div class="form-group">
		                <label>Password*</label>
		                <input type="checkbox" name="password_state" checked>Gunakan Password Sebelumnnya
		                <input class="form-control" type="password" id="password" name="password" value="<?php echo $data->password; ?>" >
		            </div>
		            <div class="form-group">
		                <label>No Akreditasi LPP*</label>
		                <input class="form-control" type="text" id="no_akreditasi_lpp" name="no_akreditasi_lpp" value="<?php echo $data->no_akreditasi_lpp; ?>" >
		            </div>
		            <div class="form-group">
		                <label>Hasil Akreditasi*</label>
		                <input class="form-control" type="text" id="hasil_akreditasi" name="hasil_akreditasi" value="<?php echo $data->hasil_akreditasi; ?>" >
		            </div>
		            <div class="form-group">
		                <label>Masa Berlaku*</label>
		                <input class="form-control" type="text" id="sampai_tanggal" name="masa_berlaku" value="<?php echo $data->masa_berlaku; ?>" >
		            </div>
		            <!-- <div class="form-group">
		                <label>Satker*</label>
		                <input class="form-control" type="text" id="satker" name="satker">
		                <input type="radio" name="satker" value="1">iya
		                <input type="radio" name="satker" value="0">bukan
		            </div> -->
		            <!-- <button type="submit" class="btn btn-primary" name="submit" value="kirim">Tambah LPP</button> -->
		            <!-- <button type="submit" class="btn btn-danger" name="submit" value="cancel">Tolak Request</button> -->
		           <!--  <button type="submit" class="btn btn-primary" name="submit" value="kirim">Update LPP</button> -->
               	<button type="submit" class="btn btn-primary" name="submit" value="kirim">Update Data LPP</button>
                </form>
		            <?php
		            	if($data->approve=="0"){
		            ?>
		            <a href="<?php echo base_url(); ?>lkpp/aktifkan_lpp/<?php echo $data->id_lpp; ?>"> 
		            <button type="submit" class="btn btn-primary" name="submit" value="kirim">Aktifkan LPP</button>
		            </a>
		            <?php } ?>

		         <form method="POST" action="<?php echo base_url()."lkpp/batalkan_lpp/".$id; ?>">
		         	<br>
		         	<textarea rows="10" cols="40"></textarea><br><br>
		         	<button type="submit" class="btn btn-danger" name="submit" value="kirim">Batalkan Pendaftaran LPP</button> 	
		         </form>
               	
              </div>
            </div>
          </div>
        </div><!-- /.row -->
        <?php endforeach; ?>