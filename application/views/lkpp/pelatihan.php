            <div id="page-wrapper">
            <!-- <a href="<?php echo base_url(); ?>lkpp/tambah_pelatihan"><i class="fa fa-dashboard"></i> <button type="submit" class="btn btn-primary" name="submit" value="kirim" id="edit">Tambah Pelatihan</button></a><br><br> -->
            <div class="row">
            <div class="col-lg-12">

            <?php if(isset($berlangsung)){ 

          
            ?>


            <form method="POST" action="<?php if($report=="yes"){ echo base_url()."lkpp/reportSemuaPelatihan"; }else { echo base_url()."lkpp/find_pelatihan"; } ?>">
            <div style="border:1px solid #DADADA;padding:8px;">
                <tr>
                  <td><input type="text" name="dari_tanggal" id="dari_tanggal" style="width:15%;float:left;margin:7px;" placeholder="Dari Tanggal" <?php if($dari_tanggal!=null){ echo 'value="'.$dari_tanggal.'"';  ?> disabled <?php } ?> ></td>
                </tr>
                <tr>
                  <td><input type="text" name="sampai_tanggal" id="sampai_tanggal" style="width:15%;float:left;margin:7px;" placeholder="Sampai Tanggal" <?php if($sampai_tanggal!=null){ echo 'value="'.$sampai_tanggal.'"';  ?> disabled <?php } ?> ></td>
                </tr>
                <tr>
                  <td>

                  <select name="lpp" style="width:15%;float:left;margin:7px;" <?php if($id_lpp!=null){ echo "disabled"; } ?>>
                  <option value="<?php if($id_lpp!=null){ echo $id_lpp; } ?>"><?php echo $id_lpp;  ?></option>
                  <?php foreach ($semua_lpp as $data_lpp): ?>
                    <option value="<?php echo $data_lpp->id_lpp; ?>"><?php echo $data_lpp->nama_lpp; ?></option>
                  <?php endforeach; ?>
                  </select>
                  </td>
                </tr>
                <tr>
                  <td>

                  <select name="pelatihan" style="width:15%;float:left;margin:7px;" <?php if($id_pelatihan!=null){ echo "disabled"; } ?>>
                  <option value="<?php if($id_pelatihan!=null){ echo $id_pelatihan; } ?>"> <?php echo $id_pelatihan; ?></option>
                  <?php foreach ($tema_pelatihan as $data_lpp): ?>
                    <option value="<?php echo $data_lpp->id_tema; ?>"><?php echo $data_lpp->judul_pelatihan; ?></option>
                  <?php endforeach; ?>
                  </select>
                  </td>
                </tr>
                <tr>
                  <td><input type="text" name="provinsi" style="width:15%;float:left;margin:7px;" placeholder="Provinsi" <?php if($provinsi!=null){ echo 'value="'.$provinsi.'"';  ?> disabled <?php } ?> ></td>
                </tr>
                <br><br><br><br>
                <?php if($report=="no"){ ?>
                <button type="submit" class="btn btn-primary" name="submit" value="kirim" id="edit">Cari</button>
                <?php } ?>
                <a href="<?php echo base_url(); ?>lkpp/pelatihan"> <button type="button" class="btn btn-danger" name="submit" value="kirim" id="edit">Reset</button></a>
                <?php if(isset($report)){ ?>
                <button type="submit" class="btn btn-success" name="submit" value="kirim" id="edit">Generate Report</button>
                <?php } ?>
            </div><br>
            </form>
            <?php } ?>

            <div class="panel panel-primary">
              <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-bar-chart-o"></i>Semua <?php if(isset($ditolak)){ ?> Pelatihan Ditolak <?php } else if(isset($arsip)){  ?> Arsip Pelatihan <?php }else{ ?> Pelatihan Berlangsung <?php } ?></h3>
              </div>
                <table class="table table-bordered table-hover table-striped tablesorter">
                <thead>
                  <tr>
                    <th>No <i class="fa fa-sort"></i></th>
                    <th>Jenis Pelatihan <i class="fa fa-sort"></i></th>
                    <th>LPP <i class="fa fa-sort"></i></th>
                    <th>Mulai Tanggal <i class="fa fa-sort"></i></th>
                    <th>Sampai Tanggal <i class="fa fa-sort"></i></th>
                    <th>Contact Person<i class="fa fa-sort"></i></th>
                    <?php if( (isset($berlangsung)) ){ ?>
                    <th>Status<i class="fa fa-sort"></i></th>
                    <?php } ?>
                    <th>Tgl dibuat<i class="fa fa-sort"></i></th>
                    <th class="header">Tgl <?php if(isset($ditolak)){ ?> ditolak <?php } else{  ?> disetujui <?php } ?><i class="fa fa-sort"></i></th>
                    <?php if( (isset($ditolak)) ){ ?>
                    <th>Surat Permohonan <i class="fa fa-sort"></i></th>
                    <?php } ?>
                    <th>action <i class="fa fa-sort"></i></th>
                  </tr>
                </thead>
                <tbody>
                <?php $no =1; foreach ($pelatihan as $data): ?>
                  <tr>
                    <td><?php echo $no; ?></td>
                    <td><?php echo $data->judul_pelatihan; ?></td>
                    <td><?php echo $data->nama_lpp; ?></td>
                    <td><?php echo $data->mulai_tanggal; ?></td>
                    <td><?php echo $data->sampai_tanggal; ?></td>
                    <td><?php echo $data->contact_nama." - ".$data->contact_hp; ?></td>
                    <?php if(isset($berlangsung)) { 
                      $row_narasumber= $this->model_pelatihan->countRowTable('pelatihan_narasumber','id_pelatihan',$data->id_pelatihan);
                      $row_peserta = $this->model_pelatihan->countRowTable('tbl_peserta','id_pelatihan',$data->id_pelatihan);
                      $row_eval_penyelenggara = $this->model_pelatihan->countRowTable('evaluasi_penyelenggara','id_pelatihan',$data->id_pelatihan);
                      $row_eval_narasumber = $this->model_pelatihan->countRowTable('evaluasi_narasumber','id_pelatihan',$data->id_pelatihan);
                    ?>
                    <td>
                      <?php  
                        echo "N "; if($row_narasumber!=0){ echo "<img src=\"".base_url()."assets/checklist.png\" width=\"14\">"; } echo "<br>";
                        echo "P "; if($row_peserta!=0){ echo "<img src=\"".base_url()."assets/checklist.png\" width=\"14\">"; } echo "<br>";
                        echo "EN "; if($row_eval_narasumber!=0){ echo "<img src=\"".base_url()."assets/checklist.png\" width=\"14\">"; } echo "<br>";
                        echo "EP "; if($row_eval_penyelenggara!=0){ echo "<img src=\"".base_url()."assets/checklist.png\" width=\"14\">"; } echo "<br>";
                      ?>
                    </td>
                    <?php } ?>
                    
                    <td><?php echo $data->tgl_dibuat; ?></td>
                    <td><?php echo $data->tgl_diapprove; ?></td>
                    <?php if( (isset($ditolak)) ){ ?>
                    <td><a href="<?php echo base_url(); ?>assets/upload/surat_permohonan/<?php echo $data->surat_permohonan; ?>"><?php echo substr($data->surat_permohonan,0,20); ?></a> </td>
                    <?php } ?>
                    <td>
                    <?php if(isset($ditolak)){ ?> 
                    alasan ditolak karena, <span style="color:red;"><?php echo $data->alasan_ditolak; ?></span>
                    <?php }else{ ?>
                    <a href="<?php echo base_url().$role."/detail_pelatihan/".$data->id_pelatihan; ?>"><button type="submit" class="btn btn-success" name="submit" value="kirim" id="delete">detail</button></a>
                    <a href="<?php echo base_url().$role."/delete_pelatihan/".$data->id_pelatihan; ?>"><button type="submit" class="btn btn-danger" name="submit" value="kirim" id="delete" onclick="return confirm('Anda Yakin Akan Menghapus Pelatihan ini ? semua yg berhubungan dengan pelatihan ini tidak akan dapat di akses kembali')">delete</button></a>
                    <?php } ?>
                    </td>
                  </tr>
                <?php $no++; endforeach; ?>
                </tbody>
              </table>
              <div id="pager" class="pager">
                <form>
                  <img src="<?php echo base_url()?>assets/table_sorter/addons/pager/icons/first.png" class="first"/>
                  <img src="<?php echo base_url()?>assets/table_sorter/addons/pager/icons/prev.png" class="prev"/>
                  <input type="text" class="pagedisplay"/>
                  <img src="<?php echo base_url()?>assets/table_sorter/addons/pager/icons/next.png" class="next"/>
                  <img src="<?php echo base_url()?>assets/table_sorter/addons/pager/icons/last.png" class="last"/>
                  <select class="pagesize">
                    <option selected="selected"  value="10">10</option>
                    <option value="20">20</option>
                    <option value="30">30</option>
                    <option  value="40">40</option>
                  </select>
                </form>
              </div>