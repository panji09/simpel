 <div id="page-wrapper">
   <div class="row">
   <div class="col-lg-12">
     <div class="panel panel-primary">
<div class="panel-heading">
  <h3 class="panel-title"><i class="fa fa-bar-chart-o"></i> Tambah Dokumen Baru</h3>
</div>
<?php 
$status = $this->session->flashdata('status');
if(isset($status)){ echo $status; } 
?>
<div class="panel-body">
 <?php echo form_open_multipart('lkpp/proses_tambah_dokumen');?>
  <div class="form-group">
		  <label>Judul Dokumen</label><!-- <input type="text" class="form-control" name="narasumber"> -->
    <input type="text" class="form-control" name="judul_dokumen">
		     </div>
  <div class="form-group">
      <label>Kategori Dokumen</label><!-- <input type="text" class="form-control" name="narasumber"> -->
    <input type="text" class="form-control" name="kategori_dokumen">
  </div>
  <div class="form-group">
    <label>File Dokumen</label><!-- <input type="text" class="form-control" name="narasumber"> -->
    <input type="file" name="userfile" size="20" />
  </div>
  <div class="form-group">
    <div id="files"></div>
  </div>
   <button type="submit" class="btn btn-primary" name="submit" value="kirim">Tambah Dokumen</button>
  <!-- <button type="submit" class="btn btn-danger" name="submit" value="cancel">Tolak Request</button> -->
  </form>
</div>
     </div>
     <table class="table table-hover table-striped tablesorter">
     <thead>
       <tr>
        <th class="header">id<i class="fa fa-sort"></i></th>
        <th class="header">judul Dokumen<i class="fa fa-sort"></i></th>
        <th class="header">kategori Dokumen <i class="fa fa-sort"></i></th>
        <th class="header">file Dokumen <i class="fa fa-sort"></i></th>
        <th class="header">action <i class="fa fa-sort"></i></th>
      </tr>
      </thead>
      <tbody>
      <?php $no = 1; foreach ($dataDokumen as $data): ?>
        <tr>
          <td><?php echo $no; ?></td>
          <td><?php echo $data->judul_dokumen; ?></td>
          <td><?php echo $data->kategori_dokumen; ?></td>
          <td><a target="_blank" href="<?php echo base_url(); ?>assets/upload/Dokumen/<?php echo $data->file_dokumen; ?>"> <?php echo $data->file_dokumen; ?></a></td>
          <td><a href="<?php echo base_url()."lkpp/delete_dokumen/$data->id_dokumen" ?>" onclick="return confirm('Anda Yakin Akan Menghapus Data ini')">delete</a> | 
          <a href="<?php echo base_url()."lkpp/edit_dokumen/$data->id_dokumen" ?>" >edit</a>
          </td>
      <?php $no++; endforeach; ?>
      </tbody>
      
    </table>
    <div id="pager" class="pager" style="margin-top:0px;">
                <form>
                  <img src="<?php echo base_url()?>assets/table_sorter/addons/pager/icons/first.png" class="first"/>
                  <img src="<?php echo base_url()?>assets/table_sorter/addons/pager/icons/prev.png" class="prev"/>
                  <input type="text" class="pagedisplay"/>
                  <img src="<?php echo base_url()?>assets/table_sorter/addons/pager/icons/next.png" class="next"/>
                  <img src="<?php echo base_url()?>assets/table_sorter/addons/pager/icons/last.png" class="last"/>
                  <select class="pagesize">
                    <option selected="selected"  value="10">10</option>
                    <option value="20">20</option>
                    <option value="30">30</option>
                    <option  value="40">40</option>
                  </select>
                </form>
              </div>
   </div>

 </div><!-- /.row -->

 