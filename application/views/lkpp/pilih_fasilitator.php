<br>
<?php foreach ($detail as $data): ?>
			<div class="col-lg-12">
            <div class="panel panel-primary">
              <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-bar-chart-o"></i><?php echo $data->judul_pelatihan; ?> </h3>
              </div>
              <div class="panel-body">
                <?php echo form_open(base_url().'lkpp/proses_pilih_fasilitator/'.$id_pelatihan); ?>
                	<div class="form-group">
                		<label>Jenis Pelatihan*</label>
		                <select class="form-control" name="tema_pelatihan">
		                  <option value=""><?php echo $data->judul_pelatihan; ?></option>
		                </select>
		            </div>
		            <div class="form-group">
                		<label>LPP*</label>
		                <select class="form-control" name="id_lpp">
		                  <option value=""><?php echo $data->nama_lpp; ?></option>
		                </select>
		            </div>
		            <div class="form-group">
		                <label>instansi*</label>
		                <input class="form-control" type="text" id="tempat_pelatihan" name="instansi" disabled="" value="<?php echo $data->instansi; ?>">
		            </div>
		            <div class="form-group">
		                <label>tempat pelatihan*</label>
		                <input class="form-control" type="text" id="tempat_pelatihan" name="tempat_pelatihan" disabled="" value="<?php echo $data->tempat_pelatihan; ?>">
		            </div>
		            <div class="form-group">
		                <label>contact person*</label>
		                <textarea class="form-control" rows="4" name="isi_pelatihan" disabled=""><?php echo "nama : ".$data->contact_nama." \n\nno handphone : ".$data->contact_hp; ?></textarea>
		             </div>
		            <div class="form-group">
		                <label>Dari tanggal*</label>
		                <input class="form-control" type="text" id="dari_tanggal" name="dari_tanggal" disabled="" value="<?php echo $data->mulai_tanggal; ?>" disabled="">
		            </div>
		            <div class="form-group">
		                <label>Sampai tanggal*</label>
		                <input class="form-control" type="text" id="sampai_tanggal" name="sampai_tanggal" value="<?php echo $data->sampai_tanggal; ?>" disabled="">
		            </div>
		            <div class="form-group">
		                <label>Pilih Fasilitator*</label>
		                 <select class="form-control" name="id_fasilitator">
			                <?php foreach ($fasilitator as $data): ?>
			                  <option value="<?php echo $data->id_fasilitator; ?>"><?php echo $data->nama_fasilitator; ?></option>
			                  <?php endforeach; ?>
			                </select>
		            </div>
		            <div class="form-group">
		            	<button type="submit" class="btn btn-primary" name="submit" value="kirim">Kirim</button>
		            </div>
		            
		             
		            

		            <!-- <button type="submit" class="btn btn-primary" name="submit" value="kirim">Kirim Request</button> -->
		            <!-- <button type="submit" class="btn btn-danger" name="submit" value="cancel">Tolak Request</button> -->
                </form>
              </div>
            </div>
          </div>
        </div><!-- /.row -->
  	<?php endforeach; ?>