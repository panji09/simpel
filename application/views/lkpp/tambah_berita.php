        <div id="page-wrapper">
          <div class="row">
          <div class="col-lg-12">
            <div class="panel panel-primary">
              <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-bar-chart-o"></i> Tambah Berita Baru</h3>
              </div>
              <?php 
              $status = $this->session->flashdata('status');
              if(isset($status)){ echo $status; } 
              ?>
              <div class="panel-body">
               <?php echo form_open_multipart('lkpp/proses_tambah_berita');?>
                <div class="form-group">
		                <label>Judul Berita</label><!-- <input type="text" class="form-control" name="narasumber"> -->
                  <input type="text" class="form-control" name="judul_berita">
		            </div>
                <div class="form-group">
                    <label>Tanggal Berita</label><!-- <input type="text" class="form-control" name="narasumber"> -->
                  <input type="text" class="form-control" name="tgl_berita" id="dari_tanggal">
                </div>
                <div class="form-group">
                    <label>Berita</label><!-- <input type="text" class="form-control" name="narasumber"> -->
                    <script type="text/javascript" src="<?php echo base_url(); ?>assets/ckeditor/ckeditor.js"></script>
                    <textarea class="ckeditor" name="isi_berita"></textarea>
                   <!--  <textarea class="form-control" name="isi_berita" rows="6"></textarea> -->
                </div>
                <div class="form-group">
                  <label>Photo Berita</label><!-- <input type="text" class="form-control" name="narasumber"> -->
                  <input type="file" name="userfile" size="20" />
                </div>
                <span style="color:red;">Header Berita ukuran 983px x 380px</span>
                <div class="form-group">
                    <label>Header</label><br>
                    <input type="radio" name="header" value="1">ya<br>
                    <input type="radio" name="header" value="0">tidak
                </div>
                <div class="form-group">
                  <div id="files"></div>
                </div>
                 <button type="submit" class="btn btn-primary" name="submit" value="kirim">Tambah Berita</button>
                <!-- <button type="submit" class="btn btn-danger" name="submit" value="cancel">Tolak Request</button> -->
                </form>
              </div>
            </div>
          </div>
        </div><!-- /.row -->