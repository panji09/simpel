 <div id="page-wrapper">
   <div class="row">
   <div class="col-lg-12">
     <div class="panel panel-primary">
<div class="panel-heading">
  <h3 class="panel-title"><i class="fa fa-bar-chart-o"></i> Tambah Dokumen Baru</h3>
</div>
<?php 
$status = $this->session->flashdata('status');
if(isset($status)){ echo $status; } 
?>
<div class="panel-body">
<?php $no = 1; foreach ($dataDokumen as $data): ?>
 <?php echo form_open_multipart('lkpp/proses_edit_dokumen');?>
  <div class="form-group">
		  <label>Judul Dokumen</label><!-- <input type="text" class="form-control" name="narasumber"> -->
    <input type="hidden" name="id_dokumen" value="<?php echo $data->id_dokumen; ?>">
    <input type="text" class="form-control" name="judul_dokumen" value="<?php echo $data->judul_dokumen; ?>">
		     </div>
  <div class="form-group">
      <label>Kategori Dokumen</label><!-- <input type="text" class="form-control" name="narasumber"> -->
    <input type="text" class="form-control" name="kategori_dokumen" value="<?php echo $data->kategori_dokumen; ?>">
  </div>
  <div class="form-group">
    <label>File Dokumen</label><!-- <input type="text" class="form-control" name="narasumber"> -->
    <input type="file" name="userfile" size="20" />
  </div>
  <div class="form-group">
      <a href="<?php echo base_url()."assets/upload/dokumen/$data->file_dokumen" ?>"><label><?php echo $data->file_dokumen; ?></label> </a><br>
      <label>Gunakan File Sebelumnya</label> <!-- <input type="text" class="form-control" name="narasumber"> -->
      <?php if(isset($data->file_dokumen)){?>
      <input type="checkbox" name="cek_photo" checked> <span style="color:red;">*apabila file diubah harap un-check tombol ini</span>
      <?php } ?>
  </div>
  <div class="form-group">
    <div id="files"></div>
  </div>
   <button type="submit" class="btn btn-primary" name="submit" value="kirim">Tambah Dokumen</button>
  <!-- <button type="submit" class="btn btn-danger" name="submit" value="cancel">Tolak Request</button> -->
  </form>
</div>
<?php endforeach; ?>
     </div>
   </div>

 </div><!-- /.row -->

 