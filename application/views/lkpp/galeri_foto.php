 <div id="page-wrapper">
   <div class="row">
   <div class="col-lg-12">
   <div class="panel panel-primary">
<div class="panel-heading">
  <h3 class="panel-title"><i class="fa fa-bar-chart-o"></i> Tambah Kategori Baru</h3>
</div>
<?php 
$status = $this->session->flashdata('status');
if(isset($status)){ echo $status; } 
?>
<div class="panel-body">
 <?php echo form_open_multipart('lkpp/proses_tambah_kategori');?>
  <div class="form-group">
      <label>Kategori Foto</label><!-- <input type="text" class="form-control" name="narasumber"> -->
    <input type="text" class="form-control" name="kategori_foto">
         </div>
  <div class="form-group">
    <div id="files"></div>
  </div>
   <button type="submit" class="btn btn-primary" name="submit" value="kirim">Tambah Kategori</button>
  <!-- <button type="submit" class="btn btn-danger" name="submit" value="cancel">Tolak Request</button> -->
  </form>
</div>
     </div>

     <div class="panel panel-primary">
<div class="panel-heading">
  <h3 class="panel-title"><i class="fa fa-bar-chart-o"></i> Tambah Foto Baru</h3>
</div>
<?php 
$status = $this->session->flashdata('status');
if(isset($status)){ echo $status; } 
?>
<div class="panel-body">
 <?php echo form_open_multipart('lkpp/proses_tambah_foto');?>
  <div class="form-group">
		  <label>Judul Foto</label><!-- <input type="text" class="form-control" name="narasumber"> -->
    <input type="text" class="form-control" name="judul_foto">
		     </div>
  <div class="form-group">
      <label>Kategori Foto</label><!-- <input type="text" class="form-control" name="narasumber"> -->
      <select class="form-control" name="deskripsi_foto">
      <?php foreach ($kategori as $data_kategori): ?>
        <option value="<?php echo $data_kategori->id_kategori; ?>"><?php echo $data_kategori->kategori; ?></option>
      <!-- <input type="text" class="form-control" name="deskripsi_foto"> -->
      <?php endforeach; ?>
      </select>
  </div>
  <div class="form-group">
    <label>File Foto</label><!-- <input type="text" class="form-control" name="narasumber"> -->
    <input type="file" name="userfile" size="20" />
  </div>
  <div class="form-group">
    <div id="files"></div>
  </div>
   <button type="submit" class="btn btn-primary" name="submit" value="kirim">Tambah Foto</button>
  <!-- <button type="submit" class="btn btn-danger" name="submit" value="cancel">Tolak Request</button> -->
  </form>
</div>
     </div>
     <table class="table table-hover table-striped tablesorter">
     <thead>
       <tr>
        <th class="header">id<i class="fa fa-sort"></i></th>
        <th class="header">judul Foto<i class="fa fa-sort"></i></th>
        <th class="header">Kategori Foto <i class="fa fa-sort"></i></th>
        <th class="header">File Foto <i class="fa fa-sort"></i></th>
        <th class="header">Action <i class="fa fa-sort"></i></th>
      </tr>
      </thead>
      <tbody>
      <?php $no = 1; foreach ($dataFoto as $data): ?>
        <tr>
          <td><?php echo $no; ?></td>
          <td><?php echo $data->judul_foto; ?></td>
          <td><?php echo $data->kategori; ?></td>
          <td><img src="<?php echo base_url()."assets/upload/galeri/".$data->file_foto; ?>" width="100"> </td>
          <td><a href="<?php echo base_url().$role."/delete_foto/".$data->id_foto; ?>"><button type="submit" class="btn btn-danger" name="submit" value="kirim" id="delete" onclick="return confirm('Anda Yakin Akan Menghapus Data ini')">delete</button></a></td>
      <?php $no++; endforeach; ?>
      </tbody>
      
    </table>
    <div id="pager" class="pager" style="margin-top:0px;">
                <form>
                  <img src="<?php echo base_url()?>assets/table_sorter/addons/pager/icons/first.png" class="first"/>
                  <img src="<?php echo base_url()?>assets/table_sorter/addons/pager/icons/prev.png" class="prev"/>
                  <input type="text" class="pagedisplay"/>
                  <img src="<?php echo base_url()?>assets/table_sorter/addons/pager/icons/next.png" class="next"/>
                  <img src="<?php echo base_url()?>assets/table_sorter/addons/pager/icons/last.png" class="last"/>
                  <select class="pagesize">
                    <option selected="selected"  value="10">10</option>
                    <option value="20">20</option>
                    <option value="30">30</option>
                    <option  value="40">40</option>
                  </select>
                </form>
              </div>
   </div>

 </div><!-- /.row -->

 