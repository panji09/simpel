        <div id="page-wrapper">
          <div class="row">
          <div class="col-lg-12">
            <div class="panel panel-primary">
              <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-bar-chart-o"></i> Tambah Tema Pelatihan</h3>
              </div>
              <?php 
              $status = $this->session->flashdata('status');
              if(isset($status)){ echo $status; } 
              ?>
              <?php foreach ($dataTemaPelatihan as $data): ?>
              <div class="panel-body">
               <?php echo form_open_multipart('lkpp/proses_update_tema_pelatihan');?>
                <div class="form-group">
		                <label>Judul Tema</label><!-- <input type="text" class="form-control" name="narasumber"> -->
                  <input type="text" class="form-control" name="judul_tema" value="<?php echo $data->judul_pelatihan; ?>">
		            </div>
                <div class="form-group">
                    <label>Keterangan</label><!-- <input type="text" class="form-control" name="narasumber"> -->
                    <textarea class="form-control" name="keterangan_tema" rows="6"><?php echo $data->keterangan; ?></textarea>
                </div>
                 <button type="submit" class="btn btn-primary" name="submit" value="kirim">Edit Tema Pelatihan</button>
                <!-- <button type="submit" class="btn btn-danger" name="submit" value="cancel">Tolak Request</button> -->
                </form>
              </div>
            <?php endforeach; ?>
            </div>
          </div>
        </div><!-- /.row -->