

      <div id="page-wrapper">

        <?php 
          $status = $this->session->flashdata('status');
          if(isset($status)){ echo $status; } 
        ?>
        <div class="row">

          
        <form method="POST" action="<?php echo base_url();?>fasilitator/detail_kegiatan_narasumber/<?php echo $id_narasumber; ?>">
            <div style="border:1px solid #DADADA;padding:8px;">
                
                <tr>
                  <td><span style="float:left;">Dari Tanggal : </span></td>
                  <td>
                  <input type="text" placeholder="dari tanggal" name="dari_tanggal" id="dari_tanggal" class="form-control" style="float:left;width:190px;margin:0px 4px 0px 0px;" value="<?php if(isset($dari_tanggal)){ echo $dari_tanggal; } ?>">
                  </td>
                </tr>
                <tr>
                  <td><span style="float:left;">Sampai Tanggal : </span> </td>
                  <td>
                  <input type="text" name="sampai_tanggal" id="sampai_tanggal" placeholder="sampai tanggal" class="form-control" style="float:left;width:190px;margin:0px 4px 0px 0px;" value="<?php if(isset($sampai_tanggal)){ echo $sampai_tanggal; } ?>">
                  </td>
                </tr>
                <button type="submit" class="btn btn-primary" name="submit" value="kirim" id="edit">Cari</button>
                
            </div><br>
            </form>

        </div>
        <div class="table table-striped">
                <?php foreach($detail_narasumber as $data) : ?>
                  <div>
                    <div style="font-size:14px;background:#F0F0F6;padding:4px;">Nama : <?php echo $data->nama_narasumber; ?></div>
                  </div>
                  <div>
                    <div style="font-size:14px;">Lokasi : <?php echo $data->lokasi; ?></div>
                  </div>
                  <div>
                    <div style="font-size:14px;background:#F0F0F6;padding:4px;">No Telp : <?php echo $data->no_telp; ?></div>
                  </div>
                  <div>
                    <div style="font-size:14px;">Email : <?php echo $data->email; ?></div>
                  </div>
                <?php endforeach; ?>
        </div>
        <div class="panel panel-primary">
              <div class="panel-heading">
              <?php //echo $conditionQuery; ?>
                <h3 class="panel-title"><i class="fa fa-bar-chart-o"></i>Detail Kegiatan Narasumber</h3>
              </div>
                <table class="table table-hover table-striped tablesorter">
                <thead>
                  <tr>
                    <th class="header">No<i class="fa fa-sort"></i></th>
                    <!-- <th class="header">Nama Narasumber <i class="fa fa-sort"></i></th>
                    <th class="header">No Telp Narasumber <i class="fa fa-sort"></i></th>
                    <th class="header">Lokasi<i class="fa fa-sort"></i></th>
                    <th class="header">Email<i class="fa fa-sort"></i></th> -->
                    <th class="header">Instansi<i class="fa fa-sort"></i></th>
                    <th class="header">Catatan<i class="fa fa-sort"></i></th>
                    <th class="header">Tanggal<i class="fa fa-sort"></i></th>
                    <th class="header">Total JP <i class="fa fa-sort"></i></th>
                  </tr>
                </thead>
                <tbody>
                <?php $no = 1; $total = 0; foreach ($detail_kegiatan_narasumber as $data): ?>
                  <tr>
                    <td><?php echo $no; ?></td>
                    <!-- <td><?php echo $data->nama_narasumber; ?></td>
                    <td><?php echo $data->no_telp; ?></td>
                    <td><?php echo $data->lokasi; ?></td>
                    <td><?php echo $data->email; ?></td>
 -->                    <td><?php echo $data->instansi; ?></td>
                    <td><?php echo $data->catatan_narasumber; ?></td>
                    <td><?php echo nama_hari($data->tanggal).','.tgl_indo($data->tanggal); ?></td>
                    <td><?php echo $data->jumlah_jp; ?></td>
                  </tr>
                <?php $no++; $total += $data->jumlah_jp; endforeach; ?>
                  <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>Total : </td>
                    <td><?php echo $total; ?></td>
                  </tr>
                </tbody>
              </table>
              <div id="pager" class="pager">
                <form>
                  <img src="<?php echo base_url()?>assets/table_sorter/addons/pager/icons/first.png" class="first"/>
                  <img src="<?php echo base_url()?>assets/table_sorter/addons/pager/icons/prev.png" class="prev"/>
                  <input type="text" class="pagedisplay"/>
                  <img src="<?php echo base_url()?>assets/table_sorter/addons/pager/icons/next.png" class="next"/>
                  <img src="<?php echo base_url()?>assets/table_sorter/addons/pager/icons/last.png" class="last"/>
                  <select class="pagesize">
                    <option selected="selected"  value="10">10</option>
                    <option value="20">20</option>
                    <option value="30">30</option>
                    <option  value="40">40</option>
                  </select>
                </form>
              </div>
      </div><!-- /#page-wrapper -->

    </div><!-- /#wrapper -->