      <br>
      
      <div class="col-lg-12">
            <div class="panel panel-primary">
              <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-bar-chart-o"></i>Evaluasi Narasumber</h3>
              </div>
              <table class="table table-hover table-striped tablesorter">
                <thead>
                  <tr>
                    <th class="header">Pelatihan <i class="fa fa-sort"></i></th>
                    <th class="header">Nama Narasumber <i class="fa fa-sort"></i></th>
                    <th class="header">Penguasaan materi<i class="fa fa-sort"></i></th>
                    <th class="header">Memberikan materi secara sistematis dan mudah dipahami <i class="fa fa-sort"></i></th>
                    <th class="header">Kritik dan Saran terhadap narasumber <i class="fa fa-sort"></i></th>
                    <th class="header">Memberikan contoh yang menarik dan mudah diingat <i class="fa fa-sort"></i></th>
                    <th class="header">Mendorong peserta ikut aktif dalam kelas  <i class="fa fa-sort"></i></th>
                    <th class="header">Hasil <i class="fa fa-sort"></i></th>
                    <th class="header">File Pendukung <i class="fa fa-sort"></i></th>
                    <!-- <th class="header">Action<i class="fa fa-sort"></i></th> -->
                  </tr>
                </thead>
                <tbody>
                <?php foreach ($dataEvaluasi as $data):
                ?>
                  <tr>
                    <td><?php echo $data->judul_pelatihan; ?></td>
                    <td><?php echo $data->nama_narasumber; ?></td>
                    <td><?php echo ($data->penguasaan_materi); ?></td>
                    <td><?php echo ($data->mudah_dipahami); ?></td>
                    <td><?php echo $data->kritik_saran; ?></td>
                    <td><?php echo ($data->mudah_diingat); ?></td>
                    <td><?php echo ($data->aktif_dalam_kelas); ?></td>
                    <td>
                    <h1 style="color:#1C63C7">
                    <?php echo number_format((float)($data->penguasaan_materi+$data->mudah_dipahami+$data->mudah_diingat+$data->aktif_dalam_kelas)/4, 2, '.', ''); ?>
                    </h1>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>assets/upload/evaluasi_narasumber/<?php echo $data->file_pendukung; ?>"><?php echo $data->file_pendukung; ?></a>
                    </td>
                    <td>
                    <!-- <button type="submit" class="btn btn-primary" name="submit" value="kirim" id="edit">edit</button> -->
                    <!-- <button type="submit" class="btn btn-danger" name="submit" value="kirim" id="delete">delete</button> -->
                    </td>
                  </tr>
                <?php endforeach; ?>
                </tbody>
              </table>