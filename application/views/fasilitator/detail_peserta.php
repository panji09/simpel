<div class="panel-body">
                <?php echo form_open(base_url().'fasilitator/proses_update_peserta/'.$id); 
                foreach ($detail_peserta as $data):
                ?>
              <input type="hidden" name="id_peserta" value="<?php echo $data->id_peserta; ?>">
                <div class="form-group">
                    <label>Nama lengkap*</label>
                    <input type="text" name="nama_lengkap" class="form-control" value="<?php echo $data->nama_lengkap; ?>">
                </div>
                <div class="form-group">
                    <label>Gelar Akademis</label>
                  <td><!-- <input type="text" class="form-control" value="<?php echo $data->nama_lengkap; ?>" name="narasumber"> -->
                  <input type="text" name="gelar_akademis" class="form-control" value="<?php echo $data->gelar_akademis; ?>">
                  </label>
                </div>
                <div class="form-group">
                    <label>Jenis Kelamin</label>
                  <td><!-- <input type="text" class="form-control" value="<?php echo $data->nama_lengkap; ?>" name="narasumber"> -->
                  <select name="jenis_kelamin" class="form-control" value="<?php echo $data->jenis_kelamin; ?>">
                    <option value="L">Laki Laki</option>
                    <option value="P">Perempuan</option>
                  </select>
                  </label>
                </div>
                <div class="form-group">
                    <label>NIP</label>
                  <td><!-- <input type="text" class="form-control" value="<?php echo $data->nama_lengkap; ?>" name="narasumber"> -->
                  <input type="text" class="form-control" value="<?php echo $data->NIP; ?>" name="nip">
                  </label>
                </div>
                <div class="form-group">
                    <label>NO KTP</label>
                  <td><!-- <input type="text" class="form-control" value="<?php echo $data->nama_lengkap; ?>" name="narasumber"> -->
                  <input type="text" class="form-control" value="<?php echo $data->no_ktp; ?>" name="no_ktp">
                  </label>
                </div>
                <div class="form-group">
                    <label>Tempat Lahir</label>
                  <td><!-- <input type="text" class="form-control" value="<?php echo $data->nama_lengkap; ?>" name="narasumber"> -->
                  <input type="text" class="form-control" value="<?php echo $data->tempat_lahir; ?>" name="tempat_lahir">
                  </label>
                </div>
                <div class="form-group">
                    <label>Tanggal Lahir</label>
                  <td><!-- <input type="text" class="form-control" value="<?php echo $data->nama_lengkap; ?>" name="narasumber"> -->
                  <input type="text" class="form-control" value="<?php echo $data->tgl_lahir  ; ?>" name="tanggal_lahir">
                  </label>
                </div>
                <div class="form-group">
                    <label>Pendidikan Terakhir</label>
                  <td><!-- <input type="text" class="form-control" value="<?php echo $data->nama_lengkap; ?>" name="narasumber"> -->
                  <input type="text" class="form-control" value="<?php echo $data->pendidikan_terakhir; ?>" name="pendidikan_terakhir">
                  </label>
                </div>
                <div class="form-group">
                    <label>Status Kepegawaian</label>
                  <td><!-- <input type="text" class="form-control" value="<?php echo $data->nama_lengkap; ?>" name="narasumber"> -->
                  <input type="text" class="form-control" value="<?php echo $data->status_kepegawaian; ?>" name="status_kepegawaian">
                  </label>
                </div>
                <div class="form-group">
                    <label>Instansi</label>
                  <td><!-- <input type="text" class="form-control" value="<?php echo $data->nama_lengkap; ?>" name="narasumber"> -->
                  <input type="text" class="form-control" value="<?php echo $data->instansi; ?>" name="instansi">
                  </label>
                </div>
                <div class="form-group">
                    <label>Lembaga</label>
                  <td><!-- <input type="text" class="form-control" value="<?php echo $data->nama_lengkap; ?>" name="narasumber"> -->
                  <input type="text" class="form-control" value="<?php echo $data->lembaga; ?>" name="lembaga">
                  </label>
                </div>
                <div class="form-group">
                    <label>Domisili Kabupaten</label>
                  <td><!-- <input type="text" class="form-control" value="<?php echo $data->nama_lengkap; ?>" name="narasumber"> -->
                  <input type="text" class="form-control" value="<?php echo $data->domisili_kabupaten; ?>" name="domisili_kabupaten">
                  </label>
                </div>
                <div class="form-group">
                    <label>Domisili Provinsi</label>
                  <td><!-- <input type="text" class="form-control" value="<?php echo $data->nama_lengkap; ?>" name="narasumber"> -->
                  <input type="text" class="form-control" value="<?php echo $data->domisili_provinsi; ?>" name="domisili_provinsi">
                  </label>
                </div>
                <div class="form-group">
                    <label>No Telepon</label>
                  <td><!-- <input type="text" class="form-control" value="<?php echo $data->nama_lengkap; ?>" name="narasumber"> -->
                  <input type="text" class="form-control" value="<?php echo $data->no_telepon; ?>" name="no_telepon">
                  </label>
                </div>
                <div class="form-group">
                    <label>Alamat Email</label>
                  <td><!-- <input type="text" class="form-control" value="<?php echo $data->nama_lengkap; ?>" name="narasumber"> -->
                  <input type="text" class="form-control" value="<?php echo $data->alamat_email; ?>" name="alamat_email">
                  </label>
                </div>
                <div class="form-group">
                    <label>Pengalaman di bidang pengadaan barang / jasa pemerintah</label>
                  <td><!-- <input type="text" class="form-control" value="<?php echo $data->nama_lengkap; ?>" name="narasumber"> -->
                  <input type="radio" name="pengalaman" value="yes">ya
                  <input type="radio" name="pengalaman" value="no">tidak
                  </label>
                </div>
                <div class="form-group">
                    <label>Pernah Mengikuti pelatihan barang / jasa </label>
                  <td><!-- <input type="text" class="form-control" value="<?php echo $data->pengalaman_kerja_bidang_pengadaan_barang_pemerintah; ?>" name="narasumber"> -->
                  <input type="radio" name="pernah" value="yes" >ya
                  <input type="radio" name="pernah" value="yes" >tidak
                  </label>
                </div>
                <div class="form-group">
                    <label>Berapa Kali</label>
                  <td><!-- <input type="text" class="form-control" value="<?php echo $data->pernah_mengikuti_pelatihan_barang_jasa; ?>" name="narasumber"> -->
                  <input type="text" class="form-control" value="<?php echo $data->berapa_kali; ?>" name="berapa_kali">
                  </label>
                </div>
                <div class="form-group">
                    <label>Alasan Mengikuti</label>
                  <td><!-- <input type="text" class="form-control" value="<?php echo $data->nama_lengkap; ?>" name="narasumber"> -->
                  <input type="text" class="form-control" value="<?php echo $data->alasan_mengikuti; ?>" name="alasan_mengikuti">
                  </label>
                </div>
                <div class="form-group">
                    <label>Pre Test</label>
                  <td><!-- <input type="text" class="form-control" value="<?php echo $data->nama_lengkap; ?>" name="narasumber"> -->
                  <input type="text" class="form-control" value="<?php echo $data->pre_test; ?>" name="pre_test">
                  </label>
                </div>

                <div class="form-group">
                    <label>Post Test</label>
                  <td><!-- <input type="text" class="form-control" value="<?php echo $data->nama_lengkap; ?>" name="narasumber"> -->
                  <input type="text" class="form-control" value="<?php echo $data->post_test; ?>" name="post_test">
                  </label>
                </div>

                <tr>
                  <input type="submit" value="Save" id="save">
                </div>
              </table>
              </form>
            <?php endforeach; ?>
            </div>