<html>
<head>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()."assets/css/report.css"; ?>">
</head>

<body>

<br>
<br>
<br>
	<div id="page-body" style="padding:5px 5px;font-size:14px;">

	<div id="page-header">
		<img src="http://localhost/simpel_new/assets/design/images/lkpp.jpg" width="200px">
	</div>
	
	<hr>
	<table style="width:100%;float:left;border:none;" border="0">
		<tr style="border:none;">
			<td style="border:none;">No </td>
			<td style="border:none;">:</td>
			<td style="border:none;"></td>
			<td style="border:none;"></td>
			<td style="border:none;"></td>
			<td style="text-align:right;border:none;"><?php echo tgl_indo(date('Y-m-d')); ?></td>
		</tr>
		<tr style="border:none;">
			<td style="border:none;">Lamp </td>
			<td style="border:none;">:</td>
			<td style="border:none;"></td>
		</tr>
		<tr style="border:none;">
			<td style="border:none;">Hal </td>
			<td style="border:none;">:</td>
			<td style="border:none;">Perencanaan Pelatihan</td>
		</tr>
	</table>
	
	<p style="width:80%;margin:10px 5px;">
		Kepada Yth. 
		Deputi Bidang Pengembangan dan Pembinaan Sumber Daya Manusia,
		u.p. Direktorat Pelatihan Kompetensi
		Gedung SME Tower Lt.7
		 Jl. Jend Gatot Subroto Kav. 94 Jakarta Selatan 12780

	</p>
	<?php foreach ($detail as $data_detail): ?>
		<p style="width:100%;margin:10px 0px;">
Dengan Hormat,<br>
		<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Sehubungan dengan rencana pelatihan keahlian pengadaan barang/jasa yang akan dilaksanakan pada tanggal <?php echo tgl_indo($data_detail->mulai_tanggal)." s/d ".tgl_indo($data_detail->sampai_tanggal); ?>  di <?php echo $data_detail->tempat_pelatihan; ?>  bersama ini kami sampaikan perencanaan pelatihan <?php echo $data_detail->judul_pelatihan; ?> sebanyak <?php echo $data_detail->total_jp; ?> JP sebagai berikut : </p>

		</p>
	<?php endforeach; ?>
	
	
		<div id="content_box">
		<p>1.	Daftar materi dan narasumber yang telah ditetapkan sebagai berikut :</p>
		<table id="table_border">
			<tr>
				<th>No</th>
				<th>Tanggal Pelatihan</th>
				<th>Materi</th>
				<th>Nama Narasumber</th>
				<th>Total Jam</th>
			</tr>
	<?php $no=1; foreach ($data_narasumber as $narasumber): ?>
			<tr>
				<td><?php echo $no; ?></td>
				<td><?php echo tgl_indo($narasumber->tgl_pelatihan); ?></td>
				<td><?php echo $narasumber->materi_pelatihan; ?></td>
				<td><?php echo $narasumber->nama_narasumber; ?></td>
				<td><?php echo $narasumber->total_jam; ?></td>
			</tr>
	<?php $no++; endforeach; ?>		
		</table>
		<p>Catatan : tabel di atas dapat disampaikan berupa lampiran </p>
		</div>

		<?php foreach ($detail as $data_detail): ?>
		<p>2.	Penanggung jawab/contact person  pelatihan :</p>
		<table style="margin:0px 20px;">
			<tr>
				<td>a.  Nama</td>
				<td>:</td>
				<td><?php echo $data_detail->contact_nama; ?></td>
			</tr>
			<tr>
				<td>b.	Jabatan</td>
				<td>:</td>
				<td><?php echo $data_detail->jabatan; ?></td>
			</tr>
			<tr>
				<td>c.	No Telp</td>
				<td>:</td>
				<td><?php echo $data_detail->contact_hp; ?></td>
			</tr>
			<tr>
				<td>d.	E-mail</td>
				<td>:</td>
				<td><?php echo $data_detail->kontak_email; ?></td>
			</tr>
		</table>
		<?php endforeach; ?>

		<p>3.	Hal hal yang perlu disampaikan :</p>
		<table style="margin:0px 20px;">
			<tr>
				<td>a.   Jumlah peserta pelatihan</td>
				<td>:</td>
				<td>.... Orang</td>
			</tr>
			<tr>
				<td>b.   Jumlah kelas</td>
				<td>:</td>
				<td>.... Orang</td>
			</tr>
			<tr>
				<td>c.   Jumlah panitia</td>
				<td>:</td>
				<td>.... Orang</td>
			</tr>
		</table>

		<br>
		<small>Demikian kami sampaikan, atas kerjasamanya kami sampaikan terima kasih. </small>

		<div align="right">
			Penanggung jawab  Pelatihan
		</div>
		<br>
		<br>
		<br>
		
		<div align="right">
			(..........................................)
		</div>

	</div>
	<!-- <div id="page-footer">
		<p>Footer text</p>
		<p>Page <span class="page-number"/> of <span class="page-count"/></p>
	</div>

	<div id="page-content">
		<p>Page 1.</p>
		
		<p style="page-break-after:always;"/>
		
		<p>Page 2.</p>
	</div> -->
</body>
</html>