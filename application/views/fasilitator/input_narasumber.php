  <style type="text/css">
  .entry-form{
    display: none;
  }
  </style>
  <script type="text/javascript">
  function alasan(alasan) {
   //alert(alasan);
   //alert(alasan);
   $(".entry-form").fadeIn("fast");
   document.getElementById("alasan_text").innerHTML=alasan;
  }

  $(document).ready(function(){
  
  /*$("#alasan").click(function(){
    alert("baaa!");
  });*/
  
  function ajax(action){
    $.ajax({
      type: "POST", 
      url: "<?php echo base_url(); ?>lkpp/proses_insert_narasumber", 
      data : $("#userinfo").serialize(),
      dataType: "json",
      success: function(response){
        if(response.result=="success"){
          $(".entry-form").fadeOut("fast",function(){
              location.reload();
          });
        }
      },
      error: function(xhr, status, error){
        //alert("Unexpected error! Try again."+res);
        console.log("Unexpected error! Try again. = "+xhr.responseText+" - "+status+" - "+error);
      }
    });
  }

    /*if(action =="save"){
      //data = $("#userinfo").serialize()+"&action="+action;
      myData = $("#userinfo").serialize();
    }else if(action == "delete"){
      myData = "action="+action+"&item_id="+id;
    }

    $.ajax({
      type: "POST", 
      url: "<?php echo base_url(); ?>lpp/proses_insert_peserta", 
      data : myData,
      dataType: "json",
      success: function(response){
        alert(response.msg);
        if(response.success == "1"){
          if(action == "save"){
            $(".entry-form").fadeOut("fast",function(){
              $(".table-list").append("<tr><td>"+response.fname+"</td><td>"+response.lname+"</td><td>"+response.email+"</td><td>"+response.phone+"</td><td><a href='#' id='"+response.row_id+"' class='del'>Delete</a></a></td></tr>"); 
              $(".table-list tr:last").effect("highlight", {
                color: '#4BADF5'
              }, 1000);
            }); 
          }else if(action == "delete"){
            var row_id = response.item_id;
            $("a[id='"+row_id+"']").closest("tr").effect("highlight", {
              color: '#4BADF5'
            }, 1000);
            $("a[id='"+row_id+"']").closest("tr").fadeOut();
          }
        }else{
          alert(response.msg);
        }
      },
      error: function(xhr, status, error){
        //alert("Unexpected error! Try again."+res);
        console.log("Unexpected error! Try again. = "+xhr+" - "+status+" - "+error);
      }
    });*/

});

</script>
  <?php foreach ($detail as $data_detail):
              $total_jp = $data_detail->total_jp;

              //echo $total_jp_row_approve." ".$total_jp;
              endforeach; ?>
              <div class="col-lg-12">
              <?php if($total_jp_row_approve<$total_jp){ ?>
              <a href="<?php echo base_url(); ?>fasilitator/request_narasumber/<?php echo $id; ?>"><button type="submit" class="btn btn-primary" name="submit" value="kirim">Tambah Narasumber</button></a>
              <?php } ?>

              <h3>Belum Diproses</h3>
              <hr>

              <table class="table table-hover table-striped tablesorter">
                <thead>
                  <tr>
                    <th class="header">No <i class="fa fa-sort"></i></th>
                    <th class="header">Tanggal Pelatihan <i class="fa fa-sort"></i></th>
                    <th class="header">Dari Jam <i class="fa fa-sort"></i></th>
                    <th class="header">Sampai Jam <i class="fa fa-sort"></i></th>
                    <th class="header">Nama Narasumber <i class="fa fa-sort"></i></th>
                    <th class="header">No Handphone <i class="fa fa-sort"></i></th>
                    <th class="header">Total Jam <i class="fa fa-sort"></i></th>
                    <th class="header">Materi Pelatihan <i class="fa fa-sort"></i></th>
                    <th class="header">Action <i class="fa fa-sort"></i></th>
                    <!-- <th class="header">Action<i class="fa fa-sort"></i></th> -->
                  </tr>
                </thead>
                <tbody>
                <?php $no = 1; foreach ($semua_narasumber_temporary as $data): 
                if($data->approve==1){
                  $approve = "approved";
                }else if($data->approve==2){
                  $approve = "cancel";
                }else{
                  $approve = "pending";
                }
                ?>
                  <tr>
                    <td><?php echo $no; ?></td>
                    <td><?php echo $data->tgl_pelatihan; ?></td>
                    <td><?php echo $data->dari_jam; ?></td>
                    <td><?php echo $data->sampai_jam; ?></td>
                    <td><?php echo $data->nama_narasumber; ?></td>
                    <td><?php echo $data->no_telp; ?></td>
                    <td><?php echo $data->total_jam; ?></td>
                    <td><?php echo $data->materi_pelatihan; ?></td>
                    <td>
                    <?php if($total_jp_row<$total_jp){ ?>
                    <a href="<?php echo base_url()."fasilitator/eksekusi_narasumber/".$data->id_pelatihan."/".$data->id; ?>"> <button type="submit" class="btn btn-primary" name="submit" value="kirim" id="edit">Proses</button></a>
                    <?php } ?>
                    <a href="<?php echo base_url()."fasilitator/delete_temporary_narasumber/".$data->id_pelatihan."/".$data->id; ?>"><button type="submit" class="btn btn-danger" name="submit" value="kirim" id="delete" onclick="return confirm('Anda Yakin Akan Menghapus Data ini')">Hapus</button></a>
                    </td>
                  </tr>
                <?php $no++ ; endforeach; ?>
                </tbody>
              </table>

              <br>
              <h3>Sudah Diproses</h3>
              <hr>

              <table class="table table-hover table-striped tablesorter">
                <thead>
                  <tr>
                    <th class="header">No <i class="fa fa-sort"></i></th>
                    <th class="header">Tanggal Pelatihan <i class="fa fa-sort"></i></th>
                    <th class="header">Dari Jam <i class="fa fa-sort"></i></th>
                    <th class="header">Sampai Jam <i class="fa fa-sort"></i></th>
                    <th class="header">Nama Narasumber <i class="fa fa-sort"></i></th>
                    <th class="header">No Handphone <i class="fa fa-sort"></i></th>
                    <th class="header">Total Jam <i class="fa fa-sort"></i></th>
                    <th class="header">Materi Pelatihan <i class="fa fa-sort"></i></th>
                    <th class="header">Approve <i class="fa fa-sort"></i></th>
                    <!-- <th class="header">Action<i class="fa fa-sort"></i></th> -->
                  </tr>
                </thead>
                <tbody>
                <?php $no = 1; foreach ($semua_narasumber as $data): 
                if($data->approve==1){
                  $approve = "approved";
                }else if($data->approve==2){
                  $approve = "cancel";
                }else{
                  $approve = "pending";
                }
                ?>
                  <tr>
                    <td><?php echo $no; ?></td>
                    <td><?php echo $data->tgl_pelatihan; ?></td>
                    <td><?php echo $data->dari_jam; ?></td>
                    <td><?php echo $data->sampai_jam; ?></td>
                    <td><?php echo $data->nama_narasumber; ?></td>
                    <td><?php echo $data->no_telp; ?></td>
                    <td><?php echo $data->total_jam; ?></td>
                    <td><?php echo $data->materi_pelatihan; ?></td>
                    <td><?php echo "<img src=\"".base_url()."assets/".$approve.".png\" title=\"".$approve."\">"; ?><br><?php echo $approve; 
                    if($approve=="cancel"){ 
                      echo "<br><a href=\"#\" onClick=\"javascript:alasan('".$data->alasan."');\">alasan ditolak</a>"; 
                      }?>
                    <br>
                    <?php if($approve=="approved"){ ?>
                    <a href="<?php echo base_url(); ?>fasilitator/batalkan_narasumber/<?php echo $data->id_pelatihan."/".$data->id_narasumber; ?>" onclick="return confirm('Anda Yakin Membatalkan Narasumber ini ?')"> <button type="submit" class="btn btn-danger" name="submit" value="kirim">Edit</button></a>
                    <?php } ?>
                    </td>
                  </tr>
                <?php $no++ ; endforeach; ?>
                </tbody>
              </table>

              <div class="entry-form">
              <form name="userinfo" id="userinfo"> 
              <table width="100%" border="0" cellpadding="4" cellspacing="0">
                <tr>
                  <td colspan="2" align="right"><a href="#" id="close">Close</a></td>
                </tr>
                
                <tr>
                  <td>
                      Alasan ditolak karena, 
                    <p id="alasan_text">
                      lorem ipsum dolor sit amet
                    </p>
                  </td>
                </tr>
              </table>
              </form>
            </div>
              