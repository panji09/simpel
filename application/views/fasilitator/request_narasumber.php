             
              <div id="page-wrapper">
                <div class="row">
                <div class="col-lg-12">
                  <div class="panel panel-primary">
                    <div class="panel-heading">
                      <h3 class="panel-title"><i class="fa fa-bar-chart-o"></i>Request Narasumber</h3>
                    </div>
                    <?php 
                    $status = $this->session->flashdata('status');
                    if(isset($status)){ echo $status; } 
                    ?>
                    <div class="panel-body">
                     <?php echo form_open_multipart('fasilitator/proses_insert_narasumber/'.$id);?>
                     <input type="hidden" name="id_pelatihan" value="<?php echo $id; ?>">
                     <?php foreach ($detail as $data): ?>
                     <div class="form-group">
                          <label>Pelatihan Pada Tanggal</label><br>
                          <span><?php echo tgl_indo($data->mulai_tanggal)." - ".tgl_indo($data->sampai_tanggal); ?></span>
                      </div>
                    <?php 

                    $total_jp = $data->total_jp - $total_jp_row_approve;
                   /* $sampai_tanggal = strtotime($data->sampai_tanggal);
                    $dari_tanggal = strtotime($data->mulai_tanggal);
                    $datediff = $sampai_tanggal - $dari_tanggal;
                    //echo $datediff;
                    $date = $datediff/(60*60*24);
                    $total_date = floor($date + 1);*/

                    //print_r($array_date);

                    endforeach; ?>
                      <div class="form-group">
                          <label>tanggal pelatihan</label>
                          <select class="form-control" name="tgl_pelatihan">
                          <?php
                          for($i=0;$i<count($array_date);$i++){ ?>
                              <option value="<?php echo $array_date[$i]; ?>"><?php echo tgl_indo($array_date[$i]); ?></option>
                            <?php  
                              }
                            ?>
                          </select>
                          <!-- <input type="text" class="form-control" name="tgl_pelatihan" id="dari_tanggal"> -->
                      </div>
                      <div class="form-group">
                          <label>Dari Jam</label>
                          <select name="dari_jam" class="form-control">
                          <option value="08.00">08.00</option>
                          <?php for($i=0;$i<24;$i++){ ?>
                            <option value="<?php echo $i; ?>"><?php if($i<=9){echo "0".$i.".00";}else{echo $i.".00";} ?></option>
                          <?php } ?>
                          </select>
                      </div>
                      <div class="form-group">
                          <label>Sampai Jam</label>
                          <select name="sampai_jam" class="form-control">
                          <option value="17.00">17.00</option>
                          <?php for($i=0;$i<24;$i++){ ?>
                            <option value="<?php echo $i; ?>"><?php if($i<=9){echo "0".$i.".00";}else{echo $i.".00";} ?></option>
                          <?php } ?>
                          </select>
                      </div>
                      <div class="form-group">
                          <label>Total JP</label>
                          <select name="total_jp" class="form-control">
                            <?php for($i=1;$i<=$total_jp;$i++){ ?>
                              <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                            <?php } ?>
                          </select>
                         <!--  <input type="text" name="total_jp" class="form-control" value="10"> -->
                      </div>
                      <div class="form-group">
                          <label>Narasumber</label>
                          <select name="id_narasumber" class="form-control">
                          <option> -- Pilih Narasumber --</option>
                          <?php foreach ($narasumber as $data): ?>
                            <option class="opt" value="<?php echo $data->id_narasumber; ?>"><?php echo $data->nama_narasumber; ?></option>
                          <?php endforeach; ?>
                          </select>
                      </div>
                      <div class="form-group">
                          <label>Materi Pelatihan</label>
                          <input type="text" name="materi_pelatihan" class="form-control">
                          <!-- <input type="file" name="userfile"> -->
                      </div>
                       <button type="submit" class="btn btn-primary" name="submit" value="kirim">Request</button>
                      <!-- <button type="submit" class="btn btn-danger" name="submit" value="cancel">Tolak Request</button> -->
                      </form>
                    </div>
                  </div>
                </div>
              </div><!-- /.row -->