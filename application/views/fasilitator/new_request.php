            <style type="text/css">
            #catatan_arsip{
              display: none;
            }
            </style>

            <script type="text/javascript">
            function muncul_form () {
                document.getElementById("catatan_arsip").style.display="block";
            }
                
            </script>
            <div id="page-wrapper">
            
            <div class="row">
            <div class="col-lg-12">
            <div class="panel panel-primary">
              <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-bar-chart-o"></i>Semua Pelathan</h3>
              </div>
                <table class="table table-bordered table-hover table-striped tablesorter">
                <thead>
                  <tr>
                    <th>No <i class="fa fa-sort"></i></th>
                    <th>Jenis Pelatihan <i class="fa fa-sort"></i></th>
                    <th>Mulai Tanggal <i class="fa fa-sort"></i></th>
                    <th>Sampai Tanggal <i class="fa fa-sort"></i></th>
                    <th>Tanggal Disetujui<i class="fa fa-sort"></i></th>
                    <th>Contact Person<i class="fa fa-sort"></i></th>
                    <th>action <i class="fa fa-sort"></i></th>
                  </tr>
                </thead>
                <tbody>
                <?php $no=1; foreach ($pelatihan as $data): ?>
                  <tr>
                    <td><?php echo $no; ?></td>
                    <td><?php echo $data->judul_pelatihan; ?></td>
                    <td><?php echo $data->mulai_tanggal; ?></td>
                    <td><?php echo $data->sampai_tanggal; ?></td>
                    <td><?php echo $data->tgl_diapprove; ?></td>
                    <td><?php echo $data->contact_nama." - ".$data->contact_hp; ?></td>
                    <td>
                    <a href="<?php echo base_url().$role."/detail_pelatihan/".$data->id_pelatihan; ?>"><button type="submit" class="btn btn-success" name="submit" value="kirim" id="delete">proses</button></a>


                    </td>
                  </tr>
                <?php $no++; endforeach; ?>
                </tbody>
              </table>
              <div id="pager" class="pager">
                <form>
                  <img src="<?php echo base_url()?>assets/table_sorter/addons/pager/icons/first.png" class="first"/>
                  <img src="<?php echo base_url()?>assets/table_sorter/addons/pager/icons/prev.png" class="prev"/>
                  <input type="text" class="pagedisplay"/>
                  <img src="<?php echo base_url()?>assets/table_sorter/addons/pager/icons/next.png" class="next"/>
                  <img src="<?php echo base_url()?>assets/table_sorter/addons/pager/icons/last.png" class="last"/>
                  <select class="pagesize">
                    <option selected="selected"  value="10">10</option>
                    <option value="20">20</option>
                    <option value="30">30</option>
                    <option  value="40">40</option>
                  </select>
                </form>
              </div>