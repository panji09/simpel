            <?php
		/*
		<th>Jul</th>
                                        <th>I</th>
                                        <th>II</th>
                                        <th>III</th>
                                        <th>IV</th>
                                        <th>V</th>
                                        <th>Agu</th>
                                        <th>I</th>
                                        <th>II</th>
                                        <th>III</th>
                                        <th>IV</th>
                                        <th>V</th>
                                        <th>Sep</th>
                                        <th>I</th>
                                        <th>II</th>
                                        <th>III</th>
                                        <th>IV</th>
                                        <th>V</th>
                                        <th>Okt</th>
                                        <th>I</th>
                                        <th>II</th>
                                        <th>III</th>
                                        <th>IV</th>
                                        <th>V</th>
                                        <th>Nov</th>
                                        <th>I</th>
                                        <th>II</th>
                                        <th>III</th>
                                        <th>IV</th>
                                        <th>V</th>
                                        <th>Des</th>
                                        <th>I</th>
                                        <th>II</th>
                                        <th>III</th>
                                        <th>IV</th>
                                        <th>V</th>
		*/
            ?>
            <div id="page-wrapper">
            
            <div class="row">
		<div class="col-lg-12">
		    <form action='<?=site_url('fasilitator/post_pelatihan_narasumber/'.$this->uri->segment(3))?>' method='post'>
			<div class="form-group">
			  <label for="exampleInputName2">Tahun</label>
			  <select class="form-control" name='tahun'>
			    <option value=''>#</option>
			    <?php for($tahun=2013; $tahun<=date('Y'); $tahun++):?>
			    <option value='<?=$tahun?>' <?=($this->session->flashdata('tahun')==$tahun ? 'selected' : '')?>><?=$tahun?></option>
			    <?php endfor;?>
			  </select>
			</div>
			<div class="form-group">
			  <label for="exampleInputEmail2">Periode</label>
			  <select class="form-control" name='periode'>
			    
			    <option value='1' <?=($this->session->flashdata('periode')==1 ? 'selected' : '')?>>januari-juni</option>
			    <option value='2' <?=($this->session->flashdata('periode')==2 ? 'selected' : '')?>>juli-desember</option>
			    
			  </select>
			</div>
			<button type="submit" class="btn btn-default">Proses</button>
		      </form>
		</div>
	    </div>
	    
	    
            <div class="row">
            <div class="col-lg-12">
            <div class="panel panel-primary">
              <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-bar-chart-o"></i>Data Narasumber</h3>
              </div>
                
              <div id="collapse2" class="body collapse in">
                            <?php
                                $notif=$this->session->flashdata('success');
                                if($notif):
                            ?>
                            <div class="alert <?=($notif['status'] ? 'alert-success' : 'alert-danger')?>"><?=$notif['msg']?></div>
                            <?php endif;?>
                            <?php if($this->session->flashdata('tahun')):?>
                            <table id="load-table" class="table responsive table-bordered table-condensed table-hover table-striped">
                                <thead>
				    <tr>              
					<th>&nbsp;</th>
					<th><input id="i-1" type="text" name="search_1" value="" class="form-control input-sm search_init" /></th>
					<th><input id="i-2" type="text" name="search_2" value="" class="form-control input-sm search_init" /></th>
					<th><input id="i-3" type="text" name="search_3" value="" class="form-control input-sm search_init" /></th>
					<th>&nbsp;</th>
                                        <th>&nbsp;</th>
                                        <th>&nbsp;</th>
                                        <th>&nbsp;</th>
                                        <th>&nbsp;</th>
                                        <th>&nbsp;</th>
                                        <th>&nbsp;</th>
                                        <th>&nbsp;</th>
                                        <th>&nbsp;</th>
                                        <th>&nbsp;</th>
                                        <th>&nbsp;</th>
                                        <th>&nbsp;</th>
                                        <th>&nbsp;</th>
                                        <th>&nbsp;</th>
                                        <th>&nbsp;</th>
                                        <th>&nbsp;</th>
                                        <th>&nbsp;</th>
                                        <th>&nbsp;</th>
                                        <th>&nbsp;</th>
                                        <th>&nbsp;</th>
                                        <th>&nbsp;</th>
                                        <th>&nbsp;</th>
                                        <th>&nbsp;</th>
                                        <th>&nbsp;</th>
                                        <th>&nbsp;</th>
                                        <th>&nbsp;</th>
                                        <th>&nbsp;</th>
                                        <th>&nbsp;</th>
                                        <th>&nbsp;</th>
                                        <th>&nbsp;</th>
                                        <th>&nbsp;</th>
                                        <th>&nbsp;</th>
                                        <th>&nbsp;</th>
                                        <th>&nbsp;</th>
                                        <th>&nbsp;</th>
                                        <th>&nbsp;</th>
					<th>&nbsp;</th>
				    </tr>
                                    <tr>
                                        <th>*</th>
                                        <th>ID TOT</th>
                                        <th>Nama</th>
                                        <th>Instansi</th>
                                        <?php if($this->session->flashdata('periode')==1):?>
                                        <th>Jan</th>
                                        <th>I</th>
                                        <th>II</th>
                                        <th>III</th>
                                        <th>IV</th>
                                        <th>V</th>
                                        <th>Feb</th>
                                        <th>I</th>
                                        <th>II</th>
                                        <th>III</th>
                                        <th>IV</th>
                                        <th>V</th>
                                        <th>Mar</th>
                                        <th>I</th>
                                        <th>II</th>
                                        <th>III</th>
                                        <th>IV</th>
                                        <th>V</th>
                                        <th>Apr</th>
                                        <th>I</th>
                                        <th>II</th>
                                        <th>III</th>
                                        <th>IV</th>
                                        <th>V</th>
                                        <th>Mei</th>
                                        <th>I</th>
                                        <th>II</th>
                                        <th>III</th>
                                        <th>IV</th>
                                        <th>V</th>
                                        <th>Jun</th>
                                        <th>I</th>
                                        <th>II</th>
                                        <th>III</th>
                                        <th>IV</th>
                                        <th>V</th>
                                        <?php endif;?>
                                        
                                        <?php if($this->session->flashdata('periode')==2):?>
                                        <th>Jul</th>
                                        <th>I</th>
                                        <th>II</th>
                                        <th>III</th>
                                        <th>IV</th>
                                        <th>V</th>
                                        <th>Agu</th>
                                        <th>I</th>
                                        <th>II</th>
                                        <th>III</th>
                                        <th>IV</th>
                                        <th>V</th>
                                        <th>Sep</th>
                                        <th>I</th>
                                        <th>II</th>
                                        <th>III</th>
                                        <th>IV</th>
                                        <th>V</th>
                                        <th>Okt</th>
                                        <th>I</th>
                                        <th>II</th>
                                        <th>III</th>
                                        <th>IV</th>
                                        <th>V</th>
                                        <th>Nov</th>
                                        <th>I</th>
                                        <th>II</th>
                                        <th>III</th>
                                        <th>IV</th>
                                        <th>V</th>
                                        <th>Des</th>
                                        <th>I</th>
                                        <th>II</th>
                                        <th>III</th>
                                        <th>IV</th>
                                        <th>V</th>
                                        <?php endif;?>
                                        <th>Total 1 Thn</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>    
                            <?php endif;?>
                        </div>