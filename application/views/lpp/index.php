<script>
     var time = new Date().getTime();
     $(document.body).bind("mousemove keypress", function(e) {
         time = new Date().getTime();
     });

     function refresh() {
         if(new Date().getTime() - time >= 60000) 
             window.location.reload(true);
         else 
             setTimeout(refresh, 1000);
     }

     setTimeout(refresh, 1000);
</script>
            <div id="page-wrapper">


        <div class="row">

          <!-- <div class="col-lg-4">
            <div class="panel panel-info">
              <div class="panel-heading">
                <div class="row">
                  <div class="col-xs-6">
                    <i class="fa fa-tasks fa-5x"></i>
                  </div>
                  <div class="col-xs-6 text-right">
                    <p class="announcement-heading"><?php echo $jumlah_request; ?></p>
                    <p class="announcement-text">Total Request</p>
                  </div>
                </div>
              </div>
              <a href="#">
                <div class="panel-footer announcement-bottom">
                  <div class="row">
                    <div class="col-xs-6">
                      <a href="<?php echo base_url(); ?>satker/allrequest/1">View All</a>
                    </div>
                    <div class="col-xs-6 text-right">
                      <i class="fa fa-arrow-circle-right"></i>
                    </div>
                  </div>
                </div>
              </a>
            </div>
          </div> -->

          <div class="col-lg-4">
            <div class="panel panel-info">
              <div class="panel-heading">
                <div class="row">
                  <div class="col-xs-6">
                    <i class="fa fa-tasks fa-5x"></i>
                  </div>
                  <div class="col-xs-6 text-right">
                    <p class="announcement-heading"><?php echo $jumlah_pelatihan; ?></p>
                    <p class="announcement-text">Total Pelatihan</p>
                  </div>
                </div>
              </div>
              <a href="#">
                <div class="panel-footer announcement-bottom">
                  <div class="row">
                    <div class="col-xs-6">
                      <a href="<?php echo base_url(); ?>lpp//semua_pelatihan">View All</a>
                    </div>
                    <div class="col-xs-6 text-right">
                      <i class="fa fa-arrow-circle-right"></i>
                    </div>
                  </div>
                </div>
              </a>
            </div>
          </div>

          <!-- <div class="col-lg-4">
            <div class="panel panel-info">
              <div class="panel-heading">
                <div class="row">
                  <div class="col-xs-6">
                    <i class="fa fa-tasks fa-5x"></i>
                  </div>
                  <div class="col-xs-6 text-right">
                    <p class="announcement-heading"><?php echo $jumlah_narasumber; ?></p>
                    <p class="announcement-text">Total Narasumber</p>
                  </div>
                </div>
              </div>
              <a href="#">
                <div class="panel-footer announcement-bottom">
                  <div class="row">
                    <div class="col-xs-6">
                    </div>
                    <div class="col-xs-6 text-right">
                      <i class="fa fa-arrow-circle-right"></i>
                    </div>
                  </div>
                </div>
              </a>
            </div>
          </div> -->
          </div>
              <div class="panel panel-primary">
              <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-bar-chart-o"></i>Notifikasi Narasumber</h3>
              </div>
              <table class="table table-hover table-striped tablesorter">
                <thead>
                  <tr>
                    <th class="header">Narasumber <i class="fa fa-sort"></i></th>
                    <th class="header">Pelatihan<i class="fa fa-sort"></i></th>
                    <th class="header">Tanggal<i class="fa fa-sort"></i></th>
                    <th class="header">Dari Jam <i class="fa fa-sort"></i></th>
                    <th class="header">Sampai Jam <i class="fa fa-sort"></i></th>
                    <th class="header">Status <i class="fa fa-sort"></i></th>
                    <th class="header">Action<i class="fa fa-sort"></i></th>
                  </tr>
                </thead>
                <tbody>
                <?php foreach ($notifikasi_narasumber as $data): 
                if($data->approve=="1"){
                  $status = "<span style='color:green;'>approved</span>";
                }else{
                  $status = "<span style='color:red;'>ditolak</span>";
                }
                ?>
                  <tr>
                    <td><?php echo $data->nama_narasumber; ?></td>
                    <td><?php echo $data->judul_pelatihan; ?></td>
                    <td><?php echo tgl_indo($data->tgl_pelatihan); ?></td>
                    <td><?php echo $data->dari_jam; ?></td>
                    <td><?php echo $data->sampai_jam; ?></td>
                    <td><?php echo $status; ?></td>
                    <td>
                    <a href="<?php echo base_url().$role."/setAktif/".$data->id."/".$data->id_pelatihan; ?>"><button type="submit" class="btn btn-danger" name="submit" value="kirim" id="delete">detail</button></a>
                    </td>
                  </tr>
                <?php endforeach; ?>
                </tbody>
              </table>
