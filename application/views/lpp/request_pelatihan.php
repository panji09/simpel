<div id="page-wrapper">
<div class="row">
          <div class="col-lg-12">
            <div class="panel panel-primary">
              <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-bar-chart-o"></i> Request Pelatihan Baru</h3>
              </div>
              <?php 
              $status = $this->session->flashdata('status');
              if(isset($status)){ echo $status; } 
              ?>
              <div class="panel-body">
                <?php echo form_open(base_url().'lpp/proses_request_pelatihan'); ?>
                	<div class="form-group">
                		<label>Tema Pelatihan*</label>
		                <select class="form-control" name="tema_pelatihan">
		                <?php foreach ($tema_pelatihan as $data) : ?>
		                  <option value="<?php echo $data->id_tema; ?>"><?php echo $data->judul_pelatihan; ?></option>
		                <?php endforeach; ?>
		                </select>
		            </div>
		            <div class="form-group">
		                <label>Dari tanggal*</label>
		                <input class="form-control" type="text" id="dari_tanggal" name="dari_tanggal" value="">
		            </div>
		            <div class="form-group">
		                <label>Sampai tanggal*</label>
		                <input class="form-control" type="text" id="sampai_tanggal" name="sampai_tanggal" value="">
		            </div>
		            <div class="form-group">
		                <label>isi pelatihan*</label>
		                <textarea class="form-control" rows="4" name="isi_pelatihan"></textarea>
		             </div>
		             <div class="form-group">
		                <label>tempat pelatihan*</label>
		                <input class="form-control" type="text" id="tempat_pelatihan" name="tempat_pelatihan">
		            </div>

		            <button type="submit" class="btn btn-primary" name="submit" value="kirim">Kirim Request</button>
		            <!-- <button type="submit" class="btn btn-danger" name="submit" value="cancel">Tolak Request</button> -->
                </form>
              </div>
            </div>
          </div>
        </div><!-- /.row -->