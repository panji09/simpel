      <br>
      
      <div class="col-lg-12">
      <a href="<?php echo base_url(); ?>lpp/evaluasi_narasumber_hasil/<?php echo $id_pelatihan; ?>">
        <button type="submit" class="btn btn-primary" name="submit" value="kirim" id="delete">Lihat Hasil Evaluasi</button> 
      </a><br><br>
            <div class="panel panel-primary">
              <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-bar-chart-o"></i>Evaluasi Narasumber</h3>
              </div>
              <table class="table table-hover table-striped tablesorter">
                <thead>
                  <tr>
                    <th class="header">Tanggal Pelatihan <i class="fa fa-sort"></i></th>
                    <th class="header">Dari Jam <i class="fa fa-sort"></i></th>
                    <th class="header">Sampai Jam <i class="fa fa-sort"></i></th>
                    <th class="header">Nama Narasumber <i class="fa fa-sort"></i></th>
                    <th class="header">Total Jam <i class="fa fa-sort"></i></th>
                    <th class="header">Evaluasi <i class="fa fa-sort"></i></th>
                    <!-- <th class="header">Action<i class="fa fa-sort"></i></th> -->
                  </tr>
                </thead>
                <tbody>
                <?php foreach ($semua_narasumber as $data): 
                if($data->approve==1){
                  $approve = "approved";
                }else if($data->approve==2){
                  $approve = "cancel";
                }else{
                  $approve = "pending";
                }
                ?>
                  <tr>
                    <td><?php echo $data->tgl_pelatihan; ?></td>
                    <td><?php echo $data->dari_jam; ?></td>
                    <td><?php echo $data->sampai_jam; ?></td>
                    <td><?php echo $data->nama_narasumber; ?></td>
                    <td><?php echo $data->total_jam; ?></td>
                    <td>
                      <a href="<?php echo base_url(); ?>lpp/evaluasi_narasumber_input/<?php echo $id_pelatihan."/".$data->id_narasumber; ?>">
                        <button type="submit" class="btn btn-primary" name="submit" value="kirim" id="delete">Evaluasi</button> 
                      </a>
                    </td>
                    <td>
                    <!-- <button type="submit" class="btn btn-primary" name="submit" value="kirim" id="edit">edit</button> -->
                    <!-- <button type="submit" class="btn btn-danger" name="submit" value="kirim" id="delete">delete</button> -->
                    </td>
                  </tr>
                <?php endforeach; ?>
                </tbody>
              </table>