      <br>
      <div class="col-lg-12">
            <div class="panel panel-primary">
              <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-bar-chart-o"></i>Evaluasi Penyelenggara</h3>
              </div>
              <div class="panel-body">
                <?php foreach ($dataEvaluasi as $data):

                function cek_value($value)
                {
                    if($value==1){
                        return " (Kurang)";
                    }else if($value==2){
                        return " (Cukup)";
                    }else if($value==3){
                        return " (Baik)";
                    }else if($value==4){
                        return " (Sangat Baik)";
                    }
                }
                ?>
                <div class="form-group">
                    <label>LPP :</label>
                    <p><?php echo $nama_lpp; ?></p>
                </div>
                <div class="form-group">
                    <?php foreach ($detail as $datas): ?>
                    <label>Pelatihan : <?php echo $datas->judul_pelatihan; ?></label>
                    <p><?php echo $datas->judul_pelatihan; ?></p>
                    <?php endforeach; ?>
                </div>
                
                <div class="form-group">
                    <label>kritik dan saran</label>
                    <p>
                        <?php echo $data->kritik_saran; ?>
                    </p>
                 </div><br>
                <div class="form-group" id="instansi">
                    <label>Kenyamanan ruang/tempat pelatihan : </label>
                    <p><?php echo ($data->kenyamanan_ruang); ?></p>
                </div> <br>
                <div class="form-group" id="instansi">
                    <label>Perlengkapan pelatihan :</label>
                    <p><?php echo ($data->perlengkapan); ?></p>
                </div> <br>
                <div class="form-group" id="instansi">
                    <label>Ketersediaan materi pelatihan :</label>
                    <p><?php echo ($data->materi_pelatihan); ?></p>
                </div>
                <hr>
                <div class="form-group" id="instansi">
                    <label>Hasil Evaluasi :</label>
                    
                    <h1 style="color:#1C63C7">
                    <?php echo number_format((float)($data->kenyamanan_ruang+$data->perlengkapan+$data->materi_pelatihan)/3, 2, '.', ''); ?>
                    </h1>
                </div>
                <div class="form-group" id="instansi">
                    <label>File Pendukung :</label>
                    
                    <h1 style="color:#1C63C7">
                    <a href="<?php echo base_url(); ?>assets/upload/evaluasi_penyelenggara/<?php echo $data->file_pendukung; ?>"><?php echo $data->file_pendukung; ?></a>
                    </h1>
                </div>
                <br>
                <?php endforeach; ?>
              </div>
            </div>
          </div>
        </div><!-- /.row -->
    