      <SCRIPT language=Javascript>
      <!--
      function digits(obj, e, allowDecimal, allowNegative)
      {
       var key;
       var isCtrl = false;
       var keychar;
       var reg;
        
       if(window.event) {
        key = e.keyCode;
        isCtrl = window.event.ctrlKey
       }
       else if(e.which) {
        key = e.which;
        isCtrl = e.ctrlKey;
       }
       
       if (isNaN(key)) return true;
       
       keychar = String.fromCharCode(key);
       
       // check for backspace or delete, or if Ctrl was pressed
       if (key == 8 || isCtrl)
       {
        return true;
       }

       reg = /\d/;
       var isFirstN = allowNegative ? keychar == '-' && obj.value.indexOf('-') == -1 : false;
       var isFirstD = allowDecimal ? keychar == '.' && obj.value.indexOf('.') == -1 : false;
       
       return isFirstN || isFirstD || reg.test(keychar);
      }
      //-->
   </SCRIPT>
      <br>
      <div class="col-lg-12">
            <div class="panel panel-primary">
              <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-bar-chart-o"></i>Evaluasi Penyelenggara</h3>
              </div>
              <div class="panel-body">
                <?php echo form_open_multipart(base_url().'lpp/proses_evaluasi_narasumber'); ?>
                <div class="form-group">
                    <label>Narasumber*</label>
                    <select class="form-control" name="id_narasumber" id="id_narasumber">
                      <option value="<?php echo $id_narasumber; ?> "><?php echo $nama_narasumber; ?> </option>
                    </select>
                </div>
                <div class="form-group">
                    <label>Pelatihan*</label>
                    <select class="form-control" name="id_pelatihan" id="id_pelatihan">
                    <?php foreach ($detail as $data): ?>
                      <option value="<?php echo $data->id_pelatihan; ?> "><?php echo $data->judul_pelatihan; ?> </option>
                    <?php endforeach; ?>
                    </select>
                </div>
                <div class="form-group" id="instansi">
                    <label>Nilai Penguasaan Materi*</label><br>
                    <input type="text" class="form-control" name="materi" onkeypress="return digits(this, event, true, true)" maxlength="4" style="width:60px;">
                    <!-- <input type="radio" name="materi" value="1">Kurang
                    <input type="radio" name="materi" value="2">Cukup
                    <input type="radio" name="materi" value="3">Baik
                    <input type="radio" name="materi" value="4">Sangat Baik -->
                </div> <br>
                <div class="form-group" id="instansi">
                    <label>Nilai Memberikan materi secara sistematis dan mudah dipahami*</label><br>
                    <input type="text" class="form-control" name="sistematis" onkeypress="return digits(this, event, true, true)" maxlength="4" style="width:60px;">
                    <!-- <input type="radio" name="sistematis" value="1">Kurang
                    <input type="radio" name="sistematis" value="2">Cukup
                    <input type="radio" name="sistematis" value="3">Baik
                    <input type="radio" name="sistematis" value="4">Sangat Baik -->
                </div> <br>
                <div class="form-group" id="instansi">
                    <label>Nilai Memberikan contoh yang menarik dan mudah diingat*</label><br>
                    <input type="text" class="form-control" name="contoh" onkeypress="return digits(this, event, true, true)" maxlength="4" style="width:60px;">
                    <!-- <input type="radio" name="contoh" value="1">Kurang
                    <input type="radio" name="contoh" value="2">Cukup
                    <input type="radio" name="contoh" value="3">Baik
                    <input type="radio" name="contoh" value="4">Sangat Baik -->
                </div> <br>
                <div class="form-group" id="instansi">
                    <label>Nilai Mendorong peserta ikut aktif dalam kelas *</label><br>
                    <input type="text" class="form-control" name="aktif" onkeypress="return digits(this, event, true, true)" maxlength="4" style="width:60px;">
                    <!-- <input type="radio" name="aktif" value="1">Kurang
                    <input type="radio" name="aktif" value="2">Cukup
                    <input type="radio" name="aktif" value="3">Baik
                    <input type="radio" name="aktif" value="4">Sangat Baik -->
                </div>  <br>
                <div class="form-group">
                    <label>kritik dan saran*</label>
                    <textarea class="form-control" rows="7" name="kritik"></textarea>
                 </div><br>
                <div class="form-group" id="file">
                    <input type="file" name="userfile">
                </div>

                <button type="submit" class="btn btn-primary" name="submit" value="approve">Kirim</button>
                <br>
                </form>
              </div>
            </div>
          </div>
        </div><!-- /.row -->
    