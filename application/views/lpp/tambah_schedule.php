			<SCRIPT language=Javascript>
		      <!--
		      function digits(obj, e, allowDecimal, allowNegative)
		      {
		       var key;
		       var isCtrl = false;
		       var keychar;
		       var reg;
		        
		       if(window.event) {
		        key = e.keyCode;
		        isCtrl = window.event.ctrlKey
		       }
		       else if(e.which) {
		        key = e.which;
		        isCtrl = e.ctrlKey;
		       }
		       
		       if (isNaN(key)) return true;
		       
		       keychar = String.fromCharCode(key);
		       
		       // check for backspace or delete, or if Ctrl was pressed
		       if (key == 8 || isCtrl)
		       {
		        return true;
		       }

		       reg = /\d/;
		       var isFirstN = allowNegative ? keychar == '-' && obj.value.indexOf('-') == -1 : false;
		       var isFirstD = allowDecimal ? keychar == '.' && obj.value.indexOf('.') == -1 : false;
		       
		       return isFirstN || isFirstD || reg.test(keychar);
		      }
		      //-->
		   </SCRIPT>
		   <br>
			<div class="col-lg-12">
            <div class="panel panel-primary">
              <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-bar-chart-o"></i>Perencanaan Pelatihan Baru</h3>
              </div>
              <div class="panel-body">
              <div class="alert alert-danger" style="font-size:18px;">Perencanaan Pelatihan harus disampaikan paling lambat 3 (tiga) minggu sebelum pelaksanaan pelatihan</div>
                <?php echo form_open_multipart(base_url().'lpp/proses_request_schedule'); ?>
		            
		            <input type="hidden" name="id_lpp" value="<?php echo $id_user; ?> ">
		                <input type="hidden" name="instansi" value="<?php echo $nama_lpp; ?> ">
		            <div class="form-group" id="instansi">
                		<label>Jumlah Peserta*</label>
		                <input type="text" name="jumlah_peserta" class="form-control" onkeypress="return digits(this, event, false, false)" maxlength="2">
		                <!-- <select  name="jumlah_peserta" class="form-control" style="width:7%;">
		                <?php for ($i=1; $i <=50 ; $i++) { ?>
		                	<option value="<?php echo $i; ?>"><?php echo $i; ?></option>
		                <?php } ?>

		                </select> -->
		                <span style="color:red">jumlah peserta maksimal 50 perkelas</span>
		            </div> 
		             <div class="form-group">
		                <label>total jam pelajaran (JP)*</label>
		                <input class="form-control" type="text" id="total_jp" name="total_jp" value="">
		            </div>
		             <div class="form-group">
		                <label>Kota*</label>
		                <input class="form-control" type="text" id="kota" name="kota" value="">
		            </div>
		             <div class="form-group">
		                <label>Provinsi*</label>
		                <input class="form-control" type="text" id="provinsi" name="provinsi" value="">
		            </div>
		             <div class="form-group">
		                <label>Jumlah Panitia*</label>
		                <input class="form-control" type="text" id="jumlah_panitia" name="jumlah_panitia" value="">
		            </div>
		             <div class="form-group">
		                <label>Jumlah Kelas*</label>
		                <input class="form-control" type="text" id="jumlah_kelas" name="jumlah_kelas" value="">
		            </div>
		             <div class="form-group">
		                <label>Jabatan*</label>
		                <input class="form-control" type="text" id="jabatan" name="jabatan" value="">
		            </div>
		             <div class="form-group">
		                <label>Email*</label>
		                <input class="form-control" type="text" id="email" name="email" value="">
		            </div>
		             <div class="form-group">
		                <label>tempat pelatihan*</label>
		                <input class="form-control" type="text" id="tempat_pelatihan" name="tempat_pelatihan" value="">
		            </div>
		            <!-- <div class="form-group">
		                <label>contact person*</label>
		                <textarea class="form-control" rows="4" name="isi_pelatihan"></textarea>
		             </div> -->
		             <div class="form-group">
		                <label>contact person*</label>
		                <input class="form-control" type="text" id="tempat_pelatihan" name="contact_nama" value="">
		             </div>
		             <div class="form-group">
		                <label>no handphone*</label>
		                <input class="form-control" type="text" id="tempat_pelatihan" name="contact_hp" value="">
		             </div>
                	<div class="form-group">
                		<label>Jenis Pelatihan*</label>
		                <select class="form-control" name="tema_pelatihan">
		                <?php foreach ($tema_pelatihan as $data): ?>
		                  <option value="<?php echo $data->id_tema; ?>"><?php echo $data->judul_pelatihan; ?></option>
		                  <?php endforeach; ?>
		                </select>
		            </div>
		            <div class="form-group">
		                <label>Dari tanggal*</label>
		                <input class="form-control" type="text" name="dari_tanggal" id="dari_tanggal" value="">
		            </div>
		            <div class="form-group">
		                <label>Sampai tanggal*</label>
		                <input class="form-control" type="text" name="sampai_tanggal" id="sampai_tanggal"  value="">
		            </div>
		             <div class="form-group">
		                <label>Surat Permohonan Pelatihan*</label>
		                <input class="form-control" type="file" id="tempat_pelatihan" name="userfile" value="">
		                <span style="font-size:11px;font-style:italic;color:red;">Surat Permohonan yang diupload dalam bentuk PDF dan telah di tanda tangani oleh Pimpinan yang bersangkutan</span>
		            </div>

		            <button type="submit" class="btn btn-primary" name="submit" value="approve">Simpan</button>
                </form>
              </div>
            </div>
          </div>
        </div><!-- /.row -->
  	