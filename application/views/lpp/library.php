 <div id="page-wrapper">
   <div class="row">
   <div class="col-lg-12">
     <div class="panel panel-primary">
<div class="panel-heading">
  <h3 class="panel-title"><i class="fa fa-bar-chart-o"></i> Materi Untuk Di Download</h3>
</div>
<?php 
$status = $this->session->flashdata('status');
if(isset($status)){ echo $status; } 
?>
<div class="panel-body">
 <table class="table table-hover table-striped tablesorter">
     <thead>
       <tr>
        <th class="header">id<i class="fa fa-sort"></i></th>
        <th class="header">judul materi<i class="fa fa-sort"></i></th>
        <th class="header">kategori materi <i class="fa fa-sort"></i></th>
        <th class="header">file materi <i class="fa fa-sort"></i></th>
      </tr>
      </thead>
      <tbody>
      <?php foreach ($dataMateri as $data): ?>
        <tr>
          <td><?php echo $data->id_library; ?></td>
          <td><?php echo $data->judul_materi; ?></td>
          <td><?php echo $data->kategori_materi; ?></td>
          <td><a target="_blank" href="<?php echo base_url(); ?>assets/upload/materi/<?php echo $data->file_materi; ?>"> <?php echo $data->file_materi; ?></a></td>
          <td></td>
      <?php endforeach; ?>
      </tbody>
      
    </table>
    <div id="pager" class="pager" style="margin-top:0px;">
                <form>
                  <img src="<?php echo base_url()?>assets/table_sorter/addons/pager/icons/first.png" class="first"/>
                  <img src="<?php echo base_url()?>assets/table_sorter/addons/pager/icons/prev.png" class="prev"/>
                  <input type="text" class="pagedisplay"/>
                  <img src="<?php echo base_url()?>assets/table_sorter/addons/pager/icons/next.png" class="next"/>
                  <img src="<?php echo base_url()?>assets/table_sorter/addons/pager/icons/last.png" class="last"/>
                  <select class="pagesize">
                    <option selected="selected"  value="10">10</option>
                    <option value="20">20</option>
                    <option value="30">30</option>
                    <option  value="40">40</option>
                  </select>
                </form>
              </div>
</div>
     </div>
     
   </div>

 </div><!-- /.row -->

 