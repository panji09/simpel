<br>
			<?php 
              $status = $this->session->flashdata('status');
              if(isset($status)){ echo $status; } 

              if($row_fasilitator!=1){
            ?>
			<div class="col-lg-12">
            <div class="panel panel-primary">
              <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-bar-chart-o"></i>Pelatihan Baru</h3>
              </div>
              <div class="panel-body">
                <?php echo form_open(base_url().'lpp/proses_input_fasilitator'); ?>
                <input type="hidden" name="id_pelatihan" value="<?php echo $id; ?>">
                	<div class="form-group">
                		<label>Fasilitator*</label>
		                <select class="form-control" name="id_fasilitator">
		                <?php foreach ($fasilitator as $data): ?>
		                  <option value="<?php echo $data->id_fasilitator; ?>"><?php echo $data->nama_fasilitator; ?></option>
		                  <?php endforeach; ?>
		                </select>
		            </div>

		            <button type="submit" class="btn btn-primary" name="submit" value="approve">Kirim</button>
                </form>
              </div>
            </div>
          </div>
        </div><!-- /.row -->
        <?php
        } else {
        
        ?>

        <div class="col-lg-12">
        <a href="<?php echo base_url(); ?>lpp/lihat_narasumber/<?php echo $id; ?>"> 
        <button type="submit" class="btn btn-primary" name="submit" value="approve">Lihat Narasumber</button>
        </a>
        <br><br>
            <div class="panel panel-primary">
              <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-bar-chart-o"></i>Fasilitator</h3>
              </div>
              <div class="panel-body">
                <div class="form-group">
                <?php foreach ($data_fasilitator as $data):?>
                	<ul>
                		<li><label><?php echo $data->nama_fasilitator; ?></label></li>
                	</ul>
		                
		        <?php endforeach; ?>
		            </div>
              </div>
            </div>
          </div>
        </div><!-- /.row -->

        <?php
        
        }
        ?>