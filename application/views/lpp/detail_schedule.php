<br>
<?php foreach ($detail as $data): ?>
			<div class="col-lg-12">
            <div class="panel panel-primary">
              <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-bar-chart-o"></i><?php echo $data->judul_pelatihan; ?> </h3>
              </div>
              <div class="panel-body">
                <?php echo form_open(base_url().'lpp/proses_request_pelatihan'); ?>
                	<div class="form-group">
                		<label>Tema Pelatihan*</label>
		                <select class="form-control" name="tema_pelatihan">
		                  <option value=""><?php echo $data->judul_pelatihan; ?></option>
		                </select>
		            </div>
		            <div class="form-group">
                		<label>LPP*</label>
		                <select class="form-control" name="id_lpp">
		                  <option value=""><?php echo $data->nama_lpp; ?></option>
		                </select>
		            </div>
		            <div class="form-group">
		                <label>instansi*</label>
		                <input class="form-control" type="text" id="tempat_pelatihan" name="instansi" disabled="" value="<?php echo $data->instansi; ?>">
		            </div>
		            <div class="form-group">
		                <label>tempat pelatihan*</label>
		                <input class="form-control" type="text" id="tempat_pelatihan" name="tempat_pelatihan" disabled="" value="<?php echo $data->tempat_pelatihan; ?>">
		            </div>
		            <div class="form-group">
		                <label>contact person*</label>
		                <textarea class="form-control" rows="4" name="isi_pelatihan" disabled=""><?php echo $data->isi_pelatihan; ?></textarea>
		             </div>
		            <div class="form-group">
		                <label>Dari tanggal*</label>
		                <input class="form-control" type="text" id="dari_tanggal" name="dari_tanggal" disabled="" value="<?php echo $data->mulai_tanggal; ?>" disabled="">
		            </div>
		            <div class="form-group">
		                <label>Sampai tanggal*</label>
		                <input class="form-control" type="text" id="sampai_tanggal" name="sampai_tanggal" value="<?php echo $data->sampai_tanggal; ?>" disabled="">
		            </div>
		            
		             
		            <?php
		            if( isset($stat) ){
		            ?>
		            <div class="form-group">
		                <label>alasan ditolak*</label>
		                <textarea class="form-control" rows="4" name="alasan_ditolak" disabled=""><?php echo $data->alasan_ditolak; ?></textarea>
		            </div>
		            <?php
		        	}
		        	?>

		            <!-- <button type="submit" class="btn btn-primary" name="submit" value="kirim">Kirim Request</button> -->
		            <!-- <button type="submit" class="btn btn-danger" name="submit" value="cancel">Tolak Request</button> -->
                </form>
              </div>
            </div>
          </div>
        </div><!-- /.row -->
  	<?php endforeach; ?>