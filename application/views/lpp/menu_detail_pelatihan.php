<style type="text/css">
    a{
        padding: 8px 4px;
    }
    a:hover{
        color: #000;padding: 8px 4px;
    }
    .active{
        color: #000;padding: 8px 4px;
    }
</style>
<?php
    $uri = $this->uri->segment(2);

?>
<div id="page-wrapper">
<div class="row">
<ol class="breadcrumb">
    <li ><a style="color:#9BCF70;text-shadow:1px 1px #000;" href="<?php echo base_url().$role."/detail_pelatihan/".$id; ?>" class = "<?php if($uri=="detail_pelatihan"){ echo "active"; } ?>">Detail Pelatihan</a></li>
    <?php if($akreditasi=="A"){ ?>
    <li><a href="<?php echo base_url().$role."/input_narasumber/".$id; ?>" class = "<?php if($uri=="input_narasumber"){ echo "active"; } ?>" <?php if($row_narasumber!=0){ echo "style=\"color:#9BCF70;text-shadow:1px 1px #000;\" ";} ?>>Narasumber</a></li>
    <?php }else{ ?>
    <li><a href="<?php echo base_url().$role."/lihat_narasumber/".$id; ?>" class = "<?php if($uri=="lihat_narasumber"){ echo "active"; } ?>" <?php if($row_narasumber!=0){ echo "style=\"color:#9BCF70;text-shadow:1px 1px #000;\" ";} ?>>Narasumber</a></li>
    <?php } ?>
    <li><a href="<?php echo base_url().$role."/input_peserta/".$id; ?>" class = "<?php if($uri=="input_peserta"){ echo "active"; } ?>" <?php if($row_peserta!=0){ echo "style=\"color:#9BCF70;text-shadow:1px 1px #000;\" ";} ?>>Peserta</a></li>
    <!-- <li>Narasumber</li> -->
    
    <li><a href="<?php echo base_url().$role."/evaluasi_penyelenggara/".$id; ?>" class = "<?php if($uri=="evaluasi_penyelenggara"){ echo "active"; } ?>"<?php if($row_eval_penyelenggara!=0){ echo "style=\"color:#9BCF70;text-shadow:1px 1px #000;\" ";} ?>> Evaluasi Penyelenggaraan</a></li>
    <li><a href="<?php echo base_url().$role."/evaluasi_narasumber/".$id; ?>" class = "<?php if($uri=="evaluasi_narasumber"){ echo "active"; } ?>" <?php if($row_eval_narasumber!=0){ echo "style=\"color:#9BCF70;text-shadow:1px 1px #000;\" ";} ?>>Evaluasi Narasumber</a></li><!-- 
    <li><a href="<?php echo base_url().$role."/evaluasi_peserta/".$id; ?>">Evaluasi Peserta</a></li> -->
    <!-- <li><a href="<?php echo base_url().$role."/report/".$id; ?>" class = "<?php if($uri=="report"){ echo "active"; } ?>">Report</a></li> -->
</ol>