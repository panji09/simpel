<!-- 			<br>
			<div class="col-lg-12">
            <div class="panel panel-primary">
              <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-bar-chart-o"></i>Pelatihan Baru</h3>
              </div>
              <div class="panel-body">
                <?php echo form_open(base_url().'lpp/proses_request_pelatihan'); ?>
		            <div class="form-group">
                		<label>LPP*</label>
		                <select class="form-control" name="id_lpp" id="id_lpp">
		                	<option value="<?php echo $id_user; ?> "><?php echo $nama_lpp; ?> </option>
		                </select>
		            </div>
		            <div class="form-group" id="instansi">
                		<label>Instansi*</label>
		                <input type="text" name="instansi" class="form-control">
		            </div> 
		             <div class="form-group">
		                <label>tempat pelatihan*</label>
		                <input class="form-control" type="text" id="tempat_pelatihan" name="tempat_pelatihan" value="">
		            </div>
		            <div class="form-group">
		                <label>contact person*</label>
		                <textarea class="form-control" rows="4" name="isi_pelatihan"></textarea>
		             </div>
                	<div class="form-group">
                		<label>Tema Pelatihan*</label>
		                <select class="form-control" name="tema_pelatihan">
		                <?php foreach ($tema_pelatihan as $data): ?>
		                  <option value="<?php echo $data->id_tema; ?>"><?php echo $data->judul_pelatihan; ?></option>
		                  <?php endforeach; ?>
		                </select>
		            </div>
		            <div class="form-group">
		                <label>Dari tanggal*</label>
		                <input class="form-control" type="text" name="dari_tanggal" id="dari_tanggal" value="">
		            </div>
		            <div class="form-group">
		                <label>Sampai tanggal*</label>
		                <input class="form-control" type="text" name="sampai_tanggal" id="sampai_tanggal"  value="">
		            </div>

		            <button type="submit" class="btn btn-primary" name="submit" value="approve">Kirim</button>
                </form>
              </div>
            </div>
          </div>
        </div><!-- /.row -->

                    <div id="page-wrapper">
           
            <div class="row">
            <div class="col-lg-12">
            <div class="panel panel-primary">
              <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-bar-chart-o"></i>Semua Perencanaan Pelatihan</h3>
              </div>
                      <table class="table table-hover table-striped tablesorter">
                <thead>
                  <tr>
                    <th class="header">No<i class="fa fa-sort"></i></th>
                    <th class="header">Jenis Pelatihan <i class="fa fa-sort"></i></th>
                    <th class="header">Contact Person <i class="fa fa-sort"></i></th>
                    <th class="header">No Handphone <i class="fa fa-sort"></i></th>
                    <th class="header">Tempat Pelatihan <i class="fa fa-sort"></i></th>
                    <th class="header">Surat Permohonan <i class="fa fa-sort"></i></th>
                    <th class="header">Mulai Tanggal <i class="fa fa-sort"></i></th>
                    <th class="header">Sampai Tanggal <i class="fa fa-sort"></i></th>
                    <th class="header">Total JP <i class="fa fa-sort"></i></th>
                    <th class="header">Tgl dibuat <i class="fa fa-sort"></i></th>
                   
                    <th class="header">Action<i class="fa fa-sort"></i></th>
                    
                      
                    
                  </tr>
                </thead>
                <tbody>
                <?php $no = 1; foreach ($schedule_pelatihan as $data): 
                
                ?>
                  <tr>
                    <td><?php echo $no; ?></td>
                    <td><?php echo $data->judul_pelatihan; ?></td>
                    <td><?php echo $data->contact_nama; ?></td>
                    <td><?php echo $data->contact_hp; ?></td>
                    <td><?php echo $data->tempat_pelatihan; ?></td>
                    <td><a href="<?php echo base_url(); ?>assets/upload/surat_permohonan/<?php echo $data->surat_permohonan; ?>"> <?php echo substr($data->surat_permohonan,0,20); ?></a></td>
                    <td><?php echo $data->mulai_tanggal; ?></td>
                    <td><?php echo $data->sampai_tanggal; ?></td>
                    <td><?php echo $data->total_jp." JP";; ?></td>
                    <td><?php echo $data->tgl_dibuat; ?></td>
                    <td>
	                    <?php echo form_open(base_url().'lpp/proses_request_pelatihan_schedule'); ?>
	                    <input type="hidden" name="id_pelatihan" value="<?php echo $data->id_pelatihan; ?>">
	                    	<button type="submit" class="btn btn-default" name="submit" value="kirim" id="delete">Kirim</button>
	                    </form>
                    </td>
                    
                  </tr>
                <?php $no++; endforeach; ?>
                </tbody>
              </table>
              <br>
              <div id="pager" class="pager" style="margin-top:0px;">
                <form>
                  <img src="<?php echo base_url()?>assets/table_sorter/addons/pager/icons/first.png" class="first"/>
                  <img src="<?php echo base_url()?>assets/table_sorter/addons/pager/icons/prev.png" class="prev"/>
                  <input type="text" class="pagedisplay"/>
                  <img src="<?php echo base_url()?>assets/table_sorter/addons/pager/icons/next.png" class="next"/>
                  <img src="<?php echo base_url()?>assets/table_sorter/addons/pager/icons/last.png" class="last"/>
                  <select class="pagesize">
                    <option selected="selected"  value="10">10</option>
                    <option value="20">20</option>
                    <option value="30">30</option>
                    <option  value="40">40</option>
                  </select>
                </form>
              </div>

  	 		<!-- 	<br>
			<div class="col-lg-12">
            <div class="panel panel-primary">
              <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-bar-chart-o"></i>Pelatihan Baru</h3>
              </div>
              <div class="panel-body">
              
                <?php echo form_open(base_url().'lpp/proses_request_pelatihan_schedule'); ?>
		            <div class="form-group">
                		<label>Perencanaan Pelatihan*</label>
		                <select class="form-control" name="id_pelatihan" id="id_pelatihan">
		                <?php $no=1; foreach ($schedule_pelatihan as $data): ?>
		                	<option value="<?php echo $data->id_pelatihan; ?> "><?php echo $no.". ".$data->judul_pelatihan." ".tgl_indo($data->mulai_tanggal)." s/d ".tgl_indo($data->sampai_tanggal)." di ".$data->tempat_pelatihan; ?></option>
		                <?php $no++; endforeach; ?>
		                </select>
		            </div>
		            

		            <button type="submit" class="btn btn-primary" name="submit" value="approve">Kirim</button>
		        
                </form>
              </div>
            </div>
          </div>
        </div>
  	  -->