            <div id="page-wrapper">
            <?php if($menu=="schedule"){ 
                $detail = "detail_schedule";
            ?>
            <a href="<?php echo base_url(); ?>lpp/tambah_schedule"><i class="fa fa-dashboard"></i> <button type="submit" class="btn btn-primary" name="submit" value="kirim" id="edit">Tambah Perencanaan Pelatihan</button></a><br><br>
            <?php }else{
              $detail = "detail_pelatihan";
            } ?>
            <div class="row">
            <div class="col-lg-12">
            <div class="panel panel-primary">
              <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-bar-chart-o"></i>Semua Perencanaan Pelatihan</h3>
              </div>
                      <table class="table table-hover table-striped tablesorter">
                <thead>
                  <tr>
                    <th class="header">No<i class="fa fa-sort"></i></th>
                    <th class="header">Jenis Pelatihan <i class="fa fa-sort"></i></th>
                    <th class="header">Contact Person <i class="fa fa-sort"></i></th>
                    <th class="header">No Handphone <i class="fa fa-sort"></i></th>
                    <th class="header">Tempat Pelatihan <i class="fa fa-sort"></i></th>
                    <th class="header">Surat Permohonan <i class="fa fa-sort"></i></th>
                    <th class="header">Mulai Tanggal <i class="fa fa-sort"></i></th>
                    <th class="header">Sampai Tanggal <i class="fa fa-sort"></i></th>
                    <th class="header">Total JP <i class="fa fa-sort"></i></th>
                    <th class="header">Tgl dibuat <i class="fa fa-sort"></i></th>
                    <th class="header">Tgl <?php if(isset($ditolak)){ ?> ditolak <?php } else{  ?> disetujui <?php } ?><i class="fa fa-sort"></i></th>
                    <?php if($menu=="schedule"){ ?>
                        <th class="header">
                        Status
                        </th>
                      <?php } ?>
                    <th class="header">Action<i class="fa fa-sort"></i></th>
                    
                      
                    
                  </tr>
                </thead>
                <tbody>
                <?php $no = 1; foreach ($semua_pelatihan as $data): 
                
                ?>
                  <tr>
                    <td><?php echo $no; ?></td>
                    <td><?php echo $data->judul_pelatihan; ?></td>
                    <td><?php echo $data->contact_nama; ?></td>
                    <td><?php echo $data->contact_hp; ?></td>
                    <td><?php echo $data->tempat_pelatihan; ?></td>
                    <td><a href="<?php echo base_url(); ?>assets/upload/surat_permohonan/<?php echo $data->surat_permohonan; ?>"> <?php echo substr($data->surat_permohonan,0,20); ?></a></td>
                    <td><?php echo $data->mulai_tanggal; ?></td>
                    <td><?php echo $data->sampai_tanggal; ?></td>
                    <td><?php echo $data->total_jp." JP";; ?></td>
                    <td><?php echo $data->tgl_dibuat; ?></td>
                    <td><?php if(isset($data->tgl_diapprove)){echo $data->tgl_diapprove; }?></td>

                    
                      <?php if($menu=="pelatihan" || $menu=="arsip"){ 
                        if($data->approve_pelatihan=='0'){
                      ?>
                        <td>
                          <span style="color:red">Pending</span>
                        </td>
                      <?php }else if($data->approve_pelatihan=='1'){ ?>
                        <td>
                          <a href="<?php echo base_url().$role."/".$detail."/".$data->id_pelatihan; ?>"><button type="submit" class="btn btn-success" name="submit" value="kirim" id="delete">lihat</button></a>
                        </td>
                      <?php }else if($data->approve_pelatihan=='2'){ ?>
                        <td>
                          alasan ditolak karena, <span style="color:red;"><?php echo $data->alasan_ditolak; ?></span>
                        </td>

                      <?php } }else {?>
                      <td></td>
                        <td>
                          <a href="<?php echo base_url().$role."/delete_schedule/".$data->id_pelatihan; ?>"  onclick="return confirm('Anda Yakin Akan Menghapus Perencanaan Pelatihan ini ?')"><button type="submit" class="btn btn-danger" name="submit" value="kirim" id="delete">Delete</button></a>
                        </td>

                      <?php } ?>
                  </tr>
                <?php $no++; endforeach; ?>
                </tbody>
              </table>
              <div id="pager" class="pager" style="margin-top:0px;">
                <form>
                  <img src="<?php echo base_url()?>assets/table_sorter/addons/pager/icons/first.png" class="first"/>
                  <img src="<?php echo base_url()?>assets/table_sorter/addons/pager/icons/prev.png" class="prev"/>
                  <input type="text" class="pagedisplay"/>
                  <img src="<?php echo base_url()?>assets/table_sorter/addons/pager/icons/next.png" class="next"/>
                  <img src="<?php echo base_url()?>assets/table_sorter/addons/pager/icons/last.png" class="last"/>
                  <select class="pagesize">
                    <option selected="selected"  value="10">10</option>
                    <option value="20">20</option>
                    <option value="30">30</option>
                    <option  value="40">40</option>
                  </select>
                </form>
              </div>

              <!--

              <?php if($menu=="schedule"){ ?>
                        <td>
                        <span style="">
                        <?php if($data->approved==0){
                            echo "<span style='color:red;'>pending</span>";
                          ?>
                          <td></td>
                          <?php
                          }else if($data->approved==1){
                            echo "<span style='color:blue;'>approved</span>";
                            ?>
                            <td>
                            <a href="<?php echo base_url().$role."/".$detail."/".$data->id_pelatihan; ?>"><button type="submit" class="btn btn-danger" name="submit" value="kirim" id="delete">lihat</button></a>
                            </td>
                          <?php
                          }else if($data->approved==2){
                            echo "<span style='color:green;'>approved/sudah jadi pelatihan</span>";
                            ?>
                            <td>
                            <a href="<?php echo base_url().$role."/".$detail."/".$data->id_pelatihan; ?>"><button type="submit" class="btn btn-danger" name="submit" value="kirim" id="delete">lihat</button></a>
                            </td>
                          <?php
                            }
                            ?>

                          </span> 

                        </td>
                      <?php } ?>-->