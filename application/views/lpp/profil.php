<div id="page-wrapper">
<div class="row">
          <div class="col-lg-12">
            <div class="panel panel-primary">
              <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-bar-chart-o"></i> </h3>
              </div>
              <?php 
              $status = $this->session->flashdata('status');
              if(isset($status)){ echo $status; } 
              foreach ($profil_lpp as $data):
              ?>
              <div class="panel-body">
                <?php echo form_open(base_url().'lpp/proses_update_lpp'); ?>
			         <input type="hidden" name="id_lpp" value="<?php echo $data->id_lpp; ?>">
		             <div class="isi">
				      <?php 
			              $status = $this->session->flashdata('status');
			              if(isset($status)){ echo $status; } 
			              ?>
			              <div class="panel-body">
			                	<input type="hidden" name="from" value="home">
					             <div class="form-group">
					                <label>Nama Lembaga*</label>
					                <input class="form-control" type="text" value="<?php echo $data->nama_lpp; ?>" id="nama_lpp" name="nama_lpp">
					            </div>
					            <div class="form-group">
					                <label>Penanggung jawab*</label>
					                <input class="form-control" type="text" value="<?php echo $data->penanggungjawab; ?>" id="penanggungjawab" name="penanggungjawab">
					            </div>
					            <div class="form-group">
					                <label>Jabatan*</label>
					                <input class="form-control" type="text" value="<?php echo $data->jabatan; ?>" id="jabatan" name="jabatan">
					            </div>
					            <div class="form-group">
					                <label>No KTP*</label>
					                <input class="form-control" type="text" value="<?php echo $data->no_ktp; ?>" id="no_ktp" name="no_ktp">
					            </div>
					            <div class="form-group">
					                <label>Program*</label>
					                <input class="form-control" type="text" value="<?php echo $data->program; ?>" id="nama_lpp" name="program" value="pelatihan tingkat dasar">
					            </div>
					            <div class="form-group">
					                <label>Alamat/Sekertariat*</label>
					                <input class="form-control" type="text" value="<?php echo $data->alamat_lpp; ?>" id="alamat_lpp" name="alamat_lpp">
					            </div>
					            <div class="form-group">
					                <label>No Telp*</label>
					                <input class="form-control" type="text" value="<?php echo $data->no_telp_lpp; ?>" id="no_telp_lpp" name="no_telp_lpp">
					            </div>
					            <div class="form-group">
					                <label>No Fax*</label>
					                <input class="form-control" type="text" value="<?php echo $data->no_fax; ?>" id="nama_lpp" name="no_fax">
					            </div>
					            <div class="form-group">
					                <label>Email*</label>
					                <input class="form-control" type="text" value="<?php echo $data->email; ?>" id="nama_lpp" name="email">
					            </div>
					            <!-- <div class="form-group">
					                <label>Password*</label>
					                <input class="form-control" type="password" value="<?php echo $data->password; ?>" id="nama_lpp" name="password">
					            </div> -->
					            <div class="form-group">
					                <label>Website*</label>
					                <input class="form-control" type="text" value="<?php echo $data->website; ?>" id="nama_lpp" name="website">
					            </div>

					            <div class="form-group" style="margin:20px 0px;padding:20px;">
					            	<div class="form-group">
						                <label>Kontak Person*</label>
						            </div>
						            <div class="form-group" style="margin:10px;">
						                <label>Nama*</label>
						                <input class="form-control" type="text" value="<?php echo $data->kontak_nama; ?>" id="nama_lpp" name="kontak_nama">
						            </div>
						            <div class="form-group" style="margin:10px;">
						                <label>Telp/HP*</label>
						                <input class="form-control" type="text" value="<?php echo $data->kontak_tlp; ?>" id="nama_lpp" name="kontak_telp">
						            </div>
						            <div class="form-group" style="margin:10px;">
						                <label>Email*</label>
						                <input class="form-control" type="text" value="<?php echo $data->kontak_email; ?>" id="nama_lpp" name="kontak_email">
						            </div>
					            </div>
					            <input type="submit" class="btn btn-primary" name="submit" value="Update">
					            <!-- <button type="submit" class="btn btn-danger" name="submit" value="cancel">Tolak Request</button> -->
			                </form>
			              </div>
				  </div>
		      </div>
		            <!-- <button type="submit" class="btn btn-primary" name="submit" value="kirim">Tambah</button> -->
		            <!-- <button type="submit" class="btn btn-danger" name="submit" value="cancel">Tolak Request</button> -->
                </form>
            	<?php endforeach; ?>
              </div>
            </div>
          </div>
        </div><!-- /.row -->