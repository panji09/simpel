      <SCRIPT language=Javascript>
      <!--
      function digits(obj, e, allowDecimal, allowNegative)
      {
       var key;
       var isCtrl = false;
       var keychar;
       var reg;
        
       if(window.event) {
        key = e.keyCode;
        isCtrl = window.event.ctrlKey
       }
       else if(e.which) {
        key = e.which;
        isCtrl = e.ctrlKey;
       }
       
       if (isNaN(key)) return true;
       
       keychar = String.fromCharCode(key);
       
       // check for backspace or delete, or if Ctrl was pressed
       if (key == 8 || isCtrl)
       {
        return true;
       }

       reg = /\d/;
       var isFirstN = allowNegative ? keychar == '-' && obj.value.indexOf('-') == -1 : false;
       var isFirstD = allowDecimal ? keychar == '.' && obj.value.indexOf('.') == -1 : false;
       
       return isFirstN || isFirstD || reg.test(keychar);
      }

      //-->
   </SCRIPT>
      <br>
      <div class="col-lg-12">
            <div class="panel panel-primary">
              <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-bar-chart-o"></i>Evaluasi Penyelenggara</h3>
              </div>
              <div class="panel-body">
                

                <?php echo form_open_multipart(base_url().'lpp/proses_evaluasi_penyelenggara'); ?>



                <div class="form-group">
                    <label>LPP*</label>
                    <select class="form-control" name="id_lpp" id="id_lpp">
                      <option value="<?php echo $id_lpp; ?> "><?php echo $nama_lpp; ?> </option>
                    </select>
                </div>
                <div class="form-group">
                    <label>Pelatihan*</label>
                    <select class="form-control" name="id_pelatihan" id="id_pelatihan">
                    <?php foreach ($detail as $data): ?>
                      <option value="<?php echo $data->id_pelatihan; ?> "><?php echo $data->judul_pelatihan; ?> </option>
                    <?php endforeach; ?>
                    </select>
                </div>
                <div class="form-group" id="instansi">
                    <label>Nilai Kenyamanan ruang/tempat pelatihan*</label><br>
                    <input type="text" class="form-control" name="kenyamanan" onkeypress="return digits(this, event, true, true)" maxlength="4" style="width:60px;">
                    <!-- <input type="radio" name="kenyamanan" value="1">Kurang
                    <input type="radio" name="kenyamanan" value="2">Cukup
                    <input type="radio" name="kenyamanan" value="3">Baik
                    <input type="radio" name="kenyamanan" value="4">Sangat Baik -->
                </div> <br>
                <div class="form-group" id="instansi">
                    <label>Nilai Perlengkapan pelatihan*</label><br>
                    <input type="text" class="form-control" name="perlengkapan" onkeypress="return digits(this, event, true, true)" maxlength="4" style="width:60px;">
                    <!-- <input type="radio" name="perlengkapan" value="1">Kurang
                    <input type="radio" name="perlengkapan" value="2">Cukup
                    <input type="radio" name="perlengkapan" value="3">Baik
                    <input type="radio" name="perlengkapan" value="4">Sangat Baik -->
                </div> <br>
                <div class="form-group" id="instansi">
                    <label>Nilai Ketersediaan materi pelatihan*</label><br>
                    <input type="text" class="form-control" name="ketersediaan" onkeypress="return digits(this, event, true, true)" maxlength="4" style="width:60px;">
                    <!-- <input type="radio" name="ketersediaan" value="1">Kurang
                    <input type="radio" name="ketersediaan" value="2">Cukup
                    <input type="radio" name="ketersediaan" value="3">Baik
                    <input type="radio" name="ketersediaan" value="4">Sangat Baik -->
                </div>  <br>
                <div class="form-group">
                    <label>kritik dan saran*</label>
                    <textarea class="form-control" rows="7" name="kritik"></textarea>
                 </div><br>
                <div class="form-group" id="file">
                    <input type="file" name="userfile">
                </div>
                <button type="submit" class="btn btn-primary" name="submit" value="approve">Kirim</button>
                <br>
                </form>
              </div>
            </div>
          </div>
        </div><!-- /.row -->
    