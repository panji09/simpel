<div id="page-wrapper">

        <div class="row">
          <div class="col-lg-12">
            <h2>All Request</h2>
            <div class="table-responsive">
              <table class="table table-bordered table-hover table-striped tablesorter">
                <thead>
                  <tr>
                    <th>Tema <i class="fa fa-sort"></i></th>
                    <th>Mulai Tanggal <i class="fa fa-sort"></i></th>
                    <th>Sampai Tanggal <i class="fa fa-sort"></i></th>
                    <th>Keterangan <i class="fa fa-sort"></i></th>
                    <?php if($stat==2){ ?>
                    <th>Keterangan Ditolak<i class="fa fa-sort"></i></th>
                    <?php } ?>
                  </tr>
                </thead>
                <tbody>
                <?php foreach ($allRequest as $data): ?>
                  <tr>
                    <td><?php echo $data->judul_pelatihan; ?></td>
                    <td><?php echo $data->mulai_tanggal; ?></td>
                    <td><?php echo $data->sampai_tanggal; ?></td>
                    <td><?php echo $data->keterangan; ?></td>
                    <?php if($stat==2){ ?>
                    <td><?php echo $data->keterangan_ditolak; ?></td>
                    <?php } ?>
                  </tr>
                <?php endforeach; ?>
                </tbody>
              </table>
            </div>
          </div>

      </div><!-- /#page-wrapper -->