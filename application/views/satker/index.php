

      <div id="page-wrapper">


        <div class="row">

          <div class="col-lg-4">
            <div class="panel panel-info">
              <div class="panel-heading">
                <div class="row">
                  <div class="col-xs-6">
                    <i class="fa fa-tasks fa-5x"></i>
                  </div>
                  <div class="col-xs-6 text-right">
                    <p class="announcement-heading"><?php echo $totalApproved; ?></p>
                    <p class="announcement-text">Request approved</p>
                  </div>
                </div>
              </div>
              <a href="#">
                <div class="panel-footer announcement-bottom">
                  <div class="row">
                    <div class="col-xs-6">
                      <a href="<?php echo base_url(); ?>satker/allrequest/1">View All</a>
                    </div>
                    <div class="col-xs-6 text-right">
                      <i class="fa fa-arrow-circle-right"></i>
                    </div>
                  </div>
                </div>
              </a>
            </div>
          </div>

          <div class="col-lg-4">
            <div class="panel panel-warning">
              <div class="panel-heading">
                <div class="row">
                  <div class="col-xs-6">
                    <i class="fa fa-tasks fa-5x"></i>
                  </div>
                  <div class="col-xs-6 text-right">
                    <p class="announcement-heading"><?php echo $totalPending; ?></p>
                    <p class="announcement-text">Request pending</p>
                  </div>
                </div>
              </div>
              <a href="#">
                <div class="panel-footer announcement-bottom">
                  <div class="row">
                    <div class="col-xs-6">
                      <a href="<?php echo base_url(); ?>satker/allrequest/0">View All</a>
                    </div>
                    <div class="col-xs-6 text-right">
                      <i class="fa fa-arrow-circle-right"></i>
                    </div>
                  </div>
                </div>
              </a>
            </div>
          </div>
          
          <div class="col-lg-4">
            <div class="panel panel-danger">
              <div class="panel-heading">
                <div class="row">
                  <div class="col-xs-6">
                    <i class="fa fa-tasks fa-5x"></i>
                  </div>
                  <div class="col-xs-6 text-right">
                    <p class="announcement-heading"><?php echo $totalCanceled; ?></p>
                    <p class="announcement-text">Request di tolak</p>
                  </div>
                </div>
              </div>
              <a href="#">
                <div class="panel-footer announcement-bottom">
                  <div class="row">
                    <div class="col-xs-6">
                      <a href="<?php echo base_url(); ?>satker/allrequest/2">View All</a>
                    </div>
                    <div class="col-xs-6 text-right">
                      <i class="fa fa-arrow-circle-right"></i>
                    </div>
                  </div>
                </div>
              </a>
            </div>
          </div>
          
        </div><!-- /.row -->

        <div class="row">
          <div class="col-lg-12">
            <div class="panel panel-primary">
              <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-bar-chart-o"></i> Request Pelatihan Baru</h3>
              </div>
              <?php 
              $status = $this->session->flashdata('status');
              if(isset($status)){ echo $status; } 
              ?>
              <div class="panel-body">
                <?php echo form_open(base_url().'satker/insertPelatihan'); ?>
                	<div class="form-group">
                		<label>Tema Pelatihan</label>
		                <select class="form-control" name="tema_pelatihan">
		                <?php foreach ($tema_pelatihan as $data) : ?>
		                  <option value="<?php echo $data->id_tema; ?>"><?php echo $data->judul_pelatihan; ?></option>
		                <?php endforeach; ?>
		                </select>
		            </div>
		            <div class="form-group">
		                <label>Dari tanggal</label>
		                <input class="form-control" type="text" id="dari_tanggal" name="dari_tanggal">
		            </div>
		            <div class="form-group">
		                <label>Sampai tanggal</label>
		                <input class="form-control" type="text" id="sampai_tanggal" name="sampai_tanggal">
		            </div>
		            <div class="form-group">
		                <label>Keterangan</label>
		                <textarea class="form-control" rows="4" name="keterangan"></textarea>
		              </div>
		            <button type="submit" class="btn btn-default">Kirim Request</button>
                </form>
              </div>
            </div>
          </div>
        </div><!-- /.row -->

        

      </div><!-- /#page-wrapper -->

    </div><!-- /#wrapper -->