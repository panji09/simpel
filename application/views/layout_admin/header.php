<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SIMPEL</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url(); ?>assets/css/bootstrap.css" rel="stylesheet">
    
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugin/datatables-bootstrap/css/datatables.css">
    
    <!-- Add custom CSS here -->
    <link href="<?php echo base_url(); ?>assets/css/sb-admin.css" rel="stylesheet">
    
    <!-- <link rel="stylesheet" href="font-awesome/<?php echo base_url(); ?>assets/css/font-awesome.min.css"> -->
    <!-- Page Specific CSS 
    <link rel="stylesheet" href="http://cdn.oesmith.co.uk/morris-0.4.3.min.css">-->
    <!-- datepicker -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/jquery-ui.css">
    
    <script src="https://code.jquery.com/jquery-1.11.2.min.js"></script>
<!--     <script src="<?php echo base_url(); ?>assets/form/js/jquery.js"></script>  -->
<!--     <script src="<?php echo base_url(); ?>assets/plugin/bootstrap/js/bootstrap.min.js"></script> -->
<!--     <script src="<?php echo base_url(); ?>assets/plugin/jquery-ui/jquery-ui.min.js"></script> -->
    
    <script src="<?php echo base_url(); ?>assets/js/jquery-ui.js"></script>

    <!-- form dynamic -->
    <link href="<?php echo base_url(); ?>assets/form/css/style.css" rel="stylesheet">

    
    <!-- table sorter -->
    <link rel="stylesheet" href="<?php echo base_url()?>assets/table_sorter/css/jq.css" type="text/css" media="print, projection, screen" />
    <link rel="stylesheet" href="<?php echo base_url()?>assets/table_sorter/themes/blue/style.css" type="text/css" media="print, projection, screen" />
    <!--<script src="<?php echo base_url(); ?>assets/table_sorter/jquery-latest.js"></script> -->
    
    
<!--     <script type="text/javascript" src="<?php echo base_url()?>assets/table_sorter/jquery.tablesorter.js"></script> -->
<!--     <script type="text/javascript" src="<?php echo base_url()?>assets/table_sorter/addons/pager/jquery.tablesorter.pager.js"></script> -->
    
    <?php if($this->uri->segment(2) !='pelatihan_narasumber'):?>
    <script src="<?php echo base_url(); ?>assets/form/js/script.js"></script>  
    <?php endif;?>


    <!--<script type="text/javascript">
    $(function() {
      $("table")
        .tablesorter({widthFixed: true, widgets: ['zebra']})
        .tablesorterPager({container: $("#pager")});
    });
    </script>-->

    <style type="text/css">
      .fileUpload {
        position: relative;
        overflow: hidden;
        margin: 10px;
      }
      .fileUpload input.upload {
        position: absolute;
        top: 0;
        right: 0;
        margin: 0;
        padding: 0;
        font-size: 20px;
        cursor: pointer;
        opacity: 0;
        filter: alpha(opacity=0);
      }
           </style>

           <?php
           /*
          <script type = "text/javascript">

          var tim = 0;
          function reload () {
          tim = setTimeout("location.reload(true);",60000);   // 3 minutes
          }

          function canceltimer() {
          window.clearTimeout(tim);  // cancel the timer on each mousemove/click
          reload();  // and restart it
          }

          </script>
          */
	  ?>
  </head>

  <body>
  <div id="wrapper" onmousemove = "canceltimer()"; onclick = "canceltimer()" onkeypress="canceltimer()">

      <!-- Sidebar -->
      <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="<?php echo base_url(); ?>satker">SIMPEL</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse navbar-ex1-collapse">
          

          <?php if($role=="lpp"){ ?>


          <ul class="nav navbar-nav side-nav">
            <li><a href="<?php echo base_url().$role; ?>"><!--<i class="fa fa-dashboard">--></i> Dashboard <span class="badge"><?php echo $notifikasi_row; ?></span></a></li>
            <li><a href="<?php echo base_url().$role; ?>/profil"><!--<i class="fa fa-dashboard">--></i> Profil</a></li>
            <!-- <li><a href="<?php echo base_url().$role; ?>/request_pelatihan"><!--<i class="fa fa-dashboard"></i> Request Pelatihan</a></li>
             -->
             <li><a href="<?php echo base_url().$role; ?>/schedule_pelatihan"><!--<i class="fa fa-dashboard">--></i> Perencanaan Pelatihan</a></li>
             <li><a href="<?php echo base_url().$role; ?>/tambah_pelatihan"><!--<i class="fa fa-dashboard">--></i> kirim permohonan pelatihan</a></li>
             <li><a href="<?php echo base_url().$role; ?>/semua_pelatihan"><!--<i class="fa fa-dashboard">--></i> Pelatihan Berlangsung <span class="badge"><?php echo $notifikasi_schedule; ?></a></li>
             <li><a href="<?php echo base_url().$role; ?>/arsip_pelatihan"><!--<i class="fa fa-dashboard">--></i> Arsip Pelatihan</a></li>
             <li><a href="<?php echo base_url().$role; ?>/pelatihan_ditolak"><!--<i class="fa fa-dashboard">--></i> Pelatihan Ditolak <span class="badge"><?php echo $notifikasi_ditolak; ?></a></li>
             
            <li><a href="<?php echo base_url().$role."/library"; ?>"><!--<i class="fa fa-dashboard">--></i>Library</a></li>
             <li><a href="<?php echo base_url().$role."/file_explorer/";?>"><!--<i class="fa fa-dashboard">--></i>File Explorer</a></li>
            <!-- <li><a href="<?php echo base_url().$role; ?>/semua_pelatihan"><!--<i class="fa fa-dashboard"></i> Semua Request</a></li>
             --><li><a href="<?php echo base_url()."admin/logout"; ?>"><!--<i class="fa fa-dashboard">--></i>Logout</a></li>
          </ul>
          <ul class="nav navbar-nav navbar-right navbar-user">
          <li>
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-envelope"></i> 
            
            <span class="badge"><?php echo $this->session->userdata('username'); ?></span>
            </a>
          </li>
          <li>
            <a href="<?php echo base_url(); ?>" target="_blank" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-envelope"></i> 
            
            <span class="badge">View Website</span>
            </a>
          </li>
            <!-- <li class="dropdown messages-dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-envelope"></i>Pelatihan Diterima <span class="badge">
              <?php echo $notiftotalApproved; ?>
              </span> <b class="caret"></b></a>
              <ul class="dropdown-menu">
                <li class="dropdown-header"><?php echo $notiftotalApproved; ?> New Notification</li>
                <?php foreach ($notiflisttotalApproved as $data) : ?>
                <li class="message-preview">
                  <a href="<?php echo base_url().$role."/detail_pelatihan/".$data->id_request; ?>">
                    <span class="message"><?php echo $data->judul_pelatihan; ?></span>
                  </a>
                </li>
                <?php endforeach; ?>
                <li class="divider"></li>
                <!-- <li><a href="#">View All <span class="badge"><?php echo $notiftotalApproved; ?></span></a></li> 
              </ul>
            </li>
            <li class="dropdown messages-dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-envelope"></i>Pelatihan Pending <span class="badge">
              <?php echo $notiftotalPending; ?>
              </span> <b class="caret"></b></a>
              <ul class="dropdown-menu">
                <li class="dropdown-header"><?php echo $notiftotalPending; ?> New Notification</li>
                <?php foreach ($notiflisttotalPending as $data) : ?>
                <li class="message-preview">
                  <a href="<?php echo base_url().$role."/detail_pelatihan_2/".$data->id_request; ?>/pending">
                    <span class="message"><?php echo $data->judul_pelatihan; ?></span>
                  </a>
                </li>
                <?php endforeach; ?>
                <li class="divider"></li>
                <!-- <li><a href="#">View All <span class="badge"><?php echo $notiftotalPending; ?></span></a></li>
              </ul>
            </li>
            <li class="dropdown messages-dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-envelope"></i>Pelatihan Ditolak <span class="badge">
              <?php echo $notiftotalCanceled; ?>
              </span> <b class="caret"></b></a>
              <ul class="dropdown-menu">
                <li class="dropdown-header"><?php echo $notiftotalCanceled; ?> New Notification</li>
                <?php foreach ($notiflisttotalCanceled as $data) : ?>
                <li class="message-preview">
                  <a href="<?php echo base_url().$role."/detail_pelatihan_2/".$data->id_request; ?>">
                    <span class="message"><?php echo $data->judul_pelatihan; ?></span>
                  </a>
                </li>
                <?php endforeach; ?>
                <li class="divider"></li>
                <!-- <li><a href="#">View All <span class="badge"><?php echo $notiftotalCanceled; ?></span></a></li> 
              </ul>
            </li>
            <li class="dropdown user-dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?php echo $username; ?> <b class="caret"></b></a>
              <ul class="dropdown-menu">
                <li><a href="<?php echo base_url(); ?>admin/logout"><i class="fa fa-power-off"></i> Log Out</a></li>
              </ul>
            </li> -->
          </ul>
          <?php }elseif ($role=="lkpp") { ?>
          <ul class="nav navbar-nav side-nav" style="overflow:scroll;height:90%;">
            <li><a href="<?php echo base_url().$role; ?>"><!--<i class="fa fa-dashboard">--></i> Dashboard</a></li>
            <li><a href="<?php echo base_url().$role."/lpp/all"; ?>"><!--<i class="fa fa-dashboard">--></i> Data LPP / Satker</a></li>
            <li><a href="<?php echo base_url().$role."/narasumber"; ?>"><!--<i class="fa fa-dashboard">--></i> Data Narasumber</a></li>
            <li><a href="<?php echo base_url().$role."/new_fasilitator"; ?>"><!--<i class="fa fa-dashboard">--></i>Fasilitator</a></li>
            <li><div style="width:220px;border:1px solid #CACACA;float:left;"></div></li>
            <li><a href="<?php echo base_url().$role."/berita"; ?>"><!--<i class="fa fa-dashboard">--></i>Berita</a></li>
            <li><a href="<?php echo base_url().$role."/pengumuman/all"; ?>"><!--<i class="fa fa-dashboard">--></i>Pengumuman</a></li>
            <li><div style="width:220px;border:1px solid #CACACA;float:left;"></div></li>
            <li><a href="<?php echo base_url().$role."/tema_pelatihan"; ?>"><!--<i class="fa fa-dashboard">--></i> Jenis Pelatihan</a></li>
            <li><a href="<?php echo base_url().$role."/approve_schedule"; ?>"><!--<i class="fa fa-dashboard">--></i>Setujui Pelatihan <span class="badge"><?php echo $notifScheduleBaru; ?></span></a></li>
            <li><a href="<?php echo base_url().$role."/fasilitatori_pelatihan"; ?>"><!--<i class="fa fa-dashboard">--></i>Fasilitatori Pelatihan <span class="badge"><?php echo $notifMintaFasilitator; ?></span></a></li>
            <li><a href="<?php echo base_url().$role."/pelatihan"; ?>"><!--<i class="fa fa-dashboard">--></i>Pelatihan Berlangsung</a></li>
            <li><a href="<?php echo base_url().$role."/pelatihan_ditolak"; ?>"><!--<i class="fa fa-dashboard">--></i>Pelatihan Ditolak</a></li>
             <li><a href="<?php echo base_url().$role; ?>/arsip_pelatihan"><!--<i class="fa fa-dashboard">--></i> Arsip Pelatihan</a></li>
            <li><div style="width:220px;border:1px solid #CACACA;float:left;"></div></li>
            <li><a href="<?php echo base_url().$role."/approve_rencana_kegiatan"; ?>"><!--<i class="fa fa-dashboard">--></i> Setujui Agenda Pribadi <span class="badge"><?php echo $notifAgendaBaru; ?></span></a></li>
            <li><a href="<?php echo base_url().$role."/arsip_rencana_kegiatan"; ?>"><!--<i class="fa fa-dashboard">--></i> Arsip Agenda Pribadi</a></li>
            <li><div style="width:220px;border:1px solid #CACACA;float:left;"></div></li>
            
            <li><a href="<?php echo base_url().$role."/file_explorer"; ?>"><!--<i class="fa fa-dashboard">--></i>File Explorer</a></li>
            <li><a href="<?php echo base_url().$role."/upload_materi"; ?>"><!--<i class="fa fa-dashboard">--></i>Library</a></li>
            <li><a href="<?php echo base_url().$role."/upload_dokumen"; ?>"><!--<i class="fa fa-dashboard">--></i>Upload Dokumen</a></li>
            <li><a href="<?php echo base_url().$role."/upload_regulasi"; ?>"><!--<i class="fa fa-dashboard">--></i>Upload Regulasi</a></li>
            <li><a href="<?php echo base_url().$role."/galeri"; ?>"><!--<i class="fa fa-dashboard">--></i>Galeri</a></li>
            <li><a href="<?php echo base_url().$role."/ganti_password"; ?>"><!--<i class="fa fa-dashboard">--></i>Ganti Password</a></li>
            <li><a href="<?php echo base_url()."admin/logout"; ?>"><!--<i class="fa fa-dashboard">--></i>Logout</a></li>
            <!-- <li><a href="<?php echo base_url(); ?>lkpp/pelatihan"><!--<i class="fa fa-dashboard"></i> Pelatihan Narasumber</a></li> -->
          </ul>
          <ul class="nav navbar-nav navbar-right navbar-user">
            <li><a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-envelope"></i> Welcome, <span class="badge"><?php echo $this->session->userdata('username'); ?></span></a></li>
            <!-- <li class="dropdown messages-dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-envelope"></i> Request Baru <span class="badge">
              <?php echo $notiftotalPending; ?>
              </span> <b class="caret"></b></a>
              <ul class="dropdown-menu">
                <li class="dropdown-header"><?php echo $notiftotalPending; ?> New Notification</li>
                <?php foreach ($notiflisttotalPending as $data): ?>
                <li class="message-preview">
                  <a href="<?php echo base_url(); ?>lkpp/detail_request/<?php echo $data->id_request; ?>">
                    <span class="message"><?php echo $data->judul_pelatihan ; ?></span>
                  </a>
                </li>
                <?php endforeach; ?>
                <li class="divider"></li>
                <li><a href="#">View All <span class="badge"><?php echo $notiftotalPending; ?></span></a></li>
              </ul>
            </li> 
            <li class="dropdown user-dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?php echo $username; ?> <b class="caret"></b></a>
              <ul class="dropdown-menu">
                <li><a href="<?php echo base_url(); ?>admin/logout"><i class="fa fa-power-off"></i> Log Out</a></li>
              </ul>
            </li>-->
          <li>
            <a href="<?php echo base_url(); ?>" target="_blank" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-envelope"></i> 
            
            <span class="badge">View Website</span>
            </a>
          </li>
            </ul>

          <?php } elseif ($role=="narasumber") { ?>
          <?php 
          foreach ($detail as $data): 
            $newname1 = str_replace(" ","_",$data->nama_narasumber);
            $newname2 = str_replace(".","_",$newname1);
          endforeach;
          ?>
          <ul class="nav navbar-nav side-nav">
            <li><a href="<?php echo base_url().$role; ?>"><!--<i class="fa fa-dashboard">--></i>Dashboard</a></li>
            <li><a href="<?php echo base_url().$role; ?>/profil/<?php echo $id_user; ?>"><!--<i class="fa fa-dashboard">--></i>Profil</a></li>
            <li><a href="<?php echo base_url().$role; ?>/schedule_narasumber"><!--<i class="fa fa-dashboard">--></i>Perencanaan Narasumber</a></li>
             <li><a href="<?php echo base_url().$role; ?>/arsip_pelatihan"><!--<i class="fa fa-dashboard">--></i> Arsip Pelatihan</a></li>
            <li><a href="<?php echo base_url(); ?>narasumber/all_request_pelatihan"><!--<i class="fa fa-dashboard">--></i>All Request Pelatihan <span class="badge"><?php echo $notiftotalPending; ?></span></a></li>
            <li><a href="<?php echo base_url(); ?>narasumber/rencana_kegiatan"><!--<i class="fa fa-dashboard">--></i>Agenda Pribadi</a></li>
            
            <li><a href="<?php echo base_url()."narasumber/file_explorer/";?>"><!--<i class="fa fa-dashboard">--></i>File Explorer</a></li>
            
            <li><a href="<?php echo base_url()."narasumber/ganti_password"; ?>"><!--<i class="fa fa-dashboard">--></i>Ganti Password</a></li>
            <li><a href="<?php echo base_url()."admin/logout"; ?>"><!--<i class="fa fa-dashboard">--></i>Logout</a></li>
          </ul>

          <!-- NOTIF DI ATAS -->
          <ul class="nav navbar-nav navbar-right navbar-user">
          <li><a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-envelope"></i> <!--<img src="<?php echo base_url(); ?>assets/upload/narasumber/<?php echo $newname2.".jpg"; ?>" width="30px" height="30px">--> <span style="color:white;font-weight:bold;"><?php echo $nama_narasumber; ?></span></a></li>
          <li>
            <a href="<?php echo base_url(); ?>" target="_blank" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-envelope"></i> 
            
            <span class="badge">View Website</span>
            </a>
          </li>
            <!-- <li class="dropdown messages-dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-envelope"></i>Request Pelatihan<span class="badge">
              <?php echo $notiftotalApproved; ?>
              </span> <b class="caret"></b></a>
              <ul class="dropdown-menu">
                <li class="dropdown-header"><?php echo $notiftotalApproved; ?> New Notification</li>
                <?php foreach ($notiflisttotalApproved as $data) : ?>
                <li class="message-preview">
                  <a href="<?php echo base_url().$role."/detail_pelatihan/".$data->id_request; ?>">
                    <span class="message"><?php echo $data->judul_pelatihan; ?></span>
                  </a>
                </li>
                <?php endforeach; ?>
                <li class="divider"></li>
                <li><a href="#">View All <span class="badge"><?php echo $notiftotalApproved; ?></span></a></li>
              </ul> 
            </li> -->
            <!-- <li class="dropdown messages-dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-envelope"></i>Request Pelatihan <span class="badge">
              <?php echo $notiftotalPending; ?>
              </span> <b class="caret"></b></a>
              <ul class="dropdown-menu">
                <li class="dropdown-header"><?php echo $notiftotalPending; ?> New Notification</li>
                <?php foreach ($notiflisttotalPending as $data) : ?>
                <li class="message-preview">
                  <a href="<?php echo base_url().$role."/detail_all_request_pelatihan/".$data->id; ?>">
                    <span class="message"><?php echo $data->judul_pelatihan; ?></span>
                  </a>
                </li>
                <?php endforeach; ?>
                <li class="divider"></li>
                <!-- <li><a href="#">View All <span class="badge"><?php echo $notiftotalPending; ?></span></a></li>
              </ul>
            </li> -->
            <!-- <li class="dropdown messages-dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-envelope"></i>Pelatihan Ditolak<span class="badge">
              <?php echo $notiftotalCanceled; ?>
              </span> <b class="caret"></b></a>
              <ul class="dropdown-menu">
                <li class="dropdown-header"><?php echo $notiftotalCanceled; ?> New Notification</li>
                <?php foreach ($notiflisttotalCanceled as $data) : ?>
                <li class="message-preview">
                  <a href="#">
                    <span class="message"><?php echo $data->judul_pelatihan; ?></span>
                  </a>
                </li>
                <?php endforeach; ?>
                <li class="divider"></li>
                <li><a href="#">View All <span class="badge"><?php echo $notiftotalCanceled; ?></span></a></li>
              </ul>
            </li> --> 
            <!-- <li class="dropdown user-dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?php echo $username; ?> <b class="caret"></b></a>
              <ul class="dropdown-menu">
                <li><a href="<?php echo base_url(); ?>admin/logout"><i class="fa fa-power-off"></i> Log Out</a></li>
              </ul>
            </li>
          </ul> -->

          <!-- NOTIF DI ATAS -->

          <?php } elseif ($role=="fasilitator") { ?>
          <ul class="nav navbar-nav side-nav">
            <li><a href="<?php echo base_url().$role; ?>"><!--<i class="fa fa-dashboard">--></i>Dashboard <span class="badge"><?php echo $countNotifNarasumber; ?></span></a></li>
            <li><a href="<?php echo base_url().$role; ?>/new_request/<?php echo $id_user; ?>"><!--<i class="fa fa-dashboard">--></i>Request Baru <span class="badge"><?php echo $total_request; ?></span></a></li>
            <li><a href="<?php echo base_url().$role; ?>/pelatihan/<?php echo $id_user; ?>"><!--<i class="fa fa-dashboard">--></i>Pelatihan</a></li>
            <li><a href="<?php echo base_url().$role; ?>/arsip_pelatihan/<?php echo $id_user; ?>"><!--<i class="fa fa-dashboard">--></i>Arsip Pelatihan</a></li>
            <li><a href="<?php echo base_url().$role; ?>/pelatihan_narasumber/<?php echo $id_user; ?>"><!--<i class="fa fa-dashboard">--></i>Data Narasumber</a></li>
            <li><a href="<?php echo base_url().$role."/file_explorer/".$id_user;?>"><!--<i class="fa fa-dashboard">--></i>File Explorer</a></li>
            <li><a href="<?php echo base_url()."admin/logout"; ?>"><!--<i class="fa fa-dashboard">--></i>Logout</a></li>
          </ul>
            
          <?php } ?>
        </div><!-- /.navbar-collapse -->
      </nav>
      <br><br><br>