
    <!-- JavaScript
    <script src="<?php echo base_url(); ?>assets/js/jquery-1.10.2.js"></script> 
    <script src="<?php echo base_url(); ?>assets/js/bootstrap.js"></script>-->

    <!-- Page Specific Plugins
    <script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
    <script src="http://cdn.oesmith.co.uk/morris-0.4.3.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/morris/chart-data-morris.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/tablesorter/jquery.tablesorter.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/tablesorter/tables.js"></script> 
      <script src="//code.jquery.com/jquery-1.9.1.js"></script>-->
      <script src="<?php echo base_url(); ?>assets/js/jquery-ui.js"></script>
      <script>
      $(function() {
        $( "#dari_tanggal" ).datepicker({ dateFormat: 'yy-mm-dd' });
        $("#dari_tanggal").mousedown(function() {
            $(this).datepicker("hide");    
        });
        $( "#sampai_tanggal" ).datepicker({ dateFormat: 'yy-mm-dd' });
        $("#sampai_tanggal").mousedown(function() {
            $(this).datepicker("hide");    
        });
        $( "#tgl_pelatihan" ).datepicker({ dateFormat: 'yy-mm-dd' });
      });
      </script>
      
      <?php if($this->uri->segment(2)=='pelatihan_narasumber'):?>
      <!-- datatables-->
      <script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>assets/plugin/datatables/media/js/jquery.dataTables.js"></script>
      <script src="<?php echo base_url(); ?>assets/plugin/datatables-bootstrap/js/datatables.js"></script>
      <script type="text/javascript" charset="utf-8">
	var asInitVals = new Array();
	var i=0;
	var update=true;
	$(document).ready(function() {		      
	  $.fn.dataTableExt.oApi.fnPagingInfo = function ( oSettings )
	  {
	    return {
	      "iStart":         oSettings._iDisplayStart,
	      "iEnd":           oSettings.fnDisplayEnd(),
	      "iLength":        oSettings._iDisplayLength,
	      "iTotal":         oSettings.fnRecordsTotal(),
	      "iFilteredTotal": oSettings.fnRecordsDisplay(),
	      "iPage":          Math.ceil( oSettings._iDisplayStart / oSettings._iDisplayLength ),
	      "iTotalPages":    Math.ceil( oSettings.fnRecordsDisplay() / oSettings._iDisplayLength )
	    };
	  }

	  var oTable = $('#load-table').dataTable( {
	    "bStateSave": false,
	    "bProcessing": true,
	    "bServerSide": true,
	    "iDisplayLength": 25,
	    "sPaginationType": "bs_full",
	    "sAjaxSource": "<?=site_url('fasilitator/ajax_pelatihan_narasumber/'.$this->session->flashdata('tahun').'/'.$this->session->flashdata('periode'))?>",
	    "sServerMethod": "POST",
	    "aoColumnDefs": [ 
	      { "bSortable": false, "aTargets": [ 0 ] }, //no
	      
	      {
		"fnRender": function ( oObj ) {
		  if(update){
		    i=oTable.fnPagingInfo().iStart;
		    update=false;
		  }
		  if(i+1 == oTable.fnPagingInfo().iEnd){
		    update=true;
		  }
		  
		  return ++i;
			
		},
		"bUseRendered": false,
		"aTargets": [ 0 ] //no
	      }
	    ],
	    "fnDrawCallback": function () {
		
	    }
	  } );
	  
	   $("#load-table thead input").change( function () {
		    /* Filter on the column (the index) of this element */
		    var id = $(this).attr('id').split("-")[1];
		    oTable.fnFilter( this.value, id );
	    } );
	    $("#load-table thead input").keyup( function () {
		    /* Filter on the column (the index) of this element */
		    var id = $(this).attr('id').split("-")[1];
		    oTable.fnFilter( this.value, id );
	    } );
	  $('#load-table').each(function(){
	      var datatable = $(this);
	      // SEARCH - Add the placeholder for Search and Turn this into in-line form control
	      var search_input = datatable.closest('.dataTables_wrapper').find('div[id$=_filter] input');
	      search_input.attr('placeholder', 'Search');
	      search_input.addClass('form-control input-sm');
	      // LENGTH - Inline-Form control
	      var length_sel = datatable.closest('.dataTables_wrapper').find('div[id$=_length] select');
	      length_sel.addClass('form-control input-sm');
	  });
	  
			      
	});
      </script>
      <!-- end datatables-->
      <?php endif;?>
  </body>
</html>
