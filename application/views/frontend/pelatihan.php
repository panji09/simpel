<script src="<?php echo base_url(); ?>assets/form/js/jquery.js"></script> 
<style type="text/css">
  .entry-form2{
    display: none;
  }
</style>
<script type="text/javascript">
function lihat_narasumber(id) {
  $.ajax({
      type: "POST", 
      url: "<?php echo base_url(); ?>jadwal/get_narasumber/"+id, 
      data : $("#userinfo").serialize(),
      dataType: "json",
      success: function(response){
        $(".entry-form").fadeIn("fast");
        var json_obj = JSON.parse(JSON.stringify(response));
        //console.log("narasumber : "+JSON.stringify(json_obj));
        var content = "<ul>";
        
        json_obj.forEach(function(object) {
          //document.getElementById("list_narasumber").innerHTML="<ul><li>"+object.nama_narasumber;
          content += "<li>"+object.nama_narasumber+"</li><br>";
        });

          content += "</ul>";

          document.getElementById("list_narasumber").innerHTML=content;
      },
      error: function(xhr, status, error){
        document.getElementById("list_narasumber").innerHTML="Unexpected error! Try again. = "+error;
      }
    });

  
   
  }

  function lihat_lpp(id) {
  $.ajax({
      type: "POST", 
      url: "<?php echo base_url(); ?>jadwal/get_lpp/"+id, 
      data : $("#userinfo").serialize(),
      dataType: "json",
      success: function(response){
        $(".entry-form2").fadeIn("fast");
        var json_obj = JSON.parse(JSON.stringify(response));
        //console.log("narasumber : "+JSON.stringify(json_obj));
        var content = "<ul>";
        
        json_obj.forEach(function(object) {
          //document.getElementById("list_narasumber").innerHTML="<ul><li>"+object.nama_narasumber;
          content += "<li> Email : "+object.email+"</li><br>";
          content += "<li> Kontak Person : "+object.kontak_nama+"</li><br>";
          content += "<li> No Telp : "+object.kontak_tlp+"</li><br>";
        });

          content += "</ul>";

          document.getElementById("list_lpp").innerHTML=content;
      },
      error: function(xhr, status, error){
        document.getElementById("list_lpp").innerHTML="Unexpected error! Try again. = "+error;
      }
    });

  
   
  }

  function close_popup () {
    $(".entry-form").fadeOut("fast");
    $(".entry-form2").fadeOut("fast");
  }
</script>
<style type="text/css">
  #negeri{
    display: none;
  }
  #swasta{
    display: none;
  }
  td{
    padding: 10px;
  }
  .entry-form{
    display: none;
  }
</style>

<!-- content -->
<div class="wrapper row2">

  <div id="container_body" class="clear" style="padding:15px 0px 0px 0px;min-height:500px;">
    <!-- content body -->
   
    <div id="col2" style="margin:0px 0px 20px 0px;width:100%">
      <div class="wrapper row2">
		  <div id="container" class="clear">
		    <div id="homepage" class="clear">
		       <div id="daftar" class="box_tengah" style="padding:20px;min-height:200px;height:100%;float:left;">

            <table style="border:1px solid #DADADA; border-collapse:collapse;" border="" class="table table-hover table-striped tablesorter">
              <tr>
                <th>No</th>
                <th>Judul Pelatihan</th>
                <th>LPP</th>
                <th>Dari Tanggal</th>
                <th>Sampai Tanggal</th>
                <th>Narasumber</th>
              </tr>
              <?php $no=1; foreach ($pelatihan as $data): ?>
              <tr>
                <td><?php echo $no; ?></td>
                <td><?php  echo $data->judul_pelatihan; ?></td>
                <td><a href="#"  onclick="javascript:lihat_lpp('<?php  echo $data->id_lpp; ?>');"><?php  echo $data->nama_lpp; ?></td>
                <td><?php  echo tgl_indo($data->mulai_tanggal); ?></td>
                <td><?php  echo tgl_indo($data->sampai_tanggal); ?></td>
                <td><a href="#" onclick="javascript:lihat_narasumber('<?php  echo $data->id_pelatihan; ?>');">Lihat Narasumber</a></td>
              </tr>
              <?php $no ++; endforeach; ?>
            </table>
		      			
		      			<!-- <div style="float:left;border:1px solid #CACACA;width:100%;margin:8px 0px;padding:20px 8px;font-size:18px;">
		      			<?php  echo $data->judul_pelatihan; ?>
		      			</div> -->
                <div class="entry-form">
                  <form name="userinfo" id="userinfo"> 
                  <table width="100%" border="0" cellpadding="4" cellspacing="0">
                    <tr>
                      <td colspan="2" align="right"><a href="#" id="close" onclick="javascript:close_popup();">Close</a></td>
                    </tr>
                    
                    <tr>
                      <td>
                        <p id="list_narasumber">
                          lorem ipsum dolor sit amet
                        </p>
                      </td>
                    </tr>
                  </table>
                  </form>
                </div>
                <div class="entry-form2">
                  <form name="userinfo" id="userinfo"> 
                  <table width="100%" border="0" cellpadding="4" cellspacing="0">
                    <tr>
                      <td colspan="2" align="right"><a href="#" id="close" onclick="javascript:close_popup();">Close</a></td>
                    </tr>
                    
                    <tr>
                      <td>
                        <p id="list_lpp">
                          lorem ipsum dolor sit amet
                        </p>
                      </td>
                    </tr>
                  </table>
                  </form>
                </div>



		      </div>

		      
		    </div>
		    <!-- / content body -->
		  </div>
		</div>
      
    </div>
</div>