<!-- content -->
<div class="wrapper row2">

  <div id="container_body" class="clear" style="padding:15px 0px 0px 0px;min-height:500px;">
    <!-- content body -->
   
    <div id="col2" style="margin:0px 0px 20px 0px;width:100%">
      <div class="wrapper row2">
		  <div id="container" class="clear">
		    <div id="homepage" class="clear">
		       <div id="daftar" class="box_tengah" style="padding:20px;min-height:200px;height:100%;float:left;">
		       	<h1>Misi LKPP:</h1>
				<br>
		       <p>LKPP bertekad untuk menjadi lembaga kebijakan pengadaan yang berkualitas, memiliki kapabilitas, serta otoritas untuk menghasilkan dan mengembangkan berbagai kebijakan yang dapat mewujudkan sistem pengadaan barang/jasa yang terpercaya di Indonesia.<br><br>

				
				MISI LKPP:

				<dl>
					<li>Mewujudkan aturan yang jelas</li>
					<li>Sistem Evaluasi dan Monitoring yang andal</li>
					<li>Sumber daya manusia yang professional</li>
					<li>Kepastian hukum pengadaan barang/jasa pemerintah</li>
				</dl>

				<br>

				maka dalam kurun waktu 5 (lima) tahun pertama (2010-2014) LKPP bertujuan:  
				<dl>
				<li>Mencegah dan mengurangi Penyimpangan yang terjadi dalam proses Pengadaan Barang/Jasa dalam lingkup institusi pemerintahan.</li>
				<li>Mewujudkan kinerja yang efektif yang pada akhirnya akan menciptakan efisiensi Anggaran Negara yang dipergunakan untuk pengadaan barang/jasa</li>
				<li>Meningkatkan kapasitas SDM pengelola pengadaan barang/jasa, sehingga diharapkan akan terwujud SDM yang 
				menjunjung tinggi semangat profesionalisme dan bermartabat.</li>
				<li>Mewujudkan Kebijakan Nasional tentang Pengadaan Barang/jasa yang jelas, kondusif serta komprehensif.</li>
				<li>Meningkatkan kapasitas kelembagaan LKPP.</li>
				</dl>
				
				</p>
            	
		      </div>

		      
		    </div>
		    <!-- / content body -->
		  </div>
		</div>
      
    </div>
</div>