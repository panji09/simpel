<!-- table sorter -->
    <link rel="stylesheet" href="<?php echo base_url()?>assets/table_sorter/css/jq.css" type="text/css" media="print, projection, screen" />
    <link rel="stylesheet" href="<?php echo base_url()?>assets/table_sorter/themes/blue/style.css" type="text/css" media="print, projection, screen" />
    <!--<script src="<?php echo base_url(); ?>assets/table_sorter/jquery-latest.js"></script> -->
    <script src="<?php echo base_url(); ?>assets/form/js/jquery.js"></script> 

    <script type="text/javascript" src="<?php echo base_url()?>assets/table_sorter/jquery.tablesorter.js"></script>
    <script type="text/javascript" src="<?php echo base_url()?>assets/table_sorter/addons/pager/jquery.tablesorter.pager.js"></script>

    <script src="<?php echo base_url(); ?>assets/form/js/script.js"></script>  
  


    <script type="text/javascript">
    $(function() {
      $("#table1")
        .tablesorter({widthFixed: true, widgets: ['zebra']})
        .tablesorterPager({container: $(".pager1")});

      $("#table2")
        .tablesorter({widthFixed: true, widgets: ['zebra']})
        .tablesorterPager({container: $(".pager2")});

      $("#table3")
        .tablesorter({widthFixed: true, widgets: ['zebra']})
        .tablesorterPager({container: $(".pager3")});
    });
    </script>
<!-- content -->
<div class="wrapper row2">

  <div id="container_body" class="clear" style="padding:15px 0px 0px 0px;min-height:500px;">
    <!-- content body -->
   
    <div id="col2" style="margin:0px 0px 20px 0px;width:100%">
      <div class="wrapper row2">
		  <div id="container" class="clear">
		    <div id="homepage" class="clear">
		       <div class="box_tengah" style="padding:20px;min-height:200px;height:100%;float:left;">
            <form method="POST" action="<?php echo base_url(); ?>data/find">
              <div style="float:right;margin:0px 20px 0px 0px;font-size:10px;color:#6B6B6A;">
              <input type="hidden" name="jenis"  value="terakreditasi">
              <input type="text" placeholder="Pencarian" style="width:200px;height:30px;font-size:19px;" name="keyword">
              <button class="btn btn-primary" style="margin:-8px 0px 0px 0px;height:32px;width:50px;padding:-3px">Cari</button>
              </div>
            </form>
           <h3 style="background:#BC2024;color:#fff;padding:4px;width:250px;text-align:center;">Terakreditasi</h3>
           <hr style="border:1px solid #BC2024;margin-top:-2px;height:1px;">
            <table class="table table-hover table-striped tablesorter" id="table1">
             <thead>
               <tr>
                <th>No</th>
                <th>Nama LPP</th>
                <th>Alamat LPP </th>
                <th style="width:135px;">Email LPP </th>
                <th>No Telp LPP </th>
                <th>Akreditasi </th>
              </tr>
              </thead>
              <tbody>
              <?php $no = 1; foreach ($dataLPP as $data): ?>
                <tr>
                  <td><?php echo $no; ?></td>
                  <td>
                    <?php if($data->website!=null){ echo "<a href = '".$data->website."' target='_blank'>"; } ?>
                      <?php echo $data->nama_lpp; ?>
                    </a>
                  </td>
                  <td><?php echo $data->alamat_lpp; ?></td>
                  <td><?php echo $data->email; ?></td>
                  <td><?php echo $data->kontak_nama." - ".$data->kontak_tlp; ?></td>
                  <td><?php echo $data->hasil_akreditasi; ?></td>
              <?php $no++; endforeach; ?>
              </tbody>
              
            </table>
            <div id="pager" class="pager1">
                <form>
                  <img src="<?php echo base_url()?>assets/table_sorter/addons/pager/icons/first.png" class="first"/>
                  <img src="<?php echo base_url()?>assets/table_sorter/addons/pager/icons/prev.png" class="prev"/>
                  <input type="text" class="pagedisplay"/>
                  <img src="<?php echo base_url()?>assets/table_sorter/addons/pager/icons/next.png" class="next"/>
                  <img src="<?php echo base_url()?>assets/table_sorter/addons/pager/icons/last.png" class="last"/>
                  <select class="pagesize">
                    <option selected="selected"  value="10">10</option>
                    <option value="20">20</option>
                    <option value="30">30</option>
                    <option  value="40">40</option>
                  </select>
                </form>
              </div>

          <br>

          <h3 style="background:#BC2024;color:#fff;padding:4px;width:250px;text-align:center;">Terdaftar</h3>
           <hr style="border:1px solid #BC2024;margin-top:-2px;height:1px;">
           <form method="POST" action="<?php echo base_url(); ?>data/find">
              <div style="float:right;margin:0px 20px 0px 0px;font-size:10px;color:#6B6B6A;">
              <input type="hidden" name="jenis"  value="terdaftar">
              <input type="text" placeholder="Pencarian" style="width:200px;height:30px;font-size:19px;" name="keyword">
              <button class="btn btn-primary" style="margin:-8px 0px 0px 0px;height:32px;width:50px;padding:-3px">Cari</button>
              </div>
            </form>
            <table class="table table-hover table-striped tablesorter" id="table2">
             <thead>
               <tr>
                <th>No</th>
                <th>Nama LPP</th>
                <th>Alamat LPP </th>
                <th style="width:135px;">Email LPP </th>
                <th>No Telp LPP </th>
                <th>Status </th>
              </tr>
              </thead>
              <tbody>
              <?php $no = 1; foreach ($dataLPPTerdaftar as $data): 
              if($data->approve==1){
                $stat = "Terdaftar";
              }
              ?>
                <tr>
                  <td><?php echo $no; ?></td>
                  <td><?php if($data->website!=null){ echo "<a href = '".$data->website."' target='_blank'>"; } ?><?php echo $data->nama_lpp; ?></a></td>
                  <td><?php echo $data->alamat_lpp; ?></td>
                  <td><?php echo $data->email; ?></td>
                  <td><?php echo $data->kontak_nama." - ".$data->kontak_tlp; ?></td>
                  <td><?php echo $stat; ?></td>
              <?php $no++; endforeach; ?>
              </tbody>
              
            </table>
            <div id="pager" class="pager2">
                <form>
                  <img src="<?php echo base_url()?>assets/table_sorter/addons/pager/icons/first.png" class="first"/>
                  <img src="<?php echo base_url()?>assets/table_sorter/addons/pager/icons/prev.png" class="prev"/>
                  <input type="text" class="pagedisplay"/>
                  <img src="<?php echo base_url()?>assets/table_sorter/addons/pager/icons/next.png" class="next"/>
                  <img src="<?php echo base_url()?>assets/table_sorter/addons/pager/icons/last.png" class="last"/>
                  <select class="pagesize">
                    <option selected="selected"  value="10">10</option>
                    <option value="20">20</option>
                    <option value="30">30</option>
                    <option  value="40">40</option>
                  </select>
                </form>
              </div>

              <br>

          <h3 style="background:#BC2024;color:#fff;padding:4px;width:250px;text-align:center;">Verifikasi Pendaftaran</h3>
           <hr style="border:1px solid #BC2024;margin-top:-2px;height:1px;">
           <form method="POST" action="<?php echo base_url(); ?>data/find">
              <div style="float:right;margin:0px 20px 0px 0px;font-size:10px;color:#6B6B6A;">
              <input type="hidden" name="jenis"  value="verifikasi">
              <input type="text" placeholder="Pencarian" style="width:200px;height:30px;font-size:19px;" name="keyword">
              <button class="btn btn-primary" style="margin:-8px 0px 0px 0px;height:32px;width:50px;padding:-3px">Cari</button>
              </div>
            </form>
            <table class="table table-hover table-striped tablesorter" id="table3">
             <thead>
               <tr>
                <th>No</th>
                <th>Nama LPP</th>
                <th>Alamat LPP </th>
                <th style="width:135px;">Email LPP </th>
                <th>No Telp LPP </th>
                <th>Akreditasi </th>
              </tr>
              </thead>
              <tbody>
              <?php 
		  //print_r($dataLPPBelumAkreditasi);
		  $no = 1; foreach ($dataLPPBelumAkreditasi as $data):  
              if($data->approve==1){
                $stat = "Terdaftar";
              }
              ?>
                <tr>
                  <td><?php echo $no; ?></td>
                  <td><?php if($data->website!=null){ echo "<a href = '".$data->website."' target='_blank'>"; } ?><?php echo $data->nama_lpp; ?></a></td>
                  <td><?php echo $data->alamat_lpp; ?></td>
                  <td><?php echo $data->email; ?></td>
                  <td><?php echo $data->kontak_nama."<br>".$data->kontak_tlp; ?></td>
                  <td><?php echo $data->hasil_akreditasi; ?></td>
              <?php $no++; endforeach; ?>
              </tbody>
              
            </table>
            <div id="pager" class="pager3">
                <form>
                  <img src="<?php echo base_url()?>assets/table_sorter/addons/pager/icons/first.png" class="first"/>
                  <img src="<?php echo base_url()?>assets/table_sorter/addons/pager/icons/prev.png" class="prev"/>
                  <input type="text" class="pagedisplay"/>
                  <img src="<?php echo base_url()?>assets/table_sorter/addons/pager/icons/next.png" class="next"/>
                  <img src="<?php echo base_url()?>assets/table_sorter/addons/pager/icons/last.png" class="last"/>
                  <select class="pagesize">
                    <option selected="selected"  value="10">10</option>
                    <option value="20">20</option>
                    <option value="30">30</option>
                    <option  value="40">40</option>
                  </select>
                </form>
              </div>


		      </div>

          

		      
		    </div>
		    <!-- / content body -->
		  </div>
		</div>
      
    </div>
</div>
</div>