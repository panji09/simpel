<!-- content -->
<div class="wrapper row2">

  <div id="container_body" class="clear" style="padding:15px 0px 0px 0px;min-height:500px;">
    <!-- content body -->
   
    <div id="col2" style="margin:0px 0px 20px 0px;width:100%">
      <div class="wrapper row2">
		  <div id="container" class="clear">
		    <div id="homepage" class="clear">
		       <div id="daftar" class="box_tengah" style="padding:20px;min-height:200px;height:100%;float:left;">
		       	<h1>Frequently Asked Question</h1>
				<br>
		       	<p>
		       	  Q: Tujuan Pelatihan pengadaan barang/jasa pemerintah tingkat dasar<br><br>
			      A: pelatihan yang bertujuan untuk memberikan pembekalan keterampilan, pengetahuan dan perilaku kepada peserta latih tentang                     prosedur pengadaan barang/jasa pemerintah, sehingga para peserta memiliki kompetensi untuk melaksanakan tugas sehari-hari                 dalam bidang pengadaan barang dan jasa pemerintah
			      <br><br><br>
			     Q: Standar Pelatihan pengadaan barang/jasa pemerintah tingkat dasar<br><br>
			      A: 
			a.	 	Ruangan pelatihan: ruangan yang memadai untuk menampung 50 orang peserta dan  dilengkapi dengan  pendingin udara/kipas angin<br><br>
			b.	 	Kriteria peserta: pendidikan minimal D3<br><br>
			c.	 	Jumlah peserta: maksimal 50 orang per ruang pelatihan<br><br>
			d.	 	Prasarana pelatihan: LCD proyektor, komputer/laptop, flipchart/papan tulis, spidol, kertas<br><br>
			e.	 	Lama waktu pelatihan: minimal 3 hari, diutamakan 5 hari sesuai standar pelatihan LKPP<br><br>
			f.	 	Kurikulum pelatihan: memakai kurikulum yang sudah diumumkan LKPP di website http://www.lkpp.go.id dan http://simpel.lkpp.go.id<br><br>
			g.	 	Narasumber: narasumber yang sudah memperoleh sertifikat Training of Trainers dari LKPP<br><br>
			h.	 	Bahan/materi pelatihan: minimal Buku Perpres 54 tahun 2010 beserta perubahannya, untuk masing-masing peserta<br><br>
			i.	 	Permintaan sertifikat pelatihan pengadaan barang/jasa pemerintah tingkat dasar wajib melampirkan evaluasi <br>pelatihan dan absensi pelatihan<br><br>
			j.	 	Pencantuman logo LKPP sesuai dengan yang tercantum dalam template sertifika
				</p>
            	
		      </div>

		      
		    </div>
		    <!-- / content body -->
		  </div>
		</div>
      
    </div>
</div>