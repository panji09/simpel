            <?php 
              $status = $this->session->flashdata('status');
              if(isset($status)){ echo $status; } 
            ?>
            <?php foreach ($dataNarasumber as $data): 
            $newname1 = str_replace(" ","_",$data->nama_narasumber);
            $newname2 = str_replace(".","_",$newname1);
            ?>
            <!-- content -->
<div class="wrapper row2">

  <div id="container_body" class="clear" style="padding:15px 0px 0px 0px;min-height:500px;">
    <!-- content body -->
   
    <div id="col2" style="margin:0px 0px 20px 0px;width:100%">
      <div class="wrapper row2">
      <div id="container" class="clear">
        <div id="homepage" class="clear">
           <div id="daftar" class="box_tengah" style="padding:20px;min-height:200px;height:100%;float:left;">

              <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-bar-chart-o"></i>Detail Narasumber</h3>
              </div>

            <!-- <div class="col-lg-2"><br>
            <img src="" width="180px" height="180px">
            </div>   -->
            <div class="col-lg-12"><br>
            <table class="table table-striped">
              <tr>
                <td class="col-lg-3" rowspan="6">
                <img src="<?php echo base_url(); ?>assets/upload/narasumber/<?php echo $data->photo_narasumber; ?>" width="240px" height="260px">
                </td>
              </tr>
              <tr>
                <td>Nama</td>
                <td><label ><?php echo $data->nama_narasumber; ?></label></td>
              </tr>
              <tr>
                <td>Alamat</td>
                <td><label><?php echo $data->alamat_narasumber; ?></label></td>
              </tr><!-- 
              <tr>
                <td>No Telp</td>
                <td><label><?php echo $data->no_telp_narasumber; ?></label></td>
              </tr> -->
              <tr>
                <td>Tempat Lahir</td>
                <td><label><?php echo $data->tempat_lahir_narasumber; ?></label></td>
              </tr>
              <tr>
                <td>Tanggal Lahir</td>
                <td colspan="2"><label><?php echo $data->tanggal_lahir_narasumber; ?></label></td>
              </tr>
              <tr>
                <td>NIP / NIK</td>
                <td colspan="2">
                <label><?php echo $data->nip_narasumber; ?></label>
                </td>
              </tr>
              <tr>
                <td>Gelar</td>
                <td colspan="2">
                <label><?php echo $data->gelar_narasumber; ?></label>
                </td>
              </tr>
              <tr>
                <td>Golongan</td>
                <td colspan="2">
                <label><?php echo $data->golongan_narasumber; ?></label>
                </td>
              </tr>
              <tr>
                <td>Instansi</td>
                <td colspan="2">
                <label><?php echo $data->instansi_narsum; ?></label>
                </td>
              </tr>
              <tr>
                <td>Pangkat</td>
                <td colspan="2">
                <label><?php echo $data->pangkat_narasumber; ?></label>
                </td>
              </tr>
              <tr>
                <td>Satuan Kerja</td>
                <td colspan="2">
                <label><?php echo $data->satker_narasumber; ?></label>
                </td>
              </tr>
              <tr>
                <td>Provinsi</td>
                <td colspan="2">
                <label><?php echo $data->provinsi_narasumber; ?></label>
                </td>
              </tr>
              <tr>
                <td>Kota</td>
                <td colspan="2">
                <label><?php echo $data->kota_narasumber; ?></label>
                </td>
              </tr>
              <tr>
                <td>Email</td>
                <td colspan="2">
                <label><?php echo $data->email_narasumber; ?></label>
                </td>
              </tr> 
              <!-- <tr>
                <td>Username</td>
                <td colspan="2">
                <label><?php echo $data->username; ?></label>
                </td>
              </tr>-->
              <tr>
                <td>Lokasi</td>
                <td colspan="2">
                <label><?php echo $data->lokasi; ?></label>
                </td>
              </tr><!-- 
              <tr>
                <td>Nilai TOT Dasar</td>
                <td colspan="2">
                <label><?php echo $data->nilai_tot_dasar; ?></label>
                </td>
              </tr> -->
              <tr>
                <td>Pelatihan yang pernah di ikuti</td>
                <td colspan="2">
                <label><?php echo $data->tot_yang_diikuti; ?></label>
                </td>
              </tr>
              <tr>
                <td>No Sertifikat TOT Dasar</td>
                <td colspan="2">
                <label><?php echo $data->no_sertifikat_tot_dasar; ?></label>
                </td>
              </tr>
            </table>
            </div> 
            </form>
            <?php endforeach;?>
            </div>
            </div>

        </div>
          </div>

          
        </div>
        <!-- / content body -->
      </div>
    </div>
      
    </div>