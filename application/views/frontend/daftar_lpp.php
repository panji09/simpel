<script type="text/javascript">

$(document).ready(function(){
    var status = $('[name="status"]:checked').val();
    
    function pilih_status(selected_value){
      if(selected_value=="negeri"){
	//alert("asn");
	$('#negeri').show();
	$('#swasta').hide();
      }else{
	//alert("bukan asn");
	$('#negeri').hide();
	$('#swasta').show();
      }
    }
    
    pilih_status(status);
    $('[name="status"]').change(function(){
	
	status = $('[name="status"]:checked').val();
	pilih_status(status);
    });
});
/*
function pilih_status(selected_value){
  if(selected_value=="negeri"){
    //alert("asn");
    $('#negeri').css("display", "block");
    $('#swasta').css("display", "none");
  }else{
    //alert("bukan asn");
    $('#swasta').css("display", "block");
    $('#negeri').css("display", "none");
  }
}
*/
</script>
<!--
<style type="text/css">
  #negeri{
    display: none;
  }
  #swasta{
    display: none;
  }
</style>
-->
<!-- content -->
<div class="wrapper row2">

  <div id="container_body" class="clear" style="padding:15px 0px 0px 0px;">
    <!-- content body -->
   
    <div id="col2" style="margin:0px 0px 20px 0px;width:100%">
      <div class="wrapper row2">
		  <div id="container" class="clear">
		    <div id="homepage" class="clear">
		      <div id="daftar" class="box_tengah">
		      <div class="header" align="center" style="font-weight:bold;padding:18px 0px;font-size:1.2em;">
		      		FORM PENDAFTARAN<br>
					LEMBAGA PENYELENGGARA PELATIHAN (LPP)
					</div>
			      <div class="isi">
				      <?php 
			              $status = $this->session->flashdata('status');
			              $form = $this->session->flashdata('form');
			              
			              if(isset($status)){ echo $status;} 
			              ?>
			              Kolom dengan tanda bintang (*) Wajib diisi
			              <div class="panel-body">
			                <?php echo form_open_multipart(base_url().'daftar_lpp/submit'); ?>
				                <div class="form-group">
				                <label>Status Kelembagaan*</label>
					                  <input name="status" class='status' type="radio" value="negeri" <?=(isset($form['status']) && $form['status']=='negeri' ? 'checked' : 'checked')?>>Negeri
					                  <input name="status" class='status' type="radio" value="swasta" <?=(isset($form['status']) && $form['status']=='swasta' ? 'checked' : '')?>>Swasta
					            </div>
			                	<input type="hidden" name="from" value="home">
					             <div class="form-group">
					                <label>Nama Lembaga*</label>
					                <input class="form-control" type="text" id="nama_lpp" name="nama_lpp" value="<?=(isset($form['nama_lpp']) ? $form['nama_lpp'] : '')?>">
					            </div>
					            <div class="form-group">
					                <label>Penanggungjawab*</label>
					                <input class="form-control" type="text" id="nama_lpp" name="penanggungjawab" value="<?=(isset($form['penanggungjawab']) ? $form['penanggungjawab'] : '')?>">
					            </div>
					            <div class="form-group">
					                <label>Jabatan*</label>
					                <input class="form-control" type="text" id="nama_lpp" name="jabatan" value="<?=(isset($form['jabatan']) ? $form['jabatan'] : '')?>">
					            </div>
					            <div class="form-group">
					                <label>No KTP*</label>
					                <input class="form-control" type="text" id="nama_lpp" name="no_ktp" value="<?=(isset($form['no_ktp']) ? $form['no_ktp'] : '')?>">
					            </div>
					            <!-- <div class="form-group">
					                <label>Program*</label> -->
					                <input class="form-control" type="hidden" id="nama_lpp" name="program" value="pelatihan tingkat dasar" value="<?=(isset($form['program']) ? $form['program'] : '')?>">
					            <!-- </div> -->
					            <div class="form-group">
					                <label>Alamat/Sekertariat*</label>
					                <input class="form-control" type="text" id="alamat_lpp" name="alamat_lpp" value="<?=(isset($form['alamat_lpp']) ? $form['alamat_lpp'] : '')?>">
					            </div>
					            <div class="form-group">
					                <label>No Telp*</label>
					                <input class="form-control" type="text" id="no_telp_lpp" name="no_telp_lpp" value="<?=(isset($form['no_telp_lpp']) ? $form['no_telp_lpp'] : '')?>">
					            </div>
					            <div class="form-group">
					                <label>No Fax*</label>
					                <input class="form-control" type="text" id="nama_lpp" name="no_fax" value="<?=(isset($form['no_fax']) ? $form['no_fax'] : '')?>">
					            </div>
					            <div class="form-group">
					                <label>Email / Username*</label>
					                <input class="form-control" type="text" id="nama_lpp" name="email" value="<?=(isset($form['email']) ? $form['email'] : '')?>">
					            </div>
					            <div class="form-group">
					                <label>Password*</label>
					                <input class="form-control" type="password" id="nama_lpp" name="password">
					            </div>
					            <div class="form-group">
					                <label>Konfirmasi Password*</label>
					                <input class="form-control" type="password" id="nama_lpp" name="konfirmasi_password">
					            </div>

					            <div class="form-group" style="margin:20px 0px;padding:20px;">
					            	<div class="form-group">
						                <label>Kontak Person*</label>
						            </div>
						            <div class="form-group" style="margin:10px;">
						                <label>Nama*</label>
						                <input class="form-control" type="text" id="nama_lpp" name="kontak_nama" value="<?=(isset($form['kontak_nama']) ? $form['kontak_nama'] : '')?>">
						            </div>
						            <div class="form-group" style="margin:10px;">
						                <label>Telp/HP*</label>
						                <input class="form-control" type="text" id="nama_lpp" name="kontak_telp" value="<?=(isset($form['kontak_telp']) ? $form['kontak_telp'] : '')?>">
						            </div>
						            <div class="form-group" style="margin:10px;">
						                <label>Email*</label>
						                <input class="form-control" type="text" id="nama_lpp" name="kontak_email" value="<?=(isset($form['kontak_email']) ? $form['kontak_email'] : '')?>">
						            </div>
					            </div>
					            <div class="form-group">
							                <label>Harap Melampirkan Dokumen Sebagai Berikut</label>
							                <em>file yang perbolehkan (jpg, png, pdf, doc, docx, xls, xlsx) dan maksimum size 10MB</em>
							            </div>
					            <div class="form-group" style="margin:20px 0px;padding:20px;">
					            	<div class="form-group" id="swasta">
							            <div class="form-group" style="margin:10px;">
							                <label>Lampiran KTP*</label>
							                <input class="form-control" type="file" id="nama_lpp" name="files[]" >
							            </div>
					            		<div class="form-group" style="margin:10px;">
							                <label>	Surat Ijin Operasional* </label>
							                <input class="form-control" type="file" id="nama_lpp" name="files[]">
							            </div>
							            <div class="form-group" style="margin:10px;">
							                <label>	Surat Keterangan domisili* </label>
							                <input class="form-control" type="file" id="nama_lpp" name="files[]">
							            </div>
							            <div class="form-group" style="margin:10px;">
							                <label>	Akta Notaris* </label>
							                <input class="form-control" type="file" id="nama_lpp" name="files[]">
							            </div>
							            <div class="form-group" style="margin:10px;">
							                <label>	Sistem Manajemen Mutu (SOP)* </label>
							                <input class="form-control" type="file" id="nama_lpp" name="files[]">
							            </div>
							            <div class="form-group" style="margin:10px;">
							                <label>	Surat Pernyataan Komitmen*  </label>
							                <input class="form-control" type="file" id="nama_lpp" name="files[]">
							            </div>
							        </div>
							        <div class="form-group" id="negeri">
							            <div class="form-group" style="margin:10px;">
							                <label>Lampiran KTP*</label>
							                <input class="form-control" type="file" id="nama_lpp" name="files2[]" >
							            </div>
							            <div class="form-group" style="margin:10px;">
							                <label>	Mekanisme Keuangan* </label>
							                <input class="form-control" type="file" id="nama_lpp" name="files2[]">
							            </div>
							            <div class="form-group" style="margin:10px;">
							                <label>	Sistem Manajemen Mutu (SOP)*  </label>
							                <input class="form-control" type="file" id="nama_lpp" name="files2[]">
							            </div>
							            <div class="form-group" style="margin:10px;">
							                <label>	Surat Pernyataan Komitmen*  </label>
							                <input class="form-control" type="file" id="nama_lpp" name="files2[]">
							            </div>
							            <div class="form-group" style="margin:10px;">
							                <label>	Tugas dan Fungsi Pelatihan* </label>
							                <input class="form-control" type="file" id="nama_lpp" name="files2[]">
							            </div>
						            </div>

					            </div>

					            <!-- <div class="form-group">
					                <label>    Khusus Lembaga Swasta dan Perguruan Tinggi Negeri/Swasta</label><br>
					                <label>  Khusus Instansi Pemerintah/ Perguruan Tinggi Negeri </label>
					            </div> -->
					            <!-- <div class="form-group">
					                <label>Username*</label>
					                <input class="form-control" type="text" id="username" name="username">
					            </div>
					            <div class="form-group">
					                <label>Password*</label>
					                <input class="form-control" type="password" id="password" name="password">
					            </div>
					            <div class="form-group">
					                <label>No Akreditasi LPP*</label>
					                <input class="form-control" type="text" id="no_akreditasi_lpp" name="no_akreditasi_lpp">
					            </div>
					            <div class="form-group">
					                <label>Hasil Akreditasi*</label>
					                <input class="form-control" type="text" id="hasil_akreditasi" name="hasil_akreditasi">
					            </div>
					            <div class="form-group">
					                <label>Satker*</label>
					                <input type="radio" name="satker" value="1">iya
					                <input type="radio" name="satker" value="0">bukan
					            </div>
					            <div class="form-group">
					                <label>File Akreditasi*</label>
					                <input class="form-control" type="text" id="file_akreditasi" name="file_akreditasi">
					            </div> -->
					            <input type="submit" class="btn btn-primary" name="submit" value="Daftar">
					            <!-- <button type="submit" class="btn btn-danger" name="submit" value="cancel">Tolak Request</button> -->
			                </form>
			              </div>

				  </div>
		      </div>

		      
		    </div>
		    <!-- / content body -->
		  </div>
		</div>
      
    </div>
</div>