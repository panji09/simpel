<!-- table sorter -->
    <link rel="stylesheet" href="<?php echo base_url()?>assets/table_sorter/css/jq.css" type="text/css" media="print, projection, screen" />
    <link rel="stylesheet" href="<?php echo base_url()?>assets/table_sorter/themes/blue/style.css" type="text/css" media="print, projection, screen" />
    <!--<script src="<?php echo base_url(); ?>assets/table_sorter/jquery-latest.js"></script> -->
    <script src="<?php echo base_url(); ?>assets/form/js/jquery.js"></script> 

    <script type="text/javascript" src="<?php echo base_url()?>assets/table_sorter/jquery.tablesorter.js"></script>
    <script type="text/javascript" src="<?php echo base_url()?>assets/table_sorter/addons/pager/jquery.tablesorter.pager.js"></script>

    <script src="<?php echo base_url(); ?>assets/form/js/script.js"></script>  
  


    <script type="text/javascript">
    $(function() {
      $("#table1")
        .tablesorter({widthFixed: true, widgets: ['zebra']})
        .tablesorterPager({container: $(".pager1")});

      $("#table2")
        .tablesorter({widthFixed: true, widgets: ['zebra']})
        .tablesorterPager({container: $(".pager2")});

      $("#table3")
        .tablesorter({widthFixed: true, widgets: ['zebra']})
        .tablesorterPager({container: $(".pager3")});
    });
    </script>
<!-- content -->
<div class="wrapper row2">

  <div id="container_body" class="clear" style="padding:15px 0px 0px 0px;min-height:500px;">
    <!-- content body -->
   
    <div id="col2" style="margin:0px 0px 20px 0px;width:100%">
      <div class="wrapper row2">
		  <div id="container" class="clear">
		    <div id="homepage" class="clear">
		       

          <h3 style="background:#BC2024;color:#fff;padding:4px;width:250px;text-align:center;font-size:14px;">Narasumber berdasarkan Instansi</h3>
           <hr style="border:1px solid #BC2024;margin-top:-2px;height:1px;">
            <table class="table table-hover table-striped tablesorter" id="table2">
             <thead>
               <tr>
                <th>No</th>
                <th style="width:700px;">Nama Instansi</th>
                <th>Jumlah Narasumber</th>
              </tr>
              </thead>
              <tbody>
              <?php $no = 1; foreach ($narsum_berdasarkan_instansi as $data): ?>
                <tr>
                  <td><?php echo $no; ?></td>
                  <td><?php echo $data->instansi_narsum; ?></td>
                  <td><?php echo $data->jumlah_narasumber; ?></td>
              <?php $no++; endforeach; ?>
              </tbody>
              
            </table>
            <div id="pager" class="pager2">
                <form>
                  <img src="<?php echo base_url()?>assets/table_sorter/addons/pager/icons/first.png" class="first"/>
                  <img src="<?php echo base_url()?>assets/table_sorter/addons/pager/icons/prev.png" class="prev"/>
                  <input type="text" class="pagedisplay"/>
                  <img src="<?php echo base_url()?>assets/table_sorter/addons/pager/icons/next.png" class="next"/>
                  <img src="<?php echo base_url()?>assets/table_sorter/addons/pager/icons/last.png" class="last"/>
                  <select class="pagesize">
                    <option selected="selected"  value="10">10</option>
                    <option value="20">20</option>
                    <option value="30">30</option>
                    <option  value="40">40</option>
                  </select>
                </form>
              </div>

              <br>

          <h3 style="background:#BC2024;color:#fff;padding:4px;width:250px;text-align:center;font-size:14px;">Narasumber berdasarkan Lokasi</h3>
           <hr style="border:1px solid #BC2024;margin-top:-2px;height:1px;">
            <table class="table table-hover table-striped tablesorter" id="table3">
             <thead>
               <tr>
                <th>No</th>
                <th style="width:700px;">Lokasi</th>
                <th>Jumlah Narasumber</th>
              </tr>
              </thead>
              <tbody>
              <?php $no = 1; foreach ($narsum_berdasarkan_lokasi as $data): ?>
                <tr>
                  <td><?php echo $no; ?></td>
                  <td><?php echo $data->lokasi; ?></td>
                  <td><?php echo $data->jumlah_narasumber; ?></td>
              <?php $no++; endforeach; ?>
              </tbody>
              
            </table>
            <div id="pager" class="pager3">
                <form>
                  <img src="<?php echo base_url()?>assets/table_sorter/addons/pager/icons/first.png" class="first"/>
                  <img src="<?php echo base_url()?>assets/table_sorter/addons/pager/icons/prev.png" class="prev"/>
                  <input type="text" class="pagedisplay"/>
                  <img src="<?php echo base_url()?>assets/table_sorter/addons/pager/icons/next.png" class="next"/>
                  <img src="<?php echo base_url()?>assets/table_sorter/addons/pager/icons/last.png" class="last"/>
                  <select class="pagesize">
                    <option selected="selected"  value="10">10</option>
                    <option value="20">20</option>
                    <option value="30">30</option>
                    <option  value="40">40</option>
                  </select>
                </form>
              </div>

              <br>

          <h3 style="background:#BC2024;color:#fff;padding:4px;width:250px;text-align:center;font-size:14px;">Narasumber berdasarkan Gender</h3>
           <hr style="border:1px solid #BC2024;margin-top:-2px;height:1px;">
            <table class="table table-hover table-striped tablesorter" id="table1">
             <thead>
               <tr>
                <th>No</th>
                <th style="width:700px;">Gender</th>
                <th>Jumlah Narasumber</th>
              </tr>
              </thead>
              <tbody>
              <?php $no = 1; foreach ($narsum_berdasarkan_gender as $data): ?>
                <tr>
                  <td><?php echo $no; ?></td>
                  <td><?php echo $data->jeniskel_narasumber; ?></td>
                  <td><?php echo $data->jumlah_narasumber; ?></td>
              <?php $no++; endforeach; ?>
              </tbody>
              
            </table>


              <br>

          <h3 style="background:#BC2024;color:#fff;padding:4px;width:250px;text-align:center;font-size:14px;">Jumlah Pelatihan berdasarkan LPP</h3>
           <hr style="border:1px solid #BC2024;margin-top:-2px;height:1px;">
            <table class="table table-hover table-striped tablesorter" id="table4">
             <thead>
               <tr>
                <th>No</th>
                <th style="width:700px;">Nama LPP</th>
                <th>Jumlah Pelatihan </th>
              </tr>
              </thead>
              <tbody>
              <?php $no = 1; foreach ($pelatihan_berdasarkan_lpp as $data):
              ?>
                <tr>
                  <td><?php echo $no; ?></td>
                  <td><?php echo $data->nama_lpp; ?></td>
                  <td><?php echo $data->jumlah_pelatihan; ?></td>
              <?php $no++; endforeach; ?>
              </tbody>
              
            </table>
            <div id="pager" class="pager1">
                <form>
                  <img src="<?php echo base_url()?>assets/table_sorter/addons/pager/icons/first.png" class="first"/>
                  <img src="<?php echo base_url()?>assets/table_sorter/addons/pager/icons/prev.png" class="prev"/>
                  <input type="text" class="pagedisplay"/>
                  <img src="<?php echo base_url()?>assets/table_sorter/addons/pager/icons/next.png" class="next"/>
                  <img src="<?php echo base_url()?>assets/table_sorter/addons/pager/icons/last.png" class="last"/>
                  <select class="pagesize">
                    <option selected="selected"  value="10">10</option>
                    <option value="20">20</option>
                    <option value="30">30</option>
                    <option  value="40">40</option>
                  </select>
                </form>
              </div>

		      </div>

          

		      
		    </div>
		    <!-- / content body -->
		  </div>
		</div>
      
    </div>
</div>
</div>