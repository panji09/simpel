<!-- Footer -->
<style type="text/css">
    #foot_link{
        
    }
</style>
<div class="wrapper row3" id="footer">
 <footer class="clear">
    <div  style="margin:0px auto;width:80%;">
    <div class="footer_coloumn">
        <span class="head">Tentang LKPP</span>
        <dl class="foot_link" id="foot_link">
            <li><a href="<?php echo base_url()."profil/visi"; ?> " style="color:#fff;"> Visi dan Misi </a></li>
            <li><a href="<?php echo base_url()."profil/maklumat"; ?>" style="color:#fff;"> Maklumat Pelayanan </a></li>
            <li><a href="<?php echo base_url()."profil/standar_pelayanan"; ?>" style="color:#fff;"> Standar Pelayanan </a></li>
            <li><a href="<?php echo base_url()."profil/layanan"; ?>" style="color:#fff;"> Layanan </a></li>
        </dl>
    </div>
    <div class="footer_coloumn">
        <span class="head">Data</span>
        <dl class="foot_link" id="foot_link">
            <li><a href="<?php echo base_url()."data/lpp"; ?> " style="color:#fff;">Data LPP </a></li>
            <li><a href="<?php echo base_url()."data/narasumber"; ?> " style="color:#fff;">Data Narasumber </a></li>
        </dl>
    </div>
    <div class="footer_coloumn">
        <span class="head">Layanan Publik</span>
        <dl class="foot_link" id="foot_link">
            <li><a href="<?php echo base_url()."daftar_lpp"; ?> " style="color:#fff;">Pendaftaran LPP </a></li>
            <li><a href="<?php echo base_url()."unduh"; ?> " style="color:#fff;">Unduh Dokumen Pelatihan </a></li>
            <li><a href="<?php echo base_url()."regulasi"; ?> " style="color:#fff;">Unduh Dokumen Regulasi </a></li>
            <li><a href="<?php echo base_url()."faq"; ?> " style="color:#fff;">FAQ </a></li>
        </dl>
    </div>
    <div class="footer_coloumn">
        <span class="head">Info Kontak</span><br><br><br>
        <p>
            SME Tower Lt. 7 <br>
            Jln. Gatot Subroto Kav 94 <br> 
            Jakarta - 12780 <br>
            Email    :  simpel@lkpp.go.id <br>
            Telpon   : (021) 7989374<br>
            Fax      : (021) 7989374<br>
            Helpdesk :  081284870255, 081284870266
        </p>
    </div>



    </div>
  </footer>
  <footer id="footer" class="clear">
    
    <!-- <p class="fl_right">Developed by  <a href="http://www.twitter.com/abang_adit" >abang_adit</a></p> -->
  </footer>
</div>

<div id="footer_red"><div  style="margin:0px auto;width:80%;"><p class="fl_left">Dikembangkan oleh Direktorat Pelatihan Kompetensi Deputi PPSDM - Direktorat Pelatihan Kompetensi. Ver. 1.0 ©Copyright 2014 LKPP. ALL rights reserved.</p></div></div>
</body>


    <!-- JavaScript
    <script src="<?php echo base_url(); ?>assets/js/jquery-1.10.2.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/bootstrap.js"></script> -->
</html>