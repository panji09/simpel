<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
<title>Sistem Informasi Pelatihan</title>
<meta charset="iso-8859-1">
<!-- Bootstrap core CSS -->
<link href="<?php echo base_url(); ?>assets/css/bootstrap.css" rel="stylesheet">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/design/styles/layout.css" type="text/css">
<!--[if lt IE 9]><script src="scripts/html5shiv.js"></script><![endif]-->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/slider/style.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js" ></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.5.3/jquery-ui.min.js" ></script>
<link href="<?php echo base_url(); ?>assets/form/css/style.css" rel="stylesheet">
<style type="text/css">
  ul {   font-family: Arial, Verdana;   font-size: 14px;     padding: 0;   list-style: none; z-index: 99; } ul li {  margin-top: -14px;  display: block;    float: left; z-index: 99;} li ul { display: none; z-index: 99;} ul li a {   display: block;   text-decoration: none;   color: #ffffff;    white-space: nowrap; } ul li a:hover { background: #617F8A; } li:hover ul {   display: block;   position: absolute; } li:hover li {   float: none;   font-size: 11px; } li:hover a { background: #617F8A; } li:hover li a:hover { background: #95A9B1; }
</style>
<script type="text/javascript">
  $(document).ready(function(){
    $("#featured > ul").tabs({fx:{opacity: "toggle"}}).tabs("rotate", 5000, true);
  });
</script>

<!-- colorbox -->

<link rel="stylesheet" href="<?php echo base_url(); ?>assets/fancybox/colorbox.css" />
    <script src="<?php echo base_url(); ?>assets/fancybox/jquery.colorbox.js"></script>
    <script>
      $(document).ready(function(){
        //Examples of how to assign the Colorbox event to elements
        $(".group1").colorbox({rel:'group1'});
        $(".group2").colorbox({rel:'group2', transition:"fade"});
        $(".group3").colorbox({rel:'group3', transition:"none", width:"75%", height:"75%"});
        $(".group4").colorbox({rel:'group4', slideshow:true});
        $(".ajax").colorbox();
        $(".youtube").colorbox({iframe:true, innerWidth:640, innerHeight:390});
        $(".vimeo").colorbox({iframe:true, innerWidth:500, innerHeight:409});
        $(".iframe").colorbox({iframe:true, width:"80%", height:"80%"});
        $(".inline").colorbox({inline:true, width:"50%"});
        $(".callbacks").colorbox({
          onOpen:function(){ alert('onOpen: colorbox is about to open'); },
          onLoad:function(){ alert('onLoad: colorbox has started to load the targeted content'); },
          onComplete:function(){ alert('onComplete: colorbox has displayed the loaded content'); },
          onCleanup:function(){ alert('onCleanup: colorbox has begun the close process'); },
          onClosed:function(){ alert('onClosed: colorbox has completely closed'); }
        });

        $('.non-retina').colorbox({rel:'group5', transition:'none'})
        $('.retina').colorbox({rel:'group5', transition:'none', retinaImage:true, retinaUrl:true});
        
        //Example of preserving a JavaScript event for inline calls.
        $("#click").click(function(){ 
          $('#click').css({"background-color":"#f00", "color":"#fff", "cursor":"inherit"}).text("Open this window again and this message will still be here.");
          return false;
        });
      });
    </script>

    <script type="text/javascript" src="<?php echo base_url(); ?>assets/slide/images/jquery-1.4.2.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/slide/images/coin-slider.min.js"></script>
    
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/slide/images/coin-slider-styles.css" type="text/css" />
    <script>$(document).ready(function() {
      $('#games').coinslider({ hoverPause: false });
    });
    </script>

</head>
<body>
<!-- <div id="header_bg" class=""></div> -->
  <header id="header_abu" class="head">
    <div class="tengah"> <a href="#"><img src="<?php echo base_url(); ?>assets/design/images/lkpp.png" width="220px;"></a>
    <span style="font-family: Helvetica, Arial, Sans-Serif;float:right;margin:-5px 20px 0px 0px;font-size:35px;color:#000;font-weight:900;">SIMPEL</span><br>
    <span style="float:right;margin:-20px 20px 0px 0px;font-size:10px;color:#6B6B6A;">Sistem Informasi Manajemen Pelatihan</span>

    <form method="POST" action="<?php echo base_url(); ?>berita/find">
      <div style="float:right;margin:0px 20px 0px 0px;font-size:10px;color:#6B6B6A;">
      <input type="text" placeholder="Pencarian" style="width:200px;height:34px;font-size:19px;" name="keyword">
      <button class="btn btn-primary" style="margin:-8px 0px 0px 0px;height:32px;width:50px;padding:-3px">Cari</button>
      </div>
    </form>
    </div>

  </header>

<div id="header_atas">
<!-- 


<div class="wrapper row2">
    <section id="slider"><a href="#"><img src="<?php echo base_url(); ?>assets/design/images/header.png" alt="" style="height:220px;width:100%;"></a></section>
  
</div> -->

<div class="wrapper row1">
  <header id="header" class="head">
    <nav>
      <ul>
        <li id="active"><a href="<?php echo base_url() ?>">Home</a></li>
        <li><a href="#">Profil<img src="<?php echo base_url()."assets/panah_bawah.png"; ?>" width="12"></a>
        <ul>
          <a href="<?php echo base_url() ?>profil/visi"><li>Visi dan Misi</li></a>
          <a href="<?php echo base_url() ?>profil/maklumat"><li>Maklumat Pelayanan</li></a>
          <a href="<?php echo base_url() ?>profil/standar_pelayanan"><li>Standar Layanan</li></a>
          <a href="<?php echo base_url() ?>profil/layanan"><li>Program Pelatihan </li></a>
        </ul>
        </li>
        <li><a href="<?php echo base_url() ?>jadwal">Jadwal Pelatihan<img src="<?php echo base_url()."assets/panah_bawah.png"; ?>" width="12"></a>
        <ul>
          <a href="<?php echo base_url() ?>statistik"><li>Statistik</li></a>
        </ul>
        </li>
        <!-- <li><a href="<?php echo base_url() ?>regulasi">Regulasi</a></li> -->
        <!-- <li><a href="<?php echo base_url() ?>layanan">Layanan</a></li> -->
        <li><a href="#">Data <img src="<?php echo base_url()."assets/panah_bawah.png"; ?>" width="12"></a>
        <ul>
          <a href="<?php echo base_url() ?>data/lpp"><li>Data LPP</li></a>
          <a href="<?php echo base_url() ?>data/narasumber"><li>Data Narasumber</li></a>
        </ul>
        </li>
        <li><a href="<?php echo base_url() ?>daftar_lpp">Pendaftaran LPP</a></li>
        <li><a href="<?php echo base_url() ?>faq">FAQ</a></li>
        <li><a href="#">Peraturan <img src="<?php echo base_url()."assets/panah_bawah.png"; ?>" width="12"></a>
            <ul>
              <a href="<?php echo base_url() ?>unduh"><li>Pelatihan</li></a>
              <a href="<?php echo base_url() ?>regulasi"><li>Regulasi</li></a>
            </ul>
        </li>
        <li><a href="<?php echo base_url() ?>galeri">Galeri Foto</a></li>
        <li><a href="<?php echo base_url() ?>kontak">Kontak Kami</a></li>
      </ul>
    </nav>
  </header>

  
</div>
</div>
<div id="headline_news">
  
      <marquee>
      <?php foreach ($headerPengumuman as $data): ?>
        <?php echo "<a href=\"".base_url()."pengumuman/detail/".$data->id_pengumuman."\">".tgl_indo($data->tgl_pengumuman).",".$data->judul_pengumuman." &nbsp;&nbsp;&nbsp;&nbsp; - &nbsp;&nbsp;&nbsp;&nbsp; "; ?>
        <?php endforeach; ?>
      </marquee>
  
  </div>
