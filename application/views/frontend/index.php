<!-- content -->
<div class="wrapper row2">

  <div id="container_body" class="clear">
  
   	<div id="page" >
	  		<div id="gamesHolder" >
			<div id="games" >
				<?php $no =1; foreach ($headerBerita as $data): ?>
				<a href="<?php echo base_url()."berita/detail/".$data->id_berita; ?>">
					<img src="<?php echo base_url()."assets/upload/berita/".$data->photo_berita; ?>"/>
					<span>
						<b><?php echo $data->judul_berita; ?></b><br />
						<?php 
	                            $string = $data->isi_berita;
	                            $string = strip_tags($string);

	                    if (strlen($string) > 120) {

	                        // truncate string
	                        $stringCut = substr($string, 0, 120);

	                        // make sure it ends in a word so assassinate doesn't become ass...
	                        $string = substr($stringCut, 0, strrpos($stringCut, ' ')).'...'; 
	                    }
	                    echo $string; 
	                  ?>
					</span>
				</a>
				<?php endforeach; ?>
				
			</div>
	  </div>
	</div>	

    <div id="col1">
    <div class="header">Pengumuman</div>
      <div id="pengumuman" class="box">
      	
	      <div class="isi">
		      	
		      	<ul>
		      	<?php foreach ($dataPengumuman as $data): ?>
		      		<a href="<?php echo base_url(); ?>pengumuman/detail/<?php echo $data->id_pengumuman; ?>">
		      		<li>
		      			<span class="tgl_pengumuman"><?php echo tgl_indo($data->tgl_pengumuman); ?> </span>
		      			<span class="isi_pengumuman">
			      			
			      				<?php echo $data->judul_pengumuman; ?>
			      			
		      			</span>
		      		</li>
		      		</a>
		      	<?php endforeach; ?>
		      	</ul>

		  </div>
      </div>

      <!-- <img src="<?php echo base_url(); ?>assets/design/<?php echo base_url(); ?>assets/slide/images/elearning.jpg" class="box">
      <br><br>
      <img src="<?php echo base_url(); ?>assets/design/<?php echo base_url(); ?>assets/slide/images/rl.jpg" height="150" class="box">
 -->      
      
      <div id="login_area" class="box">
      <div class="header">Login</div>
	      <div class="isi">
	      	 <?php echo form_open('admin/login') ?>
		      <div>
		        
		        <?php 
		        $options = array(
		          '0'  => 'Login Sebagai',
		          '1'    => 'lpp',
		          '3' => 'narasumber',
		          '4' => 'fasilitator',
		        );
		        $role = array('small', 'large');
		        echo "Username : ".form_input('data[username]'); 
		        echo "<br>Password &nbsp;: ".form_password('data[password]')."<br><br>"; 
		        echo form_dropdown('data[role]', $options);
		        ?>
		        <br>
		        <br>
		        <!--<?php//echo form_submit('login', 'Login');?>
		        <input type="text" class="input-block-level" placeholder="Username" name="username">
		        <input type="password" class="input-block-level" placeholder="Password" name="password">-->
		        <input class="btn btn-large btn-primary" type="submit" name="login" value="Login">
		      </div>
		      <?php echo form_close();?>
		    </div>
      </div>
      <!-- <div id="video" class="box">
      <div class="header">Video</div>
      	<iframe width="225" height="220"  src="//www.youtube.com/embed/jgaY-AHaXgE" frameborder="0" allowfullscreen></iframe>
      </div> -->
    </div>
    <div id="col2" class="box">
      <div class="wrapper row2">
      <div class="header">Berita Terbaru</div>
		  <div id="container" class="clear">
		    <div id="homepage" class="clear">
		      <section id="services" class="clear">
		        <?php foreach ($dataBerita as $data): ?>
		        <!-- article 1 -->
		        <article class="two_third">
		         
		          <small style="font-size:11px;color:#6F6F6F;font-style:italic;"><?php echo nama_hari($data->tgl_berita).','.tgl_indo($data->tgl_berita); ?></small>
		          <img src="<?php echo base_url()."/assets/upload/berita/".$data->photo_berita; ?>" alt="" style="width:78px;height:78px;">
		           <h2><a href="<?php echo base_url()."berita/detail/".$data->id_berita; ?>"> <?php echo $data->judul_berita; ?> </a></h2>
		          <!-- <p><?php 
                            $string = $data->isi_berita;
                            $string = strip_tags($string);

                    if (strlen($string) > 220) {

                        // truncate string
                        $stringCut = substr($string, 0, 220);

                        // make sure it ends in a word so assassinate doesn't become ass...
                        $string = substr($stringCut, 0, strrpos($stringCut, ' ')).'... <footer class=\"more\"><a class=\"read_more\" href="'.base_url().'berita/detail/'.$data->id_berita.'">read more</a></footer>'; 
                    }
                    echo $string; 
                  ?></p> -->
		        </article>
		        <?php endforeach; ?>
		        
		<a href="<?php echo base_url(); ?>berita"><input class="btn btn-large btn-danger" type="submit" name="login" value="See All Berita"></a>
		      </section>

		      
		    </div>
		    <!-- / content body -->

		  </div>
		</div>
      
    </div>
    <div id="col3">
    <div id="daftar" class="box">
      <div class="header">Pendaftaran </div>
	      <div class="isi">
		      	<p style="">
		      		Klik disini untuk pendaftaran LPP <br>
		      	</p>
		      	<a href="<?php echo base_url(); ?>daftar_lpp"><input style="width:100%;" class="btn btn-large btn-primary" type="submit" name="login" value="Daftar"></a>
		  </div>
      </div><br><Br>
<div id="daftar" class="box">
      <div class="header">Galeri Foto </div>
	      <div class="isi">
		      	<?php $no =1; foreach ($dataFoto as $data): 
		      	$photo = str_replace(".", "_thumb.", $data->file_foto);
		      	?>
		  		<a class="group1" href="<?php echo base_url()."assets/upload/galeri/".$data->file_foto; ?>" title="<?php echo base_url().$data->judul_foto; ?>"><img src="<?php echo base_url()."assets/upload/galeri/".$photo; ?>" height="150"></a>
			<?php endforeach; ?><br><br>
			<a href="<?php echo base_url()."galeri/"; ?>"><button type="submit" class="btn btn-primary" name="submit" value="kirim">Lihat Semua Foto</button></a>
		  </div>
      </div><br><Br>
      	<div class="header">Link Terkait</div>
    
      <div class="box" id="link">
      	<a href="http://inaproc.lkpp.go.id/v3/" target="_blank"> <img class="link_image" src="http://www.lkpp.go.id/v3/files/apps/inaproc.png" width="75px;" height="75px;" style="margin:0px 15px;">	</a>

      	<a href="http://konsultasi.lkpp.go.id/" target="_blank"><img class="link_image" src="http://www.lkpp.go.id/v3/files/apps/LKPP.jpg" width="75px;" height="75px;" style="margin:0px 15px;">	</a>
      </div>
      <div class="box" id="link">
      	<a href="http://lpse.lkpp.go.id/" target="_blank"><img class="link_image" src="http://www.lkpp.go.id/v3/files/apps/lpse.png" width="75px;" height="75px;" style="margin:0px 15px;">	</a>

      	<a href="http://eproc.lkpp.go.id/" target="_blank"><img class="link_image" src="http://www.lkpp.go.id/v3/files/apps/MT%20LPSE%20Edvance%20banner%20web%20380%20x%20690px%20rev.jpg" width="75px;" height="75px;" style="margin:0px 15px;">	</a>

      </div>
      <div class="box" id="link">
      	<a href="http://pak.lkpp.go.id/" target="_blank"><img class="link_image" src="http://www.lkpp.go.id/v3/files/apps/icon-sijabfung-new.png" width="75px;" height="75px;" style="margin:0px 15px;">	</a>

      	<a href="http://e-learning.lkpp.go.id/" target="_blank"><img class="link_image" src="http://www.lkpp.go.id/v3/files/apps/ikon%20e-learning%202.png" width="75px;" height="75px;" style="margin:0px 15px;">	</a>
      </div>
      <div class="box" id="link">
      	<a href="http://sertifikasi.lkpp.go.id/" target="_blank"><img class="link_image" src="http://www.lkpp.go.id/v3/files/apps/Sertifikasi_icon.png" width="75px;" height="75px;" style="margin:0px 15px;">	</a>
      </div>
	 
	  

      

      

      <!-- <div id="visimisi" class="box">
      <div class="header">Visi & Misi</div>
      <div id="isi_visi_misi">
      	<h1>Visi</h1>
      	Andal dalam
		mewujudkan
		sistem
		pengadaan yang
		kredibel
		Tujuan
		<br>
		<br>
		<h1>Visi& Misi</h1>
		Mewujudkan aturan yang
		jelas, sistem monitoring
		dan evaluasi yang andal,
		sumber daya manusia
		yang profesional, dan
		kepastian hukum
		pengadaan barang/
		jasa pemerintah
		<br>
		<br>
		<h1>Tujuan</h1>
		1. Mengurangi dan mencegah penyimpangan
		dalam pengadaan barang/jasa;<br><br>
		2. Mewujudkan efisiensi dan efektifitas anggaran
		negara yang dibelanjakan melalui pengadaan
		barang/jasa;<br><br>
		3. Meningkatkan kapasitas sumber daya
		manusia yang menangani pengadaan
		barang/jasa;<br><br>
		4. Mewujudkan kebijakan nasional
		pengadaan barang/jasa yang jelas,
		kondusif, dan komprehensif;<br><br>
		5. Meningkatkan kapasitas
		organisasi Lembaga Kebijakan
		Pengadaan Barang/Jasa
		Pemerintah (LKPP).
		</div>
      </div> -->

      <!-- <div id="kosong" class="box">
      	asdasdas
      </div> -->

  </div>

  
	
</div>