<!-- content -->
<div class="wrapper row2">

  <div id="container_body" class="clear" style="padding:15px 0px 0px 0px;min-height:500px;">
    <!-- content body -->
   
    <div id="col2" style="margin:0px 0px 20px 0px;width:100%">
      <div class="wrapper row2">
		  <div id="container" class="clear">
		    <div id="homepage" class="clear">
		       <div id="daftar" class="box_tengah" style="padding:20px;min-height:200px;height:100%;float:left;">
		       	<h1>Maklumat Pelayanan</h1>
				<br>
		       	<p>
		       	Kami berupaya dengan sungguh-sungguh untuk :<br>
				<dl>
				<li>Melayani dengan jujur, profesional, bertanggung jawab, dan berintegritas.</li>
				<li>Memberikan kemudahan dan ketepatan pelayanan fasilitasi pelatihan sesuai dengan standar yang ditetapkan.</li>
				<li>Memberikan kemudahan dalam mendapatkan informasi pelatihan pengadaan.</li>
				<li>Merespon informasi dan pengaduan dengan tepat.</li>
				</dl><br>

				     Apabila tidak menepati janji tersebut, kami bersedia menerima sanksi sesuai peraturan yang berlaku.

				</p>
            	
		      </div>

		      
		    </div>
		    <!-- / content body -->
		  </div>
		</div>
      
    </div>
</div>