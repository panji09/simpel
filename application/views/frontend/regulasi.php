<!-- content -->
<div class="wrapper row2">

  <div id="container_body" class="clear" style="padding:15px 0px 0px 0px;min-height:500px;">
    <!-- content body -->
   
    <div id="col2" style="margin:0px 0px 20px 0px;width:100%">
      <div class="wrapper row2">
      <div id="container" class="clear">
        <div id="homepage" class="clear">
           <div id="daftar" class="box_tengah" style="padding:20px;min-height:200px;height:100%;float:left;">
           <form method="POST" action="<?php echo base_url(); ?>unduh/kategori">
            <div style="float:left;margin:0px 20px 0px 0px;font-size:10px;color:#6B6B6A;">
            <select name="kategori" style="width:200px;height:34px;font-size:19px;">
              <?php foreach ($kategori_dokumen as $list_kategori_dokumen) { ?>
                <option value="<?php echo $list_kategori_dokumen->kategori_dokumen; ?>"><?php echo $list_kategori_dokumen->kategori_dokumen; ?></option>
              <?php } ?>
            </select>
            <button class="btn btn-primary" style="margin:-8px 0px 0px 0px;height:32px;width:50px;padding:-3px">Cari</button>
            </div><br>
          </form>
            <table class="table table-hover table-striped tablesorter">
             <thead>
               <tr>
                <th>No<i class="fa fa-sort"></i></th>
                <th>Judul Dokumen<i class="fa fa-sort"></i></th>
                <th>Kategori Dokumen <i class="fa fa-sort"></i></th>
                <th>File Dokumen <i class="fa fa-sort"></i></th>
              </tr>
              </thead>
              <tbody>
              <?php $no = 1; foreach ($dataDokumen as $data): ?>
                <tr>
                  <td><?php echo $no; ?></td>
                  <td><?php echo $data->judul_dokumen; ?></td>
                  <td><?php echo $data->kategori_dokumen; ?></td>
                  <td><a target="_blank" href="<?php echo base_url(); ?>assets/upload/dokumen/<?php echo $data->file_dokumen; ?>"> <?php echo $data->judul_dokumen; ?></a></td>
              <?php $no++; endforeach; ?>
              </tbody>
              
            </table>
          </div>

          
        </div>
        <!-- / content body -->
      </div>
    </div>
      
    </div>
</div>