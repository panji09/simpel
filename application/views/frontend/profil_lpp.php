<script src="<?php echo base_url(); ?>assets/form/js/jquery.js"></script> 
<script type="text/javascript">
function lihat_narasumber(id) {
  $.ajax({
      type: "POST", 
      url: "<?php echo base_url(); ?>jadwal/get_narasumber/"+id, 
      data : $("#userinfo").serialize(),
      dataType: "json",
      success: function(response){
        $(".entry-form").fadeIn("fast");
        var json_obj = JSON.parse(JSON.stringify(response));
        //console.log("narasumber : "+JSON.stringify(json_obj));
        var content = "<ul>";
        
        json_obj.forEach(function(object) {
          //document.getElementById("list_narasumber").innerHTML="<ul><li>"+object.nama_narasumber;
          content += "<li>"+object.nama_narasumber+"</li><br>";
        });

          content += "</ul>";

          document.getElementById("list_narasumber").innerHTML=content;
      },
      error: function(xhr, status, error){
        document.getElementById("list_narasumber").innerHTML="Unexpected error! Try again. = "+error;
      }
    });

  
   
  }

  function close_popup () {
    $(".entry-form").fadeOut("fast");
  }
</script>
<style type="text/css">
  #negeri{
    display: none;
  }
  #swasta{
    display: none;
  }
  td{
    padding: 10px;
  }
  .entry-form{
    display: none;
  }
</style>

<!-- content -->
<div class="wrapper row2">

  <div id="container_body" class="clear" style="padding:15px 0px 0px 0px;min-height:500px;">
    <!-- content body -->
   
    <div id="col2" style="margin:0px 0px 20px 0px;width:100%">
      <div class="wrapper row2">
		  <div id="container" class="clear">
		    <div id="homepage" class="clear">
		       <div id="daftar" class="box_tengah" style="padding:20px;min-height:200px;height:100%;float:left;">

		       	<?php 
              $status = $this->session->flashdata('status');
              if(isset($status)){ echo $status; } 
              foreach ($profil_lpp as $data):
              ?>
              <div class="panel-body">
                <?php echo form_open(base_url().'lpp/proses_update_lpp'); ?>
			         <input type="hidden" name="id_lpp" value="<?php echo $data->id_lpp; ?>">
		             <div class="isi">
				      <?php 
			              $status = $this->session->flashdata('status');
			              if(isset($status)){ echo $status; } 
			              ?>
			              <div class="panel-body">
			                <?php echo form_open_multipart(base_url().'daftar_lpp/proses_tambah_lpp'); ?>
				                
			                	<input type="hidden" name="from" value="home">
					             <div class="form-group">
					                <label>Nama Lembaga : </label><br>
					                <label><?php echo $data->nama_lpp; ?></label>
					            </div>
					            <div class="form-group">
					                <label>Penanggung jawab : </label><br>
					                <label><?php echo $data->penanggungjawab; ?></label>
					            </div>
					            <div class="form-group">
					                <label>Jabatan : </label><br>
					                <label><?php echo $data->jabatan; ?></label>
					            </div>
					            <div class="form-group">
					                <label>No KTP : </label><br>
					                <label><?php echo $data->no_ktp; ?></label>
					            </div>
					            <div class="form-group">
					                <label>Program : </label><br>
					                <label><?php echo $data->program; ?></label>
					            </div>
					            <div class="form-group">
					                <label>Alamat/Sekertariat : </label><br>
					                <label><?php echo $data->alamat_lpp; ?></label>
					            </div>
					            <div class="form-group">
					                <label>No Telp : </label><br>
					                <label><?php echo $data->no_telp_lpp; ?></label>
					            </div>
					            <div class="form-group">
					                <label>No Fax : </label><br>
					                <label><?php echo $data->no_fax; ?></label>
					            </div>
					            <div class="form-group">
					                <label>Email : </label><br>
					                <label><?php echo $data->email; ?></label>
					            </div>
					            <div class="form-group">
					                <label>Kontak Person : </label>
					            </div>
					            <div class="form-group" style="margin:10px;">
					                <label>Nama : </label><br>
					                <label><?php echo $data->kontak_nama; ?></label>
					            </div>
					            <div class="form-group" style="margin:10px;">
					                <label>Telp/HP : </label><br>
					                <label><?php echo $data->kontak_tlp; ?></label>
					            </div>
					            <div class="form-group" style="margin:10px;">
					                <label>Email : </label><br>
					                <label><?php echo $data->kontak_email; ?></label>
					            </div>
					            <!-- <button type="submit" class="btn btn-danger" name="submit" value="cancel">Tolak Request</button> -->
			                </form>
			              </div>
				  </div>
		      </div>
		            <!-- <button type="submit" class="btn btn-primary" name="submit" value="kirim">Tambah</button> -->
		            <!-- <button type="submit" class="btn btn-danger" name="submit" value="cancel">Tolak Request</button> -->
                </form>
            	<?php endforeach; ?>
              </div>
            </div>

				</div>
		      </div>

		      
		    </div>
		    <!-- / content body -->
		  </div>
		</div>
      
    </div>
</div>