<!-- content -->
<div class="wrapper row2">

  <div id="container_body" class="clear">
  <!-- <div id="slider" style="border:1px solid #ff0000;height:240px;margin:5px 0px;">
  	adasdasdasdsad
  </div> -->
    <!-- content body -->
    <br>
    <div id="col1">
    <div class="header">Pengumuman</div>
      <div id="pengumuman" class="box">
      	
	      <div class="isi">
		      	
		      	<ul>
		      	<?php foreach ($dataPengumuman as $data): ?>
		      		<a href="<?php echo base_url(); ?>pengumuman/detail/<?php echo $data->id_pengumuman; ?>">
		      		<li>
		      			<span class="tgl_pengumuman"><?php echo tgl_indo($data->tgl_pengumuman); ?> </span>
		      			<span class="isi_pengumuman">
			      			
			      				<?php echo $data->judul_pengumuman; ?>
			      			
		      			</span>
		      		</li>
		      		</a>
		      	<?php endforeach; ?>
		      	</ul>

		  </div>
      </div>

      <!-- <img src="<?php echo base_url(); ?>assets/design/images/elearning.jpg" class="box">
      <br><br>
      <img src="<?php echo base_url(); ?>assets/design/images/rl.jpg" height="150" class="box">
 -->      
      
      <div id="login_area" class="box">
      <div class="header">Silahkan Login</div>
	      <div class="isi">
	      	 <?php echo form_open('admin/login') ?>
		      <div>
		        
		        <?php 
		        $options = array(
		          '0'  => 'Login Sebagai',
		          '1'    => 'lpp',
		          '3' => 'narasumber',
		          '4' => 'fasilitator',
		        );
		        $role = array('small', 'large');
		        echo "Username : ".form_input('data[username]'); 
		        echo "<br>Password &nbsp;: ".form_password('data[password]')."<br><br>"; 
		        echo form_dropdown('data[role]', $options);
		        ?>
		        <br>
		        <br>
		        <!--<?php//echo form_submit('login', 'Login');?>
		        <input type="text" class="input-block-level" placeholder="Username" name="username">
		        <input type="password" class="input-block-level" placeholder="Password" name="password">-->
		        <input class="btn btn-large btn-primary" type="submit" name="login" value="Login">
		      </div>
		      <?php echo form_close();?>
		    </div>
      </div>
      <!-- <div id="video" class="box">
      <div class="header">Video</div>
      	<iframe width="225" height="220"  src="//www.youtube.com/embed/jgaY-AHaXgE" frameborder="0" allowfullscreen></iframe>
      </div> -->
    </div>
    <div id="col4" class="box">
      <div class="wrapper row2">
      <div class="header">Galeri Foto</div>
		  <div id="container" class="clear">
		    <div id="homepage" class="clear">
		      <section id="services" class="clear">
		      <?php foreach ($galeri_kategori as $data):
		      $photo = str_replace(".", "_thumb.", $data->file_foto);
		      ?>
		      <div style="float:left;width:32%;height:250px;margin:20px 4px;" class="box">
		      <h3 class="header"> <?php echo $data->kategori; ?></h3>
				<img src="<?php echo base_url()."assets/upload/galeri/".$photo; ?>" style="width:95%;height:70%;margin:0px 0px 0px 8px;"><br>	
				
				<a href="<?php echo base_url()."galeri/detail/".$data->id_kategori; ?> "> <span style="margin:0px 0px 0px 8px"> lihat lainya </span></a>
		      </div>
			  <?php endforeach; ?>
				        
		      </section>

		      
		    </div>
		    <!-- / content body -->
		  </div>
		</div>
      
    </div>
</div>