<!-- content -->
<div class="wrapper row2">

  <div id="container_body" class="clear">
  
   	<div id="page" >
	  		<div id="gamesHolder" >
			<div id="games" >
				<?php $no =1; foreach ($headerBerita as $data): ?>
				<a href="<?php echo base_url()."berita/detail/".$data->id_berita; ?>">
					<img src="<?php echo base_url()."assets/upload/berita/".$data->photo_berita; ?>"/>
					<span>
						<b><?php echo $data->judul_berita; ?></b><br />
						<?php 
	                            $string = $data->isi_berita;
	                            $string = strip_tags($string);

	                    if (strlen($string) > 120) {

	                        // truncate string
	                        $stringCut = substr($string, 0, 120);

	                        // make sure it ends in a word so assassinate doesn't become ass...
	                        $string = substr($stringCut, 0, strrpos($stringCut, ' ')).'...'; 
	                    }
	                    echo $string; 
	                  ?>
					</span>
				</a>


				<?php endforeach; ?>
				
			</div>
	  </div>
	</div>
    <br>
    <div id="col1">
    <div class="header">Pengumuman</div>
      <div id="pengumuman" class="box">
      	
	      <div class="isi">
		      	
		      	<ul>
		      	<?php foreach ($dataPengumuman as $data): ?>
		      		<a href="<?php echo base_url(); ?>pengumuman/detail/<?php echo $data->id_pengumuman; ?>">
		      		<li>
		      			<span class="tgl_pengumuman"><?php echo tgl_indo($data->tgl_pengumuman); ?> </span>
		      			<span class="isi_pengumuman">
			      			
			      				<?php echo $data->judul_pengumuman; ?>
			      			
		      			</span>
		      		</li>
		      		</a>
		      	<?php endforeach; ?>
		      	</ul>

		  </div>
      </div>

      <!-- <img src="<?php echo base_url(); ?>assets/design/images/elearning.jpg" class="box">
      <br><br>
      <img src="<?php echo base_url(); ?>assets/design/images/rl.jpg" height="150" class="box">
 -->      
      
      <div id="login_area" class="box">
      <div class="header">Silahkan Login</div>
	      <div class="isi">
	      	 <?php echo form_open('admin/login') ?>
		      <div>
		        
		        <?php 
		        $options = array(
		          '0'  => 'silahkan pilih',
		          '1'    => 'lpp',
		          '3' => 'narasumber',
		          '4' => 'fasilitator',
		        );
		        $role = array('small', 'large');
		        echo "Username : ".form_input('data[username]'); 
		        echo "<br>Password &nbsp;: ".form_password('data[password]')."<br><br>"; 
		        echo form_dropdown('data[role]', $options);
		        ?>
		        <br>
		        <br>
		        <!--<?php//echo form_submit('login', 'Login');?>
		        <input type="text" class="input-block-level" placeholder="Username" name="username">
		        <input type="password" class="input-block-level" placeholder="Password" name="password">-->
		        <input class="btn btn-large btn-primary" type="submit" name="login" value="Login">
		      </div>
		      <?php echo form_close();?>
		    </div>
      </div>
      <!-- <div id="video" class="box">
      <div class="header">Video</div>
      	<iframe width="225" height="220"  src="//www.youtube.com/embed/jgaY-AHaXgE" frameborder="0" allowfullscreen></iframe>
      </div> -->
    </div>
    <div id="col4" class="box">
      <div class="wrapper row2">
      <div class="header"></div>
		  <div id="container" class="clear">
		    <div id="homepage" class="clear">
		      <section id="services" class="clear">
		        <?php foreach ($dataBerita as $data): ?>
		        <!-- article 1 -->
		        <article class="two_third">
		          <h2 style="color:#BC2024"><?php echo $data->judul_pengumuman; ?></h2>
		          <small style="font-size:11px;color:#B29999;"><span style="font-size:11px;font-weight:bold;color:#B29999;">Diterbitkan pada : </span> <?php echo nama_hari($data->tgl_pengumuman).','.tgl_indo($data->tgl_pengumuman); ?></small>
		          <br>
		          <p><?php echo $data->isi_pengumuman; ?></p>
		          <a href="<?php echo base_url()."upload/file/pengumuman/".$data->file_pengumuman; ?>"><?php echo $data->file_pengumuman; ?></a>
		        </article>
		        <?php endforeach; ?>

		      </section>

		      
		    </div>
		    <!-- / content body -->
		  </div>
		</div>

      
    </div>
    
<!-- 
  <div id="galeri_home" class="box">
  <h3 class="header">Galeri Foto</h3>
  <div >
  <?php $no =1; foreach ($dataFoto as $data): ?>
  		<a class="group1" href="<?php echo base_url()."assets/upload/galeri/".$data->file_foto; ?>" title="<?php echo base_url().$data->judul_foto; ?>"><img src="<?php echo base_url()."assets/upload/galeri/".$data->file_foto; ?>" height="150"></a>
	<?php endforeach; ?>
	</div>
  </div> -->
</div>