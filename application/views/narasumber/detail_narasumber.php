            <?php 
              $status = $this->session->flashdata('status');
              if(isset($status)){ echo $status; } 
            ?>
            <?php foreach ($detail as $data): 
            $newname1 = str_replace(" ","_",$data->nama_narasumber);
            $newname2 = str_replace(".","_",$newname1);
            ?>
            <?php echo form_open_multipart('narasumber/update_data_narasumber');?>
            <div id="page-wrapper">
            <div class="row">
            <div class="col-lg-12">
            <div class="panel panel-primary">
              <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-bar-chart-o"></i>Detail Narasumber</h3>
              </div>

            <!-- <div class="col-lg-2"><br>
            <img src="" width="180px" height="180px">
            </div>   -->
            <input type="hidden" value="<?php echo $data->id_narasumber; ?>" class="form-control" name="id_narasumber">
            <div class="col-lg-12"><br>
            <table class="table table-striped">
              <tr>
                <td class="col-lg-3" rowspan="6">
                <span style=""><i>350px x 350px</i></span>
                <img src="<?php echo base_url(); ?>assets/upload/narasumber/<?php echo $data->photo_narasumber; ?>" width="240px" height="260px">
                <input type="file" name="userfile">
                <input type="checkbox" name="isImage" <?php if($newname2!=null){ echo "checked"; }?>> Gunakan Gambar Sebelumnya
                </td>
              </tr>
              <tr>
                <td>Nama</td>
                <td><input type="text" value="<?php echo $data->nama_narasumber; ?>" class="form-control" name="nama_narasumber"></td>
              </tr>
              <tr>
                <td>Alamat</td>
                <td><textarea name ="alamat_narasumber" class="form-control" rows="5"><?php echo $data->alamat_narasumber; ?></textarea></td>
              </tr>
              <tr>
                <td>No Telp</td>
                <td><input type="text" name ="no_telp_narasumber" value="<?php echo $data->no_telp_narasumber; ?>" class="form-control"></td>
              </tr>
              <tr>
                <td>Tempat Lahir</td>
                <td><input type="text" name ="tempat_lahir_narasumber" value="<?php echo $data->tempat_lahir_narasumber; ?>" class="form-control"></td>
              </tr>
              <tr>
                <td>Tanggal Lahir</td>
                <td colspan="2"><input type="text" name ="tgl_lahir_narasumber" value="<?php echo $data->tanggal_lahir_narasumber; ?>" class="form-control" id="dari_tanggal"></td>
              </tr>
              <tr>
                <td>NIP / NIK</td>
                <td colspan="2">
                <input type="text" name ="nip_narasumber" value="<?php echo $data->nip_narasumber; ?>" class="form-control">
                </td>
              </tr>
              <tr>
                <td>Gelar</td>
                <td colspan="2">
                <input type="text" name ="gelar_narasumber" value="<?php echo $data->gelar_narasumber; ?>" class="form-control">
                </td>
              </tr>
              <tr>
                <td>Jenis Kelamin</td>
                <td colspan="2">
                <input type="radio" name ="jenis_kelamin_narasumber" value="pria" <?php if($data->jeniskel_narasumber=="pria"){ echo "checked"; } ?> >Pria
                <input type="radio" name ="jenis_kelamin_narasumber" value="wanita" <?php if($data->jeniskel_narasumber=="wanita"){ echo "checked"; } ?> >Wanita
                </td>
              </tr>
              <tr>
                <td>Golongan</td>
                <td colspan="2">
                <input type="text" name ="golongan_narasumber" value="<?php echo $data->golongan_narasumber; ?>" class="form-control">
                </td>
              </tr>
              <tr>
                <td>Pendidikan</td>
                <td colspan="2">
                <!-- <input type="text" name ="pendidikan_narasumber" value="<?php echo $data->pendidikan_narasumber; ?>" class="form-control"> -->
                <select name ="pendidikan_narasumber" class="form-control">
                  <option value="S1">S1</option>
                  <option value="S2">S2</option>
                  <option value="S3">S3</option>
                </select>
                </td>
              </tr>
              <tr>
                <td>Instansi</td>
                <td colspan="2">
                <input type="text" name ="instansi_narasumber" value="<?php echo $data->instansi_narsum; ?>" class="form-control">
                </td>
              </tr>
              <tr>
                <td>Pangkat</td>
                <td colspan="2">
                <input type="text" name ="pangkat_narasumber" value="<?php echo $data->pangkat_narasumber; ?>" class="form-control">
                </td>
              </tr>
              <tr>
                <td>Satuan Kerja</td>
                <td colspan="2">
                <input type="text" name ="satuan_kerja_narasumber" value="<?php echo $data->satker_narasumber; ?>" class="form-control">
                </td>
              </tr>
              <tr>
                <td>Status Pegawai</td>
                <td colspan="2">
               <!--  <input type="text" name ="status_pegawai" value="<?php echo $data->status_pegawai_narasumber; ?>" class="form-control"> -->
               <select name ="status_pegawai" class="form-control">
                 <option value="ASN">ASN</option>
                 <option value="NON-ASN">NON-ASN</option>
               </select>
                </td>
              </tr>
              <tr>
                <td>Provinsi</td>
                <td colspan="2">
                <input type="text" name ="provinsi" value="<?php echo $data->provinsi_narasumber; ?>" class="form-control">
                </td>
              </tr>
              <tr>
                <td>Kota</td>
                <td colspan="2">
                <input type="text" name ="kota_narasumber" value="<?php echo $data->kota_narasumber; ?>" class="form-control">
                </td>
              </tr>
              <tr>
                <td>Email</td>
                <td colspan="2">
                <input type="text" name ="email_narasumber" value="<?php echo $data->email_narasumber; ?>" class="form-control">
                </td>
              </tr>
              <tr>
                <td>Status Aktif</td>
                <td colspan="2">
                <input type="radio" name ="status_aktif_narasumber" value="1" <?php if($data->status_aktif_narasumber=="1"){ echo "checked"; } ?> >Aktif
                <input type="radio" name ="status_aktif_narasumber" value="0" <?php if($data->status_aktif_narasumber=="0"){ echo "checked"; } ?>>NONAktif
                </td>
              </tr>
              <tr>
                <td>Username</td>
                <td colspan="2">
                <input type="hidden" name ="username" value="<?php echo $data->username; ?>" class="form-control">
                <?php echo $data->username; ?>
                </td>
              </tr>
              <tr>
                <td>Lokasi</td>
                <td colspan="2">
                <input type="text" name ="lokasi_narasumber" value="<?php echo $data->lokasi; ?>" class="form-control">
                </td>
              </tr>
              <tr>
                <td>Nilai TOT Dasar</td>
                <td colspan="2">
                <input type="text" name ="nilai_tot_narasumber" value="<?php echo $data->nilai_tot_dasar; ?>" class="form-control" disabled>
                </td>
              </tr>
              <tr>
                <td>Pelatihan yang pernah di ikuti</td>
                <td colspan="2">
                <input type="text" name ="tot_yang_diikuti" value="<?php echo $data->tot_yang_diikuti; ?>" class="form-control" disabled>
                </td>
              </tr>
              <tr>
                <td>No Sertifikat TOT Dasar</td>
                <td colspan="2">
                <input type="text" name ="sertifikat_tot_narasumber" value="<?php echo $data->no_sertifikat_tot_dasar; ?>" class="form-control" disabled>
                </td>
              </tr>
              <!-- <tr>
                <td>No Sertifikat</td>
                <td colspan="2">
                <input type="text" name ="no_sertifikat_narasumber" value="<?php echo $data->no_sertifikat; ?>" class="form-control">
                </td>
              </tr> -->
              <!-- <tr>
                <td>Hasil Evaluasi</td>
                <td colspan="2">
                <input type="text" name ="hasil_evaluasi" value="<?php echo $data->hasil_evaluasi; ?>" class="form-control">
                </td>
              </tr> -->
              <tr>
                <td>Informasi Lainnya</td>
                <td colspan="2">
                <textarea name="deskripsi_pengalaman" colspan="5" style="height:200px;" class="form-control"><?php echo $data->deskripsi_pengalaman; ?></textarea>
                </td>
              </tr>
              <tr>
                <td>Upload CV</td>
                <td colspan="2">
                <span><a target="_blank" href="<?php echo base_url(); ?>assets/upload/narasumber/<?php echo $data->upload_cv_narasumber; ?>"> <?php if($data->upload_cv_narasumber!=null){ echo $data->upload_cv_narasumber; } ?></a></span>
                <input type="file" name="cv">
                </td>
              </tr>
              <tr>
                <td><button type="submit" class="btn btn-success" name="submit" value="kirim" id="delete">Update Data</button></td>
                <td colspan="2"></td>
                
              </tr>
            </table>
            </div> 
            </form>
            <?php endforeach;?>