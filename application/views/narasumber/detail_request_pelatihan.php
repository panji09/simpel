    <style type="text/css">
    #form-tolak{
        display: none;
    }
    </style>
    <script type="text/javascript">
    function muncul_form () {
        document.getElementById("form-tolak").style.display="block";
    }
        
    </script>
    <br>
      <div class="col-lg-12">
            <div class="panel panel-primary">
              <div class="panel-heading">
                <!-- <h3 class="panel-title"><i class="fa fa-bar-chart-o"></i><?php echo $data->judul_pelatihan; ?> </h3> -->
              </div>
              <div class="panel-body">
                <?php foreach ($detailAll as $data): ?>
                <?php echo form_open(base_url().'narasumber/approve_request_pelatihan/'.$data->id); ?>
                <input type="hidden" name="id" value="<?php echo $data->id; ?>">
                <div class="form-group">
                    <label>Judul Pelatihan</label>
                    <input class="form-control" type="text" id="isi_pelatihan" name="isi_pelatihan" disabled="" value="<?php echo $data->judul_pelatihan; ?>" disabled="">
                </div>
                <div class="form-group">
                    <label>LPP</label>
                    <input class="form-control" type="text" id="isi_pelatihan" name="isi_pelatihan" disabled="" value="<?php echo $data->nama_lpp; ?>" disabled="">
                </div>
                <div class="form-group">
                    <label>Instansi</label>
                    <input class="form-control" type="text" id="isi_pelatihan" name="isi_pelatihan" disabled="" value="<?php echo $data->instansi; ?>" disabled="">
                </div>
                <div class="form-group">
                    <label>Tempat Pelatihan</label>
                    <input class="form-control" type="text" id="isi_pelatihan" name="isi_pelatihan" disabled="" value="<?php echo $data->tempat_pelatihan; ?>" disabled="">
                </div>
                <div class="form-group">
                    <label>Contact Person</label>
                    <input class="form-control" type="text" id="isi_pelatihan" name="isi_pelatihan" disabled="" value="<?php echo $data->contact_nama." - ".$data->contact_hp; ?>" disabled="">
                </div>
                <div class="form-group">
                    <label>Tanggal Pelatihan</label>
                    <input class="form-control" type="text" id="tgl_pelatihan" name="tgl_pelatihan" disabled="" value="<?php echo $data->tgl_pelatihan; ?>" disabled="">
                </div>
                <div class="form-group">
                    <label>Dari Jam</label>
                    <input class="form-control" type="text" id="dari_jam" name="dari_jam" value="<?php echo $data->dari_jam; ?>" disabled="">
                </div>
                <div class="form-group">
                    <label>Sampai Jam</label>
                    <input class="form-control" type="text" id="sampai_jam" name="sampai_jam" disabled="" value="<?php echo $data->sampai_jam; ?>">
                 </div>
                <div class="form-group">
                    <label>Total Jam</label>
                    <input class="form-control" type="text" id="total_jam" name="total_jam" disabled="" value="<?php echo $data->total_jam; ?>">
                </div>

                <div class="form-group">
                    <label>Materi Pelatihan</label><br>
                    <input class="form-control" type="text" id="total_jam" name="total_jam" disabled="" value="<?php echo $data->materi_pelatihan; ?>">
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-success" name="submit" value="1">setujui</button>
                    <button type="button" class="btn btn-danger" name="tolak" id="tolak" onclick="muncul_form();">tolak</button>
                </div>
                
                <!-- <div class="form-group">
                    <label>Total Jam</label>
                    <input class="form-control" type="text" id="total_jam" name="total_jam" disabled="" value="<?php echo $data->total_jam; ?>">
                </div> -->
                <div id="form-tolak">
                    <div class="form-group">
                        <label>Alasan Menolak</label>
                        <textarea class="form-control" name="alasan_tolak" rows="7"></textarea>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-danger" name="submit" value="2">Kirim</button>
                    </div>
                </div>
                </form>
                <?php endforeach; ?>
              </div>
            </div>
            <hr>

            <h3>Jadwal Pelatihan</h3>
            <table class="table table-hover table-striped tablesorter">
                <thead>
                  <tr>
                    <th class="header">No <i class="fa fa-sort"></i></th>
                    <th class="header">Tanggal Pelatihan <i class="fa fa-sort"></i></th>
                    <th class="header">Dari Jam <i class="fa fa-sort"></i></th>
                    <th class="header">Sampai Jam <i class="fa fa-sort"></i></th>
                    <th class="header">Nama Narasumber <i class="fa fa-sort"></i></th>
                    <th class="header">No Handphone <i class="fa fa-sort"></i></th>
                    <th class="header">Total Jam <i class="fa fa-sort"></i></th>
                    <th class="header">Materi Pelatihan <i class="fa fa-sort"></i></th>
                    <th class="header">Approve <i class="fa fa-sort"></i></th>
                    <!-- <th class="header">Action<i class="fa fa-sort"></i></th> -->
                  </tr>
                </thead>
                <tbody>
                <?php $no = 1; foreach ($semua_narasumber as $data): 
                if($data->approve==1){
                  $approve = "approved";
                }else if($data->approve==2){
                  $approve = "cancel";
                }else{
                  $approve = "pending";
                }
                ?>
                  <tr>
                    <td><?php echo $no; ?></td>
                    <td><?php echo $data->tgl_pelatihan; ?></td>
                    <td><?php echo $data->dari_jam; ?></td>
                    <td><?php echo $data->sampai_jam; ?></td>
                    <td><?php echo $data->nama_narasumber; ?></td>
                    <td><?php echo $data->no_telp; ?></td>
                    <td><?php echo $data->total_jam; ?></td>
                    <td><a href="<?php echo base_url(); ?>assets/upload/materi_pelatihan/<?php echo $data->materi_pelatihan; ?>"> <?php echo $data->materi_pelatihan; ?></td>
                    <td><?php echo "<img src=\"".base_url()."assets/".$approve.".png\" title=\"".$approve."\">"; ?><br><?php echo $approve; if($approve=="cancel"){ echo "<br><a href=\"#\" onClick=\"javascript:alasan('".$data->alasan."');\">alasan ditolak</a>"; }?></td>
                    <td>
                    <!-- <button type="submit" class="btn btn-primary" name="submit" value="kirim" id="edit">edit</button> -->
                    <!-- <button type="submit" class="btn btn-danger" name="submit" value="kirim" id="delete">delete</button> -->
                    </td>
                  </tr>
                <?php $no++ ; endforeach; ?>
                </tbody>
              </table>
          </div>
        </div><!-- /.row -->



   