<div id="page-wrapper">
<div class="row">
          <div class="col-lg-12">
            <div class="panel panel-primary">
              <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-bar-chart-o"></i>Tambah Rencana Kegiatan</h3>
              </div>
              <?php 
	              $status = $this->session->flashdata('status');
	              if(isset($status)){ echo $status; } 
              ?>
              <div class="panel-body">

                <?php echo form_open_multipart(base_url().'narasumber/add_rencana_kegiatan'); ?>

		            <div class="form-group">
		                <label>Dari tanggal *</label>
		                <input class="form-control" type="text" id="dari_tanggal" name="dari_tanggal" value="">
		            </div>
		            <div class="form-group">
		                <label>Sampai tanggal *</label>
		                <input class="form-control" type="text" id="sampai_tanggal" name="sampai_tanggal" value="">
		            </div>
		            <div class="form-group">
		                <label>Instansi*</label>
		                <input class="form-control" type="text" id="instansi" name="instansi" value="">
		            </div>
		            <div class="form-group">
		                <label>Judul Materi*</label>
		                <input class="form-control" type="text" id="judul_materi" name="judul_materi" value="">
		            </div>
		            <div class="form-group">
		                <label>Jumlah JP *</label>
		                <input class="form-control" type="text" id="jumlah_jp" name="jumlah_jp" value="">
		            </div>
		            <div class="form-group">
		                <label>Penugasan Oleh*</label>
		                <input class="form-control" type="text" id="penugasan_oleh" name="penugasan_oleh" value="">
		            </div>
		            <div class="form-group">
		                <label>Keterangan *</label>
		                <textarea class="form-control" rows="4" name="keterangan"></textarea>
		            </div>
		            <div class="form-group">
		                <label>Bukti *</label>
		                <input type="file" name="userfile">
		            </div>

		            <button type="submit" class="btn btn-primary" name="submit" value="kirim">Kirim</button>
		            <!-- <button type="submit" class="btn btn-danger" name="submit" value="cancel">Tolak Request</button> -->
                </form>

                <?php echo form_close(); ?>
              
              </div>
            </div>
          </div>
        </div><!-- /.row -->