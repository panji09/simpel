        
        <div id="page-wrapper">
            <div class="row">
              <div class="col-lg-12">
              <ol class="breadcrumb">
                 <a href="<?php echo base_url(); ?>narasumber/the_rencana_kegiatan"><i class="fa fa-dashboard"></i> <button type="submit" class="btn btn-primary" name="submit" value="kirim" id="edit">Agenda Pribadi</button></a>
               </div>
            </div><!-- /.row -->
            <div class="col-lg-12">
            <div class="panel panel-primary">
              <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-bar-chart-o"></i>Agenda Pribadi</h3>
              </div>
                <table class="table table-hover table-striped tablesorter">
                <thead>
                  <tr>
                    <!-- <th>Rencana Kegiatan <i class="fa fa-sort"></i></th> -->
                    <th>No <i class="fa fa-sort"></i></th>
                    <th>Dari Tanggal <i class="fa fa-sort"></i></th>
                    <th>Sampai Tanggal <i class="fa fa-sort"></i></th>
                    <th>Jumlah JP <i class="fa fa-sort"></i></th>
                    <th>Keterangan <i class="fa fa-sort"></i></th>
                    <th>Instansi <i class="fa fa-sort"></i></th>
                    <th>Judul Materi <i class="fa fa-sort"></i></th>
                    <th>Penugasan Oleh <i class="fa fa-sort"></i></th>
                    <th>Bukti Kegiatan <i class="fa fa-sort"></i></th>
                    <th>Approved <i class="fa fa-sort"></i></th>
                    <!-- <th colspan="2" align="center">Action<i class="fa fa-sort"></i></th> -->
                  </tr>
                </thead>
                <tbody>
                <?php $no = 1; foreach ($renKegiatan as $data): 
                if($data->approved==1){
                  $approved = "approved";
                }else if($data->approved==2){
                  $approved = "cancel";
                }else{
                  $approved = "pending";
                }
                ?>
                  <tr>
                    <!-- <td><?php echo $data->id_rencana; ?></td> -->
                    <td><?php echo $no; ?></td>
                    <td><?php echo $data->dari_tanggal; ?></td>
                    <td><?php echo $data->sampai_tanggal; ?></td>
                    <td><?php echo $data->jumlah_jp; ?></td>
                    <td><?php echo $data->keterangan; ?></td>
                    <td><?php echo $data->instansi; ?></td>
                    <td><?php echo $data->judul_materi; ?></td>
                    <td><?php echo $data->penugasan_oleh; ?></td>
                    <td><a href="<?php echo base_url(); ?>assets/upload/bukti_kegiatan/<?php echo $data->bukti_kegiatan; ?>"><?php echo $data->bukti_kegiatan; ?></a> </td>
                    <td>
                    <?php echo "<img src=\"".base_url()."assets/".$approved.".png\" title=\"".$approved."\"><br>".$approved; 
                    if($data->alasan_ditolak!=""){
                      echo "<br>alasan,".$data->alasan_ditolak;
                    }
                    ?></td>
                    <!-- <td>
                        <?php echo anchor('rencana_kegiatan/edit_rencana_kegiatan/'.$data->id_rencana,"Edit");?> | 
                        <?php echo anchor('rencana_kegiatan/proses_hapus/'.$data->id_rencana,"Delete");?>
                        <button type="submit" class="btn btn-primary" name="submit" value="kirim" id="edit">Edit</button>
                        <button type="submit" class="btn btn-danger" name="submit" value="kirim" id="delete">Delete</button>
                        <button type="submit" class="btn btn-success" name="submit" value="kirim" id="delete">Detail</button>
                    </td> -->
                  </tr>
                <?php
                $no++;
                endforeach;
                ?>
                </tbody>
              </table>
              <div id="pager" class="pager">
                <form>
                  <img src="<?php echo base_url()?>assets/table_sorter/addons/pager/icons/first.png" class="first"/>
                  <img src="<?php echo base_url()?>assets/table_sorter/addons/pager/icons/prev.png" class="prev"/>
                  <input type="text" class="pagedisplay"/>
                  <img src="<?php echo base_url()?>assets/table_sorter/addons/pager/icons/next.png" class="next"/>
                  <img src="<?php echo base_url()?>assets/table_sorter/addons/pager/icons/last.png" class="last"/>
                  <select class="pagesize">
                    <option selected="selected"  value="10">10</option>
                    <option value="20">20</option>
                    <option value="30">30</option>
                    <option  value="40">40</option>
                  </select>
                </form>
              </div>