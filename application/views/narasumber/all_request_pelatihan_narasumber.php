            <div id="page-wrapper">
            <div class="row">
            <div class="col-lg-12">
            <div class="panel panel-primary">
              <?php 
              $status = $this->session->flashdata('status');
              if(isset($status)){ echo $status; } 
              ?>
              <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-bar-chart-o"></i>All Request Pelatihan Narasumber</h3>
              </div>
              <table class="table table-bordered table-hover tablesorter">
                <thead>
                  <tr>
                    <th>No <i class="fa fa-sort"></i></th>
                    <th>Judul Pelatihan <i class="fa fa-sort"></i></th>
                    <th>Tanggal Pelatihan<i class="fa fa-sort"></i></th>
                    <th>LPP<i class="fa fa-sort"></i></th>
                    <th>Dari Jam <i class="fa fa-sort"></i></th>
                    <th>Sampai Jam<i class="fa fa-sort"></i></th>
                    <th>Total Jam<i class="fa fa-sort"></i></th>
                    <th>Action<i class="fa fa-sort"></i></th>
                  </tr>
                </thead>
                <tbody>
                <?php $no=1; foreach ($all_request as $data):  
                if($data->id_lpp==0){
                  $lpp = "LKPP";
                }else{
                  $lpp = $data->nama_lpp;
                }
                ?>
                  <tr>
                    <td><?php echo $no; ?></td>
                    <td><?php echo $data->judul_pelatihan; ?></td>
                    <td><?php echo $data->tgl_pelatihan; ?></td>
                    <td><?php echo $lpp; ?></td>
                    <td><?php echo $data->dari_jam; ?></td>
                    <td><?php echo $data->sampai_jam; ?></td>
                    <td><?php echo $data->total_jam; ?></td>
                    <td>
                      <a href="<?php echo base_url().$role."/detail_all_request_pelatihan/".$data->id."/".$data->id_pelatihan; ?>"><button type="submit" class="btn btn-success" name="submit" value="kirim" id="delete">Detail</button></a>
                    </td>
                  </tr>
                <?php 
                $no++;
                endforeach;
              ?>
                </tbody>
              </table>