<script>
     var time = new Date().getTime();
     $(document.body).bind("mousemove keypress", function(e) {
         time = new Date().getTime();
     });

     function refresh() {
         if(new Date().getTime() - time >= 60000) 
             window.location.reload(true);
         else 
             setTimeout(refresh, 1000);
     }

     setTimeout(refresh, 1000);
</script>
      

      <div id="page-wrapper">

 
        <div class="row">

          <div class="col-lg-4">
            <div class="panel panel-info">
              <div class="panel-heading">
                <div class="row">
                  <div class="col-xs-6">
                    <i class="fa fa-tasks fa-5x"></i>
                  </div>
                  <div class="col-xs-6 text-right">
                    <p class="announcement-heading"><?php echo $JPpribadi; ?></p>
                    <p class="announcement-text"> Jam</p>
                  </div>
                </div>
              </div>
              <a href="#">
                <div class="panel-footer announcement-bottom">
                  <div class="row">
                    <div class="col-xs-6">
                      <a href="<?php echo base_url(); ?>narasumber/rencana_kegiatan">JP agenda pribadi</a>
                    </div>
                    <div class="col-xs-6 text-right">
                      <i class="fa fa-arrow-circle-right"></i>
                    </div>
                  </div>
                </div>
              </a>
            </div>
          </div>

          <div class="col-lg-4">
            <div class="panel panel-info">
              <div class="panel-heading">
                <div class="row">
                  <div class="col-xs-6">
                    <i class="fa fa-tasks fa-5x"></i>
                  </div>
                  <div class="col-xs-6 text-right">
                    <p class="announcement-heading"><?php echo $JPlkpp; ?></p>
                    <p class="announcement-text">Jam</p>
                  </div>
                </div>
              </div>
              <a href="#">
                <div class="panel-footer announcement-bottom">
                  <div class="row">
                    <div class="col-xs-6">
                      <a href="<?php echo base_url(); ?>narasumber/schedule_narasumber">Total JP oleh lkpp</a>
                    </div>
                    <div class="col-xs-6 text-right">
                      <i class="fa fa-arrow-circle-right"></i>
                    </div>
                  </div>
                </div>
              </a>
            </div>
          </div>


          
        </div><!-- /.row -->

        <!-- <div class="row">
          <div class="col-lg-12">
            <div class="panel panel-primary">
              <div class="panel-heading">
                <?php 
                $status = $this->session->flashdata('status');
                if(isset($status)){ echo $status; } 
                ?>
                <h3 class="panel-title"><i class="fa fa-bar-chart-o"></i> Request Pelatihan Baru</h3>
              </div>
              
            </div>
          </div>
        </div> -->
        <form method="POST" action="<?php echo base_url();?>narasumber/detail_kegiatan_narasumber/<?php echo $id_user; ?>">
            <div style="border:1px solid #DADADA;padding:8px;">
                
                <tr>
                  <td><span style="float:left;">Dari Tanggal : </span></td>
                  <td>
                  <input type="text" placeholder="dari tanggal" name="dari_tanggal" id="dari_tanggal" class="form-control" style="float:left;width:190px;margin:0px 4px 0px 0px;" value="<?php if(isset($dari_tanggal)){ echo $dari_tanggal; } ?>">
                  </td>
                </tr>
                <tr>
                  <td><span style="float:left;">Sampai Tanggal : </span> </td>
                  <td>
                  <input type="text" name="sampai_tanggal" id="sampai_tanggal" placeholder="sampai tanggal" class="form-control" style="float:left;width:190px;margin:0px 4px 0px 0px;" value="<?php if(isset($sampai_tanggal)){ echo $sampai_tanggal; } ?>">
                  </td>
                </tr>
                <button type="submit" class="btn btn-primary" name="submit" value="kirim" id="edit">Cari</button>
                
            </div><br>
            </form>
         <div class="panel panel-primary">
              <div class="panel-heading">
              <?php //echo $conditionQuery; ?>
                <h3 class="panel-title"><i class="fa fa-bar-chart-o"></i>Detail Kegiatan Narasumber</h3>
              </div>
                <table class="table table-hover table-striped tablesorter">
                <thead>
                  <tr>
                    <th class="header">No<i class="fa fa-sort"></i></th>
                    <!-- <th class="header">Nama Narasumber <i class="fa fa-sort"></i></th>
                    <th class="header">No Telp Narasumber <i class="fa fa-sort"></i></th>
                    <th class="header">Lokasi<i class="fa fa-sort"></i></th>
                    <th class="header">Email<i class="fa fa-sort"></i></th> -->
                    <th class="header">Instansi<i class="fa fa-sort"></i></th>
                    <th class="header">Catatan<i class="fa fa-sort"></i></th>
                    <th class="header">Tanggal<i class="fa fa-sort"></i></th>
                    <th class="header">Total JP <i class="fa fa-sort"></i></th>
                  </tr>
                </thead>
                <tbody>
                <?php $no = 1; $total = 0; foreach ($detail_kegiatan_narasumber as $data): ?>
                  <tr>
                    <td><?php echo $no; ?></td>
                    <!-- <td><?php echo $data->nama_narasumber; ?></td>
                    <td><?php echo $data->no_telp; ?></td>
                    <td><?php echo $data->lokasi; ?></td>
                    <td><?php echo $data->email; ?></td>
 -->                <td><?php echo $data->instansi; ?></td>
                    <td><?php echo $data->catatan_narasumber; ?></td>
                    <td><?php echo $data->tanggal.','.$data->tanggal; ?></td>
                    <td><?php echo $data->jumlah_jp; ?></td>
                  </tr>
                <?php $no++; $total += $data->jumlah_jp; endforeach; ?>
                  <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>Total : </td>
                    <td><?php echo $total; ?></td>
                  </tr>
                </tbody>
              </table>
              <div id="pager" class="pager">
                <form>
                  <img src="<?php echo base_url()?>assets/table_sorter/addons/pager/icons/first.png" class="first"/>
                  <img src="<?php echo base_url()?>assets/table_sorter/addons/pager/icons/prev.png" class="prev"/>
                  <input type="text" class="pagedisplay"/>
                  <img src="<?php echo base_url()?>assets/table_sorter/addons/pager/icons/next.png" class="next"/>
                  <img src="<?php echo base_url()?>assets/table_sorter/addons/pager/icons/last.png" class="last"/>
                  <select class="pagesize">
                    <option selected="selected"  value="10">10</option>
                    <option value="20">20</option>
                    <option value="30">30</option>
                    <option  value="40">40</option>
                  </select>
                </form>
              </div>

      </div><!-- /#page-wrapper -->

    </div><!-- /#wrapper -->