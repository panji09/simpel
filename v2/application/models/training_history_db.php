<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Training_history_db extends CI_Model{
    function __construct(){
        // Call the Model constructor
        parent::__construct();
    }
    
    function exist($training_history_id){
	return count($this->get_all(array('training_history_id' => $training_history_id))) == 1;
    }
    
    function get($training_history_id){
	return $this->get_all($filter = array('training_history_id' => $training_history_id));
    }
    
    function get_all($filter=null, $limit=null, $offset=null){
	
	if(isset($filter['created_lt']))
	    $this->mongoci->whereLt('created', $filter['created_lt']);
	    
	if(isset($filter['training_history_id']))
	    $this->mongoci->where('_id', new MongoId($filter['training_history_id']));
	
	if(isset($filter['user_id']))
	    $this->mongoci->where('author.user_id', $filter['user_id']);
		
	if(isset($filter['published']))
	    $this->mongoci->where('published', $filter['published']);
	if($limit)
	    $this->mongoci->limit($limit);
        if($offset)
	    $this->mongoci->offset($offset);
	    
	$this->mongoci->where('deleted',0);
        $this->mongoci->orderBy(array('created' => 'DESC'));
        
        return $this->mongoci->get('training_history');
        //$this->mongoci->lastQuery();
    }
    
    function save($training_history_id=null, $data_content=array(), $data_push=array()){
	$result = false;
	
	$last_update = array('time' => time(), 'ip' => $this->input->ip_address(), 'author' => $data_content['author']);
	
	if($training_history_id && $this->exist($training_history_id)){
	    //update
	    $this->mongoci->where('_id', new MongoId($training_history_id));
	    
	    if($data_content)
		$this->mongoci->set($data_content);
	    
	    $this->mongoci->set('last_update', $last_update);
// 	    $this->mongoci->push('log_last_update', $last_update);
	    
	    if($data_push)
		$this->mongoci->push($data_push);

	    $result = $this->mongoci->update('training_history');
	    
	}else{
	    //insert
	    $data_content['last_update'] = $last_update;
// 	    $data_content['log_last_update'] = array($last_update);
	    $data_content['created'] = time();
	    $data_content['deleted'] = 0;
	    
	    $result = $this->mongoci->insert('training_history', $data_content);
	    
	}
	
	return $result;
    }
    
    function delete($training_history_id=null){
	return $this->save($training_history_id, array('deleted' => 1));
    }
    
}
?> 
