<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Invite_participant_db extends CI_Model{
    function __construct(){
        // Call the Model constructor
        parent::__construct();
    }
    
    function exist($invite_participant_id){
		return count($this->get_all(array('invite_participant_id' => $invite_participant_id))) == 1;
    }
    
    function get($invite_participant_id){
		return $this->get_all($filter = array('invite_participant_id' => $invite_participant_id));
    }
    
    function exist_email($email){
		return count($this->get_all(array('email' => $email))) == 1;
    }
	
    function exist_invite_participant($invite_participant){
		return count($this->get_all(array('invite_participant' => $invite_participant))) == 1;
    }
    
    function get_invite_participant($invite_participant){
		return $this->get_all($filter = array('invite_participant' => $invite_participant));
    }
    
	function get_email($email){
		return $this->get_all($filter = array('email' => $email));
    }
    
    
    function get_all($filter=null, $limit=null, $offset=null){
		
		if(isset($filter['invite_participant_id']))
			$this->mongoci->where('_id', new MongoId($filter['invite_participant_id']) );		
		
		if(isset($filter['invite_participant_code']))
			$this->mongoci->where('invite_participant_code', $filter['invite_participant_code'] );		
		
		if(isset($filter['email']))
			$this->mongoci->where('email', $filter['email'] );		
		
        if(isset($filter['training_id']))
			$this->mongoci->where('training_id', $filter['training_id'] );		
		
		if(isset($filter['created_lt']))
			$this->mongoci->whereLt('created', $filter['created_lt']);
			
		if(isset($filter['published']))
			$this->mongoci->where('published', $filter['published']);
		if($limit)
			$this->mongoci->limit($limit);
			if($offset)
			$this->mongoci->offset($offset);
			
		$this->mongoci->where('deleted',0);
        $this->mongoci->orderBy(array('created' => 'DESC'));
        
        return $this->mongoci->get('invite_participant');
        //$this->mongoci->lastQuery();
    }
    
    function save($invite_participant_id=null, $data_content=array(), $data_push=array()){
		$result = false;
		
		$last_update = array('time' => time(), 'ip' => $this->input->ip_address(), 'author' => $this->connect_auth->get_basic_me());
		
		if($invite_participant_id && $this->exist($invite_participant_id)){
			//update
			$this->mongoci->where('_id', new MongoId($invite_participant_id) );	
			$data_content['author'] =  $this->connect_auth->get_basic_me();
			if($data_content)
			$this->mongoci->set($data_content);
			
			$this->mongoci->set('last_update', $last_update);
	// 	    $this->mongoci->push('log_last_update', $last_update);
			
			if($data_push)
			$this->mongoci->push($data_push);
	
			$result = $this->mongoci->update('invite_participant');
			
		}else{
			//insert
			$data_content['creator'] = $this->connect_auth->get_basic_me();
			$data_content['author'] = $data_content['creator'];
			$data_content['last_update'] = $last_update;
	// 	    $data_content['log_last_update'] = array($last_update);
			$data_content['created'] = time();
			$data_content['deleted'] = 0;
			
			$result = $this->mongoci->insert('invite_participant', $data_content);
		}
		
		return $result;
    }
    
    function delete($invite_participant_id=null){
		return $this->save($invite_participant_id, array('deleted' => 1));
    }
	
    
	
}
?> 
 
 
