<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Pages_db extends CI_Model{
    function __construct(){
        // Call the Model constructor
        parent::__construct();
    }
    
    
    
    function get_all($filter=null, $limit=null, $offset=null){

	if(isset($filter['created_lt']))
	    $this->mongoci->whereLt('created', $filter['created_lt']);
	
	if(isset($filter['search'])){
// 	    $this->mongoci->Where('title', new MongoRegex('/'.$filter['search'].'/'));
	    $this->mongoci->whereIn('page_id', array('news','event','gallery'));
	    $this->mongoci->OrWhere(array('title' => new MongoRegex('/'.$filter['search'].'/i')));
// 	    $this->mongoci->orWhere(array('content' => new MongoRegex('/'.$filter['search'].'/')));
// 	    $this->mongoci->whereLike('content', $filter['search']);
	    
	}
	
	if(isset($filter['published']))
	    $this->mongoci->where('published', $filter['published']);
	if($limit)
	    $this->mongoci->limit($limit);
        if($offset)
	    $this->mongoci->offset($offset);
	    
	$this->mongoci->where('deleted',0);
        $this->mongoci->orderBy(array('created' => 'DESC'));
        
        return $this->mongoci->get('pages');
//         print_r($this->mongoci->lastQuery());
    }
    
    
}
?> 
 
