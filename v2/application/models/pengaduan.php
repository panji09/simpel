<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Pengaduan extends CI_Model{
	function __construct(){
        // Call the Model constructor
		parent::__construct();
	}
	
	function exist($todolist_id){
		return count($this->get_all(array('todolist_id' => $todolist_id))) == 1;
	}
	
	function get($pengaduan_id){
		return $this->get_all($filter = array('setting_id' => $pengaduan_id));
	}
	
	function get_all($filter=null, $limit=null, $offset=null){
		
		if(isset($filter['created_lt']))
			$this->mongoci->whereLt('created', $filter['created_lt']);
		
		if(isset($filter['code_email_verification']))
			$this->mongoci->where('code_email_verification', $filter['code_email_verification']);

		if(isset($filter['setting_id']))
			$this->mongoci->where('setting_id', $filter['setting_id']);
		
		if(isset($filter['user_id']))
			$this->mongoci->where('author.user_id', $filter['user_id']);
		
		if(isset($filter['published']))
			$this->mongoci->where('published', $filter['published']);
		if($limit)
			$this->mongoci->limit($limit);
		if($offset)
			$this->mongoci->offset($offset);
		
		 $this->mongoci->where('deleted',0);
		 $this->mongoci->orderBy(array('created' => 'DESC'));
		
		return $this->mongoci->get('pengaduan');
        //$this->mongoci->lastQuery();
	}
	
	function save($todolist_id=null, $data_content=array(), $data_push=array()){
		$result = false;
		
		$last_update = array('time' => time(), 'ip' => $this->input->ip_address(), 'author' => $this->connect_auth->get_basic_me());
		
		if($todolist_id && $this->exist($todolist_id)){
	    //update
			$this->mongoci->where('_id', new MongoId($todolist_id));
			
			$data_content['author'] = $this->connect_auth->get_basic_me();
			if($data_content)
				$this->mongoci->set($data_content);
			
			
			$this->mongoci->set('last_update', $last_update);
// 	    $this->mongoci->push('log_last_update', $last_update);
			
			if($data_push)
				$this->mongoci->push($data_push);

			$result = $this->mongoci->update('todolist');
			
		}else{
	    //insert
			$data_content['creator'] = sha1_salt(time());
			$data_content['author'] = $data_content['creator'];
			
			$data_content['last_update'] = $last_update;
// 	    $data_content['log_last_update'] = array($last_update);
			$data_content['created'] = time();
			$data_content['deleted'] = 0;
			
			$result = $this->mongoci->insert('pengaduan', $data_content);
			
		}
		
		return $result;
	}
	
	function delete($todolist_id=null){
		return $this->save($todolist_id, array('deleted' => 1));
	}
	
}
?> 

