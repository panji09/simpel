<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Static_pages_db extends CI_Model{
    function __construct(){
        // Call the Model constructor
        parent::__construct();
    }
    
    function exist($page_id){
	return count($this->get_all(array('page_id' => $page_id))) == 1;
    }
    
    function get($page_id){
	return $this->get_all($filter = array('page_id' => $page_id));
    }
    
    function get_all($filter=null){
	
	if(isset($filter['page_id']))
	    $this->mongoci->where('page_id', $filter['page_id']);
	
	$this->mongoci->where('deleted',0);
        $this->mongoci->orderBy(array('created' => 'DESC'));
        
        return $this->mongoci->get('pages');
        //$this->mongoci->lastQuery();
    }
    
    function save($page_id=null, $data_page=array(), $data_push=array()){
	$result = false;
	
	$last_update = array('time' => time(), 'ip' => $this->input->ip_address(), 'author' => $this->connect_auth->get_basic_me());
	
	if($page_id && $this->exist($page_id)){
	    //update
	    $this->mongoci->where('page_id', $page_id);
	    
	    $data_page['author'] = $this->connect_auth->get_basic_me();
	    if($data_page)
		$this->mongoci->set($data_page);
	    
	    $this->mongoci->set('last_update', $last_update);
// 	    $this->mongoci->push('log_last_update', $last_update);
	    
	    if($data_push)
		$this->mongoci->push($data_push);

	    $result = $this->mongoci->update('pages');
	    
	}else{
	    //insert
	    $data_page['creator'] = $this->connect_auth->get_basic_me();
	    $data_page['author'] = $data_content['creator'];
	    $data_page['last_update'] = $last_update;
// 	    $data_page['log_last_update'] = array($last_update);
	    $data_page['created'] = time();
	    $data_page['deleted'] = 0;
	    
	    $result = $this->mongoci->insert('pages', $data_page);
	    
	}
	
	return $result;
    }
    
    function delete($page_id=null){
	return $this->save($page_id, array('deleted' => 1));
    }
    
}
?> 
