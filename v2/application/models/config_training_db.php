<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Config_training_db extends CI_Model{
    function __construct(){
        // Call the Model constructor
        parent::__construct();
    }
    
    function exist($config_training_id){
		return count($this->get_all(array('config_training_id' => $config_training_id))) == 1;
    }
    
    function get($config_training_id){
		return $this->get_all($filter = array('config_training_id' => $config_training_id));
    }
    
    function get_all($filter=null, $limit=null, $offset=null){
	
		if(isset($filter['created_lt']))
			$this->mongoci->whereLt('created', $filter['created_lt']);
			
		if(isset($filter['config_training_id']))
			$this->mongoci->where('_id', new MongoId($filter['config_training_id']));
		
		if(isset($filter['user_id']))
			$this->mongoci->where('author.user_id', $filter['user_id']);
		
		
		if(isset($filter['type_training_id']))
			$this->mongoci->where('type_training.id', $filter['type_training_id']);
		
		if(isset($filter['published']))
			$this->mongoci->where('published', $filter['published']);
		if($limit)
			$this->mongoci->limit($limit);
			if($offset)
			$this->mongoci->offset($offset);
			
		$this->mongoci->where('deleted',0);
			$this->mongoci->orderBy(array('created' => 'DESC'));
			
			return $this->mongoci->get('config_training');
			//$this->mongoci->lastQuery();
    }
    
    function save($config_training_id=null, $data_content=array(), $data_push=array()){
		$result = false;
		
		$last_update = array('time' => time(), 'ip' => $this->input->ip_address(), 'author' => $this->connect_auth->get_basic_me());
		$data_content['author'] = $this->connect_auth->get_basic_me();
		
		if($config_training_id && $this->exist($config_training_id)){
			//update
			$this->mongoci->where('_id', new MongoId($config_training_id));
			
			if($data_content)
			$this->mongoci->set($data_content);
			
			$this->mongoci->set('last_update', $last_update);
	// 	    $this->mongoci->push('log_last_update', $last_update);
			
			if($data_push)
			$this->mongoci->push($data_push);
	
			$result = $this->mongoci->update('config_training');
			
		}else{
			//insert
			$data_content['last_update'] = $last_update;
			$data_content['creator'] = $this->connect_auth->get_basic_me();
	// 	    $data_content['log_last_update'] = array($last_update);
			$data_content['created'] = time();
			$data_content['deleted'] = 0;
			
			$result = $this->mongoci->insert('config_training', $data_content);
			
			$config_training_id = ($result ? $result->{'$id'} : '');
		}
		
		if($config_training_id){
			$this->update_mysql($config_training_id);
		}
		
		return $result;
    }
    
    function delete($config_training_id=null){
		return $this->save($config_training_id, array('deleted' => 1));
    }
	
	function generate_data($row){
		
		$temp['id'] = $row['_id']->{'$id'};
		$temp['type_training_id'] = isset($row['type_training']['id']) ? $row['type_training']['id'] : '';
		$temp['type_training_name'] = isset($row['type_training']['name']) ? $row['type_training']['name'] : '';
		$temp['name'] = isset($row['name']) ? $row['name'] : '';
		$temp['hour_leasson'] = isset($row['hour_leasson']) ? $row['hour_leasson'] : '';
		$temp['requirement_description'] = isset($row['requirement_description']) ? $row['requirement_description'] : '';
		$temp['training_description_short'] = isset($row['training_description_short']) ? $row['training_description_short'] : '';
		$temp['training_description'] = isset($row['training_description']) ? $row['training_description'] : '';
		$temp['teaching_material_upload'] = isset($row['teaching_material_upload']) ? $row['teaching_material_upload'] : '';
		$temp['published'] = isset($row['published']) ? $row['published'] : '';
		$temp['requirement_certificate'] = isset($row['requirement_certificate']) ? $row['requirement_certificate'] : '';
		$temp['requirement_educational'] = isset($row['requirement_educational']) ? implode(',',$row['requirement_educational']) : '';
		$temp['requirement_letter_assignment'] = isset($row['requirement_letter_assignment']) ? $row['requirement_letter_assignment'] : '';
		$temp['enable_support_document'] = isset($row['enable_support_document']) ? $row['enable_support_document'] : '';
		$temp['created'] = isset($row['created']) ? $row['created'] : '';
		$temp['deleted'] = isset($row['deleted']) ? $row['deleted'] : '';
		
		return $temp;
	}
	
	function update_mysql($config_training_id = null){
		if($config_training_id){
			$query = $this->get($config_training_id);
			$data = $this->generate_data($query[0]);
			
			$this->db->where('id', $config_training_id);
			$this->db->from('config_training');
			
			if($this->db->count_all_results() == 1){
				//update
				$this->db->where('id', $config_training_id);
				$this->db->update('config_training', $data); 
			}else{
				//insert
				
				$this->db->insert('config_training', $data); 
			}
		}
	}
	function generate_mysql(){
		
		$query = $this->get_all();
		$this->db->empty_table('config_training');
		
		$data_config_training = array();
		foreach($query as $row){
			
			
			$data_config_training[] = $this->generate_data($row);
		} 
		
		 $this->db->insert_batch('config_training', $data_config_training);
		 
	}
    
}
?> 
