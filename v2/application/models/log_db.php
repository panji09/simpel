<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Log_db extends CI_Model{
    function __construct(){
        // Call the Model constructor
        parent::__construct();
    }
    
    function exist($id){
	return count($this->get_all(array('id' => $id))) == 1;
    }
    
    function get($id){
	return $this->get_all($filter = array('id' => $id));
    }
    
    function get_all($filter=null){
	
	if(isset($filter['id']))
	    $this->mongoci->where('_id',  new MongoId($filter['id']));
	
	
	$this->mongoci->where('deleted',0);
        
        $this->mongoci->orderBy(array('created' => 'DESC'));
        
        return $this->mongoci->get('log');
        //$this->mongoci->lastQuery();
    }
    
    function save($log_id=null, $data_log=array(), $data_push=array()){
	$result = false;
	
	if($log_id && $this->exist($log_id)){
	    //update
	    $this->mongoci->where('_id', new MongoId($log_id));
	    
	    if($data_log)
		$this->mongoci->set($data_log);
	    
// 	    $this->mongoci->set('last_update', $last_update);
// 	    $this->mongoci->push('log_last_update', $last_update);
	    
	    if($data_push)
		$this->mongoci->push($data_push);

	    $result = $this->mongoci->update('log');
	    
	}else{
	    //insert
	    $data_log['created'] = time();
	    $data_log['deleted'] = 0;
	    
	    $result = $this->mongoci->insert('log', $data_log);
	    
	}
	return $result;
    }
    
    function delete($log_id=null){
	return $this->save($log_id, array('deleted' => 1));
    }
    
}
?> 
 
