<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class User_db extends CI_Model{
    function __construct(){
        // Call the Model constructor
        parent::__construct();
    }
    
    function exist($id){
	return count($this->get_all(array('id' => $id))) == 1;
    }
    
    function get($id){
	return $this->get_all($filter = array('id' => $id));
    }
    
    function user_exist($username){
	return count($this->get_all(array('username' => $username))) == 1;
    }
    
    function email_exist($email){
	return count($this->get_all(array('email' => $email))) == 1;
    }
    
    
    function get_all($filter=null){
	
	if(isset($filter['id']))
	    $this->mongoci->where('_id',  new MongoId($filter['id']));
	
	if(isset($filter['username']))
	    $this->mongoci->where('username',  $filter['username']);
	
	if(isset($filter['email']))
	    $this->mongoci->where('email',  $filter['email']);
	
	if(isset($filter['user']))
	    $this->mongoci->where('user',  $filter['user']);
	
	if(isset($filter['password']))
	    $this->mongoci->where('password',  sha1_salt($filter['password']));
	
	$this->mongoci->where('deleted',0);
        $this->mongoci->orderBy(array('created' => 'DESC'));
        
        return $this->mongoci->get('user');
        //$this->mongoci->lastQuery();
    }
    
    function save($user_id=null, $data_user=array(), $data_push=array()){
	$result = false;
	
	$last_update = array('time' => time(), 'ip' => $this->input->ip_address());
	
	if($user_id && $this->exist($user_id)){
	    //update
	    $this->mongoci->where('_id', new MongoId($user_id));
	    
	    if($data_user)
		$this->mongoci->set($data_user);
	    
	    $this->mongoci->set('last_update', $last_update);
	    $this->mongoci->push('log_last_update', $last_update);
	    
	    if($data_push)
		$this->mongoci->push($data_push);

	    $result = $this->mongoci->update('user');
	    
	}else{
	    //insert
	    $data_user['last_update'] = $last_update;
	    $data_user['log_last_update'] = array($last_update);
	    $data_user['created'] = time();
	    $data_user['deleted'] = 0;
	    
	    if(!$this->user_exist($data_user['username']) && !$this->email_exist($data_user['email']))	
		$result = $this->mongoci->insert('user', $data_user);
	    
	}
	return $result;
    }
    
    function delete($user_id=null){
	return $this->save($user_id, array('deleted' => 1));
    }
    
}
?> 
