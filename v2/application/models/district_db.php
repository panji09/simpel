<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class District_db extends CI_Model{
    function __construct(){
        // Call the Model constructor
        parent::__construct();
    }
    
   
    function get_all($filter=null){
        
        if(isset($filter['province_code'])&& $filter['province_code'] != 0)
                $this->mongoci->where('province_code',  (int)$filter['province_code']);
        
        if(isset($filter['district_code'])&& $filter['district_code'] != 0)
                $this->mongoci->where('district_code',  (int)$filter['district_code']);
        
        
        return $this->mongoci->get('district');
        //$this->mongoci->lastQuery();
    }
    
    function get($district_id){
	return $this->get_all(array('district_code' => $district_id));
    }
	
	

	

	

    
}
?> 
