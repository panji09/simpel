<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class type_training_db extends CI_Model{
    function __construct(){
        // Call the Model constructor
        parent::__construct();
    }
    
    function exist($type_training_id){
		return count($this->get_all(array('type_training_id' => $type_training_id))) == 1;
    }
    
    function get($type_training_id){
		return $this->get_all($filter = array('type_training_id' => $type_training_id));
    }
    
    function get_all($filter=null, $limit=null, $offset=null){
	
		if(isset($filter['created_lt']))
			$this->mongoci->whereLt('created', $filter['created_lt']);
			
		if(isset($filter['type_training_id']))
			$this->mongoci->where('_id', new MongoId($filter['type_training_id']));
		
		if(isset($filter['user_id']))
			$this->mongoci->where('author.user_id', $filter['user_id']);
		
		if(isset($filter['published']))
			$this->mongoci->where('published', $filter['published']);
		if($limit)
			$this->mongoci->limit($limit);
			if($offset)
			$this->mongoci->offset($offset);
			
		$this->mongoci->where('deleted',0);
		$this->mongoci->orderBy(array('created' => 'DESC'));
		
		return $this->mongoci->get('type_training');
		//$this->mongoci->lastQuery();
    }
    
    function save($type_training_id=null, $data_content=array(), $data_push=array()){
		$result = false;
		
		$last_update = array('time' => time(), 'ip' => $this->input->ip_address(), 'author' => $this->connect_auth->get_basic_me());
		
		if($type_training_id && $this->exist($type_training_id)){
			//update
			$this->mongoci->where('_id', new MongoId($type_training_id));
			$data_content['author'] = $this->connect_auth->get_basic_me();
			
			if($data_content)
			$this->mongoci->set($data_content);
			
			$this->mongoci->set('last_update', $last_update);
	// 	    $this->mongoci->push('log_last_update', $last_update);
			
			if($data_push)
			$this->mongoci->push($data_push);
	
			$result = $this->mongoci->update('type_training');
			
		}else{
			//insert
			$data_content['creator'] = $this->connect_auth->get_basic_me();
			$data_content['author'] = $data_content['creator'];
			
			$data_content['last_update'] = $last_update;
	// 	    $data_content['log_last_update'] = array($last_update);
			$data_content['created'] = time();
			$data_content['deleted'] = 0;
			
			$result = $this->mongoci->insert('type_training', $data_content);
			$type_training_id = ($result ? $result->{'$id'} : '');
		}
		
		if($type_training_id){
			$this->update_mysql($type_training_id);
		}
		
		return $result;
    }
    
    function delete($type_training_id=null){
		return $this->save($type_training_id, array('deleted' => 1));
    }
    
	function generate_data($row){
		
		$temp['id'] = $row['_id']->{'$id'};
		$temp['name'] = isset($row['name']) ? $row['name'] : '';
		$temp['published'] = isset($row['published']) ? $row['published'] : 0;
		
		return $temp;
	}
	
	function update_mysql($type_training_id = null){
		if($type_training_id){
			$query = $this->get($type_training_id);
			$data = $this->generate_data($query[0]);
			
			$this->db->where('id', $type_training_id);
			$this->db->from('type_training');
			
			if($this->db->count_all_results() == 1){
				//update
				$this->db->where('id', $type_training_id);
				$this->db->update('type_training', $data); 
			}else{
				//insert
				$this->db->insert('type_training', $data); 
			}
			
		}
	}
	function generate_mysql(){
		$query = $this->get_all();
		$this->db->empty_table('type_training');

		$data = array();
		foreach($query as $row){
			$data[] = $this->generate_data($row);
			
		}
		
		$this->db->insert_batch('type_training', $data);
	}
}
?> 
 
