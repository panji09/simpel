<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Logbook_db extends CI_Model{
    function __construct(){
        // Call the Model constructor
        parent::__construct();
    }
    
    function exist($logbook_id){
	return count($this->get_all(array('logbook_id' => $logbook_id))) >= 1;
    }
    
   
    function get($logbook_id){
	return $this->get_all($filter = array('logbook_id' => $logbook_id));
    }
    
   
    function get_all($filter=null, $limit=null, $offset=null){
	
	if(isset($filter['logbook_id']))
	    $this->mongoci->where('no_sertifikat', $filter['logbook_id']);
	
	if($limit)
	    $this->mongoci->limit($limit);
        if($offset)
	    $this->mongoci->offset($offset);
	    
	$this->mongoci->where('deleted',0);
        $this->mongoci->orderBy(array('created' => 'DESC'));
        
        return $this->mongoci->get('logbook');
        //$this->mongoci->lastQuery();
    }
    
    function save($logbook_id=null, $data_content=array(), $data_push=array()){
	$result = false;
	
	$last_update = array('time' => time(), 'ip' => $this->input->ip_address(), 'author' => $this->connect_auth->get_basic_me());
	
	if($logbook_id && $this->exist($logbook_id)){
	    //update
	    $this->mongoci->where('no_sertifikat', $logbook_id);
	    
	    if($data_content)
		$this->mongoci->set($data_content);
	    
	    $this->mongoci->set('last_update', $last_update);
	    $data_content['author'] = $this->connect_auth->get_basic_me();
	    
	    if($data_push)
		$this->mongoci->push($data_push);

	    $result = $this->mongoci->update('logbook');
	    
	}else{
	    //insert
	    $data_content['creator'] = $this->connect_auth->get_basic_me();
	    $data_content['author'] = $data_content['creator'];
	    
	    $data_content['last_update'] = $last_update;
// 	    $data_content['log_last_update'] = array($last_update);
	    $data_content['created'] = time();
	    $data_content['deleted'] = 0;
	    
	    $result = $this->mongoci->insert('logbook', $data_content);
	    
	}
	
	return $result;
    }
    
    function delete($logbook_id=null){
	return $this->save($logbook_id, array('deleted' => 1));
    }
    
}
?> 
 
 
 
