<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Init_config_db extends CI_Model{
    function __construct(){
        // Call the Model constructor
        parent::__construct();
    }
    
    function get(){
		$query = $this->mongoci->get('init_config');
		return $query[0];
    }
    function get_accreditation_level(){
		return $this->get()['accreditation_level'];
    }
    
    function get_type_evaluation($type_evaluation_id = null){
		if($type_evaluation_id){
			$query = $this->get()['type_evaluation'];
			$output = array();
			foreach($query as $row){
				if($row['id'] == $type_evaluation_id){
					$output = $row['name'];
				}
			}
			
			return $output;
		}else
			return $this->get()['type_evaluation'];
    }
    
    function get_type_training($type_training_id = null){
	// 	return $this->get()['type_training'];
		
		if($type_training_id){
			$query = $this->get()['type_training'];
			$output = array();
			foreach($query as $row){
				if($row['id'] == $type_training_id){
					$output = $row['name'];
				}
			}
			
			return $output;
		}else
			return $this->get()['type_training'];
    }
    function get_type_submission_training($type_submission_training_id = null){
	// 	return $this->get()['type_submission_training'];
		
		if($type_submission_training_id){
			$query = $this->get()['type_submission_training'];
			$output = array();
			foreach($query as $row){
				if($row['id'] == $type_submission_training_id){
					$output = $row['name'];
				}
			}
			
			return $output;
		}else
			return $this->get()['type_submission_training'];
    }
	
    function get_type_registration_participant($type_registration_participant_id=null){
	// 	return $this->get()['type_registration_participant'];
		
		if($type_registration_participant_id){
			$query = $this->get()['type_registration_participant'];
			$output = array();
			foreach($query as $row){
				if($row['id'] == $type_registration_participant_id){
					$output = $row['name'];
				}
			}
			
			return $output;
		}else
			return $this->get()['type_registration_participant'];
    }
    
    function get_main_instructor($main_instructor_id = null){

		if($main_instructor_id){
			$query = $this->get()['main_instructor'];
			$output = array();
			foreach($query as $row){
				if($row['id'] == $main_instructor_id){
					$output = $row['name'];
				}
			}
			
			return $output;
		}else
			return $this->get()['main_instructor'];
    }
    
    function get_max_bussinesday_submission_training(){
		return $this->get()['max_bussinesday_submission_training'];
    }
	
	function generate_data($row){
		
		if(isset($row['id'])){
			$temp['id'] = $row['id'];
		}
		
		if(isset($row['name'])){
			$temp['name'] = isset($row['name']) ? $row['name'] : '';
		}
		
		if(isset($row['published'])){
			$temp['published'] = isset($row['published']) ? $row['published'] : 0;
		}
		
		if(isset($row['day'])){
			$temp['day'] = isset($row['day']) ? $row['day'] : 0;
		}
		
		return $temp;
	}
	function generate_mysql(){
		$query = $this->get();
		unset($query['_id']);
		unset($query['type_training']);
		//print_r($query); exit();
		foreach($query as $table => $value){
			
			if($table == 'max_bussinesday_submission_training'){
				$this->db->empty_table('init_config');
			}else{
				$this->db->empty_table($table);	
			}
				
		}
		
		foreach($query as $table => $value){
			
			if(in_array($table, array('type_registration_participant','type_submission_training', 'accreditation_level', 'type_evaluation','main_instructor'))){
				$list_data = $value;
				$data = array();
				foreach($list_data as $row){
					$data[] = $this->generate_data($row);
				}
				
				$this->db->insert_batch($table, $data);
			}elseif($table == 'max_bussinesday_submission_training'){
				$data = array(
							  'name' => 'max_bussinesday_submission_training',
							  'value' => $value['day'],
							  'unit' => 'day'
							  );
				$this->db->insert('init_config', $data);
			}
		}
		
		
	}
//     
//     function get_kabkota(){
// 	return $this->get()['kabkota'];
//     }
//     
//     function get_kecamatan(){
// 	return $this->get()['kecamatan'];
//     }
}
?>