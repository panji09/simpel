<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Agenda_instructor_db extends CI_Model{
    function __construct(){
        // Call the Model constructor
        parent::__construct();
    }
    
    function exist($date_id){
	return count($this->get_all(array('date_id' => $date_id))) == 1;
    }
    
    function get($date_id){
	return $this->get_all($filter = array('date_id' => $date_id));
    }
    
    function get_all($filter=null, $limit=null, $offset=null){
	
	if(isset($filter['created_lt']))
	    $this->mongoci->whereLt('created', $filter['created_lt']);
	    
	if(isset($filter['date_id']))
	    $this->mongoci->where('date', $filter['date_id']);
	
	if(isset($filter['published']))
	    $this->mongoci->where('published', $filter['published']);
	
	if($limit)
	    $this->mongoci->limit($limit);
        if($offset)
	    $this->mongoci->offset($offset);
	    
	$this->mongoci->where('deleted',0);
        $this->mongoci->orderBy(array('created' => 'DESC'));
        
        return $this->mongoci->get('agenda_instructor');
        //$this->mongoci->lastQuery();
    }
    
    function save($date_id=null, $data_content=array(), $data_push=array()){
	$result = false;
	
	$last_update = array('time' => time(), 'ip' => $this->input->ip_address(), 'author' => $this->connect_auth->get_me());
	
	if($date_id && $this->exist($date_id)){
	    //update
	    $this->mongoci->where('date', $date_id);
	    
	    if($data_content)
		$this->mongoci->set($data_content);
	    
	    $this->mongoci->set('last_update', $last_update);
	    $data_content['author'] = $this->connect_auth->get_me();
	    if($data_push)
		$this->mongoci->push($data_push);

	    $result = $this->mongoci->update('agenda_instructor');
	    
	}else{
	    //insert
	    $data_content['creator'] = $this->connect_auth->get_me();
	    
	    $data_content['last_update'] = $last_update;
	    $data_content['created'] = time();
	    $data_content['deleted'] = 0;
	    
	    $result = $this->mongoci->insert('agenda_instructor', $data_content);
	    
	}
	
	return $result;
    }
    
    function save_instructor_agenda($date, $instructor_id, $training_id){
	$query = $this->get($date);
	$agenda = array();
	$exist = false;
	$temp_agenda = array();
	    
	if($query){
	    $result = $query[0];
	    $agenda = $result['agenda'];
	}
	
	$agenda_new = array(
	    'instructor_id' => $instructor_id,
	    'training_id' => $training_id
	);
	
	if($agenda){
	    
	    foreach($agenda as $row){
		
		if($agenda_new['instructor_id'] == $row['instructor_id']){
		    $temp_agenda[] = array(
			'instructor_id' => $agenda_new['instructor_id'],
			'training_id' => $agenda_new['training_id']
		    );
		    $exist = true;
		}else{
		    $temp_agenda[] = array(
			'instructor_id' => $row['instructor_id'],
			'training_id' => $row['training_id']
		    );
		}
		
	    }
	}
	
	$data_input['date'] = $date;
	
	if($exist){
	    $data_input['agenda'] = $temp_agenda;
	    
	}else{
	    
	    if($agenda){
		//data append
		$data_input['agenda'] = $agenda;
		$data_input['agenda'][] = $agenda_new;
		
	    }else{
		//data baru
		$data_input['agenda'][] = $agenda_new;
	    }
	    
	}
	
	return $this->save($date, $data_input);
    }
    
    function delete_agenda($date, $instructor_id){
	
	$agenda = array();
	$temp_agenda = array();
	$query = $this->get($date);
	if($query){
	    $result = $query[0];
	    $agenda = $result['agenda'];
	}
	
	if($agenda){
	    
	    foreach($agenda as $row){
		
		if($row['instructor_id'] != $instructor_id){
		    $temp_agenda[] = $row;
		}
	    }
	    
	    $agenda = $temp_agenda;
	    
	    return $this->save($date, $agenda);
	    
	}else{
	    return true;
	}
	
    }
    
    function delete($date_id=null){
	return $this->save($date_id, array('deleted' => 1));
    }
    
}
?> 
 
 
