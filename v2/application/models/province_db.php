<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Province_db extends CI_Model{
    function __construct(){
        // Call the Model constructor
        parent::__construct();
    }
    
   
    function get_all($filter=null){
        
       if(isset($filter['province_code'])&& $filter['province_code']!=0)
	    $this->mongoci->where('province_code', (int)$filter['province_code']);
        
        return $this->mongoci->get('province');
        //$this->mongoci->lastQuery();
    }
    
    function get($province_id){
	return $this->get_all(array('province_code' => $province_id));
    }

	

	

    
}
?> 
