<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Setting_db extends CI_Model{
    function __construct(){
        // Call the Model constructor
        parent::__construct();
    }
    
    function exist($setting_id){
	return count($this->get_all(array('setting_id' => $setting_id))) >= 1;
    }
    
    function get($setting_id){
	return $this->get_all($filter = array('setting_id' => $setting_id));
    }
    
    function get_all($filter=null, $limit=null, $offset=null){
	
	if(isset($filter['setting_id']))
	    $this->mongoci->where('setting_id', $filter['setting_id']);
	
	return $this->mongoci->get('setting');
    }
    
    function save($setting_id=null, $data_content=array(), $data_push=array()){
	$result = false;
	
	$last_update = array('time' => time(), 'ip' => $this->input->ip_address(), 'author' => $this->connect_auth->get_basic_me());
	
	if($setting_id && $this->exist($setting_id)){
	    //update
	    
	    $this->mongoci->where('setting_id', $setting_id);
	    
	    $data_content['author'] = $this->connect_auth->get_basic_me();
	    if($data_content)
		$this->mongoci->set($data_content);
	    
	    $this->mongoci->set('last_update', $last_update);
	    
	    if($data_push)
		$this->mongoci->push($data_push);

	    $result = $this->mongoci->update('setting');
	    
	}else{
	    //insert
	    $data_content['creator'] = $this->connect_auth->get_basic_me();
	    $data_content['author'] = $data_content['creator'];
	    
	    $data_content['last_update'] = $last_update;

	    $data_content['created'] = time();
	    $data_content['deleted'] = 0;
	    
	    $result = $this->mongoci->insert('setting', $data_content);
	    
	}
	
	return $result;
    }
    
    function delete($setting_id=null){
	return $this->save($setting_id, array('deleted' => 1));
    }
    
}
?> 
 
