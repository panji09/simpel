<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Evaluation_data_db extends CI_Model{
    function __construct(){
        // Call the Model constructor
        parent::__construct();
    }
    
    function exist($evaluation_data_id){
		return count($this->get_all(array('evaluation_data_id' => $evaluation_data_id))) == 1;
    }
    
    function get($evaluation_data_id){
		return $this->get_all($filter = array('evaluation_data_id' => $evaluation_data_id));
    }
    
    function get_all($filter=null, $limit=null, $offset=null){
		
		if(isset($filter['evaluation_data_id']))
			$this->mongoci->where('_id', new MongoId($filter['evaluation_data_id']) );		
		
		if(isset($filter['created_lt']))
			$this->mongoci->whereLt('created', $filter['created_lt']);
			
		if(isset($filter['training_id']))
			$this->mongoci->where('training_id', $filter['training_id']);
		
		if(isset($filter['evaluation_form_id']))
			$this->mongoci->where('evaluation_form_id', $filter['evaluation_form_id']);
		
		if(isset($filter['user_id']))
			$this->mongoci->where('author.user_id', $filter['user_id']);
			
		if(isset($filter['published']))
			$this->mongoci->where('published', $filter['published']);
		if($limit)
			$this->mongoci->limit($limit);
			if($offset)
			$this->mongoci->offset($offset);
			
		$this->mongoci->where('deleted',0);
		$this->mongoci->orderBy(array('created' => 'DESC'));
		
		return $this->mongoci->get('evaluation_data');
		//$this->mongoci->lastQuery();
    }
    
    function save($evaluation_data_id=null, $data_content=array(), $data_push=array()){
		$result = false;
		
		$me = $this->connect_auth->get_basic_me();
		$last_update = array('time' => time(), 'ip' => $this->input->ip_address(), 'author' => $me);
		
		if($evaluation_data_id && $this->exist($evaluation_data_id)){
			//update
			$this->mongoci->where('_id', new MongoId($evaluation_data_id));
			$data_content['author'] =  $me;
			if($data_content)
			$this->mongoci->set($data_content);
			
			$this->mongoci->set('last_update', $last_update);
	// 	    $this->mongoci->push('log_last_update', $last_update);
			
			if($data_push)
			$this->mongoci->push($data_push);
	
			$result = $this->mongoci->update('evaluation_data');
			
		}else{
			//insert
			$data_content['creator'] = $me;
			$data_content['author'] = $data_content['creator'];
			$data_content['last_update'] = $last_update;
	// 	    $data_content['log_last_update'] = array($last_update);
			$data_content['created'] = time();
			$data_content['deleted'] = 0;
			
			$result = $this->mongoci->insert('evaluation_data', $data_content);
			$evaluation_data_id = ($result ? $result->{'$id'} : '');
		}
		
		if($evaluation_data_id){
			$this->update_mysql($evaluation_data_id);
		}
		
		return $result;
    }
    
    function delete($evaluation_data_id=null){
		return $this->save($evaluation_data_id, array('deleted' => 1));
    }
	
	function generate_data($row){
		
		$temp['id'] = $row['_id']->{'$id'};
		$temp['training_id'] = isset($row['training_id']) ? $row['training_id'] : '';
		
		$temp['evaluation_form_id'] = isset($row['evaluation_form_id']) ? $row['evaluation_form_id'] : '';
		$temp['feedback'] = isset($row['feedback']) ? $row['feedback'] : '';
		
		$temp['published'] = isset($row['published']) ? $row['published'] : '';
		$temp['created'] = isset($row['created']) ? $row['created'] : '';
		$temp['deleted'] = isset($row['deleted']) ? $row['deleted'] : '';
		
		return $temp;
	}
	
	function generate_data_question($row){
		$data = array();
		if(isset($row['data']) && count($row['data'])){
			for($i=0; $i<count($row['data']); $i++){
				
				for($j=0; $j<count($row['data'][$i]); $j++){
					
					for($k=0; $k<count($row['data'][$i][$j]); $k++){
						$temp['evaluation_data_id'] = $row['_id']->{'$id'};
						$temp['answer'] = isset($row['data'][$i][$j][$k]) ? $row['data'][$i][$j][$k] : '';
						$temp['section_index'] = $i;
						$temp['question_index'] = $j;
						
						$data[] = $temp;	
					}
					
				}
				
			}	
		}
		
		return $data;
	}
	
	function update_mysql($evaluation_data_id = null){
		if($evaluation_data_id){
			$query = $this->get($evaluation_data_id);
			$data = $this->generate_data($query[0]);
			
			$this->db->where('id', $evaluation_data_id);
			$this->db->from('evaluation_data');
			
			if($this->db->count_all_results() == 1){
				//update
				$this->db->where('id', $evaluation_data_id);
				$this->db->update('evaluation_data', $data); 
			}else{
				//insert
				$this->db->insert('evaluation_data', $data); 
			}
			
			//hapus semua
			$this->db->where('evaluation_data_id', $evaluation_data_id);
			$this->db->delete('evaluation_data_question');
			
			//insert yang baru
			$data_question = $this->generate_data_question($query[0]);
			$this->db->insert_batch('evaluation_data_question', $data_question);
		}
	}
	function generate_mysql(){
		$query = $this->get_all();
		$this->db->empty_table('evaluation_data');
		$this->db->empty_table('evaluation_data_question');
		$data = $data_question = $temp = array();
		foreach($query as $row){
			$data[] = $this->generate_data($row);
			
			$temp = $this->generate_data_question($row);
			$data_question = array_merge($data_question, $temp);
		}
		
		$this->db->insert_batch('evaluation_data', $data);
		$this->db->insert_batch('evaluation_data_question', $data_question);
		
	}
    
}
?> 
