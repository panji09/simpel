<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Hour_leasson_db extends CI_Model{
    function __construct(){
        // Call the Model constructor
        parent::__construct();
    }
    
    function exist($hour_leasson_id){
		return count($this->get_all(array('hour_leasson_id' => $hour_leasson_id))) == 1;
    }
    
    function get($hour_leasson_id){
		return $this->get_all($filter = array('hour_leasson_id' => $hour_leasson_id));
    }
    
    function get_default_jp($instructor_id=null){
		return $this->get_all($filter = array(
			'instructor_id' => $instructor_id,
			'training_id' => 'default_jp',
			'date_assignment_year' => date('Y')
		));
    }
    
    function get_count($instructor_id=null, $year=null){
		$result = 0;
		
		$query = $this->get_all($filter = array(
			'instructor_id' => $instructor_id,
			'date_assignment_year' => $year,
			'instructor_approved' => 1,
			'admin_approved' => 1
		));
		
		if($query){
			foreach($query as $row){
			$result += $row['hour_leasson'];
			}
		}
		
		return $result;
    }
    
    function exist_training_instructor($training_id=null, $instructor_id=null){
		$query = $this->get_all(array(
			'training_id' => $training_id, 
			'instructor_id' => $instructor_id
		));
		
		if(count($query) == 1){
			$result = $query[0];
			
			return $result['_id']->{'$id'};
		}
		return null;
    }
    
    function get_all($filter=null, $limit=null, $offset=null){
	
		if(isset($filter['created_lt']))
			$this->mongoci->whereLt('created', $filter['created_lt']);
			
		if(isset($filter['hour_leasson_id']))
			$this->mongoci->where('_id', new MongoId($filter['hour_leasson_id']));
		
		if(isset($filter['date_assignment']))
			$this->mongoci->where('date_assignment.date', $filter['date_assignment']);
		
		if(isset($filter['date_assignment_year']))
			$this->mongoci->where('date_assignment.year', $filter['date_assignment_year']);
		
		if(isset($filter['date_assignment_month']))
			$this->mongoci->where('date_assignment.month', $filter['date_assignment_month']);
		
		if(isset($filter['date_assignment_day']))
			$this->mongoci->where('date_assignment.day', $filter['date_assignment_day']);
		
		if(isset($filter['user_id']))
			$this->mongoci->where('creator.user_id', $filter['user_id']);
		
		if(isset($filter['instructor_id']))
			$this->mongoci->where('instructor_id', $filter['instructor_id']);
		
		if(isset($filter['instructor_approved']))
			$this->mongoci->where('instructor_approved', $filter['instructor_approved']);
		
		if(isset($filter['admin_approved']))
			$this->mongoci->where('admin_approved', $filter['admin_approved']);
		
		if(isset($filter['admin_approved_ne']))
			$this->mongoci->whereNe('admin_approved', $filter['admin_approved_ne']);
		
		if(isset($filter['training_id']))
			$this->mongoci->where('training_id', $filter['training_id']);
		
		if(isset($filter['published']))
			$this->mongoci->where('published', $filter['published']);
		if($limit)
			$this->mongoci->limit($limit);
			if($offset)
			$this->mongoci->offset($offset);
			
		$this->mongoci->where('deleted',0);
		$this->mongoci->orderBy(array('created' => 'DESC'));
		
		return $this->mongoci->get('hour_leasson');
		//$this->mongoci->lastQuery();
    }
    
    function save($hour_leasson_id=null, $data_content=array(), $data_push=array()){
		$result = false;
		
		$last_update = array('time' => time(), 'ip' => $this->input->ip_address(), 'author' => $this->connect_auth->get_basic_me());
		
		if($hour_leasson_id && $this->exist($hour_leasson_id)){
			
			//update
			$this->mongoci->where('_id', new MongoId($hour_leasson_id));
			$data_content['author'] = $this->connect_auth->get_basic_me();
			if($data_content)
			$this->mongoci->set($data_content);
			
			
			$this->mongoci->set('last_update', $last_update);
	// 	    $this->mongoci->push('log_last_update', $last_update);
			
			if($data_push)
			$this->mongoci->push($data_push);
			
			
			
			$result = $this->mongoci->update('hour_leasson');
			
			//update ke training_db
			if($result && isset($data_content['training_id'])){
				$this->update_training_db($data_content['training_id'], array('select_instructor_finished' => 1));
			}
			
			
		}else{
			//insert
			$data_content['creator'] = $this->connect_auth->get_basic_me();
			$data_content['author'] = $data_content['creator'];
			
			$data_content['last_update'] = $last_update;
	// 	    $data_content['log_last_update'] = array($last_update);
			$data_content['created'] = time();
			$data_content['deleted'] = 0;
			
			
			$result = $this->mongoci->insert('hour_leasson', $data_content);
			$hour_leasson_id = ($result ? $result->{'$id'} : '');
			
			//update ke training_db
			if($result && isset($data_content['training_id'])){
				$this->update_training_db($data_content['training_id'], array('select_instructor_finished' => 1));
			}
		}
		
		if($hour_leasson_id){
			$this->update_mysql($hour_leasson_id);
		}
		
		return $result;
    }
    
    function delete($hour_leasson_id=null){
		return $this->save($hour_leasson_id, array('deleted' => 1));
    }
    
    function update_training_db($training_id=null, $data_input=array()){
		$CI =& get_instance();
		
		
		if($training_id !='default_jp' && $CI->training_db->exist($training_id)){
			$CI->training_db->save($training_id, $data_input);
		}
	
    }
	
	function generate_data($row){
		$temp['id'] = $row['_id']->{'$id'};
		$temp['training_id'] = isset($row['training_id']) ? $row['training_id'] : '';
		$temp['instructor_id'] = isset($row['instructor_id']) ? $row['instructor_id'] : '';
		$temp['hour_leasson'] = isset($row['hour_leasson']) ? intval($row['hour_leasson']) : null;
		$temp['date_assignment'] = isset($row['date_assignment']['date']) ? $row['date_assignment']['date'] : null;
		$temp['submission_date'] = isset($row['submission_date']) ? json_encode($row['submission_date']) : '';
		$temp['submission_time_range'] = isset($row['submission_time_range']) ? json_encode($row['submission_time_range']) : '';
		
		$temp['instructor_approved'] = isset($row['instructor_approved']) ? $row['instructor_approved'] : 0;
		$temp['admin_approved'] = isset($row['admin_approved']) ? $row['admin_approved'] : 0;
		
		$temp['created'] = isset($row['created']) ? $row['created'] : 0;
		$temp['deleted'] = isset($row['deleted']) ? $row['deleted'] : 0;
		
		return $temp;
	}
	
	function update_mysql($hour_leasson_id = null){
		if($hour_leasson_id){
			$query = $this->get($hour_leasson_id);
			$data = $this->generate_data($query[0]);
			
			$this->db->where('id', $hour_leasson_id);
			$this->db->from('hour_leasson');
			
			if($this->db->count_all_results() == 1){
				//update
				$this->db->where('id', $hour_leasson_id);
				$this->db->update('hour_leasson', $data); 
			}else{
				//insert
				$this->db->insert('hour_leasson', $data); 
			}
			
		}
	}
	function generate_mysql(){
		$query = $this->get_all();
		$this->db->empty_table('hour_leasson');

		$data = array();
		foreach($query as $row){
			$data[] = $this->generate_data($row);
			
		}
		
		$this->db->insert_batch('hour_leasson', $data);
	}
}
?> 
 
