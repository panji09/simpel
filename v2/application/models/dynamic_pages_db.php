<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Dynamic_pages_db extends CI_Model{
    function __construct(){
        // Call the Model constructor
        parent::__construct();
    }
    
    function exist($content_id){
	return count($this->get_all(array('content_id' => $content_id))) == 1;
    }
    
    function get($content_id){
	return $this->get_all($filter = array('content_id' => $content_id));
    }
    
    function get_all($filter=null, $limit=null, $offset=null){
	
	if(isset($filter['created_lt']))
	    $this->mongoci->whereLt('created', $filter['created_lt']);
	    
	if(isset($filter['content_id']))
	    $this->mongoci->where('_id', new MongoId($filter['content_id']));
	
	if(isset($filter['user_id']))
	    $this->mongoci->where('author.user_id', $filter['user_id']);
	
	
	if(isset($filter['page_id']))
	    $this->mongoci->where('page_id', $filter['page_id']);
	
	if(isset($filter['published']))
	    $this->mongoci->where('published', $filter['published']);
	if($limit)
	    $this->mongoci->limit($limit);
        if($offset)
	    $this->mongoci->offset($offset);
	    
	$this->mongoci->where('deleted',0);
        $this->mongoci->orderBy(array('created' => 'DESC'));
        
        return $this->mongoci->get('pages');
        //$this->mongoci->lastQuery();
    }
    
    function save($content_id=null, $data_content=array(), $data_push=array()){
	$result = false;
	
	$last_update = array('time' => time(), 'ip' => $this->input->ip_address(), 'author' => $this->connect_auth->get_basic_me() );
	
	if($content_id && $this->exist($content_id)){
	    //update
	    $this->mongoci->where('_id', new MongoId($content_id));
	    $data_content['author'] = $this->connect_auth->get_basic_me();
	    if($data_content)
		$this->mongoci->set($data_content);
	    
	    $this->mongoci->set('last_update', $last_update);
// 	    $this->mongoci->push('log_last_update', $last_update);
	    
	    if($data_push)
		$this->mongoci->push($data_push);

	    $result = $this->mongoci->update('pages');
	    
	}else{
	    //insert
	    $data_content['creator'] = $this->connect_auth->get_basic_me();
	    $data_content['author'] = $data_content['creator'];
	    $data_content['last_update'] = $last_update;
// 	    $data_content['log_last_update'] = array($last_update);
	    $data_content['created'] = (isset($data_content['created']) ? $data_content['created'] : time());
	    $data_content['deleted'] = 0;
	    
	    $result = $this->mongoci->insert('pages', $data_content);
	    
	}
	
	return $result;
    }
    
    function delete($content_id=null){
	return $this->save($content_id, array('deleted' => 1));
    }
    
}
?> 
