<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Pengaduan_db extends CI_Model{
    function __construct(){
        // Call the Model constructor
        parent::__construct();
    }
    
   
    function get_all($filter=null){
		
		
                
		if(isset($filter['province_code'])&& $filter['province_code']!=0)
                $this->mongoci->where('province.id', $filter['province_code']); //rudiest
               
                if(isset($filter['district_code']) && $filter['district_code']!=0)
                $this->mongoci->where('district.id',  $filter['district_code']);
				
				if(isset($filter['code_email_verification']))
			    $this->mongoci->where('code_email_verification', $filter['code_email_verification']);

                
                if(isset($filter['_id']))
                $this->mongoci->where('_id', new MongoId($filter['_id']));
                
                if(isset($filter['publish']))
                $this->mongoci->where('publish',$filter['publish']);
				
				if(isset($filter['verification']))
                $this->mongoci->where('verification',$filter['verification']);
                
                if(isset($filter['year'])&& $filter['year']!=0)
                $this->mongoci->where('year',$filter['year']);
              
               //keyword
                if(isset($filter['keyword'])&& $filter['keyword']!=0)
                {
                    $regex = '/' . $filter['keyword'] . '/i';

                    $search_keyword = array(
                    'complaint_description' => new MongoRegex($regex),
                    );
                    $this->mongoci->orWhere($search_keyword);

                }
                
                if (isset($filter['complaint_category'])&& $filter['complaint_category']!=0)
                 $this->mongoci->where('category',$filter['complaint_category']);
                    
                
                
                $this->mongoci->where('deleted',0);
                
        return $this->mongoci->get('pengaduan');
        //$this->mongoci->lastQuery();
    }
    
    function exist($complaint_id){
	return count($this->get_all(array('_id' => $complaint_id))) == 1;
    }
	
    function save($complaint_id=null, $data_complaint=array(), $data_push=array()){
		$result = false;
			
		   // var_dump($distribution_id);
		
		$last_update = array('time' => time(), 'ip' => $this->input->ip_address());
		
			
			
		if($complaint_id && $this->exist($complaint_id)){
			//update
			$this->mongoci->where('_id', new MongoId($complaint_id));
			
			if($data_complaint)
			$this->mongoci->set($data_complaint);
			
			$this->mongoci->set('last_update', $last_update);
	// 	    $this->mongoci->push('log_last_update', $last_update);
			
			if($data_push)
			$this->mongoci->push($data_push);
	
			$result = $this->mongoci->update('pengaduan');
			
		}else{
			//insert
			$data_complaint['last_update'] = $last_update;
	// 	    $data_content['log_last_update'] = array($last_update);
			$data_complaint['created'] = time();
			$data_complaint['deleted'] = 0;
			
			$result = $this->mongoci->insert('pengaduan', $data_complaint);
			
		}
		
		return $result;
    }
	
    function delete($complaint_id=null){
	return $this->save($complaint_id, array('deleted' => 1));
    }	

    
}
?> 
