<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Join_training_db extends CI_Model{
    function __construct(){
        // Call the Model constructor
        parent::__construct();
    }
    
    function exist($join_training_id){
		return count($this->get_all(array('join_training_id' => $join_training_id))) == 1;
    }
    
    function user_exist($training_id, $user_join_id){
		return count($this->get_all(
			  array(
				  'training_id' => $training_id, 
				  'user_join_id' => $user_join_id 
			  ))) >= 1;
    }
    
    function get($join_training_id){
		return $this->get_all($filter = array('join_training_id' => $join_training_id));
    }
    
    function count_approved_participant($training_id){
		return count($this->get_all(array('institute_approved' => 1, 'training_id' => $training_id)));
    }
    
    function get_all($filter=null, $limit=null, $offset=null){
	
		if(isset($filter['created_lt']))
			$this->mongoci->whereLt('created', $filter['created_lt']);
			
		if(isset($filter['join_training_id']))
			$this->mongoci->where('_id', new MongoId($filter['join_training_id']));
		
		if(isset($filter['training_id']))
			$this->mongoci->where('training_id', $filter['training_id']);
		
		if(isset($filter['user_join_id']))
			$this->mongoci->where('user_join.user_id', $filter['user_join_id']);
		
		if(isset($filter['training_id_in']))
			$this->mongoci->whereIn('training_id', $filter['training_id_in']);
		
		if(isset($filter['institute_approved_ne']))
			$this->mongoci->whereNe('institute_approved', $filter['institute_approved_ne']);
		
		if(isset($filter['institute_approved']))
			$this->mongoci->where('institute_approved', $filter['institute_approved']);
		
		if(isset($filter['published']))
			$this->mongoci->where('published', $filter['published']);
		if($limit)
			$this->mongoci->limit($limit);
			if($offset)
			$this->mongoci->offset($offset);
			
		$this->mongoci->where('deleted',0);
		$this->mongoci->orderBy(array('created' => 'DESC'));
		
		return $this->mongoci->get('join_training');
		//$this->mongoci->lastQuery();
    }
    
    function save($join_training_id=null, $data_content=array(), $data_push=array()){
		$result = false;
		
		$last_update = array('time' => time(), 'ip' => $this->input->ip_address(), 'author' => $this->connect_auth->get_basic_me());
		
		if($join_training_id && $this->exist($join_training_id)){
			//update
			$this->mongoci->where('_id', new MongoId($join_training_id));
			
			if($data_content)
			$this->mongoci->set($data_content);
			
			$this->mongoci->set('last_update', $last_update);
	// 	    $this->mongoci->push('log_last_update', $last_update);
			$data_content['author'] = $this->connect_auth->get_basic_me();
			
			if($data_push)
			$this->mongoci->push($data_push);
	
			$result = $this->mongoci->update('join_training');
			
		}else{
			//insert
			$data_content['creator'] = $this->connect_auth->get_basic_me();
			$data_content['author'] = $data_content['creator'];
			
			$data_content['last_update'] = $last_update;
	// 	    $data_content['log_last_update'] = array($last_update);
			$data_content['created'] = time();
			$data_content['deleted'] = 0;
			
			$result = $this->mongoci->insert('join_training', $data_content);
			$join_training_id = ($result ? $result->{'$id'} : '');
		}
		
		if($join_training_id){
			$this->update_mysql($join_training_id);
		}
		
		return $result;
    }
    
    function delete($join_training_id=null){
		return $this->save($join_training_id, array('deleted' => 1));
    }
    
	function generate_data($row){
		
		$temp['id'] = $row['_id']->{'$id'};
		$temp['letter_of_assignment_id'] = isset($row['letter_of_assignment_id']) ? $row['letter_of_assignment_id'] : '';
		$temp['letter_of_assignment_upload'] = isset($row['letter_of_assignment_upload']) ? $row['letter_of_assignment_upload'] : '';
		$temp['certificate_procurement_expert_id'] = isset($row['certificate_procurement_expert_id']) ? $row['certificate_procurement_expert_id'] : '';
		$temp['certificate_procurement_expert_upload'] = isset($row['certificate_procurement_expert_upload']) ? $row['certificate_procurement_expert_upload'] : '';
		$temp['require_document_name'] = isset($row['require_document_name']) ? json_encode($row['require_document_name']) : '';
		$temp['require_document_upload'] = isset($row['require_document_upload']) ? json_encode($row['require_document_upload']) : '';
		
		$temp['participant_user_id'] = isset($row['user_join']['user_id']) ? $row['user_join']['user_id'] : '';
		$temp['training_id'] = isset($row['training_id']) ? $row['training_id'] : '';
		$temp['institute_approved'] = isset($row['institute_approved']) ? $row['institute_approved'] : '';
		
		$temp['created'] = isset($row['created']) ? $row['created'] : '';
		$temp['deleted'] = isset($row['deleted']) ? $row['deleted'] : '';
		
		return $temp;
	}
	
	function update_mysql($join_training_id = null){
		if($join_training_id){
			$query = $this->get($join_training_id);
			$data = $this->generate_data($query[0]);
			
			$this->db->where('id', $join_training_id);
			$this->db->from('join_training');
			
			if($this->db->count_all_results() == 1){
				//update
				$this->db->where('id', $join_training_id);
				$this->db->update('join_training', $data); 
			}else{
				//insert
				$this->db->insert('join_training', $data); 
			}
			
		}
	}
	function generate_mysql(){
		$query = $this->get_all();
		$this->db->empty_table('join_training');

		$data = array();
		foreach($query as $row){
			$data[] = $this->generate_data($row);
			
		}
		
		$this->db->insert_batch('join_training', $data);
	}
}
?> 
 
 
 
