<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Evaluation_db extends CI_Model{
    function __construct(){
        // Call the Model constructor
        parent::__construct();
    }
    
    function exist($evaluation_id){
		return count($this->get_all(array('evaluation_id' => $evaluation_id))) == 1;
    }
    
    function get($evaluation_id){
		return $this->get_all($filter = array('evaluation_id' => $evaluation_id));
    }
    
    function get_all($filter=null, $limit=null, $offset=null){
	
		if(isset($filter['created_lt']))
			$this->mongoci->whereLt('created', $filter['created_lt']);
			
		if(isset($filter['evaluation_id']))
			$this->mongoci->where('_id', new MongoId($filter['evaluation_id']));
		
		if(isset($filter['user_id']))
			$this->mongoci->where('author.user_id', $filter['user_id']);
			
		if(isset($filter['published']))
			$this->mongoci->where('published', $filter['published']);
		if($limit)
			$this->mongoci->limit($limit);
		if($offset)
			$this->mongoci->offset($offset);
			
		$this->mongoci->where('deleted',0);
		$this->mongoci->orderBy(array('created' => 'DESC'));
			
		return $this->mongoci->get('evaluation');
		//$this->mongoci->lastQuery();
    }
    
    function save($evaluation_id=null, $data_content=array(), $data_push=array()){
		$result = false;
		
		$me = $this->connect_auth->get_basic_me();
		$last_update = array('time' => time(), 'ip' => $this->input->ip_address(), 'author' => $me);
		
		if($evaluation_id && $this->exist($evaluation_id)){
			//update
			$this->mongoci->where('_id', new MongoId($evaluation_id));
			
			if($data_content)
			$this->mongoci->set($data_content);
			
			$this->mongoci->set('last_update', $last_update);
			$this->mongoci->set('author', $me);
			
	// 	    $this->mongoci->push('log_last_update', $last_update);
			
			if($data_push)
			$this->mongoci->push($data_push);
	
			$result = $this->mongoci->update('evaluation');
			
		}else{
			//insert
			$data_content['last_update'] = $last_update;
			$data_content['creator'] = $me;
			$data_content['author'] = $data_content['creator'];
			$data_content['created'] = time();
			$data_content['deleted'] = 0;
			
			$result = $this->mongoci->insert('evaluation', $data_content);
			$evaluation_id = ($result ? $result->{'$id'} : '');
		}
		
		if($evaluation_id){
			$this->update_mysql($evaluation_id);
		}
		return $result;
    }
    
    function delete($evaluation_id=null){
		return $this->save($evaluation_id, array('deleted' => 1));
    }
	
	
	
	function generate_data($row){
		
		$temp['id'] = $row['_id']->{'$id'};
		$temp['name'] = isset($row['name']) ? $row['name'] : '';
		
		$temp['type_evaluation_id'] = isset($row['type_evaluation']['id']) ? $row['type_evaluation']['id'] : '';
		$temp['type_evaluation_name'] = isset($row['type_evaluation']['name']) ? $row['type_evaluation']['name'] : '';
		
		$temp['published'] = isset($row['published']) ? $row['published'] : '';
		$temp['created'] = isset($row['created']) ? $row['created'] : '';
		$temp['deleted'] = isset($row['deleted']) ? $row['deleted'] : '';
		
		return $temp;
	}
	
	function generate_data_question($row){
		
		if(isset($row['question']) && count($row['question'])){
			for($i=0; $i<count($row['section']); $i++){
				
				for($j=0; $j<count($row['question'][$i]); $j++){
					$temp['evaluation_id'] = $row['_id']->{'$id'};
					$temp['section'] = isset($row['section'][$i]) ? $row['section'][$i] : '';
					$temp['question'] = isset($row['question'][$i][$j]) ? $row['question'][$i][$j] : '';
					$temp['section_index'] = $i;
					$temp['question_index'] = $j;
					
					$data[] = $temp;
				}
				
			}	
		}
		
		return $data;
	}
	
	function update_mysql($evaluation_id = null){
		if($evaluation_id){
			$query = $this->get($evaluation_id);
			$data = $this->generate_data($query[0]);
			
			$this->db->where('id', $evaluation_id);
			$this->db->from('evaluation');
			
			if($this->db->count_all_results() == 1){
				//update
				$this->db->where('id', $evaluation_id);
				$this->db->update('evaluation', $data); 
			}else{
				//insert
				$this->db->insert('evaluation', $data); 
			}
			
			//hapus semua
			$this->db->where('evaluation_id', $evaluation_id);
			$this->db->delete('evaluation_question');
			
			//insert yang baru
			$data_question = $this->generate_data_question($query[0]);
			$this->db->insert_batch('evaluation_question', $data_question);
		}
	}
	function generate_mysql(){
		$query = $this->get_all();
		$this->db->empty_table('evaluation');
		$this->db->empty_table('evaluation_question');
		$data = $data_question = $temp = array();
		foreach($query as $row){
			$data[] = $this->generate_data($row);
			
			$temp = $this->generate_data_question($row);
			$data_question = array_merge($data_question, $temp);
		}
		
		$this->db->insert_batch('evaluation', $data);
		$this->db->insert_batch('evaluation_question', $data_question);
		
	}
    
}
?> 
