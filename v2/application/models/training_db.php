<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Training_db extends CI_Model{
    function __construct(){
        // Call the Model constructor
        parent::__construct();
    }
    
    function exist($training_id){
		return count($this->get_all(array('training_id' => $training_id))) == 1;
    }
    
    function get($training_id){
		return $this->get_all($filter = array('training_id' => $training_id));
    }
    
    function get_all($filter=null, $limit=null, $offset=null){
	
		if(isset($filter['created_lt']))
			$this->mongoci->whereLt('created', $filter['created_lt']);
			
		
		if(isset($filter['training_id']))
			$this->mongoci->where('_id', new MongoId($filter['training_id']));
		
		if(isset($filter['user_id']))
			$this->mongoci->where('creator.user_id', $filter['user_id']);
		
		if(isset($filter['facilitator_user_id']))
			$this->mongoci->where('facilitator_user_id', $filter['facilitator_user_id']);
		
		if(isset($filter['search_lpp'])){
			$regex = '/' . $filter['search_lpp'] . '/i';
	    
			$search_name = array(
			'user_submission.training_institute' => new MongoRegex($regex),
			);
			$this->mongoci->orWhere($search_name);
		}
		
		if(isset($filter['training_location_province']))
			$this->mongoci->where('training_location_province.id', $filter['training_location_province']);
		
		if(isset($filter['training_name_id']))
			$this->mongoci->where('training_name.id', $filter['training_name_id']);
		
		if(isset($filter['type_submission']))
			$this->mongoci->where('type_submission', $filter['type_submission']);
		
		if(isset($filter['select_instructor_finished']))
			$this->mongoci->where('select_instructor_finished', $filter['select_instructor_finished']);
		
		
		if(isset($filter['verified_submission_training']))
			$this->mongoci->where('verified_submission_training', $filter['verified_submission_training']);
		
		if(isset($filter['verified_submission_training_in']))
			$this->mongoci->whereIn('verified_submission_training', $filter['verified_submission_training_in']);
		
		if(isset($filter['verified_submission_training_ne']))
			$this->mongoci->whereNe('verified_submission_training', $filter['verified_submission_training_ne']);
		
		if(isset($filter['draft']))
			$this->mongoci->where('draft', $filter['draft']);
		
		if(isset($filter['published']))
			$this->mongoci->where('published', $filter['published']);
		if($limit)
			$this->mongoci->limit($limit);
			if($offset)
			$this->mongoci->offset($offset);
			
		$this->mongoci->where('deleted',0);
		$this->mongoci->orderBy(array('created' => 'DESC'));
			
		return $this->mongoci->get('training');
		//$this->mongoci->lastQuery();
    }
    
    function save($training_id=null, $data_content=array(), $data_push=array()){
		$result = false;
		
		$last_update = array('time' => time(), 'ip' => $this->input->ip_address(), 'author' => $this->connect_auth->get_basic_me());
		
		if($training_id && $this->exist($training_id)){
			//update
			$this->mongoci->where('_id', new MongoId($training_id));
			$data_content['author'] = $this->connect_auth->get_basic_me();
			if($data_content)
			$this->mongoci->set($data_content);
			
			$this->mongoci->set('last_update', $last_update);
	// 	    $this->mongoci->push('log_last_update', $last_update);
			
			if($data_push)
			$this->mongoci->push($data_push);
	
			$result = $this->mongoci->update('training');
			
			
		}else{
			//insert
			$data_content['creator'] = $this->connect_auth->get_basic_me();
			$data_content['author'] = $data_content['creator'];
			$data_content['last_update'] = $last_update;
	// 	    $data_content['log_last_update'] = array($last_update);
			$data_content['created'] = time();
			$data_content['deleted'] = 0;
			
			$result = $this->mongoci->insert('training', $data_content);
			$training_id = ($result ? $result->{'$id'} : '');
		}
		
		if($training_id){
			$this->update_mysql($training_id);
		}
		return $result;
    }
    
    function delete($training_id=null){
		return $this->save($training_id, array('deleted' => 1));
    }
    
	function generate_data($row){
		
		$temp['id'] = $row['_id']->{'$id'};
		$temp['mail_submission_id'] = isset($row['mail_submission_id']) ? $row['mail_submission_id'] : '';
		$temp['mail_submission_upload'] = isset($row['mail_submission_upload']) ? $row['mail_submission_upload'] : '';
		
		
		$temp['type_submission_training_id'] = isset($row['type_submission_training']['id']) ? $row['type_submission_training']['id'] : '';
		$temp['type_submission_training_name'] = isset($row['type_submission_training']['name']) ? $row['type_submission_training']['name'] : '';
		
		$temp['type_training_id'] = isset($row['type_training']['id']) ? $row['type_training']['id'] : '';
		$temp['type_training_name'] = isset($row['type_submission_training']['name']) ? $row['type_submission_training']['name'] : '';
		
		$temp['training_date'] = isset($row['training_date']) ? $row['training_date'] : '';
		
		$training_date = array();
		if($temp['training_date']){
			$training_date = explode(" - ", $temp['training_date']);
		}
		$temp['training_date_start'] = isset($training_date[0]) ? $training_date[0] : null;
		$temp['training_date_end'] = isset($training_date[1]) ? $training_date[1] : null;
		
		$temp['start_training_time'] = isset($row['start_training_time']) ? $row['start_training_time'] : '';
		$temp['training_location_address'] = isset($row['training_location_address']) ? $row['training_location_address'] : '';
		
		
		$temp['training_location_province_id'] = isset($row['training_location_province']['id']) ? $row['training_location_province']['id'] : '';
		$temp['training_location_province_name'] = isset($row['training_location_province']['name']) ? $row['training_location_province']['name'] : '';
		
		$temp['training_location_district_id'] = isset($row['training_location_district']['id']) ? $row['training_location_district']['id'] : '';
		$temp['training_location_district_name'] = isset($row['training_location_district']['name']) ? $row['training_location_district']['name'] : '';
		
		$temp['class_amount'] = isset($row['class_amount']) ? intval($row['class_amount']) : null;
		
		$temp['type_registration_participant_id'] = isset($row['type_registration_participant']['id']) ? $row['type_registration_participant']['id'] : '';
		$temp['type_registration_participant_name'] = isset($row['type_registration_participant']['name']) ? $row['type_registration_participant']['name'] : '';
		
		$temp['participant_amount'] = isset($row['participant_amount']) ? intval($row['participant_amount']) : null;
		$temp['contact_person_name'] = isset($row['contact_person_name']) ? $row['contact_person_name'] : '';
		$temp['contact_person_phone'] = isset($row['contact_person_phone']) ? $row['contact_person_phone'] : '';
		$temp['institute_user_id'] = isset($row['creator']['user_id']) ? $row['creator']['user_id'] : '';
		
		$temp['created'] = isset($row['created']) ? $row['created'] : '';
		$temp['deleted'] = isset($row['deleted']) ? $row['deleted'] : '';
		
		
		return $temp;
	}
	
	function update_mysql($training_id = null){
		if($training_id){
			$query = $this->get($training_id);
			$data = $this->generate_data($query[0]);
			
			$this->db->where('id', $training_id);
			$this->db->from('training');
			
			if($this->db->count_all_results() == 1){
				//update
				$this->db->where('id', $training_id);
				$this->db->update('training', $data); 
			}else{
				//insert
				$this->db->insert('training', $data); 
			}
			
		}
	}
	function generate_mysql(){
		$query = $this->get_all();
		$this->db->empty_table('training');

		$data = array();
		foreach($query as $row){
			$data[] = $this->generate_data($row);
		}
		
		$this->db->insert_batch('training', $data);
	}
}
?> 
