<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Score_training_db extends CI_Model{
    function __construct(){
        // Call the Model constructor
        parent::__construct();
    }
    
    function exist($score_training_id){
		return count($this->get_all(array('score_training_id' => $score_training_id))) == 1;
    }
    
    function get($score_training_id){
		return $this->get_all($filter = array('score_training_id' => $score_training_id));
    }
    
    function get_all($filter=null, $limit=null, $offset=null){
		
		if(isset($filter['score_training_id']))
			$this->mongoci->where('_id', new MongoId($filter['score_training_id']) );		
		
		if(isset($filter['created_lt']))
			$this->mongoci->whereLt('created', $filter['created_lt']);
			
		if(isset($filter['training_id']))
			$this->mongoci->where('training_id', $filter['training_id']);
		
		if(isset($filter['evaluation_form_id']))
			$this->mongoci->where('evaluation_form_id', $filter['evaluation_form_id']);
		
		if(isset($filter['user_id']))
			$this->mongoci->where('author.user_id', $filter['user_id']);
			
		if(isset($filter['published']))
			$this->mongoci->where('published', $filter['published']);
		if($limit)
			$this->mongoci->limit($limit);
			if($offset)
			$this->mongoci->offset($offset);
			
		$this->mongoci->where('deleted',0);
		$this->mongoci->orderBy(array('created' => 'DESC'));
		
		return $this->mongoci->get('score_training');
			//$this->mongoci->lastQuery();
    }
    
    function save($score_training_id=null, $data_content=array(), $data_push=array()){
		$result = false;
		
		$last_update = array('time' => time(), 'ip' => $this->input->ip_address(), 'author' => $this->connect_auth->get_basic_me());
		
		if($score_training_id && $this->exist($score_training_id)){
			//update
			$this->mongoci->where('_id', new MongoId($score_training_id));
			$data_content['author'] =  $this->connect_auth->get_basic_me();
			if($data_content)
			$this->mongoci->set($data_content);
			
			$this->mongoci->set('last_update', $last_update);
	// 	    $this->mongoci->push('log_last_update', $last_update);
			
			if($data_push)
			$this->mongoci->push($data_push);
	
			$result = $this->mongoci->update('score_training');
			
		}else{
			//insert
			$data_content['creator'] = $this->connect_auth->get_basic_me();
			$data_content['author'] = $data_content['creator'];
			$data_content['last_update'] = $last_update;
	// 	    $data_content['log_last_update'] = array($last_update);
			$data_content['created'] = time();
			$data_content['deleted'] = 0;
			
			$result = $this->mongoci->insert('score_training', $data_content);
			$score_training_id = ($result ? $result->{'$id'} : '');
		}
		
		if($score_training_id){
			$this->update_mysql($score_training_id);
		}
		return $result;
    }
    
    function delete($score_training_id=null){
		return $this->save($score_training_id, array('deleted' => 1));
    }
	
	function generate_data($row){
		$data = array();
		if(isset($row['data']) && count($row['data'])){
			foreach($row['data'] as $participant_user_id => $value){
				
				$temp['id'] = $row['_id']->{'$id'};
				$temp['training_id'] = isset($row['training_id']) ? $row['training_id'] : '';
				$temp['participant_user_id'] = isset($participant_user_id) ? $participant_user_id : '';
				$temp['pretest'] = isset($value['pretest']) ? intval($value['pretest']) : 0;
				$temp['posttest'] = isset($value['posttest']) ? intval($value['posttest']) : 0;
				
				$temp['published'] = isset($row['published']) ? $row['published'] : 0;
				$temp['created'] = isset($row['created']) ? $row['created'] : 0;
				$temp['deleted'] = isset($row['deleted']) ? $row['deleted'] : 0;
				
				$data[] = $temp;	
			}
		}
		
		
		
		
		return $data;
	}
	
	function update_mysql($score_training_id = null){
		if($score_training_id){
			$query = $this->get($score_training_id);
			$data = $this->generate_data($query[0]);
			
			$this->db->where('id', $score_training_id);
			$this->db->delete('score_training');
			
			//insert
			$this->db->insert_batch('score_training', $data); 
		}
	}
	function generate_mysql(){
		$query = $this->get_all();
		$this->db->empty_table('score_training');

		$data = array();
		foreach($query as $row){
			$temp = $this->generate_data($row);
			$data = array_merge($data, $temp);
			
		}
		
		$this->db->insert_batch('score_training', $data);
	}
    
}
?> 
 
