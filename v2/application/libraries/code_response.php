<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Code_response {
    function __construct(){
        $this->CI =& get_instance();
    }
    /*
    2xx Success
    */
    function code_200(){
	/*
	Standard response for successful HTTP requests. The actual response will depend on the request method used. In a GET request, the response will contain an entity corresponding to the requested resource. In a POST request the response will contain an entity describing or containing the result of the action.
	*/
	return array('code' => 200, 'message' => 'OK');
    }
    function code_201(){
	/*
	The request has been fulfilled and resulted in a new resource being created.
	*/
	return array('code' => 201, 'message' => 'Created');
    }
    function code_202(){
	/*
	The request has been accepted for processing, but the processing has not been completed. The request might or might not eventually be acted upon, as it might be disallowed when processing actually takes place.
	*/
	return array('code' => 202, 'message' => 'Accepted');
    }
    
    function code_204(){
	/*
	The server successfully processed the request, but is not returning any content. Usually used as a response to a successful delete request.
	*/
	return array('code' => 204, 'message' => 'No Content');
    }
    /*
    4xx Client Error
    */
    function code_400(){
	/*
	The request cannot be fulfilled due to bad syntax.
	*/
	return array('code' => 400, 'message' => 'Bad Request');
    }
    function code_401(){
	/*
	Similar to 403 Forbidden, but specifically for use when authentication is required and has failed or has not yet been provided. The response must include a WWW-Authenticate header field containing a challenge applicable to the requested resource. See Basic access authentication and Digest access authentication.
	*/
	return array('code' => 401, 'message' => 'Unauthorized');
    }
    function code_403(){
	/*
	The request was a valid request, but the server is refusing to respond to it. Unlike a 401 Unauthorized response, authenticating will make no difference.
	*/
	return array('code' => 403, 'message' => 'Forbidden');
    }
    function code_409(){
	/*
	Indicates that the request could not be processed because of conflict in the request, such as an edit conflict in the case of multiple updates.
	*/
	return array('code' => 409, 'message' => 'Conflict');
    }
    
    /*
    5xx Server Error
    */
    function code_500(){
	/*
	A generic error message, given when an unexpected condition was encountered and no more specific message is suitable.
	*/
	return array('code' => 500, 'message' => 'Internal Server Error');
    }
    
    function code_501(){
	/*
	The server either does not recognize the request method, or it lacks the ability to fulfil the request. Usually this implies future availability (e.g., a new feature of a web-service API).
	*/
	return array('code' => 501, 'message' => ' Not Implemented');
    }
}

/* End of file Someclass.php */