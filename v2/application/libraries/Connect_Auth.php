<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Connect Auth Class
 *
 * Authentication library for Code Igniter.
 *
 * @author		Dexcell
 * @version		1.0.6
 * @based on	CL Auth by Jason Ashdown (http://http://www.jasonashdown.co.uk/)
 * @link			http://dexcell.shinsengumiteam.com/dx_auth
 * @license		MIT License Copyright (c) 2008 Erick Hartanto
 * @credits		http://dexcell.shinsengumiteam.com/dx_auth/general/credits.html
 */
 
class Connect_Auth
{
	function Connect_Auth()
	{
		$this->ci =& get_instance();
		// Load required library
		$this->ci->load->library('Session');

	}
	
	function _set_session($data)
	{
		// Get role data
		// $role_data = $this->_get_role_data($data->role_id);
	
		// Set session data array
		$user = array(						
			'Connect_user_id'						=> $data->id,
			'Connect_username'						=> $data->username,
			'Connect_token'							=> $data->access_token,
			// 'Connect_role_id'						=> $data->role_id,
			// 'Connect_role_name'					=> $role_data['role_name'],
			// 'Connect_parent_roles_id'		=> $role_data['parent_roles_id'],	// Array of parent role_id
			// 'Connect_parent_roles_name'	=> $role_data['parent_roles_name'], // Array of parent role_name
			// 'Connect_permission'					=> $role_data['permission'],
			// 'Connect_parent_permissions'	=> $role_data['parent_permissions'],		
			'Connect_logged_in'					=> TRUE
		);

		$this->ci->session->set_userdata($user);
	}
	
	function _get_session(){
		
	}
	
	function logout()
	{
		// Trigger event
		$this->user_logging_out($this->ci->session->userdata('Connect_user_id'));
		// Destroy session
		$this->ci->session->sess_destroy();
	}
	
	// Get user id
	function get_user_id()
	{
		return $this->ci->session->userdata('Connect_user_id');
	}

	// Get username string
	function get_username()
	{
		return $this->ci->session->userdata('Connect_username');
	}
	
	function get_access_token(){
		return $this->ci->session->userdata('Connect_token');
	}
	
	// Get user role id
	function get_role_id()
	{
		return $this->ci->session->userdata('Connect_role_id');
	}
	
	// Get user role name
	function get_role_name()
	{
		return $this->ci->session->userdata('Connect_role_name');
	}
	
	// Check is user is has admin privilege
	function is_admin()
	{
		return strtolower($this->ci->session->userdata('Connect_role_name')) == 'admin';
	}
	
	function is_logged_in()
	{
		return $this->ci->session->userdata('Connect_logged_in');
	}
	
	function user_logging_out($user_id)
	{
		
	}
	
	function get_me(){
	    $result = array();
		if($this->is_logged_in()){
			
			$expired = strtotime("-1 hour");
			$me = $this->ci->session->userdata('me'); 
			if($me && $expired <= $me['created']){
				$result = $this->ci->session->userdata('me');
				return $result;
			}
		
			$response = call_api_get($this->ci->config->item('connect_api_url').'/user/me/'.$this->get_access_token());
			if($response['header_info']['http_code']==200){
				$result = json_decode($response['body'],true);
				$result['created'] = time();
				$this->ci->session->set_userdata('me',$result);
			}
		}
	    
	    return $result;
// 	    print_r($response);
	}
	
	function get_basic_me(){
	    $result = array();
	    
	    if($query = $this->get_me()){
		$result = array(
		    'user_id' => $query['user_id'],
		    'entity' => $query['entity'],
		    'photo' => $query['photo'],
		    'username' => $query['username'],
		    'email' => $query['email'],
		    'birth_place' => $query['birth_place'],
		    'birth_date' => $query['birth_date'],
		    'phone' => $query['phone'],
		    'gender' => $query['gender'],
		    'fullname' => $query['fullname']
		);
	    }
	    
	    return $result;
	}
}