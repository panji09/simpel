<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends CI_Controller {
	
    function __construct(){
		parent::__construct();
    }
    
    function index(){
		$data['title'] = $this->config->item('title').' : Pengaduan';
		$data['content'] = 'dashboard/pengaduan_dashboard';
		
		//search button action
		if($this->input->post('submit') != ''):
		
			$input = $this->input->post();
			//var_dump($input);
			
			$data['search'] = $this->pengaduan_db->get_all($input);
			//var_dump($search);
		
		endif;
		
		$this->load->view('home/main',$data);
    }
	
	
}

/* End of file dashboard.php */
/* Location: ./application/controllers/pengaduan/dashboard.php */