<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once APPPATH.'/controllers/notification.php';

class Pengaduan_online extends CI_Controller {
	
    function __construct(){
			parent::__construct();
			$this->load->model('Pengaduan');
			$this->load->model('Setting_db');
			error_reporting(0);
    }
    
    function index(){
		$data['title'] = $this->config->item('title').' : Kirim Pengaduan';
		$data['content'] = 'dashboard/pengaduan_online';

		$this->load->library('antispam');
		$configs = array(
					'img_path' => './captcha/',
					'img_url' => base_url() . 'captcha/',
					'img_height' => '50',
				);			
		$data['captcha'] 		= $this->antispam->get_antispam_image($configs);	
		$data['kategori']		= $this->Setting_db->get_all('pengaduan_kategori');
		$data['substansi']	= $this->Setting_db->get_all('pengaduan_substansi');
		$data['peran']			= $this->Setting_db->get_all('pengaduan_peran');

		if($this->input->post('submit') != ''):
				//lokasi provinsi
				$province_id = $_POST['provinsi'];				
				if(isset($province_id)){					
					$query = file_get_contents($this->config->item('connect_api_url').'/location/province');
					$province = array();
					$province_name = '';
					if($query){
						$province = json_decode($query,true);
					}
					
					if($province){
						foreach($province as $row){
							if($row['id'] == $province_id){
								$province_name = $row['name'];
							}
						}
					}					
					$province = array('id' => $province_id, 'name' => $province_name );
				}
				
			//lokasi kabkota
				$district_id = $_POST['kota'];
				if(isset($_POST['kota'])){
					
					$query = file_get_contents($this->config->item('connect_api_url').'/location/district/'.$province_id);
					$district = array();
					$district_name = '';
					if($query){
						$district = json_decode($query,true);
					}
					
					if($district){
						foreach($district as $row){
							if($row['id'] == $district_id){
								$district_name = $row['name'];
							}
						}
					}
					
					$kota = array('id' => $district_id, 'name' => $district_name );
				}

				//lokasi subdistrict
				$subdistrict_id = $_POST['kecamatan'];
				if(isset($_POST['kecamatan'])){
					
					$query = file_get_contents($this->config->item('connect_api_url').'/location/subdistrict/'.$district_id);
					$subdistrict = array();
					$subdistrict_name = '';
					if($query){
						$subdistrict = json_decode($query,true);
					}
					
					if($subdistrict){
						foreach($subdistrict as $row){
							if($row['id'] == $subdistrict_id){
								$subdistrict_name = $row['name'];
							}
						}
					}
					
					$kecamatan = array('id' => $subdistrict_id, 'name' => $subdistrict_name );
				}


			if($this->input->post('captcha') == $this->input->post('captcha_invisible')):
				$pengaduan = array('province' => $province, 
					'district' => $kota,
					'subdistrict' => $kecamatan,
					'category' => $_POST['kategori'],
					'substance' => $_POST['substansi'],
					'role' => $_POST['peran'],
					'name' => $_POST['nama'],
					'show_email_phone' => $_POST['tampilkansatu'],
					'email' => $_POST['email'],
					'phone' => $_POST['telp'],
					'show_address' => $_POST['tampilkandua'],
					'address' => $_POST['alamat'],
					'show_description' => $_POST['tampilkantiga'],
					'description' => $_POST['deskripsi'],
					'code_email_verification' => sha1_salt(time()),
					'year'=>date("Y"),
					);
				if($this->Pengaduan->save(null, $pengaduan)):
					$Notification = new notification();
					$Notification->ch_confirmation_email($_POST['nama'], $_POST['email'], $pengaduan['code_email_verification']);
					$this->session->set_flashdata('notif', array('status' => true, 'msg' => 'Silahkan cek email anda untuk verifikasi email'));
					redirect('/pengaduan/pengaduan_online');
				endif;
	    else:
	    	$this->session->set_flashdata('notif', array('status' => false, 'msg' => 'Masukkan code captcha dengan benar'));
	    	redirect('/pengaduan/pengaduan_online');
			endif;
		endif;

		$this->load->view('home/main',$data);
    }

    function code_email_verification($code){
    	$code = $this->pengaduan_db->get_all($code);
    	print_r($code);
    }
	
	
}

/* End of file pengaduan_online.php */
/* Location: ./application/controllers/pengaduan/pengaduan_online.php */