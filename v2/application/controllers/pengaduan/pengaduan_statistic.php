<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pengaduan_statistic extends CI_Controller {
	
    function __construct(){
		parent::__construct();
    }
    
    function index(){
		$data['title'] = $this->config->item('title').' : Kirim Pengaduan';
		$data['content'] = 'dashboard/pengaduan_statistic';
		
		if($this->input->post('submit') != ''):
		$input = $this->input->post();
		//var_dump($data);
		$data['search'] = $this->pengaduan_db->get_all($input + array('verification'=>'1'));
		$data['year'] 	= $this->input->post('year');
		
		endif;
		
		$this->load->view('home/main',$data);
		
		
    }
}

/* End of file pengaduan_statistic.php */
/* Location: ./application/controllers/pengaduan/pengaduan_statistic.php */