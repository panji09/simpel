<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once APPPATH.'/libraries/REST_Controller.php';
class Invite_code extends REST_Controller {
    
    function __construct(){
	parent::__construct();
	header('Access-Control-Allow-Origin: *'); 
    }
    
    function index_get($invite_code = null){
	
	if($invite_code){
	    
	    if($this->invite_code_db->exist_invite_code($invite_code)){
		$this->response(
		    array(
			'status'=> 'OK'
		    ),
		200);
	      
	    }else{
		//no content
		$this->response(
		array(
		    'error'=> 'no_content',
		    'error_description' => 'no content'
		),
		204); //no content
	    }
	}else{
	    
	    //no content
	    $this->response(
	    array(
		'error'=> 'no_content',
		'error_description' => 'no content'
	    ),
	    204); //no content
	}
	
    }
    
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */ 
