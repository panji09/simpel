<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once APPPATH.'/libraries/REST_Controller.php';
class Dynamic_pages extends REST_Controller {
    
    function __construct(){
	parent::__construct();
	
    }
    
    function gallery_post($token=null){
      //new
      $input = json_decode(json_encode($this->post()), true);
      $data_input = $input;
      $data_input['page_id'] = 'gallery';
      //check param
      if(!$token){
	  $this->response(
	      array(
		      'error'=> 'invalid_field',
		      'error_description' => 'missing field [token]'
	      )
	  ,400); //bad request  
      }
      
      //check token
      check_token_api($token, $allow_role = array('superadmin','lpp'));
      
      //check field
      $array_field = array('album_name', 'photo', 'published');
      if(!check_request($input, $array_field)){
	  $this->response(
	  array(
	      'error'=> 'invalid_field',
	      'error_description' => 'invalid field, field allowed [album_name, photo, published]'
	  )
	  ,400);
      }

      
      //get author
      $response = call_api_get($this->config->item('connect_api_url').'/user/me/'.$token);
      $response_body = json_decode($response['body'], true);
      $data_page = array();
      if($response['header_info']['http_code'] == 200){
	  $user_id = $response_body['user_id'];
	  $fullname = $response_body['fullname'];
	  
	  $data_input['author'] = array('user_id' => $user_id, 'fullname'=>$fullname);
      }
      
      if($this->dynamic_pages_db->save($content_id=null, $data_input)){
	  //success
	  $this->response(
	    array(
		'status' => 'OK'
	    ), 200); //OK
      }else{
	  //fail
	  $this->response(
	    array(
		'error'=> 'save_failed',
		'error_description' => 'unable to save data'
	    ), 500); //internal server error
      }
      //$this->response($data_page, 200);
      
    }
    
    function news_post($token=null){
      //new
      $input = json_decode(json_encode($this->post()), true);
      $data_input = $input;
      $data_input['page_id'] = 'news';
      //check param
      if(!$token){
	  $this->response(
	      array(
		      'error'=> 'invalid_field',
		      'error_description' => 'missing field [token]'
	      )
	  ,400); //bad request  
      }
      
      //check token
      check_token_api($token, $allow_role = array('superadmin'));
      
      //check field
      $array_field = array('title', 'cover', 'content', 'content_short', 'published');
      if(!check_request($input, $array_field)){
	  $this->response(
	  array(
	      'error'=> 'invalid_field',
	      'error_description' => 'invalid field, field allowed [title, cover, content, content_short, published]'
	  )
	  ,400);
      }

      
      //get author
      $response = call_api_get($this->config->item('connect_api_url').'/user/me/'.$token);
      $response_body = json_decode($response['body'], true);
      $data_page = array();
      if($response['header_info']['http_code'] == 200){
	  $user_id = $response_body['user_id'];
	  $fullname = $response_body['fullname'];
	  
	  $data_input['author'] = array('user_id' => $user_id, 'fullname'=>$fullname);
      }
      
      if($this->dynamic_pages_db->save($content_id=null, $data_input)){
	  //success
	  $this->response(
	    array(
		'status' => 'OK'
	    ), 200); //OK
      }else{
	  //fail
	  $this->response(
	    array(
		'error'=> 'save_failed',
		'error_description' => 'unable to save data'
	    ), 500); //internal server error
      }
      //$this->response($data_page, 200);
      
    }
    
    function news_put($content_id=null, $token=null){
      //hanya update
      $input = json_decode(json_encode($this->put()), true);
      $data_input = $input;
      $data_input['page_id'] = 'news';
      //check param
      if(!$content_id || !$token){
	  $this->response(
	      array(
		      'error'=> 'invalid_field',
		      'error_description' => 'missing field [content, token]'
	      )
	  ,400); //bad request  
      }
      
      //check token
      check_token_api($token, $allow_role = array('superadmin'));
      
      //check field
      $array_field = array('title', 'cover', 'content', 'content_short', 'published');
      if(!check_request($input, $array_field)){
	  $this->response(
	  array(
	      'error'=> 'invalid_field',
	      'error_description' => 'invalid field, field allowed [title, cover, content, content_short, published]'
	  )
	  ,400);
      }

      
      //get author
      $response = call_api_get($this->config->item('connect_api_url').'/user/me/'.$token);
      $response_body = json_decode($response['body'], true);
      $data_page = array();
      if($response['header_info']['http_code'] == 200){
	  $user_id = $response_body['user_id'];
	  $fullname = $response_body['fullname'];
	  
	  $data_input['author'] = array('user_id' => $user_id, 'fullname'=>$fullname);
      }
      
      if($this->dynamic_pages_db->save($content_id, $data_input)){
	  //success
	  $this->response(
	    array(
		'status' => 'OK'
	    ), 200); //OK
      }else{
	  //fail
	  $this->response(
	    array(
		'error'=> 'save_failed',
		'error_description' => 'unable to save data'
	    ), 500); //internal server error
      }
      //$this->response($data_page, 200);
    }
    
    function index_get($content_id=null){
	//get
	
	//check param
	if(!$content_id){
	    $this->response(
		array(
			'error'=> 'invalid_field',
			'error_description' => 'missing field [page_id, token]'
		)
	    ,400); //bad request  
	}
	
	$query = $this->dynamic_pages_db->get($content_id);
	
	if($query){
	    //ada
	    $result = $query[0];
	    $result['content_id'] = $result['_id']->{'$id'};
	    
	    unset($result['log_last_update']);
	    unset($result['_id']);
	    $this->response(
	    $result,
	    200);
	    
	}else{
	    //no content
	    $this->response(
	    array(
		'error'=> 'no_content',
		'error_description' => 'no content'
	    ),
	    204); //no content
	}
    }
    
    function news_all_get($page=null, $per_page=null){
	
	$meta = array();
	if($page && $per_page){
	    $meta = array(
		'page' => 1,
		'per_page' => 20,
		'page_count' => 20,
		'total_count' => 500,
		'link' => array(
		    'self' => '/products?page=5&per_page=20',
		    'first' => '/products?page=5&per_page=20',
		    'previous' => '/products?page=5&per_page=20',
		    'next' => '/products?page=5&per_page=20',
		    'last' => '/products?page=5&per_page=20'
		)
	    );
	    $query = $this->dynamic_pages_db->get_all(array('page_id'=>'news'), $limit=$per_page, $offset=$page-1);
	}else{
	    $query = $this->dynamic_pages_db->get_all(array('page_id'=>'news'));
	}
	
	if(!$query){
	    //no content
	    $this->response(
	    array(
		'error'=> 'no_content',
		'error_description' => 'no content'
	    ),
	    204); //no content
	}
	
	foreach($query as $row){
	    
	    $row['content_id'] = $row['_id']->{'$id'};
	    unset($row['log_last_update']);
	    unset($row['_id']);
	    $data[] = $row;
	}
	
	$output = array(
	    'meta' => $meta,
	    'data' => $data
	);
	$this->response($output,200);
    }
    
    
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */