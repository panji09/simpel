<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once APPPATH.'/libraries/REST_Controller.php';
class Example_document extends REST_Controller {
    
    function __construct(){
        parent::__construct();
        header('Access-Control-Allow-Origin: *'); 
    }
    
    function index_get(){
        $query = $this->setting_db->get('example_document_registration');
        //print_r($query);
        if($query){
            $result = $query[0];
            $this->response($result['example_doc'], 200);
        }
    }
    
    
    
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */