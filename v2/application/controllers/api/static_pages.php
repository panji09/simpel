<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once APPPATH.'/libraries/REST_Controller.php';
class Static_pages extends REST_Controller {
    
    function __construct(){
	parent::__construct();
	
    }
    
    function index_put($page_id=null, $token=null){
      //hanya update
      $input = json_decode(json_encode($this->put()), true);
      $data_input = $input;
      $data_input['page_id']=$page_id;
      //check param
      if(!$page_id || !$token){
	  $this->response(
	      array(
		      'error'=> 'invalid_field',
		      'error_description' => 'missing field [page_id, token]'
	      )
	  ,400); //bad request  
      }
      
      //check token
      check_token_api($token, $allow_role = array('superadmin'));
      
      //check field
      $array_field = array('title', 'cover', 'content', 'content_short', 'published');
      if(!check_request($input, $array_field)){
	  $this->response(
	  array(
	      'error'=> 'invalid_field',
	      'error_description' => 'invalid field, field allowed [title, cover, content, content_short, published]'
	  )
	  ,400);
      }

      
      //get author
      $response = call_api_get($this->config->item('connect_api_url').'/user/me/'.$token);
      $response_body = json_decode($response['body'], true);
      $data_page = array();
      if($response['header_info']['http_code'] == 200){
	  $user_id = $response_body['user_id'];
	  $fullname = $response_body['fullname'];
	  
	  $data_input['author'] = array('user_id' => $user_id, 'fullname'=>$fullname);
	  $allow = true;
      }
      
      if($this->static_pages_db->save($page_id, $data_input)){
	  //success
	  $this->response(
	    array(
		'status' => 'OK'
	    ), 200); //OK
      }else{
	  //fail
	  $this->response(
	    array(
		'error'=> 'save_failed',
		'error_description' => 'unable to save data'
	    ), 500); //internal server error
      }
      //$this->response($data_page, 200);
      
    }
    
    function index_get($page_id=null){
	//get
	//check param
	if(!$page_id){
	    $this->response(
		array(
			'error'=> 'invalid_field',
			'error_description' => 'missing field [page_id, token]'
		)
	    ,400); //bad request  
	}
	
	$query = $this->static_pages_db->get($page_id);
	
	if($query){
	    //ada
	    $result = $query[0];
	    unset($result['log_last_update']);
	    $this->response(
	    $result,
	    200);
	    
	}else{
	    //no content
	    $this->response(
	    array(
		'error'=> 'no_content',
		'error_description' => 'no content'
	    ),
	    204); //no content
	}
    }
    
    
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */