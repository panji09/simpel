<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once APPPATH.'/libraries/REST_Controller.php';
class Training extends REST_Controller {
    
    function __construct(){
	parent::__construct();
	header('Access-Control-Allow-Origin: *'); 
    }
    
    function name_simple_get($type_training_id=null){
	$this->name_get($type_training_id, $simple=true);
    }
    
    function name_get($type_training_id=null, $simple=false){
	
	if(!$type_training_id){
	    $query = $this->config_training_db->get_all(array('published'=>1));
	    
	    if($query){
		//ada
		$result = array();
		foreach($query as $row){
    // 		print_r($row);
		    $row['id'] = $row['_id']->{'$id'};
		    unset($row['author']);
		    unset($row['published']);
		    unset($row['_id']);
		    unset($row['last_update']);
		    unset($row['created']);
		    unset($row['deleted']);
		    
		    if($simple){
			$result[] = array('id' => $row['_id']->{'$id'}, 'name' => $row['name']);
		    }else{
			$result[] = $row;
		    }
		    
		}
		
		$this->response(
		$result,
		200);
	    }else{
		//no content
		$this->response(
		array(
		    'error'=> 'no_content',
		    'error_description' => 'no content'
		),
		204); //no content
	    }
	}else{
	    $query = $this->config_training_db->get_all(array('published'=>1, 'type_training_id' => $type_training_id));
	
	    if($query){
		//ada
		$result = array();
		foreach($query as $row){
    // 		print_r($row);
		    $row['id'] = $row['_id']->{'$id'};
		    unset($row['author']);
		    unset($row['published']);
		    unset($row['_id']);
		    unset($row['last_update']);
		    unset($row['created']);
		    unset($row['deleted']);

		    if($simple){
			$result[] = array('id' => $row['id'], 'name' => $row['name']);
		    }else{
			$result[] = $row;
		    }
		}
		
		$this->response(
		$result,
		200);
	    }else{
		//no content
		$this->response(
		array(
		    'error'=> 'no_content',
		    'error_description' => 'no content'
		),
		204); //no content
	    }
	}
	
    }
    
    function type_get(){
	$query = $this->type_training_db->get_all();
	
	if($query){
	    
// 	    print_r($query);
	    $output = array();
	    foreach($query as $row){
		$output[] = array(
		    'id' => $row['_id']->{'$id'},
		    'name' => $row['name']
		);
	    }
	    //ada
	    $result = $output;
	    $this->response(
	    $result,
	    200);
	}else{
	    //no content
	    $this->response(
	    array(
		'error'=> 'no_content',
		'error_description' => 'no content'
	    ),
	    204); //no content
	}
    }
    
    function evaluation_get($evaluation_id=null){
	
	if(!$evaluation_id){
	    $query = $this->evaluation_db->get_all(array('published' => 1) );
	
	    
	}else{
	    
	    $query = $this->evaluation_db->get_all(array('published' => 1, 'evaluation_id' => $evaluation_id) );
	
	    
	}
	
	if($query){
	    $output = array();
	    foreach($query as $row){
		$row['evaluation_id'] = $row['_id']->{'$id'};
		unset($row['published']);
		unset($row['last_update']);
		unset($row['created']);
		unset($row['deleted']);
		unset($row['_id']);
		
		$output[] = $row;
	    }
	    //ada
	    $result = $output;
	    $this->response(
	    $result,
	    200);
	}else{
	    //no content
	    $this->response(
	    array(
		'error'=> 'no_content',
		'error_description' => 'no content'
	    ),
	    204); //no content
	}
    }
    
    
    
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */