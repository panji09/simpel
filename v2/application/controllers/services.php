<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Services extends CI_Controller {
    
//     function provinsi(){
//     
// 	$this->response(array(
// 	    'meta' => $this->code_response->code_200(),
// 	    'data' => $this->config_db->get_provinsi()
// 	));
//     }
//     
//     function kabkota($provinsi_id=null){
// 	if($provinsi_id){
// 	    
// 	    $query = $this->config_db->get_kabkota();
// 	    
// 	    if($query){
// 		
// 		$filter_kabkota = array();
// 		foreach($query as $row){
// 		    if($row['provinsi_id'] == $provinsi_id){
// 			$filter_kabkota[] = $row;
// 		    }
// 		}
// 		
// 		if($filter_kabkota){
// 		    $this->response(array(
// 			'meta' => $this->code_response->code_200(),
// 			'data' => $filter_kabkota
// 		    ));
// 		}else{
// 		    $this->response(array(
// 			'meta' => $this->code_response->code_204(),
// 			'data' => array()
// 		    ));
// 		}
// 	    }else{
// 		$this->response(array(
// 		    'meta' => $this->code_response->code_204(),
// 		    'data' => array()
// 		));
// 	    }
// 	    
// 	}else{
// 	    $this->response(array(
// 		'meta' => $this->code_response->code_400(),
// 		'data' => array()
// 	    ));
// 	}
//     }
//     
//     function kecamatan($kabkota_id=null){
// 	if($kabkota_id){
// 	    
// 	    $query = $this->config_db->get_kecamatan();
// 	    
// 	    if($query){
// 		
// 		$filter_kecamatan = array();
// 		foreach($query as $row){
// 		    if($row['kabkota_id'] == $kabkota_id){
// 			$filter_kecamatan[] = $row;
// 		    }
// 		}
// 		
// 		if($filter_kecamatan){
// 		    $this->response(array(
// 			'meta' => $this->code_response->code_200(),
// 			'data' => $filter_kecamatan
// 		    ));
// 		}else{
// 		    $this->response(array(
// 			'meta' => $this->code_response->code_204(),
// 			'data' => array()
// 		    ));
// 		}
// 	    }else{
// 		$this->response(array(
// 		    'meta' => $this->code_response->code_204(),
// 		    'data' => array()
// 		));
// 	    }
// 	    
// 	}else{
// 	    $this->response(array(
// 		'meta' => $this->code_response->code_400(),
// 		'data' => array()
// 	    ));
// 	}
//     }
    
    function response($output = array()){
	$this->output
	    ->set_content_type('application/json')
	    ->set_output(json_encode($output));
    }
    
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */