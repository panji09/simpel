<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Lpp extends CI_Controller {
    function __construct(){
	parent::__construct();
	session_start();
	config_kcfinder(array('user_id' => $this->connect_auth->get_user_id()));
	check_token($this->connect_auth->get_access_token(), $allow_role = array('lpp'));
	
// 	print_r($this->connect_auth->get_me());
	
    }
    
    function index(){
	redirect('lpp/dashboard');
    }
    
    function manual_book(){
	redirect('https://docs.google.com/document/d/1o_WneYY-8LzZeKBKZqeO_0hafW09GjwJCudvFgE42d0/edit?pli=1#heading=h.xyjpgqrnd383');
    }
    function disabled(){
	$data['title'] = $this->config->item('title').' : Dashboard LPP';
	$data['content'] = 'dashboard/lpp_disabled';
	$this->load->view('home/main',$data);
    }
//     
//     function index(){	
// 	$this->check_verified_insitite();
// 	$data['title'] = $this->config->item('title').' : Dashboard LPP';
// 	$data['content'] = 'dashboard/lpp/lpp_dashboard';
// 	$this->load->view('home/main',$data);
//     }
// 	
//     function training(){
// 	$this->check_verified_insitite();
// 	$data['title'] = $this->config->item('title').' : Dashboard-Pelatihan';
// 	$data['content'] = 'dashboard/lpp/lpp_training';
// 	$this->load->view('home/main',$data);
//     }
// 
//     function approve_participant(){
// 	$this->check_verified_insitite();
// 	$data['title'] = $this->config->item('title').' : Dashboard-Approve Peserta';
// 	$data['content'] = 'dashboard/lpp/lpp_approve_participant';
// 	$this->load->view('home/main',$data);
//     }
// 	
//     function gallery(){
// 	$this->check_verified_insitite();
// 	$data['title'] = $this->config->item('title').' : Dashboard-Gallery';
// 	$data['content'] = 'dashboard/lpp/lpp_gallery';
// 	$this->load->view('home/main',$data);
//     }
// 	
//     function news(){
// 	$this->check_verified_insitite();
// 	$data['title'] = $this->config->item('title').' : Dashboard-Berita';
// 	$data['content'] = 'dashboard/lpp/lpp_news';
// 	$this->load->view('home/main',$data);
//     }
// 	
//     function notification(){	
// 	$this->check_verified_insitite();
// 	$data['title'] = $this->config->item('title').' : Dashboard-Berita';
// 	$data['content'] = 'dashboard/lpp_notification';
// 	$this->load->view('home/main',$data);
//     }
// 	
//     function training_submission(){	
// 	$this->check_verified_insitite();
// 	$data['title'] = $this->config->item('title').' : Dashboard-Berita';
// 	$data['content'] = 'dashboard/lpp_training_submission';
// 	$this->load->view('home/main',$data);
//     }
}

/* End of file welcome.php */
/* Location: ./dashboard/instructor.php */