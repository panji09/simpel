<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends CI_Controller {
    function __construct(){
	parent::__construct();
	session_start();
	config_kcfinder(array('user_id' => $this->connect_auth->get_user_id()));
	check_token($this->connect_auth->get_access_token(), $allow_role = array('lpp'));
	check_verified_institute();
    }
	
    function index(){	
	$data['title'] = $this->config->item('title').' : Dashboard LPP';
	$data['content'] = 'dashboard/lpp_dashboard';
	$this->load->view('home/main',$data);
    }
	
    
}

/* End of file welcome.php */
/* Location: ./dashboard/instructor.php */