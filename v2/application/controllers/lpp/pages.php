<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pages extends CI_Controller {
    
    function __construct(){
	parent::__construct();
	session_start();
	config_kcfinder(array('user_id' => $this->connect_auth->get_user_id()));
	check_token($this->connect_auth->get_access_token(), $allow_role = array('lpp'));
	check_verified_institute();
    }
    
    function gallery($content = 'home', $content_id=null){
	
	$data['title'] = $this->config->item('title').' : Dashboard Galeri';
	$data['title_page'] = 'Galeri';
	$data['title'] = $this->config->item('title').' : '.$data['title_page'];
	$data['content_id'] = $content_id;

	if($content == 'home'){
	    $this->session->set_userdata('redirect',current_url());
	    $data['content'] = 'dashboard/lpp_gallery';
	}elseif($content == 'add'){
	    $data['title_content'] = 'Tambah';
	    $data['content'] = 'dashboard/lpp_gallery_add';

	}elseif($content == 'edit'){
	    $data['title_content'] = 'Edit';
    
	    $data['content'] = 'dashboard/lpp_gallery_add';

	}elseif($content == 'delete'){
	    //$data['content'] = 'user_admin';
	    if($this->dynamic_pages_db->delete($content_id)){
		$this->session->set_flashdata('notif', array('status' => true, 'msg' => 'Sukses!'));
	    }else{
		$this->session->set_flashdata('notif', array('status' => false, 'msg' => 'Gagal!'));
	    }
    
	    redirect($this->session->userdata('redirect'));
	}

	$this->load->view('home/main',$data);
    }
	
    function news($content = 'home', $content_id=null){
	$data['title'] = $this->config->item('title').' : Dashboard Berita';
	$data['title_page'] = 'Berita';
	$data['title'] = $this->config->item('title').' : '.$data['title_page'];
	$data['content_id'] = $content_id;

	if($content == 'home'){
	    $this->session->set_userdata('redirect',current_url());
	    $data['content'] = 'dashboard/lpp_news';
	}elseif($content == 'add'){
	    $data['title_content'] = 'Tambah';
	    $data['content'] = 'dashboard/lpp_news_add';

	}elseif($content == 'edit'){
	    $data['title_content'] = 'Edit';
    
	    $data['content'] = 'dashboard/lpp_news_add';

	}elseif($content == 'delete'){
	    //$data['content'] = 'user_admin';
	    if($this->dynamic_pages_db->delete($content_id)){
		$this->session->set_flashdata('notif', array('status' => true, 'msg' => 'Sukses!'));
	    }else{
		$this->session->set_flashdata('notif', array('status' => false, 'msg' => 'Gagal!'));
	    }
    
	    redirect($this->session->userdata('redirect'));
	}

	$this->load->view('home/main',$data);
    }
    
    
}

/* End of file pages.php */
/* Location: ./application/controllers/superadmin/pages.php */