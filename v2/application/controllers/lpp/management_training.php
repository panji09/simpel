<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once APPPATH.'/controllers/notification.php';

class Management_training extends CI_Controller {
    
    function __construct(){
		parent::__construct();
		session_start();
		config_kcfinder(array('user_id' => $this->connect_auth->get_user_id()));
		
		if($this->connect_auth->get_me()['entity']=='lpp')
		check_verified_institute();
    }
    
    
    function training($content='home', $training_id = null){
	
		check_token($this->connect_auth->get_access_token(), $allow_role = array('lpp'));
		$data['title'] = $this->config->item('title').' : Dashboard List Pelatihan';
		$data['title_page'] = 'List Pelatihan';
		$data['title'] = $this->config->item('title').' : '.$data['title_page'];
		$data['training_id'] = $training_id;
	
		if($content == 'home'){
			
			$this->session->set_userdata('redirect',current_url());
			$data['content'] = 'dashboard/lpp_training';
		}elseif($content == 'add'){
			
			//check limit
			$query = $this->training_db->get_all(array(
											 'user_id' => $this->connect_auth->get_user_id(),
											 'verified_submission_training_in' => array(0,-1)
											 ));
			
			$count_avalable_training = count($query);
			$limit_submission = (isset($this->setting_db->get('limit_submission_training')[0]['limit']) ? $this->setting_db->get('limit_submission_training')[0]['limit'] : 0);
			
			if($limit_submission > $count_avalable_training){
				$data['title_content'] = 'Pengajuan Pelatihan';
				$data['content'] = 'dashboard/lpp_training_submission';	
			}else{
				$this->session->set_flashdata('notif', array('status' => false, 'msg' => 'Gagal!, Sudah melebihi batas limit pengajuan'));
				redirect($this->session->userdata('redirect'));
			}
			
		}elseif($content == 'detail'){
			$this->session->set_userdata('redirect',current_url());
			$data['title_content'] = 'Detail Pelatihan';
			$data['content'] = 'dashboard/lpp_detail_training';
		}
		elseif($content == 'evaluation'){
			
			$data['title_content'] = 'Evaluasi Pelatihan Pelatihan';
			$data['training_id'] = $training_id;
	// 		$data['form_id'] = "55dace12ae5dbe3f04b7acd9";
			
			$data['content'] = 'dashboard/lpp_form_evaluation';
			$this->load->view('home/main',$data);
		}elseif($content == 'edit'){
			$data['title_content'] = 'Pengajuan Pelatihan';
			$data['content'] = 'dashboard/lpp_training_submission';
		}elseif($content == 'reschedule'){
			$data['title_content'] = 'Pengajuan Pelatihan';
			$data['content'] = 'dashboard/lpp_training_submission_reschedule';
		}elseif($content == 'delete'){
			//$data['content'] = 'user_admin';
			if($this->training_db->delete($training_id)){
			$this->session->set_flashdata('notif', array('status' => true, 'msg' => 'Sukses!'));
			}else{
			$this->session->set_flashdata('notif', array('status' => false, 'msg' => 'Gagal!'));
			}
		
			redirect($this->session->userdata('redirect'));
		}
		$this->load->view('home/main',$data);
	
    }
    
    function training_post($training_id=null){
		/*
			semua post pengajuan pelatihan dsini
		*/
		check_token($this->connect_auth->get_access_token(), $allow_role = array('admin','lpp', 'narasumber'));
		$input = $this->input->post();	
		$send_notif = array();
		
		$data_before_update = array();
		if($query = $this->training_db->get($training_id)){
			$data_before_update = $query[0];
		}
		
		if($this->connect_auth->get_me()['entity']=='lpp'){
			//lpp yang submit
			
			
			if(isset($input['training_date'])){
				$training_date = explode(' - ', $input['training_date']);
				$input['training_date_start'] = $training_date[0];
				$input['training_date_end'] = $training_date[1];
			}
			
			$input['user_submission'] = $this->connect_auth->get_me();
			if(isset($input['submit']) && $input['submit']=='draft'){
				//draft
				$input['published'] = 0;
				$input['draft'] = 1;
			}elseif(isset($input['submit']) && $input['submit']=='reschedule'){
				//reschedule
				$input['rescheduled'] = 1;
			}else{
				//pengajuan pelatihan
				
				//form validation
				$this->form_validation->set_message('required', '- <strong>%s</strong> harus diisi!');
				$this->form_validation->set_rules('mail_submission_id', 'No Surat Permohonan', 'required');
				$this->form_validation->set_rules('mail_submission_upload', 'Upload Surat Permohonan', 'required');
				$this->form_validation->set_rules('type_submission_training', 'Jenis Pengajuan', 'required');
				$this->form_validation->set_rules('type_training', 'Jenis Pelatihan', 'required');
				$this->form_validation->set_rules('training_name', 'Nama Pelatihan', 'required');
				$this->form_validation->set_rules('training_date', 'Tanggal Pelatihan', 'required');
				$this->form_validation->set_rules('start_training_time', 'Jam Mulai Pelatihan', 'required');
				$this->form_validation->set_rules('training_location_address', 'Tempat Pelatihan', 'required');
				$this->form_validation->set_rules('training_location_province', 'Provinsi', 'required');
				$this->form_validation->set_rules('training_location_district', 'Kab / Kota', 'required');
				$this->form_validation->set_rules('class_amount', 'Jumlah Kelas', 'required');
				$this->form_validation->set_rules('type_registration_participant', 'Pendaftaran Peserta', 'required');
				$this->form_validation->set_rules('participant_amount', 'Jumlah Peserta', 'required');
				$this->form_validation->set_rules('contact_person_name', 'Contact Person Nama', 'required');
				$this->form_validation->set_rules('contact_person_phone', 'Contact Person No HP', 'required');
				
				
				if ($this->form_validation->run() == FALSE){
					
					
					$this->session->set_flashdata('notif', array('status' => false, 'msg' => validation_errors()));
					redirect(site_url('lpp/management_training/training/add'));
				}	
				$input['draft'] = 0;
				$input['verified_submission_training'] = 0; //menunggu verifikasi
				
				$send_notif['send_submission_training'] = true;
				
				
			}
			unset($input['submit']);
		  
		}elseif($this->connect_auth->get_me()['entity']=='admin'){
			
			//klik bttn
			$input['verified_submission_training_log'] = $this->connect_auth->get_me();
				
			if(isset($input['submit'])){
				//approved
				if($input['submit']=='approved'){
					$input['published'] = 1;
					$input['verified_submission_training'] = 1;
					$input['select_instructor_finished'] = 0;
					$input['verified_submission_training_log'] = $this->connect_auth->get_me();
					
					$send_notif['choose_instructor'] = true;
				}
				
				//verifikasi lagi
				if($input['submit'] == 'clarification' ){
					$input['published'] = 0;
					$input['verified_submission_training'] = -1;
				}
				
				//tolak
				if($input['submit'] == 'reject' ){
					$input['published'] = 0;
					$input['verified_submission_training'] = -2;
				}
				
				$send_notif['status_submission_training'] = $input['verified_submission_training'];
			}
			unset($input['submit']);
			
			
		}elseif($this->connect_auth->get_me()['entity']=='fasilitator'){
			
		}elseif($this->connect_auth->get_me()['entity']=='narasumber'){
			if(isset($input['submit'])){
				//approved
				if($input['submit']=='approved'){
		
					
				}
				
				//tolak
				if($input['submit'] == 'reject' ){
		
				}
			}
			unset($input['submit']);
		}
		
		
		
		//filter dulu
		
		//jenis pengajuan
		if(isset($input['type_submission_training'])){
			$input['type_submission_training'] = array('id' => $input['type_submission_training'], 'name' => $this->init_config_db->get_type_submission_training( $input['type_submission_training'] ));
		}
		
		//jenis pelatihan
		if(isset($input['type_training'])){
			$input['type_training'] = array(
			'id' => $input['type_training'], 
			'name' => $this->type_training_db->get( $input['type_training'])[0]['name']
			);
		}
		
		//nama pelatihan
		if(isset($input['training_name'])){
			$input['training_name'] = array(
			'id' => $input['training_name'], 
			'name' => $this->config_training_db->get( $input['training_name'] )[0]['name']
			);
		}
		
		//tipe pendaftaran peserta
		
		if(isset($input['type_registration_participant'])){
			$input['type_registration_participant'] = array('id' => $input['type_registration_participant'], 'name' => $this->init_config_db->get_type_registration_participant( $input['type_registration_participant'] ));
		}
		//lokasi provinsi
		$province_id = $input['training_location_province'];
		
		if(isset($input['training_location_province'])){
			
			$query = file_get_contents($this->config->item('connect_api_url').'/location/province');
			$province = array();
			$province_name = '';
			if($query){
			$province = json_decode($query,true);
			}
			
			if($province){
			foreach($province as $row){
				if($row['id'] == $province_id){
				$province_name = $row['name'];
				}
			}
			}
			
			$input['training_location_province'] = array('id' => $province_id, 'name' => $province_name );
		}
		
		//lokasi kabkota
		$district_id = $input['training_location_district'];
		if(isset($input['training_location_district'])){
			
			$query = file_get_contents($this->config->item('connect_api_url').'/location/district/'.$province_id);
			$district = array();
			$district_name = '';
			if($query){
				$district = json_decode($query,true);
			}
			
			if($district){
				foreach($district as $row){
					if($row['id'] == $district_id){
					$district_name = $row['name'];
					}
				}
			}
			
			$input['training_location_district'] = array('id' => $district_id, 'name' => $district_name );
		}
		
		
	
		if($this->training_db->save($training_id, $input)){
		
			/*save log*/
			$data_log = array(
			'table_name' => 'training',
			'table_id' => $training_id,
			'log_type' => 'training_last_update',
			'ip_address' => $this->input->ip_address(),
			'user_id' => $this->connect_auth->get_me()['user_id'],
			'fullname' => $this->connect_auth->get_me()['fullname'],
			'time' => time(),
			'data_before_update' => $data_before_update
			);
		
			$this->log_db->save(null, $data_log);
			
			
			if($send_notif){
				if(isset($send_notif['send_submission_training']) && $send_notif['send_submission_training']){
					$Notification = new notification();
					$Notification->review_training_submission($this->connect_auth->get_me()['user_id']);
				}
				
				if(isset($send_notif['choose_instructor']) && $send_notif['choose_instructor']){
					$Notification = new notification();
					$Notification->choose_instructor($user_admin = $this->connect_auth->get_me(), $facilitator_user_id = $input['facilitator_user_id'], $training = $data_before_update);
				}
				if(isset($send_notif['status_submission_training'])){
					
					$Notification = new notification();
					$Notification->send_status_approval_training_submission($training_id, $send_notif['status_submission_training']);
					
				}
			}
		
			$this->session->set_flashdata('notif', array('status' => true, 'msg' => 'Sukses!'));
		}else{
			$this->session->set_flashdata('notif', array('status' => false, 'msg' => 'Gagal!'));
		}	
		redirect($this->session->userdata('redirect'));
    }
    
    function ExportCSVFile($records) {
	// create a file pointer connected to the output stream
	$fh = fopen( 'php://output', 'w' );
	$heading = false;
	    if(!empty($records))
	      foreach($records as $row) {
		if(!$heading) {
		  // output the column headings
		  fputcsv($fh, array_keys($row));
		  $heading = true;
		}
		// loop over the rows, outputting them
		fputcsv($fh, array_values($row));
		  
	      }
	      fclose($fh);
    }

    function approve_participant($page='home', $param=null){
		check_token($this->connect_auth->get_access_token(), $allow_role = array('lpp'));
		
		if($page == 'home'){
			$this->session->set_userdata('redirect',current_url());
		
			$data['title'] = $this->config->item('title').' : Dashboard-Approve Peserta';
			$data['title_page'] = 'Approval Peserta Pelatihan';
			$data['content'] = 'dashboard/lpp_approve_participant';
			$this->load->view('home/main',$data);
		}elseif($page == 'export'){
			$input = $this->input->get();
	// 	     print_r($input);
			if($param=='list_waiting'){
				$query = $this->join_training_db->get_all(array(
					'training_id' => $input['training_id']
				));
				
			}elseif($param=='list_approved'){
			
				if(isset($input['training_id']) && $input['training_id']){
					$query = $this->join_training_db->get_all(array(
					'training_id' => $input['training_id'],
					'institute_approved' => 1
					));
					
				}else{
					$query = $this->training_db->get_all(array('user_id' => $this->connect_auth->get_me()['user_id']));
					$training_ids =array();
					if($query)
					foreach($query as $row){
					$training_ids[] = $row['_id']->{'$id'};
					}
					
					$query = $this->join_training_db->get_all(array(
					'training_id_in' => $training_ids,
					'institute_approved' => 1
					));
				}
		// 		print_r($query);
			}elseif($param=='list_all'){
			
				if(isset($input['training_id']) && $input['training_id']){
					$query = $this->join_training_db->get_all(array(
					'training_id' => $input['training_id'],
					'institute_approved' => 1
					));
					
				}else{
					$query = $this->training_db->get_all(array('user_id' => $this->connect_auth->get_me()['user_id']));
					$training_ids =array();
					if($query)
					foreach($query as $row){
						$training_ids[] = $row['_id']->{'$id'};
					}
					
					$query = $this->join_training_db->get_all(array(
					'training_id_in' => $training_ids
					));
				}
			
			}
			
			$user = array();
			if($query){
				foreach($query as $row){
					if(isset($row['user_join'])){
						$user = $row['user_join'];
						
						$verifikasi = array();
													
						if(isset($row['institute_approved']))
						switch($row['institute_approved']){
							case 0 : $verifikasi = array('name' => 'Menunggu Verifikasi', 'label' => 'label-warning'); break;
							case 1 : $verifikasi = array('name' => 'Disetujui', 'label' => 'label-success'); break;
							case -1 : $verifikasi = array('name' => 'Klarifikasi', 'label' => 'label-warning'); break;
							case -2 : $verifikasi = array('name' => 'Tolak', 'label' => 'label-danger'); break;
							default : $verifikasi = array('name' => 'Menunggu Verifikasi', 'label' => 'label-warning'); break;
						}
						
						//http://simpel.ruangpanji.com/v2/connect/index.php/api/certificate/index/123
						$no_sertifikat = (isset($user['procurement_certificate']) ? $user['procurement_certificate'] : '');
						
						
						$logbook = array();
						
						if($no_sertifikat){
							
							if($this->logbook_db->exist($no_sertifikat)){
								$query = $this->logbook_db->get($no_sertifikat);
								$logbook = $query[0];
							}else{
								$response = call_api_get($this->config->item('connect_url').'/api/certificate/index/'.$no_sertifikat);
								$response_body = json_decode($response['body'], true);
								
								if($response['header_info']['http_code'] == 200 && !isset($response_body['error']) ){
									$logbook = $response_body[0];
									$this->logbook_db->save(null,$logbook);
								}
							}
						}
						
						$data_user[] = array(
							'photo' => (isset($user['photo']) ? $user['photo'] : ''),
							'username' => (isset($user['username']) ? $user['username'] : ''),
							'sertifikat_pengadaan' => (isset($user['procurement_certificate']) ? "'".$user['procurement_certificate'] : ''),
							'email' => (isset($user['email']) ? $user['email'] : ''),
							'nama' => (isset($user['fullname']) ? $user['fullname'] : ''),
							'tempat_lahir' => (isset($user['birth_place']) ? $user['birth_place'] : ''),
							'tanggal_lahir' => (isset($user['birth_date']) ? $user['birth_date'] : ''),
							'phone' => (isset($user['phone']) ? "'".$user['phone'] : ''),
							'gender' => (isset($user['gender']) ? $user['gender'] : '') ,
							'pendidikan_terakhir' => (isset($user['last_education']) ? $user['last_education'] : ''),
							'gelar_pendidikan' => (isset($user['academic_degree']) ? $user['academic_degree'] : '') ,
							'pekerjaan' => (isset($user['employment']) ? $user['employment'] : '') ,
							'nip_id' => (isset($user['nip_id']) ? "'".$user['nip_id'] : ''),
							'ktp_id' => (isset($user['ktp_id']) ? "'".$user['ktp_id'] : ''),
							'institusi' => (isset($user['institution']) ? $user['institution'] : '') ,
							'kantor_provinsi' => (isset($user['office_province']['name']) ? $user['office_province']['name'] : '') ,
							'kantor_kabkota' => (isset($user['office_district']['name']) ?$user['office_district']['name'] : '') ,
							'kantor_alamat' => (isset($user['office_address']) ? $user['office_address'] : ''),
							'kantor_kodepos' => (isset($user['office_zip_code']) ? "'".$user['office_zip_code'] : ''),
							
							'pelatihan' => $row['training_log']['type_training']['name'].' / '.$row['training_log']['training_name']['name'],
							'tanggal_pelatihan' => $row['training_log']['training_date'],
							'tempat_pelatihan' => $row['training_log']['training_location_address'],
							'status' => $verifikasi['name'],
							
							'surat_tugas_id' => (isset($row['letter_of_assignment_id']) ? "'".$row['letter_of_assignment_id'] : ''),
							'surat_tugas_upload' => (isset($row['letter_of_assignment_upload']) ? $row['letter_of_assignment_upload'] : ''),
							'sertifikat_pengadaan_id' => (isset($row['certificate_procurement_expert_id']) ? "'".$row['certificate_procurement_expert_id'] : ''),
							'sertifikat_pengadaan_upload' => (isset($row['certificate_procurement_expert_upload']) ? $row['certificate_procurement_expert_upload'] : ''),
							'logbook_pangkat_golongan' => (isset($logbook['pangkat_golongan']) ? $logbook['pangkat_golongan'] : ''  )
						);
					}
				}
				
				$filename = "peserta_".time().".csv";      
				header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
				header("Content-type: text/csv");
				header("Content-Disposition: attachment; filename=\"$filename\"");
				header("Expires: 0");
				ob_clean();
				$this->ExportCSVFile($data_user);
		
				exit();
			}
			
			
		}
	
    }
    
    function training_ajax($load_page, $training_id){
		check_token($this->connect_auth->get_access_token(), $allow_role = array('lpp','narasumber'));
		$this->load->view('admin/inc/ajax/'.$load_page,array('training_id' => $training_id));
    }
    
    function institution_ajax($user_id=null, $param=null){
		check_token($this->connect_auth->get_access_token(), $allow_role = array('lpp'));
		$this->load->view('admin/inc/ajax/'.$param,array('user_id'=>$user_id));
    }
    
    function instructor_ajax($load_page=null, $training_id=null, $user_id=null){
		check_token($this->connect_auth->get_access_token(), $allow_role = array('lpp'));
		$this->load->view('admin/inc/ajax/'.$load_page,array('user_id' => $user_id, 'training_id' => $training_id));
    }
    
    function todolist_post($training_id = null, $todolist_id=null){
		$input = $this->input->post();
		if($training_id && $todolist_id){
			
			if($this->training_db->save($training_id, $input)){
			$this->session->set_flashdata('notif', array('status' => true, 'msg' => 'Sukses!'));
			}else{
			$this->session->set_flashdata('notif', array('status' => false, 'msg' => 'Gagal!'));
			}
		}
		redirect($this->session->userdata('redirect'));
    }
    
    function evaluation($training_id){
		
    }
    
    function tab_ajax($load_page=null, $training_id=null){
	check_token($this->connect_auth->get_access_token(), $allow_role = array('lpp'));
	if($training_id && $load_page){
	    $this->load->view('home/inc/ajax/'.$load_page, array('training_id' => $training_id));
	}
    }
    
    function evaluation_post($evaluation_id = null){
	    // $input = $this->input->post();
	    check_token($this->connect_auth->get_access_token(), $allow_role = array('lpp'));
	    
	    $input = $this->input->post();
	    
	    $input['published'] = 1;
			    
	    //
	    if($this->evaluation_data_db->save($evaluation_id, $input)){
		    $this->session->set_flashdata('notif', array('status' => true, 'msg' => 'Sukses!'));
	    }else{
		    $this->session->set_flashdata('notif', array('status' => false, 'msg' => 'Gagal!'));
	    }
	    redirect($this->session->userdata('redirect'));
    }
    
    function score_training_post($score_training_id = null){
	    check_token($this->connect_auth->get_access_token(), $allow_role = array('lpp'));
	    
	    $input = $this->input->post();
	    
	    $input['published'] = 1;
	    if($this->score_training_db->save($score_training_id, $input)){
		    $this->session->set_flashdata('notif', array('status' => true, 'msg' => 'Sukses!'));
	    }else{
		    $this->session->set_flashdata('notif', array('status' => false, 'msg' => 'Gagal!'));
	    }
	    redirect($this->session->userdata('redirect'));
    }
    
    function overview_participant_ajax($load_page=null, $join_training_id=null){
		if($load_page && $join_training_id){
			$this->load->view('home/inc/ajax/'.$load_page, array('join_training_id' => $join_training_id));
		}
    }
	
    function approval_participant_post($join_training_id = null){
		$input = $this->input->post();
		
		if($input['submit'] == 'approved'){
			$input['institute_approved'] = 1;
			$input['institute_approved_log'] = $this->connect_auth->get_me();
			
		}elseif($input['submit'] == 'clarification'){
			$input['institute_approved'] = -1;
			$input['institute_approved_log'] = $this->connect_auth->get_me();
			
		}elseif($input['submit'] == 'rejected'){
			$input['institute_approved'] = -2;
			$input['institute_approved_log'] = $this->connect_auth->get_me();
			
		}
		unset($input['submit']);
		if($this->join_training_db->save($join_training_id, $input )){
			$Notification = new Notification();
			$Notification->send_review_join_training($join_training_id, $input['institute_approved']);
			
			$this->session->set_flashdata('notif', array('status' => true, 'msg' => 'Sukses!'));
		}else{
			$this->session->set_flashdata('notif', array('status' => false, 'msg' => 'Gagal!'));
		}
		
		redirect($this->session->userdata('redirect'));
    }
	
	
	
	function invite_participant($content = 'post', $param=null){
		
		if($content == 'post'){
			$training_id = $param;
			$this->session->set_flashdata('notif', array('status' => false, 'msg' => 'Gagal!'));
			if($training_id && $this->training_db->exist($training_id)){
				$input = $this->input->post();
				//print_r($input);
				
				
				if(isset($input['invite_via']) && $input['invite_via']){
					$i=0;
					foreach($input['invite_via'] as $invite_via){
						if(strtolower($invite_via) == 'email' && $input['invite_value'][$i]){
							//echo $input['invite_value'][$i];
							$email_to = $input['invite_value'][$i];
							$Notif = new Notification();
							$Notif->send_invite_training($training_id, $email_to); 
							$this->session->set_flashdata('notif', array('status' => true, 'msg' => 'Sukses!'));
						}
						$i++;
					}	
				}
			}
		}elseif($content == 'delete'){
			$invite_participant_id = $param;
			if($this->invite_participant_db->delete($invite_participant_id)){
				$this->session->set_flashdata('notif', array('status' => true, 'msg' => 'Sukses!'));
			}else{
				$this->session->set_flashdata('notif', array('status' => false, 'msg' => 'Gagal!'));
			}
		
		}
		
		
		
		redirect($this->session->userdata('redirect').'#tab_participant');
    }
	
    private function _objectToArray($d) {
		if (is_object($d)) {
			// Gets the properties of the given object
			// with get_object_vars function
			$d = get_object_vars($d);
		}
	
		if (is_array($d)) {
			/*
			* Return array converted to object
			* Using __FUNCTION__ (Magic constant)
			* for recursive call
			*/
			return array_map(array($this, '_objectToArray'), $d);
			
		}
		else {
			// Return array
			return $d;
		}
    }
}

/* End of file pages.php */
/* Location: ./application/controllers/superadmin/pages.php */