<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Callback extends CI_Controller {
    
    function __construct(){
      parent::__construct();
    }
    
    function index(){
	$connect_url = $this->config->item('connect_url');
	
	$code = $this->input->get('code');
	$state = base64_decode($this->input->get('state'));
	
	if($code && $state){
	    $client_id = $this->config->item('client_id');
	    $client_secret = $this->config->item('client_secret');
	    
	    $input = array(
		'grant_type' => 'authorization_code',
		'code' => $code,
		
	    );
	    
	    $auth = array(
		'username' => $client_id,
		'password' => $client_secret
	    );
	    
	    /*
		$header = array(
			'Content-Type'=>'application/x-www-form-urlencoded'
	    );
	    */
		
		$header = array("Content-type: multipart/form-data",'Expect:  ');

	    $response = curl_post($connect_url.'/oauth/token', $input, $auth, $header);

	    if($response && $response['header_info']['http_code'] == 200){
		$response_body = json_decode($response['body'],true);
		
		
		$input = array(
		    'access_token' => $response_body['access_token']
		);
		
		$response = curl_get($connect_url."/oauth/check_token", $input);
		if($response && $response['header_info']['http_code'] == 200){
		    $response_body = json_decode($response['body'],true);
		    
		    if($response_body['status'] == 'OK'){
			$data =  new StdClass();
			$data->id = $response_body['user_id'];
			$data->access_token = $input['access_token'];
			$this->connect_auth->_set_session($data);
			redirect($state);	
		    }
		}
	    }
	}
	
	redirect('');
    }
    
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */