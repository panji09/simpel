<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Participant extends CI_Controller {
    
    function index(){
		$data['title'] = $this->config->item('title').' : Dashboard Peserta';
		$data['content'] = 'dashboard/participant_dashboard';
		$this->load->view('home/main',$data);		
    }
	
	function training() {
		$data['title'] = $this->config->item('title').' : Dashboard-Training';
		$data['content'] = 'dashboard/participant_training';
		$this->load->view('home/main',$data);
	}

	function training_history() {
		$data['title'] = $this->config->item('title').' : Dashboard-Training';
		$data['content'] = 'dashboard/participant_training_history';
		$this->load->view('home/main',$data);
	}

	function training_notification() {
		$data['title'] = $this->config->item('title').' : Dashboard-Training';
		$data['content'] = 'dashboard/participant_notification';
		$this->load->view('home/main',$data);
	}
}

/* End of file welcome.php */
/* Location: ./dashboard/participant.php */