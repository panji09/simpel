<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Management_training extends CI_Controller {
    
    function __construct(){
		parent::__construct();
		session_start();
		config_kcfinder(array('user_id' => $this->connect_auth->get_user_id()));	
		// if($this->connect_auth->get_me()['entity']=='narasumber')
		// check_verified_institute();
    }
    function training() {
	check_token($this->connect_auth->get_access_token(), $allow_role = array('peserta'));
	$this->session->set_userdata('redirect',current_url());
	$data['title'] = $this->config->item('title').' : Dashboard-Training';
	$data['content'] = 'dashboard/participant_training';
	$this->load->view('home/main',$data);
    }
}
?> 
