<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once APPPATH.'/controllers/notification.php';

class Training extends CI_Controller {
	public function schedule($content = 'home', $training_name_id = null){
		$data['title'] = $this->config->item('title').' : Jadwal Pelatihan';
		
		$data['training_name_id'] = $training_name_id;
		
		//var_dump($training_name_id);
		
		if($content == ''){
			redirect('schedule/program');
		}
		
		if($content == 'program' && !$training_name_id){
		    $data['content'] = 'schedule_training';
		
		}elseif($content == 'program'){
		    $data['content'] = 'program_training_detail';
			
			//for meta socmed rudiest
			//$query = $this->training_db->get($training_id); 
			
			$query1 = $this->config_training_db->get($training_name_id);
			
			
			if(!isset($query1[0]['name']))
				$query1[0]['name'] = "Sistem Informasi Pelatihan";
			
			if(!isset($query1[0]['training_description']))
				$query1[0]['training_description'] = "Sistem Informasi Pelatihan";
			
			//if(!isset($query[0]['cover']) && $query[0]['cover'] == '')
				$query1[0]['cover'] = $this->config->item('home_img').'/logo-lkpp.png';
				
			$data['meta'] = array (
								   'on'=>'1',
								   'title'=>strip_tags($query1[0]['name']),
								   'description'=>ellipse(strip_tags($query1[0]['training_description']),300),
								   'cover'=>$query1[0]['cover'],
								   );
			//end meta socmed
			
			
		    
		}
		
		if($content == 'search'){
		    $data['content'] = 'schedule_training_institute';
		}
		
		if($content == 'home'){
			$data['content'] = 'schedule_training_institute';
		}
		$this->load->view('home/main',$data);
	}
	
	public function index($training_id=null){
		$me = ($this->connect_auth->get_access_token() ? $this->connect_auth->get_me() : false);
		
		
		if($me){
		    if($this->uri->segment(2) == 'index'){
			redirect('training/participant_success/'.$training_id);
		    }
		}else{
		    if($this->uri->segment(2) == 'participant_success'){
			redirect('training/index/'.$training_id);
		    }
		}
		
		$data['title'] = $this->config->item('title').' : Detail Jadwal Pelatihan';
		
		$data['training_id'] = $training_id;
		$data['content'] = 'schedule_training_detail';
		//for meta socmed rudiest
			$query = $this->training_db->get($training_id); 
			
			$query1 = $this->config_training_db->get($query[0]['training_name']['id']);
			
			
			
			//var_dump($query1[0]);
			//echo 'TETTT';
			//var_dump(strip_tags($query1[0]['training_description']));
			
			
			if(!isset($query[0]['training_name']['name']))
				$query[0]['training_name']['name'] = "Sistem Informasi Pelatihan";
			
			if(!isset($query1[0]['training_description']))
				$query1[0]['training_description'] = "Sistem Informasi Pelatihan";
			
			//if(!isset($query[0]['cover']) && $query[0]['cover'] == '')
				$query[0]['cover'] = $this->config->item('home_img').'/logo-lkpp.png';
				
			$data['meta'] = array (
								   'on'=>'1',
								   'title'=>strip_tags($query[0]['training_name']['name']),
								   'description'=>ellipse(strip_tags($query1[0]['training_description']),300),
								   'cover'=>$query[0]['cover'],
								   );
			//end meta socmed
		
		
		$this->load->view('home/main',$data);
	}
	
	public function participant_success($training_id=null){
		session_start();
		config_kcfinder(array('user_id' => $this->connect_auth->get_user_id()));
		$this->session->set_userdata('redirect',current_url());
		
		$this->index($training_id);
	}
	
	function check_requirement_training_ajax($training_id=null, $join_training_id = null){
// 	    check_token($this->connect_auth->get_access_token(), $allow_role = array('peserta'));
	    $this->load->view('home/inc/join_requirement_training', array('training_id' => $training_id, 'join_training_id' => $join_training_id) );
	
	}
	
	function join_post($training_id, $join_training_id = null){
// 	    print_r($_POST);
	    check_token($this->connect_auth->get_access_token(), $allow_role = array('peserta'));
	    $input = $this->input->post();
	    $query = $this->training_db->get($training_id);
	    
	    if($input && $query){
		
		//check requirement 
		$training = $query[0];
		$query_program_training = $this->config_training_db->get($training['training_name']['id']);
		
		if($query_program_training){
		    $program_training = $query_program_training[0];
		    $this->form_validation->set_rules();
		    if($program_training['requirement_letter_assignment']){
			$this->form_validation->set_rules('letter_of_assignment_id', 'No Surat Tugas', 'required');
			$this->form_validation->set_rules('letter_of_assignment_upload', 'Upload Surat Tugas', 'required');
			
		    }
		    
		    if($program_training['requirement_certificate']){
			//certificate_procurement_expert_id
			//certificate_procurement_expert_upload
			
			$this->form_validation->set_rules('certificate_procurement_expert_id', 'No Sertifikat Ahli Pengadaan', 'required');
			$this->form_validation->set_rules('certificate_procurement_expert_upload', 'Upload Sertifikat Ahli Pengadaan', 'required');
			
		    }
		    
		    if($program_training['enable_support_document']){
			//require_document_name
			//require_document_upload
			$this->form_validation->set_rules('require_document_name[]', 'Upload Dokumen Pendukung', 'required');
			$this->form_validation->set_rules('require_document_upload[]', 'Upload Dokumen Pendukung', 'required');
			
		    }
		    
		    if ($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('notif', array('status' => false, 'msg' => validation_errors()));
			redirect($this->session->userdata('redirect'));
		    }
		    
		    
		}
	
		
		$input['training_id'] = $training_id;
		$input['training_log'] = $this->training_db->get($training_id)[0];
		$input['user_join'] = $this->connect_auth->get_me();
		$input['institute_approved'] = 0;
		$input['institute_approved_log'] = array();
		
		if(!$this->join_training_db->user_exist($training_id, $this->connect_auth->get_me()['user_id']) ){
		    //join baru
    // 		var_dump($this->join_training_db->user_exist($training_id, $this->connect_auth->get_me()['user_id']));
    // 		echo $training_id.' '.$this->connect_auth->get_me()['user_id']; exit();
		    if($this->join_training_db->save($join_training_id, $input)){
			$Notification = new Notification();
			$Notification->review_user_join_training($user_join = $input['user_join'], $training_id);
			$this->session->set_flashdata('notif', array('status' => true, 'msg' => 'Sukses!'));
		    }else{
			$this->session->set_flashdata('notif', array('status' => false, 'msg' => 'Join Pelatihan Gagal!'));
		    }
		    
		}else{
		    //cek lagi
		    if($join_training_id){
			if($this->join_training_db->save($join_training_id, $input)){
			    $Notification = new Notification();
			    $Notification->review_user_join_training($user_join = $input['user_join'], $training_id);
			    $this->session->set_flashdata('notif', array('status' => true, 'msg' => 'Sukses!'));
			}else{
			    $this->session->set_flashdata('notif', array('status' => false, 'msg' => 'Join Pelatihan Gagal!'));
			}
		    }else{
			$this->session->set_flashdata('notif', array('status' => false, 'msg' => 'Gagal!, sudah pernah join pelatihan yang sama'));
		    }
		    
		}
		
		redirect($this->session->userdata('redirect'));
		
	    }else{
		$this->session->set_flashdata('notif', array('status' => false, 'msg' => 'Gagal!'));
		redirect('training/index/'.$training_id);
	    }
	}
	
	
	
}