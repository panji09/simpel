<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Search extends CI_Controller {
	
	public function search_result_content() {
		$data['title'] = $this->config->item('title').' : Search';

		$data['content'] = 'search_result_content';
		$this->load->view('home/main',$data);
	}
	
	public function query() {
		$data['title'] = $this->config->item('title').' : Search';
		
		$data['search'] = stripHTMLtags($this->input->get('s'));
		$data['content'] = 'search_result_content';
		$this->load->view('home/main',$data);
	}
	
	function ajax_search_loadmore($created=null, $search=null){
	    $query = $this->pages_db->get_all(array('search' => $search, 'published' => 1, 'created_lt' => intval($created)),$limit=20, $offsset=0);
	    foreach($query as $row){
			echo $this->load->view('home/inc/loadmore_search',array('row'=>$row), true);
	    }
	}
	
	public function search_result_training() {
		$data['title'] = $this->config->item('title').' : Search';

		$data['content'] = 'search_result_training';
		$this->load->view('home/main',$data);
	}
}