<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Controller {
	
	
	public function institution($page = ''){
		$data['title'] = $this->config->item('title').' : Lembaga Pelatihan';
		
		if($page==''){
			redirect('institution/chart');
		}
	      
		if($page=='chart'){
			$data['content'] = 'institution_chart';
			$response = call_api_get($this->config->item('connect_api_url').'/user/all_lpp');
			$response_body = json_decode($response['body'], true);
			$data_chart = array();
			$accreditation = '';
			$sum_lpp = 0;
			if($response['header_info']['http_code'] == 200)
			foreach($response_body['data'] as $row){
				
				if(isset($row['verified_institute']) && $row['verified_institute'] == 1){
					if(strtolower($row['accreditation_level']) == 'terdaftar'){
						$accreditation = strtoupper($row['accreditation_level']);
						
					}else{
						$accreditation = 'Akreditasi '.strtoupper($row['accreditation_level']);
						
					}
				}else if(isset($row['verified_institute']) && $row['verified_institute'] == 0){
					$accreditation = 'Verifikasi';
					
				}else if(isset($row['verified_institute']) && $row['verified_institute'] == -1){
					$accreditation = 'Tunda';
					
				}
				
				if($accreditation){
					if(!isset($data_chart[strtoupper($accreditation)])){
						$data_chart[strtoupper($accreditation)] = 0;
					}
						
					$data_chart[strtoupper($accreditation)] += 1;	
					$sum_lpp += 1;
						
				}
				
				$accreditation = '';
			}
			
			if(isset($data_chart)){
				foreach($data_chart as $key => $val){
					$data_chart1[] = array(
										 'name' => $key,
										 'y' => $val
										 );
				}
				
				$data['data_chart_json'] = json_encode($data_chart1);
				$data['sum_lpp'] = $sum_lpp;
			}
		  

		}elseif($page=='accreditation'){
			
			$data['content'] = 'institution_accreditation';
		}elseif($page=='detail'){
			
			$data['content'] = 'institution_detail';
		}
		
		$this->load->view('home/main',$data);
	}
	
	public function instructor($page=''){
	      $data['title'] = $this->config->item('title').' : Pengajar';
	      
	      if($page==''){
		  redirect('instructor/chart');
	      }
	      
	      if($page=='chart'){
		  $data['content'] = 'instructor_chart';
		  
		  $query = file_get_contents($this->config->item('connect_api_url').'/location/province');
		  $province = array();
		  
		  if($query){
		      $province = json_decode($query,true);
		  }
		  
		  // print_r($province); exit();
		  $row = array();
		  if($province)
		  foreach($province as $row){
		      $temp = array(
							'id' => $row['id'],
							'name' => $row['name'],
							'count' => 0
							);
			  
		      $list_province[] = $temp;
		  }
		  
		  $response = call_api_get($this->config->item('connect_api_url').'/user/all_narasumber');
		  $response_body = json_decode($response['body'], true);
		  $row = array();
		  if($response['header_info']['http_code'] == 200)
		  foreach($response_body['data'] as $row){
		      
			  if(isset($list_count_province[intval($row['office_province']['id'])])){
				$list_count_province[intval($row['office_province']['id'])] += 1;
			  }else{
				$list_count_province[intval($row['office_province']['id'])] = 1;
				
			  }
			  
		  }
		  
		  //insert to array
		  $row = array();
		  foreach($list_province as $key => $row){
			if(isset($list_count_province[$row['id']])){
				$list_province[$key]['count'] = $list_count_province[$row['id']];
			}
		  }
		  
		  //print_r($list_province); exit();
	      
		  $row = $list_name = $list_value = array();
		  $sum = 0;
		  if($list_province){
		      foreach($list_province as $row){
					$list_value[] = array('y' => $row['count'], 'url' => site_url('instructor/province/'.$row['id']));
					//$temp[] = $row['count'];
					$sum += $row['count'];
					
					$list_name[] = $row['name'];
			  }
		  }
		  
		  $data['list_name'] = $list_name;
		  $data['list_value'] = $list_value;
		  $data['sum_instructor'] = $sum;
		  
// 		  print_r($list_name_province); exit();
	      }elseif($page == 'province'){
		  $data['content'] = 'instructor_province';
	      }elseif($page == 'search'){
		  $data['content'] = 'instructor_search';
	      }elseif($page == 'detail'){
	      	$data['content'] = 'instructor_detail';
	      }
	      
	      $this->load->view('home/main',$data);
	}
	
	
	
}