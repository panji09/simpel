<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pages extends CI_Controller {
	
	public function about(){
		$data['title'] = $this->config->item('title').' : Tentang';
		$data['content'] = 'about';
		$this->load->view('home/main',$data);
	}
	
	public function faq(){
		$data['title'] = $this->config->item('title').' : FAQ';
		$data['content'] = 'faq';
		$this->load->view('home/main',$data);
	}
	
	public function contact(){
		$data['title'] = $this->config->item('title').' : Kontak';
		$data['content'] = 'contact';
		$this->load->view('home/main',$data);
	}
	
	function contact_post(){
		
	}
	public function how_to(){
		$data['title'] = $this->config->item('title').' : Cara Penggunaan';
		$data['content'] = 'how_to';
		$this->load->view('home/main',$data);
	}
	
	public function gallery($content_id=null){
		$data['title'] = $this->config->item('title').' : Galeri Foto Album';
		
		$data['content_id'] = $content_id;
		if($content_id)
		    $data['content'] = 'gallery_detail';
		else
		    $data['content'] = 'gallery';
		    
		$this->load->view('home/main',$data);
	}
	
	function ajax_album_loadmore($created=null){
	    $query = $this->dynamic_pages_db->get_all(array('page_id'=> 'gallery','published' => 1, 'created_lt' => intval($created)),$limit=10, $offsset=0);
	    
	    foreach($query as $row){
			echo $this->load->view('home/inc/loadmore_album',array('row'=>$row), true);
	    }
	}
	
	public function gallery_detail(){
		$data['title'] = $this->config->item('title').' : Foto';
		$data['content'] = 'gallery_detail';
		$this->load->view('home/main',$data);
	}
	
	public function news($content_id=null){
		$data['title'] = $this->config->item('title').' : Berita';
		
		$data['content_id'] = $content_id;
		if($content_id)
		{
		    $data['content'] = 'news_detail';
			//for meta socmed rudiest
			$query	 = $this->dynamic_pages_db->get($content_id);
			
			if(!isset($query[0]['title']))
				$query[0]['title'] = "Sistem Informasi Pelatihan";
			
			if(!isset($query[0]['content']))
				$query[0]['content'] = "Sistem Informasi Pelatihan";
			
			if(!isset($query[0]['cover']) && $query[0]['cover'] == '')
				$query[0]['cover'] = $this->config->item('home_img').'/logo-lkpp.png';
				
			$data['meta'] = array (
								   'on'=>'1',
								   'title'=>strip_tags($query[0]['title']),
								   'description'=>ellipse(strip_tags($query[0]['content']),300),
								   'cover'=>$query[0]['cover'],
								   );
			//end meta socmed
			
			$data['meta_content_id'] =  $content_id;
			//$query = $this->dynamic_pages_db->get($content_id);
			
		
		}
		else
		{
		    $data['content'] = 'news';
		}
		
		$this->load->view('home/main',$data);
	}
	
	function ajax_news_loadmore($created=null){
	    $query = $this->dynamic_pages_db->get_all(array('page_id'=> 'news', 'published' => 1, 'created_lt' => intval($created)),$limit=10, $offsset=0);
	    foreach($query as $row){
			echo $this->load->view('home/inc/loadmore_news',array('row'=>$row), true);
	    }
	}
	
	public function regulatory_training(){
		$data['title'] = $this->config->item('title').' : Regulatory Training';
		$data['content'] = 'regulatory_training';
		$this->load->view('home/main',$data);
	}
	
	public function participant_dashboard(){
		$data['title'] = $this->config->item('title').' : Dashboard';
		$data['content'] = 'participant_dashboard';
		$this->load->view('home/main',$data);
	}
	
	public function lpp(){
		$data['title'] = $this->config->item('title').' : LPP';
		$data['content'] = 'lpp';
		$this->load->view('home/main',$data);
	}
	
	public function event($content_id=null) {
		$data['title'] = $this->config->item('title').' : Agenda';

		$data['content_id'] = $content_id;
		if($content_id)
		{
		    $data['content'] = 'event_single';
			//for meta socmed rudiest
			$query	 = $this->dynamic_pages_db->get($content_id);
			
			if(!isset($query[0]['title']))
				$query[0]['title'] = "Sistem Informasi Pelatihan";
			
			if(!isset($query[0]['content']))
				$query[0]['content'] = "Sistem Informasi Pelatihan";
			
			if(!isset($query[0]['cover']) && $query[0]['cover'] == '')
				$query[0]['cover'] = $this->config->item('home_img').'/logo-lkpp.png';
				
			$data['meta'] = array (
								   'on'=>'1',
								   'title'=>strip_tags($query[0]['title']),
								   'description'=>ellipse(strip_tags($query[0]['content']),300),
								   'cover'=>$query[0]['cover'],
								   );
			//end meta socmed
		}
		else
		    $data['content'] = 'event';
		
		$this->load->view('home/main',$data);
	}
	
	public function event_single($content_id=null) {
		$data['title'] = $this->config->item('title').' : Agenda';
		$data['content_id'] = $content_id;
		$data['content'] = 'event_single';
		//for meta socmed rudiest
			$query	 = $this->dynamic_pages_db->get($content_id);
			
			if(!isset($query[0]['title']))
				$query[0]['title'] = "Sistem Informasi Pelatihan";
			
			if(!isset($query[0]['content']))
				$query[0]['content'] = "Sistem Informasi Pelatihan";
			
			if(!isset($query[0]['cover']) && $query['cover'] == '')
				$query[0]['cover'] = $this->config->item('home_img').'/logo-lkpp.png';
				
			$data['meta'] = array (
								   'on'=>'1',
								   'title'=>strip_tags($query[0]['title']),
								   'description'=>ellipse(strip_tags($query[0]['content']),300),
								   'cover'=>$query[0]['cover'],
								   );
			//end meta socmed
		$this->load->view('home/main',$data);
	}
	
	public function home_sample_a() {
		$data['title'] = $this->config->item('title').' : Home A';

		$data['content'] = 'home_sample_a';
		$this->load->view('home/main',$data);
	}
	
	public function home_sample_b() {
		$data['title'] = $this->config->item('title').' : Home B';

		$data['content'] = 'home_sample_b';
		$this->load->view('home/main',$data);
	}
	
	public function home_sample_c() {
		$data['title'] = $this->config->item('title').' : Home B';

		$data['content'] = 'home_sample_c';
		$this->load->view('home/main',$data);
	}
	
}