<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {
    function __construct(){
	parent::__construct();
	
	// print_r($this->connect_auth->get_me());
    }
    public function index(){
		$data['title'] = $this->config->item('title').' : Home';

		$data['content'] = 'home_sample_c';
		$this->load->view('home/main',$data);
    }
	
}

/* End of file home.php */
/* Location: ./application/controllers/home/home.php */