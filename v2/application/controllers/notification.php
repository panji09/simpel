<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Notification extends CI_Controller {

	function __construct(){
		parent::__construct();
	}
	
	function send_invite_training($training_id = null, $email=null){
		//echo $training_id.' '.$email;
		$to = $subject = $message = '';
		$to = $email;
		
		if($training_id && $email && $this->training_db->exist($training_id)){
			$query = $this->training_db->get($training_id);
			$training = $query[0];
			
			$response = call_api_get($this->config->item('connect_api_url').'/user/exist/email?value='.$email);
			$response_body = json_decode($response['body'], true);

			$user = array();
			if($response['header_info']['http_code'] == 200){
				//print_r($response_body);
				//exit();
				$result = $response_body;
				
				$subject = $training['user_submission']['training_institute'].' mengundang anda untuk registrasi sebagai Peserta';
					
				$invite_participant_code = sha1_salt(time());
				$data_input['invite_participant_code'] = $invite_participant_code;
				$data_input['email'] = $email;
				$data_input['training_id'] = $training_id;
				
				$query = $this->invite_participant_db->get_email($email);
				$invite_participant_id = (count($query) && $query > 0 ? $query[0]['_id']->{'$id'} : null);
				$this->invite_participant_db->save($invite_participant_id, $data_input);
				$data['title'] = $training['user_submission']['training_institute'].' Mengundang Anda untuk Registrasi';
				$data['title_overview'] = 'Hai, '.$training['user_submission']['training_institute'].' Mengundang anda untuk registrasi sebagai Peserta di pelatihan '.$training['training_name'].' pada tanggal '.$training['training_date'].'di '.$training['training_location_address'].', '.$training['training_location_district']['name'].' - '.$training['training_location_province']['name'];
				
				if($result['exist']){
					//sudah teregister di connect
					$data['title_detail'] = 'Akun anda sudah terdaftar di sistem kami, Untuk daftar pelatihan silahkan klik dibawah ini';
					
				}else{
					//belum teregister di connect
					$data['title_detail'] = 'Akun anda belum terdaftar di sistem kami, Untuk daftar pelatihan silahkan klik dibawah ini';
					
				}
				$data['link'] = array(
					'url' => site_url('training/index/'.$training_id.'?invite_participant_code='.$invite_participant_code),
					'name' => 'Daftar Pelatihan'
					);
	
				$message = $this->load->view('email/email_template', $data,TRUE);
				//exist();
			}
		}
		
		if($to && $subject && $message)
			$this->send_email($to, $subject, $message);
	}
	function send_invite($entity=null, $email=null){

		$to = $email;

		if($entity == 'lpp'){
			$subject = 'SIMPEL-LKPP mengundang anda untuk registrasi sebagai Lembaga Pelatihan';

	    //$message = 'Silahkan klik link berikut ini untuk registrasi '.site_url('login/register_institution');

			$data['title'] = 'SIMPEL-LKPP Mengundang Anda untuk Registrasi';
			$data['title_overview'] = 'Hai, SIMPEL-LKPP Mengundang anda untuk registrasi sebagai Lembaga Pelatihan';
			$data['title_detail'] = 'Untuk registrasi silahkan klik dibawah ini';
			$data['link'] = array(
				'url' => site_url('login/register_institution'),
				'name' => 'Register'
				);

			$message = $this->load->view('email/email_template', $data,TRUE);

		}

		if($entity == 'peserta'){
			$subject = 'SIMPEL-LKPP mengundang anda untuk registrasi sebagai Peserta';
	    //$message = 'Silahkan klik link berikut ini untuk registrasi '.site_url('login/register_participant');
			$data['title'] = 'SIMPEL-LKPP Mengundang Anda untuk Registrasi';
			$data['title_overview'] = 'Hai, SIMPEL-LKPP Mengundang anda untuk registrasi sebagai Peserta';
			$data['title_detail'] = 'Untuk registrasi silahkan klik dibawah ini';
			$data['link'] = array(
				'url' => site_url('login/register_participant'),
				'name' => 'Register'
				);

			$message = $this->load->view('email/email_template', $data,TRUE);
		}

		if($entity == 'narasumber'){
			$subject = 'SIMPEL-LKPP mengundang anda untuk registrasi sebagai Pengajar';
	    //$message = 'Silahkan klik link berikut ini untuk registrasi '.site_url('login/register_instructor');
			$invite_code = sha1_salt(time());
			$data_input['invite_code'] = $invite_code;
			$data_input['email'] = $email;
			$data_input['entity'] = $entity;
			
			$query = $this->invite_code_db->get_email($email);
			$invite_code_id = (count($query) && $query > 0 ? $query[0]['_id']->{'$id'} : null);
			$this->invite_code_db->save($invite_code_id, $data_input);
			$data['title'] = 'SIMPEL-LKPP Mengundang Anda untuk Registrasi';
			$data['title_overview'] = 'Hai, SIMPEL-LKPP Mengundang anda untuk registrasi sebagai Pengajar/Narasumber';
			$data['title_detail'] = 'Untuk registrasi silahkan klik dibawah ini';
			$data['link'] = array(
				'url' => site_url('login/register_instructor/'.$invite_code),
				'name' => 'Register'
				);

			$message = $this->load->view('email/email_template', $data,TRUE);
		}

		if($entity && $email)
			$this->send_email($to, $subject, $message);
	}

	function lpp_review_news($data_lpp=null){
	//kirim notif dari lpp ke superadmin utk review news
		if($data_lpp){
			$subject = 'Review Berita';

			$data['title'] = 'Review Berita dari LPP';
			$data['title_overview'] = 'Hai %fullname%, Lembaga Pelatihan <strong>'.$data_lpp['training_institute'].'</strong> telah menambahkan berita dan menunggu review anda!';
			$data['title_detail'] = 'Untuk review berita silahkan klik dibawah ini';
			$data['link'] = array(
				'url' => site_url('superadmin/pages/news'),
				'name' => 'Review '
				);

			$message = $this->load->view('email/email_template', $data,TRUE);

			$this->broadcast_superadmin($subject, $message);
		}
	}

	function status_review_news($data_news = null){
	//kirim notif dari superadmin ke lpp status review news
		if($data_news){
			$token = $this->connect_auth->get_access_token();
			$response = call_api_get($this->config->item('connect_api_url').'/user/index/'.$token.'/'.$data_news['creator']['user_id']);
			$response_body = json_decode($response['body'], true);

			$user = array();
			if($response['header_info']['http_code'] == 200){
				$user = $response_body;

				$to = $user['email'];
				$subject = 'Status Review Berita';

				$data['title'] = 'Status Review Berita Anda';
				$data['title_overview'] = 'Hai '.$user['training_institute'].', Berita anda yang berjudul <strong>'.$data_news['title'].'</strong> telah ditayangkan!';
				$data['title_detail'] = 'Untuk lebih lengkap silahkan klik dibawah ini';
				$data['link'] = array(
					'url' => site_url('lpp/pages/news'),
					'name' => 'Detail'
					);

				$message = $this->load->view('email/email_template', $data,TRUE);

				$this->send_email($to, $subject, $message);
			}

		}
	}

	function lpp_review_gallery($data_lpp=null){
	//kirim notif dari lpp ke superadmin utk review gallery
		if($data_lpp){
			$subject = 'Review Galeri Foto';

			$data['title'] = 'Review Galeri Foto dari LPP';
			$data['title_overview'] = 'Hai %fullname%, Lembaga Pelatihan <strong>'.$data_lpp['training_institute'].'</strong> telah menambahkan Galeri Foto dan menunggu review anda!';
			$data['title_detail'] = 'Untuk review Galeri Foto silahkan klik dibawah ini';
			$data['link'] = array(
				'url' => site_url('superadmin/pages/gallery'),
				'name' => 'Review '
				);

			$message = $this->load->view('email/email_template', $data,TRUE);

			$this->broadcast_superadmin($subject, $message);
		}
	}

	function status_review_gallery($data_gallery = null){
	//kirim notif dari superadmin ke lpp status review gallery

		if($data_gallery){
			$token = $this->connect_auth->get_access_token();
			$response = call_api_get($this->config->item('connect_api_url').'/user/index/'.$token.'/'.$data_gallery['creator']['user_id']);
			$response_body = json_decode($response['body'], true);

			$user = array();
			if($response['header_info']['http_code'] == 200){
				$user = $response_body;

				$to = $user['email'];
				$subject = 'Status Review Galeri Foto';

				$data['title'] = 'Status Review Galeri Foto Anda';
				$data['title_overview'] = 'Hai '.$user['training_institute'].', Galeri Foto anda yang berjudul <strong>'.$data_gallery['title'].'</strong> telah ditayangkan!';
				$data['title_detail'] = 'Untuk lebih lengkap silahkan klik dibawah ini';
				$data['link'] = array(
					'url' => site_url('lpp/pages/gallery'),
					'name' => 'Detail'
					);

				$message = $this->load->view('email/email_template', $data,TRUE);

				$this->send_email($to, $subject, $message);
			}

		}
	}

	function choose_instructor($data_admin=null, $facilitator_user_id=null, $data_training=null){
	//notif ke fasilitator utk milih pengajar
		if($data_admin && $facilitator_user_id && $data_training){
			$token = $this->connect_auth->get_access_token();
			$response = call_api_get($this->config->item('connect_api_url').'/user/index/'.$token.'/'.$facilitator_user_id);
			$response_body = json_decode($response['body'], true);
			
		

			$user = array();
			if($response['header_info']['http_code'] == 200){
				$user = $response_body;

				$to = $user['email'];
		//template email
				$subject = 'Status Pemilihan Pengajar Pelatihan';

				$data['title'] = 'Pemilihan Pengajar Pelatihan ';
				$data['title_overview'] = 'Hai '.$user['fullname'].', Admin <strong>'.$data_admin['fullname'].'</strong> meminta anda untuk menfasilitasi pemilihan narasumber <strong>'.$data_training['user_submission']['training_institute'].'</strong> untuk pelatihan <strong>'.$data_training['training_name']['name'].'</strong> pada tanggal '.$data_training['training_date'].'';
				$data['title_detail'] = 'Untuk lebih detail silahkan klik dibawah ini';
				$data['link'] = array(
					'url' => site_url('fasilitator/management_training/training'),
					'name' => 'Review '
					);

				$message = $this->load->view('email/email_template', $data,TRUE);

				$this->send_email($to, $subject, $message);
			}
		}

	}

	function review_nominate_instructor($data_fasilitator = null, $training_id = null){
	//notif ke admin, utk mereview pengajar yg dipilih fasilitator

		if($data_fasilitator && $training_id){
			if($query = $this->training_db->get($training_id)){
				$training = $query[0];

				$subject = 'Review Nominasi Pengajar';

				$data['title'] = 'Review Nominasi Pengajar';
				$data['title_overview'] = 'Hai %fullname%, Fasilitator <strong>'.$data_fasilitator['fullname'].'</strong> telah memilih pengajar  untuk Lembaga Pelatihan <strong>'.$training['user_submission']['training_institute'].'</strong> di pelatihan <strong>'.$training['training_name']['name'].'</strong> pada tanggal '.$training['training_date'].' dan menunggu review anda!';
				$data['title_detail'] = 'Untuk review nominasi pengajar silahkan klik dibawah ini';
				$data['link'] = array(
					'url' => site_url('admin/approval/instructor'),
					'name' => 'Review '
					);

				$message = $this->load->view('email/email_template', $data,TRUE);

				$this->broadcast_admin($subject, $message);
			}
		}
	}

	function review_training_instructor($instructor_id = null, $training_id = null){
	//notif ke pengajar, utk approval menjadi narasumber
		if($instructor_id && $training_id){
			$token = $this->connect_auth->get_access_token();
			$response = call_api_get($this->config->item('connect_api_url').'/user/index/'.$token.'/'.$instructor_id);
			$response_body = json_decode($response['body'], true);

			$user = array();
			if($response['header_info']['http_code'] == 200){
				$user = $response_body;

				if($query = $this->training_db->get($training_id)){
					$training = $query[0];

					$to = $user['email'];
					$subject = 'Konfirmasi Narasumber Pelatihan';

					$data['title'] = 'Konfirmasi Menjadi Narasumber Pelatihan';
					$data['title_overview'] = 'Hai '.$user['fullname'].', Anda dipilih untuk menjadi Narasumber untuk Lembaga Pelatihan <strong>'.$training['user_submission']['training_institute'].'</strong> di pelatihan <strong>'.$training['training_name']['name'].'</strong> pada tanggal '.$training['training_date'].' dan menunggu Konfirmasi anda!';
					$data['title_detail'] = 'Untuk lebih lengkap, silahkan klik dibawah ini';
					$data['link'] = array(
						'url' => site_url('narasumber/management_training/training'),
						'name' => 'Konfirmasi'
						);

					$message = $this->load->view('email/email_template', $data,TRUE);

					$this->send_email($to, $subject, $message);
				}
			}
		}
	}

	function status_training_instructor($instructor_id = null, $training_id = null, $status_approval = null, $data_hour_leasson=null){
	//notif ke admin, ketersediaan pengajar menjadi narasumber
		if($instructor_id && $training_id && $status_approval && $data_hour_leasson){

			$token = $this->connect_auth->get_access_token();
			$response = call_api_get($this->config->item('connect_api_url').'/user/index/'.$token.'/'.$instructor_id);
			$response_body = json_decode($response['body'], true);

			$user = array();
			if($response['header_info']['http_code'] == 200){
				$user = $response_body;

				if($query = $this->training_db->get($training_id)){
					$training = $query[0];

					$subject = 'Status Konfirmasi Narasumber Pelatihan';

					if($status_approval == 1){
			//disetujui
						$data['title'] = 'Status Konfirmasi Menjadi Narasumber Pelatihan';
						$data['title_overview'] = 'Hai %fullname%, Pengajar '.$user['fullname'].' <strong>telah Setuju</strong> menjadi Narasumber untuk Lembaga Pelatihan <strong>'.$training['user_submission']['training_institute'].'</strong> di pelatihan <strong>'.$training['training_name']['name'].'</strong> pada tanggal '.$training['training_date'];
						$data['title_detail'] = 'Untuk lebih lengkap, silahkan klik dibawah ini';
						$data['link'] = array(
							'url' => site_url('admin/approval/instructor'),
							'name' => 'Detail'
							);

					}elseif($status_approval == -2){
			//ditolak
						$data['title'] = 'Status Konfirmasi Menjadi Narasumber Pelatihan';
						$data['title_overview'] = 'Hai %fullname%, Pengajar '.$user['fullname'].' <strong>telah Menolak</strong> menjadi Narasumber untuk Lembaga Pelatihan <strong>'.$training['user_submission']['training_institute'].'</strong> di pelatihan <strong>'.$training['training_name']['name'].'</strong> pada tanggal '.$training['training_date'];
						$data['title_detail'] = 'Alasannya :<br>';
						$data['title_detail'] .= $data_hour_leasson['review_reject_note'];

						$data['link'] = array(
							'url' => site_url('admin/approval/instructor'),
							'name' => 'Detail'
							);

					}

					$message = $this->load->view('email/email_template', $data,TRUE);

					$this->broadcast_admin($subject, $message);
				}
			}
		}
	}

	function review_training_submission($user_lpp_id=null){
	//notif ke admin, ada lpp yang ngajuin pelatihan
		if($user_lpp_id){
			$token = $this->connect_auth->get_access_token();
			$response = call_api_get($this->config->item('connect_api_url').'/user/index/'.$token.'/'.$user_lpp_id);
			$response_body = json_decode($response['body'], true);

			$user = array();
			if($response['header_info']['http_code'] == 200){
				$user = $response_body;

		//template email
				$subject = 'Review Pengajuan Fasilitasi Pelatihan';

				$data['title'] = 'Lembaga Pelatihan Mengajukan Fasilitasi Pelatihan dan Narasumber ';
				$data['title_overview'] = 'Hai %fullname%, Lembaga Pelatihan <strong>'.$user['training_institute'].'</strong> menunggu review anda!';
				$data['title_detail'] = 'Untuk review Pengajuan Fasilitasi Pelatihan klik dibawah ini';
				$data['link'] = array(
					'url' => site_url('admin/approval/submission_training'),
					'name' => 'Review '
					);

				$message = $this->load->view('email/email_template', $data,TRUE);

				$this->broadcast_admin($subject, $message);
			}
		}
	}

	function review_user_join_training($user_join=null, $training_id=null){
		if($user_join && $training_id){
			if($query = $this->training_db->get($training_id)){
				$training = $query[0];

		/*
		  kirim notif ke lpp
		*/
		//template email
		  $to = $training['user_submission']['email'];
		  $subject = 'Review Join Pelatihan Baru';

		  $data['title'] = 'Peserta baru mendaftar pelatihan';
		  $data['title_overview'] = 'Hai '.$training['user_submission']['training_institute'].', Peserta <strong>'.$user_join['fullname'].'</strong> ingin mendaftar di pelatihan <strong>'.$training['training_name']['name'].'</strong> pada tanggal '.$training['training_date'].' dan menunggu review anda!';
		  $data['title_detail'] = 'Untuk review join peserta silahkan klik dibawah ini';
		  $data['link'] = array(
		  	'url' => site_url('lpp/management_training/approve_participant'),
		  	'name' => 'Review '
		  	);

		  $message = $this->load->view('email/email_template', $data,TRUE);

		  $this->send_email($to, $subject, $message);

		/*
		  kirim notif ke peserta
		*/
		//template email
		  $to = $user_join['email'];
		  $subject = 'Status Join Pelatihan';

		  $data['title'] = 'Status join pelatihan';
		  $data['title_overview'] = 'Hai '.$user_join['fullname'].', Status join pelatihan <strong>'.$training['training_name']['name'].'</strong> pada tanggal '.$training['training_date'].' anda sedang direview oleh <strong>'.$training['user_submission']['training_institute'].'</strong> ';
		  $data['title_detail'] = 'Untuk melihat status pendaftaran pelatihan anda bisa diklik dibawah ini';
		  $data['link'] = array(
		  	'url' => site_url('peserta/management_training/training'),
		  	'name' => 'Cek Join Peserta '
		  	);

		  $message = $this->load->view('email/email_template', $data,TRUE);

		  $this->send_email($to, $subject, $message);


		}
	}
}

function send_review_join_training($join_training_id = null, $status_join_training = null){
	//peserta notif ke lpp, utk review
	if($join_training_id && $status_join_training){

		if($query = $this->join_training_db->get($join_training_id)){
			$join_training = $query[0];
			$user = $join_training['user_join'];

			if($query = $this->training_db->get($join_training['training_id'])){
				$training = $query[0];
				$to = $user['email'];
				$subject = 'Status Verifikasi Join Pelatihan';

				if($status_join_training == 1){
			//diapprove

			//template email
					$data['title'] = 'Status review join pelatihan anda';
					$data['title_overview'] = 'Hai '.$user['fullname'].', join pelatihan <strong>'.$training['training_name']['name'].'</strong> anda di '.$training['user_submission']['training_institute'].' pada tanggal '.$training['training_date'].' telah <strong>Disetujui</strong>';
					$data['title_detail'] = 'Untuk lebih detail. Silahkan klik dibawah ini';
					$data['link'] = array(
						'url' => site_url('peserta/management_training/training'),
						'name' => 'Detail'
						);

					$message = $this->load->view('email/email_template', $data,TRUE);

				}elseif($status_join_training == -1){
			//ditunda

			//template email
					$data['title'] = 'Status review join pelatihan anda';
					$data['title_overview'] = 'Hai '.$user['fullname'].', status join pelatihan <strong>'.$training['training_name']['name'].'</strong> anda di '.$training['user_submission']['training_institute'].' pada tanggal '.$training['training_date'].' sedang <strong>Ditunda</strong> karena membutuhkan <strong>Klarifikasi</strong>!';
					$data['title_detail'] = $join_training['review_note'];
					$data['title_detail'] .= '<br>Silahkan revisi dokumen pendaftaran anda dibawah ini';

					$data['link'] = array(
						'url' => $this->config->item('connect_url'),
						'name' => 'Revisi'
						);

					$message = $this->load->view('email/email_template', $data,TRUE);

				}elseif($status_join_training == -2){
			//ditolak

			//template email
					$data['title'] = 'Status review join pelatihan anda';
					$data['title_overview'] = 'Hai '.$user['fullname'].', status join pelatihan <strong>'.$training['training_name']['name'].'</strong> anda di '.$training['user_submission']['training_institute'].' pada tanggal '.$training['training_date'].' telah <strong>Ditolak</strong>';
					$data['title_detail'] = $join_training['review_reject'];


					$message = $this->load->view('email/email_template', $data,TRUE);
				}

				$this->send_email($to, $subject, $message);
			}
		}


	}
	
}

function send_status_approval_training_submission($training_id = null, $status_submission_training=null){
	//approval pendaftaran lpp oleh admin
	if($training_id && $status_submission_training){

		if($query = $this->training_db->get($training_id)){
			$training = $query[0];
			$user = $training['user_submission'];
			$to = $user['email'];
			$subject = 'Status Pengajuan Fasilitasi Pelatihan dan Narasumber';

			if($status_submission_training == 1){
		    //diapprove

		    //template email
				$data['title'] = 'Status review Pengajuan Fasilitasi Pelatihan dan Narasumber';
				$data['title_overview'] = 'Hai '.$user['training_institute'].', pengajuan fasilitasi anda telah <strong>Disetujui</strong> ';
				$data['title_detail'] = 'Modul bahan ajar yang terbaru bisa di unduh disini';
				$data['link'] = array(
					'url' => site_url('lpp/management_training/training/detail/'.$training_id),
					'name' => 'Unduh Bahan Ajar Terbaru'
					);

				$message = $this->load->view('email/email_template', $data,TRUE);

			}

			elseif($status_submission_training == -1){
		    //ditunda

		    //template email
				$data['title'] = 'Status review Pengajuan Fasilitasi Pelatihan dan Narasumber';
				$data['title_overview'] = 'Hai '.$user['training_institute'].', pengajuan fasilitasi anda <strong>Ditunda</strong> karena membutuhkan <strong>Klarifikasi</strong>!';
				$data['title_detail'] = $training['review_note'];
				$data['title_detail'] .= '<br>Silahkan revisi dokumen pengajuan fasilitasi anda dibawah ini';

				$data['link'] = array(
					'url' => site_url('lpp/management_training/training#tab_submission_training_history'),
					'name' => 'Revisi'
					);

				$message = $this->load->view('email/email_template', $data,TRUE);

			}elseif($status_submission_training == -2){
		    //ditolak

		    //template email
				$data['title'] = 'Status review Pengajuan Fasilitasi Pelatihan dan Narasumber';
				$data['title_overview'] = 'Hai '.$user['training_institute'].', pengajuan fasilitasi anda <strong>Ditolak</strong>';
				$data['title_detail'] = $training['review_reject'];


				$message = $this->load->view('email/email_template', $data,TRUE);
			}

			$this->send_email($to, $subject, $message);
		}

	}
	
}

function send_status_approval_institute($user_id=null, $verified_institute=null){
	//approval pendaftaran lpp oleh admin
	if($user_id && $verified_institute){
		$token = $this->connect_auth->get_access_token();
		$response = call_api_get($this->config->item('connect_api_url').'/user/index/'.$token.'/'.$user_id);
		$response_body = json_decode($response['body'], true);

		$user = array();
		if($response['header_info']['http_code'] == 200){
			$user = $response_body;

			$to = $user['email'];
			$subject = 'Status Verifikasi Pendaftaran';

			if($verified_institute == 1){
		    //diapprove
		    /*
		    $message = '
			Nama Lembaga : '.$user['training_institute'].'<br>
			Status Lembaga  : '.$user['training_institute_status'].'<br>
			Email : '.$user['email'].'<br>
			Status : Disetujui<br>
			Akreditasi : '.$user['accreditation_level'].'<br>
			Login : '.site_url('').'
		    ';
		    */
		    
		    //template email
		    $data['title'] = 'Status review akun anda';
		    $data['title_overview'] = 'Hai '.$user['training_institute'].', pengajuan pendaftaran anda telah <strong>Disetujui</strong> dengan akreditasi <strong>'.strtoupper($user['accreditation_level']).'</strong>!';
		    $data['title_detail'] = 'Akun anda sekarang bisa digunakan. Silahkan klik dibawah ini untuk login';
		    $data['link'] = array(
		    	'url' => site_url('login'),
		    	'name' => 'Login'
		    	);
		    
		    $message = $this->load->view('email/email_template', $data,TRUE);
		    
		  }elseif($verified_institute == -1){
		    //ditunda
		    /*
		    $message = '
			Nama Lembaga : '.$user['training_institute'].'<br>
			Status Lembaga  : '.$user['training_institute_status'].'<br>
			Email : '.$user['email'].'<br>
			Status : Ditunda<br>
			Pesan : '.$user['review_note'].'<br>
			Perbaiki data anda : '.$this->config->item('connect_url').'
		    ';
		    */
		    //template email
		    $data['title'] = 'Status review akun anda';
		    $data['title_overview'] = 'Hai '.$user['training_institute'].', pendaftaran anda <strong>Ditunda</strong> karena membutuhkan <strong>Klarifikasi</strong>!';
		    $data['title_detail'] = $user['review_note'];
		    $data['title_detail'] .= '<br>Silahkan revisi dokumen pendaftaran anda dibawah ini';
		    
		    $data['link'] = array(
		    	'url' => $this->config->item('connect_url'),
		    	'name' => 'Revisi'
		    	);
		    
		    $message = $this->load->view('email/email_template', $data,TRUE);

		  }elseif($verified_institute == -2){
		    //ditolak
		    /*
		    $message = '
			Nama Lembaga : '.$user['training_institute'].'<br>
			Status Lembaga  : '.$user['training_institute_status'].'<br>
			Email : '.$user['email'].'<br>
			Status : Ditolak<br>
			
		    ';
		    */
		    //template email
		    $data['title'] = 'Status review akun anda';
		    $data['title_overview'] = 'Hai '.$user['training_institute'].', pendaftaran anda <strong>Ditolak</strong>';
		    $data['title_detail'] = $user['review_reject'];
		    
		    
		    $message = $this->load->view('email/email_template', $data,TRUE);
		  }

		  $this->send_email($to, $subject, $message);
		}

	}
	
}

public function ch_confirmation_email($fullname, $email, $code_email_verification){
		$subject = 'Verifikasi Email';  
	    //template email
	    $data['title'] = 'Verifikasi Email';
	    $data['title_overview'] = 'Hai '.$fullname.', lengkapi proses pengaduan anda!';
	    $data['title_detail'] = 'Proses pengaduan anda hampir selesai, klik link konfirmasi dibawah ini untuk menyelesaikan proses pengaduan.';
	    $data['link'] = array(
				'url' => site_url('pengaduan/code_email_verification/code/'.$code_email_verification),
				'name' => 'Konfirmasi Email'
	    );
	    
	    $message = $this->load->view('email/email_template', $data,TRUE);
	    $this->send_email($to = $email, $subject, $message);
}	

private function send_email($to, $subject, $message){
	
	$url = $this->config->item('connect_api_url').'/send_email/queuemail';
	
	$input = array(
		'to' => $to,
		'subject' => $subject,
		'message' => $message
		);
	
	call_api_post($url, $input);
}

private function broadcast_admin($subject, $message){
	
	$url = $this->config->item('connect_api_url').'/send_email/broadcast_admin';
	
	$input = array(
		'subject' => $subject,
		'message' => $message
		);
	
	call_api_post($url, $input);
}

private function broadcast_superadmin($subject, $message){
	
	$url = $this->config->item('connect_api_url').'/send_email/broadcast_superadmin';
	
	$input = array(
		'subject' => $subject,
		'message' => $message
		);
	
	call_api_post($url, $input);
}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */