<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Complaint_handling extends CI_Controller {
    
    function __construct(){
		parent::__construct();
		check_token($this->connect_auth->get_access_token(), $allow_role = array('admin'));
    }
    
    function index(){
		$data['title'] = $this->config->item('title').' : Pengaduan';
		$data['title_page'] = 'Data Pengaduan';
		$data['content'] = 'complaint_handling';
		$this->load->view('admin/main',$data);
    }
	
	public function post_answer($complaint_id=null)
    {
        $data['title'] = $this->config->item('title').' : Pengaduan';
	    $data['content'] = 'complaint_handling';
        $data['title_page'] = 'Data Pengaduan';
		
        $input = $this->input->post();
        
        //$publish = array($this->input->post('publish'));
        
      // var_dump($input);
       //var_dump($input);
        
       
        if (isset($input['answer']))
        {
            $input['publish'] = "1" ;
            $input['answer_time'] = time();
            //this->complaint_handling_db->save($complaint_id, $input);
			$this->pengaduan_db->save($complaint_id, $input);
           // var_dump($input);
			$this->load->view('admin/main',$data);
        }
        else       
        {
           $input['publish'] = "0" ;
           //$this->complaint_handling_db->save($complaint_id, $input);  
           $this->load->view('admin/main',$data);
        }
         
         
    }    
    
    public function test()
    {
        
		echo 'TEST';
		
        // $this->load->view('admin/test');
    }
    
    public function test_proses()
    {
         $input = $this->input->post();
         var_dump($input);
    }
    
     function delete_complaint($complaint_id=null)
        {
           
            $this->pengaduan_db->delete($complaint_id);
            redirect ('admin/complaint_handling');
        }
    
	
}

/* End of file dashboard.php */
/* Location: ./application/controllers/admin/dashboard.php */