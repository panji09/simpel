<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Management_training extends CI_Controller {
    
    function __construct(){
		parent::__construct();
		session_start();
		config_kcfinder(array('user_id' => $this->connect_auth->get_user_id()));
    }
    
    function training($content='home', $training_id = null){
		check_token($this->connect_auth->get_access_token(), $allow_role = array('admin','fasilitator'));
		$data['title'] = $this->config->item('title').' : Dashboard List Pelatihan';
		$data['title_page'] = 'List Pelatihan';
		$data['title'] = $this->config->item('title').' : '.$data['title_page'];
		$data['training_id'] = $training_id;
	
		if($content == 'home'){	    
			$this->session->set_userdata('redirect',current_url());
			$data['content'] = 'approval_submission_training_revise';
		}elseif($content == 'detail'){
			$this->session->set_userdata('redirect',current_url());
			$data['title_content'] = 'Detail Pelatihan';
			$data['content'] = 'detail_training';
		}
		$this->load->view('admin/main',$data);
	
    }
	
	function training_instructor($content='home', $training_id = null){
		check_token($this->connect_auth->get_access_token(), $allow_role = array('admin','fasilitator'));
		$data['title'] = $this->config->item('title').' : Dashboard List Pelatihan';
		$data['title_page'] = 'List Pelatihan';
		$data['title'] = $this->config->item('title').' : '.$data['title_page'];
		$data['training_id'] = $training_id;
	
		if($content == 'home'){	    
			$this->session->set_userdata('redirect',current_url());
			$data['content'] = 'approval_submission_training';
		}elseif($content == 'detail'){
			$this->session->set_userdata('redirect',current_url());
			$data['title_content'] = 'Detail Pengajar';
			$data['content'] = 'detail_training_instructor';
		}
		$this->load->view('admin/main',$data);
	
    }
	
	
    function instructor_ajax($load_page=null, $training_id=null, $user_id=null){
	check_token($this->connect_auth->get_access_token(), $allow_role = array('admin', 'fasilitator'));
	
	$this->load->view('admin/inc/ajax/'.$load_page,array('user_id' => $user_id, 'training_id' => $training_id));
    }
    
    function tab_ajax($load_page=null, $training_id=null){
	check_token($this->connect_auth->get_access_token(), $allow_role = array('fasilitator', 'admin', 'superadmin'));
	if($training_id && $load_page){
	    $this->load->view('admin/inc/ajax/'.$load_page, array('training_id' => $training_id));
	}
    }
}

/* End of file pages.php */
/* Location: ./application/controllers/superadmin/pages.php */ 
