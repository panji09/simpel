<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once APPPATH.'/controllers/notification.php';

class Evaluation extends CI_Controller {
    function __construct(){
	parent::__construct();
	
	
    }
	
	function training($content='home', $training_id = null){
		check_token($this->connect_auth->get_access_token(), $allow_role = array('admin'));
		$data['title'] = $this->config->item('title').' : Dashboard List Evaluasi';
		$data['title_page'] = 'Evaluasi Pelatihan';
		$data['title'] = $this->config->item('title').' : '.$data['title_page'];	
		$data['training_id'] = $training_id;
		$data['form_id'] = "55dace12ae5dbe3f04b7acd9";		

		if($content == 'home'){	    
		    $this->session->set_userdata('redirect',current_url());
		    $data['content'] = 'evaluation';
		}elseif($content == 'rekap'){
		    $data['title_content'] = 'Rekap Hasil Evaluasi';
		    $data['content'] = 'evaluation_add';
		}elseif($content == 'edit'){
		    $data['title_content'] = 'Pengajuan Pelatihan';
		    $data['content'] = 'evaluation_add';
		}elseif($content == 'delete'){
		    //$data['content'] = 'user_admin';
		    if($this->evaluation_db->delete($form_id)){
			$this->session->set_flashdata('notif', array('status' => true, 'msg' => 'Sukses!'));
		    }else{
			$this->session->set_flashdata('notif', array('status' => false, 'msg' => 'Gagal!'));
		    }
    
		    redirect($this->session->userdata('redirect'));
		}
		
		$this->load->view('admin/main',$data);					
	}
	
	function form($content='home', $form_id = null){
	check_token($this->connect_auth->get_access_token(), $allow_role = array('admin'));
	$data['title'] = $this->config->item('title').' : Dashboard List Form Evaluasi';
	$data['title_page'] = 'Form Evaluasi';
	$data['title'] = $this->config->item('title').' : '.$data['title_page'];
	$data['form_id'] = $form_id;
	
	if($content == 'home'){	    
	    $this->session->set_userdata('redirect',current_url());	
	    $data['content'] = 'evaluation_form';
	}elseif($content == 'add'){
	    $data['title_content'] = 'Tambah Form Evaluasi';
	    $data['content'] = 'evaluation_form_add';
	}elseif($content == 'edit'){
	    $data['title_content'] = 'Edit Form Pelatihan';
	    $data['content'] = 'evaluation_form_add';
	}elseif($content == 'delete'){
	    //$data['content'] = 'user_admin';
	    if($this->evaluation_db->delete($form_id)){
		$this->session->set_flashdata('notif', array('status' => true, 'msg' => 'Sukses!'));
	    }else{
		$this->session->set_flashdata('notif', array('status' => false, 'msg' => 'Gagal!'));
	    }
    
	    redirect($this->session->userdata('redirect'));
	}
	$this->load->view('admin/main',$data);
	
    }	
	
	function index_form_post($form_id=null){
		
		check_token($this->connect_auth->get_access_token(), $allow_role = array('admin'));
		//get input
		$input = $this->input->post();
		
		$data_before_update = array();
		if($query = $this->evaluation_db->get($form_id)){
		    $data_before_update = $query[0];
		}
		
		//publis silabus
		$input['published'] = (isset($input['published']) ? intval($input['published']) : 0 );
		
		if(isset($input['type_evaluation'])){
		    $input['type_evaluation'] = array(
			'id' => $input['type_evaluation'],
			'name' => $this->init_config_db->get_type_evaluation($input['type_evaluation'])
		    );
		}
		
		//
		if($this->evaluation_db->save($form_id, $input)){

			/*save log*/
			$data_log = array(
				'table_name' => 'evaluation	',
				'table_id' => $form_id,
				'log_type' => 'evaluation_last_update',
				'ip_address' => $this->input->ip_address(),
				'user_id' => $this->connect_auth->get_me()['user_id'],
				'fullname' => $this->connect_auth->get_me()['fullname'],
				'time' => time(),
				'data_before_update' => $data_before_update
			);

			$this->log_db->save(null, $data_log);

			$this->session->set_flashdata('notif', array('status' => true, 'msg' => 'Sukses!'));
		}else{
			$this->session->set_flashdata('notif', array('status' => false, 'msg' => 'Gagal!'));
		}
		redirect($this->session->userdata('redirect'));
	}
	
}
