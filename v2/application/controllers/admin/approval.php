<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once APPPATH.'/controllers/notification.php';

class Approval extends CI_Controller {
    function __construct(){
	parent::__construct();
	
    }
	
    function institution($content='home', $user_id=null){
	check_token($this->connect_auth->get_access_token(), $allow_role = array('admin'));
	
	$data['title_page'] = 'Approval Lembaga Pelatihan/LPP';
	$data['title'] = $this->config->item('title').' : '.$data['title_page'];
	$data['entity'] = 'lpp';
	
	if($content == 'home'){
	    $this->session->set_userdata('redirect', current_url());
	    $data['content'] = 'approval_institution';
	    
	}elseif($content == 'add'){
	    $data['title_content'] = 'Tambah';
	    $data['content'] = 'user_admin_add';
	}elseif($content == 'edit'){
	    $data['title_content'] = 'Edit';
	    $data['user_id'] = $user_id;
	    $data['content'] = 'user_admin_add';
	}elseif($content == 'delete'){
	
	    $url = $this->config->item('connect_api_url').'/user/index/'.$this->connect_auth->get_access_token().'/'.$user_id;
	    $input = array('deleted' => 1);
	    $response = call_api_put($url, $input);
	    if($response && $response['header_info']['http_code'] == 200){
		$this->session->set_flashdata('notif', array('status' => true, 'msg' => 'Sukses!'));
	    }else{
		$this->session->set_flashdata('notif', array('status' => false, 'msg' => 'Gagal!'));
	    }
	    redirect($this->session->userdata('redirect'));
	}elseif($content == 'status'){
	    $verified_institute = $this->input->get('verified_institute');
	    $url = $this->config->item('connect_api_url').'/user/index/'.$this->connect_auth->get_access_token().'/'.$user_id;
	    $input = array('verified_institute' => intval($verified_institute));
	    
	    //ditolak
	    if($verified_institute == -2){
		$input['activated'] = 0;
	    }
	    
	    $response = call_api_put($url, $input);
	    if($response && $response['header_info']['http_code'] == 200){
		if(isset($input['verified_institute'])){
		    $input['verified_institute'] = intval($input['verified_institute']);
		    $Notification = new Notification();
		    
		    $Notification->send_status_approval_institute($user_id, $input['verified_institute']);
		}
		
		$this->session->set_flashdata('notif', array('status' => true, 'msg' => 'Sukses!'));
	    }else{
		$this->session->set_flashdata('notif', array('status' => false, 'msg' => 'Gagal!'));
	    }
	    redirect($this->session->userdata('redirect'));
	}

	$this->load->view('admin/main',$data);
    }
    
    function institution_ajax($user_id=null, $param=null){
	check_token($this->connect_auth->get_access_token(), $allow_role = array('admin', 'fasilitator'));
	$this->load->view('admin/inc/ajax/'.$param,array('user_id'=>$user_id));
    }
    
    function institution_post($user_id=null){
		check_token($this->connect_auth->get_access_token(), $allow_role = array('admin'));
		
		if($user_id){
			$url = $this->config->item('connect_api_url').'/user/index/'.$this->connect_auth->get_access_token().'/'.$user_id;
			$input = $this->input->post();
			
			$response = call_api_put($url, $input);
			if($response && $response['header_info']['http_code'] == 200){
			
			switch($input['submit']){
				case 'approved' : $input['verified_institute'] = 1; break;
				case 'clarification' : $input['verified_institute'] = -1; break;
				case 'rejected' : $input['verified_institute'] = -2; break;
				
			}
			if(isset($input['verified_institute'])){
				$input['verified_institute'] = intval($input['verified_institute']);
				$Notification = new Notification();
				
				$Notification->send_status_approval_institute($user_id, $input['verified_institute']);
			}
			
			$this->session->set_flashdata('notif', array('status' => true, 'msg' => 'Sukses!'));
			}else{
			$this->session->set_flashdata('notif', array('status' => false, 'msg' => 'Gagal!'));
			}
		}else{
			$this->session->set_flashdata('notif', array('status' => false, 'msg' => 'Gagal!'));
		}
		
		redirect($this->session->userdata('redirect'));
    }
    
    function submission_training($content='home', $training_id=null){
		check_token($this->connect_auth->get_access_token(), $allow_role = array('admin','fasilitator'));
		
		$data['title_page'] = 'Approval Pengajuan Pelatihan';
		$data['title'] = $this->config->item('title').' : '.$data['title_page'];
		
		$data['training_id'] = $training_id;
		if($content == 'home'){
			$this->session->set_userdata('redirect', current_url());
			$data['content'] = 'approval_submission_training';	    
		}elseif($content == 'detail'){
			$data['title_content'] = 'Detail Pelatihan';
			$data['content'] = 'detail_training';
		}elseif($content == 'status'){
			$verified_institute = $this->input->get('verified_institute');
			$url = $this->config->item('connect_api_url').'/user/index/'.$this->connect_auth->get_access_token().'/'.$training_id;
			$input = array('verified_institute' => intval($verified_institute));
			
			//ditolak
			if($verified_institute == -2){
			$input['activated'] = 0;
			}
			
			$response = call_api_put($url, $input);
			if($response && $response['header_info']['http_code'] == 200){
			if(isset($input['verified_institute'])){
				$input['verified_institute'] = intval($input['verified_institute']);
				$Notification = new Notification();
				
				$Notification->send_status_approval_institute($training_id, $input['verified_institute']);
			}
			
			$this->session->set_flashdata('notif', array('status' => true, 'msg' => 'Sukses!'));
			}else{
			$this->session->set_flashdata('notif', array('status' => false, 'msg' => 'Gagal!'));
			}
			redirect($this->session->userdata('redirect'));
		}
		
		$this->load->view('admin/main',$data);
    }

    function training_submission_ajax($load_page=null, $training_id=null){
	check_token($this->connect_auth->get_access_token(), $allow_role = array('admin','fasilitator'));
	
	$this->load->view('admin/inc/ajax/'.$load_page,array('training_id' => $training_id));
    }
    
    function instructor_ajax($load_page=null, $training_id=null, $user_id=null){
		check_token($this->connect_auth->get_access_token(), $allow_role = array('admin'));
		
		$this->load->view('admin/inc/ajax/'.$load_page,array('user_id' => $user_id, 'training_id' => $training_id));
	}
	
	function instructor($content='home', $training_id=null){
		check_token($this->connect_auth->get_access_token(), $allow_role = array('admin'));
		
		$data['title_page'] = 'Approval Pengajuan Pengajar';
		$data['title'] = $this->config->item('title').' : '.$data['title_page'];
		$data['entity'] = 'lpp';
			
		if($content == 'home'){
			$this->session->set_userdata('redirect', current_url());
			$data['content'] = 'approval_instructor';	    
		}elseif($content == 'add'){
			$data['title_content'] = 'Tambah';
			$data['content'] = 'user_admin_add';
		}elseif($content == 'edit'){
			$data['title_content'] = 'Edit';
			$data['training_id'] = $training_id;
			$data['content'] = 'user_admin_add';
		}elseif($content == 'delete'){
			$url = $this->config->item('connect_api_url').'/user/index/'.$this->connect_auth->get_access_token().'/'.$training_id;
			$input = array('deleted' => 1);
			$response = call_api_put($url, $input);
			if($response && $response['header_info']['http_code'] == 200){
			$this->session->set_flashdata('notif', array('status' => true, 'msg' => 'Sukses!'));
			}else{
			$this->session->set_flashdata('notif', array('status' => false, 'msg' => 'Gagal!'));
			}
			redirect($this->session->userdata('redirect'));
		}elseif($content == 'status'){
			$verified_submission_training = $this->input->get('verified_submission_training');
			$url = $this->config->item('connect_api_url').'/user/index/'.$this->connect_auth->get_access_token().'/'.$training_id;
			$input = array('verified_submission_training' => intval($verified_submission_training));
			
			//ditolak
			if($verified_submission_training == -2){
			$input['published'] = 0;
			}
			
			$response = call_api_put($url, $input);
			if($response && $response['header_info']['http_code'] == 200){
			if(isset($input['verified_institute'])){
				$input['verified_institute'] = intval($input['verified_institute']);
				$Notification = new Notification();
				
				$Notification->send_status_approval_institute($training_id, $input['verified_institute']);
			}
			
			$this->session->set_flashdata('notif', array('status' => true, 'msg' => 'Sukses!'));
			}else{
			$this->session->set_flashdata('notif', array('status' => false, 'msg' => 'Gagal!'));
			}
			redirect($this->session->userdata('redirect'));
		}
	
		$this->load->view('admin/main',$data);
    }
    
    
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */