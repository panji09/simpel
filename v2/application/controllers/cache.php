<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cache extends CI_Controller {
    function __construct(){
      parent::__construct();
    }
	
    function visitor_summary(){
        $this->output->cache(60);
        $this->load->view('admin/inc/json/json_visitor_summary');
        
    }
	
	function visitor_hourly(){
		$this->output->cache(60);
		$this->load->view('admin/inc/json/json_visitor_hourly');
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/login.php */