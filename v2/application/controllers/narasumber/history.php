<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class History extends CI_Controller {
    
    function __construct(){
	parent::__construct();
	session_start();
	config_kcfinder(array('user_id' => $this->connect_auth->get_user_id()));
	
	// if($this->connect_auth->get_me()['entity']=='narasumber')
	// check_verified_institute();
    }
    
    
    function teach_history($content='home', $id = null){
	
	check_token($this->connect_auth->get_access_token(), $allow_role = array('narasumber'));
	$data['title'] = $this->config->item('title').' : Dashboard History Pelatihan';
	$data['title_page'] = 'History Pelatihan';
	$data['title'] = $this->config->item('title').' : '.$data['title_page'];
	$data['training_history_id'] = $id;

	if($content == 'home'){
	    $this->session->set_userdata('redirect',current_url());
	    $data['content'] = 'dashboard/instructor_training_history';
	}elseif($content == 'add'){
	    $data['title_content'] = 'Pengajuan Pelatihan';
	    $data['content'] = 'dashboard/instructor_training_history_add';
	}
	
	$this->load->view('home/main',$data);
	
    }
    
    function training_history($content='home', $id = null){
	
	check_token($this->connect_auth->get_access_token(), $allow_role = array('narasumber'));
	$data['title'] = $this->config->item('title').' : Dashboard History Pelatihan';
	$data['title_page'] = 'History Pelatihan';
	$data['title'] = $this->config->item('title').' : '.$data['title_page'];
	$data['training_history_id'] = $id;

	if($content == 'home'){
	    $this->session->set_userdata('redirect',current_url());
	    $data['content'] = 'dashboard/instructor_training_history';
	}elseif($content == 'add'){
	    $data['title_content'] = 'Pengajuan Pelatihan';
	    $data['content'] = 'dashboard/instructor_training_history_add';
	}
	
	$this->load->view('home/main',$data);
	
    }
	
    function training_history_post($training_history_id=null){
	/*
	    semua post pengajuan pelatihan dsini
	*/
	check_token($this->connect_auth->get_access_token(), $allow_role = array('admin','narasumber'));
	$input = $this->input->post();	
	
	$data_before_update = array();
	if($query = $this->training_history_db->get($training_history_id)){
	    $data_before_update = $query[0];
	}
	
	if(!$training_history_id){
	    //save di new aja, edit ga bisa
	    $input['creator'] = $this->connect_auth->get_me();   
	}
	
	$input['author'] = $this->connect_auth->get_me();
	
	
	//nama pelatihan
	if(isset($input['published'])){
	    $input['published'] = intval($input['published']);
	}
		
	//lokasi provinsi
	$province_id = $input['training_location_province'];
	
	if(isset($input['training_location_province'])){
	    
	    $query = file_get_contents($this->config->item('connect_api_url').'/location/province');
	    $province = array();
	    $province_name = '';
	    if($query){
		$province = json_decode($query,true);
	    }
	    
	    if($province){
		foreach($province as $row){
		    if($row['id'] == $province_id){
			$province_name = $row['name'];
		    }
		}
	    }
	    
	    $input['training_location_province'] = array('id' => $province_id, 'name' => $province_name );
	}
	
	//lokasi kabkota
	$district_id = $input['training_location_district'];
	if(isset($input['training_location_district'])){
	    
	    $query = file_get_contents($this->config->item('connect_api_url').'/location/district/'.$province_id);
	    $district = array();
	    $district_name = '';
	    if($query){
		$district = json_decode($query,true);
	    }
	    
	    if($district){
		foreach($district as $row){
		    if($row['id'] == $district_id){
			$district_name = $row['name'];
		    }
		}
	    }
	    
	    $input['training_location_district'] = array('id' => $district_id, 'name' => $district_name );
	}
	
	

	if($this->training_history_db->save($training_history_id, $input)){
    
	    /*save log*/
	    $data_log = array(
		'table_name' => 'training',
		'table_id' => $training_history_id,
		'log_type' => 'training_last_update',
		'ip_address' => $this->input->ip_address(),
		'user_id' => $this->connect_auth->get_me()['user_id'],
		'fullname' => $this->connect_auth->get_me()['fullname'],
		'time' => time(),
		'data_before_update' => $data_before_update
	    );
    
	    $this->log_db->save(null, $data_log);
    
	    $this->session->set_flashdata('notif', array('status' => true, 'msg' => 'Sukses!'));
	}else{
	    $this->session->set_flashdata('notif', array('status' => false, 'msg' => 'Gagal!'));
	}	
	redirect($this->session->userdata('redirect'));
    }
		
    function certification_history($content='home', $id = null){
	
	check_token($this->connect_auth->get_access_token(), $allow_role = array('narasumber'));
	$data['title'] = $this->config->item('title').' : Dashboard History Sertifikasi';
	$data['title_page'] = 'History Sertifikasi';
	$data['title'] = $this->config->item('title').' : '.$data['title_page'];
	$data['training_history_id'] = $id;

	if($content == 'home'){
	    $this->session->set_userdata('redirect',current_url());
	    $data['content'] = 'dashboard/instructor_certification_history';
	}
	
	$this->load->view('home/main',$data);
	
    }
	
    function procurement_history($content='home', $id = null){
	
	check_token($this->connect_auth->get_access_token(), $allow_role = array('narasumber'));
	$data['title'] = $this->config->item('title').' : Dashboard History Kegiatan Pengadaan';
	$data['title_page'] = 'History Pengadaan';
	$data['title'] = $this->config->item('title').' : '.$data['title_page'];
	$data['training_history_id'] = $id;

	if($content == 'home'){
	    $this->session->set_userdata('redirect',current_url());
	    $data['content'] = 'dashboard/instructor_procurement_history';
	}
	
	$this->load->view('home/main',$data);
	
    }
	
    function training_ajax($load_page, $training_history_id){
	$this->load->view('admin/inc/ajax/'.$load_page,array('training_history_id' => $training_history_id));
    }
}

/* End of file pages.php */
/* Location: ./application/controllers/superadmin/pages.php */