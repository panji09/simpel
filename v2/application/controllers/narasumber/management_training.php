<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once APPPATH.'/controllers/notification.php';

class Management_training extends CI_Controller {
    
    function __construct(){
		parent::__construct();
		session_start();
		config_kcfinder(array('user_id' => $this->connect_auth->get_user_id()));	
		// if($this->connect_auth->get_me()['entity']=='narasumber')
		// check_verified_institute();
    }
    
    
    function training($content='home', $training_id = null){
		check_token($this->connect_auth->get_access_token(), $allow_role = array('narasumber'));
		$data['title'] = $this->config->item('title').' : Dashboard List Pelatihan';
		$data['title_page'] = 'List Pelatihan';
		$data['title'] = $this->config->item('title').' : '.$data['title_page'];
		$data['training_id'] = $training_id;

		if($content == 'home'){
		    $this->session->set_userdata('redirect',current_url());
		    $data['content'] = 'dashboard/instructor_training';
		}elseif($content == 'add'){
		    $data['title_content'] = 'Pengajuan Pelatihan';
		    $data['content'] = 'dashboard/lpp_training_submission';
		}elseif($content == 'edit'){
		    $data['title_content'] = 'Pengajuan Pelatihan';
		    $data['content'] = 'dashboard/lpp_training_submission';
		}elseif($content == 'reschedule'){
		    $data['title_content'] = 'Pengajuan Pelatihan';
		    $data['content'] = 'dashboard/lpp_training_submission_reschedule';
		}elseif($content == 'delete'){
		    //$data['content'] = 'user_admin';
		    if($this->training_db->delete($training_id)){
			$this->session->set_flashdata('notif', array('status' => true, 'msg' => 'Sukses!'));
		    }else{
			$this->session->set_flashdata('notif', array('status' => false, 'msg' => 'Gagal!'));
		    }
    
		    redirect($this->session->userdata('redirect'));
		}
		$this->load->view('home/main',$data);	
    }
    
	function confirm_post($training_id = null){
	    check_token($this->connect_auth->get_access_token(), $allow_role = array('narasumber','admin'));
	    $input = $this->input->post();
	    $send_notif = array();
	    
	    $data_input['training_id'] = $training_id;
	    
	    if($this->connect_auth->get_me()['entity'] == 'narasumber' ){
	    
		$data_input['instructor_id'] = $this->connect_auth->get_me()['user_id'];
		
		if($input['submit']=='approved'){
		    $data_input['instructor_approved'] = 1;
		    
		}elseif($input['submit']=='reject'){
		    $data_input['instructor_approved'] = -2;
		    $data_input['review_reject_note'] = $input['review_reject_note'];
		}
		$send_notif['status_training_instructor'] = $data_input['instructor_approved'];
		
		$data_input['instructor_approved_log'] = $this->connect_auth->get_me();
		
	    }elseif($this->connect_auth->get_me()['entity'] == 'admin'){
// 		print_r($input); exit();
		$data_input['instructor_id'] = $input['instructor_id'];
		
		if($input['submit']=='approved'){
		    $data_input['admin_approved'] = 1;
		    $send_notif['review_training_instructor'] = true;
		}elseif($input['submit']=='reject'){
		    $data_input['admin_approved'] = -2;
		}
		
		$data_input['admin_approved_log'] = $this->connect_auth->get_basic_me();
	    }
	    
	    
	    $hour_leasson_id = $this->hour_leasson_db->exist_training_instructor($data_input['training_id'], $data_input['instructor_id']);
	    
	    
	    if($this->hour_leasson_db->save($hour_leasson_id, $data_input)){
		    
		    if(isset($data_input['submission_date']))
		    foreach($data_input['submission_date'] as $row){

			    $this->agenda_instructor_db->save_instructor_agenda($row, $data_input['instructor_id'], $training_id);
		    }
		    
		    $this->session->set_userdata('notif', array('status' => true, 'msg' => 'Sukses!'));
// 		    print_r( $this->session->userdata('notif') );
		    
		    $Notification = new Notification();
		    if(isset($send_notif['review_training_instructor']) && $send_notif['review_training_instructor']){
			$Notification->review_training_instructor($instructor_id = $data_input['instructor_id'], $training_id);
		    }
		    
		    if(isset($send_notif['status_training_instructor'])){
			
			$Notification->status_training_instructor($instructor_id = $data_input['instructor_id'], $training_id, $status_approval = $send_notif['status_training_instructor'], $data_input);
		    }
	    }else{
		$this->session->set_userdata('notif', array('status' => false, 'msg' => 'Gagal!'));
	    }
	    
	    redirect($this->session->userdata('redirect'));
	}
	
	
	
    function training_ajax($load_page, $training_id){
		$this->load->view('admin/inc/ajax/'.$load_page,array('training_id' => $training_id));
    }
}

/* End of file management_training.php */
/* Location: ./application/controllers/narasumber/management_training.php */