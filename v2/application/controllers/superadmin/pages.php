<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once APPPATH.'/controllers/notification.php';

class Pages extends CI_Controller {
    
    function __construct(){
		parent::__construct();
		
		session_start();
		config_kcfinder(array('user_id' => $this->connect_auth->get_user_id()));
    }
    
    function single_content_post($page_id=null){
		check_token($this->connect_auth->get_access_token(), $allow_role = array('superadmin'));
		
		$input = $this->input->post();
	
		$input['page_id'] = $page_id;
		$input['published'] = (isset($input['published']) ? intval($input['published']) : 0);
		
		$input['author'] = array(
		    'fullname' => $this->connect_auth->get_me()['fullname'],
		    'user_id' => $this->connect_auth->get_me()['user_id'],
		);
	
		$data_before_update = array();
		if($query = $this->static_pages_db->get($page_id)){
		    $data_before_update = $query[0];
		}
	
		if($this->static_pages_db->save($page_id, $input)){
	    
		    /*save log*/
		    $data_log = array(
			'table_name' => 'pages',
			'table_id' => $page_id,
			'log_type' => $input['page_id'].'_last_update',
			'ip_address' => $this->input->ip_address(),
			'user_id' => $this->connect_auth->get_me()['user_id'],
			'fullname' => $this->connect_auth->get_me()['fullname'],
			'time' => time(),
			'data_before_update' => $data_before_update
		    );
	    
		    $this->log_db->save(null, $data_log);
	    
		    $this->session->set_flashdata('notif', array('status' => true, 'msg' => 'Sukses!'));
		}else{
		    $this->session->set_flashdata('notif', array('status' => false, 'msg' => 'Gagal!'));
		}
	    
		redirect($this->session->userdata('redirect'));
    }
    
    function info_slider($content = 'home', $content_id=null){
		check_token($this->connect_auth->get_access_token(), $allow_role = array('superadmin'));
		$this->session->set_userdata('redirect',current_url());
		$data['title_page'] = 'Info Slider';
		$data['title'] = $this->config->item('title').' : '.$data['title_page'];
	
		$data['title_content'] = 'Edit';
		$data['page_id'] = 'info_slider';
		$data['content'] = 'info_slider';
	
		$this->load->view('admin/main',$data);
    }
    
    function how_to($content = 'home', $content_id=null){
		check_token($this->connect_auth->get_access_token(), $allow_role = array('superadmin'));
		$this->session->set_userdata('redirect',current_url());
		$data['title_page'] = 'Cara Penggunaan';
		$data['title'] = $this->config->item('title').' : '.$data['title_page'];
	
		$data['title_content'] = 'Edit';
		$data['page_id'] = 'how_to';
		$data['content'] = 'single_content';      
	
		$this->load->view('admin/main',$data);
    }
    
    function how_to_submission_accreditation($content = 'home', $content_id=null){
		check_token($this->connect_auth->get_access_token(), $allow_role = array('superadmin'));
		$this->session->set_userdata('redirect',current_url());
		$data['title_page'] = 'Pengajuan Akreditasi LPP';
		$data['title'] = $this->config->item('title').' : '.$data['title_page'];
	
		$data['title_content'] = 'Edit';
		$data['page_id'] = 'how_to_submission_accreditation';
		$data['content'] = 'single_content_how_to';      
	
		$this->load->view('admin/main',$data);
    }
    
    function how_to_submission_training_facilitator($content = 'home', $content_id=null){
		check_token($this->connect_auth->get_access_token(), $allow_role = array('superadmin'));
		$this->session->set_userdata('redirect',current_url());
		$data['title_page'] = 'Pengajuan Fasilitasi Pelatihan dan Narasumber';
		$data['title'] = $this->config->item('title').' : '.$data['title_page'];
	
		$data['title_content'] = 'Edit';
		$data['page_id'] = 'how_to_submission_training_facilitator';
		$data['content'] = 'single_content_how_to';      
	
		$this->load->view('admin/main',$data);
    }
    
    function how_participant_registration($content = 'home', $content_id=null){
		check_token($this->connect_auth->get_access_token(), $allow_role = array('superadmin'));
		$this->session->set_userdata('redirect',current_url());
		$data['title_page'] = 'Pendaftaran Peserta Pelatihan';
		$data['title'] = $this->config->item('title').' : '.$data['title_page'];
	
		$data['title_content'] = 'Edit';
		$data['page_id'] = 'how_participant_registration';
		$data['content'] = 'single_content_how_to';      
	
		$this->load->view('admin/main',$data);
    }
    function faq($content = 'home', $content_id=null){
		check_token($this->connect_auth->get_access_token(), $allow_role = array('superadmin'));
		$this->session->set_userdata('redirect',current_url());
		$data['title_page'] = 'FAQ';
		$data['title'] = $this->config->item('title').' : '.$data['title_page'];
	
		$data['title_content'] = 'Edit';
		$data['page_id'] = 'faq';
		$data['content'] = 'faq';      
	
		$this->load->view('admin/main',$data);
    }
    
    function terms_of_service($content = 'home', $content_id=null){
		check_token($this->connect_auth->get_access_token(), $allow_role = array('superadmin'));
		$this->session->set_userdata('redirect',current_url());
		$data['title_page'] = 'Persyaratan Layanan';
		$data['title'] = $this->config->item('title').' : '.$data['title_page'];
	
		$data['title_content'] = 'Edit';
		$data['page_id'] = 'terms_of_service';
		$data['content'] = 'single_content';
      	
		$this->load->view('admin/main',$data);
    }
    
    function contact($content = 'home', $content_id=null){
		check_token($this->connect_auth->get_access_token(), $allow_role = array('superadmin'));
		$this->session->set_userdata('redirect',current_url());
		$data['title_page'] = 'Kontak';
		$data['title'] = $this->config->item('title').' : '.$data['title_page'];
	
		$data['title_content'] = 'Edit';
		$data['page_id'] = 'contact';
		$data['content'] = 'single_content';
      	
		$this->load->view('admin/main',$data);
    }
    
    function about($content = 'home', $content_id=null){
		check_token($this->connect_auth->get_access_token(), $allow_role = array('superadmin'));
		$this->session->set_userdata('redirect',current_url());
		$data['title_page'] = 'Tentang Kami';
		$data['title'] = $this->config->item('title').' : '.$data['title_page'];
	
		$data['title_content'] = 'Edit';
		$data['page_id'] = 'about';
		$data['content'] = 'single_content';
      	
		$this->load->view('admin/main',$data);
    }
    
    function regulatory_training($content = 'home', $content_id=null){
		check_token($this->connect_auth->get_access_token(), $allow_role = array('superadmin'));
		$data['title_page'] = 'Peraturan Pelatihan';
		$data['title'] = $this->config->item('title').' : '.$data['title_page'];
		$data['content_id'] = $content_id;
	
		if($content == 'home'){
		    $this->session->set_userdata('redirect',current_url());
		    $data['content'] = 'regulatory_training';
		}elseif($content == 'add'){
		    $data['title_content'] = 'Tambah';
		    $data['content'] = 'regulatory_training_add';
	
		}elseif($content == 'edit'){
		    $data['title_content'] = 'Edit';
	    
		    $data['content'] = 'regulatory_training_add';
	
		}elseif($content == 'delete'){
		    //$data['content'] = 'user_admin';
		    if($this->dynamic_pages_db->delete($content_id)){
			$this->session->set_flashdata('notif', array('status' => true, 'msg' => 'Sukses!'));
		    }else{
			$this->session->set_flashdata('notif', array('status' => false, 'msg' => 'Gagal!'));
		    }
	    
		    redirect($this->session->userdata('redirect'));
		}
	
		$this->load->view('admin/main',$data);
    }
    
    function regulatory_training_post($content_id=null){
		check_token($this->connect_auth->get_access_token(), $allow_role = array('superadmin'));
		$input = $this->input->post();
	
		$input['page_id'] = 'regulatory_training';
		$input['published'] = (isset($input['published']) ? intval($input['published']) : 0);
		
		$input['author'] = array(
		    'fullname' => $this->connect_auth->get_me()['fullname'],
		    'user_id' => $this->connect_auth->get_me()['user_id'],
		);
		$data_before_update = array();
		if($query = $this->dynamic_pages_db->get($content_id)){
	    	$data_before_update = $query[0];
		}
	
		if($this->dynamic_pages_db->save($content_id, $input)){
	    
		    /*save log*/
		    $data_log = array(
			'table_name' => 'pages',
			'table_id' => $content_id,
			'log_type' => $input['page_id'].'_last_update',
			'ip_address' => $this->input->ip_address(),
			'user_id' => $this->connect_auth->get_me()['user_id'],
			'fullname' => $this->connect_auth->get_me()['fullname'],
			'time' => time(),
			'data_before_update' => $data_before_update
		    );
	    
		    $this->log_db->save(null, $data_log);
	    
		    $this->session->set_flashdata('notif', array('status' => true, 'msg' => 'Sukses!'));
		}else{
		    $this->session->set_flashdata('notif', array('status' => false, 'msg' => 'Gagal!'));
		}
	
		redirect($this->session->userdata('redirect'));
    }
	
    function gallery($content = 'home', $content_id=null){
		check_token($this->connect_auth->get_access_token(), $allow_role = array('superadmin'));
		$data['title_page'] = 'Galeri Foto';
		$data['title'] = $this->config->item('title').' : '.$data['title_page'];
		$data['content_id'] = $content_id;
	
		if($content == 'home'){
		    $this->session->set_userdata('redirect',current_url());
		    $data['content'] = 'gallery';
		}elseif($content == 'add'){
		    $data['title_content'] = 'Tambah';
		    $data['content'] = 'gallery_add';
	
		}elseif($content == 'edit'){
		    $data['title_content'] = 'Edit';
	    
		    $data['content'] = 'gallery_add';
	
		}elseif($content == 'delete'){
		    //$data['content'] = 'user_admin';
		    if($this->dynamic_pages_db->delete($content_id)){
			$this->session->set_flashdata('notif', array('status' => true, 'msg' => 'Sukses!'));
		    }else{
			$this->session->set_flashdata('notif', array('status' => false, 'msg' => 'Gagal!'));
		    }
	    
		    redirect($this->session->userdata('redirect'));
		}elseif($content == 'published'){
		    //$data['content'] = 'user_admin';
		    $published = $this->input->get('published');
		    if($this->dynamic_pages_db->save($content_id, array('published' => intval($published)))){
			
			//cek notif ke lpp
			if($content_id){
			    if($query = $this->dynamic_pages_db->get($content_id)){
				$data_gallery = $query[0];
				
				if($data_gallery['creator']['entity'] == 'lpp' && intval($published)){
				    $Notification = new Notification();
				    $Notification->status_review_gallery($data_gallery);
				}
			    }
			}
			
			$this->session->set_flashdata('notif', array('status' => true, 'msg' => 'Sukses!'));
		    }else{
			$this->session->set_flashdata('notif', array('status' => false, 'msg' => 'Gagal!'));
		    }
	    
		    redirect($this->session->userdata('redirect'));
		}

		$this->load->view('admin/main',$data);	
    }
    
    function gallery_post($content_id=null){
		check_token($this->connect_auth->get_access_token(), $allow_role = array('superadmin','lpp'));
		$input = $this->input->post();
		$send_notif = array();
		$input['page_id'] = 'gallery';
		
		if($this->connect_auth->get_me()['entity'] == 'superadmin'){
		    $input['published'] = (isset($input['published']) ? intval($input['published']) : 0);
		}
		    
		if(!$content_id){
		    //save di new aja, edit ga bisa
		    $input['author'] = array(
			'fullname' => $this->connect_auth->get_me()['fullname'],
			'user_id' => $this->connect_auth->get_me()['user_id'],
		    );
		    
		    if($this->connect_auth->get_me()['entity'] == 'lpp'){
			$input['published'] = 0;
			$send_notif['lpp_review_gallery'] = true;
		    }
		}
		
		//cek notif ke lpp
		if($content_id){
		    if($query = $this->dynamic_pages_db->get($content_id)){
			$data_gallery = $query[0];
			
			if($data_gallery['creator']['entity'] == 'lpp' && $input['published']){
			    $send_notif['status_review_gallery'] = true;
			}
		    }
		}
		
		$data_before_update = array();
		if($query = $this->dynamic_pages_db->get($content_id)){
		    $data_before_update = $query[0];
		}
	
		if($this->dynamic_pages_db->save($content_id, $input)){
	    
		    /*save log*/
		    $data_log = array(
			'table_name' => 'pages',
			'table_id' => $content_id,
			'log_type' => $input['page_id'].'_last_update',
			'ip_address' => $this->input->ip_address(),
			'user_id' => $this->connect_auth->get_me()['user_id'],
			'fullname' => $this->connect_auth->get_me()['fullname'],
			'time' => time(),
			'data_before_update' => $data_before_update
		    );
	    
		    $this->log_db->save(null, $data_log);
		    
		    $Notification =  new Notification();
		    if(isset($send_notif['lpp_review_gallery']) && $send_notif['lpp_review_gallery']){
			$Notification->lpp_review_gallery($user_lpp = $this->connect_auth->get_me());
		    }
		    
		    if(isset($send_notif['status_review_gallery']) && $send_notif['status_review_gallery']){
			$Notification->status_review_gallery($data_gallery);
		    }
		    $this->session->set_flashdata('notif', array('status' => true, 'msg' => 'Sukses!'));
		}else{
		    $this->session->set_flashdata('notif', array('status' => false, 'msg' => 'Gagal!'));
		}
	
		redirect($this->session->userdata('redirect'));
    }
	
    function news($content = 'home', $content_id=null){
		check_token($this->connect_auth->get_access_token(), $allow_role = array('superadmin'));
		$data['title_page'] = 'Berita';
		$data['title'] = $this->config->item('title').' : '.$data['title_page'];
		$data['content_id'] = $content_id;
	
		if($content == 'home'){
		    $this->session->set_userdata('redirect',current_url());
		    $data['content'] = 'news';
		}elseif($content == 'add'){
		    $data['title_content'] = 'Tambah';
		    $data['content'] = 'news_add';
	
		}elseif($content == 'edit'){
		    $data['title_content'] = 'Edit';
	    
		    $data['content'] = 'news_add';
	
		}elseif($content == 'delete'){
		    //$data['content'] = 'user_admin';
		    if($this->dynamic_pages_db->delete($content_id)){
			$this->session->set_flashdata('notif', array('status' => true, 'msg' => 'Sukses!'));
		    }else{
			$this->session->set_flashdata('notif', array('status' => false, 'msg' => 'Gagal!'));
		    }
	    
		    redirect($this->session->userdata('redirect'));
		}
		elseif($content == 'published'){
		    //$data['content'] = 'user_admin';
		    $published = $this->input->get('published');
		    if($this->dynamic_pages_db->save($content_id, array('published' => intval($published)))){
			//cek notif ke lpp
			if($content_id){
			    if($query = $this->dynamic_pages_db->get($content_id)){
				$data_news = $query[0];
				
				if($data_news['creator']['entity'] == 'lpp' && intval($published)){
				    $Notification = new Notification();
				    $Notification->status_review_news($data_news);
				}
			    }
			}
			
			$this->session->set_flashdata('notif', array('status' => true, 'msg' => 'Sukses!'));
		    }else{
			$this->session->set_flashdata('notif', array('status' => false, 'msg' => 'Gagal!'));
		    }
	    
		    redirect($this->session->userdata('redirect'));
		}
		$this->load->view('admin/main',$data);
    }
    
    function news_post($content_id=null){
		check_token($this->connect_auth->get_access_token(), $allow_role = array('superadmin','lpp'));
		$input = $this->input->post();	
		$input['page_id'] = 'news';
		$send_notif = array();
		
		if($this->connect_auth->get_me()['entity'] == 'superadmin'){
		    $input['published'] = (isset($input['published']) ? intval($input['published']) : 0);
		    
		    //$timestamp = strtotime($mysqltime);
		    //$unix_time = date("Y-m-d H:i", $timestamp);
		    $input['created'] = (isset($input['created']) ? strtotime($input['created']) : time());
		    
		    //cek notif ke lpp
		    if($content_id){
			if($query = $this->dynamic_pages_db->get($content_id)){
			    $data_news = $query[0];
			    
			    if($data_news['creator']['entity'] == 'lpp' && $input['published']){
				$send_notif['status_review_news'] = true;
			    }
			}
		    }
		    
		}
		    
		if(!$content_id){
		    //save di new aja, edit ga bisa
		    $input['author'] = array(
			'fullname' => $this->connect_auth->get_me()['fullname'],
			'user_id' => $this->connect_auth->get_me()['user_id'],
		    );
		    
		    if($this->connect_auth->get_me()['entity'] == 'lpp'){
			$input['published'] = 0;
			$send_notif['lpp_review_news'] = true;
		    }
		}
		
		$data_before_update = array();
		if($query = $this->dynamic_pages_db->get($content_id)){
		    $data_before_update = $query[0];
		}
	
		if($this->dynamic_pages_db->save($content_id, $input)){
	    
		    /*save log*/
		    $data_log = array(
			'table_name' => 'pages',
			'table_id' => $content_id,
			'log_type' => $input['page_id'].'_last_update',
			'ip_address' => $this->input->ip_address(),
			'user_id' => $this->connect_auth->get_me()['user_id'],
			'fullname' => $this->connect_auth->get_me()['fullname'],
			'time' => time(),
			'data_before_update' => $data_before_update
		    );
	    
		    $this->log_db->save(null, $data_log);
		    
		    $Notification =  new Notification();
		    if(isset($send_notif['lpp_review_news']) && $send_notif['lpp_review_news']){
			$Notification->lpp_review_news($user_lpp = $this->connect_auth->get_me());
		    }
		    
		    if(isset($send_notif['status_review_news']) && $send_notif['status_review_news']){
			$Notification->status_review_news($data_news);
		    }
		    
		    $this->session->set_flashdata('notif', array('status' => true, 'msg' => 'Sukses!'));
		}else{
		    $this->session->set_flashdata('notif', array('status' => false, 'msg' => 'Gagal!'));
		}	
		redirect($this->session->userdata('redirect'));
    }
    
    function event($content = 'home', $content_id=null){	
		check_token($this->connect_auth->get_access_token(), $allow_role = array('superadmin'));
		$data['title_page'] = 'Agenda';
		$data['title'] = $this->config->item('title').' : '.$data['title_page'];
		$data['content_id'] = $content_id;
	
		if($content == 'home'){
		    $this->session->set_userdata('redirect',current_url());
		    $data['content'] = 'event';
		}elseif($content == 'add'){
		    $data['title_content'] = 'Tambah';
		    $data['content'] = 'event_add';
	
		}elseif($content == 'edit'){
		    $data['title_content'] = 'Edit';
	    
		    $data['content'] = 'event_add';
	
		}elseif($content == 'delete'){
		    //$data['content'] = 'user_admin';
		    if($this->dynamic_pages_db->delete($content_id)){
			$this->session->set_flashdata('notif', array('status' => true, 'msg' => 'Sukses!'));
		    }else{
			$this->session->set_flashdata('notif', array('status' => false, 'msg' => 'Gagal!'));
		    }
	    
		    redirect($this->session->userdata('redirect'));
		}
	
		$this->load->view('admin/main',$data);
    }
    
    function event_post($content_id=null){
		check_token($this->connect_auth->get_access_token(), $allow_role = array('superadmin'));
		$input = $this->input->post();
	
		$input['page_id'] = 'event';
		
		
		$input['published'] = (isset($input['published']) ? intval($input['published']) : 0);
		
		$input['author'] = array(
		    'fullname' => $this->connect_auth->get_me()['fullname'],
		    'user_id' => $this->connect_auth->get_me()['user_id'],
		);
	
		$data_before_update = array();
		if($query = $this->dynamic_pages_db->get($content_id)){
		    $data_before_update = $query[0];
		}
	
		if($this->dynamic_pages_db->save($content_id, $input)){
	    
		    /*save log*/
		    $data_log = array(
			'table_name' => 'pages',
			'table_id' => $content_id,
			'log_type' => $input['page_id'].'_last_update',
			'ip_address' => $this->input->ip_address(),
			'user_id' => $this->connect_auth->get_me()['user_id'],
			'fullname' => $this->connect_auth->get_me()['fullname'],
			'time' => time(),
			'data_before_update' => $data_before_update
		    );
	    
		    $this->log_db->save(null, $data_log);
	    
		    $this->session->set_flashdata('notif', array('status' => true, 'msg' => 'Sukses!'));
		}else{
		    $this->session->set_flashdata('notif', array('status' => false, 'msg' => 'Gagal!'));
		}
	    
		redirect($this->session->userdata('redirect'));
    }
    
    function index(){
		redirect('superadmin/pages/news');
    }
}

/* End of file pages.php */
/* Location: ./application/controllers/superadmin/pages.php */