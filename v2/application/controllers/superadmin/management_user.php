<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once APPPATH.'/controllers/notification.php';

class Management_user extends CI_Controller {
    
    function __construct(){
		parent::__construct();

		session_start();
		config_kcfinder(array('user_id' => $this->connect_auth->get_user_id()));
		check_token($this->connect_auth->get_access_token(), $allow_role = array('superadmin','admin'));
	}
    
    function invite($content='post', $entity=null){
		
		if($content == 'post' && $entity){
			$input = $this->input->post();
			$Notif = new Notification();
			switch($entity){
				case 'lpp': 
				case 'peserta': 
				case 'narasumber': 
				$Notif->send_invite($entity, $input['email']); 
				$this->session->set_flashdata('notif', array('status' => true, 'msg' => 'Sukses!')); break;
				default: $this->session->set_flashdata('notif', array('status' => false, 'msg' => 'Gagal!'));
				
			}	
		}
		
		if($content == 'delete'){
			$invite_code = $this->input->get('invite_code');
			$this->session->set_flashdata('notif', array('status' => false, 'msg' => 'Gagal!'));
			if($invite_code){
				$query = $this->invite_code_db->get_invite_code($invite_code);
				if($query && count($query) > 0){
					if($this->invite_code_db->delete($query[0]['_id']->{'$id'})){
						$this->session->set_flashdata('notif', array('status' => true, 'msg' => 'Sukses!'));
					}
				}
			}
		}
		redirect($this->session->userdata('redirect'));
    }
	
    function superadmin($content = 'home', $user_id=null){
	
		$data['title_page'] = 'User Superadmin';
		$data['title'] = $this->config->item('title').' : '.$data['title_page'];
		$data['entity'] = 'superadmin';
		
		if($content == 'home'){
		    $this->session->set_userdata('redirect', current_url());
		    $data['content'] = 'user_admin';
		    
		}elseif($content == 'add'){
		    $data['title_content'] = 'Tambah';
		    $data['content'] = 'user_admin_add';
		}elseif($content == 'edit'){
		    $data['title_content'] = 'Edit';
		    $data['user_id'] = $user_id;
		    $data['content'] = 'user_admin_add';
		}elseif($content == 'delete'){
		
		    $url = $this->config->item('connect_api_url').'/user/index/'.$this->connect_auth->get_access_token().'/'.$user_id;
		    $input = array('deleted' => 1);
		    $response = call_api_put($url, $input);
		    if($response && $response['header_info']['http_code'] == 200){
			$this->session->set_flashdata('notif', array('status' => true, 'msg' => 'Sukses!'));
		    }else{
			$this->session->set_flashdata('notif', array('status' => false, 'msg' => 'Gagal!'));
		    }
		    redirect($this->session->userdata('redirect'));
		}elseif($content == 'status'){
		    $activated = $this->input->get('activated');
		    $url = $this->config->item('connect_api_url').'/user/index/'.$this->connect_auth->get_access_token().'/'.$user_id;
		    $input = array(
						   'activated' => intval($activated),
						   'verified_email' => 1
						   );
		    $response = call_api_put($url, $input);
		    if($response && $response['header_info']['http_code'] == 200){
			$this->session->set_flashdata('notif', array('status' => true, 'msg' => 'Sukses!'));
		    }else{
			$this->session->set_flashdata('notif', array('status' => false, 'msg' => 'Gagal!'));
		    }
		    redirect($this->session->userdata('redirect'));
		}
	
		$this->load->view('admin/main',$data);
    }
    
    function admin($content = 'home', $user_id=null){
	
		$data['title_page'] = 'User Admin';
		$data['title'] = $this->config->item('title').' : '.$data['title_page'];
		$data['entity'] = 'admin';
		
		if($content == 'home'){
		    $this->session->set_userdata('redirect', current_url());
		    $data['content'] = 'user_admin';
		    
		}elseif($content == 'add'){
		    $data['title_content'] = 'Tambah';
		    $data['content'] = 'user_admin_add';
		}elseif($content == 'edit'){
		    $data['title_content'] = 'Edit';
		    $data['user_id'] = $user_id;
// 		    config_kcfinder(array('user_id' => $user_id));
		    $data['content'] = 'user_admin_add';
		}elseif($content == 'delete'){
		
		    $url = $this->config->item('connect_api_url').'/user/index/'.$this->connect_auth->get_access_token().'/'.$user_id;
		    $input = array('deleted' => 1);
		    $response = call_api_put($url, $input);
		    if($response && $response['header_info']['http_code'] == 200){
			$this->session->set_flashdata('notif', array('status' => true, 'msg' => 'Sukses!'));
		    }else{
			$this->session->set_flashdata('notif', array('status' => false, 'msg' => 'Gagal!'));
		    }
		    redirect($this->session->userdata('redirect'));
		}elseif($content == 'status'){
		    $activated = $this->input->get('activated');
		    $url = $this->config->item('connect_api_url').'/user/index/'.$this->connect_auth->get_access_token().'/'.$user_id;
		    $input = array(
						   'activated' => intval($activated),
						   'verified_email' => 1
						   );
		    $response = call_api_put($url, $input);
		    if($response && $response['header_info']['http_code'] == 200){
			$this->session->set_flashdata('notif', array('status' => true, 'msg' => 'Sukses!'));
		    }else{
			$this->session->set_flashdata('notif', array('status' => false, 'msg' => 'Gagal!'));
		    }
		    redirect($this->session->userdata('redirect'));
		}
	
		$this->load->view('admin/main',$data);
    }
    
    function facilitator($content = 'home', $user_id=null){
	
		$data['title_page'] = 'User Fasilitator';
		$data['title'] = $this->config->item('title').' : '.$data['title_page'];
		$data['entity'] = 'fasilitator';
		
		if($content == 'home'){
		    $this->session->set_userdata('redirect', current_url());
		    $data['content'] = 'user_admin';
		    
		}elseif($content == 'add'){
		    $data['title_content'] = 'Tambah';
		    $data['content'] = 'user_admin_add';
		}elseif($content == 'edit'){
		    $data['title_content'] = 'Edit';
		    $data['user_id'] = $user_id;
// 		    config_kcfinder(array('user_id' => $user_id));
		    $data['content'] = 'user_admin_add';
		}elseif($content == 'delete'){
		    
		    $url = $this->config->item('connect_api_url').'/user/index/'.$this->connect_auth->get_access_token().'/'.$user_id;
		    $input = array('deleted' => 1);
		    $response = call_api_put($url, $input);
		    if($response && $response['header_info']['http_code'] == 200){
			$this->session->set_flashdata('notif', array('status' => true, 'msg' => 'Sukses!'));
		    }else{
			$this->session->set_flashdata('notif', array('status' => false, 'msg' => 'Gagal!'));
		    }
		    redirect($this->session->userdata('redirect'));
		}elseif($content == 'status'){
		    $activated = $this->input->get('activated');
		    $url = $this->config->item('connect_api_url').'/user/index/'.$this->connect_auth->get_access_token().'/'.$user_id;
		    $input = array(
						   'activated' => intval($activated),
						   'verified_email' => 1
						   );
		    $response = call_api_put($url, $input);
		    if($response && $response['header_info']['http_code'] == 200){
			$this->session->set_flashdata('notif', array('status' => true, 'msg' => 'Sukses!'));
		    }else{
			$this->session->set_flashdata('notif', array('status' => false, 'msg' => 'Gagal!'));
		    }
		    redirect($this->session->userdata('redirect'));
		}
	
		$this->load->view('admin/main',$data);
    }
    
    function instructor($content = 'home', $user_id=null){
	
		$data['title_page'] = 'User Pengajar';
		$data['title'] = $this->config->item('title').' : '.$data['title_page'];
		$data['entity'] = 'narasumber';
		
		if($content == 'home'){
		    $this->session->set_userdata('redirect', current_url());
		    $data['content'] = 'user_admin_instructor';
		}elseif($content == 'add'){
		    $data['title_content'] = 'Tambah';
		    $data['content'] = 'instructor_add';
		}elseif($content == 'edit'){
		    $data['title_content'] = 'Edit';
		    $data['user_id'] = $user_id;
		    config_kcfinder(array('user_id' => $user_id));
		    $data['content'] = 'instructor_add';
		}elseif($content == 'delete'){
		    $url = $this->config->item('connect_api_url').'/user/index/'.$this->connect_auth->get_access_token().'/'.$user_id;
		    $input = array('deleted' => 1);
		    $response = call_api_put($url, $input);
		    if($response && $response['header_info']['http_code'] == 200){
			$this->session->set_flashdata('notif', array('status' => true, 'msg' => 'Sukses!'));
		    }else{
			$this->session->set_flashdata('notif', array('status' => false, 'msg' => 'Gagal!'));
		    }
		    redirect($this->session->userdata('redirect'));
		}elseif($content == 'status'){
		    $activated = $this->input->get('activated');
		    $url = $this->config->item('connect_api_url').'/user/index/'.$this->connect_auth->get_access_token().'/'.$user_id;
		    $input = array(
						   'activated' => intval($activated),
						   'verified_email' => 1
						   );
		    $response = call_api_put($url, $input);
		    if($response && $response['header_info']['http_code'] == 200){
			$this->session->set_flashdata('notif', array('status' => true, 'msg' => 'Sukses!'));
		    }else{
			$this->session->set_flashdata('notif', array('status' => false, 'msg' => 'Gagal!'));
		    }
		    redirect($this->session->userdata('redirect'));
		}
	
		$this->load->view('admin/main',$data);
    }
    
    function institution($content = 'home', $user_id=null){
	
		$data['title_page'] = 'User Lembaga Pelatihan/LPP';
		$data['title'] = $this->config->item('title').' : '.$data['title_page'];
		$data['entity'] = 'lpp';
		
		if($content == 'home'){
		    $this->session->set_userdata('redirect', current_url());
		    $data['content'] = 'user_admin';
		    
		}elseif($content == 'add'){
		    $data['title_content'] = 'Tambah';
		    $data['content'] = 'user_admin_add';
		}elseif($content == 'edit'){
		    $data['title_content'] = 'Edit';
		    $data['user_id'] = $user_id;
// 		    config_kcfinder(array('user_id' => $user_id));
		    $data['content'] = 'user_admin_add';
		}elseif($content == 'delete'){
		    
		    $url = $this->config->item('connect_api_url').'/user/index/'.$this->connect_auth->get_access_token().'/'.$user_id;
		    $input = array('deleted' => 1);
		    $response = call_api_put($url, $input);
		    if($response && $response['header_info']['http_code'] == 200){
			$this->session->set_flashdata('notif', array('status' => true, 'msg' => 'Sukses!'));
		    }else{
			$this->session->set_flashdata('notif', array('status' => false, 'msg' => 'Gagal!'));
		    }
	    
		    redirect($this->session->userdata('redirect'));
		}elseif($content == 'status'){
		    $activated = $this->input->get('activated');
		    $url = $this->config->item('connect_api_url').'/user/index/'.$this->connect_auth->get_access_token().'/'.$user_id;
		    $input = array(
						   'activated' => intval($activated),
						   'verified_email' => 1
						   );
		    
			$response = call_api_get($url);
			if($response && $response['header_info']['http_code'] == 200){
				$user = json_decode($response['body'],true);
				if(!isset($user['verified_institute'])){
					$input['verified_institute'] = 0;
				}
			}
			
			$response = call_api_put($url, $input);
		    if($response && $response['header_info']['http_code'] == 200){
				$this->session->set_flashdata('notif', array('status' => true, 'msg' => 'Sukses!'));
		    }else{
				$this->session->set_flashdata('notif', array('status' => false, 'msg' => 'Gagal!'));
		    }
		    redirect($this->session->userdata('redirect'));
		}
	
		$this->load->view('admin/main',$data);
    }
    
    function participant($content = 'home', $user_id=null){
	
		$data['title_page'] = 'User Peserta';
		$data['title'] = $this->config->item('title').' : '.$data['title_page'];
		$data['entity'] = 'peserta';
		
		if($content == 'home'){
		    $this->session->set_userdata('redirect', current_url());
		    $data['content'] = 'user_admin';
		    
		}elseif($content == 'add'){
		    $data['title_content'] = 'Tambah';
		    $data['content'] = 'user_admin_add';
		}elseif($content == 'edit'){
		    $data['title_content'] = 'Edit';
		    $data['user_id'] = $user_id;
// 		    config_kcfinder(array('user_id' => $user_id));
		    $data['content'] = 'user_admin_add';
		}elseif($content == 'delete'){
		    //$data['content'] = 'user_admin';
		    
		    $url = $this->config->item('connect_api_url').'/user/index/'.$this->connect_auth->get_access_token().'/'.$user_id;
		    $input = array('deleted' => 1);
		    $response = call_api_put($url, $input);
		    if($response && $response['header_info']['http_code'] == 200){
			$this->session->set_flashdata('notif', array('status' => true, 'msg' => 'Sukses!'));
		    }else{
			$this->session->set_flashdata('notif', array('status' => false, 'msg' => 'Gagal!'));
		    }
	    
		    redirect($this->session->userdata('redirect'));
		}elseif($content == 'status'){
		    $activated = $this->input->get('activated');
		    $url = $this->config->item('connect_api_url').'/user/index/'.$this->connect_auth->get_access_token().'/'.$user_id;
		    $input = array(
						   'activated' => intval($activated),
						   'verified_email' => 1
						   );
		    $response = call_api_put($url, $input);
		    if($response && $response['header_info']['http_code'] == 200){
			$this->session->set_flashdata('notif', array('status' => true, 'msg' => 'Sukses!'));
		    }else{
			$this->session->set_flashdata('notif', array('status' => false, 'msg' => 'Gagal!'));
		    }
		    redirect($this->session->userdata('redirect'));
		}
	
		$this->load->view('admin/main',$data);
    }
    
    function post_user_admin($entity=null, $user_id=null){
	
	$input = $this->input->post();
	unset($input['rpassword']);
	
	if($user_id){
	    //update
	    $url = $this->config->item('connect_api_url').'/user/index/'.$this->connect_auth->get_access_token().'/'.$user_id;
	    $response = call_api_put($url, $input);
	}else{
		//filter
		if($entity == 'narasumber'){
			//lokasi provinsi
			$province_id = $input['office_province'];
			
			if(isset($input['office_province'])){
			    
			    $query = file_get_contents($this->config->item('connect_api_url').'/location/province');
			    $province = array();
			    $province_name = '';
			    if($query){
				$province = json_decode($query,true);
			    }
			    
			    if($province){
				foreach($province as $row){
				    if($row['id'] == $province_id){
					$province_name = $row['name'];
				    }
				}
			    }
			    
			    $input['office_province'] = array('id' => $province_id, 'name' => $province_name );
			}
			
			//lokasi kabkota
			$district_id = $input['office_district'];
			if(isset($input['office_district'])){
			    
			    $query = file_get_contents($this->config->item('connect_api_url').'/location/district/'.$province_id);
			    $district = array();
			    $district_name = '';
			    if($query){
				$district = json_decode($query,true);
			    }
			    
			    if($district){
				foreach($district as $row){
				    if($row['id'] == $district_id){
					$district_name = $row['name'];
				    }
				}
			    }
			    
			    $input['office_district'] = array('id' => $district_id, 'name' => $district_name );
			}
		}
		
	    //insert
	    $url = $this->config->item('connect_api_url').'/register/index/'.$entity;
	    $response = call_api_post($url, $input);
	}
	
	if($response && $response['header_info']['http_code'] == 200){
	    $user_id = $response_body['user_id'];
	    $response_body = json_decode($response['body'], true);
	    $this->create_folder($user_id);
	    $this->session->set_flashdata('notif', array('status' => true, 'msg' => 'Sukses!'));
	}else{
	    $this->session->set_flashdata('notif', array('status' => false, 'msg' => 'Gagal!'));
	}
	redirect($this->session->userdata('redirect'));


    }
//     function post_user_admin($user_id=null){
// 	
// 		$data_user = array(
// 		    'fullname' => $this->input->post('fullname'),
// 		    'gender' => $this->input->post('gender'),
// 		    'address' => $this->input->post('address'),
// 		    'birth_date' => $this->input->post('birth_date'),
// 		    'birth_place' => $this->input->post('birth_place'),
// 		    'phone' => $this->input->post('phone'),
// 		    'province' => $this->input->post('province'),
// 		    'district' => $this->input->post('district'),
// 		    'subdistrict' => $this->input->post('subdistrict'),
// 		    'username' => $this->input->post('username'),
// 		    'email' => $this->input->post('email'),
// 		    'activated' => ($this->input->post('activated') ? 1 : 0 ),
// 		    'role' => array('admin'),
// 		    'user' => 'admin'
// 		);
// 	
// 		if($user_id){
// 		    //update
// 	    
// 	    
// 		    if($this->input->post('password')!='password')
// 			$data_user['password'] = sha1_salt($this->input->post('password'));
// 	    
// 		    if($this->user_db->save($user_id, $data_user)){
// 	// 		echo $last_save->{'$id'};
// 			$this->create_folder($user_id);
// 			$this->session->set_flashdata('notif', array('status' => true, 'msg' => 'Sukses!'));
// 		    }else{
// 			$this->session->set_flashdata('notif', array('status' => false, 'msg' => 'Gagal!'));
// 		    }
// 	    
// 		}else{
// 		    //new
// 	    
// 		    $data_user['password'] = sha1_salt($this->input->post('password'));
// 		    $data_user['token'] = generate_token();
// 	// 	    print_r($data_user);
// 	    
// 		    if($last_save = $this->user_db->save(null, $data_user)){
// 	    
// 			$this->create_folder($last_save->{'$id'});
// 			$this->session->set_flashdata('notif', array('status' => true, 'msg' => 'Sukses!'));
// 		    }else{
// 			$this->session->set_flashdata('notif', array('status' => false, 'msg' => 'Gagal!'));
// 		    }
// 		}
// 	
// 		redirect('admin/management_user/user_super_admin');
//     }
    
    function index(){
		redirect('superadmin/management_user/admin');
    }
	
//     function create_folder($user_id){	    
// 		//create folder user
// 		$path_user = getcwd().'/media/upload/users/'.$user_id;
// 		if(!file_exists($path_user)){
// 		    mkdir($path_user);
// 		    //chmod($path_user, "0775");
// 		    shell_exec('chmod 777 '.$path_user);
// 		}
//     }
    function create_folder($user_id){	
	//create folder user
	$path_user = getcwd().'/media/upload/users/'.$user_id;
	if(!file_exists($path_user)){
	    mkdir($path_user);
	    shell_exec('chmod 777 '.$path_user);

	    $path_user = getcwd().'/media/upload/users/'.$user_id.'/files';
	    if(!file_exists($path_user)){
		mkdir($path_user);
		shell_exec('chmod 777 '.$path_user);
	    }
	}
	
	return is_dir($path_user);
    }
	
	// added by yoze
	function user_admin($content = 'home', $user_id=null) {
		$data['title_page'] = 'User Admin';
		$data['title'] = $this->config->item('title').' : '.$data['title_page'];
	
		// if($content == 'home'){
		//     $data['content'] = 'user_admin';
		// }elseif($content == 'add'){
		//     $data['title_content'] = 'Tambah';
		//     $data['content'] = 'user_admin_add';
		// }elseif($content == 'edit'){
		//     $data['title_content'] = 'Edit';
		//     $data['user_id'] = $user_id;
		//     config_kcfinder(array('user_id' => $user_id));
		//     $data['content'] = 'user_admin_add';
		// }elseif($content == 'delete'){
		//     //$data['content'] = 'user_admin';
		//     if($this->user_db->delete($user_id)){
		// 		$this->session->set_flashdata('notif', array('status' => true, 'msg' => 'Sukses!'));
		//     }else{
		// 		$this->session->set_flashdata('notif', array('status' => false, 'msg' => 'Gagal!'));
		//     }
		//
		//     redirect('admin/management_user/user_admin');
		// }
	
	
		if($content == 'home'){
		    $data['content'] = 'admin';
		}
	
		$this->load->view('admin/main',$data);
	}
	// added by Dedi
	function filter_instructor($data){
		
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */