<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Setting extends CI_Controller {

    function __construct(){
		parent::__construct();

		session_start();
		config_kcfinder(array('user_id' => $this->connect_auth->get_user_id()));
    }
    
	function example_document_registration($content = 'home'){
		check_token($this->connect_auth->get_access_token(), $allow_role = array('superadmin'));
		$this->session->set_userdata('redirect',current_url());
		$data['title_page'] = 'Contoh Dokumen Registrasi';
		$data['title'] = $this->config->item('title').' : '.$data['title_page'];
	
		$data['title_content'] = 'Edit';
		$data['setting_id'] = 'example_document_registration';
		$data['content'] = 'example_document_registration';
	
		$this->load->view('admin/main',$data);
	}
	
	function example_document_registration_post(){
		$input = $this->input->post();
		$input['setting_id'] = 'example_document_registration';
		$setting_id = $input['setting_id'];
		
		if($this->setting_db->save($setting_id, $input)){
			$this->session->set_flashdata('notif', array('status' => true, 'msg' => 'Sukses!'));
		}else{
			$this->session->set_flashdata('notif', array('status' => false, 'msg' => 'Gagal!'));
		}
		
		redirect($this->session->userdata('redirect'));
    }
	
    function max_bussinesday_submission_training($content = 'home'){
		check_token($this->connect_auth->get_access_token(), $allow_role = array('superadmin'));
		$data['title_page'] = 'Set Maksimal Hari Pengajuan Pelatihan';
		$data['title'] = $this->config->item('title').' : '.$data['title_page'];
		
		if($content == 'home'){
			$this->session->set_userdata('redirect',current_url());
			$data['title_content'] = 'Edit';
			$data['content'] = 'max_bussinesday_submission_training_add';
		}

		$this->load->view('admin/main',$data);
    }
    
	function max_bussinesday_submission_training_post(){
		$input = $this->input->post();
		$input['setting_id'] = 'max_bussinesday_submission_training';
		$setting_id = $input['setting_id'];
		$input['day'] = (isset($input['day']) ? intval($input['day']) : 0 );
		
		
		if($this->setting_db->save($setting_id, $input)){
			$this->session->set_flashdata('notif', array('status' => true, 'msg' => 'Sukses!'));
		}else{
			$this->session->set_flashdata('notif', array('status' => false, 'msg' => 'Gagal!'));
		}
		
		redirect($this->session->userdata('redirect'));
    }
	
	function limit_submission_training($content = 'home'){
		check_token($this->connect_auth->get_access_token(), $allow_role = array('superadmin'));
		$data['title_page'] = 'Limit Pengajuan Pelatihan oleh LPP';
		$data['title'] = $this->config->item('title').' : '.$data['title_page'];
		
		if($content == 'home'){
			$this->session->set_userdata('redirect',current_url());
			$data['title_content'] = 'Edit';
			$data['content'] = 'limit_submission_training_add';
		}

		$this->load->view('admin/main',$data);
    }
	
    function limit_submission_training_post(){
		$input = $this->input->post();
		$input['setting_id'] = 'limit_submission_training';
		$setting_id = $input['setting_id'];
		$input['limit'] = (isset($input['limit']) ? intval($input['limit']) : 0 );
		
		
		if($this->setting_db->save($setting_id, $input)){
			$this->session->set_flashdata('notif', array('status' => true, 'msg' => 'Sukses!'));
		}else{
			$this->session->set_flashdata('notif', array('status' => false, 'msg' => 'Gagal!'));
		}
		
		redirect($this->session->userdata('redirect'));
    }
    
    function todolist($content = 'home', $todolist_id=null){
	check_token($this->connect_auth->get_access_token(), $allow_role = array('superadmin'));
	$data['title_page'] = 'Todolist';
	$data['title'] = $this->config->item('title').' : '.$data['title_page'];
	$data['todolist_id'] = $todolist_id;

	if($content == 'home'){
	    $this->session->set_userdata('redirect',current_url());
	    $data['content'] = 'todolist';
	}elseif($content == 'add'){
	    $data['title_content'] = 'Tambah';
	    $data['content'] = 'todolist_add';

	}elseif($content == 'edit'){
	    $data['title_content'] = 'Edit';

	    $data['content'] = 'todolist_add';

	}elseif($content == 'delete'){
	    //$data['content'] = 'user_admin';
      
	    if($this->todolist_db->delete($todolist_id)){
		$this->session->set_flashdata('notif', array('status' => true, 'msg' => 'Sukses!'));
	    }else{
		$this->session->set_flashdata('notif', array('status' => false, 'msg' => 'Gagal!'));
	    }

	    redirect($this->session->userdata('redirect'));
	}

	$this->load->view('admin/main',$data);
    }
    
    function todolist_post($todolist_id=null){
	check_token($this->connect_auth->get_access_token(), $allow_role = array('superadmin'));
	$input = $this->input->post();
	
	$input['published'] = (isset($input['published']) ? intval($input['published']) : 0);
	
	$data_before_update = array();
	if($query = $this->todolist_db->get($todolist_id)){
	    $data_before_update = $query[0];
	}

	if($this->todolist_db->save($todolist_id, $input)){

	    /*save log*/
	    $data_log = array(
		'table_name' => 'todolist',
		'table_id' => $todolist_id,
		'log_type' => 'todolist_last_update',
		'ip_address' => $this->input->ip_address(),
		'user_id' => $this->connect_auth->get_me()['user_id'],
		'fullname' => $this->connect_auth->get_me()['fullname'],
		'time' => time(),
		'data_before_update' => $data_before_update
	    );

	    $this->log_db->save(null, $data_log);

	    $this->session->set_flashdata('notif', array('status' => true, 'msg' => 'Sukses!'));
	}else{
	    $this->session->set_flashdata('notif', array('status' => false, 'msg' => 'Gagal!'));
	}

	redirect($this->session->userdata('redirect'));
    }
    
    function type_training($content = 'home', $type_training_id=null){
	check_token($this->connect_auth->get_access_token(), $allow_role = array('superadmin'));
	$data['title_page'] = 'Jenis Pelatihan';
	$data['title'] = $this->config->item('title').' : '.$data['title_page'];
	$data['type_training_id'] = $type_training_id;

	if($content == 'home'){
	    $this->session->set_userdata('redirect',current_url());
	    $data['content'] = 'type_training';
	}elseif($content == 'add'){
	    $data['title_content'] = 'Tambah';
	    $data['content'] = 'type_training_add';

	}elseif($content == 'edit'){
	    $data['title_content'] = 'Edit';

	    $data['content'] = 'type_training_add';

	}elseif($content == 'delete'){
	    //$data['content'] = 'user_admin';
      
	    if($this->type_training_db->delete($type_training_id)){
		$this->session->set_flashdata('notif', array('status' => true, 'msg' => 'Sukses!'));
	    }else{
		$this->session->set_flashdata('notif', array('status' => false, 'msg' => 'Gagal!'));
	    }

	    redirect($this->session->userdata('redirect'));
	}

	$this->load->view('admin/main',$data);
    }
    
    function type_training_post($type_training_id=null){
	check_token($this->connect_auth->get_access_token(), $allow_role = array('superadmin'));
	$input = $this->input->post();
	
	$input['published'] = (isset($input['published']) ? intval($input['published']) : 0);
	
	$data_before_update = array();
	if($query = $this->type_training_db->get($type_training_id)){
	    $data_before_update = $query[0];
	}

	if($this->type_training_db->save($type_training_id, $input)){

	    /*save log*/
	    $data_log = array(
		'table_name' => 'type_training',
		'table_id' => $type_training_id,
		'log_type' => 'type_training_last_update',
		'ip_address' => $this->input->ip_address(),
		'user_id' => $this->connect_auth->get_me()['user_id'],
		'fullname' => $this->connect_auth->get_me()['fullname'],
		'time' => time(),
		'data_before_update' => $data_before_update
	    );

	    $this->log_db->save(null, $data_log);

	    $this->session->set_flashdata('notif', array('status' => true, 'msg' => 'Sukses!'));
	}else{
	    $this->session->set_flashdata('notif', array('status' => false, 'msg' => 'Gagal!'));
	}

	redirect($this->session->userdata('redirect'));
    }
    
    function set_default_hour_leasson_instructor($content = 'home', $user_id=null){
	check_token($this->connect_auth->get_access_token(), $allow_role = array('superadmin'));
	
	$data['title_page'] = 'Set Default JP Pengajar';
	$data['title'] = $this->config->item('title').' : '.$data['title_page'];
	$data['entity'] = 'narasumber';
	
	if($content == 'home'){
	    $this->session->set_userdata('redirect', current_url());
	    $data['content'] = 'instructor_hour_leasson';
	}elseif($content == 'status'){
	    $activated = $this->input->get('activated');
	    $url = $this->config->item('connect_api_url').'/user/index/'.$this->connect_auth->get_access_token().'/'.$user_id;
	    $input = array('activated' => intval($activated));
	    $response = call_api_put($url, $input);
	    if($response && $response['header_info']['http_code'] == 200){
		$this->session->set_flashdata('notif', array('status' => true, 'msg' => 'Sukses!'));
	    }else{
		$this->session->set_flashdata('notif', array('status' => false, 'msg' => 'Gagal!'));
	    }
	    redirect($this->session->userdata('redirect'));
	}

	$this->load->view('admin/main',$data);
    }
    
    function set_default_hour_leasson_instructor_post($hour_leasson_id = null){
	check_token($this->connect_auth->get_access_token(), $allow_role = array('superadmin'));
	
	$input = $this->input->post();
	$input['date_assignment'] = array(
	    'date' => date('Y-m-d'),
	    'year' => date('Y'),
	    'month' => date('m'),
	    'day' => date('d')
	);
	
	$input['instructor_approved'] = 1;
	$input['instructor_approved_log'] = $this->connect_auth->get_me();
	$input['admin_approved'] = 1;
	$input['admin_approved_log'] = $this->connect_auth->get_me();
	//check training_id dengan instructor_id dan tahun yang sama tdk bisa doble alias unique data
	
	$data_before_update = array();
	
	if($hour_leasson_id && $query = $this->hour_leasson_db->get($hour_leasson_id)){
	    $data_before_update = $query[0];
	}

	if($this->hour_leasson_db->save($hour_leasson_id, $input)){

	    /*save log*/
	    
	    if($hour_leasson_id){
		$data_log = array(
		    'table_name' => 'hour_leasson',
		    'table_id' => $hour_leasson_id,
		    'log_type' => 'hour_leasson_last_update',
		    'ip_address' => $this->input->ip_address(),
		    'user_id' => $this->connect_auth->get_me()['user_id'],
		    'fullname' => $this->connect_auth->get_me()['fullname'],
		    'time' => time(),
		    'data_before_update' => $data_before_update
		);

		$this->log_db->save(null, $data_log);

	    }
	    
	    $this->session->set_flashdata('notif', array('status' => true, 'msg' => 'Sukses!'));
	}else{
	    $this->session->set_flashdata('notif', array('status' => false, 'msg' => 'Gagal!'));
	}

	redirect($this->session->userdata('redirect'));
	
    }
    
    function config_training($content = 'home', $config_training_id=null){
	check_token($this->connect_auth->get_access_token(), $allow_role = array('superadmin'));
	$data['title_page'] = 'Nama Pelatihan';
	$data['title'] = $this->config->item('title').' : '.$data['title_page'];
	$data['config_training_id'] = $config_training_id;

	if($content == 'home'){
	    $this->session->set_userdata('redirect',current_url());
	    $data['content'] = 'config_training';
	}elseif($content == 'add'){
	    $data['title_content'] = 'Tambah';
	    $data['content'] = 'config_training_add';

	}elseif($content == 'edit'){
	    $data['title_content'] = 'Edit';

	    $data['content'] = 'config_training_add';

	}elseif($content == 'delete'){
	    //$data['content'] = 'user_admin';
      
	    if($this->config_training_db->delete($config_training_id)){
		$this->session->set_flashdata('notif', array('status' => true, 'msg' => 'Sukses!'));
	    }else{
		$this->session->set_flashdata('notif', array('status' => false, 'msg' => 'Gagal!'));
	    }

	    redirect($this->session->userdata('redirect'));
	}

	$this->load->view('admin/main',$data);
    }

    function config_training_post($config_training_id=null){
		check_token($this->connect_auth->get_access_token(), $allow_role = array('superadmin'));
		$input = $this->input->post();
// 		print_r($input); exit();
		if(!isset($input['published'])){
		    $input['published'] = 0;
		}else{
		    $input['published'] = intval($input['published']);
		}
		
		if(!isset($input['requirement_letter_assignment'])){
		    $input['requirement_letter_assignment'] = 0;
		}else{
		    $input['requirement_letter_assignment'] = intval($input['requirement_letter_assignment']);
		}
		
		if(!isset($input['enable_support_document'])){
		    $input['enable_support_document'] = 0;
		}else{
		    $input['enable_support_document'] = intval($input['enable_support_document']);
		}
		
		if(!isset($input['requirement_certificate'])){
		    $input['requirement_certificate'] = 0;
		}else{
		    $input['requirement_certificate'] = intval($input['requirement_certificate']);
		}
		
		if(!isset($input['requirement_educational'])){
		    $input['requirement_educational'] = array();
		}
		
		if(isset($input['type_training'])){
		    $input['type_training'] = array(
			'id' => $input['type_training'],
			'name' => $this->type_training_db->get($input['type_training'])[0]['name']
		    );
		}
		
		if(!$config_training_id){
		    //save di new aja, edit ga bisa
		    $input['author'] = array(
			'fullname' => $this->connect_auth->get_me()['fullname'],
			'user_id' => $this->connect_auth->get_me()['user_id'],
		    );
		}
		
		
		$data_before_update = array();
		if($query = $this->config_training_db->get($config_training_id)){
		    $data_before_update = $query[0];
		}

		if($this->config_training_db->save($config_training_id, $input)){

		    /*save log*/
		    $data_log = array(
			'table_name' => 'config_training',
			'table_id' => $config_training_id,
			'log_type' => 'config_training_last_update',
			'ip_address' => $this->input->ip_address(),
			'user_id' => $this->connect_auth->get_me()['user_id'],
			'fullname' => $this->connect_auth->get_me()['fullname'],
			'time' => time(),
			'data_before_update' => $data_before_update
		    );

		    $this->log_db->save(null, $data_log);

		    $this->session->set_flashdata('notif', array('status' => true, 'msg' => 'Sukses!'));
		}else{
		    $this->session->set_flashdata('notif', array('status' => false, 'msg' => 'Gagal!'));
		}

		redirect($this->session->userdata('redirect'));
    }

    function index(){
		redirect('superadmin');
    }
}

/* End of file pages.php */
/* Location: ./application/controllers/superadmin/pages.php */
