<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends CI_Controller {
    
    function __construct(){
		parent::__construct();
		check_token($this->connect_auth->get_access_token(), $allow_role = array('superadmin'));
    }
    
    function index(){
		$data['title'] = $this->config->item('title').' : Dashboard';
		$data['content'] = 'dashboard';
		$this->load->view('admin/main',$data);
    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */