<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once APPPATH.'/controllers/notification.php';

class Management_training extends CI_Controller {
    
    function __construct(){
		parent::__construct();
		session_start();
		config_kcfinder(array('user_id' => $this->connect_auth->get_user_id()));
    }
    
    
    function training($content='home', $training_id = null){
		check_token($this->connect_auth->get_access_token(), $allow_role = array('fasilitator'));
		$data['title'] = $this->config->item('title').' : Dashboard List Pelatihan';
		$data['title_page'] = 'List Pelatihan';
		$data['title'] = $this->config->item('title').' : '.$data['title_page'];
		$data['training_id'] = $training_id;
	
		if($content == 'home'){	    
		    $this->session->set_userdata('redirect',current_url());
		    $data['content'] = 'submission_training_fasilitator';
		}elseif($content == 'detail'){
		    $this->session->set_userdata('redirect',current_url());
		    $data['title_content'] = 'Detail Pelatihan';
		    $data['content'] = 'detail_training';
		}
		/*
		elseif($content == 'add'){
		    $data['title_content'] = 'Pengajuan Pelatihan';
		    $data['content'] = 'dashboard/lpp_training_submission';
		}elseif($content == 'edit'){
		    $data['title_content'] = 'Pengajuan Pelatihan';
		    $data['content'] = 'dashboard/lpp_training_submission';
		}elseif($content == 'delete'){
		    //$data['content'] = 'user_admin';
		    if($this->training_db->delete($training_id)){
			$this->session->set_flashdata('notif', array('status' => true, 'msg' => 'Sukses!'));
		    }else{
			$this->session->set_flashdata('notif', array('status' => false, 'msg' => 'Gagal!'));
		    }
    
		    redirect($this->session->userdata('redirect'));
		}
		*/
		$this->load->view('admin/main',$data);
	
    }
	
	//ADDED BY YOZE
	/*
	function training_revise($content='home', $training_id = null) {
		check_token($this->connect_auth->get_access_token(), $allow_role = array('fasilitator'));
		$data['title'] = $this->config->item('title').' : Dashboard List Pelatihan';
		$data['title_page'] = 'List Pelatihan';
		$data['title'] = $this->config->item('title').' : '.$data['title_page'];
		$data['training_id'] = $training_id;
		
		if($content == 'home'){	    
		    $this->session->set_userdata('redirect',current_url());
		    $data['content'] = 'approval_submission_training_revise';
		}
		$this->load->view('admin/main',$data);
	}
	*/
	function training_post($training_id=null){
		check_token($this->connect_auth->get_access_token(), $allow_role = array('admin','lpp'));
		$input = $this->input->post();	

		if(isset($input['submit']) && $input['submit']=='draft'){
			$input['published'] = 0;
			$input['draft'] = 1;
		}else{
			$input['draft'] = 0;
			$input['verified_submission_training'] = 0; //menunggu verifikasi
		}
		unset($input['submit']);

		if(!$training_id){
			//save di new aja, edit ga bisa
			$input['author'] = $this->connect_auth->get_me();	    
		}

		//filter dulu

		//jenis pengajuan
		if(isset($input['type_submission_training'])){
			$input['type_submission_training'] = array('id' => $input['type_submission_training'],
			'name' => $this->init_config_db->get_type_submission_training
			( $input['type_submission_training'] ));
		}

		//jenis pelatihan
		if(isset($input['type_training'])){
			$input['type_training'] = array('id' => $input['type_training'], 
			'name' => $this->init_config_db->get_type_training( $input['type_training'] ));
		}

		//nama pelatihan
		if(isset($input['training_name'])){
			$input['training_name'] = array('id' => $input['training_name'], 'name' => $this->config_training_db->get( $input['training_name'] )[0]['name']);
		}

		//tipe pendaftaran peserta	
		if(isset($input['type_registration_participant'])){
			$input['type_registration_participant'] = array('id' =>
				$input['type_registration_participant'], 'name' =>
					$this->init_config_db->get_type_registration_participant( 
			$input['type_registration_participant'] ));
		}

		//lokasi provinsi
		$province_id = $input['training_location_province'];

		if(isset($input['training_location_province'])){
			$query = file_get_contents($this->config->item('connect_api_url').'/location/province');
			$province = array();
			$province_name = '';
			if($query){
				$province = json_decode($query,true);
			}
			if($province){
				foreach($province as $row){
					if($row['id'] == $province_id){
						$province_name = $row['name'];
					}
				}
			}

			$input['training_location_province'] = array('id' => $province_id, 
			'name' => $province_name );
		}

		//lokasi kabkota
		$district_id = $input['training_location_district'];
		if(isset($input['training_location_district'])){
			$query = file_get_contents($this->config->item('connect_api_url').'/location/district/'.$province_id);
			$district = array();
			$district_name = '';
			if($query){
				$district = json_decode($query,true);
			}

			if($district){
				foreach($district as $row){
					if($row['id'] == $district_id){
						$district_name = $row['name'];
					}
				}
			}

			$input['training_location_district'] = array('id' => $district_id, 
			'name' => $district_name );
		}

		$data_before_update = array();
		if($query = $this->training_db->get($training_id)){
			$data_before_update = $query[0];
		}

		if($this->training_db->save($training_id, $input)){
			/*save log*/
			$data_log = array(
				'table_name' => 'training',
				'table_id' => $training_id,
				'log_type' => 'training_last_update',
				'ip_address' => $this->input->ip_address(),
				'user_id' => $this->connect_auth->get_me()['user_id'],
				'fullname' => $this->connect_auth->get_me()['fullname'],
				'time' => time(),
				'data_before_update' => $data_before_update
			);

			$this->log_db->save(null, $data_log);

			$this->session->set_flashdata('notif', array('status' => true, 'msg' => 'Sukses!'));
		}else{
			$this->session->set_flashdata('notif', array('status' => false, 'msg' => 'Gagal!'));
		}	
		redirect($this->session->userdata('redirect'));
	}
	
	
	function elearning($content='home', $training_id=null){
		check_token($this->connect_auth->get_access_token(), $allow_role = array('fasilitator'));
		$data['title'] = $this->config->item('title').' : Dashboard List eLearning';
		$data['title_page'] = 'List Pelatihan eLearning';
		$data['title'] = $this->config->item('title').' : '.$data['title_page'];
		$data['training_id'] = $training_id;
	
		if($content == 'home'){	    
		    $this->session->set_userdata('redirect',current_url());
		    $data['content'] = 'submission_training_fasilitator_elearning';
		}		
				
		$this->load->view('admin/main',$data);				
	}
	
	function elearning_proccess_instructor(){
		$lms_api_url = $this->config->item('lms_api_url');		
		$input = $this->input->post();

		$count = count($input['training_id']);
		$updated = 0;
		//redirect if empty data
		if($count == 0) redirect($this->session->userdata('redirect'));
		for($i = 0; $i< $count; $i++){
			$instructor_id = $input['instructor_id'][$i];
			$training_id = $input['training_id'][$i];	
			
			if($instructor_id == '') continue;

			
		    $input = array(
			'training_id' => $training_id,
			'user_id' => $instructor_id,		
		    );
			
		    $header = array(
			'Content-Type'=>'application/x-www-form-urlencoded'
		    );
			$update_url = $lms_api_url."/training/instructor/".$this->connect_auth->get_access_token();
				
		    $response = curl_post($update_url, $input, null, $header);

		    if($response && $response['header_info']['http_code'] == 200){
			$response_body = json_decode($response['body'],true);
		
			    if($response_body['status'] == 'OK'){
					//ok success
					$updated++;
			    }
			}								
		}
							
		if($updated > 0){
		    $this->session->set_flashdata('notif', array('status' => true, 'msg' => 'Sukses!'));
		}else{
		    $this->session->set_flashdata('notif', array('status' => false, 'msg' => 'Gagal!'));
		}
		
		redirect($this->session->userdata('redirect'));
	}
	
	/*
	function instructor($content='home', $training_id = null){
		check_token($this->connect_auth->get_access_token(), $allow_role = array('fasilitator'));
		$data['title'] = $this->config->item('title').' : Dashboard List Pelatihan';
		$data['title_page'] = 'List Pelatihan';
		$data['title'] = $this->config->item('title').' : '.$data['title_page'];
		$data['training_id'] = $training_id;

		if($content == 'home'){
			$this->session->set_userdata('redirect',current_url());
			$data['content'] = 'info_instructor';
		}elseif($content == 'choose'){
			$this->session->set_userdata('redirect',current_url());
			$data['content'] = 'choose_instructor';
		}elseif($content == 'add'){
			$data['title_content'] = 'Pengajuan Pelatihan';
			$data['content'] = 'dashboard/lpp_training_submission';
		}elseif($content == 'edit'){
			$data['title_content'] = 'Pengajuan Pelatihan';
			$data['content'] = 'dashboard/lpp_training_submission';
		}elseif($content == 'delete'){
			//$data['content'] = 'user_admin';
			if($this->training_db->delete($training_id)){
				$this->session->set_flashdata('notif', array('status' => true, 'msg' => 'Sukses!'));
			}else{
				$this->session->set_flashdata('notif', array('status' => false, 'msg' => 'Gagal!'));
			}

			redirect($this->session->userdata('redirect'));
		}
		$this->load->view('admin/main',$data);
	}
	*/
	
	// ADDED BY YOZE
	function instructor($content='home', $training_id = null){
		check_token($this->connect_auth->get_access_token(), $allow_role = array('fasilitator'));
		$data['title'] = $this->config->item('title').' : Dashboard List Pelatihan';
		$data['title_page'] = 'List Pelatihan';
		$data['title'] = $this->config->item('title').' : '.$data['title_page'];
		$data['training_id'] = $training_id;

		if($content == 'home'){
			$this->session->set_userdata('redirect',current_url());
			$data['content'] = 'info_instructor';
		}elseif($content == 'choose'){
			
			$data['content'] = 'choose_instructor_revise';
		}elseif($content == 'add'){
			$data['title_content'] = 'Pengajuan Pelatihan';
			$data['content'] = 'dashboard/lpp_training_submission';
		}elseif($content == 'edit'){
			$data['title_content'] = 'Pengajuan Pelatihan';
			$data['content'] = 'dashboard/lpp_training_submission';
		}elseif($content == 'delete'){
			//$data['content'] = 'user_admin';
			if($this->training_db->delete($training_id)){
				$this->session->set_flashdata('notif', array('status' => true, 'msg' => 'Sukses!'));
			}else{
				$this->session->set_flashdata('notif', array('status' => false, 'msg' => 'Gagal!'));
			}

			redirect($this->session->userdata('redirect'));
		}
		$this->load->view('admin/main',$data);
	}

	function set_date_instructor_ajax($training_id=null, $instructor_id = null){
		if($training_id && $instructor_id){
			$this->load->view('admin/inc/ajax/set_date_instructor',
			array( 'training_id' => $training_id, 'instructor_id' => $instructor_id ) );
		}
	}
	
	function select_instructor_ajax($training_id=null){
	    if($training_id){
		$this->load->view('admin/inc/ajax/select_instructor',array( 'training_id' => $training_id ) );
	    }
	}
	function hour_leasson_post($training_id=null){
		$input = $this->input->post();
// 		print_r($input); exit();
		$result = false;
		
		if($input['submit']=='save'){
		    
			//form validation
			$this->form_validation->set_message('required', '- <strong>%s</strong> harus diisi!');
			$this->form_validation->set_rules('instructor_id[]', 'Nama Pengajar', 'required');
			$this->form_validation->set_rules('set_date', 'Tanggal Pengajar', 'required');
			$this->form_validation->set_rules('hour_leasson[]', 'Jam Pelajaran', 'required');
			$this->form_validation->set_rules('main_instructor[]', 'Status Pengajar', 'required');
			
			if ($this->form_validation->run() == FALSE){
				$this->session->set_flashdata('notif', array('status' => false, 'msg' => validation_errors()));
				redirect($this->session->userdata('redirect').'#tab_instructor');
			}
			//exit();
			
// 		    print_r($input); exit();
		    $count = count($input['instructor_id']);
		    for($i=0; $i < $count; $i++){
			    
			    if($input['main_instructor'][$i]=='main'){
					//set pengajar utama
					
					//save
					$hour_leasson_id = null;
					$data_input = array();
					
	// 				print_r($input); exit();
					
					$data_training = null;
					if($query = $this->training_db->get($training_id)){
						$data_training = $query[0];
					}
					
					$data_input = array(
						'training_id' => $training_id,
						'training_log' => ($data_training ? $data_training : array()),
						'instructor_id' => $input['instructor_id'][$i],
						'hour_leasson' => $input['hour_leasson'][$i],
						'date_assignment' => array(
							'date' => date('Y-m-d'),
							'year' => date('Y'),
							'month' => date('m'),
							'day' => date('d')
						),
						'submission_date' => $input['set_date'][$input['instructor_id'][$i]],
						'submission_time_range' => $input['set_time_range'][$input['instructor_id'][$i]],
						
						'instructor_approved' => 0,
						'instructor_approved_log' => array(),
						'admin_approved' => 0,
						'admin_approved_log' => array()
	
					);
	
					$hour_leasson_id = $this->hour_leasson_db->exist_training_instructor($data_input['training_id'], $data_input['instructor_id']);
					if($this->hour_leasson_db->save($hour_leasson_id, $data_input)){
	
						foreach($data_input['submission_date'] as $row){
							//booking date,
							$this->agenda_instructor_db->save_instructor_agenda($row, $data_input['instructor_id'], $training_id);
						}
						$result = true;
					}
				
			    }elseif($input['main_instructor'][$i]=='alternative'){
					//set pengajar alternatif
					//masuk worker
			    }
			    
			    
		    }
		    
		}elseif($input['submit']=='draft'){
		    
		}
		
		if($result){
		    $Notification = new Notification();
		    $Notification->review_nominate_instructor($data_fasilitator = $this->connect_auth->get_me(), $training_id);
		    $this->session->set_flashdata('notif', array('status' => true, 'msg' => 'Sukses!'));
		}else{
		    $this->session->set_flashdata('notif', array('status' => false, 'msg' => 'Gagal!'));
		}
		
		redirect($this->session->userdata('redirect'));
	}

	function create_schedule(){
		check_token($this->connect_auth->get_access_token(), $allow_role = array('fasilitator'));
		// 	$this->session->set_userdata('redirect',current_url());
		$data['title_page'] = 'Tentang Kami';
		$data['title'] = $this->config->item('title').' : '.$data['title_page'];

		$data['title_content'] = 'Edit';
		$data['page_id'] = 'about';
		$data['content'] = 'create_schedule';

		$this->load->view('admin/main',$data);
	}
}

/* End of file pages.php */
/* Location: ./application/controllers/superadmin/pages.php */