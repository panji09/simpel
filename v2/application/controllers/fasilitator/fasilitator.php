<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Fasilitator extends CI_Controller {
    function __construct(){
	parent::__construct();
	session_start();
	config_kcfinder(array('user_id' => $this->connect_auth->get_user_id()));
	check_token($this->connect_auth->get_access_token(), $allow_role = array('fasilitator'));
	
// 	print_r($this->connect_auth->get_me());
	
    }
    
    function index(){
	redirect('fasilitator/dashboard');
    }
    
}

/* End of file dashboard.php */
/* Location: ./application/controllers/admin/dashboard.php */