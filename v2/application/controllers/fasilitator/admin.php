<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Controller {
    function __construct(){
		parent::__construct();
		check_token($this->connect_auth->get_access_token(), $allow_role = array('fasilitator'));
    }
    function index(){
		redirect('admin/dashboard');
    }
}

/* End of file admin.php */
/* Location: ./application/controllers/admin/admin.php */