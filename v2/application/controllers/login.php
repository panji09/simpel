<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {
    function __construct(){
      parent::__construct();
    }
    public function index($state=null){
	if(!$state)
	    $state = base64_encode(site_url(''));
	redirect($this->config->item('connect_url').'/authorize?response_type=code&client_id='.$this->config->item('client_id').'&state='.$state);
    }

    function logout(){
		$this->connect_auth->logout();
		redirect('');
    }
    
    function my_profile(){
	redirect($this->config->item('connect_url').'/authorize?response_type=code&client_id='.$this->config->item('client_connect_id').'&state=dashboard');
    }
    function register_institution($state=null){
	//http://simpel.ruangpanji.com/v2/connect/register/lpp
	if(!$state)
	    $state = base64_encode(site_url(''));
	redirect($this->config->item('connect_url').'/register/lpp?state='.$state);
    }
    
    function register_participant($state=null){
	if(!$state)
	    $state = base64_encode(site_url(''));
	redirect($this->config->item('connect_url').'/register/peserta?state='.$state);
    }
    
    function register_instructor($invite_code = null, $state=null){
	if(!$state)
	    $state = base64_encode(site_url(''));
	
	if($query = $this->invite_code_db->exist_invite_code($invite_code))
	    redirect($this->config->item('connect_url').'/register/narasumber?state='.$state.'&invite_code='.$invite_code);
    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */