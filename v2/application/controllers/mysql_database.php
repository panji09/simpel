<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mysql_database extends CI_Controller {
    

    function __construct(){
      parent::__construct();
    }
    
    function index(){
		
		$define_db = array(
						  'config_training_db',
						  'evaluation_db',
						  'evaluation_data_db',
						  'training_db',
						  'init_config_db',
						  'type_training_db',
						  'join_training_db',
						  'score_training_db',
						  'hour_leasson_db'
						  );
	
		foreach($define_db as $db){
			$this->{$db}->generate_mysql();
		}
		
		echo 'ok';
	}
    
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */