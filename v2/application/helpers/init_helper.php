<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

 
 /*striptag*/
function stripHTMLtags($str){
    $t = preg_replace('/<[^<|>]+?>/', '', htmlspecialchars_decode($str));
    $t = htmlentities($t, ENT_QUOTES, "UTF-8");
    return $t;
}
 /*./striptag*/

/*
check token dipake di api
*/
function check_token_api($token=null, $allow_role=array()){
    $CI =& get_instance();
    $allow = false;
    
    if($allow_role && $token){
	$api_url = $CI->config->item('connect_url').'/oauth/check_token?access_token='.$token;
	$response = call_api_get($api_url);
	$response_body = json_decode($response['body'], true);
	
	if($response['header_info']['http_code'] == 200){
	    
	    
	    $user_id = $response_body['user_id'];
	    
	    $user['user_id'] = $user_id;
	    
	    $allow = true;
	}else{
	    $CI->response($response_body,$response['header_info']['http_code']);
	}
	
    }
    
    if(!$allow){
	$CI->response(
	    array(
		'error'=> 'forbidden',
		'error_description' => 'forbidden access'
	    )
	,403);
    }
}
/*
  check valid request
*/

function call_api_get($url){
    $curl = curl_init();

    curl_setopt_array($curl, array(
	CURLOPT_URL => $url,
// 	CURLOPT_HEADER => true,
	CURLOPT_RETURNTRANSFER => true,
	CURLOPT_ENCODING => "",
	CURLOPT_MAXREDIRS => 10,
	CURLOPT_TIMEOUT => 30,
	CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	CURLOPT_CUSTOMREQUEST => "GET",
	CURLOPT_HTTPHEADER => array(
	  "content-type: application/json"
	),
    ));

    $response = curl_exec($curl);
    $httpcode = curl_getinfo($curl);
    $err = curl_error($curl);

    curl_close($curl);

    if ($err) {
      return false;
    } else {
      return array('header_info' => $httpcode, 'body' => $response);
    }
    
}

function call_api_post($url, $input){
    $input = json_encode($input);
    $curl = curl_init();
    curl_setopt_array($curl, array(
	CURLOPT_URL => $url,
	CURLOPT_RETURNTRANSFER => true,
	CURLOPT_ENCODING => "",
	CURLOPT_MAXREDIRS => 10,
	CURLOPT_TIMEOUT => 30,
	CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	CURLOPT_CUSTOMREQUEST => "POST",
	CURLOPT_POSTFIELDS => $input,
	CURLOPT_HTTPHEADER => array(
	  "Content-Type: application/json"
	),
    ));
    
    $response = curl_exec($curl);
    $httpcode = curl_getinfo($curl);
    $err = curl_error($curl);

    curl_close($curl);


    if ($err) {
      return false;
    } else {
      return array('header_info' => $httpcode, 'body' => $response);
    }
    
}

function call_api_put($url, $input){
    $input = json_encode($input);
    $curl = curl_init();
    curl_setopt_array($curl, array(
	CURLOPT_URL => $url,
	CURLOPT_RETURNTRANSFER => true,
	CURLOPT_ENCODING => "",
	CURLOPT_MAXREDIRS => 10,
	CURLOPT_TIMEOUT => 30,
	CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	CURLOPT_CUSTOMREQUEST => "PUT",
	CURLOPT_POSTFIELDS => $input,
	CURLOPT_HTTPHEADER => array(
	  "Content-Type: application/json"
	),
    ));
    
    $response = curl_exec($curl);
    $httpcode = curl_getinfo($curl);
    $err = curl_error($curl);

    curl_close($curl);


    if ($err) {
      return false;
    } else {
      return array('header_info' => $httpcode, 'body' => $response);
    }
    
}

/*curl*/
function curl_post($url, $input, $auth=null, $header = array("Content-Type: application/json")){
    
    $curl = curl_init();
    $config = array(
	CURLOPT_URL => $url,
	CURLOPT_RETURNTRANSFER => true,
	CURLOPT_ENCODING => "",
	CURLOPT_MAXREDIRS => 10,
	CURLOPT_TIMEOUT => 30,
	CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	CURLOPT_CUSTOMREQUEST => "POST",
	CURLOPT_POSTFIELDS => $input,
	CURLOPT_HTTPHEADER => $header,
    );
    
    if($auth){
	$config[CURLOPT_USERPWD] = $auth['username'].":".$auth['password'];
    }
    
    curl_setopt_array($curl, $config);
    
    $response = curl_exec($curl);
    $httpcode = curl_getinfo($curl);
    $err = curl_error($curl);

    curl_close($curl);


    if ($err) {
      return false;
    } else {
      return array('header_info' => $httpcode, 'body' => $response);
    }
    
}

function curl_get($url, $input, $auth=null, $header = array("Content-Type: application/json")){
    
    $curl = curl_init();
    
    $input = '?'.http_build_query($input);
    
//     echo http_build_query($input);
    $config = array(
	CURLOPT_URL => $url.$input,
	CURLOPT_RETURNTRANSFER => true,
	CURLOPT_ENCODING => "",
	CURLOPT_MAXREDIRS => 10,
	CURLOPT_TIMEOUT => 30,
	CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	CURLOPT_CUSTOMREQUEST => "GET",
	
	CURLOPT_HTTPHEADER => $header,
    );
    
    if($auth){
	$config[CURLOPT_USERPWD] = $auth['username'].":".$auth['password'];
    }
    
    curl_setopt_array($curl, $config);
    
    $response = curl_exec($curl);
    $httpcode = curl_getinfo($curl);
    $err = curl_error($curl);

    curl_close($curl);


    if ($err) {
      return false;
    } else {
      return array('header_info' => $httpcode, 'body' => $response);
    }
}
/*./curl*/
function check_date_format($date){
    if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$date)){
        return true;
    }else{
        return false;
    }
}
function check_request($param, $variable, $optional=null){
    //echo $param;
    
    if($param){
	//print_r($param);
	
	foreach($variable as $row){
	    if(!isset($param[$row]))
		return false;
	}
	/*
	if(count($param) > count($variable)){
	    //ada variable optional
	    
	    if($optional){
		foreach($optional as $row){
		    if(!isset($param[$row]))
			return false;
		}
	    }else
		return false;
	}
	*/
	return $param;
	
    }else{
	return false;
    }
}

/*
  md5 dengan salt key
*/
function md5_salt($text){
    $CI =& get_instance();
    return md5($text.':'.$CI->config->item('encryption_key'));
}

/*
  sha1 dengan salt key
*/
function sha1_salt($text){
    $CI =& get_instance();
    return sha1($text.':'.$CI->config->item('encryption_key'));
}

/*
  check token
*/
function check_token($token=null, $allow_role=array()){
    $CI =& get_instance();
    
    $allow = false;
    
    if($allow_role && $token){
	$api_url = $CI->config->item('connect_url').'/oauth/check_token?access_token='.$token;
	$response = call_api_get($api_url);
	
	if($response['header_info']['http_code'] == 200){
	    
	    $response_body = json_decode($response['body'], true);
	    $user_id = $response_body['user_id'];
	    
	    
	    foreach($allow_role as $row){
		if(in_array($row, $response_body['role'])){
		    $allow = true; break;
		}
	    }
	    
	}
	
    }
//     print_r($allow_role);
    if(!$allow){
	$CI =& get_instance();
	redirect($CI->config->item('connect_url').'/authorize?response_type=code&client_id='.$CI->config->item('client_id').'&state='.base64_encode(current_url()));
    }
}

function generate_token(){
    return sha1_salt(time());
}

function redirect_login(){
    $CI =& get_instance();
    $user_login = $CI->session->userdata('user_login');
    
    if(isset($user_login['token'])){
	$query = $CI->user_db->get_by_token($user_login['token']);
    
	if($query){
	    $user = $query[0];
	    
	    if(count($user['role']) == 1){
		//role yg cuma 1
		if(in_array('admin',$user['role']))
		    redirect('admin');
	    }else{
		//role isinya lebih dari satu
	    }
	}else{
	    //redirect('login');
	}
    }else{
	//redirect('login');
    }
    
}

/*
  check token facebook
*/

function check_token_facebook($token_fb){
    $result = false;
    
    $param = http_build_query(array(
	'access_token' => $token_fb
    ));
    
    $url = 'https://graph.facebook.com/v2.0/me?'.$param;
    
    if($content = json_decode(file_get_contents($url), true)){
	if(isset($content['error'])){
	    return false;
	}else{
	    return true;
	}
    }
    
    return $result;
}

function get_friendlist_facebook_use_app($token_fb){
    
    $result = false;
    
    $param = http_build_query(array(
	'access_token' => $token_fb,
	'offset' => 0
    ));
    $url = 'https://graph.facebook.com/v2.0/me/friends?'.$param;
    
    if($content = json_decode(file_get_contents($url), true)){
	if(isset($content['error'])){
	    $result = false;
	}else{
	    /*
		format output
		array(
		    array([name] => <name>, [id] => <facebook_id)
		)
	    */
	    //print_r ($content['data']);
	    $result = $content;
	}
    }
    
    return $result;
}

/*
  check token twitter
*/

function check_token_twitter($token_twitter, $token_secret_twitter){
    $CI =& get_instance();
    $result = false;

    $settings = array(
	'oauth_access_token' => $token_twitter,
	'oauth_access_token_secret' => $token_secret_twitter,
	'consumer_key' => $CI->config->item('consumer_key'),
	'consumer_secret' => $CI->config->item('consumer_secret')
    );
    
    //print_r($settings);
    $url = 'https://api.twitter.com/1.1/account/verify_credentials.json';
    $getfield = '';
    $requestMethod = 'GET';
    $twitter = new TwitterAPIExchange($settings);
    $content = json_decode($twitter->buildOauth($url, $requestMethod)
		->performRequest(), true);
    
    if(isset($content['error']) || isset($content['errors'])){
	$result = false;
    }else{
	/*
	    format output
	    array(
		array([name] => <name>, [id] => <facebook_id)
	    )
	*/
	//print_r ($content['data']);
	$result = $content;
    }
    return $result;
}

function get_friendlist_twitter($token_twitter, $token_secret_twitter){
    $output = array();
    $result = false;
    $CI =& get_instance();
    
    if($user = check_token_twitter($token_twitter, $token_secret_twitter)){
	$screen_name = $user['screen_name'];
	$settings = array(
	    'oauth_access_token' => $token_twitter,
	    'oauth_access_token_secret' => $token_secret_twitter,
	    'consumer_key' => $CI->config->item('consumer_key'),
	    'consumer_secret' => $CI->config->item('consumer_secret')
	);
	
	$cursor = -1;
	
	do{
	    
	    $url = 'https://api.twitter.com/1.1/friends/ids.json';
	    $getfield = '?cursor='.$cursor.'&amp;screen_name='.$screen_name.'&amp;count=5000';
	    $requestMethod = 'GET';
	    $twitter = new TwitterAPIExchange($settings);
	    $content = '';
	    $content = $twitter->setGetfield($getfield)
				->buildOauth($url, $requestMethod)
				->performRequest();
	    
	    if($content = json_decode($content, true)){
		//$result = $content; break;
		if(isset($content['error']) || isset($content['errors'])){
		    $result = false;
		    break;
		}else{
		    
		    $output = array_merge($output,$content['ids']);
		    $result = $output;
		    $cursor = $content['next_cursor_str'];
		}
	    }
	}while($cursor > 0);
    }else{
	$return = false;
    }

    return $result;
}

function get_namefriendlist_twitter($token_twitter, $token_secret_twitter){
    $output = array();
    $result = false;
    $CI =& get_instance();
    
    if($user = check_token_twitter($token_twitter, $token_secret_twitter)){
	$screen_name = $user['screen_name'];
	$settings = array(
	    'oauth_access_token' => $token_twitter,
	    'oauth_access_token_secret' => $token_secret_twitter,
	    'consumer_key' => $CI->config->item('consumer_key'),
	    'consumer_secret' => $CI->config->item('consumer_secret')
	);
	
	$cursor = -1;
	
	do{
	    
	    $url = 'https://api.twitter.com/1.1/friends/list.json';
	    $getfield = '?cursor='.$cursor.'&amp;screen_name='.$screen_name.'&amp;skip_status=true&amp;include_user_entities=false&amp;count=200';
	    $requestMethod = 'GET';
	    $twitter = new TwitterAPIExchange($settings);
	    $content = '';
	    $content = $twitter->setGetfield($getfield)
				->buildOauth($url, $requestMethod)
				->performRequest();
	    
	    if($content = json_decode($content, true)){
		//$result = $content; break;
		if(isset($content['error']) || isset($content['errors'])){
		    $result = false;
		    break;
		}else{
		    
		    $output = array_merge($output,$content['users']);
		    $result = $output;
		    $cursor = $content['next_cursor_str'];
		    //print_r($content);
		    //echo $cursor.' '; 
		}
	    }
	}while($cursor > 0);
    }else{
	$return = false;
    }
    //print_r($result);
    return $result;
}

function get_namefriendlist_limit_twitter($token_twitter, $token_secret_twitter, &$cursor=null){
    $output = array();
    $result = false;
    $CI =& get_instance();
    
    if($user = check_token_twitter($token_twitter, $token_secret_twitter)){
	$screen_name = $user['screen_name'];
	$settings = array(
	    'oauth_access_token' => $token_twitter,
	    'oauth_access_token_secret' => $token_secret_twitter,
	    'consumer_key' => $CI->config->item('consumer_key'),
	    'consumer_secret' => $CI->config->item('consumer_secret')
	);
	
	if($cursor==null)
	    $cursor = -1;
	
	$url = 'https://api.twitter.com/1.1/friends/list.json';
	$getfield = '?cursor='.$cursor.'&amp;screen_name='.$screen_name.'&amp;skip_status=true&amp;include_user_entities=false&amp;count=100';
	$requestMethod = 'GET';
	$twitter = new TwitterAPIExchange($settings);
	$content = '';
	$content = $twitter->setGetfield($getfield)
			    ->buildOauth($url, $requestMethod)
			    ->performRequest();
	
	if($content = json_decode($content, true)){
	    //$result = $content; break;
	    if(isset($content['error']) || isset($content['errors'])){
		$result = false;
		break;
	    }else{
		
		$output = array_merge($output,$content['users']);
		$result = $output;
		$cursor = $content['next_cursor_str'];
		//print_r($content);
		//echo $cursor.' '; 
	    }
	}
	
    }else{
	$return = false;
    }
    //print_r($result);
    return $result;
}

function time_to_day($time){
    return floor((time()-$time)/(60*60*24));
}

function get_hashtag($text){
    
    preg_match_all("/(#\w+)/", $text, $matches);
    /*
    if($matches[0])
	return '"'.implode('", "', $matches[0]).'"';
    else
	return false;
    */
    return $matches[0];
}

function get_random_code($length=4){
    $an = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    $su = strlen($an) - 1;
    
    $string = '';
    
    for($i=0; $i<$length; $i++){
	$string .= substr($an, rand(0, $su), 1);
    }
    
    return $string;
}

function config_kcfinder($data = array()){
    $CI =& get_instance();
    if($data){
	
	if (!file_exists(getcwd().'/media/upload/users/'.$data['user_id'].'/')) {
	    mkdir(getcwd().'/media/upload/users/'.$data['user_id'], 0777);
// 	    echo "The directory $dirname was successfully created.";
// 	    exit;
	}
	
	$_SESSION['KCFINDER'] = array(
	    'disabled' => false,
	    'uploadURL' => $CI->config->item('upload')."/users/".$data['user_id'],
	    'uploadDir' => $CI->config->item('upload_dir')."/users/".$data['user_id'],
	    'access' => array(

	      'files' => array(
		  'upload' => true,
		  'delete' => true,
		  'copy'   => true,
		  'move'   => true,
		  'rename' => true
	      ),

	      'dirs' => array(
		  'create' => true,
		  'delete' => true,
		  'rename' => true
	      )
	  )
	);
    }
}

function callback_submit(){
    $CI =& get_instance();
    $notif = ($CI->session->flashdata('notif') ? $CI->session->flashdata('notif') : $CI->session->userdata('notif') );
    
    if($notif){
	echo '
	    <div class="note '.($notif['status'] ? 'note-success' : 'note-danger').' note-shadow">
		    <button class="close" data-close="alert"></button>
		    <p>'.$notif['msg'].'</p>
	    </div>
	';
    
	$CI->session->unset_userdata('notif');
    }
}

function callback_submit_home(){
    $CI =& get_instance();
    $notif = ($CI->session->flashdata('notif') ? $CI->session->flashdata('notif') : $CI->session->userdata('notif') );
    if($notif){
	echo '
	    <div class="alert '.($notif['status'] ? 'alert-success' : 'alert-danger').' alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<p>'.$notif['msg'].'</p>
	    </div>
	';
	
	$CI->session->unset_userdata('notif');
    }
}

function url_exist($url=null){
   $result = false;
   if($url){
      $headers=@get_headers($url);
      $result = stripos($headers[0],"200 OK")? true:false;
   }
   
   return $result;
}

function check_verified_institute(){
    $CI =& get_instance();
    if($CI->connect_auth->get_me()['verified_institute'] != 1){
	redirect('lpp/disabled');
    }
  
}

// by character
function ellipse($str, $n_chars, $crop_str = '...')
{
    $buff=strip_tags($str);
    if(strlen($buff) > $n_chars) {
	    $cut_index=strpos($buff,' ',$n_chars);
	    $buff=substr($buff,0,($cut_index===false? $n_chars: $cut_index+1)).$crop_str;
    }
    return $buff;
}

function str_month($int_month){
    $mons = array(1 => "Jan", 2 => "Feb", 3 => "Mar", 4 => "Apr", 5 => "May", 6 => "Jun", 7 => "Jul", 8 => "Aug", 9 => "Sep", 10 => "Oct", 11 => "Nov", 12 => "Dec");
    
    return $mons[$int_month];
}