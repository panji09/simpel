<?php
    $page = $this->uri->segment(2);
    $page1 = $this->uri->segment(3);
?>
<!-- Being Page Title -->
<div class="container">
    <div class="page-title clearfix">
        <div class="row">
            <div class="col-md-12">
                <h6><a href="<?= site_url();?>">Home</a></h6>
                <h6><span class="page-active">Pengajar</span></h6>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-md-12" id='container-news'>
            <div class="list-event-item" data-created=''>
                
                <div class="regulatory-container">
			<div>
				<h3 class="inline">Daftar Pengajar</h3>
				<div class="pull-right">
					<form class="form-inline">
					  <div class="form-group">
					    <input type="text" class="form-control" id="" placeholder="nama pengajar">
					  </div>
					  
					  <button type="submit" class="btn btn-primary">Cari</button>
					</form>
				</div>
			</div>
			<hr />
			
			<?php if($page == 'chart'):?>
			
			
			<div class="row" style="margin-top: 20px">
				<div class="col-md-12">
					<div class="content-dashboard">
						<div id="container" style="min-width: 310px; height: 800px; margin: 0 auto"></div>

						<table id="datatable" class="table table-striped">
							<thead>
								<tr>
									<th>Provinsi</th>
									<th>Jumlah</th>
									<th>Detail</th>
									
								</tr>
							</thead>
							
							<tbody>
								<?php foreach($list_name_province as $province_name => $instructor_sum):?>
								
								<tr>
									<th><?=$province_name?></th>
									<td><?=$instructor_sum['count']?></td>
									<td><a href='<?=site_url('instructor/province/'.$instructor_sum['id'])?>' class="btn btn-default">Detail</a></td>
									
								</tr>
								
								<?php endforeach;?>
								
							</tbody>
						</table>
					</div>
				</div>
				
				
			</div>
			
			<?php endif;?>
			
			<?php if($page == 'province'):?>
			
			<div class="row instructor">
				<ul class="list-unstyled">
					<?php
					    $response = call_api_get($this->config->item('connect_api_url').'/user/all_narasumber/?office_province='.$page1);
// 					    print_r($response); exit();
					    $response_body = json_decode($response['body'], true);
					    if($response['header_info']['http_code'] == 200)
					    foreach($response_body['data'] as $row):
					?>
					<li class="col-md-3">
						<div class="thumbnail">
							<img alt="" src="<?=(isset($row['photo']) && $row['photo'] != '' ? $row['photo'] : 'http://placehold.it/252x252?text=no+image')?>">
							<h3>
								<strong><?=ellipse($row['fullname'], 15)?></strong> 
								<small><?=ellipse($row['institution'], 70)?></small>
								<small><?=ellipse($row['work_unit'], 50)?></small>
								<small><?=$row['office_district']['name']?>, <?=$row['office_province']['name']?></small>
								
							</h3>
						</div>
					</li>
					<?php endforeach;?>
					
				</ul>            
			</div>
			
			<?php endif;?>
                </div> <!-- /.box-content-inner -->
            
            </div> <!-- /.list-event-item -->
		</div>
    </div> <!-- /.row -->
    
    <?php
    /*
    <div class="row">
        <div class="col-md-12">
            <div class="load-more-btn">
                <a href="#">Lanjutkan</a>
            </div>
        </div> <!-- /.col-md-12 -->
    </div> <!-- /.row -->
    */
    ?>
	
</div> <!-- /.container -->
