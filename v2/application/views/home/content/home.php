 <!-- SLIDER -->
<div class="container">
    <div class="row">
        <div class="col-md-12">
			<!-- MAIN SLIDESHOW -->
            <div class="main-slideshow">
                <div class="flexslider" style="height: 437px">
                    <ul class="slides">
			<?php
			    //info_slider
			    $query =  $content = array();
			    
			    $query = $this->static_pages_db->get('info_slider');
			    if($query)
				$content = $query[0];
			    
			?>
			
			<?php if($content):?>
			<?php for($i=0; $i<count($content['title']); $i++):?>
			<?php if(isset($content['cover']) && $content['cover'][$i]):?>
			<li>
                            <a href="<?=$content['url'][$i]?>"><img src="<?=$content['cover'][$i]?>" alt="<?=$content['title'][$i]?>"/></a>
                            <div class="slider-caption">
                                <h2><a href="<?=$content['url'][$i]?>"><?=$content['title'][$i]?></a></h2>
                            </div>
                        </li>
                        <?php endif;?>
                        <?php endfor;?>
                        <?php endif;?>
                        
                    </ul> <!-- /.slides -->
                </div> <!-- /.flexslider -->
            </div> <!-- /.main-slideshow -->
        </div> <!-- /.col-md-12 -->
    </div>
</div>
<!-- END / SLIDER -->

<!-- SEARCH CONTENT -->
<div id="after-slider" class="after-slider section">
    <div class="container">
        <div class="col-md-12">
		    <div class="awe-color bg-color-1"></div>
		    <div class="after-slider-bg-2"></div>
			
	        <div class="after-slider-content tb">
	            <div class="inner tb-cell">
					
	                <h4>Cari Pelatihan</h4>
	                <div class="course-keyword">
	                    <input type="text" placeholder="keyword Pelatihan">
	                </div>
	                <div class="mc-select-wrap">
	                    <div class="mc-select">
	                        <select class="select" name="" id="all-categories">
	                            <option value="" selected>Jenis Pelatihan</option>
	                            <option value="">Pelatihan Dasar</option>
	                        </select>
	                    </div>
	                </div>
	                <div class="mc-select-wrap">
	                    <div class="mc-select">
	                        <select class="select" name="" id="beginner-level">
	                            <option value="" selected>Provinsi</option>
	                            <option value="">Yogyakarta</option>
	                            <option value="">Ambon</option>
	                        </select>
	                    </div>
	                </div>
	            </div>
	            <div class="tb-cell text-right">
	                <div class="form-actions">
	                    <input type="submit" value="Cari Pelatihan" class="mc-btn btn-style-search">
	                </div>
	            </div>
	        </div>
		</div>

    </div>
</div>
<!-- END / SEARCH CONTENT -->

<!-- AFTER SLIDER -->
<div class="container">
    <div class="row">
      <div class="col-lg-4">
		  <div class="grid-event-item">
		  	 <div class="well well-lg counter">
		  	 	<h4>Jumlah Peserta</h4>
				<h3>12000</h3>
		  	 </div>
		  </div>
      </div> <!-- /.col-md-4 -->
	   
       <div class="col-lg-4">
           <div class="grid-event-item">
		  	 <div class="well well-lg counter">
		  	 	<h4>Jumlah Pengajar</h4>
				<h3>200</h3>
		  	 </div>
           </div> <!-- /.grid-event-item -->
       </div> <!-- /.col-md-4 -->
	   
       <div class="col-lg-4">
           <div class="grid-event-item">
 		  	 <div class="well well-lg counter">
		  	 	<h4>Jumlah LPP</h4>
				<h3>100</h3>
 		  	 </div>
           </div> <!-- /.grid-event-item -->
       </div> <!-- /.col-md-4 -->
       
	    
       
    </div> <!-- /.col-md-12 -->
</div>
<!-- END / AFTER SLIDER -->


<div class="container">
    <div class="row">
		
        <!-- Here begin Main Content -->
        <div class="col-md-12 home-course">
			
			<h3>Pelatihan Terkini</h3>
            <div class="row">

                <div class="col-xs-6 col-sm-4 col-md-4">
                    <div class="grid-event-item">
                        <div class="grid-event-header">
                            <span class="event-place small-text"><i class="fa fa-globe"></i>Cibinong</span>
                            <span class="event-date small-text pull-right"><i class="fa fa-calendar-o"></i>January 08, 2014</span>
                        </div>
                        <div class="box-content-inner">
                            <h5 class="event-title"><a href="event-single.html">Pelatihan Tingkat Dasar</a></h5>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quis, cum, quidem aut natus odit deleniti placeat quia est quibusdam hic. Quod, minima, excepturi eum repellat tempora...</p>
							<button type="button" class="btn btn-primary btn-block">Daftar</button>
                        </div>
                    </div> <!-- /.grid-event-item -->
                </div> <!-- /.col-md-4 -->

               <div class="col-xs-6 col-sm-4 col-md-4">
                   <div class="grid-event-item">
                       <div class="grid-event-header">
                           <span class="event-place small-text"><i class="fa fa-globe"></i>Cibinong</span>
                           <span class="event-date small-text pull-right"><i class="fa fa-calendar-o"></i>January 08, 2014</span>
                       </div>
                       <div class="box-content-inner">
                           <h5 class="event-title"><a href="event-single.html">Pelatihan Tingkat Dasar</a></h5>
                           <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quis, cum, quidem aut natus odit deleniti placeat quia est quibusdam hic. Quod, minima, excepturi eum repellat tempora...</p>
						<button type="button" class="btn btn-primary btn-block">Daftar</button>
                       </div>
                   </div> <!-- /.grid-event-item -->
               </div> <!-- /.col-md-4 -->

                <div class="col-xs-6 col-sm-4 col-md-4">
                    <div class="grid-event-item">
                        <div class="grid-event-header">
                            <span class="event-place small-text"><i class="fa fa-globe"></i>Cibinong</span>
                            <span class="event-date small-text pull-right"><i class="fa fa-calendar-o"></i>January 08, 2014</span>
                        </div>
                        <div class="box-content-inner">
                            <h5 class="event-title"><a href="event-single.html">Pelatihan Tingkat Dasar</a></h5>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quis, cum, quidem aut natus odit deleniti placeat quia est quibusdam hic. Quod, minima, excepturi eum repellat tempora...</p>
							<button type="button" class="btn btn-primary btn-block">Daftar</button>
                        </div>
                    </div> <!-- /.grid-event-item -->
                </div> <!-- /.col-md-4 -->
               <div class="col-xs-6 col-sm-4 col-md-4">
                   <div class="grid-event-item">
                       <div class="grid-event-header">
                           <span class="event-place small-text"><i class="fa fa-globe"></i>Cibinong</span>
                           <span class="event-date small-text pull-right"><i class="fa fa-calendar-o"></i>January 08, 2014</span>
                       </div>
                       <div class="box-content-inner">
                           <h5 class="event-title"><a href="event-single.html">Pelatihan Tingkat Dasar</a></h5>
                           <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quis, cum, quidem aut natus odit deleniti placeat quia est quibusdam hic. Quod, minima, excepturi eum repellat tempora...</p>
						<button type="button" class="btn btn-primary btn-block">Daftar</button>
                       </div>
                   </div> <!-- /.grid-event-item -->
               </div> <!-- /.col-md-4 -->
               <div class="col-xs-6 col-sm-4 col-md-4">
                   <div class="grid-event-item">
                       <div class="grid-event-header">
                           <span class="event-place small-text"><i class="fa fa-globe"></i>Cibinong</span>
                           <span class="event-date small-text pull-right"><i class="fa fa-calendar-o"></i>January 08, 2014</span>
                       </div>
                       <div class="box-content-inner">
                           <h5 class="event-title"><a href="event-single.html">Pelatihan Tingkat Dasar</a></h5>
                           <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quis, cum, quidem aut natus odit deleniti placeat quia est quibusdam hic. Quod, minima, excepturi eum repellat tempora... </p>
						<button type="button" class="btn btn-primary btn-block">Daftar</button>
                       </div>
                   </div> <!-- /.grid-event-item -->
               </div> <!-- /.col-md-4 -->

                <div class="col-xs-6 col-sm-4 col-md-4">
                    <div class="grid-event-item">
                        <div class="grid-event-header">
                            <span class="event-place small-text"><i class="fa fa-globe"></i>Cibinong</span>
                            <span class="event-date small-text pull-right"><i class="fa fa-calendar-o"></i>January 08, 2014</span>
                        </div>
                        <div class="box-content-inner">
                            <h5 class="event-title"><a href="event-single.html">Pelatihan Tingkat Dasar</a></h5>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quis, cum, quidem aut natus odit deleniti placeat quia est quibusdam hic. Quod, minima, excepturi eum repellat tempora...</p>
							<button type="button" class="btn btn-primary btn-block">Daftar</button>
                        </div>
                    </div> <!-- /.grid-event-item -->
                </div> <!-- /.col-md-4 -->

            </div> <!-- /.row -->

            <div class="row">
                <div class="col-md-12">
                    <div class="load-more-btn">
                        <a href="#">Click here to load more events</a>
                    </div>
                </div> <!-- /.col-md-12 -->
            </div> <!-- /.row -->

        </div> <!-- /.col-md-8 -->

    </div> <!-- /.row -->
</div> <!-- /.container -->

