<?php
    
    $query = $content = array();
    $query = $this->dynamic_pages_db->get($content_id);
    if($query)
	$content = $query[0];
    
?>
<!-- Being Page Title -->
<div class="container">
    <div class="page-title clearfix">
        <div class="row">
            <div class="col-md-12">
                <h6><a href="<?= site_url();?>">Home</a></h6>
                <h6><a href="<?= site_url('event');?>">List Agenda</a></h6>
                <h6><span class="page-active"><?=$content['title']?></span></h6>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <!-- Here begin Main Content -->
        <div class="col-md-8">
            <div class="row">
                <div class="col-md-12">
                    <div class="event-container clearfix">												
						<div class="col-md-4 left-single-event">
								<img class="img-responsive"  src="<?=(isset($content['cover']) && $content['cover'] ? $content['cover'] : $this->config->item('home_img').'/event-default.jpg')?>" alt="">		
								<div class="event-contact">
                                <h4>Waktu</h4>
                                <ul>
                                    <li><?= tgl_indo(date("Y-m-j",$content['created']));?></li>
                                </ul>
                                <h4>Lokasi</h4>
                                <ul>
                                    <li><?=$content['location_name']?></li>
                                </ul>								
                            </div>
						</div>						
						<div class="col-md-8 right-single-event">
                            <h2 class="event-title"><?=$content['title']?></h2> 
                           		<?=$content['content']?>
							<div class="google-map-canvas" id="map-canvas" style="height: 210px;"></div>
							<!-- Go to www.addthis.com/dashboard to customize your tools -->
								<div class="addthis_sharing_toolbox"></div>
						</div>						
						<?php /*
                        <div class="left-event-content">
                            <img class="img-responsive"  src="http://placehold.it/225x240" alt="">
                            <div class="event-contact">
                                <h4>Waktu</h4>
                                <ul>
                                    <li>Friday 22 November - Friday 24 January 2014</li>
                                </ul>

                                <h4>Pembicara</h4>
                                <ul>
                                    <li>Prof Abdul Yunus</li>
                                </ul>

                                <h4>Lokasi</h4>
                                <ul>
                                    <li>Bogor</li>
                                </ul>

                            </div>

							<div class="google-map-canvas" id="map-canvas" style="height: 210px;"></div>
                        </div> <!-- /.left-event-content -->
						
                        <div class="right-event-content">
                            <h2 class="event-title">Public and Patient Involvement in Health Research</h2> 
                            <p>During this inaugural lecture Professor Amanda Burls considers the state of public and patient involvement in shared decision making and health research. Professor Burls will discuss the activities of the Network to Amanda-Burls-NuffieldSupport Understanding of Health Research and ThinkWell, a not-for-profit organisation set up to help the public understand health information so they can make.</p>
                            <p>Amanda Burls is a public health physician. She founded and directs ThinkWell, a novel internet-based research programme, which aims to help the public understand health information so they can make informed health decisions and also set up and participate in research studies.</p>
                            <p>In 2011 she co-organised a Conference on Enhancing Public Understanding of Health Research, which resulted in the formation of the International Network for Enhancing Understanding of Health Research.</p>
                            <!-- <div class="google-map-canvas" id="map-canvas" style="height: 210px;"></div> -->
                        </div> <!-- /.right-event-content -->
                    
						*/?>
						</div> <!-- /.event-container -->
                </div>
            </div> <!-- /.row -->
        </div> <!-- /.col-md-8 -->

        <!-- Here begin Sidebar -->
        <div class="col-md-4">
	    <?php $this->load->view('home/inc/widget/widget_news')?>
            
            <?php $this->load->view('home/inc/widget/widget_gallery')?>
        </div> <!-- /.col-md-4 -->


    </div> <!-- /.row -->
</div> <!-- /.container -->
