<?php
$page1 = $this->uri->segment(3);
?>
<!-- Being Page Title -->
<div class="container">
	<div class="page-title clearfix">
		<div class="row">
			<div class="col-md-12">
				<h6><a href="<?= site_url();?>">Home</a></h6>
				<h6><a href="<?= site_url('instructor');?>">Pengajar</a></h6>
				<?php
				$query = file_get_contents($this->config->item('connect_api_url').'/location/province');
				$province = array();

				if($query){
					$province = json_decode($query,true);
				}

				$province_name = '';
				foreach($province as $row){
					if($row['id'] == $page1){
						$province_name = $row['name']; break;
					}
				}
				?>
				<h6><span class="page-active"><?=$province_name?></span></h6>

			</div>
		</div>
	</div>
</div>

<div class="container">
	<div class="row">
		<div class="col-md-12" id='container-news'>
			<div class="list-event-item" data-created=''>

				<div class="regulatory-container">
					<div>
						<h3 class="inline">Daftar Pengajar</h3>
						<div class="pull-right">
							<form class="form-inline" action='<?=site_url('instructor/search')?>' method='get'>
								<div class="form-group">
									<input name='search_name' type="text" class="form-control" id="" placeholder="nama pengajar">
								</div>

								<button type="submit" class="btn btn-primary">Cari</button>
							</form>
						</div>
					</div>
					<hr />

					<div class="row instructor">
						<ul class="list-unstyled">
							<?php
							$response = call_api_get($this->config->item('connect_api_url').'/user/all_narasumber/?office_province='.$page1);
							// 					    print_r($response); exit();
							$response_body = json_decode($response['body'], true);
							if($response['header_info']['http_code'] == 200)
							foreach($response_body['data'] as $row):
								?>
								<li class="col-md-3">
									<div class="thumbnail">
										<a href='<?=site_url('instructor/detail/'.$row['user_id'])?>'>
											<img class="img-rounded" alt="" src="<?=(isset($row['photo']) && $row['photo'] != '' ? $row['photo'] : 'http://placehold.it/252x252?text=no+image')?>">
										</a>
										<h3>
											<a href='<?=site_url('instructor/detail/'.$row['user_id'])?>'>
												<strong><?=ellipse($row['fullname'], 15)?></strong> 
											</a>
											<small><?=ellipse($row['institution'], 70)?></small>
											<small><?=ellipse($row['work_unit'], 50)?></small>
											<small><?=$row['office_district']['name']?>, <?=$row['office_province']['name']?></small>

										</h3>
									</div>
								</li>
							<?php endforeach;?>

						</ul>
					</div>

					<!-- Pagination -->
					<?php
					/*
					 *template jangan di hapus
					<div class="pull-right">
						<nav>
						  <ul class="pagination">
						    <li class="disabled">
						      <a href="#" aria-label="Previous">
						        <span aria-hidden="true">&laquo;</span>
						      </a>
						    </li>
						    <li class="active"><a href="#">1</a></li>
						    <li><a href="#">2</a></li>
						    <li><a href="#">3</a></li>
						    <li><a href="#">4</a></li>
						    <li><a href="#">5</a></li>
						    <li>
						      <a href="#" aria-label="Next">
						        <span aria-hidden="true">&raquo;</span>
						      </a>
						    </li>
						  </ul>
						</nav>
					</div>
					*/
					?>
					<div class="clearfix">&nbsp;</div>
				</div> <!-- /.box-content-inner -->
			</div> <!-- /.list-event-item -->
			
		</div>
	</div> <!-- /.row -->
</div> <!-- /.container -->

