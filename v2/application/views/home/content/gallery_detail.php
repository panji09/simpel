<?php
    $query = $content = array();
    $query = $this->dynamic_pages_db->get($content_id);
    if($query)
	$content = $query[0];
?>
<!-- Being Page Title -->
<div class="container">
    <div class="page-title clearfix">
        <div class="row">
            <div class="col-md-12">
               	<h6><a href="<?= site_url();?>">Home</a></h6>
               	<h6><a href="<?= site_url('gallery');?>">Galeri</a></h6>
			 	<h6><span class="page-active"><?=$content['title']?></span></h6>
            </div>
        </div>
    </div>
</div>


<div class="container">
    <div class="row">

        <div class="col-md-3">
            <div class="widget-main">
                <div class="widget-main-title">
                    <h4 class="widget-title">Album Lainnya</h4>
                </div>
                <div class="widget-inner">
					
					<ul class="list-unstyled album-detail">
					<?php
					    $query = $this->dynamic_pages_db->get_all(array('page_id'=>'gallery','published'=>1), $limit=10);

					    if($query)
					    foreach($query as $row):
							if($row['_id']->{'$id'} != $content_id):	
					?>
						<li>
							<div class="gallery-item fit-scale" style="background-image:url('<?=$row['photo'][0]?>')">
								<div class="photo-content">
									<a href="#" class="photo-over">&nbsp;</a>
									<div class="photo-overlay">
										<h4 class="gallery-title">
											<a href="<?=site_url('gallery/'.$row['_id']->{'$id'})?>"><?=$row['title']?></a>
										</h4>			
									</div>
								</div>	            
							</div>
						</li>
						
					<?php endif; endforeach;?>
						
						
					</ul>
										
                </div> <!-- /.widget-inner -->
            </div> <!-- /.widget-main -->
        </div> <!-- /.col-md-3 -->

        <div class="col-md-9">
			<div class="row" style="margin-top: 10px;">
				<div class="progress-bar"></div>
				<div role="main" >

					<ul id="container" class="tiles-wrap animated">
						<!-- These are our grid blocks -->
						<?php if(isset($content['photo'])): for($i=0; $i < count($content['photo']); $i++):?>
							<li class="kosong mix"  data-cat="<?=$i?>">
								<a class="fancybox" title='<?=$content['title_photo'][$i]?>' data-fancybox-group="gallery2" href="<?=$content['photo'][$i]?>">
									<img src="<?=$content['photo'][$i]?>" alt=""/>
								</a>
								<p><?=$content['title_photo'][$i]?></p>
							</li>
						<?php endfor; endif;?>
					</ul>

				</div>
			</div> <!-- /.row -->
        </div> <!-- /.col-md-9 -->

    </div> <!-- /.row -->
    
</div> <!-- /.container -->
