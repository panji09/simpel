<?php
    $page1 = $this->uri->segment(3);
    

    $response = call_api_get($this->config->item('connect_api_url').'/user/all_narasumber/?id='.$page1);
// 					    print_r($response);
//exit();
    $response_body = json_decode($response['body'], true);
    if($response['header_info']['http_code'] == 200){
	$row = $response_body['data'][0];
    }
	else
	{
		redirect('instructor/chart');
	}
	
	
				      
?>
<!-- Being Page Title -->
<div class="container">
    <div class="page-title clearfix">
        <div class="row">
            <div class="col-md-12">
                <h6><a href="<?= site_url();?>">Home</a></h6>
                <h6><a href="<?= site_url('instructor');?>">Pengajar</a></h6>
                
                <h6><span class="page-active">Detail Pengajar</span></h6>
                
            </div>
        </div>
    </div>
</div>

    <div class="container">
        <div class="row">

            <!-- Here begin Sidebar -->
            <div class="col-md-3">
                <div class="event-container clearfix detail-profile thumbnail">
                    
		    <img class="img-rounded" alt="" src="<?=(isset($row['photo']) ? $row['photo'] : '')?>">
							
                    <div class="event-contact">
                        <h3><strong><?=ellipse($row['fullname'], 15)?></strong></h3>
                        <ul>
                            <li><?=ellipse($row['institution'], 70)?></li>
                            <li><?=ellipse($row['work_unit'], 50)?></li>
                            <li><?=$row['office_district']['name']?>, <?=$row['office_province']['name']?></li>
                        </ul>
						
						<ul class="footer-media-icons">
                            <li><a href="<?=(isset($row['link_fb']) && $row['link_fb'] !='' ? 'target="_blank" '.$row['link_fb'] : '#')?>" class="fa fa-facebook"></a></li>
                            <li><a href="<?=(isset($row['link_twitter']) && $row['link_twitter'] !='' ? 'target="_blank" '.$row['link_twitter'] : '#')?>" class="fa fa-twitter"></a></li>
                            <li><a href="<?=(isset($row['link_web']) && $row['link_web'] !='' ? 'target="_blank" '.$row['link_web'] : '#')?>" class="fa fa-external-link"></a></li>
                            
                        </ul>
                    </div>
					
					
                </div>
            </div> <!-- /.col-md-4 -->


            <!-- Here begin Main Content -->
            <div class="col-md-9">
                <div class="row">
                    <div class="col-md-12">
                        <div class="event-container clearfix">
                            <!-- Nav tabs -->
                            <ul id="tabs" class="nav nav-tabs" data-tabs="tabs">
                                <li class="active"><a href="#section-1" data-toggle="tab">Data Pendidikan dan Kepegawaian</a></li>
                                <li><a href="#section-2" data-toggle="tab">Data Lembaga</a></li>
                            </ul>
                            <div id="my-tab-content" class="tab-content">
                                <div class="tab-pane fade in active" id="section-1">
									<div class="form-horizontal" novalidate="novalidate">
										<div class="form-body">
											<h4 class="form-section">Detail Data Pendidikan dan Kepegawaian</h4>
											<div class="form-group">
												<label class="control-label col-md-3">Pendidikan Terakhir
												</label>
												<div class="col-md-4">
													<div class="padding-top-7"><?=$row['last_education']?></div>
												</div>
											</div>
											<div class="form-group">
												<label class="control-label col-md-3">Gelar Akademis
												</label>
												<div class="col-md-4">
													<div class="padding-top-7"><?=$row['academic_degree']?></div>
												</div>
											</div>
											<div class="form-group">
												<label class="control-label col-md-3">Status Kepegawaian
												</label>
												<div class="col-md-4">
													<div class="padding-top-7"><?=$row['employment']?></div>
												</div>
											</div>
											
											<!--<div class="form-group">
												<label class="control-label col-md-3">No.KTP
												</label>
												<div class="col-md-4">
													<div class="padding-top-7">7371071308610001</div>
												</div>
											</div>-->
										</div>
									</div>

                                </div>
                                <div class="tab-pane fade" id="section-2">
									<div class="form-horizontal" novalidate="novalidate">
										<div class="form-body">
											<h4 class="form-section">Detail Data Lembaga</h4>
											<div class="form-group">
												<label class="control-label col-md-3">
													Nama Instansi/Institusi/Perusahaan
												</label>
												<div class="col-md-4">
													<div class="padding-top-7"><?=$row['institution']?></div>
												</div>
											</div>
											<div class="form-group">
												<label class="control-label col-md-3">Satuan Kerja
												</label>
												<div class="col-md-4">
													<div class="padding-top-7"><?=$row['work_unit']?></div>
												</div>
											</div>
											<div class="form-group">
												<label class="control-label col-md-3">Provinsi
												</label>
												<div class="col-md-4">
													<div class="padding-top-7"><?=$row['office_province']['name']?></div>
												</div>
											</div>
											<div class="form-group">
												<label class="control-label col-md-3">Kab/Kota
												</label>
												<div class="col-md-4">
													<div class="padding-top-7"><?=$row['office_district']['name']?></div>
												</div>
											</div>
											<div class="form-group">
												<label class="control-label col-md-3">Alamat
												</label>
												<div class="col-md-4">
													<div class="padding-top-7"><?=$row['office_address']?></div>
												</div>
											</div>
											<div class="form-group">
												<label class="control-label col-md-3">Kode Pos
												</label>
												<div class="col-md-4">
													<div class="padding-top-7"><?=$row['office_zip_code']?></div>
												</div>
											</div>
										</div>
									</div>
									
                            	</div><!-- /.end tab -->
                           
                        </div> <!-- /.event-container -->
                    </div>
                </div> <!-- /.row -->
            </div> <!-- /.col-md-8 -->
    
        </div> <!-- /.row -->
    </div> <!-- /.container -->

 
