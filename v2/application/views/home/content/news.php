<!-- Being Page Title -->
<div class="container">
    <div class="page-title clearfix">
        <div class="row">
            <div class="col-md-12">
                <h6><a href="<?= site_url();?>">Home</a></h6>
                <h6><span class="page-active">Berita</span></h6>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">

        <!-- Here begin Main Content -->
        <div class="col-md-8">
            <div class="row">

                <div class="col-md-12" id='container-news'>
		    <?php
		    $query = $this->dynamic_pages_db->get_all(array('page_id'=>'news', 'published' => 1), $limit=10);
		    
		    if($query):
			foreach($query as $row):
			    
		    ?>
                    <div class="list-event-item" data-created='<?=$row['created']?>'>
                        <div class="box-content-inner clearfix">
                            <?php if($row['cover']!=''):?>
                            <div class="list-event-thumb">
                                <a href="<?=site_url('news/'.$row['_id']->{'$id'})?>">
                                    <img src="<?=$row['cover']?>" alt="">
                                </a>
                            </div>
                            <?php endif;?>
                            <div class="list-event-header">
                                <span class="event-date small-text"><i class="fa fa-calendar-o"></i><?php
								$date = date("Y-m-j, h:i a",$row['created']);
										$date_ = explode(',',$date);
										echo $date_ = tgl_indo($date_[0]);
										
										//echo ', '.$date = date("h:i a",$row['created']);
										echo ', '.$date = date("H:i",$row['created']);
								?></span>
                                <div class="view-details"><a href="<?=site_url('news/'.$row['_id']->{'$id'})?>" class="lightBtn">Lihat Lengkap</a></div>
                            </div>
                            <h5 class="event-title"><a href="<?=site_url('news/'.$row['_id']->{'$id'})?>"><?=$row['title']?></a></h5>
                            <?=$row['content_short']?>
                        </div> <!-- /.box-content-inner -->
                    </div> <!-- /.list-event-item -->
			
                    <?php endforeach;?>
                    <?php endif;?>
		</div>
            </div> <!-- /.row -->

            <div class="row">
                <div class="col-md-12">
                    <div class="load-more-btn">
                        <a id='loadmore_news'>Lanjutkan</a>
                    </div>
                </div> <!-- /.col-md-12 -->
            </div> <!-- /.row -->

        </div> <!-- /.col-md-8 -->

        <!-- Here begin Sidebar -->
        <div class="col-md-4">

            <?php $this->load->view('home/inc/widget/widget_gallery')?>

        </div> <!-- /.col-md-4 -->

    </div> <!-- /.row -->
</div> <!-- /.container -->
