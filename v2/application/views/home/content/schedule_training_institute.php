<?php
	    $query = $content = array();
	    $query = $this->config_training_db->get($training_name_id);
	    if($query):
	$content = $query[0];
?>
<!-- Being Page Title -->
<div class="container">
    <div class="page-title clearfix">
        <div class="row">
            <div class="col-md-12">
                <h6><a href="<?= site_url();?>">Home</a></h6>
                <h6><a href="<?= site_url('schedule');?>">Pelatihan</a></h6>
                <h6><span class="page-active">Jadwal</span></h6>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        
	<!-- list training-->
	
	<div class='col-md-12'>
	    <div class="row">
                <div class="col-md-12">
                    <div class="widget-main">
                        <div class="widget-inner">
                            <?php $this->load->view('home/inc/widget/search_training')?>
                        </div> <!-- /.widget-inner -->
                    </div> <!-- /.widget-main -->
                </div> <!-- /.col-md-12 -->
            </div> <!-- /.row -->
	    
            <div class="row">
                <div class="col-md-12" id='container-news'>
            <div class="list-event-item" data-created=''>
                <div class="regulatory-container">
					<h3>Jadwal Pelatihan PBJ</h3>
					
					<hr />
					
					<table class="table table-striped table-hover" id="schedule_lpp_table">
						<thead>
							<tr>
								<th>Lembaga Pelatihan</th>
								<th>Alamat</th>
								<th>Kuota Peserta</th>
								<th>Tanggal</th>
								<th>Status Pendaftaran</th>
								<th>Status Pelatihan</th>
								<th>Action</th>
								
								
							</tr>
						</thead>
						<tbody>
							<?php
								$param['published'] = 1;
								$input = $this->input->get();
							    if(isset($input['query']) && $input['query']){
										$param['search_lpp'] = $input['query'];
								}
								
								if(isset($input['province']) && $input['province']){
										$param['training_location_province'] = $input['province'];
								}
								
								if(isset($input['type_training']) && $input['type_training']){
										$param['training_name_id'] = $input['type_training'];
								}
								
								$query = $this->training_db->get_all($param);
							    
								
								//if(isset($input['province'])){
								//		$param['search_lpp'] = $$input['query'];
								//}
								//
								//if(isset($input['type_training'])){
								//		$param['search_lpp'] = $$input['query'];
								//}
								
							    if($query)
							    foreach($query as $row):
							?>
							<tr>
								<td><?=$row['user_submission']['training_institute']?></td>
								<td><?=$row['training_location_address']?>, <?=$row['training_location_province']['name']?> - <?=$row['training_location_district']['name']?></td>
								<td><?=$row['participant_amount']?></td>
								
								<?php
										$range_date = explode(' - ', $row['training_date']);
										$start_date = explode('-', $range_date[0]);
										//print_r($start_date);
										$end_training = mktime(0, 0, 0, $start_date[1], $start_date[2], $start_date[0]);
									
			                    ?>
			                    
								<td data-order="<?=$end_training?>">
								<?=tgl_indo($range_date[0])?> - <?=tgl_indo($range_date[1])?>
								</td>
								<td>
								    <span class="label label-info"><?=$row['type_registration_participant']['name']?></span>
								</td>
								
								
								<td>
										<?php
										$range_date = explode(' - ', $row['training_date']);
										
										//echo $range_date[0];
										//ambil tanggal berakhir
										$start_date = explode('-', $range_date[1]);
										//print_r($start_date);
										$end_training = mktime(0, 0, 0, intval($start_date[1]), intval($start_date[2]), intval($start_date[0])) + (60 * 60 * 24);
										
										if(time() < $end_training):
												
										?>
												<span class="label label-info">Tersedia</span>
										<?php else:?>
												<span class="label label-danger">Berakhir</span>
										<?php endif;?>
								    
								</td>
								<td><button type='button' class='btn btn-primary btn-xs' onclick='location.href="<?=site_url('training/index/'.$row['_id']->{'$id'})?>"'>Detail</button> </td>
							</tr>
							<?php endforeach;?>

						</tbody>
					</table>
					
					
                </div> <!-- /.box-content-inner -->
            </div> <!-- /.list-event-item -->
		</div>
		
            </div> <!-- /.row -->
            
            
	</div>
	
	<!-- ./list training-->
    </div> <!-- /.row -->
</div> <!-- /.container -->

<!-- Modal -->
<div class="modal fade" id="trainingRequirement" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Masukkan Syarat Pelatihan</h4>
			</div>
			<div class="modal-body">
				<div class="form-horizontal">
					<div class="form-group">
						<label class="control-label col-md-4">Textfield</label>
						<div class="col-md-6">
							 <input type="text" class="form-control" id="" placeholder="Textfield">
						</div>
					</div>
		
					<div class="form-group">
						<label class="control-label col-md-4">Checkbox</label>
						<div class="col-md-5">
						    <div class="checkbox">
					           <label>
					             <input type="checkbox"> Pilih
					           </label>
					         </div>
						</div>
					</div>
	
					<div class="form-group">
						<label class="control-label col-md-4">Textarea</label>
						<div class="col-md-7">
							<textarea class="form-control" rows="3"></textarea>
						</div>
					</div>
	
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
				<button type="button" class="btn btn-primary">Simpan</button>
			</div>
		</div>
	</div>
</div>
 
<?php endif;?>