<!-- Being Page Title -->
<div class="container">
    <div class="page-title clearfix">
        <div class="row">
            <div class="col-md-12">
                <h6><a href="<?= site_url();?>">Home</a></h6>
                <h6><span class="page-active">Agenda</span></h6>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">

        <!-- Here begin Main Content -->
        <div class="col-md-8">
            <div class="row">
				
			    <?php
			    $query = $this->dynamic_pages_db->get_all(array('page_id'=>'event'), $limit=6);
	    
			    if($query):
				foreach($query as $row):
				    if($row['published']):
			    ?>
				
                <div class="col-md-6">
                    <div class="grid-event-item">
                        <div class="grid-event-header">
                            <span class="event-place small-text"><i class="fa fa-globe"></i><?=$row['location_name']?></span>
                            <span class="event-date small-text pull-right"><i class="fa fa-calendar-o"></i><?=tgl_indo($row['date'])?></span>
                        </div>
                        <div class="box-content-inner">
                            <h5 class="event-title"><a href="<?=site_url('home/pages/event_single/'.$row['_id']->{'$id'})?>"><?=$row['title']?></a></h5>
                            <p><?=$row['content_short']?><a href="<?=site_url('home/pages/event_single/'.$row['_id']->{'$id'})?>">View Details &rarr;</a></p>
                        </div>
                    </div> <!-- /.grid-event-item -->
                </div> <!-- /.col-md-6 -->
				
				<?php endif;?>
	                    <?php endforeach;?>
	                    <?php endif;?>
								
            </div> <!-- /.row -->

            <div class="row">
                <div class="col-md-12">
                    <div class="load-more-btn">
                        <a href="#">Lanjutkan</a>
                    </div>
                </div> <!-- /.col-md-12 -->
            </div> <!-- /.row -->

        </div> <!-- /.col-md-8 -->

        <!-- Here begin Sidebar -->
        <div class="col-md-4">

            <?php $this->load->view('home/inc/widget/widget_news')?>
			
            <?php $this->load->view('home/inc/widget/widget_gallery')?>

        </div> <!-- /.col-md-4 -->


    </div> <!-- /.row -->
</div> <!-- /.container -->
