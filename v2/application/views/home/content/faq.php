    <!-- Being Page Title -->
    <div class="container">
        <div class="page-title clearfix">
            <div class="row">
                <div class="col-md-12">
                    <h6><a href="<?= site_url();?>">Home</a></h6>
                    <h6><span class="page-active">FAQ</span></h6>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                
                <div class="widget-main">
                    <div class="widget-inner">
                        
                        <h4>FAQ</h4>
						
			<div class="panel-group" id="accordion">
			    <?php
				$query = $this->static_pages_db->get('faq');
				$content = array();
				if($query):
				    $content = $query[0];
				    if($content['published']):
					for($i=0; $i < count($content['question']); $i++):
			    ?>
                         <div class="panel panel-default">
                           <div class="panel-heading">
                               <a data-toggle="collapse" data-parent="#accordion" href="#collapse_<?=$i?>">
                                 <?=$content['question'][$i]?>
                               </a>
                           </div>
                           <div id="collapse_<?=$i?>" class="panel-collapse collapse <?=$i==0 ? 'in' : ''?>">
                             <div class="panel-body">
                               <?=$content['answer'][$i]?>
                             </div>
                           </div>
                         </div>
				<?php endfor;?>
			    <?php endif;?>
                         <?php endif;?>
						
                    </div> <!-- /.widget-inner -->
                </div> <!-- /.widget-main -->

            </div> <!-- /.col-md-12 -->
        </div> <!-- /.row -->
    </div> <!-- /.container -->
