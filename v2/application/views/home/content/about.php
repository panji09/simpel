<?php
$query = $this->static_pages_db->get('about');
$content = array();
if($query){
    $content = $query[0];
}
?>
    <!-- Being Page Title -->
    <div class="container">
        <div class="page-title clearfix">
            <div class="row">
                <div class="col-md-12">
                    <h6><a href="<?= site_url();?>">Home</a></h6>
                    <h6><span class="page-active">Tentang</span></h6>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                
                <div class="widget-main">
                    <div class="widget-inner">
                    <?=($content && $content['published'] ? $content['content'] : '')?>
                    </div> <!-- /.widget-inner -->
                </div> <!-- /.widget-main -->

            </div> <!-- /.col-md-12 -->
        </div> <!-- /.row -->
    </div> <!-- /.container -->
