<!-- Being Page Title -->
<div class="container">
    <div class="page-title clearfix">
        <div class="row">
            <div class="col-md-12">
                <h6><a href="index.html">Home</a></h6>
                <h6><span class="page-active">LPP</span></h6>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-md-12" id='container-news'>
            <div class="list-event-item" data-created=''>
                <div class="regulatory-container">
					<h3>Daftar Lembaga Pelaksana Pelatihan</h3>
					
					<hr />
					
					<table class="table table-striped table-hover" id="lpp_table">
						<thead>
							<tr>
								<th>Lembaga Pelaksana Pelatihan</th>
								<th>Alamat</th>
								<th>Email</th>
								<th>Telp</th>
								<th>Status</th>
							</tr>
						</thead>
						<tbody>
							<?php
							    $response = call_api_get($this->config->item('connect_api_url').'/user/all_lpp');
							    $response_body = json_decode($response['body'], true);
// 							    print_r($response);
							    if($response['header_info']['http_code'] == 200)
							    foreach($response_body['data'] as $row):
							?>
							
							
						        <tr>
						          <td><?=$row['training_institute']?></td>
						          <td><?=$row['office_address'].' '.$row['office_district']['name'].' - '.$row['office_province']['name']?></td>
						          <td><?=$row['email']?></td>
						          <td><?=$row['office_phone']?></td>
						          
						          <?php if($row['verified_institute']):?>
							      
							      <?php if($row['accreditation_level'] == 'terdaftar'):?>
								  <td><span class="label label-info"> <?=strtoupper($row['accreditation_level'])?></span></td>
							      <?php else:?>
								  <td><span class="label label-success">Akreditasi <?=strtoupper($row['accreditation_level'])?></span></td>
							      <?php endif;?>
							      
						          <?php else:?>
							      <td><span class="label label-default">Verifikasi</span></td>
						          <?php endif;?>
						          
						        </tr>
						        
						        <?php endforeach;?>
						        
							
							

						</tbody>
					</table>
					
					
                </div> <!-- /.box-content-inner -->
            </div> <!-- /.list-event-item -->
		</div>
		
    </div> <!-- /.row -->
</div> <!-- /.container -->
