 <!-- SLIDER -->
<div class="container">
    <div class="row">
        <div class="col-md-8 home-course">
			<!-- MAIN SLIDESHOW -->
            <div class="main-slideshow">
                <div class="flexslider">
                    <ul class="slides">
			<?php
			    //info_slider
			    $query =  $content = array();
			    
			    $query = $this->static_pages_db->get('info_slider');
			    if($query)
				$content = $query[0];
			    
			?>
			
			<?php if($content):?>
			<?php for($i=0; $i<count($content['title']); $i++):?>
			<?php if(isset($content['cover']) && $content['cover'][$i]):?>
			<li>
                            <a href="<?=$content['url'][$i]?>">
								<img src="<?=$content['cover'][$i]?>" alt="<?=$content['title'][$i]?>" style="height: 392px"/></a>
                            <div class="slider-caption">
                                <h2><a href="<?=$content['url'][$i]?>"><?=$content['title'][$i]?></a></h2>
                            </div>
                        </li>
                        <?php endif;?>
                        <?php endfor;?>
                        <?php endif;?>
                        
                    </ul> <!-- /.slides -->
                </div> <!-- /.flexslider -->
            </div> <!-- /.main-slideshow -->
      
			<h3>Pelatihan Terkini</h3>
			<div class="row">

			  	<div class="col-xs-6 col-sm-6 col-md-6">
			     <div class="grid-event-item">
			         <div class="grid-event-header">
			             <span class="event-place small-text"><i class="fa fa-globe"></i>Cibinong</span>
			             <span class="event-date small-text pull-right"><i class="fa fa-calendar-o"></i>January 08, 2014</span>
			         </div>
			         <div class="box-content-inner">
			             <h5 class="event-title"><a href="event-single.html">Pelatihan Tingkat Dasar</a></h5>
			             <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quis, cum, quidem aut natus odit deleniti placeat quia est quibusdam hic. Quod, minima, excepturi eum repellat tempora...</p>
					<button type="button" class="btn btn-primary btn-block">Daftar</button>
			         </div>
			     </div> <!-- /.grid-event-item -->
			 </div> <!-- /.col-md-4 -->
			 	<div class="col-xs-6 col-sm-6 col-md-6">
			      <div class="grid-event-item">
			          <div class="grid-event-header">
			              <span class="event-place small-text"><i class="fa fa-globe"></i>Cibinong</span>
			              <span class="event-date small-text pull-right"><i class="fa fa-calendar-o"></i>January 08, 2014</span>
			          </div>
			          <div class="box-content-inner">
			              <h5 class="event-title"><a href="event-single.html">Pelatihan Tingkat Dasar</a></h5>
			              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quis, cum, quidem aut natus odit deleniti placeat quia est quibusdam hic. Quod, minima, excepturi eum repellat tempora...</p>
						<button type="button" class="btn btn-primary btn-block">Daftar</button>
			          </div>
			      </div> <!-- /.grid-event-item -->
			  </div> <!-- /.col-md-4 -->
			 	<div class="col-xs-6 col-sm-6 col-md-6">
			     <div class="grid-event-item">
			         <div class="grid-event-header">
			             <span class="event-place small-text"><i class="fa fa-globe"></i>Cibinong</span>
			             <span class="event-date small-text pull-right"><i class="fa fa-calendar-o"></i>January 08, 2014</span>
			         </div>
			         <div class="box-content-inner">
			             <h5 class="event-title"><a href="event-single.html">Pelatihan Tingkat Dasar</a></h5>
			             <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quis, cum, quidem aut natus odit deleniti placeat quia est quibusdam hic. Quod, minima, excepturi eum repellat tempora...</p>
					<button type="button" class="btn btn-primary btn-block">Daftar</button>
			         </div>
			     </div> <!-- /.grid-event-item -->
			 </div> <!-- /.col-md-4 -->
				<div class="col-xs-6 col-sm-6 col-md-6">
			     <div class="grid-event-item">
			         <div class="grid-event-header">
			             <span class="event-place small-text"><i class="fa fa-globe"></i>Cibinong</span>
			             <span class="event-date small-text pull-right"><i class="fa fa-calendar-o"></i>January 08, 2014</span>
			         </div>
			         <div class="box-content-inner">
			             <h5 class="event-title"><a href="event-single.html">Pelatihan Tingkat Dasar</a></h5>
			             <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quis, cum, quidem aut natus odit deleniti placeat quia est quibusdam hic. Quod, minima, excepturi eum repellat tempora... </p>
					<button type="button" class="btn btn-primary btn-block">Daftar</button>
			         </div>
			     </div> <!-- /.grid-event-item -->
			 </div> <!-- /.col-md-4 -->
			
			</div> <!-- /.row -->

			<?php 
			/*
			<div class="row">
			  <div class="col-md-12">
			      <div class="load-more-btn">
			          <a href="#">Click here to load more events</a>
			      </div>
			  </div> <!-- /.col-md-12 -->
			</div> <!-- /.row -->
			
			*/ ?>
			
			<h3 style="margin-top: 30px">Berita dan Event</h3>
            <div class="row">
                
                <!-- Show Latest Blog News -->
                <div class="col-md-6">
                    <div class="widget-main" style="margin-top: 20px">
                        <div class="widget-main-title">
                            <h4 class="widget-title">Berita</h4>
                        </div> <!-- /.widget-main-title -->
                        <div class="widget-inner">
                            <div class="blog-list-post clearfix">
                                <div class="blog-list-thumb">
                                    <a href="blog-single.html"><img src="http://placehold.it/65x65" alt=""></a>
                                </div>
                                <div class="blog-list-details">
                                    <h5 class="blog-list-title"><a href="blog-single.html">LPP Semarang Jalan Lancar</a></h5>
                                    <p class="blog-list-meta small-text"><span><a href="#">12 January 2014</a></span> with <span><a href="#">3 comments</a></span></p>
                                </div>
                            </div> <!-- /.blog-list-post -->
                            <div class="blog-list-post clearfix">
                                <div class="blog-list-thumb">
                                    <a href="blog-single.html"><img src="http://placehold.it/65x65" alt=""></a>
                                </div>
                                <div class="blog-list-details">
                                    <h5 class="blog-list-title"><a href="blog-single.html">LPP Semarang Jalan Lancar</a></h5>
                                    <p class="blog-list-meta small-text"><span><a href="#">12 January 2014</a></span> with <span><a href="#">3 comments</a></span></p>
                                </div>
                            </div> <!-- /.blog-list-post -->
                            <div class="blog-list-post clearfix">
                                <div class="blog-list-thumb">
                                    <a href="blog-single.html"><img src="http://placehold.it/65x65" alt=""></a>
                                </div>
                                <div class="blog-list-details">
                                    <h5 class="blog-list-title"><a href="blog-single.html">LPP Semarang Jalan Lancar</a></h5>
                                    <p class="blog-list-meta small-text"><span><a href="#">12 January 2014</a></span> with <span><a href="#">3 comments</a></span></p>
                                </div>
                            </div> <!-- /.blog-list-post -->
                        </div> <!-- /.widget-inner -->
                    </div> <!-- /.widget-main -->
			    </div> <!-- /col-md-6 -->
                
                <!-- Show Latest Events List -->
                <div class="col-md-6">
		            <div class="widget-main" style="margin-top: 20px">
		                <div class="widget-main-title">
		                    <h4 class="widget-title">LKPP Events</h4>
		                </div> <!-- /.widget-main-title -->
		                <div class="widget-inner">
		                    <div class="event-small-list clearfix">
		                        <div class="calendar-small">
		                            <span class="s-month">Jan</span>
		                            <span class="s-date">24</span>
		                        </div>
		                        <div class="event-small-details">
		                            <h5 class="event-small-title"><a href="event-single.html">Sosialisasi ke Jogjakarta</a></h5>
		                            <p class="event-small-meta small-text">Keraton Auditorium 9:00 AM to 1:00 PM</p>
		                        </div>
		                    </div>
		                    <div class="event-small-list clearfix">
		                        <div class="calendar-small">
		                            <span class="s-month">Jan</span>
		                            <span class="s-date">24</span>
		                        </div>
		                        <div class="event-small-details">
		                            <h5 class="event-small-title"><a href="event-single.html">Halal Bihalal</a></h5>
		                            <p class="event-small-meta small-text">Gedung SMESCO 4:30 PM to 6:00 PM</p>
		                        </div>
		                    </div>
		                    <div class="event-small-list clearfix">
		                        <div class="calendar-small">
		                            <span class="s-month">Jan</span>
		                            <span class="s-date">24</span>
		                        </div>
		                        <div class="event-small-details">
		                            <h5 class="event-small-title"><a href="event-single.html">Pameran Kerajinan</a></h5>
		                            <p class="event-small-meta small-text">JEC 12:00 PM to 1:00 PM</p>
		                        </div>
		                    </div>
		                </div> <!-- /.widget-inner -->
		            </div> <!-- /.widget-main -->
                </div> <!-- /.col-md-6 -->
                
            </div> <!-- /.row -->
			
		  
	  
	    </div> <!-- /.col-md-12 -->
    
		
        <div class="col-md-4">
            <div class="widget-item">
                <div class="request-information">
                    <h4 class="widget-title">Cari Pelatihan</h4>
                    <form class="request-info clearfix"> 
                        <div class="full-row">
                            <label for="cat">Jenis Pelatihan:</label>                
                            <div class="input-select">
                                <select name="cat" id="cat" class="postform">
                                    <option value="1">Pelatihan Dasar</option>
                                    <option value="1">Pelatihan Lanjutan</option>
                                </select>
                            </div> <!-- /.input-select -->  
                        </div> <!-- /.full-row -->

                        <div class="full-row">
                            <label for="cat2">Provinsi:</label>                                               
                            <div class="input-select">
                                <select name="cat2" id="cat2" class="postform">
                                    <option value="1">DKI Jakarta</option>
                                    <option value="2">Jawa Barat</option>
                                    <option value="3">Yogyakarta</option>
                                </select>
                            </div> <!-- /.input-select -->
                        </div> <!-- /.full-row -->

                        <div class="full-row">
                            <label for="yourname">Keyword:</label>
                            <input type="text" id="keyword" name="keyword">
                        </div> <!-- /.full-row -->
                        
                        <div class="full-row">
                            <div class="submit_field">
                                <!-- <span class="small-text pull-left">&nbsp;</span> -->
								<!-- <input type="submit" value="Cari Pelatihan" class="mc-btn btn-style-search"> -->
                                <button class="mc-btn btn-style-search" name="" style="width: 100%">Cari Pelatihan</button>
                            </div> <!-- /.submit-field -->
                        </div> <!-- /.full-row -->

                    </form> <!-- /.request-info -->
                </div> <!-- /.request-information -->
            </div> <!-- /.widget-item -->
			
         	<div class="panel panel-info" style="margin-top: 30px;">
			      <!-- Default panel contents -->
			      <div class="panel-heading"><strong>Daftar LPP</strong></div>

			      <!-- Table -->
			      <table class="table">
				      <tbody>
				        <tr>
				          <td>Lembaga Pendidikan dan Pelatihan Madani (LP2M)</td>
				          <td><span class="label label-success">Akreditasi A</span></td>
				        </tr>
				        <tr>
				          <td>Lembaga Pendidikan dan Pelatihan Madani (LP2M)</td>
				          <td><span class="label label-success">Akreditasi A</span></td>
				        </tr>
				        <tr>
				          <td>Lembaga Pendidikan dan Pelatihan Madani (LP2M)</td>
				          <td><span class="label label-success">Akreditasi A</span></td>
				        </tr>
				        <tr>
				          <td>Lembaga Pendidikan dan Pelatihan Madani (LP2M)</td>
				          <td><span class="label label-success">Akreditasi A</span></td>
				        </tr>
				        <tr>
				          <td>Lembaga Pendidikan dan Pelatihan Madani (LP2M)</td>
				          <td><span class="label label-success">Akreditasi A</span></td>
				        </tr>
				        <tr>
				          <td>Lembaga Pendidikan dan Pelatihan Madani (LP2M)</td>
				          <td><span class="label label-success">Akreditasi A</span></td>
				        </tr>
				        <!-- <tr>
				          <td>Lembaga Pendidikan dan Pelatihan Madani (LP2M)</td>
				          <td><span class="label label-success">Akreditasi A</span></td>
				        </tr>
				        <tr>
				          <td>Lembaga Pendidikan dan Pelatihan Madani (LP2M)</td>
				          <td><span class="label label-success">Akreditasi A</span></td>
				        </tr>
				        <tr>
				          <td>Lembaga Pendidikan dan Pelatihan Madani (LP2M)</td>
				          <td><span class="label label-success">Akreditasi A</span></td>
				        </tr> -->
						
				      </tbody>
			      </table>
				  
				  <p style="padding: 10px 10px 0; border-top: 1px solid #ddd;"><a href="#">Klik disini</a> untuk melakukan Pengajuan Pelatihan</p>
			</div>
			
			
            <?php
			/*
            <div class="widget-main">
                <div class="widget-main-title">
                    <h4 class="widget-title">Berita</h4>
                </div>
                <div class="widget-inner">
                    <div class="prof-list-item clearfix">
                       <div class="prof-thumb">
                            <img src="http://placehold.it/75x75" alt="Profesor Name">
                        </div> <!-- /.prof-thumb -->
                        <div class="prof-details">
                            <h5 class="prof-name-list">LPP Semarang Jalan Lancar</h5>
                            <p class="small-text">Berikut berita terbaru dari LPP semarang yang mengadakan Pelatihan</p>
                        </div> <!-- /.prof-details -->
                    </div> <!-- /.prof-list-item -->
                    <div class="prof-list-item clearfix">
                       <div class="prof-thumb">
                            <img src="http://placehold.it/75x75" alt="Profesor Name">
                        </div> <!-- /.prof-thumb -->
                        <div class="prof-details">
                            <h5 class="prof-name-list">LPP Semarang Jalan Lancar</h5>
                            <p class="small-text">Berikut berita terbaru dari LPP semarang yang mengadakan Pelatihan</p>
                        </div> <!-- /.prof-details -->
                    </div> <!-- /.prof-list-item -->
                    <div class="prof-list-item clearfix">
                       <div class="prof-thumb">
                            <img src="http://placehold.it/75x75" alt="Profesor Name">
                        </div> <!-- /.prof-thumb -->
                        <div class="prof-details">
                            <h5 class="prof-name-list">LPP Semarang Jalan Lancar</h5>
                            <p class="small-text">Berikut berita terbaru dari LPP semarang yang mengadakan Pelatihan</p>
                        </div> <!-- /.prof-details -->
                    </div> <!-- /.prof-list-item -->
                </div> <!-- /.widget-inner -->
            </div> <!-- /.widget-main -->
			
            <div class="widget-main">
                <div class="widget-main-title">
                    <h4 class="widget-title">LKPP Events</h4>
                </div> <!-- /.widget-main-title -->
                <div class="widget-inner">
                    <div class="event-small-list clearfix">
                        <div class="calendar-small">
                            <span class="s-month">Jan</span>
                            <span class="s-date">24</span>
                        </div>
                        <div class="event-small-details">
                            <h5 class="event-small-title"><a href="event-single.html">Sosialisasi ke Jogjakarta</a></h5>
                            <p class="event-small-meta small-text">Keraton Auditorium 9:00 AM to 1:00 PM</p>
                        </div>
                    </div>
                    <div class="event-small-list clearfix">
                        <div class="calendar-small">
                            <span class="s-month">Jan</span>
                            <span class="s-date">24</span>
                        </div>
                        <div class="event-small-details">
                            <h5 class="event-small-title"><a href="event-single.html">Halal Bihalal</a></h5>
                            <p class="event-small-meta small-text">Gedung SMESCO 4:30 PM to 6:00 PM</p>
                        </div>
                    </div>
                    <div class="event-small-list clearfix">
                        <div class="calendar-small">
                            <span class="s-month">Jan</span>
                            <span class="s-date">24</span>
                        </div>
                        <div class="event-small-details">
                            <h5 class="event-small-title"><a href="event-single.html">Pameran Kerajinan</a></h5>
                            <p class="event-small-meta small-text">JEC 12:00 PM to 1:00 PM</p>
                        </div>
                    </div>
                </div> <!-- /.widget-inner -->
            </div> <!-- /.widget-main -->
			
            
			*/
            ?>
            
            
            <div class="widget-main">
                <div class="widget-main-title">
                    <h4 class="widget-title">Our Gallery</h4>
                </div>
                <div class="widget-inner">
                    <div class="gallery-small-thumbs clearfix">
                        <div class="thumb-small-gallery">
                            <a class="fancybox" data-fancybox-group="gallery1" href="images/slide1.jpg" title="Gallery Tittle One">
                                <img src="http://placehold.it/70x70" alt="" />
                            </a>
                        </div>
                        <div class="thumb-small-gallery">
                            <a class="fancybox" data-fancybox-group="gallery1" href="images/slide1.jpg" title="Gallery Tittle Two">
                                <img src="http://placehold.it/70x70" alt="" />
                            </a>
                        </div>
                        <div class="thumb-small-gallery">
                            <a class="fancybox" data-fancybox-group="gallery1" href="images/slide1.jpg">
                                <img src="http://placehold.it/70x70" alt="" />
                            </a>
                        </div>
                        <div class="thumb-small-gallery">
                            <a class="fancybox" data-fancybox-group="gallery1" href="images/slide1.jpg">
                                <img src="http://placehold.it/70x70" alt="" />
                            </a>
                        </div>
                        <div class="thumb-small-gallery">
                            <a class="fancybox" data-fancybox-group="gallery1" href="images/slide1.jpg">
                                <img src="http://placehold.it/70x70" alt="" />
                            </a>
                        </div>
                        <div class="thumb-small-gallery">
                            <a class="fancybox" data-fancybox-group="gallery1" href="images/slide1.jpg">
                                <img src="http://placehold.it/70x70" alt="" />
                            </a>
                        </div>
                        <div class="thumb-small-gallery">
                            <a class="fancybox" data-fancybox-group="gallery1" href="images/slide1.jpg">
                                <img src="http://placehold.it/70x70" alt="" />
                            </a>
                        </div>
                        <div class="thumb-small-gallery">
                            <a class="fancybox" data-fancybox-group="gallery1" href="images/slide1.jpg">
                                <img src="http://placehold.it/70x70" alt="" />
                            </a>
                        </div>
                    </div> <!-- /.galler-small-thumbs -->
                </div> <!-- /.widget-inner -->
            </div> <!-- /.widget-main -->
			
			
         
			
			
            <div class="widget-main">
                <div class="widget-main-title">
                    <h4 class="widget-title">Statistik Pengguna</h4>
                </div> <!-- /.widget-main-title -->
                <div class="widget-inner">
					<ul class="list-inline">
					      <li>
							  <strong>Jumlah Peserta</strong>
							  <br />1233
						  </li>
					      <li>
							  <strong>Jumlah Pengajar</strong>
						  <br />1233
					  		</li>
					      <li>
							  <strong>Jumlah LPP</strong>
						  	<br />1233
						  </li>
					</ul>
                </div> <!-- /.widget-inner -->
            </div> <!-- /.widget-main -->
			
		</div> <!-- /.col-md-4 -->
	</div>
</div>
<!-- END / SLIDER -->

