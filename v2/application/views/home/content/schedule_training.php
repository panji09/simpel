<div class="container">
    <div class="page-title clearfix">
        <div class="row">
            <div class="col-md-12">
                <h6><a href="<?= site_url();?>">Home</a></h6>
				<h6><a href="<?= site_url('schedule');?>">Pelatihan</a></h6>
                <h6><span class="page-active">Program</span></h6>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">

        <div class="col-md-3">
	    
	    <div class="widget-main">
                <div class="widget-main-title">
                    <h4 class="widget-title">Jenis Pelatihan</h4>
                </div>
                <div class="widget-inner">
                    <ul class="mixitup-controls">
			    
			    <li class="filter" data-filter="all">Tampilan Semua</li>
                        <?php
			    $query = $this->type_training_db->get_all(array(
				'published' => 1
			    ));
			    
			    foreach($query as $row):
                        ?>
			    <li class="filter" data-filter="<?=$row['_id']->{'$id'}?>"><?=$row['name']?></li>
                        
                        <?php endforeach;?>
                        
                     </ul>
                </div> <!-- /.widget-inner -->
            </div> <!-- /.widget-main -->
            
            <div class="widget-main">
                <div class="widget-main-title">
                    <h4 class="widget-title">Nama Pelatihan</h4>
                </div>
                <div class="widget-inner">
                    <ul class="mixitup-controls">
                        <?php
			    $query = $this->config_training_db->get_all(array(
				'published' => 1
			    ));
			    
			    foreach($query as $row):
                        ?>
			    <li class="filter" ><?=$row['name']?></li>
                        
                        <?php endforeach;?>
                        
                     </ul>
                </div> <!-- /.widget-inner -->
            </div> <!-- /.widget-main -->

        </div> <!-- /.col-md-3 -->

        <div class="col-md-9">
            <div class="row">
                <div class="col-md-12">
                    <div class="widget-main">
                        <div class="widget-inner">
			    <?php $this->load->view('home/inc/widget/search_training')?>
                        </div> <!-- /.widget-inner -->
                    </div> <!-- /.widget-main -->
                </div> <!-- /.col-md-12 -->
            </div> <!-- /.row -->
	    
            <div class="row">
                <div id="Grid">
                    
                    <?php
			$query = $this->config_training_db->get_all(array('published' => 1));
			
			foreach($query as $row):
		    ?>
		    
                    <div class="col-md-4 mix <?=$row['type_training']['id']?>" >
	                    <div class="grid-event-item">
				
				<div class="box-content-inner" style='height:300px;'>
				    <h5 class="event-title"><a href="<?=site_url('schedule/program/'.$row['_id']->{'$id'})?>"><?=$row['name']?></a><br><small><?=$row['type_training']['name']?></small></h5>
				    
				    <div class='desc' style='margin-top:20px; height:200px'>
				    <?=(isset($row['training_description_short']) ? ellipse($row['training_description_short'],300) : '<p></p>' )?>
				    </div>
				    
				</div>
				<div class="box-content-inner">
				    <button type="button" class="btn btn-primary btn-block" onclick='location.href="<?=site_url('schedule/program/'.$row['_id']->{'$id'})?>"'>Daftar</button>
				</div>
			    </div> <!-- /.grid-event-item -->
                    </div> <!-- /.col-md-4 -->
                    <?php endforeach;?>
                    
                </div> <!-- /#Grid -->
            </div> <!-- /.row -->

            

        </div> <!-- /.col-md-9 -->

    </div> <!-- /.row -->
    
</div> <!-- /.container -->


