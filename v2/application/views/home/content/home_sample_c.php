 <!-- SLIDER -->
<div class="container">
    <div class="row">
        <div class="col-md-12 home-course">
			<!-- MAIN SLIDESHOW -->
            <div class="main-slideshow">
                <div class="flexslider">
                    <ul class="slides">
			<?php
			    //info_slider
			    $query =  $content = array();
			    
			    $query = $this->static_pages_db->get('info_slider');
			    if($query)
				$content = $query[0];
			    
			?>
			
			<?php if($content):?>
			<?php for($i=0; $i<count($content['title']); $i++):?>
			<?php if(isset($content['cover']) && $content['cover'][$i]):?>
			<li>
                            <a href="<?=$content['url'][$i]?>">
								<img src="<?=$content['cover'][$i]?>" alt="<?=$content['title'][$i]?>" style="height: 392px"/></a>
                            <div class="slider-caption">
                                <h2><a href="<?=$content['url'][$i]?>"><?=$content['title'][$i]?></a></h2>
                            </div>
                        </li>
                        <?php endif;?>
                        <?php endfor;?>
                        <?php endif;?>
                        
                    </ul> <!-- /.slides -->
                </div> <!-- /.flexslider -->
            </div> <!-- /.main-slideshow -->
	</div>
</div>
<!-- END / SLIDER -->
</div>
</div>

<!-- News and Events -->
<div class="section" style='padding-top:20px'>
    <div class="container">
	<div class="row">
	    <div class='col-md-4'>
		<div class="panel panel-info">
		  <div class="panel-body" style='min-height:100px'>
		    <?php
		    $query = $this->static_pages_db->get('how_to_submission_accreditation');
		    $content1 = array();
		    if($query){
			$content1 = $query[0];
		    }
		    ?>
		    <?=(isset($content1['content_short']) && $content1['published'] ? $content1['content_short'].' '.anchor(site_url('how_to#section-1'), 'Selengkapnya') : '')?>
		  </div>
		  <div class="panel-footer"><button type='button' class='btn btn-info btn-block' onclick="javascript:location.href='<?=site_url('login/register_institution/'.base64_encode(current_url()))?>'">Pengajuan Akreditasi LPP</button></div>
		</div>
	    </div>
	    
	    <div class='col-md-4'>
		<div class="panel panel-info">
		  <div class="panel-body" style='min-height:100px'>
		    <?php
		    $query = $this->static_pages_db->get('how_to_submission_training_facilitator');
		    $content2 = array();
		    if($query){
			$content2 = $query[0];
		    }
		    ?>
		    <?=(isset($content2['content_short']) && $content2['published'] ? $content2['content_short'].' '.anchor(site_url('how_to#section-2'),'Selengkapnya') : '')?>
		  </div>
		  <div class="panel-footer"><button type='button' class='btn btn-info btn-block' onclick="javascript:location.href='<?=site_url('lpp/management_training/training/add')?>'">Pengajuan Fasilitasi Pelatihan dan Narasumber</button></div>
		</div>
	    </div>
	    
	    <div class='col-md-4'>
		<div class="panel panel-info">
		  <div class="panel-body" style='min-height:100px'>
		    <?php
		    $query = $this->static_pages_db->get('how_participant_registration');
		    $content3 = array();
		    if($query){
			$content3 = $query[0];
		    }
		    ?>
		    <?=(isset($content3['content_short']) && $content3['published'] ? $content3['content_short'].' '.anchor(site_url('how_to#section-3'),'Selengkapnya') : '')?>
		  </div>
		  <div class="panel-footer"><button type='button' class='btn btn-info btn-block' onclick="javascript:location.href='<?=site_url('login/register_participant/'.base64_encode(current_url()))?>'">Pendaftaran Peserta Pelatihan</button></div>
		</div>
	    </div>
	    
	    
	</div>
    </div>
</div>
<!-- SEARCH CONTENT -->
<div id="after-slider" class="after-slider section">
    <div class="container">
        <div class="col-md-12">
		    <div class="awe-color bg-color-1"></div>
		    <div class="after-slider-bg-2"></div>
			
			<form action='<?=site_url('schedule/search')?>' method='get'>
				<div class="after-slider-content tb">
	
						<div class="inner tb-cell">
							
							<h4>Cari Pelatihan</h4>
							<div class="course-keyword">
								<input type="text" name='query' class='form-control' placeholder="keyword Pelatihan">
							</div>
							<div class="mc-select-wrap">
								<div class="mc-select">
									<select class="form-control" name="type_training" id="all-categories">
										<option value="" selected>Jenis Pelatihan</option>
										<?php 
							$query = $this->config_training_db->get_all(array('published' => 1));
							foreach($query as $row):
										?>
										<option value="<?=$row['_id']->{'$id'}?>"><?=$row['name']?></option>
							<?php endforeach;?>
									</select>
								</div>
							</div>
							<div class="mc-select-wrap">
								<div class="mc-select">
									<select class="form-control" name="province" id="beginner-level">
										<option value="" selected>Provinsi</option>
										
							<?php
							$query = file_get_contents($this->config->item('connect_api_url').'/location/province');
							$province = array();
							if($query){
								$province = json_decode($query,true);
							}
							
							if($province)
							foreach($province as $row):
							?>
							<option value='<?=$row['id']?>'><?=$row['name']?></option>
							<?php
							endforeach;
							?>
													  
									</select>
								</div>
							</div>
						</div>
						<div class="tb-cell text-right">
							<div class="form-actions">
								<input type="submit" value="Cari Pelatihan" class="mc-btn btn-style-search">
							</div>
						</div>
					
				</div>
			</form>
		</div>
		
	</div>
</div>
  
  
<!-- Latest Training -->
<div class="section latest-training">
    <div class="container">
		<div class="row">
	        <div class="col-md-12">
				
				<!-- FEATURE -->
				<div class="feature-course">
					<h3 class="title-box">Program Pelatihan</h3> <a href="<?=site_url('schedule')?>" class="all-course mc-btn btn-red">View all</a>
					<div class="row">
					    <div class="feature-slider">
						<?php
						    $query = $this->config_training_db->get_all(array('published' => 1));
						    
						    foreach($query as $row):
						?>
						<div class="grid-event-item">
				
						    <div class="box-content-inner" style='height:300px;'>
							<h5 class="event-title"><a href="<?=site_url('schedule/program/'.$row['_id']->{'$id'})?>"><?=$row['name']?></a><br><small><?=$row['type_training']['name']?></small></h5>
							
							<div class='desc' style='margin-top:20px; height:200px'>
							<?=(isset($row['training_description_short']) ? ellipse($row['training_description_short'],300) : '<p></p>' )?>
							</div>
							
						    </div>
						    <div class="box-content-inner">
							<button type="button" class="btn btn-primary btn-block" onclick='location.href="<?=site_url('schedule/program/'.$row['_id']->{'$id'})?>"'>Daftar</button>
						    </div>
						</div> <!-- /.grid-event-item -->
						<?php endforeach;?>
		                    
										
							
						</div>
					</div>
				</div>
	
			</div>
			
		</div>
	</div>
</div>

<!-- News and Events -->
<div class="section">
    <div class="container">
		<div class="row">
			<div class="col-md-8">
				<h3 class="margin-bottom-0">Publikasi</h3>
	            <div class="row">
                
	                <!-- Show Latest Blog News -->
	                <div class="col-md-6" style="margin-top: -10px">
	                    <?php $this->load->view('home/inc/widget/widget_news')?>
			</div> <!-- /col-md-6 -->
                
	                <!-- Show Latest Events List -->
	                <div class="col-md-6" style="margin-top: -10px">
			            <div class="widget-main">
			                <div class="widget-main-title">
			                    <h4 class="widget-title"><a href='<?=site_url('event')?>'>Agenda</a></h4>
			                </div> <!-- /.widget-main-title -->
			                <div class="widget-inner">
					    <?php
					    $query = $this->dynamic_pages_db->get_all(array('page_id'=>'event','published' => 1), $limit=5);
					    
					    if($query)
					    foreach($query as $row):
					    ?>
			                    <div class="event-small-list clearfix">
			                        <div class="calendar-small">
			                            <?php
							$date = explode('-',$row['date']);
						    ?>
						    <span class="s-month"><?=str_month(intval($date[1]));?></span>
						    <span class="s-date"><?=$date[2]?></span>
			                        </div>
			                        <div class="event-small-details">
			                            <h5 class="event-small-title"><a href="<?=site_url('event/'.$row['_id']->{'$id'})?>"><?=$row['title']?></a></h5>
			                            <p class="event-small-meta small-text"><?=$row['location_name']?> - <?=date("j F Y",strtotime($row['date'])) ?></p>
			                        </div>
			                    </div>
					    <?php endforeach;?>
					    
			                    
			                    
			                </div> <!-- /.widget-inner -->
			            </div> <!-- /.widget-main -->
	                </div> <!-- /.col-md-6 -->
                
	            </div> <!-- /.row -->
				
				
			</div>
			
			<div class="col-md-4">
	            <div class="row">
					<h3>Lembaga Pelaksana Pelatihan</h3>
					
		         	<div class="panel panel-info" style="margin-top: 20px;">
					      <!-- Default panel contents -->
					      <div class="panel-heading"><strong><a href="<?=site_url('institution')?>">Daftar Lembaga Pelaksana Pelatihan</a></strong></div>

					      <!-- Table -->
					      <table class="table">
						      <tbody>
							<?php
							    $response = call_api_get($this->config->item('connect_api_url').'/user/all_lpp_status_verified/5');
							    $response_body = json_decode($response['body'], true);
							    if($response['header_info']['http_code'] == 200)
							    foreach($response_body['data'] as $row):
								if(isset($row['verified_institute']) && in_array($row['verified_institute'], array(1,0))):
							?>
						        <tr>
						          <td><?=$row['training_institute']?></td>
						          <?php if($row['verified_institute']==1):?>
							      
									<?php if($row['accreditation_level'] == 'terdaftar'):?>
									<td><span class="label label-info"> <?=strtoupper($row['accreditation_level'])?></span></td>
									<?php else:?>
									<td><span class="label label-success">Akreditasi <?=strtoupper($row['accreditation_level'])?></span></td>
									<?php endif;?>
									
						          <?php elseif($row['verified_institute']==0):?>
							      <td><span class="label label-default">Verifikasi</span></td>
						          <?php endif;?>
						          
						          
						        </tr>
						        <?php endif;?>
						        <?php endforeach;?>
						
						      </tbody>
					      </table>
				  
						  <p style="padding: 10px 10px 0; border-top: 1px solid #ddd;"><a href="<?=site_url('lpp/management_training/training/add')?>">Klik disini</a> untuk melakukan Pengajuan Pelatihan</p>
					</div>
					
		            
		            <?php $this->load->view('home/inc/widget/widget_gallery')?>
	            </div>
			</div>
		</div>
	</div>
</div>

