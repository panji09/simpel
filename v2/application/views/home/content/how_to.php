
    <!-- Being Page Title -->
    <div class="container">
        <div class="page-title clearfix">
            <div class="row">
                <div class="col-md-12">
                    <h6><a href="<?= site_url();?>">Home</a></h6>
                    <h6><span class="page-active">Cara Penggunaan</span></h6>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                
                <div class="widget-main">
                    <div class="widget-inner">
                        <ul id="tabs" class="nav nav-tabs" data-tabs="tabs">
			    <li class="active"><a href="#section-1" data-toggle="tab">Pengajuan Akreditasi LPP</a></li>
			    <li class=""><a href="#section-2" data-toggle="tab">Pengajuan Fasilitasi Pelatihan dan Narasumber</a></li>
			    <li class=""><a href="#section-3" data-toggle="tab">Pendaftaran Peserta Pelatihan</a></li>
			</ul>
			
			<div id="my-tab-content" class="tab-content">
			    <div class="tab-pane fade active in" id="section-1">
				<?php
				$query = $this->static_pages_db->get('how_to_submission_accreditation');
				$content1 = array();
				if($query){
				    $content1 = $query[0];
				}
				?>
				<?=($content1 && $content1['published'] ? $content1['content'] : '')?>
			    </div>
			    <div class="tab-pane fade" id="section-2">
				<?php
				$query = $this->static_pages_db->get('how_to_submission_training_facilitator');
				$content2 = array();
				if($query){
				    $content2 = $query[0];
				}
				?>
				<?=($content2 && $content2['published'] ? $content2['content'] : '')?>
			    </div>
			    <div class="tab-pane fade" id="section-3">
				<?php
				$query = $this->static_pages_db->get('how_participant_registration');
				$content3 = array();
				if($query){
				    $content3 = $query[0];
				}
				?>
				<?=($content3 && $content3['published'] ? $content3['content'] : '')?>
			    </div>
			</div>
                    </div> <!-- /.widget-inner -->
                </div> <!-- /.widget-main -->

            </div> <!-- /.col-md-12 -->
        </div> <!-- /.row -->
    </div> <!-- /.container -->
