<?php
    //var_dump($meta);
	
    $query = $content = array();
    $query = $this->dynamic_pages_db->get($content_id);
    if($query)
	$content = $query[0];
    
?>
<!-- Being Page Title -->
<div class="container">
    <div class="page-title clearfix">
        <div class="row">
            <div class="col-md-12">
                <h6><a href="<?= site_url();?>">Home</a></h6>
				 <h6><a href="<?= site_url('news');?>">Berita</a></h6>
                <h6><span class="page-active"><?=$content['title']?></span></h6>
            </div>
        </div>
    </div>
</div>

    <div class="container">
        <div class="row">

            <!-- Here begin Main Content -->
            <div class="col-md-8">

                <div class="row">
                    <div class="col-md-12">
                        <div class="blog-post-container">
                            <?php if($content['cover']!=''):?>
                            <div class="blog-post-image">
                                <img src="<?=$content['cover']?>" alt="">
                                 <!-- /.blog-post-meta -->
                            </div> <!-- /.blog-post-image -->
                            <?php endif;?>
                            <div class="blog-post-inner">
								<div class="blog-title">
									<h3 class="blog-post-title"><?=$content['title']?></h3>
                                	<?php
									//date("F j, Y, g:i a",$content['created'])
									$date = date("Y-m-j, h:i a",$content['created']);
										$date_ = explode(',',$date);
										echo $date_ = tgl_indo($date_[0]);
										
										//echo ', '.$date = date("h:i a",$row['created']);
										echo ', '.$date = date("H:i",$content['created']);
									?>
								</div>
                                <?=$content['content']?>
								<!-- Go to www.addthis.com/dashboard to customize your tools -->
								<div class="addthis_sharing_toolbox"></div>
                            </div>
							
							
							
                        </div> <!-- /.blog-post-container -->
                    </div> <!-- /.col-md-12 -->
                </div> <!-- /.row -->
<?php
/*
                <div class="row">
                    <div class="col-md-12">
                        <div class="prev-next-post clearfix">
                            <span class="whiteBtn">
                                <a href="" class="prev"><i class="fa fa-angle-left"></i>Older Post</a>
                            </span>
                            <span class="whiteBtn">
                                <a href="" class="next">Newer Post<i class="fa fa-angle-right"></i></a>
                            </span>
                        </div>
                    </div> <!-- /.col-md-12 -->
                </div> <!-- /.row -->
*/
?>
            </div> <!-- /.col-md-8 -->

            <!-- Here begin Sidebar -->
            <div class="col-md-4">
                
                <?php $this->load->view('home/inc/widget/widget_gallery')?>
            </div> <!-- /.col-md-4 -->
    
        </div> <!-- /.row -->
    </div> <!-- /.container -->
