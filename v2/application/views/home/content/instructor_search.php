<?php
    $page1 = $this->uri->segment(3);
?>
<!-- Being Page Title -->
<div class="container">
    <div class="page-title clearfix">
        <div class="row">
            <div class="col-md-12">
                <h6><a href="#">Home</a></h6>
                <h6><a href="#">Pengajar</a></h6>
                
                <h6><span class="page-active">Cari: <?=$this->input->get('search_name')?></span></h6>
                
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-md-12" id='container-news'>
            <div class="list-event-item" data-created=''>
                
                <div class="regulatory-container">
			<div>
				<h3 class="inline">Daftar Pengajar</h3>
				<div class="pull-right">
					<form class="form-inline" action='<?=site_url('instructor/search')?>' method='get'>
					  <div class="form-group">
					    <input name='search_name' type="text" class="form-control" id="" value='<?=$this->input->get('search_name')?>' placeholder="nama pengajar">
					  </div>
					  
					  <button type="submit" class="btn btn-primary">Cari</button>
					</form>
				</div>
			</div>
			<hr />
			

			
			
			
			<div class="row instructor">
				<ul class="list-unstyled">
					<?php
					    $response = call_api_get($this->config->item('connect_api_url').'/user/all_narasumber/?search_name='.$this->input->get('search_name'));
// 					    print_r($response); exit();
// 					    print_r($response); exit();
					    $response_body = json_decode($response['body'], true);
					    if($response['header_info']['http_code'] == 200)
					    foreach($response_body['data'] as $row):
					?>
					<li class="col-md-3">
						<div class="thumbnail">
							<img class="img-rounded" alt="" src="<?=(isset($row['photo']) && $row['photo'] != '' ? $row['photo'] : 'http://placehold.it/252x252?text=no+image')?>">
							<h3>
								<strong><?=ellipse($row['fullname'], 15)?></strong> 
								<small><?=ellipse($row['institution'], 70)?></small>
								<small><?=ellipse($row['work_unit'], 50)?></small>
								<small><?=$row['office_district']['name']?>, <?=$row['office_province']['name']?></small>
								
							</h3>
						</div>
					</li>
					<?php endforeach;?>
					
				</ul>            
			</div>
			
			
                </div> <!-- /.box-content-inner -->
            
            </div> <!-- /.list-event-item -->
		</div>
    </div> <!-- /.row -->
    
    <?php
    /*
    <div class="row">
        <div class="col-md-12">
            <div class="load-more-btn">
                <a href="#">Lanjutkan</a>
            </div>
        </div> <!-- /.col-md-12 -->
    </div> <!-- /.row -->
    */
    ?>
	
</div> <!-- /.container -->
 
