<!-- Being Page Title -->
<div class="container">
    <div class="page-title clearfix">
        <div class="row">
            <div class="col-md-12">
                <h6><a href="<?= site_url();?>">Home</a></h6>
                <h6><a href="<?= site_url('instructor');?>">Pengajar</a></h6>
                <h6><span class="page-active">Chart</span></h6>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-md-12" id='container-news'>
            <div class="list-event-item" data-created=''>
                
                <div class="regulatory-container">
			<div>
				<h3 class="inline">Daftar Pengajar</h3>
				<div class="pull-right">
					<form class="form-inline" action='<?=site_url('instructor/search')?>' method='get'>
					  <div class="form-group">
					    <input name='search_name' type="text" class="form-control" id="" placeholder="nama pengajar">
					  </div>
					  
					  <button type="submit" class="btn btn-primary">Cari</button>
					</form>
				</div>
			</div>
			<hr />
			
			<div class="row" style="margin-top: 20px">
				<div class="col-md-12">
					<div class="content-dashboard">
						<div id="container" style="height: 800px; margin: 0 auto"></div>
						
						<?php /*
						<table id="datatable" class="table table-striped">
							<thead>
								<tr>
									<th>Provinsi</th>
									<th>Jumlah</th>
									<th>Detail</th>
									
								</tr>
							</thead>
							
							<tbody>
								<?php foreach($list_name_province as $province_name => $instructor_sum):?>
								
								<tr>
									<th><?=$province_name?></th>
									<td><?=$instructor_sum['count']?></td>
									<td><a href='<?=site_url('instructor/province/'.$instructor_sum['id'])?>' class="btn btn-default">Detail</a></td>
									
								</tr>
								
								<?php endforeach;?>
								
							</tbody>
						</table>
						*/?>
					</div>
				</div>
				
				
			</div>
			
			
			
			
                </div> <!-- /.box-content-inner -->
            
            </div> <!-- /.list-event-item -->
		</div>
    </div> <!-- /.row -->
    
    <?php
    /*
    <div class="row">
        <div class="col-md-12">
            <div class="load-more-btn">
                <a href="#">Lanjutkan</a>
            </div>
        </div> <!-- /.col-md-12 -->
    </div> <!-- /.row -->
    */
    ?>
	
</div> <!-- /.container -->
 
