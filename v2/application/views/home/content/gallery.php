<!-- Being Page Title -->
<div class="container">
    <div class="page-title clearfix">
        <div class="row">
            <div class="col-md-12">
                <h6><a href="<?= site_url();?>">Home</a></h6>
                <h6><span class="page-active">Gallery</span></h6>
            </div>
        </div>
    </div>
</div>


<div class="container">
    <div class="row">
        <div class="col-md-12">

            <div class="row" id="container-gallery-album">

				<?php
	    			$query = $this->dynamic_pages_db->get_all(array('page_id'=>'gallery','published'=>1), $limit=10);

	    			if($query)
				foreach($query as $row):
		    				
	    		?>
				<div class="col-xs-12 col-sm-4 col-md-3 gallery-album" data-created='<?=$row['created']?>'>
					<div class="gallery-item fit-scale" style="background-image:url('<?=$row['photo'][0]?>')">
						<div class="photo-content">
							<a href="<?=site_url('gallery/'.$row['_id']->{'$id'})?>" class="photo-over">&nbsp;</a>
							<div class="photo-overlay">
								<h4 class="gallery-title">
									<a href="<?=site_url('gallery/'.$row['_id']->{'$id'})?>"><?=$row['title']?></a>
								</h4>			
							</div>
						</div>	            
					</div>
				</div>
				
			
		    				
				<?php endforeach;?>
	             	
				
            </div> <!-- /.row -->

			<?php
				$total_query = count($query);
				
				// echo '<pre>';
				// var_dump($total_query);
				// echo '</pre>';	
			?>

            <div class="row <?=($total_query >= 10 ? '' : 'kosong')?> ">
                <div class="col-md-12">
                    <div class="load-more-btn">
                        <a id='loadmore_gallery'>Lanjutkan</a>
                    </div>
                </div> <!-- /.col-md-12 -->
            </div> <!-- /.row -->

        </div> <!-- /.col-md-12 -->

    </div> <!-- /.row -->
    
</div> <!-- /.container -->
