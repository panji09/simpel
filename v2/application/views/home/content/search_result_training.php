<!-- Being Page Title -->
<div class="container">
    <div class="page-title clearfix">
        <div class="row">
            <div class="col-md-12">
                <h6><a href="#">Home</a></h6>
                <h6><span class="page-active">Hasil Pencarian</span></h6>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-md-12">
			<div class="regulatory-container">
				<h3>Ditemukan 10 untuk kata Kunci <strong>Pengadaan Dasar</strong></h3>
				
				<div class="margin-top-30">
					<table class="table table-striped table-hover">
						<thead>
							<tr>
								<th>Lembaga Pelatihan</th>
								<th>Alamat</th>
								<th>Kuota Peserta</th>
								
								<th>Status Pendaftaran</th>
								<th>Status Pelatihan</th>
								<th>Action</th>
								
								
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>Direktorat Pelatihan Kompetensi-LKPP</td>
								<td>Jakarta, DKI Jakarta - Kota Jakarta Selatan</td>
								<td>100</td>
								<td>
									<span class="label label-info">Terbuka</span>
								</td>
								<td>
									<span class="label label-info">Tersedia</span>
								</td>
								<td>
									<button type="button" class="btn btn-primary btn-xs">Detail</button>
								</td>
							</tr>
							<tr>
								<td>Direktorat Pelatihan Kompetensi-LKPP</td>
								<td>Jakarta, DKI Jakarta - Kota Jakarta Selatan</td>
								<td>100</td>
								<td>
									<span class="label label-info">Terbuka</span>
								</td>
								<td>
									<span class="label label-info">Tersedia</span>
								</td>
								<td>
									<button type="button" class="btn btn-primary btn-xs">Detail</button>
								</td>
							</tr>
							<tr>
								<td>Direktorat Pelatihan Kompetensi-LKPP</td>
								<td>Jakarta, DKI Jakarta - Kota Jakarta Selatan</td>
								<td>100</td>
								<td>
									<span class="label label-info">Terbuka</span>
								</td>
								<td>
									<span class="label label-info">Tersedia</span>
								</td>
								<td>
									<button type="button" class="btn btn-primary btn-xs">Detail</button>
								</td>
							</tr>
							<tr>
								<td>Direktorat Pelatihan Kompetensi-LKPP</td>
								<td>Jakarta, DKI Jakarta - Kota Jakarta Selatan</td>
								<td>100</td>
								<td>
									<span class="label label-info">Terbuka</span>
								</td>
								<td>
									<span class="label label-info">Tersedia</span>
								</td>
								<td>
									<button type="button" class="btn btn-primary btn-xs">Detail</button>
								</td>
							</tr>
							<tr>
								<td>Direktorat Pelatihan Kompetensi-LKPP</td>
								<td>Jakarta, DKI Jakarta - Kota Jakarta Selatan</td>
								<td>100</td>
								<td>
									<span class="label label-info">Terbuka</span>
								</td>
								<td>
									<span class="label label-info">Tersedia</span>
								</td>
								<td>
									<button type="button" class="btn btn-primary btn-xs">Detail</button>
								</td>
							</tr>
							<tr>
								<td>Direktorat Pelatihan Kompetensi-LKPP</td>
								<td>Jakarta, DKI Jakarta - Kota Jakarta Selatan</td>
								<td>100</td>
								<td>
									<span class="label label-info">Terbuka</span>
								</td>
								<td>
									<span class="label label-info">Tersedia</span>
								</td>
								<td>
									<button type="button" class="btn btn-primary btn-xs">Detail</button>
								</td>
							</tr>
							<tr>
								<td>Direktorat Pelatihan Kompetensi-LKPP</td>
								<td>Jakarta, DKI Jakarta - Kota Jakarta Selatan</td>
								<td>100</td>
								<td>
									<span class="label label-info">Terbuka</span>
								</td>
								<td>
									<span class="label label-info">Tersedia</span>
								</td>
								<td>
									<button type="button" class="btn btn-primary btn-xs">Detail</button>
								</td>
							</tr>
							<tr>
								<td>Direktorat Pelatihan Kompetensi-LKPP</td>
								<td>Jakarta, DKI Jakarta - Kota Jakarta Selatan</td>
								<td>100</td>
								<td>
									<span class="label label-info">Terbuka</span>
								</td>
								<td>
									<span class="label label-info">Tersedia</span>
								</td>
								<td>
									<button type="button" class="btn btn-primary btn-xs">Detail</button>
								</td>
							</tr>
							<tr>
								<td>Direktorat Pelatihan Kompetensi-LKPP</td>
								<td>Jakarta, DKI Jakarta - Kota Jakarta Selatan</td>
								<td>100</td>
								<td>
									<span class="label label-info">Terbuka</span>
								</td>
								<td>
									<span class="label label-info">Tersedia</span>
								</td>
								<td>
									<button type="button" class="btn btn-primary btn-xs">Detail</button>
								</td>
							</tr>
							<tr>
								<td>Direktorat Pelatihan Kompetensi-LKPP</td>
								<td>Jakarta, DKI Jakarta - Kota Jakarta Selatan</td>
								<td>100</td>
								<td>
									<span class="label label-info">Terbuka</span>
								</td>
								<td>
									<span class="label label-info">Tersedia</span>
								</td>
								<td>
									<button type="button" class="btn btn-primary btn-xs">Detail</button>
								</td>
							</tr>
							<tr>
								<td>Direktorat Pelatihan Kompetensi-LKPP</td>
								<td>Jakarta, DKI Jakarta - Kota Jakarta Selatan</td>
								<td>100</td>
								<td>
									<span class="label label-info">Terbuka</span>
								</td>
								<td>
									<span class="label label-info">Tersedia</span>
								</td>
								<td>
									<button type="button" class="btn btn-primary btn-xs">Detail</button>
								</td>
							</tr>
							<tr>
								<td>Direktorat Pelatihan Kompetensi-LKPP</td>
								<td>Jakarta, DKI Jakarta - Kota Jakarta Selatan</td>
								<td>100</td>
								<td>
									<span class="label label-info">Terbuka</span>
								</td>
								<td>
									<span class="label label-info">Tersedia</span>
								</td>
								<td>
									<button type="button" class="btn btn-primary btn-xs">Detail</button>
								</td>
							</tr>
							<tr>
								<td>Direktorat Pelatihan Kompetensi-LKPP</td>
								<td>Jakarta, DKI Jakarta - Kota Jakarta Selatan</td>
								<td>100</td>
								<td>
									<span class="label label-info">Terbuka</span>
								</td>
								<td>
									<span class="label label-info">Tersedia</span>
								</td>
								<td>
									<button type="button" class="btn btn-primary btn-xs">Detail</button>
								</td>
							</tr>
							<tr>
								<td>Direktorat Pelatihan Kompetensi-LKPP</td>
								<td>Jakarta, DKI Jakarta - Kota Jakarta Selatan</td>
								<td>100</td>
								<td>
									<span class="label label-info">Terbuka</span>
								</td>
								<td>
									<span class="label label-info">Tersedia</span>
								</td>
								<td>
									<button type="button" class="btn btn-primary btn-xs">Detail</button>
								</td>
							</tr>
							<tr>
								<td>Direktorat Pelatihan Kompetensi-LKPP</td>
								<td>Jakarta, DKI Jakarta - Kota Jakarta Selatan</td>
								<td>100</td>
								<td>
									<span class="label label-info">Terbuka</span>
								</td>
								<td>
									<span class="label label-info">Tersedia</span>
								</td>
								<td>
									<button type="button" class="btn btn-primary btn-xs">Detail</button>
								</td>
							</tr>
							<tr>
								<td>Direktorat Pelatihan Kompetensi-LKPP</td>
								<td>Jakarta, DKI Jakarta - Kota Jakarta Selatan</td>
								<td>100</td>
								<td>
									<span class="label label-info">Terbuka</span>
								</td>
								<td>
									<span class="label label-info">Tersedia</span>
								</td>
								<td>
									<button type="button" class="btn btn-primary btn-xs">Detail</button>
								</td>
							</tr>

						</tbody>
					</table>
				</div>
			</div>
	    </div> <!-- /.col-md-12 -->
	</div>
	
    <div class="row">
        <div class="col-md-12">
            <div class="load-more-btn">
                <a href="#">Click here to load more</a>
            </div>
        </div> <!-- /.col-md-12 -->
    </div> <!-- /.row -->
</div>
<!-- END / SLIDER -->

