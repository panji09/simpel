<!-- Being Page Title -->
<div class="container">
    <div class="page-title clearfix">
        <div class="row">
            <div class="col-md-12">
                <h6><a href="#">Home</a></h6>
                <h6><span class="page-active">Hasil Pencarian</span></h6>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-md-12">
			<div class="regulatory-container">
				<?php
				    $query = $this->pages_db->get_all(array('search' => $search, 'published' => 1), 20);
// 				    print_r($query);
				    $last_created = 0;
				    if($query):
				    
				    
				?>
				
				<h3>Ditemukan <?=count($query)?> untuk kata Kunci <strong><?=$search?></strong></h3>
				
				<div class="margin-top-30" id='container-search'>
					
					<?php foreach($query as $row): $last_created = $row['created']; ?>
					<div class="search-classic" data-created='<?=$row['created']?>'>
						<h4>
						<a href="<?=site_url($row['page_id'].'/'.$row['_id']->{'$id'})?>">
						<?=$row['title']?> </a>
						</h4>
						<?php
						switch($row['page_id']){
						    case 'news': $page_name = 'Berita'; $link = site_url('news'); break;
						    case 'event': $page_name = 'Agenda'; $link = site_url('event'); break;
						    case 'gallery': $page_name = 'Galeri'; $link = site_url('gallery'); break;
						    
						}
						?>
						<a href="<?=$link?>">
						<?=$page_name?> </a>
						<p>
							 <?=(isset($row['content_short']) ? $row['content_short'] : '')?>
						</p>
					</div>
					<hr>
					<?php endforeach;?>
					
				</div>
				
				<?php endif;?>
			</div>
	    </div> <!-- /.col-md-12 -->
	</div>
	
    <?php
	if($last_created):
	$query = $this->pages_db->get_all(array('created_lt' => intval($last_created), 'search' => $search) );
	
	if(count($query)):
    ?>
    <div class="row">
        <div class="col-md-12">
            <div class="load-more-btn">
                <a id='loadmore_search'>Lanjutkan</a>
            </div>
        </div> <!-- /.col-md-12 -->
    </div> <!-- /.row -->
    <?php
	endif;
	endif;
    ?>
</div>
<!-- END / SLIDER -->

