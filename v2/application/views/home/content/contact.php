<?php
$query = $this->static_pages_db->get('contact');
$content = array();
if($query){
    $content = $query[0];
}
?>
    <!-- Being Page Title -->
    <div class="container">
        <div class="page-title clearfix">
            <div class="row">
                <div class="col-md-12">
                    <h6><a href="<?=site_url('')?>">Home</a></h6>
                    <h6><span class="page-active">Kontak</span></h6>
                </div>
            </div>
        </div>
    </div>

  <div class="container">
      <div class="row">
          <div class="col-md-12">
              
              <div class="widget-main">
                  <div class="widget-inner shortcode-typo">
                      <div class="row">
                          <div class="col-md-7">
		                      <div class="contact-heading">
		                          <h4>Kontak atau feedback</h4>
		                          <p>Kirim feedback atau saran kepada kami dengan mengisi form berikut.</p>
		                      </div>
							  
							  
							  <form class="form-horizontal" style="padding-right: 10%" action='<?=site_url('home/pages/contact_post')?>' method='post'>
  							    <div class="form-group">
  							      <label for="inputEmail3" class="col-sm-2 control-label">Nama</label>
  							      <div class="col-sm-10">
  							        <input type="text" class="form-control" name='name' id="inputEmail3" placeholder="Nama Lengkap">
  							      </div>
  							    </div>
							    <div class="form-group">
							      <label for="inputEmail3" class="col-sm-2 control-label">Email</label>
							      <div class="col-sm-10">
							        <input type="email" class="form-control" name='email' id="inputEmail3" placeholder="Email">
							      </div>
							    </div>
							    <div class="form-group">
							      <label for="inputPassword3" class="col-sm-2 control-label">Pesan</label>
							      <div class="col-sm-10">
							        <textarea class="form-control" rows="3" name='message'></textarea>
							      </div>
							    </div>
							    
							    <div class="form-group">
							      <div class="col-sm-offset-2 col-sm-10">
							        <button type="submit" class="btn btn-primary">Kirim Pesan</button>
							      </div>
							    </div>
							  </form>
							
                          </div>
                          <div class="col-md-5">
							  <h4>Hubungi Kami</h4>
							  
							  <?=($content && $content['published'] ? $content['content'] : '')?>
							 
                          </div>
                      </div>
                      
                  </div> <!-- /.widget-inner -->
              </div> <!-- /.widget-main -->

          </div> <!-- /.col-md-12 -->
      </div> <!-- /.row -->
  </div> <!-- /.container -->
  