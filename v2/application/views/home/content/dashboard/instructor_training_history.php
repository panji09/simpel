<!-- Being Page Title -->
<div class="container">
    <div class="row">
        <div class="col-md-3">
			<!-- Include Dashboard Menu -->
			<?=$this->load->view('home/inc/dashboard/instructor_dashboard_menu')?>
			<!-- End Dashboard Menu -->
        </div>
		
		<div class="col-md-9 dashboard-container">
			<ol class="breadcrumb">
				<li><a href="#">Home</a></li>
			  	<li class="active">History Pelatihan</li>
			</ol>
			
			<div class="content-dashboard">
				<!-- BEGIN PAGE CONTENT-->
				<div class="row">
					<div class="col-md-12">
						<?php callback_submit_home();?>
						<div class="btn-group">
							<a href="<?=site_url('narasumber/history/training_history/add')?>" class="btn btn-default">
							Tambah Pelatihan <i class="fa fa-plus"></i>
							</a>
						</div>						
					</div>
					
					<div class="col-md-12">
						<table class="table table-striped table-bordered table-hover" id="table_instructor_training_history">
							<thead>
								<tr>
									<th>Nama Pelatihan</th>									
									<th>Tanggal Pelatihan</th>
									<th>Lokasi</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<?php
									$filter['user_id'] = $this->connect_auth->get_me()['user_id'];
									$filter['published'] = 1; 
													
								    $query = $this->training_history_db->get_all($filter);
						    
								    if($query):
									foreach($query as $row):
							    
								?>
								<tr class="odd gradeX">
								    <td><?=$row['training_name']?></td>
								    <td><?=$row['training_date']?></td>
								    								   								  
								    <td><?=$row['training_location_address']?></td>
								    <td>
									<div class="btn-group" role="group" aria-label="...">
									</div>
								    </td>
								</tr>
							
								    <?php endforeach;?>
								<?php endif;?>
							</tbody>
						</table>
					</div>
				</div>
				<!-- END PAGE CONTENT-->
			</div>
		</div>
    </div>
</div>

