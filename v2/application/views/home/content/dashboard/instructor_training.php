<!-- Being Page Title -->
<div class="container">
    <div class="row">
        <div class="col-md-3">
			<!-- Include Dashboard Menu -->
			<?=$this->load->view('home/inc/dashboard/instructor_dashboard_menu')?>
			<!-- End Dashboard Menu -->
        </div>
		
		<div class="col-md-9 dashboard-container">
			<ol class="breadcrumb">
				<li><a href="#">Pelatihan</a></li>
			  	<li class="active"><?=$title_page?></li>
			</ol>
			
			<div class="content-dashboard">
				
				
				<!-- BEGIN PAGE CONTENT-->
				<div class="row">
					<div class="col-md-12">
						<?php callback_submit_home();?>
					</div>
					
					<div class="col-md-12">
						<table class="table table-striped table-bordered table-hover" id="table_approval_training_instructor">
							<thead>
								<tr>
									<th>Tanggal</th>
									<th>Tanggal Pelatihan</th>
									<th>Nama Pelatihan</th>
									<th>No Surat Permohonan</th>
									<th>Tanggal Mengajar</th>
									<th>Jumlah JP</th>
									<th>Status</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<?php
						    
								    $query = $this->hour_leasson_db->get_all(array(
									'instructor_id' => $this->connect_auth->get_me()['user_id'],
									'admin_approved' => 1
								    ));
								    if($query):
									foreach($query as $row):
							    
								?>
								<tr class="odd gradeX">
								    <td><time class="timeago" datetime="<?=date('c',$row['created'])?>"><?=date("F j, Y, g:i a",$row['created']) ?></time></td>
								    <td><?=$row['training_log']['training_date']?></td>
								    <td><?=$row['training_log']['type_training']['name'].' / '.$row['training_log']['training_name']['name']?></td>
								    
								    <td><a href='<?=$row['training_log']['mail_submission_upload']?>' target='_blank'><?=$row['training_log']['mail_submission_id']?></a></td>
								    
								    <td><?=implode(', ',$row['submission_date'])?></td>
								    
								    <td>
									<?=$row['hour_leasson']?>
								    </td>
								    
								    <td>
									<?php
									    $verifikasi = array();
									    
									    if(isset($row['instructor_approved']))
									    switch($row['instructor_approved']){
										case 0 : $verifikasi = array('name' => 'Menunggu Persetujuan', 'label' => 'label-warning'); break;
										case 1 : $verifikasi = array('name' => 'Disetujui', 'label' => 'label-success'); break;
										case -1 : $verifikasi = array('name' => 'Klarifikasi', 'label' => 'label-warning'); break;
										case -2 : $verifikasi = array('name' => 'Tolak', 'label' => 'label-danger'); break;
										default : $verifikasi = array('name' => 'Menunggu Persetujuan', 'label' => 'label-warning'); break;
									    }
									?>
									<span class="label label-sm <?=$verifikasi['label']?>"> <?=$verifikasi['name']?> </span>
								    </td>
								    <td>
								    <?php if($this->connect_auth->get_me()['entity']=='narasumber'):?>
									
									<div class="btn-group" role="group" aria-label="...">
									    <button 
									    type="button" 
									    class="btn btn-sm btn-primary review_training_submission" 
									    data-toggle="modal" 
									    data-training_id='<?=$row['training_id']?>' 
									    data-target="#review_training_submission" 
									    data-url-submit="<?=site_url('narasumber/management_training/confirm_post/'.$row['training_id'])?>" 
									    >Review</button>
									</div>
									
								    <?php else:?>
									<div class="btn-group" role="group" aria-label="...">
									    <button 
									    type="button" 
									    class="btn btn-sm btn-primary review_training_submission" 
									    data-toggle="modal" 
									    data-training_id='<?=$row['training_id']?>' 
									    data-target="#review_training_submission" 
									    data-url-submit="<?=site_url('lpp/management_training/training_post/'.$row['training_id'])?>" 
									    >Review</button>
									</div>
									
								    <?php endif;?>
									
									
									
								    </td>
								</tr>
							
								    <?php endforeach;?>
								<?php endif;?>
							</tbody>
						</table>
					</div>
				</div>
				<!-- END PAGE CONTENT-->
				
				
			</div>
		</div>
    </div>
</div>

