<?php
    $query = $this->training_db->get($training_id);
    
    if($query):
    
    $content = $query[0];
    $form_id = $content['evaluation_form']['institute'];
    $participant_amount = $content['participant_amount'];
?>
<!-- Being Page Title -->
<div class="container">
    <div class="row">
        <div class="col-md-3">
			<!-- Include Dashboard Menu -->
			<?=$this->load->view('home/inc/dashboard/lpp_dashboard_menu')?>
			<!-- End Dashboard Menu -->
        </div>
		
		<div class="col-md-9 dashboard-container">
			<ol class="breadcrumb">
				<li><a href="#">Pelatihan</a></li>
				<li><a href="#"><?=$title_page?></a></li>
				
			  	<li class="active"><?=$title_content?></li>
			  	
			</ol>
			</ol>
			
			<div class="content-dashboard">
				
				<?php if(isset($content['verified_submission_training']) && $content['verified_submission_training']== -1):?>
				<div class="alert alert-warning alert-dismissible" role="alert">
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				  <?=nl2br($content['review_note'])?>
				</div>
				<?php endif;?>
				<!-- BEGIN PAGE CONTENT-->
				<form action="<?=site_url('lpp/management_training/training_post/'.$training_id)?>" id="form_sample_3" method='post' class="form-horizontal">
								<div class="form-body">									
									<h4 class="form-section">Data Evaluasi Penyelenggaraan Pelatihan</h4>
									
									<div class="form-group">
										<label class="control-label col-md-3">Nama Instansi 
										</label>
										<div class="col-md-7">
											<p class="form-control-static text-content"> <?=$content['user_submission']['training_institute']?> </p>
										</div>
										
									</div>
									<div class="form-group">
										<label class="control-label col-md-3">Tempat Pelatihan 
										</label>
										<div class="col-md-7">
											<p class="form-control-static text-content"> <?=$content['training_location_address']?> </p>
											
										</div>
										
									</div>									

									<div class="form-group">
										<label class="control-label col-md-3">Tanggal Pelatihan 
										</label>
										<div class="col-md-7">
											<div class='input-group date'>
											    
											    <p class="form-control-static text-content"> <?=$content['training_date']?> </p>
											</div>										      
										</div>										
									</div>
									
									<!-- input rekap here-->
									<div id="container_table">
									      <!-- begin evaluation form here-->
									      <div class="table-responsive">
									      <table class="table table-striped">
										  <thead>
											    <tr>
												    <th rowspan="2">
													      No
												    </th>
												    <th colspan="2" rowspan="2">
													      Unsur Penilaian
												    </th>

												    <th colspan="<?=$participant_amount;?>" align='center'>
													      Responden
												    </th>
											    </tr>
											    <tr>
												    <?php
												    for($i=0; $i<$participant_amount; $i++):							
												    ?>
												    <th>
													      <?=$i+1;?>
												    </th>
												    <?php endfor;?>
											    </tr>
										    </thead>
										    <tbody>
				    <?php
					$query_form = $this->evaluation_db->get($form_id);
					$no = $index_section = $index_question = 0;
					if($query_form):
					
					$content_form = $query_form[0];
					foreach($content_form['section'] as $section):
					
				    ?>
				    <tr>
					<td colspan='3'><?=$section?></td>
					<td colspan='<?=$participant_amount;?>'></td>
					
				    </tr>
					<?php foreach($content_form['question'][$index_section] as $question):?>
					<tr>
					    <td><?=++$no?></td>
					    <td colspan='2'><?=$question?></td>
					    
					    <?php for($j=0; $j < $participant_amount; $j++):?>
					    <td>
						<input type="text" style='width:40px; border:1px solid;' name='data[<?=$index_section?>][<?=$index_question?>][]'>
					    </td>
					    <?php endfor;?>
					    
					    
					</tr>
					<?php $index_question++; endforeach; $no=0; $index_question=0;?>
					
				    <?php  $index_section++; endforeach; endif;?>
										    </tbody>
									      </table>
									      </div>
									</div>									
									<p>
										Kriteria Penilaian:<br />
										4: Baik Sekali<br />
										3: Baik<br />
										2: Cukup<br />
										1: Kurang<br />
									</p>
								</div>
								<div class="form-actions">
									<div class="row">
										<div class="col-md-offset-3 col-md-9">
											<button id="btn_save" type="submit" class="btn btn-primary" name='submit' value='save'>Kirim</button>
											
											<a href='<?=($this->session->userdata('redirect') ? $this->session->userdata('redirect') : site_url('lpp/management_training/training'))?>' class="btn btn-default">Batal</a>
										</div>
									</div>
								</div>
							</form>
				<!-- END PAGE CONTENT-->
				
				
			</div>
		</div>
    </div>
</div>
<?php endif;?>
 
