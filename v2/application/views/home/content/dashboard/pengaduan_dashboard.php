<!-- Being Page Title -->
<div class="container">
	<div class="row">
		<div class="col-md-3">
			<!-- Include Dashboard Menu -->
			<?=$this->load->view('home/inc/dashboard/pengaduan_dashboard_menu')?>
			<!-- End Dashboard Menu -->
		</div>

		<div class="col-md-9 dashboard-container">
			<ol class="breadcrumb">
				<li><a href="<?= site_url();?>">Home</a></li>
				<li><a href="<?= site_url('pengaduan');?>">Pengaduan</a></li>
				<li class="active">Daftar Pengaduan</li>
			</ol>

			<div class="pengaduan">
				<div class="row">
					<div class="col-md-12">
						<div class="widget-main">
							<div class="widget-inner">
								<div class="course-search">
									<h3>Cari Pengaduan</h3>

									<form id="quick_form" class="course-search-form" method="post" action="" name="formpengaduansearch">
										<!--
										<select class="searchselect type-search" id="type_search" name="type_search">
											<option value="0">Text</option>
											<option value="1">Kode Pengaduan</option>
										</select>
									-->

										<input name="keyword" id="keyword" class="searchbox" placeholder="kata kunci" autocomplete="off"  title="Course Title">

										<div id="search_advanced" class="clearfix search-advanced">
											<!-- TAHUN -->
											<p class="search-form-item margin-top-10" style="margin-top: 10px;">
												<label class="alabel" for="Campus">Tahun:</label>
												<select class="form-control" id="year" name="year">
													<option value="0">-- select --</option>
													<option value="2015">2015</option>
													<option value="2016">2016</option>
												</select>
											</p>
											<!-- /END TAHUN -->
											
											<!-- LOKASI -->
											<div class="clear margin-top-0 search-bit-advanced">
												<p class="search-form-item margin-top-0">
													<label class="alabel" for="Discipline">Lokasi:</label>
													<select name="province_code" id="province" class="select2me form-control">
														<option value=''>-pilih-</option>
														<?php
														$query = file_get_contents($this->config->item('connect_api_url').'/location/province');
														$province = array();
														if($query){
															$province = json_decode($query,true);
														}
														
														if($province)
															foreach($province as $row):
																?>
															<option value='<?=$row['id']?>'><?=$row['name']?></option>
															<?php
															endforeach;
															?>
														</select>
												</p>
												<p class="search-form-item margin-top-0">
													<label class="alabel" for="Study_Level">&nbsp;</label>
														<select name="district_code" id="district" class="select2me form-control">
															<option value=''>-pilih-</option>
														</select>
												
												</p>
											</div>
											<!-- /END LOKASI -->
											
											<!-- CATEGORI -->
											<div class="clear margin-top-0 search-bit-advanced">
												<p class="search-form-item margin-top-0">
													<label class="alabel" for="Discipline">Kategori:</label>
													<select id="complaint_category" class="form-control" name="complaint_category">
                                                            <option value="0" selected>-semua kategori-</option>
                                                            <option value="1" >Pertanyaan</option>
                                                            <option value="2" >Saran</option> 
                                                            <option value="3" >Pengaduan</option>
                                                    </select>
												</p>
												
											</div>
											<!-- /END CATEGORY -->

										</div>
										<input class="mainBtn" name="submit" type="submit" class="btn btn-primary" value="Submit">
									</form>

								</div>
							</div> <!-- /.widget-inner -->
						</div> <!-- /.widget-main -->
					</div> <!-- /.col-md-12 -->
				</div> <!-- /.row -->


				<div class="row">
					<div class="col-md-12">
						
						<?php
						
					    if (isset($search))
						$data_complaint = $search;
						else
						$data_complaint = $this->pengaduan_db->get_all(array('publish'=>'1'));
						
						foreach ($data_complaint as $dc)
						{
						?>
						
						
						<div class="list-event-item">
							<div class="box-content-inner clearfix">
								<h5 class="event-title"><a href="event-single.html"><?= $dc['category'];?></a></h5>
								<div class="list-event-header">
									<span class="event-place small-text"><i class="fa fa-calendar-o"></i><?php echo date("F j, Y, g:i a",$dc['created']); ?></span>
									<span class="event-date small-text"><i class="fa fa-globe"></i><?= $dc['district']['name'].','.$dc['province']['name'];?></span>
								</div>
								<p><?= $dc['description']?></p>
							</div> <!-- /.box-content-inner -->
							
							<div class="box-content-inner clearfix">
								<h5 class="event-title">Jawaban Admin :</h5>
								<div class="list-event-header">
									<span class="event-place small-text"><i class="fa fa-calendar-o"></i><?php echo date("F j, Y, g:i a",$dc['answer_time']); ?></span>
									</div>
								<p><?= $dc['answer']?></p>
							</div> <!-- /.box-content-inner -->
							
						</div> <!-- /.list-event-item -->

						<?php
						
						}
						?>
						

						

					</div> <!-- /.col-md-12 -->
				</div> <!-- /.row -->
<!--
				<div class="row">
					<div class="col-md-12">
						<div class="load-more-btn">
							<a href="#">Click here to load more events</a>
						</div>
					</div> 
				</div> -->



			</div>
		</div>
	</div>
</div>

