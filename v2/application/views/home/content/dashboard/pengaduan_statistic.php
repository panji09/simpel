<!-- Being Page Title -->
<div class="container">
	<div class="row">
		<div class="col-md-3">
			<!-- Include Dashboard Menu -->
			<?=$this->load->view('home/inc/dashboard/pengaduan_dashboard_menu')?>
			<!-- End Dashboard Menu -->
		</div>

		<div class="col-md-9 dashboard-container pengaduan">
			<ol class="breadcrumb">
				<li><a href="<?= site_url();?>">Home</a></li>
				<li><a href="<?= site_url('pengaduan');?>">Pengaduan</a></li>
				<li class="active">Statistik Pengaduan</li>
			</ol>
			
			
			<div class="row">
				<div class="col-md-12">
					<div class="widget-main">
						<div class="widget-inner">
							<div class="course-search">
								<h3>Statistik Pengaduan</h3>

								<form id="quick_form" class="course-search-form" method="post">

									<div id="search_advanced" class="clearfix search-advanced">
										<!-- Pengaduan Berdasarkan -->
										<!--
										<p class="search-form-item" style="    margin-top: 10px;">
											<label class="alabel" for="Campus">Pengaduan Berdasarkan:</label>
											<select class="form-control">
											  <option>1</option>
											  <option>2</option>
											  <option>3</option>
											  <option>4</option>
											  <option>5</option>
											</select>
										</p>
										-->
										<!-- /END Pengaduan Berdasarkan -->
										
										<div class="clear margin-top-0 search-bit-advanced">
											<!-- PROVINSI -->
											<p class="search-form-item margin-top-0">
												<label class="alabel" for="Discipline">Provinsi:</label>
												<select name="province_code" id="province" class="select2me form-control">
														<option value=''>-pilih-</option>
														<?php
														$query = file_get_contents($this->config->item('connect_api_url').'/location/province');
														$province = array();
														if($query){
															$province = json_decode($query,true);
														}
														
														if($province)
															foreach($province as $row):
																?>
															<option value='<?=$row['id']?>'><?=$row['name']?></option>
															<?php
															endforeach;
															?>
														</select>
											</p>
											<!-- END PROVINSI -->
											
											<!-- KAB/KOTA -->
											<p class="search-form-item margin-top-0">
												<label class="alabel" for="Study_Level">Kab/Kota</label>
												<select name="district_code" id="district" class="select2me form-control">
															<option value=''>-pilih-</option>
														</select>
											</p>
											<!-- END KAB KOTA -->
											
											<!-- TAHUN -->
											<p class="search-form-item margin-top-0">
												<label class="alabel" for="Discipline">Tahun:</label>
												<select class="form-control" id="year" name="year">
													<option value="0">-- select --</option>
													<option value="2015">2015</option>
													<option value="2016">2016</option>										
												</select>
											</p>
											<!-- /END TAHUN -->
										</div>

									</div>
									<input class="mainBtn" name="submit" value="submit" type="submit">
								</form>

							</div>
						</div> <!-- /.widget-inner -->
					</div> <!-- /.widget-main -->
				</div> <!-- /.col-md-12 -->
			</div> <!-- /.row -->
			
		
		<?php
		//data tabel
		
		//default
		
		if (isset($search))
		$data = $search;
		else
		$data = $this->pengaduan_db->get_all(array('verification'=>'1'));
		
		$pertanyaan_p = 0;
		$pertanyaan_f = 0;
		$saran_p = 0;
		$saran_f = 0;
		$pengaduan_p = 0;
		$pengaduan_f = 0;
		
		foreach ($data as $data)
		{
			switch ($data['category'])
			{
				case "Pertanyaan":
					if (isset($data['answer']))
					$pertanyaan_f++;
					else
					$pertanyaan_p++; 
				break;
				
				case "Saran":
					if (isset($data['answer']))
					$saran_f++;
					else
					$saran_p++; 
					
					break;
				case "Pengaduan":
					if (isset($data['answer']))
					$pengaduan_f++;
					else
					$pengaduan_p++; 
					
					break;

			}
				
				
			
		}
		
		
		
		?>
		
		
			
			<div class="row" style="margin-top: 20px">
				<div class="col-md-12">
					<div class="content-dashboard">
						<div id="container_chart" style="min-width: 310px; height: 400px; margin: 0 auto"></div>

						<table id="datatable" class="table table-striped">
							<thead>
								<tr>
									<th></th>
									<th>Pending</th>
									<th>Terjawab</th>
									
								</tr>
							</thead>
							<tbody>
								<tr>
									<th>Pertanyaan</th>
									<td><?= $pertanyaan_p;?></td>
									<td><?= $pertanyaan_f;?></td>
									
								</tr>
								<tr>
									<th>Saran</th>
									<td><?= $saran_p;?></td>
									<td><?= $saran_f;?></td>
									
								</tr>
								<tr>
									<th>Pengaduan</th>
									<td><?= $pengaduan_p;?></td>
									<td><?= $pengaduan_f;?></td>

									
								</tr>
							</tbody>
						</table>
					</div>
				</div>
				
				
			</div>
			
		</div>
	</div>
</div>

