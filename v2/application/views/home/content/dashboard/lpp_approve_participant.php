  <!-- Being Page Title -->
  <div class="container">
      <div class="row">
	  <div class="col-md-3">
			  <!-- Include Dashboard Menu -->
			  <?=$this->load->view('home/inc/dashboard/lpp_dashboard_menu')?>
			  <!-- End Dashboard Menu -->
	  </div>
		  
		  <div class="col-md-9 dashboard-container">
			  <ol class="breadcrumb">
				  <li><a href="#">Pelatihan</a></li>
				  <li class="active"><?=$title_page?></li>
			  </ol>
			  
			  <!-- BEGIN PAGE CONTENT-->
			  <div class="content-dashboard">
				  
				  <div class="row">
					<div class="col-md-12">
						<?php callback_submit_home();?>
						
					</div>  
					<div class="col-md-12">
					<div class="portlet light">
				    <div class="portlet-title tabbable-line">
					<div class="caption caption-md">
					    <i class="icon-globe theme-font-color hide"></i>
					    <span class="caption-subject theme-font-color bold uppercase">Approval Peserta Pelatihan</span>
					</div>
					<ul class="nav nav-tabs">
					    <li class="active">
						<a href="#tab_submission_training_approval" data-toggle="tab">Disetujui </a>
					    </li>
					    <li>
						<a href="#tab_submission_training_history" data-toggle="tab">Riwayat </a>
					    </li>
					</ul>
				    </div>
				    
				    <div class="portlet-body">
					<div class="tab-content">
					    <div class="tab-pane active" id="tab_submission_training_approval">
						<!-- approval-->
						<div class="portlet light">
							<div class="portlet-title">
								<div class="caption">
									<i class="icon-speech"></i>
									<span class="caption-subject bold uppercase"> Disetujui</span>
								</div>
								<div class="actions">
									
									<div class="btn-group">
										<a class="btn btn-circle btn-default " href="javascript:;" data-toggle="dropdown">
										<i class="fa fa-cloud-download"></i> Export <i class="fa fa-angle-down"></i>
										</a>
										<ul class="dropdown-menu pull-right">
											<li>
												<a href="<?=site_url('lpp/management_training/approve_participant/export/list_approved/?file=csv')?>">
												<i class="fa fa-file-excel-o"></i> CSV </a>
											</li>
											
										</ul>
									</div>
									<a href="javascript:;" class="btn btn-circle btn-default btn-icon-only fullscreen"></a>
								</div>
							</div>
							<div class="portlet-body">
							
							<?php
						  $query = $this->training_db->get_all(array('user_id' => $this->connect_auth->get_me()['user_id']));
						  $training_ids =array();
						  if($query)
						  foreach($query as $row){
						      $training_ids[] = $row['_id']->{'$id'};
						  }
						  
						  $query = $this->join_training_db->get_all(array(
						      'training_id_in' => $training_ids,
						      'institute_approved' => 1
						  ));
						  ?>
						  <?php $this->load->view('home/inc/dashboard/lpp_table_approve_participant', array('query' => $query))?>
						  
					
							</div>
						</div>
						<!-- ./approval-->
				
				
					    </div>
					    
					    <div class="tab-pane" id="tab_submission_training_history">
						<!-- history-->
						<div class="portlet light">
							<div class="portlet-title">
								<div class="caption">
									<i class="icon-speech"></i>
									<span class="caption-subject bold uppercase"> Riwayat</span>
								</div>
								<div class="actions">
									<div class="btn-group">
										<a class="btn btn-circle btn-default " href="javascript:;" data-toggle="dropdown">
										<i class="fa fa-cloud-download"></i> Export <i class="fa fa-angle-down"></i>
										</a>
										<ul class="dropdown-menu pull-right">
											<li>
												<a href="<?=site_url('lpp/management_training/approve_participant/export/list_all/?file=csv')?>">
												<i class="fa fa-file-excel-o"></i> CSV </a>
											</li>
											
										</ul>
									</div>
									
									<a href="javascript:;" class="btn btn-circle btn-default btn-icon-only fullscreen"></a>
								</div>
							</div>
							<div class="portlet-body">
								
						
							<?php
							$query = $this->training_db->get_all(array('user_id' => $this->connect_auth->get_me()['user_id']));
							$training_ids =array();
							if($query)
							foreach($query as $row){
							    $training_ids[] = $row['_id']->{'$id'};
							}
							
							$query = $this->join_training_db->get_all(array(
							    'training_id_in' => $training_ids
							));
							
						    ?>
						    
						    <?php $this->load->view('home/inc/dashboard/lpp_table_approve_participant', array('query' => $query))?>
							</div>
						</div>
						<!-- ./history-->
					    </div>
					    
					</div>  
				    </div>
					</div>
				</div>
				
				  </div>
				  <!-- END PAGE CONTENT-->
				  
				  
			  </div>
		  </div>
      </div>
  </div>

