<?php
    $page = $this->uri->segment(3);
    $state = $this->uri->segment(4);
    $query = $user = array();
    if($state == 'edit'){
	$query = $this->dynamic_pages_db->get($content_id);
	if($query)
	    $content = $query[0];
    }
?>
<!-- Being Page Title -->
<div class="container">
    <div class="row">
        <div class="col-md-3">
			<!-- Include Dashboard Menu -->
			<?=$this->load->view('home/inc/dashboard/lpp_dashboard_menu')?>
			<!-- End Dashboard Menu -->
        </div>
		
		<div class="col-md-9 dashboard-container">
			<ol class="breadcrumb">
				<li><a href="#">Dashboard</a></li>
			  	<li><a href="#"><?=$title_page?></a></li>
			  	<li class="active"><?=$title_content?></li>
			</ol>
			
			<div class="content-dashboard">
				<!-- BEGIN PAGE CONTENT-->
				<form action="<?=site_url('superadmin/pages/news_post/'.$content_id)?>" id="form_sample_3" method='post' class="form-horizontal">
								<div class="form-body">
									<h3 class="form-section">Lengkapi kolom dibawah ini</h3>
									<div class="form-group">
										<label class="control-label col-md-3">Judul <span class="required">
										* </span>
										</label>
										<div class="col-md-4">
											<input value='<?=(isset($content['title']) ? $content['title'] : '' )?>' type="text" name="title" data-required="1" class="form-control"/>
										</div>										
									</div>
									
									<div class="form-group">			
									    <label class="control-label col-md-3">Cover Berita 
									    </label>
									    <div class="col-md-4">
										<div class="row">
										    <div class="col-xs-6 col-md-3">
										      <a href="#" class="thumbnail" style="width: 500px; height: auto;">
											<img class='change_cover' src="<?=(isset($content['cover']) && $content['cover']!='' ? $content['cover'] : 'http://www.placehold.it/500x150/EFEFEF/AAAAAA&text=pilih foto' )?>" alt="...">
											<input type='hidden' name='cover' id='cover' value='<?=(isset($content['cover']) ? $content['cover'] : '' )?>'>
										      </a>
										    </div>
										</div>
									    </div>
									</div>
									
									
									
									<div class="form-group last">
										<label class="control-label col-md-3">Deskripsi Pendek <span class="required">
										* </span>
										</label>
										<div class="col-md-9">
											<textarea id='' class="form-control wysiwyg-short" name="content_short" rows="6" data-error-container="#editor2_error"><?=(isset($content['content_short']) ? $content['content_short'] : '' )?></textarea>
											<div id="editor2_error">
											</div>
										</div>
									</div>
									
									<div class="form-group last">
										<label class="control-label col-md-3">Konten <span class="required">
										* </span>
										</label>
										<div class="col-md-9">
											<textarea id='' class="form-control wysiwyg-content" name="content" rows="6" data-error-container="#editor2_error"><?=(isset($content['content']) ? $content['content'] : '' )?></textarea>
											<div id="editor2_error">
											</div>
										</div>
									</div>
									
									
								</div>
								<div class="form-actions">
									<div class="row">
										<div class="col-md-offset-3 col-md-9">
											<button type="submit" class="btn btn-primary">Simpan</button>
											<a href='<?=$this->session->userdata('redirect')?>' class="btn btn-default">Batal</a>
										</div>
									</div>
								</div>
							</form>
				<!-- END PAGE CONTENT-->
				
				
			</div>
		</div>
    </div>
</div>

