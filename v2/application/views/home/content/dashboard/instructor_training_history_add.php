<?php
    $page = $this->uri->segment(3);
    $state = $this->uri->segment(4);
    $query = $user = array();
    if($state == 'edit' ){
	$query = $this->training_db->get($training_id);
	if($query)
	    $content = $query[0];
    }
?>
<!-- Being Page Title -->
<div class="container">
    <div class="row">
        <div class="col-md-3">
			<!-- Include Dashboard Menu -->
			<?=$this->load->view('home/inc/dashboard/instructor_dashboard_menu')?>
			<!-- End Dashboard Menu -->
        </div>
		
		<div class="col-md-9 dashboard-container">
			<ol class="breadcrumb">
				<li><a href="#">Pelatihan</a></li>
				<li><a href="#"><?=$title_page?></a></li>
				
			  	<li class="active"><?=$title_content?></li>
			  	
			</ol>
			</ol>
			
			<div class="content-dashboard">

				<!-- BEGIN PAGE CONTENT-->
				<form action="<?=site_url('narasumber/history/training_history_post/'.$training_history_id)?>" id="form_sample_3" method='post' class="form-horizontal">
								<div class="form-body">
									<h3 class="form-section">Lengkapi kolom dibawah ini</h3>
									
									<h4 class="form-section">Data History Pelatihan</h4>
									
									<div class="form-group">
										<label class="control-label col-md-3">Nama Pelatihan <span class="required">
										* </span>
										</label>
										<div class="col-md-7">
											<input name='training_name' value='<?=(isset($content['training_name']) ? $content['training_name'] : '')?>' type='text' class='form-control' placeholder='Masukan Nama Pelatihan'>
											<input name='published' type='hidden' value='1'> </input>
										</div>
										
									</div>
									
									<div class="form-group">
										<label class="control-label col-md-3">Tanggal Pelatihan <span class="required">
										* </span>
										</label>
										<div class="col-md-7">
											<div class='input-group date'>
											    <input name='training_date' value='<?=(isset($content['training_date']) ? $content['training_date'] : '')?>' type='text' class="form-control rangedate" />
											    <span class="input-group-addon">
												<span class="glyphicon glyphicon-calendar"></span>
											    </span>
											    <input id='start_date' type='hidden' name='training_date_start' value='<?=(isset($content['training_date_start']) ? $content['training_date_start'] : '')?>'>
											    <input id='end_date' type='hidden' name='training_date_end' value='<?=(isset($content['training_date_end']) ? $content['training_date_end'] : '')?>'>
											    
											</div>
										      
										</div>
										
									</div>
									
									<h4 class="form-section">Data Lokasi Pelatihan</h4>
									<div class="form-group">
										<label class="control-label col-md-3">Tempat Pelatihan <span class="required">
										* </span>
										</label>
										<div class="col-md-7">
											<textarea name='training_location_address'  class='form-control' placeholder='Isikan nama gedung dan alamat lengkap'><?=(isset($content['training_location_address']) ? $content['training_location_address'] : '')?></textarea>
											<span class="help-block">Isikan nama gedung dan alamat lengkap </span>
										</div>										
									</div>
																		
									<div class="form-group">
										<label class="control-label col-md-3">Provinsi <span class="required">
										* </span>
										</label>
										<div class="col-md-7">
											<select name='training_location_province' class="form-control select2me" id='province'>
											  <option value=''>-Pilih-</option>
											  <?php
											      $query = file_get_contents($this->config->item('connect_api_url').'/location/province');
											      $province = array();
											      if($query){
												  $province = json_decode($query,true);
											      }
											      
											      if($province)
											      foreach($province as $row):
											  ?>
											      <option value='<?=$row['id']?>' <?=(isset($content['training_location_province']['id']) && $content['training_location_province']['id'] == $row['id'] ? 'selected' : '')?> ><?=$row['name']?></option>
											  <?php
											  endforeach;
											  ?>
											  
											</select>
										</div>
										
									</div>
									
									<div class="form-group">
										<label class="control-label col-md-3">Kab / Kota <span class="required">
										* </span>
										</label>
										<div class="col-md-7">
											<select name='training_location_district' class="form-control select2me" id='district'>
											  <option value=''>-Pilih-</option>
											  <?php if(isset($content['training_location_province']['id'])):?>
											  <?php
											      $query = file_get_contents($this->config->item('connect_api_url').'/location/district/'.$content['training_location_province']['id']);
											      $province = array();
											      if($query){
												  $province = json_decode($query,true);
											      }
											      
											      if($province)
											      foreach($province as $row):
											  ?>
											      <option value='<?=$row['id']?>' <?=(isset($content['training_location_district']['id']) && $content['training_location_district']['id'] == $row['id'] ? 'selected' : '')?> ><?=$row['name']?></option>
											  <?php
											  endforeach;
											  ?>
											  <?php endif;?>
											</select>
										</div>
										
									</div>

									
								</div>
								<div class="form-actions">
									<div class="row">
										<div class="col-md-offset-3 col-md-9">
											<button type="submit" class="btn btn-primary" name='submit' value='save'>Simpan</button>
																						
											<a href='<?=($this->session->userdata('redirect') ? $this->session->userdata('redirect') : site_url('narasumber/history/training_history'))?>' class="btn btn-default">Batal</a>
										</div>
									</div>
								</div>
							</form>
				<!-- END PAGE CONTENT-->
				
				
			</div>
		</div>
    </div>
</div>

