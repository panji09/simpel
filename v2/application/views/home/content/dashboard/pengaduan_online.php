<!-- Being Page Title -->
<div class="container">
	<div class="row">
		<div class="col-md-3">
			<!-- Include Dashboard Menu -->
			<?=$this->load->view('home/inc/dashboard/pengaduan_dashboard_menu')?>
			<!-- End Dashboard Menu -->
		</div>

		<div class="col-md-9 dashboard-container pengaduan">
			<ol class="breadcrumb">
				<li><a href="<?= site_url();?>">Home</a></li>
				<li><a href="<?= site_url('pengaduan');?>">Pengaduan</a></li>
				<li class="active">Kirim Pengaduan</li>
			</ol>

			<div class="alert alert-warning alert-dismissible fade in" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
				Mengadu secara komplit dan bertanggung jawab sebelum anda bertanya lebih jauh, silahkan <a href='<?=site_url('faq')?>'>lihat FAQ</a>
			</div>

			<?php 
			callback_submit_home();
			?>

			<div class="content-dashboard">
				<h3>Kirim Pengaduan</h3>
				<form name="formpengaduan" method="post" action="?" class="form-horizontal margin-top-40">
					<h4 class="form-section">Lokasi</h4>
					
					<div class="form-group">
						<label for="inputEmail3" class="col-sm-2 control-label">Provinsi</label>
						<div class="col-sm-10">
							<select name="provinsi" id="province" class="select2me form-control">
								<option value=''>-pilih-</option>
								<?php
								$query = file_get_contents($this->config->item('connect_api_url').'/location/province');
								$province = array();
								if($query){
									$province = json_decode($query,true);
								}
								
								if($province)
									foreach($province as $row):
										?>
									<option value='<?=$row['id']?>'><?=$row['name']?></option>
									<?php
									endforeach;
									?>
								</select>
							</div>
						</div>
						
						<div class="form-group">
							<label for="inputEmail3" class="col-sm-2 control-label">Kabupaten/Kota</label>
							<div class="col-sm-10">
								<select name="kota" id="district" class="select2me form-control">
									<option value=''>-pilih-</option>
								</select>
							</div>
						</div>
						
						<div class="form-group">
							<label for="inputEmail3" class="col-sm-2 control-label">Kecamatan</label>
							<div class="col-sm-10">
								<select name="kecamatan" id="subdistrict" class="select2me form-control">
									<option value=''>-pilih-</option>
								</select>
							</div>
						</div>																
						<h4 class="form-section">Jenis Pengaduan</h4>
						<div class="form-group">
							<label for="inputPassword3" class="col-sm-2 control-label">Kategori</label>
							<div class="col-sm-10">
								<select name="kategori" id="disabledSelect" class="select2me form-control">
									<?php 
									foreach($kategori as $kat):
										foreach($kat['kategori'] as $k):
											echo '<option value="'.$k.'">'.$k.'</option>';
										endforeach;
										endforeach;
										?>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label for="inputPassword3" class="col-sm-2 control-label">Substansi</label>
								<div class="col-sm-10">
									<?php 
									foreach($substansi as $subs):
										foreach($subs['substansi'] as $s):
											?>
										<label class="checkbox-inline">
											<input name="substansi[]" type="checkbox" id="inlineCheckbox1" value="<?php echo $s ?>"> <?php echo $s ?>
										</label>
										<?php 
										endforeach;
										endforeach;
										?>
									</div>
								</div>
								
								<h4 class="form-section">Identitas Pelapor</h4>
								<div class="form-group">
									<label for="inputPassword3" class="col-sm-2 control-label">Peran</label>
									<div class="col-sm-10">
										<select name="peran" id="disabledSelect" class="select2me form-control">
											<?php 
											foreach($peran as $per):
												foreach($per['peran'] as $p):
													echo '<option value="'.$p.'">'.$p.'</option>';
												endforeach;
												endforeach;
												?>
											</select>
										</div>
									</div>
									
									<div class="form-group">
										<label for="inputName2" class="col-sm-2 control-label">Nama</label>
										<div class="col-sm-10">
											<input name="nama" type="text" class="form-control" id="inputName2" placeholder="Nama">
										</div>
									</div>
									
									<div class="form-group">
										<div class="col-sm-offset-2 col-sm-10">
											<div class="checkbox">
												<label>
													<input name="tampilkansatu" type="checkbox"> Apakah nama anda ingin ditampilkan?
												</label>
											</div>
										</div>
									</div>
									
									<div class="form-group">
										<label for="inputName3" class="col-sm-2 control-label">Email</label>
										<div class="col-sm-10">
											<input name="email" type="email" class="form-control" id="inputName3" placeholder="email">
										</div>
									</div>
									
									<div class="form-group">
										<label for="inputName4" class="col-sm-2 control-label">Telp</label>
										<div class="col-sm-10">
											<input name="telp" type="text" class="form-control" id="inputName4" placeholder="telpon">
										</div>
									</div>
									
									<div class="form-group">
										<div class="col-sm-offset-2 col-sm-10">
											<div class="checkbox">
												<label>
													<input name="tampilkandua" type="checkbox"> Apakah nomor telpon anda ingin ditampilkan?
												</label>
											</div>
										</div>
									</div>
									
									<div class="form-group">
										<label for="inputName4" class="col-sm-2 control-label">Alamat</label>
										<div class="col-sm-10">
											<textarea name="alamat" class="form-control" rows="3"></textarea>
										</div>
									</div>
									
									<div class="form-group">
										<div class="col-sm-offset-2 col-sm-10">
											<div class="checkbox">
												<label>
													<input name="tampilkantiga" type="checkbox"> Apakah alamat anda ingin ditampilkan?
												</label>
											</div>
										</div>
									</div>
									
									<div class="form-group">
										<label for="inputName4" class="col-sm-2 control-label">Deskripsi</label>
										<div class="col-sm-10">
											<textarea name="deskripsi" class="form-control" rows="3"></textarea>
										</div>
									</div>
									
									<div class="form-group">
										<label for="inputName4" class="col-sm-2 control-label">Verifikasi</label>
										<div class="col-sm-10">
											<?php 
											echo $captcha['image']; 
											?>
											<input type="text" name="captcha" class="form-control">
											<input type="hidden" name="captcha_invisible" value="<?php echo $captcha['word'] ?>">
										</div>
									</div>					
									
									<div class="form-actions">
										<div class="row">
											<div class="col-md-offset-3 col-md-9">
												<input name="submit" type="submit" class="btn btn-primary" value="Submit">
												<input name="cancel" type="button" class="btn btn-default" value="Cancel">
											</div>
										</div>
									</div>
									
								</form>
							</div>
						</div>
					</div>
				</div>

