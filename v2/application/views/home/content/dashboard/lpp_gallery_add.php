<?php
    $page = $this->uri->segment(3);
    $state = $this->uri->segment(4);
    $query = $user = array();
    if($state == 'edit'){
	$query = $this->dynamic_pages_db->get($content_id);
	if($query)
	    $content = $query[0];
    }
?>
<!-- Being Page Title -->
<div class="container">
    <div class="row">
        <div class="col-md-3">
			<!-- Include Dashboard Menu -->
			<?=$this->load->view('home/inc/dashboard/lpp_dashboard_menu')?>
			<!-- End Dashboard Menu -->
        </div>
		
		<div class="col-md-9 dashboard-container">
			<ol class="breadcrumb">
				<li><a href="#">Dashboard</a></li>
			  	<li><a href="#"><?=$title_page?></a></li>
			  	<li class="active"><?=$title_content?></li>
			</ol>
			
			<div class="content-dashboard">
				
				
				<!-- BEGIN PAGE CONTENT-->
				<form action="<?=site_url('superadmin/pages/gallery_post/'.$content_id)?>" id="form_sample_3" method='post' class="form-horizontal">
								<div class="form-body">
									<h3 class="form-section">Lengkapi kolom dibawah ini</h3>
									
									
									<div class="form-group">
										<label class="control-label col-md-3">Judul Album <span class="required">
										* </span>
										</label>
										<div class="col-md-4">
											<input value='<?=(isset($content['title']) ? $content['title'] : '' )?>' type="text" name="title" data-required="1" class="form-control"/>
										</div>
										
									</div>
									
									
									<h3 class="form-section">Foto</h3>
									
									<div class='row' id='upload_gallery'>
									  
									    <div class="col-md-3">
										
										<div class="row">
										    <div class="col-xs-6 col-md-3">
										      <a href="#" class="thumbnail" style="width: 100px; height: auto;">
											<img id='change_cover_multi' src="<?=(isset($content['cover']) && $content['cover']!='' ? $content['cover'] : 'http://www.placehold.it/250x250/EFEFEF/AAAAAA&text= Pilih Foto' )?>" alt="...">
											
										      </a>
										    </div>
										</div>
										
										
									    </div>
									    
									    
									    <?php if(isset($content['photo'])): for($i=0; $i < count($content['photo']); $i++):?>
									    <div class="col-md-3">
							    
										<div class="row">
										    <div class="col-xs-6 col-md-3">
										      <a href="#" class="thumbnail" style="width: 100px; height: 100px; background-color:black;">
											<img style="max-width: 100px; max-height: 100px;" id='change_cover' src="<?=(isset($content['photo'][$i]) && $content['photo'][$i]!='' ? $content['photo'][$i] : 'http://www.placehold.it/250x250/EFEFEF/AAAAAA&text= Pilih Foto' )?>" alt="...">
											<input type='hidden' name='photo[]' id='cover' value='<?=(isset($content['photo'][$i]) && $content['photo'][$i]!='' ? $content['photo'][$i] : 'http://www.placehold.it/250x250/EFEFEF/AAAAAA&text= Pilih Foto' )?>'>
										      </a>
										    </div>
										</div>
										<div class='row'>
										    
										    <div class="col-md-10">
											<button type="button" class="btn green delete_photo">Hapus</button>
											<input id='title' value='<?=(isset($content['title_photo'][$i]) && $content['title_photo'][$i]!='' ? $content['title_photo'][$i] : '' )?>' type="text" name="title_photo[]" data-required="1" class="form-control"/>
										    </div>
											      
										      <br>
										</div>
									    </div>
									    <?php endfor; endif;?>
									    
									    
									    
									</div>
									
								</div>
								<div class="form-actions">
									<div class="row">
										<div class="col-md-offset-3 col-md-9">
											<button type="submit" class="btn btn-primary">Simpan</button>
											<a href='<?=$this->session->userdata('redirect')?>' class="btn btn-default">Batal</a>
										</div>
									</div>
								</div>
							</form>
							
							<div class='hide'>
							    <div id='template_cover'>
								<div class="col-md-3">
							    
								    <div class="row">
									<div class="col-xs-6 col-md-3">
									  <a href="#" class="thumbnail" style="width: 100px; height: 100px; background-color:black;">
									    <img style="max-width: 100px; max-height: 100px;" id='change_cover' src="<?=(isset($content['cover']) && $content['cover']!='' ? $content['cover'] : 'http://www.placehold.it/250x250/EFEFEF/AAAAAA&text= Pilih Foto' )?>" alt="...">
									    <input type='hidden' name='photo[]' id='cover' value=''>
									  </a>
									</div>
								    </div>
								    <div class='row'>
									
									<div class="col-md-10">
									    <button type="button" class="btn green delete_photo">Hapus</button>
									    <input id='title' value='' type="text" name="title_photo[]" data-required="1" class="form-control"/>
									</div>
										  
									  <br>
								    </div>
								</div>
							    </div>
							</div>
				<!-- END PAGE CONTENT-->
				
				
			</div>
		</div>
    </div>
</div>

