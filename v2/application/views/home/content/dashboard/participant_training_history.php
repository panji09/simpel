<!-- Being Page Title -->
<div class="container">
    <div class="row">
        <div class="col-md-3">
			<!-- Include Dashboard Menu -->
			<?=$this->load->view('home/inc/dashboard/participant_dashboard_menu')?>
			<!-- End Dashboard Menu -->
        </div>
		
		<div class="col-md-9 dashboard-container">
			<ol class="breadcrumb">
				<li><a href="#">Home</a></li>
			  	<li class="active">History Pelatihan</li>
			</ol>
			
			<div class="content-dashboard regulatory-container">
				
				<h3>History Pelatihan</h3>
				
				<hr />
				
				<table class="table table-striped table-hover" id="history_training_table">
					<thead>
						<tr>
							<th>Pendaftaran</th>
							<th>Tanggal Pelatihan</th>
							<th>LPP</th>
							<th>Nama Pelatihan</th>
							<th>Jenis Pelatihan</th>
							<th>Status</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>2 days ago</td>
							<td>2015-02-02</td>
							<td>LPP Negeri</td>
							<td>Pelatihan Tegar</td>
							<td>Pelatihan Teknis</td>
							<td><span class="label label-default">Selesai</span></td>
						</tr>
						<tr>
							<td>2 days ago</td>
							<td>2015-02-02</td>
							<td>LPP Negeri</td>
							<td>Pelatihan Tegar</td>
							<td>Pelatihan Teknis</td>
							<td><span class="label label-default">Selesai</span></td>
						</tr>
						<tr>
							<td>2 days ago</td>
							<td>2015-02-02</td>
							<td>LPP Negeri</td>
							<td>Pelatihan Tegar</td>
							<td>Pelatihan Teknis</td>
							<td><span class="label label-default">Selesai</span></td>
						</tr>
						<tr>
							<td>2 days ago</td>
							<td>2015-02-02</td>
							<td>LPP Negeri</td>
							<td>Pelatihan Tegar</td>
							<td>Pelatihan Teknis</td>
							<td><span class="label label-default">Selesai</span></td>
						</tr>
						<tr>
							<td>2 days ago</td>
							<td>2015-02-02</td>
							<td>LPP Negeri</td>
							<td>Pelatihan Tegar</td>
							<td>Pelatihan Teknis</td>
							<td><span class="label label-default">Selesai</span></td>
						</tr>
						<tr>
							<td>2 days ago</td>
							<td>2015-02-02</td>
							<td>LPP Negeri</td>
							<td>Pelatihan Tegar</td>
							<td>Pelatihan Teknis</td>
							<td><span class="label label-default">Selesai</span></td>
						</tr>
						<tr>
							<td>2 days ago</td>
							<td>2015-02-02</td>
							<td>LPP Negeri</td>
							<td>Pelatihan Tegar</td>
							<td>Pelatihan Teknis</td>
							<td><span class="label label-default">Selesai</span></td>
						</tr>
						<tr>
							<td>2 days ago</td>
							<td>2015-02-02</td>
							<td>LPP Negeri</td>
							<td>Pelatihan Tegar</td>
							<td>Pelatihan Teknis</td>
							<td><span class="label label-default">Selesai</span></td>
						</tr>
						<tr>
							<td>2 days ago</td>
							<td>2015-02-02</td>
							<td>LPP Negeri</td>
							<td>Pelatihan Tegar</td>
							<td>Pelatihan Teknis</td>
							<td><span class="label label-default">Selesai</span></td>
						</tr>
						<tr>
							<td>2 days ago</td>
							<td>2015-02-02</td>
							<td>LPP Negeri</td>
							<td>Pelatihan Tegar</td>
							<td>Pelatihan Teknis</td>
							<td><span class="label label-default">Selesai</span></td>
						</tr>

					</tbody>
				</table>
				
			</div>
			
		</div>
    </div>
</div>

