<!-- Being Page Title -->
<div class="container">
    <div class="row">
        <div class="col-md-3">
			<!-- Include Dashboard Menu -->
			<?=$this->load->view('home/inc/facilitator_dashboard_menu')?>
			<!-- End Dashboard Menu -->
        </div>
		
		<div class="col-md-9 dashboard-container">
			<ol class="breadcrumb">
				<li><a href="#">Home</a></li>
			  	<li class="active">Notifikasi</li>
			</ol>
			
			<div class="content-dashboard notification">
				
				<div class="list-group">
				  <a href="#" class="list-group-item active">
					<div class="label label-sm label-success"><i class="fa fa-bell-o"></i></div>
					&nbsp;&nbsp;Cras justo odio
					<span class="pull-right time">just now</span>
				  </a>
				  <a href="#" class="list-group-item time active">
  					<div class="label label-sm label-success"><i class="fa fa-bell-o"></i></div>
  					&nbsp;&nbsp;Dapibus ac facilisis in
					<span class="pull-right time">just now</span>
				  </a>
				  <a href="#" class="list-group-item time active">
  					<div class="label label-sm label-success"><i class="fa fa-bell-o"></i></div>
  					&nbsp;&nbsp;Morbi leo risus
					<span class="pull-right time">just now</span>
				  </a>
				  <a href="#" class="list-group-item time">
  					<div class="label label-sm label-success"><i class="fa fa-bell-o"></i></div>
  					&nbsp;&nbsp;Porta ac consectetur ac
					<span class="pull-right time">just now</span>
				  </a>
				  <a href="#" class="list-group-item time">
  					<div class="label label-sm label-success"><i class="fa fa-bell-o"></i></div>
  					&nbsp;&nbsp;Vestibulum at eros
					<span class="pull-right time">just now</span>
				  </a>
				  <a href="#" class="list-group-item time">
  					<div class="label label-sm label-success"><i class="fa fa-bell-o"></i></div>
  					&nbsp;&nbsp;Porta ac consectetur ac
					<span class="pull-right time">just now</span>
				  </a>
				  <a href="#" class="list-group-item time">
  					<div class="label label-sm label-success"><i class="fa fa-bell-o"></i></div>
  					&nbsp;&nbsp;Vestibulum at eros
					<span class="pull-right time">just now</span>
				  </a>
				  <a href="#" class="list-group-item time">
  					<div class="label label-sm label-success"><i class="fa fa-bell-o"></i></div>
  					&nbsp;&nbsp;Porta ac consectetur ac
					<span class="pull-right time">just now</span>
				  </a>
				  <a href="#" class="list-group-item time">
  					<div class="label label-sm label-success"><i class="fa fa-bell-o"></i></div>
  					&nbsp;&nbsp;Vestibulum at eros
					<span class="pull-right time">just now</span>
				  </a>
				</div>
				
				<div class="load-more-btn">
                    <a id="loadmore_news">Lanjutkan</a>
                </div>
				
			</div>
		</div>

    </div>
</div>

