<?php
    $page = $this->uri->segment(3);
    $state = $this->uri->segment(4);
    $query = $user = array();
	$is_new = false;
	$content = array();
	
	$query = $this->evaluation_data_db->get($training_id);	

	if($query){
	    $content = $query[0];
    }else{
    	$is_new = true;
		$query_training = $this->training_db->get($training_id);
		$training = $query_training[0];
		$form_id = $training['evaluation_form']['institute'];
		$content['training_institute'] = $training['user_submission']['training_institute'];		
		$content['training_location_address'] = $training['training_location_address'];
		$content['training_date'] = $training['training_date'];
		$content['participant_amount'] = $training['participant_amount'];
		$form_query = $this->evaluation_db->get($form_id);
		if($form_query)
		$form = $form_query[0];
		//generate sessions
		$sections = array();
		//generate lpp session
		foreach($form['lpp'] as $lpp_sections){
			$sections[] = $lpp_sections;
		}
		
		foreach($training['instructors'] as $inst){
			foreach($form['instructor'] as $inst_section){
				$inst_section['name'] = $inst_section['name'].': '. $inst['fullname'];
				$sections[] = $inst_section;
			}	
		}
		
		$content['sections'] = $sections;
		
    }

	$participant = $content['participant_amount'];
	//structure data
	//sections: name
	//			questions
	//			  question
	//			  values: array
	
	//1. Cari pelatihan di tabel rekap evaluasi
	//2. Jika tidak ada maka set flag ini adalah pelatihan baru
	//3. buat struktur data untuk pelatihan baru dengan data sebagai berikut
	//   *nama instansi
	//   *tempat pelatihan 
	//   *
?>
<!-- Being Page Title -->
<div class="container">
    <div class="row">
        <div class="col-md-3">
			<!-- Include Dashboard Menu -->
			<?=$this->load->view('home/inc/dashboard/lpp_dashboard_menu')?>
			<!-- End Dashboard Menu -->
        </div>
		
		<div class="col-md-9 dashboard-container">
			<ol class="breadcrumb">
				<li><a href="#">Pelatihan</a></li>
				<li><a href="#"><?=$title_page?></a></li>
				
			  	<li class="active"><?=$title_content?></li>
			  	
			</ol>
			</ol>
			
			<div class="content-dashboard">
				
				<?php if(isset($content['verified_submission_training']) && $content['verified_submission_training']== -1):?>
				<div class="alert alert-warning alert-dismissible" role="alert">
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				  <?=nl2br($content['review_note'])?>
				</div>
				<?php endif;?>
				<!-- BEGIN PAGE CONTENT-->
				<form action="<?=site_url('lpp/management_training/training_post/'.$training_id)?>" id="form_sample_3" method='post' class="form-horizontal">
								<div class="form-body">									
									<h4 class="form-section">Data Evaluasi Penyelenggaraan Pelatihan</h4>
									
									<div class="form-group">
										<label class="control-label col-md-3">Nama Instansi <span class="required">
										* </span>
										</label>
										<div class="col-md-7">
											<input disabled name='training_institute' value='<?=$content['training_institute']?>' type='text' class='form-control'>
											<input name="participant_amount" type="hidden" value="<?=$participant?>">
										</div>
										
									</div>
									<div class="form-group">
										<label class="control-label col-md-3">Tempat Pelatihan <span class="required">
										* </span>
										</label>
										<div class="col-md-7">
											<input disabled name='training_location_address' value='<?=$content['training_location_address']?>' type='text' class='form-control' placeholder='Masukan No Surat'>
										</div>
										
									</div>									

									<div class="form-group">
										<label class="control-label col-md-3">Tanggal Pelatihan <span class="required">
										* </span>
										</label>
										<div class="col-md-7">
											<div class='input-group date'>
											    <input disabled name='training_date' value='<?=(isset($content['training_date']) ? $content['training_date'] : '')?>' type='text' class="form-control rangedate" />
											    <span class="input-group-addon">
												<span class="glyphicon glyphicon-calendar"></span>
											    </span>
											    <input id='start_date' type='hidden' name='training_date_start' value='<?=(isset($content['training_date_start']) ? $content['training_date_start'] : '')?>'>
											    <input id='end_date' type='hidden' name='training_date_end' value='<?=(isset($content['training_date_end']) ? $content['training_date_end'] : '')?>'>
											    
											</div>										      
										</div>										
									</div>
									
									<!-- input rekap here-->
									<div id="container_table">
													<!-- begin evaluation form here-->
										<table class="table table-striped table-bordered table-hover" id="table_approval_institution">
											<thead>
												<tr>
													<th rowspan="2">
														 No
													</th>
													<th colspan="2" rowspan="2">
														 Unsur Penilaian
													</th>

													<th colspan="<?=$participant;?>">
														 Responden
													</th>
												</tr>
												<tr>
													<?php
													for($i=0; $i<$participant; $i++){								
													?>
													<th>
														 <?=$i+1;?>
													</th>
													<?php }?>
												</tr>
											</thead>
										<tbody>
						
										<!-- begin table here
										this for lpp
										-->
										<?php
										$item = 0;
										$sec = 'A';
										foreach($content['sections'] as $category){ ?>
											<tr data-section="section_master" data-section-title="<?=$category['name']?>"><td><?=$sec?></td>  <td colspan="2"><?=$category['name']?></td> 
											<?php
											for($i=0; $i<$participant; $i++){								
											?>
											<td>
												 <?//=$i+1;?>
											</td>
											<?php }?>
											</tr>
											<!-- row 2-->
											<?php foreach($category['questions'] as $question){?>
											<tr data-item="item_<?=$item?>" data-item-id="<?=$question['no']?>" data-item-question="<?=$question['question']?>"> 
												<td> </td> <td> <?=$question['no']?></td> <td> <?=$question['question']?></td>
												<!-- set input here-->
												<?php
												for($i=0; $i<$participant; $i++){								
												?>
												<td>
													 <input type="text" value='<?php if(isset($question['data'][$i])) echo $question['data'][$i];?>' name="row" data-required="1" style="width:25px;height:30px">
												</td>
												<?php }?>
											</tr>
											<?php }?>
																					
										<?php
										$sec++;
										$item++;
											}
										?>
						
										<tr class="odd gradeX">
										</tr>
						
										</tbody>
										</table>
									</div>									
									<p>
										Kriteria Penilaian:<br />
										4: Baik Sekali<br />
										3: Baik<br />
										2: Cukup<br />
										1: Kurang<br />
									</p>
								</div>
								<div class="form-actions">
									<div class="row">
										<div class="col-md-offset-3 col-md-9">
											<button id="btn_save" type="submit" class="btn btn-primary" name='submit' value='save'>Kirim</button>
											
											<a href='<?=($this->session->userdata('redirect') ? $this->session->userdata('redirect') : site_url('lpp/management_training/training'))?>' class="btn btn-default">Batal</a>
										</div>
									</div>
								</div>
							</form>
				<!-- END PAGE CONTENT-->
				
				
			</div>
		</div>
    </div>
</div>

