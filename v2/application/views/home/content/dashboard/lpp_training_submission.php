<?php
    $page = $this->uri->segment(3);
    $state = $this->uri->segment(4);
    $query = $user = array();
    if($state == 'edit' ){
	$query = $this->training_db->get($training_id);
	if($query)
	    $content = $query[0];
    }
?>
<!-- Being Page Title -->
<div class="container">
    <div class="row">
        <div class="col-md-3">
			<!-- Include Dashboard Menu -->
			<?=$this->load->view('home/inc/dashboard/lpp_dashboard_menu')?>
			<!-- End Dashboard Menu -->
        </div>
		
		<div class="col-md-9 dashboard-container">
			<ol class="breadcrumb">
				<li><a href="#">Pelatihan</a></li>
				<li><a href="#"><?=$title_page?></a></li>
				
			  	<li class="active"><?=$title_content?></li>
			  	
			</ol>
			</ol>
			
			<div class="content-dashboard">
				
				<?php if(isset($content['verified_submission_training']) && $content['verified_submission_training']== -1):?>
				<div class="alert alert-warning alert-dismissible" role="alert">
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				  <?=nl2br($content['review_note'])?>
				</div>
				<?php endif;?>
				
				<?php callback_submit_home();?>
				<!-- BEGIN PAGE CONTENT-->
				<form action="<?=site_url('lpp/management_training/training_post/'.$training_id)?>" id="form_sample_3" method='post' class="form-horizontal">
								<div class="form-body">
									<h3 class="form-section">Lengkapi kolom dibawah ini</h3>
									
									<h4 class="form-section">Data Pengajuan Pelatihan</h4>
									
									<div class="form-group">
										<label class="control-label col-md-3">No Surat Permohonan <span class="required">
										* </span>
										</label>
										<div class="col-md-7">
											<input name='mail_submission_id' value='<?=(isset($content['mail_submission_id']) ? $content['mail_submission_id'] : '')?>' type='text' class='form-control' placeholder='Masukan No Surat'>
										</div>
										
									</div>
									
									<div class="form-group">
										<label class="control-label col-md-3">Upload Surat Permohonan <span class="required">
										* </span>
										</label>
										<div class="col-md-7">
											<div class="input-group">
												<input type="text" class="form-control " placeholder="klik upload button" name="mail_submission_upload" id="mail_submission_upload" value="<?=(isset($content['mail_submission_upload']) ? $content['mail_submission_upload'] : '')?>" readonly>
												<span class="browse_files input-group-btn">
													<button class="btn btn-danger" type="button" id="mail_submission_handler">Upload</button>
												</span>
												
												
											</div>
											
											
											
										</div>
										
									</div>
									<div class="form-group">
										<label class="control-label col-md-3">Jenis Pengajuan <span class="required">
										* </span>
										</label>
										<div class="col-md-7">
											<select name='type_submission_training'  class="form-control select2me">
											  <option value=''>-Pilih-</option>
											  <?php 
											      $query = $this->init_config_db->get_type_submission_training();
											      if($query)
											      foreach($query as $row):?>
											      <?php if($row['published']):?>
												  <option value='<?=$row['id']?>' <?=(isset($content['type_submission_training']['id']) && $content['type_submission_training']['id'] == $row['id'] ? 'selected' : '')?>><?=$row['name']?></option>
											      <?php endif;?>
											  <?php endforeach;?>
											  
											</select>
										</div>
										
									</div>
									
									<div class="form-group">
										<label class="control-label col-md-3">Jenis Pelatihan <span class="required">
										* </span>
										</label>
										<div class="col-md-7">
											<select id='type_training' name='type_training' class="select2me form-control">
											  <option value=''>-Pilih-</option>
											  <?php
											      $query = $this->type_training_db->get_all();
											      foreach($query as $row):
											  ?>
											      <option value='<?=$row['_id']->{'$id'}?>' <?=(isset($content['type_training']['id']) && $content['type_training']['id'] == $row['_id']->{'$id'} ? 'selected' : '')?> ><?=$row['name']?></option>
											  <?php endforeach;?>
											</select>
											
										</div>
										
									</div>
									
									
									<div class="form-group">
										<label class="control-label col-md-3">Nama Pelatihan <span class="required">
										* </span>
										</label>
										<div class="col-md-7">
											<select class="form-control select2me" name='training_name' id='training_name'>
											  <option value=''>-Pilih-</option>
											  <?php if(isset($content['type_training']['id'])):?>
											  <?php
											      $query = $this->config_training_db->get_all(array('type_training_id' => $content['type_training']['id']));
											      foreach($query as $row):
											  ?>
											      <option value='<?=$row['_id']->{'$id'}?>' <?=(isset($content['training_name']['id']) && $content['training_name']['id'] == $row['_id']->{'$id'} ? 'selected' : '')?> ><?=$row['name']?></option>
											  <?php endforeach;?>
											  <?php endif;?>
											</select>
											
										</div>
										
									</div>
									
									<div class="form-group">
										<label class="control-label col-md-3">Tanggal Pelatihan <span class="required">
										* </span>
										</label>
										<div class="col-md-7">
											<div class='input-group date'>
											    <input name='training_date' value='<?=(isset($content['training_date']) ? $content['training_date'] : '')?>' type='text' class="form-control rangedate" />
											    <span class="input-group-addon">
												<span class="glyphicon glyphicon-calendar"></span>
											    </span>
											    <input id='start_date' type='hidden' name='training_date_start' value='<?=(isset($content['training_date_start']) ? $content['training_date_start'] : '')?>'>
											    <input id='end_date' type='hidden' name='training_date_end' value='<?=(isset($content['training_date_end']) ? $content['training_date_end'] : '')?>'>
											    
											</div>
										      
										</div>
										
									</div>
									
									<div class="form-group">
										<label class="control-label col-md-3">Jam Mulai Pelatihan <span class="required">
										* </span>
										</label>
										<div class="col-md-7">
											<div class='input-group date timepicker'>
											    <input name='start_training_time' value='<?=(isset($content['start_training_time']) ? $content['start_training_time'] : '')?>' type='text' class="form-control" />
											    <span class="input-group-addon">
												<span class="glyphicon glyphicon-calendar"></span>
											    </span>
											</div>
											<span class="help-block">Isi jam pelatihan di hari pertama, diluar jam acara pembukaan </span>
										</div>
										
										
									</div>
									<h4 class="form-section">Data Lokasi Pelatihan</h4>
									<div class="form-group">
										<label class="control-label col-md-3">Tempat Pelatihan <span class="required">
										* </span>
										</label>
										<div class="col-md-7">
											<textarea name='training_location_address'  class='form-control' placeholder='Isikan nama gedung dan alamat lengkap'><?=(isset($content['training_location_address']) ? $content['training_location_address'] : '')?></textarea>
											<span class="help-block">Isikan nama gedung dan alamat lengkap </span>
										</div>										
									</div>
									
									<div class="form-group">
										<label class="control-label col-md-3">Provinsi <span class="required">
										* </span>
										</label>
										<div class="col-md-7">
											<select name='training_location_province' class="form-control select2me" id='province'>
											  <option value=''>-Pilih-</option>
											  <?php
											      $query = file_get_contents($this->config->item('connect_api_url').'/location/province');
											      $province = array();
											      if($query){
												  $province = json_decode($query,true);
											      }
											      
											      if($province)
											      foreach($province as $row):
											  ?>
											      <option value='<?=$row['id']?>' <?=(isset($content['training_location_province']['id']) && $content['training_location_province']['id'] == $row['id'] ? 'selected' : '')?> ><?=$row['name']?></option>
											  <?php
											  endforeach;
											  ?>
											  
											</select>
										</div>
										
									</div>
									
									<div class="form-group">
										<label class="control-label col-md-3">Kab / Kota <span class="required">
										* </span>
										</label>
										<div class="col-md-7">
											<select name='training_location_district' class="form-control select2me" id='district'>
											  <option value=''>-Pilih-</option>
											  <?php if(isset($content['training_location_province']['id'])):?>
											  <?php
											      $query = file_get_contents($this->config->item('connect_api_url').'/location/district/'.$content['training_location_province']['id']);
											      $province = array();
											      if($query){
												  $province = json_decode($query,true);
											      }
											      
											      if($province)
											      foreach($province as $row):
											  ?>
											      <option value='<?=$row['id']?>' <?=(isset($content['training_location_district']['id']) && $content['training_location_district']['id'] == $row['id'] ? 'selected' : '')?> ><?=$row['name']?></option>
											  <?php
											  endforeach;
											  ?>
											  <?php endif;?>
											</select>
										</div>
										
									</div>
									
									<div class="form-group">
										<label class="control-label col-md-3">Jumlah Kelas <span class="required">
										* </span>
										</label>
										<div class="col-md-7">
											<input name='class_amount' value='<?=(isset($content['class_amount']) ? $content['class_amount'] : '')?>' type='text' class='form-control' placeholder=''>
											
										</div>
										
									</div>
									
									
									
									<h4 class="form-section">Data Peserta</h4>
									<div class="form-group">
										<label class="control-label col-md-3">Pendaftaran Peserta <span class="required">
										* </span>
										</label>
										<div  class="col-md-7">
											<select name='type_registration_participant' class="select2me form-control">
											  <option value=''>-Pilih-</option>
											  <?php 
											      $query = $this->init_config_db->get_type_registration_participant();
											      if($query)
											      foreach($query as $row):?>
											      <?php if($row['published']):?>
												  <option value='<?=$row['id']?>' <?=(isset($content['type_registration_participant']['id']) && $content['type_registration_participant']['id'] == $row['id'] ? 'selected' : '')?> ><?=$row['name']?></option>
											      <?php endif;?>
											  <?php endforeach;?>
											  
											</select>
										</div>
										
									</div>
									
									
									
									
									
									<div class="form-group">
										<label class="control-label col-md-3">Jumlah Peserta <span class="required">
										* </span>
										</label>
										<div class="col-md-7">
											<input name='participant_amount' value='<?=(isset($content['participant_amount']) ? $content['participant_amount'] : '')?>' type='text' class='form-control' placeholder=''>
											<span class="help-block">Max 50 peserta untuk 1 kelas </span>
										</div>
										
									</div>
									
									
									<h4 class="form-section">Data Panitia</h4>
									<div class="form-group">
										<label class="control-label col-md-3">Contact Person Nama <span class="required">
										* </span>
										</label>
										<div class="col-md-7">
											<input name='contact_person_name' value='<?=(isset($content['contact_person_name']) ? $content['contact_person_name'] : $this->connect_auth->get_me()['fullname'])?>' type='text' class='form-control' placeholder=''>
											
										</div>
										
									</div>
									
									<div class="form-group">
										<label class="control-label col-md-3">Contact Person No HP <span class="required">
										* </span>
										</label>
										<div class="col-md-7">
											<input name='contact_person_phone' value='<?=(isset($content['contact_person_phone']) ? $content['contact_person_phone'] : $this->connect_auth->get_me()['phone'])?>' type='text' class='form-control' placeholder=''>
											
										</div>
										
									</div>
									
								</div>
								<div class="form-actions">
									<div class="row">
										<div class="col-md-offset-3 col-md-9">
											<button type="submit" class="btn btn-primary" name='submit' value='save'>Kirim</button>
											
											<?php if(isset($content['verified_submission_training']) && $content['verified_submission_training']!= -1):?>
											<button type="submit" class="btn btn-warning" name='submit' value='draft'>Draft</button>
											 <?php endif;?>
											<a href='<?=($this->session->userdata('redirect') ? $this->session->userdata('redirect') : site_url('lpp/management_training/training'))?>' class="btn btn-default">Batal</a>
										</div>
									</div>
								</div>
							</form>
				<!-- END PAGE CONTENT-->
				
				
			</div>
		</div>
    </div>
</div>

