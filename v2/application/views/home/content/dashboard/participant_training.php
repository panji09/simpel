<!-- Being Page Title -->
<div class="container">
    <div class="row">
        <div class="col-md-3">
			<!-- Include Dashboard Menu -->
			<?php $this->load->view('home/inc/dashboard/participant_dashboard_menu')?>
			<!-- End Dashboard Menu -->
        </div>
		
		<div class="col-md-9 dashboard-container">
			<ol class="breadcrumb">
				<li><a href="#">Home</a></li>
			  	<li class="active">Approve Peserta</li>
			</ol>
			
			<div class="content-dashboard">
				<div class="row">
					<div class="col-md-6">
						<h4>Pelatihan yang diikuti</h4>
					</div>
					<div class="col-md-6"></div>
					
				</div>
				<hr/>
				<?php callback_submit_home();?>
				<div class="row">
					<div class="col-md-12">
						
						<!-- Nav tabs -->
						<ul class="nav nav-tabs" role="tablist">
						  <li role="presentation" class="active"><a href="#current_participant_training_list" aria-controls="home" role="tab" data-toggle="tab">yang diikuti</a></li>
						  <li role="presentation"><a href="#history_participant_training" aria-controls="profile" role="tab" data-toggle="tab">History</a></li>
						  
						</ul>

						<!-- Tab panes -->
						<div class="tab-content">
						  <div role="tabpanel" class="tab-pane active" id="current_participant_training_list">
						  <h4>approval</h4>
						  
						  <table class="table table-bordered table-striped table-hover lpp_approval_participant_list" >
							<thead>
								<tr>
									<th>Tanggal Daftar</th>
									<th>Nama Peserta</th>
									<th>Pelatihan</th>
									<th>Tanggal Pelatihan</th>
									<th>tempat Pelatihan</th>
									<th>Status</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<?php
								    $query = $this->join_training_db->get_all(array(
									'user_join_id' => $this->connect_auth->get_me()['user_id'],
									'institute_approved' => 1
								    ));
								    
								    if($query)
								    foreach($query as $row):
								    
								?>
						    
								<tr>
									<td><time class="timeago" datetime="<?=date('c',$row['created'])?>"><?=date("F j, Y, g:i a",$row['created']) ?></td>
									<td><?=$row['user_join']['fullname']?></td>
									<td>
										<a href="#" 
										class="review_training_submission"
										data-toggle="modal"
										data-training_id="<?=$row['training_id']?>"
										data-target="#training_submission">
										<?=$row['training_log']['type_training']['name']?> / <?=$row['training_log']['training_name']['name']?></a>
									</td>
									<td><?=$row['training_log']['training_date']?></td>
									<td><?=$row['training_log']['training_location_address']?>, <?=$row['training_log']['training_location_district']['name']?> - <?=$row['training_log']['training_location_province']['name']?></td>
									<td>
									<?php
									    $verifikasi = array();
									    
									    if(isset($row['institute_approved']))
									    switch($row['institute_approved']){
										case 0 : $verifikasi = array('name' => 'Menunggu Verifikasi', 'label' => 'label-warning'); break;
										case 1 : $verifikasi = array('name' => 'Disetujui', 'label' => 'label-success'); break;
										case -1 : $verifikasi = array('name' => 'Klarifikasi', 'label' => 'label-warning'); break;
										case -2 : $verifikasi = array('name' => 'Tolak', 'label' => 'label-danger'); break;
										default : $verifikasi = array('name' => 'Menunggu Verifikasi', 'label' => 'label-warning'); break;
									    }
									?>
									<span class="label label-sm <?=$verifikasi['label']?>"> <?=$verifikasi['name']?>
									</td>
									<td>
										<div class="margin-bottom-5">
										    <button type="button" 
											class="btn btn-xs btn-primary overview_join_participant" 
											data-toggle="modal"
											data-join_user_id="<?=$row['user_join']['user_id']?>"
											data-join_training_id="<?=$row['_id']->{'$id'}?>"
											data-url_submit="<?=site_url('lpp/management_training/approval_participant_post/'.$row['_id']->{'$id'})?>"
											data-target="#participant_info_modal">Overview</button>
										</div>
										
										<?php if(isset($row['institute_approved']) && $row['institute_approved']==-1 && $_SERVER['SERVER_NAME'] != 'simpel.lkpp.go.id'):?>
										<?php $training_id = $row['training_log']['_id']->{'$id'};?>
										<div class="margin-bottom-5">
										    <button type="button" 
											class="btn btn-xs btn-warning" 
											data-training_id='<?=$training_id?>' 
											id='register_training' 
											data-toggle="modal" 
											data-load_page="<?=site_url('training/check_requirement_training_ajax/'.$training_id.'/'.$join_training_id = $row['_id']->{'$id'})?>" 
											data-url_submit="<?=site_url('training/join_post/'.$training_id.'/'.$join_training_id)?>"
											data-target="#training_requirement"
											>Update</button>
											
											
										</div>
										
										
										<?php endif;?>
										
									</td>
								</tr>
								<?php endforeach;?>
							</tbody>
						</table>
						  </div>
						  <div role="tabpanel" class="tab-pane" id="history_participant_training">
						  <h4>History</h4>
						  
						  <table class="table table-bordered table-striped table-hover lpp_approval_participant_list" >
							<thead>
								<tr>
									<th>Tanggal Daftar</th>
									<th>Nama Peserta</th>
									<th>Pelatihan</th>
									<th>Tanggal Pelatihan</th>
									<th>tempat Pelatihan</th>
									<th>Status</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<?php
								    $query = $this->join_training_db->get_all(array(
									'user_join_id' => $this->connect_auth->get_me()['user_id']
								    ));
								    
								    if($query)
								    foreach($query as $row):
								    
								?>
						    
								<tr>
									<td><time class="timeago" datetime="<?=date('c',$row['created'])?>"><?=date("F j, Y, g:i a",$row['created']) ?></td>
									<td><?=$row['user_join']['fullname']?></td>
									<td>
										<a href="#" 
										class="review_training_submission"
										data-toggle="modal"
										data-training_id="<?=$row['training_id']?>"
										data-target="#training_submission">
										<?=$row['training_log']['type_training']['name']?> / <?=$row['training_log']['training_name']['name']?></a>
									</td>
									<td><?=$row['training_log']['training_date']?></td>
									<td><?=$row['training_log']['training_location_address']?>, <?=$row['training_log']['training_location_district']['name']?> - <?=$row['training_log']['training_location_province']['name']?></td>
									<td>
									<?php
									    $verifikasi = array();
									    
									    if(isset($row['institute_approved']))
									    switch($row['institute_approved']){
										case 0 : $verifikasi = array('name' => 'Menunggu Verifikasi', 'label' => 'label-warning'); break;
										case 1 : $verifikasi = array('name' => 'Disetujui', 'label' => 'label-success'); break;
										case -1 : $verifikasi = array('name' => 'Klarifikasi', 'label' => 'label-warning'); break;
										case -2 : $verifikasi = array('name' => 'Tolak', 'label' => 'label-danger'); break;
										default : $verifikasi = array('name' => 'Menunggu Verifikasi', 'label' => 'label-warning'); break;
									    }
									?>
									<span class="label label-sm <?=$verifikasi['label']?>"> <?=$verifikasi['name']?>
									</td>
									<td>
										<div class="margin-bottom-5">
										    <button type="button" 
											class="btn btn-xs btn-primary overview_join_participant" 
											data-toggle="modal"
											data-join_user_id="<?=$row['user_join']['user_id']?>"
											data-join_training_id="<?=$row['_id']->{'$id'}?>"
											data-url_submit="<?=site_url('lpp/management_training/approval_participant_post/'.$row['_id']->{'$id'})?>"
											data-target="#participant_info_modal">Overview</button>
										</div>
										
										<?php if(isset($row['institute_approved']) && $row['institute_approved']==-1 && $_SERVER['SERVER_NAME'] != 'simpel.lkpp.go.id'):?>
										<?php $training_id = $row['training_log']['_id']->{'$id'};?>
										<div class="margin-bottom-5">
										    <button type="button" 
											class="btn btn-xs btn-warning" 
											data-training_id='<?=$training_id?>' 
											id='register_training' 
											data-toggle="modal" 
											data-load_page="<?=site_url('training/check_requirement_training_ajax/'.$training_id.'/'.$join_training_id = $row['_id']->{'$id'})?>" 
											data-url_submit="<?=site_url('training/join_post/'.$training_id.'/'.$join_training_id)?>"
											data-target="#training_requirement"
											>Update</button>
											
											
										</div>
										
										
										<?php endif;?>
										
									</td>
								</tr>
								<?php endforeach;?>
							</tbody>
						</table>
						  
						  </div>
						  
						</div>
						
						
					</div>
				</div>
			</div>
		</div>
    </div>
</div>

