<!-- Being Page Title -->
<div class="container">
    <div class="row">
        <div class="col-md-3">
			<!-- Include Dashboard Menu -->
			<?=$this->load->view('home/inc/dashboard/instructor_dashboard_menu')?>
			<!-- End Dashboard Menu -->
        </div>
		
		<div class="col-md-9 dashboard-container">
			<ol class="breadcrumb">
				<li><a href="#">Home</a></li>
			  	<li class="active">History Sertifikasi</li>
			</ol>
			
			<div class="content-dashboard">
				<!-- BEGIN PAGE CONTENT-->
				<div class="row">
					<div class="col-md-12">
						<?php callback_submit_home();?>
					</div>
					
					<div class="col-md-12">
						<table class="table table-striped table-bordered table-hover" id="table_lpp_training">
							<thead>
								<tr>
									<th>Tanggal Pelatihan</th>
									<th>Nama Pelatihan</th>
									<th>Status</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<?php
						    
								    $query = $this->training_db->get_all();
						    
								    if($query):
									foreach($query as $row):
							    
								?>
								<tr class="odd gradeX">
								    <td><?=$row['training_date']?></td>
								    <td><?=$row['type_training']['name'].' / '.$row['training_name']['name']?></td>
								    								   								  
								    <td>
									<?php
									    $status = array();
									    
									    if(isset($row['draft'])){
										$status = array('name' => 'draft', 'label' => 'label-warning');
									    }
									    
									    if(isset($row['verified_submission_training']))
									    switch($row['verified_submission_training']){
										case 0 : 
										case -1 : 
										case -2 : $status = array('name' => 'Tahap Pengajuan', 'label' => 'label-warning'); break;
										case 1 : $status = array('name' => 'Tahap Persiapan', 'label' => 'label-warning'); break;
										
										
									    }
									?>
									<span class="label label-sm <?=$status['label']?>"><?=$status['name']?></span>
									
									<?php if(isset($row['rescheduled']) && $row['rescheduled']):
									    $status = array('name' => 'Reschedule', 'label' => 'label-default');
									?>
									
									<span class="label label-sm <?=$status['label']?>"><?=$status['name']?></span>

									<?php endif;?>
								    </td>
								    <td>
									<div class="btn-group" role="group" aria-label="...">
									    
									    
									    
									    <button type="button" class="btn btn-sm btn-primary review_training_submission" data-toggle="modal" data-training_id='<?=$row['_id']->{'$id'}?>' data-target="#training_submission">Lihat</button>
									    
									    <?php
										//tolak
										if($row['verified_submission_training']!= -2):
									    ?>
									    <?php
										//reschedule
										if($row['verified_submission_training']==1):
									    ?>
										<button type="button" class="btn btn-sm btn-warning" onclick="location.href='<?=site_url('lpp/management_training/training/reschedule/'.$row['_id']->{'$id'})?>'">Reschedule</button>
									    <?php endif;?>
									    
									    <?php if(isset($row['verified_submission_training']) && ($row['verified_submission_training'] == '-1' || $row['verified_submission_training'] == '-2')):?>
									    <button type="button" class="btn btn-sm btn-warning" onclick="location.href='<?=site_url('lpp/management_training/training/edit/'.$row['_id']->{'$id'})?>'">Edit</button>
									    
									    <button type="button" class="btn btn-sm btn-danger delete-user" data-url-delete="<?=site_url('lpp/management_training/training/delete/'.$row['_id']->{'$id'})?>" data-toggle="modal" data-target="#delete">Hapus</button>
									    <?php endif;?>
									    
									    <?php endif; //tolak?>
									</div>

								    </td>
								</tr>
							
								    <?php endforeach;?>
								<?php endif;?>
							</tbody>
						</table>
					</div>
				</div>
				<!-- END PAGE CONTENT-->
			</div>
		</div>
    </div>
</div>

