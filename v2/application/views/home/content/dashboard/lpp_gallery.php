<!-- Being Page Title -->
<div class="container">
    <div class="row">
        <div class="col-md-3">
			<!-- Include Dashboard Menu -->
			<?=$this->load->view('home/inc/dashboard/lpp_dashboard_menu')?>
			<!-- End Dashboard Menu -->
        </div>
		
		<div class="col-md-9 dashboard-container">
			<ol class="breadcrumb">
				<li><a href="#">Halaman</a></li>
			  	<li class="active"><?=$title_page?></li>
			</ol>
			
			<div class="content-dashboard">
				
				
				<!-- BEGIN PAGE CONTENT-->
				<div class="row">
					<div class="col-md-12">
						<?php callback_submit_home();?>
						
						
					</div>
					
					
					<div class="col-md-12">
					<div class="portlet light">
				    <div class="portlet-title tabbable-line">
					<div class="caption caption-md">
					    <i class="icon-globe theme-font-color hide"></i>
					    <span class="caption-subject theme-font-color bold uppercase">Halaman Galeri Foto</span>
					</div>
					<ul class="nav nav-tabs">
					    <li class="active">
						<a href="#tab_submission_training_approval" data-toggle="tab">Disetujui </a>
					    </li>
					    <li>
						<a href="#tab_submission_training_history" data-toggle="tab">Riwayat </a>
					    </li>
					</ul>
				    </div>
				    
				    <div class="portlet-body">
					<div class="tab-content">
					    <div class="tab-pane active" id="tab_submission_training_approval">
						<!-- approval-->
						<div class="portlet light">
							<div class="portlet-title">
								<div class="caption">
									<i class="icon-speech"></i>
									<span class="caption-subject bold uppercase"> Disetujui</span>
								</div>
								<div class="actions">
									<a href="<?=site_url('lpp/pages/gallery/add')?>" class="btn btn-circle btn-default ">
									Tambah <i class="fa fa-plus"></i>
									</a>
									
									<a href="javascript:;" class="btn btn-circle btn-default btn-icon-only fullscreen"></a>
								</div>
							</div>
							<div class="portlet-body">
							<table class="table table-striped table-bordered table-hover table_news" >
							<thead>
								<tr>
									<th>Tanggal</th>
									<th>Album</th>
									<th>Status</th>
									<th>Terbitkan</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<?php
						    
								    $query = $this->dynamic_pages_db->get_all(array(
									'page_id'=>'gallery', 
									'user_id' =>  $this->connect_auth->get_user_id(),
									'published' => 1
									));
						    
								    if($query):
									foreach($query as $row):
							    
								?>
								<tr class="odd gradeX">
								    <td><time class="timeago" datetime="<?=date('c',$row['created'])?>"><?=date("F j, Y, g:i a",$row['created']) ?></time></td>
								    <td><?=$row['title']?></td>
								    <td><?=$row['author']['fullname']?></td>
								    <td>
									<span class="label label-sm <?=($row['published'] ? 'label-success' : 'label-danger')?>"> <?=($row['published'] ? 'Ya' : 'Tidak')?> </span>
								    </td>
								    <td>
									<div class="btn-group" role="group" aria-label="...">
									    <button type="button" class="btn btn-sm btn-primary" onclick="location.href='<?=site_url('lpp/pages/gallery/edit/'.$row['_id']->{'$id'})?>'">Edit</button>
									    <button type="button" class="btn btn-sm btn-danger delete-user" data-url-delete="<?=site_url('lpp/pages/gallery/delete/'.$row['_id']->{'$id'})?>" data-toggle="modal" data-target="#delete">Delete</button>
									</div>
									    
									    
								    </td>
								</tr>
							
								    <?php endforeach;?>
								<?php endif;?>
							</tbody>
						</table>
							</div>
						</div>
						<!-- ./approval-->
				
				
					    </div>
					    
					    <div class="tab-pane" id="tab_submission_training_history">
						<!-- history-->
						<div class="portlet light">
							<div class="portlet-title">
								<div class="caption">
									<i class="icon-speech"></i>
									<span class="caption-subject bold uppercase"> Riwayat</span>
								</div>
								<div class="actions">
									<a href="<?=site_url('lpp/pages/gallery/add')?>" class="btn btn-circle btn-default ">
									Tambah <i class="fa fa-plus"></i>
									</a>
									
									<a href="javascript:;" class="btn btn-circle btn-default btn-icon-only fullscreen"></a>
								</div>
							</div>
							<div class="portlet-body">
							<table class="table table-striped table-bordered table-hover table_news" >
							<thead>
								<tr>
									<th>Tanggal</th>
									<th>Album</th>
									<th>Status</th>
									<th>Terbitkan</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<?php
						    
								    $query = $this->dynamic_pages_db->get_all(array('page_id'=>'gallery', 'user_id' =>  $this->connect_auth->get_user_id()));
						    
								    if($query):
									foreach($query as $row):
							    
								?>
								<tr class="odd gradeX">
								    <td><time class="timeago" datetime="<?=date('c',$row['created'])?>"><?=date("F j, Y, g:i a",$row['created']) ?></time></td>
								    <td><?=$row['title']?></td>
								    <td><?=$row['author']['fullname']?></td>
								    <td>
									<span class="label label-sm <?=($row['published'] ? 'label-success' : 'label-danger')?>"> <?=($row['published'] ? 'Ya' : 'Tidak')?> </span>
								    </td>
								    <td>
									<div class="btn-group" role="group" aria-label="...">
									    <button type="button" class="btn btn-sm btn-primary" onclick="location.href='<?=site_url('lpp/pages/gallery/edit/'.$row['_id']->{'$id'})?>'">Edit</button>
									    <button type="button" class="btn btn-sm btn-danger delete-user" data-url-delete="<?=site_url('lpp/pages/gallery/delete/'.$row['_id']->{'$id'})?>" data-toggle="modal" data-target="#delete">Delete</button>
									</div>
									    
									    
								    </td>
								</tr>
							
								    <?php endforeach;?>
								<?php endif;?>
							</tbody>
						</table>
							</div>
						</div>
						<!-- ./history-->
					    </div>
					    
					</div>  
				    </div>
					</div>
				</div>
				
				</div>
				<!-- END PAGE CONTENT-->
				
				
			</div>
		</div>
    </div>
</div>

