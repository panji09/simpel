<!-- Being Page Title -->
<div class="container">
    <div class="row">
        <div class="col-md-3">
			<!-- Include Dashboard Menu -->
			<?=$this->load->view('home/inc/dashboard/lpp_dashboard_menu')?>
			<!-- End Dashboard Menu -->
        </div>
		
		<div class="col-md-9 dashboard-container">
			<ol class="breadcrumb">
				<li><a href="#">Pelatihan</a></li>
			  	<li class="active"><?=$title_page?></li>
			</ol>
			
			<div class="content-dashboard">
				
				<h4>tanggal 2015-11-1 - 2015-11-20, nama pelatihan</h4>
				<div class="row bs-wizard" style="border-bottom:0;">
				    
				    <div class="col-xs-3 bs-wizard-step complete">
				      <div class="text-center bs-wizard-stepnum">tahap pengajuan</div>
				      <div class="progress"><div class="progress-bar"></div></div>
				      <a href="#" class="bs-wizard-dot"></a>
				      <div class="bs-wizard-info text-center">Lorem ipsum dolor sit amet.</div>
				    </div>
				    
				    <div class="col-xs-3 bs-wizard-step complete"><!-- complete -->
				      <div class="text-center bs-wizard-stepnum">tahap persiapan</div>
				      <div class="progress"><div class="progress-bar"></div></div>
				      <a href="#" class="bs-wizard-dot"></a>
				      <div class="bs-wizard-info text-center">Nam mollis tristique erat vel tristique. Aliquam erat volutpat. Mauris et vestibulum nisi. Duis molestie nisl sed scelerisque vestibulum. Nam placerat tristique placerat</div>
				    </div>
				    
				    <div class="col-xs-3 bs-wizard-step active"><!-- complete -->
				      <div class="text-center bs-wizard-stepnum">tahap pelaksanaan</div>
				      <div class="progress"><div class="progress-bar"></div></div>
				      <a href="#" class="bs-wizard-dot"></a>
				      <div class="bs-wizard-info text-center">Integer semper dolor ac auctor rutrum. Duis porta ipsum vitae mi bibendum bibendum</div>
				    </div>
				    
				    <div class="col-xs-3 bs-wizard-step disabled"><!-- active -->
				      <div class="text-center bs-wizard-stepnum">tahap evaluasi</div>
				      <div class="progress"><div class="progress-bar"></div></div>
				      <a href="#" class="bs-wizard-dot"></a>
				      <div class="bs-wizard-info text-center"> Curabitur mollis magna at blandit vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae</div>
				    </div>
				</div>
				  
				  
			  </div>
			</div>
		</div>
    </div>
</div>

