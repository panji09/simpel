<?php
   $token = $this->connect_auth->get_access_token();
   $query = $this->training_db->get($training_id);
   if($query){
   	$training = $query[0];
   	$user = $training['user_submission'];
   }
   
   $on_progress = ($this->uri->segment(6) ? true : false);
?>
<!-- Being Page Title -->
<div class="container">
    <div class="row">
        <div class="col-md-3">
			<!-- Include Dashboard Menu -->
			<?=$this->load->view('home/inc/dashboard/lpp_dashboard_menu')?>
			<!-- End Dashboard Menu -->
        </div>
		
		<div class="col-md-9 dashboard-container">
			<ol class="breadcrumb">
				<li><a href="#">Pelatihan</a></li>
				<li><a href="#"><?=$title_page?></a></li>
				
			  	<li class="active"><?=$title_content?></li>
			  	
			</ol>
			</ol>
			
			<?php callback_submit_home();?>
			<div class="content-dashboard">
				
				<?php if(isset($content['verified_submission_training']) && $content['verified_submission_training']== -1):?>
				<div class="alert alert-warning alert-dismissible" role="alert">
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				  <?=nl2br($content['review_note'])?>
				</div>
				<?php endif;?>
				<!-- BEGIN PAGE CONTENT-->
				 <!-- BEGIN Tab Info -->
	    <div class='row'>
		<div class='col-md-12'>
		</div>
	    </div>
            <div class="tabbable tabbable-custom tabbable-noborder" id='overview_detail_training'>
               <ul class="nav nav-tabs " id='overview_detail_training_tab'>
                  <li class="active">
                     <a href="#tab_overview" data-toggle="tab" class="bold uppercase" data-load_page='detail_tab_overview'>
                     Overview
                     </a>
                  </li>
                  
                  <li>
                     <a href="#tab_todolist" data-toggle="tab" class="bold uppercase" data-load_page='detail_tab_todolist'>
                     Todolist
                     </a>
                  </li>
                  
                  <li>
                     <a href="#tab_instructor" data-toggle="tab" class="bold uppercase" data-load_page='detail_tab_instructor'>
                     Pengajar 
                     </a>
                  </li>
                  <li>
                     <a href="#tab_participant" data-toggle="tab" class="bold uppercase" data-load_page='detail_tab_participant'>
                     Peserta 
                     </a>
                  </li>
                  
                  <li>
                     <a href="#tab_score_training" data-toggle="tab" class="bold uppercase" data-load_page='detail_tab_score_training'>
                     Nilai Pelatihan 
                     </a>
                  </li>
                  
                  <li>
                     <a href="#tab_evaluation" data-toggle="tab" class="bold uppercase" data-load_page='detail_tab_evaluation'>
                     Evaluasi 
                     </a>
                  </li>
                  
                  <li>
                     <a href="#tab_certificate_request" data-toggle="tab" class="bold uppercase" data-load_page='detail_tab_certificate_request'>
                     Permintaan Sertifikat 
                     </a>
                  </li>
                  
               </ul>
               
               <!--tab overview-->
               <div class="tab-content">
                  <div class="tab-pane active" id="tab_overview">
		      
                  </div>
                  <!--./tab overview-->
                  
                  <!-- pengajar-->
                  <div class="tab-pane" id="tab_instructor">
                  
                  </div>
                  <!-- ./pengajar-->
                  
                  <!-- peserta-->
                   <div class="tab-pane" id="tab_participant">
                   
		  </div>
                  <!-- ./peserta-->
                  
                  <!-- evaluasi-->
                  <div class="tab-pane" id="tab_evaluation">
                  
                  </div>
                  <!-- ./evaluasi-->
                  
                  <!--todolist-->
                  <div class="tab-pane" id="tab_todolist">
                  
                  </div>
                  <!--./todolist-->
                  
                  <!--score training-->
                   <div class="tab-pane" id="tab_score_training">
                   	    
                  </div>
                  <!--./score training-->
                  
                  
		  
		  <div class='tab-pane' id='tab_certificate_request'>
		  
		  </div>
               </div>
            </div>
            <!-- END Tab Info -->
				<!-- END PAGE CONTENT-->
				
				
			</div>
		</div>
    </div>
</div>

