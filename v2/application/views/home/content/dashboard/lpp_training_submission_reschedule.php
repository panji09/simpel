<?php
    $page = $this->uri->segment(3);
    $state = $this->uri->segment(4);
    $query = $user = array();
    if($state == 'reschedule' ){
	$query = $this->training_db->get($training_id);
	if($query)
	    $content = $query[0];
    }
?>
<!-- Being Page Title -->
<div class="container">
    <div class="row">
        <div class="col-md-3">
			<!-- Include Dashboard Menu -->
			<?=$this->load->view('home/inc/dashboard/lpp_dashboard_menu')?>
			<!-- End Dashboard Menu -->
        </div>
		
		<div class="col-md-9 dashboard-container">
			<ol class="breadcrumb">
				<li><a href="#">Pelatihan</a></li>
				<li><a href="#"><?=$title_page?></a></li>
				
			  	<li class="active"><?=$title_content?></li>
			  	
			</ol>
			</ol>
			
			<div class="content-dashboard">
				
				
				<!-- BEGIN PAGE CONTENT-->
				<form action="<?=site_url('lpp/management_training/training_post/'.$training_id)?>" id="form_sample_3" method='post' class="form-horizontal">
								<div class="form-body">
									<h3 class="form-section">Lengkapi kolom dibawah ini</h3>
									
									<h4 class="form-section">Data Pengajuan Pelatihan</h4>
									
									
									<div class="form-group">
										<label class="control-label col-md-3">Tanggal Pelatihan <span class="required">
										* </span>
										</label>
										<div class="col-md-7">
											<div class='input-group date'>
											    <input name='training_date' value='<?=(isset($content['training_date']) ? $content['training_date'] : '')?>' type='text' class="form-control rangedate" />
											    <span class="input-group-addon">
												<span class="glyphicon glyphicon-calendar"></span>
											    </span>
											    <input id='start_date' type='hidden' name='training_date_start' value='<?=(isset($content['training_date_start']) ? $content['training_date_start'] : '')?>'>
											    <input id='end_date' type='hidden' name='training_date_end' value='<?=(isset($content['training_date_end']) ? $content['training_date_end'] : '')?>'>
											    
											</div>
										      
										</div>
										
									</div>
									
									<div class="form-group">
										<label class="control-label col-md-3">Jam Mulai Pelatihan <span class="required">
										* </span>
										</label>
										<div class="col-md-7">
											<div class='input-group date timepicker'>
											    <input name='start_training_time' value='<?=(isset($content['start_training_time']) ? $content['start_training_time'] : '')?>' type='text' class="form-control" />
											    <span class="input-group-addon">
												<span class="glyphicon glyphicon-calendar"></span>
											    </span>
											</div>
											<span class="help-block">Isi jam pelatihan di hari pertama </span>
										</div>
										
										
									</div>
									
									<div class="form-group">
										<label class="control-label col-md-3">No Surat Permohonan <span class="required">
										* </span>
										</label>
										<div class="col-md-7">
											<input name='mail_submission_reschedule_id' value='<?=(isset($content['mail_submission_reschedule_id']) ? $content['mail_submission_reschedule_id'] : '')?>' type='text' class='form-control' placeholder='Masukan No Surat'>
										</div>
										
									</div>
									
									<div class="form-group">
										<label class="control-label col-md-3">Upload Surat Permohonan <span class="required">
										* </span>
										</label>
										<div class="col-md-7">
											<div class="input-group">
												<input type="text" class="form-control " placeholder="klik upload button" name="mail_submission_reschedule_upload" id="mail_submission_reschedule_upload" value="<?=(isset($content['mail_submission_reschedule_upload']) ? $content['mail_submission_reschedule_upload'] : '')?>" readonly>
												<span class="browse_files input-group-btn">
													<button class="btn btn-danger" type="button" id="mail_submission_reschedule_handler">Upload</button>
												</span>
												
												
											</div>
											
											
											
										</div>
										
									</div>
									
									<div class="form-group last">
										<label class="control-label col-md-3">Alasan <span class="required">
										* </span>
										</label>
										<div class="col-md-9">
											<textarea id='wysiwyg-content-full' class="form-control" name="content" rows="6" data-error-container="#editor2_error"><?=(isset($content['content']) ? $content['content'] : '' )?></textarea>
											<div id="editor2_error">
											</div>
										</div>
									</div>
									
								</div>
								<div class="form-actions">
									<div class="row">
										<div class="col-md-offset-3 col-md-9">
											<button type="submit" class="btn btn-primary" name='submit' value='reschedule'>Reschedule</button>
											
											<a href='<?=($this->session->userdata('redirect') ? $this->session->userdata('redirect') : site_url('lpp/management_training/training'))?>' class="btn btn-default">Batal</a>
										</div>
									</div>
								</div>
							</form>
				<!-- END PAGE CONTENT-->
				
				
			</div>
		</div>
    </div>
</div>

