<?php
	    $query = $content = array();
	    $query = $this->training_db->get($training_id);
	    $input = $this->input->get();
		if(isset($input['invite_participant_code'])){
				$this->session->set_userdata('invite_participant_code', $input['invite_participant_code']);
		}
		
		
	    if($this->session->userdata('invite_participant_code')){
				$input['invite_participant_code'] = $this->session->userdata('invite_participant_code');
		}
		
	    if($query):
	$content = $query[0];
	$query1 = $this->config_training_db->get($content['training_name']['id']);
	
	$me = ($this->connect_auth->get_access_token() ? $this->connect_auth->get_me() : false);
	$config_training = $query1[0];
?>
<!-- Being Page Title -->
<div class="container">
    <div class="page-title clearfix">
        <div class="row">
            <div class="col-md-12">
                <h6><a href="<?= site_url();?>">Home</a></h6>
                <h6><a href="<?= site_url('schedule');?>">Jadwal Pelatihan</a></h6>
                <h6><span class="page-active">Pelatihan Detail</span></h6>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <!-- Here begin Main Content -->
        <div class="col-md-12">
			<div class="widget-main">
                <div class="widget-inner">
					<div class="training-title">
                    	<h3 class="blog-post-title"><?=$content['training_name']['name']?></h3>
						<p class="event-small-meta small-text"><?=$content['type_training']['name']?></p>
					</div>
					
					<div class="row">
					    <?php callback_submit_home();?>
						<!-- begin left side -->
					      <div class="col-md-8">
					      <?=$config_training['training_description']?>
								  
					      <hr>
					      <h3 class="training-requirement-title">Syarat Pelatihan</h3>
					      <?=$config_training['requirement_description']?>
					      
					      <hr>
					      <h3 class="training-requirement-title">Pengajar</h3>
					      
					      <?php
						  $query = $this->hour_leasson_db->get_all(array(
						      'training_id' => $training_id,
						      'admin_approved' => 1,
						      'instructor_approved' => 1
						  ));
						  
						  
						  if($query)
						  foreach($query as $instuctor):
						 //var_dump($instuctor);
						 //echo $instuctor['instructor_id'];
						 //echo '<br>';
					      ?>
					      <div class="col-sm-6 col-md-4">
						<div class="thumbnail">
						  <img alt="100%x200" style="height: 200px; width: 100%; display: block;" src="<?=(isset($instuctor['instructor_approved_log']['photo']) && $instuctor['instructor_approved_log']['photo'] != '' ? $instuctor['instructor_approved_log']['photo'] : 'http://placehold.it/252x252?text=no+image')?>" >
							   <div class="caption">
						    <h3><?=$instuctor['instructor_approved_log']['fullname']?></h3>
						    <p>Institusi<br>
						    <?=$instuctor['instructor_approved_log']['institution']?></p>
						    <p>Unit Kerja<br>
						    <?=$instuctor['instructor_approved_log']['work_unit']?></p>
						    <hr>
						    <p><a href="<?= site_url('instructor/detail/'.$instuctor['instructor_id']);?>" class="btn btn-primary btn-block" role="button">Info</a> </p>
						  </div>
						</div>
					      </div>
					      <?php endforeach;?>
					      
					      
					      <div class="margin-bottom-0">&nbsp;</div>
						 
					</div>
						<!-- end left side -->
						
						
						
						<!-- begin right side -->
						<div class="col-md-4">
						<div class="right-side-training">
							<div class="button-join">
								
								
								<?php
								$range_date = explode(' - ', $content['training_date']);
								//print_r($range_date);
								//tanggal terakhir
								$start_date = explode('-', $range_date[1]);
								//tanggal terakhir + 1hr
								$end_training = mktime(0, 0, 0, intval($start_date[1]), intval($start_date[2]), intval($start_date[0])) + (60 * 60 * 24);
								//echo time() .' '.$end_training;
								if(time() < $end_training):
										//pendaftaran masih buka
										$enable_join_training = false;
										if($content['type_registration_participant']['id'] == 'closed'){
												//pendaftaran tertutup
												if(isset($input['invite_participant_code'])){
														$query = $this->invite_participant_db->get_all(array(
																									'invite_participant_code' => $input['invite_participant_code'],
																									'training_id' => $training_id
																									));
												
														if(count($query) == 1){
																$enable_join_training = true;
														}
												}
										}else{
												//pendaftaran terbuka
												$enable_join_training = true;
										}
								?>
								<!-- join training -->
								<?php if($enable_join_training):?>
										<button 
											type="button" 
											class="btn btn-info btn-block" 
											data-training_id='<?=$training_id?>' 
											id='register_training' 
											data-toggle="modal" 
											data-load_page="<?=site_url('training/check_requirement_training_ajax/'.$training_id)?>" 
											data-url_submit="<?=site_url('training/join_post/'.$training_id)?>"
											data-target="#training_requirement"
										>
											Daftar Pelatihan
										</button>
								<?php else:?>
										<button 
											type="button" 
											class="btn btn-danger btn-block" 
											
										>
											Pendaftaran Tidak Untuk Umum
										</button>
								<?php endif;?>
								
								<?php
										//./pendaftaran masih buka
										else:
										//pendaftaran sudah tutup
										?>
								<button 
								    type="button" 
								    class="btn btn-danger btn-block" 
								    disabled
								>
								    Pendaftaran Berakhir
								</button>
								<?php
										//./pendaftaran sudah tutup
										endif;
								?>
							</div>
							<div class="widget widget_equipment">
			                        <i class="icon glyphicon glyphicon-globe"></i>
			                        <h4>Lembaga Pelatihan</h4>
			                        <div class="equipment-body">
			                           	<?=$content['user_submission']['training_institute']?>
			                        </div>
			                    </div>
			                    <div class="widget widget_equipment">
			                        <i class="icon glyphicon glyphicon-cog"></i>
			                        <h4>Jenis Pelatihan</h4>
			                        <div class="equipment-body">
			                           	<?=$content['type_training']['name']?>
			                        </div>
			                    </div>
					    <div class="widget widget_equipment">
			                        <i class="icon glyphicon glyphicon-cog"></i>
			                        <h4>Nama Pelatihan</h4>
			                        <div class="equipment-body">
			                           	<?=$content['training_name']['name']?>
			                        </div>
			                    </div>
								
					    <div class="widget widget_equipment"> 
						<i class="icon glyphicon glyphicon-calendar"></i>
			                        <h4>Tanggal Pelatihan</h4>
			                        <div class="equipment-body">
			                            <?php
							
							$range_date = explode(' - ', $content['training_date']);
			                            
			                            ?>
			                            <?=tgl_indo($range_date[0])?> - <?=tgl_indo($range_date[1])?>
			                        </div>						
			                    </div>
			                    
			                    <div class="widget widget_equipment"> 
						<i class="icon glyphicon glyphicon-user"></i>
			                        <h4>Nama CP</h4>
			                        <div class="equipment-body">
			                            <?=$content['contact_person_name']?>
			                        </div>						
			                    </div>
			                    <div class="widget widget_equipment"> 
						<i class="icon glyphicon glyphicon-phone"></i>
			                        <h4>Telp CP</h4>
			                        <div class="equipment-body">
			                            <?=$content['contact_person_phone']?>
			                        </div>						
			                    </div>
			                    <div class="widget widget_equipment"> 
						<i class="icon glyphicon glyphicon-envelope"></i>
			                        <h4>Email CP</h4>
			                        <div class="equipment-body">
			                            <?=$content['user_submission']['email']?>
			                        </div>						
			                    </div>
						
					    <div class="widget widget_equipment"> 
						<i class="icon fa fa-map"></i>
			                        <h4>Tempat</h4>
			                        <div class="equipment-body">
			                            <?=$content['training_location_address']?>, <?=$content['training_location_province']['name']?> - <?=$content['training_location_district']['name']?>
			                        </div>						
			                    </div>
			                    
			                    <div class="widget widget_equipment">
						<i class="icon glyphicon glyphicon-home"></i>
			                        <h4>Jumlah Kelas</h4>
			                        <div class="equipment-body">
			                            <?=$content['class_amount']?>
			                        </div>						
					    </div>
					    
					    <div class="widget widget_equipment">
						<i class="icon glyphicon glyphicon-home"></i>
			                        <h4>Kuota</h4>
			                        <div class="equipment-body">
			                            <?=$content['participant_amount']?>
			                        </div>						
					    </div>
					    
					    <div class="widget widget_equipment">
						<i class="icon fa fa-file-archive-o"></i>
			                        <h4>Jumlah Jam Pelajaran</h4>
			                        <div class="equipment-body">
			                            <?=$config_training['hour_leasson']?>
			                        </div>						
					    </div>

							</div>
						</div>
						<!-- end right side -->
					</div>
					
					<hr>
						  <!-- Go to www.addthis.com/dashboard to customize your tools -->
						<!-- just test -->
						<div class="addthis_sharing_toolbox"></div>
					
                </div> <!-- /.widget-inner -->
            </div>
			
        </div>
		<!-- end Main Content -->

    </div> <!-- /.row -->
</div> <!-- /.container -->


<?php endif;?>