
<!-- Being Page Title -->
<div class="container">
    <div class="page-title clearfix">
        <div class="row">
            <div class="col-md-12">
                <h6><a href="<?= site_url();?>">Home</a></h6>
                <h6><span class="page-active">Peraturan Pelatihan</span></h6>
            </div>
        </div>
    </div>
</div>


<div class="container">
	<div class="row">
		<div class="col-md-8">

			<!-- BEGIN REGULATORY TABLE-->
			<div class="regulatory-container">
				<table class="table table-striped table-hover" id="regulatory_table">
					<thead>
						<tr>
							<th> Judul Dokumen </th>
							<th> Kategori Dokumen </th>
							<th> File Dokumen </th>
						</tr>
					</thead>
					<tbody>
						<?php
							    
						    $query = $this->dynamic_pages_db->get_all(array('page_id'=>'regulatory_training'));
						    
						    if($query):
							foreach($query as $row):
							     if($row['published']):
						?>
						<tr class="odd gradeX">
							<td> <?=$row['title']?> </td>
							<td><?=$row['category']?></td>
							<td><a target='_blank' href="<?=$row['file']?>">download</a></td>
						</tr>
							<?php endif;?>
						    <?php endforeach;?>
						<?php endif;?>

					</tbody>
				</table>
				
			</div>
			<!-- END REGULATORY TABLE-->
			

		</div>
		<!-- /.col-md-12 -->
		<div class="col-md-4">
			<?php $this->load->view('home/inc/widget/widget_news')?>
			
			<?php $this->load->view('home/inc/widget/widget_gallery')?>
		</div>
	</div>
	<!-- /.row -->
</div>
<!-- /.container -->
