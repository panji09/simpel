<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> 
<![endif]-->
<!--[if IE 7]> <html class="no-js lt-ie9 lt-ie8" lang="en"> 
<![endif]-->
<!--[if IE 8]> <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->


<head>
    <title><?=(isset($title) ? $title : 'SIMPEL')?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Sistem Informasi Pelatihan"> 
    <meta name="author" content="Panji Adhie Musthofa - ruangpanji.com">
    <meta name="google-site-verification" content="sdkD4i1Q4bjpCIUIcMW89lGjIv4gXPGqbUtJji2VFK4" />

<?php
 //for socmed
 if (isset($meta_socmed)):
 //$query = $this->dynamic_pages_db->get($content_id);
 $query = $meta_data; 
 
 if($query):
	$document = $query[0];
	
	if(isset($document['content']))
	$content_meta = ellipse($document['content'],300);
	else
	$content_meta = "Sistem Informasi Pelatihan";
	
?>
	<meta property="og:title" content="<?=$document['title']?>">
	<meta property="og:description" content="<?=strip_tags($content_meta)?>">
	<meta property="og:type" content="website">
	<?php
	$cover = (isset($document['cover']) && $document['cover'] != '' ? $document['cover'] : $this->config->item('home_img').'/logo-lkpp.png');
	?>
	
	<meta property="og:image" content="<?=$cover?>"><!-- link to image for socio -->
	<meta property="og:url" content="<?=current_url()?>">
<?php
endif;
endif;?>	
	<meta charset="UTF-8">
    <?=$this->load->view('home/inc/script_header');?>
</head>
<body>
    <?php $this->load->view('home/inc/modal');?>
    <!-- <div class="alert alert-warning alert-dismissible" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
		  <span aria-hidden="true">&times;</span>
	  </button>
      <strong>Peringatan!</strong> Sistem ini dalam tahap pengembangan, kembali ke versi awal?
	  <a href='http://<?=$_SERVER['HTTP_HOST']?>'>
		  <strong>Ya.</strong>
	  </a>
    </div> -->
    <?=$this->load->view('home/inc/header_menu')?>
    
    
    <?=$this->load->view('home/content/'.$content)?>

    <!-- begin The Footer -->
    <?=$this->load->view('home/inc/footer')?>


    <?=$this->load->view('home/inc/script_footer')?>

</body>
</html> 
