<?php
    $query = $this->training_db->get($training_id);
    
    $me = $this->connect_auth->get_me();
    
    if(isset($join_training_id) && $join_training_id){
	$query1 = $this->join_training_db->get($join_training_id);
	if($query1){
	    $join_training = $query1[0];
	}
    }
    
    if($me && $me['entity'] == 'peserta' && $query):
	$training = $query[0];
	$query_program_training = $this->config_training_db->get($training['training_name']['id']);
	
	if($query_program_training){
	    $program_training = $query_program_training[0];
	}
?>
<div class="form-horizontal">
	<?php if(isset( $join_training['institute_approved']) && $join_training['institute_approved'] == -1):?>
	
	<div class="alert alert-warning alert-dismissible" role="alert">
	  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	  <strong>Klarifikasi!</strong> <?=( isset( $join_training['review_note']) ? $join_training['review_note'] : '' )?>
	</div>
	<?php endif;?>
	<?php if(isset( $program_training['requirement_letter_assignment']) && $program_training['requirement_letter_assignment']):?>
	<h4>Surat Tugas</h4>
	<div class="form-group">
		<label class="control-label col-md-3">No Surat Tugas <span class="required">
		* </span>
		</label>
		<div class="col-md-7">
			<input required name='letter_of_assignment_id' value='<?=(isset($join_training['letter_of_assignment_id']) ? $join_training['letter_of_assignment_id'] : '')?>' type='text' class='form-control' placeholder='Masukan No Surat Tugas'>
		</div>
		
	</div>
	
	<div class="form-group">
		<label class="control-label col-md-3">Upload Surat Tugas <span class="required">
		* </span>
		</label>
		<div class="col-md-7">
			<div class="input-group">
				<input type="text" class="form-control" placeholder="klik upload button" name="letter_of_assignment_upload" id="letter_of_assignment_upload" value="<?=(isset($join_training['letter_of_assignment_upload']) ? $join_training['letter_of_assignment_upload'] : '')?>" readonly required>
				<span class="browse_files input-group-btn">
					<button class="btn btn-danger" type="button" id="mail_submission_handler">Upload</button>
				</span>
				
				
			</div>
			
			
			
		</div>
		
	</div>
	<?php endif;?>
	
	<?php if( isset($program_training['requirement_certificate']) && $program_training['requirement_certificate']):?>
	<h4>Sertifikat Ahli Pengadaan</h4>
	<div class="form-group">
		<label class="control-label col-md-3">No Sertifikat Ahli Pengadaan<span class="required">
		* </span>
		</label>
		<div class="col-md-7">
			<input required name='certificate_procurement_expert_id' value='<?=(isset($me['procurement_certificate']) ? $me['procurement_certificate'] : '')?>' type='text' class='form-control' placeholder='Masukan No Sertifikat Ahli Pengadaan'>
		</div>
		
	</div>
	
	<div class="form-group">
		<label class="control-label col-md-3">Upload Sertifikat Ahli Pengadaan<span class="required">
		* </span>
		</label>
		<div class="col-md-7">
			<div class="input-group">
				<input required type="text" class="form-control " placeholder="klik upload button" name="certificate_procurement_expert_upload" id="certificate_procurement_expert_upload" value="<?=(isset($join_training['certificate_procurement_expert_upload']) ? $join_training['certificate_procurement_expert_upload'] : '')?>" readonly>
				<span class="browse_files input-group-btn">
					<button class="btn btn-danger" type="button" id="mail_submission_handler">Upload</button>
				</span>
				
				
			</div>
			
			
			
		</div>
		
	</div>
	<?php endif;?>
	
	
	<?php if(isset($program_training['enable_support_document']) && $program_training['enable_support_document'] ):?>
	<h4>Upload Dokumen Pendukung</h4>
	
	    <?php if(isset($join_training['require_document_name']) && $join_training['require_document_name']):?>
		<?php for($i = 0; $i < count($join_training['require_document_name']); $i++ ):?>
	    
	    <div class="form-group">
		    <div class="col-md-3">
		    <input required name='require_document_name[]' value='<?=(isset($join_training['require_document_name'][$i]) ? $join_training['require_document_name'][$i] : '')?>' type='text' class='form-control' placeholder='Nama'>
		    </div>
		    <div class="col-md-7">
			    <div class="input-group">
				    <input required type="text" class="form-control " placeholder="klik upload button" name="require_document_upload[]"  value="<?=(isset($join_training['require_document_upload'][$i]) ? $join_training['require_document_upload'][$i] : '')?>" readonly>
				    <span class="browse_files input-group-btn">
					    <button class="btn btn-danger" type="button" >Upload</button>
				    </span>
				    
				    
			    </div>
			    
			    
			    
		    </div>
		    
		    <div class="col-md-2">
			    <div class="btn-group" role="group">
			      <button type="button" class="btn add_new_document btn-default">+</button>
			      <button type="button" class="btn remove_new_document btn-default">-</button>
			      
			    </div>
		    </div>
		    
	    </div>
	    
		<?php endfor;?>
	    <?php else:?>
	    <div class="form-group">
		    <div class="col-md-3">
		    <input required name='require_document_name[]' value='' type='text' class='form-control' placeholder='Nama'>
		    </div>
		    <div class="col-md-7">
			    <div class="input-group">
				    <input required type="text" class="form-control " placeholder="klik upload button" name="require_document_upload[]"  value="" readonly>
				    <span class="browse_files input-group-btn">
					    <button class="btn btn-danger" type="button" >Upload</button>
				    </span>
				    
				    
			    </div>
			    
			    
			    
		    </div>
		    
		    <div class="col-md-2">
			    <div class="btn-group" role="group">
			      <button type="button" class="btn add_new_document btn-default">+</button>
			      <button type="button" class="btn remove_new_document btn-default">-</button>
			      
			    </div>
		    </div>
		    
	    </div>
	    
	    <?php endif;?>
	
	<?php endif;?>
	
</div> 

<?php else:?>

	<div class="form-group">
		<a href="<?=site_url('login/index/'.base64_encode(site_url('training/index/'.$training_id)))?>" class="btn btn-primary btn-lg btn-block">Login</a>
		
	</div>
	<div class="form-group">
		<a href="<?=site_url('login/register_participant/'.base64_encode(site_url('training/index/'.$training_id)))?>" class="btn btn-info btn-lg btn-block">Registrasi Peserta</a>
	</div>

<?php endif;?>