<?php
    $page = $this->uri->segment(1);
    $page1 = $this->uri->segment(2);
    
    
?>

<!-- end meta for socmed-->

<link href='http://fonts.googleapis.com/css?family=Raleway:400,100,200,300,500,600,700,800' rel='stylesheet' type='text/css'>

<!-- CSS Bootstrap & Custom -->
<link href="<?=$this->config->item('plugin')?>/bootstrap/css/bootstrap.css" rel="stylesheet" media="screen">
<link href="<?=$this->config->item('home_css')?>/animate.css" rel="stylesheet" media="screen">
<link href="<?=$this->config->item('home_css')?>/style.css" rel="stylesheet" media="screen">
<link href="<?=$this->config->item('home_css')?>/owl.carousel.css" rel="stylesheet" type="text/css">
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet" media="screen">


<!-- DATA TABLE PLUGIN -->
<link rel="stylesheet" type="text/css" href="<?=$this->config->item('plugin')?>/datatables/plugins/bootstrap/dataTables.bootstrap.css" />
<link rel="stylesheet" href="<?=$this->config->item('plugin')?>/css-spinners/css/spinner/spinner.css" type="text/css">
<link rel="stylesheet" href="<?=$this->config->item('plugin')?>/css-spinners/css/spinner/ball.css" type="text/css">
<link rel="stylesheet" href="<?=$this->config->item('plugin')?>/css-spinners/css/spinner/circles.css" type="text/css">
<!-- END DATA TABLE PLUGIN -->

<!-- Favicons -->

<link rel="shortcut icon" href="<?=$this->config->item('home_img')?>/ico/favicon.png">

<!-- JavaScripts -->
<script src="<?=$this->config->item('home_js')?>/jquery-1.10.2.min.js"></script>
<script src="<?=$this->config->item('home_js')?>/jquery-migrate-1.2.1.min.js"></script>
<script src="<?=$this->config->item('home_js')?>/modernizr.js"></script>

<?php if($page == 'lpp' && in_array($page1, array('management_training','pages'))):?>
<?php $this->load->view('home/inc/script_header_lpp_management_training');?>
<?php endif;?>

<?php if($page == 'peserta' && $page1 == 'management_training'):?>
<?php $this->load->view('home/inc/script_header_peserta_management_training');?>
<?php endif;?>

<?php if($page == 'narasumber' && $page1 == 'history'):?>
<?php $this->load->view('home/inc/script_header_instructor_story');?>
<?php endif;?>

<?php if($page == 'narasumber' && $page1 == 'management_training'):?>
<?php $this->load->view('home/inc/script_header_instructor_management_training');?>
<?php endif;?>

<?php if($page == 'pengaduan'):?>
<?php $this->load->view('home/inc/script_header_pengaduan');?>
<?php endif;?>

<!--[if lt IE 8]>
<div style=' clear: both; text-align:center; position: relative;'>
        <a href="http://www.microsoft.com/windows/internet-explorer/default.aspx?ocid=ie6_countdown_bannercode"><img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" alt="" /></a>
    </div>
<![endif]-->