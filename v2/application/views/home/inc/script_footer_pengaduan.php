<script type="text/javascript" src="<?=$this->config->item('plugin')?>/select2/select2.min.js"></script>
<script src="<?=$this->config->item('plugin')?>/Highcharts-4.1.8/js/highcharts.js"></script>
<script src="<?=$this->config->item('plugin')?>/Highcharts-4.1.8/js/modules/data.js"></script>
<script src="<?=$this->config->item('plugin')?>/Highcharts-4.1.8/js/modules/exporting.js"></script>


<script>
var URL_CONNECT_API = '<?=$this->config->item('connect_api_url')?>';
$(document).ready(function(){
    $('select.select2me').select2({
	placeholder: "-Pilih-",
	allowClear: true
    });
    
    $("#province").change(function() {
	
	$("#district").empty();
	$("#district").select2('data', {id: null, text:""});
	$("#district").append('<option value="">-pilih-</option>');
	$('#district').data("placeholder", "-Pilih-");
	$('#district').data("select2").setPlaceholder();
	
	$.get(URL_CONNECT_API+'/location/district/'+$(this).val(), function(data) {
		$.each(data, function(i, item){
		    $("#district").append(
			'<option value="' + item.id + '">'+item.name + '</option>'
		    );
		})
	    },
	    'json'
	);
    });
    
    $("#district").change(function() {
	
	$("#subdistrict").empty();
	$("#subdistrict").select2('data', {id: null, text:""});
	$("#subdistrict").append('<option value="">-pilih-</option>');
	$('#subdistrict').data("placeholder", "-Pilih-");
	$('#subdistrict').data("select2").setPlaceholder();
	
	$.get(URL_CONNECT_API+'/location/subdistrict/'+$(this).val(), function(data) {
		$.each(data, function(i, item){
		    $("#subdistrict").append(
			'<option value="' + item.id + '">'+item.name + '</option>'
		    );
		})
	    },
	    'json'
	);
    });
});
</script>
<script type="text/javascript">
	$(function () {
		$('#container_chart').highcharts({
			data: {
				table: 'datatable'
			},
			chart: {
				type: 'column'
			},
			title: {
				<?php
				if (isset($search))
				{
				?>
				text: 'Jumlah Pengaduan Tahun <?= $year;?>'
				<?php
				}
				else
				{
				?>
				text: 'Jumlah Pengaduan Berdasarkan Kategori Tahun 2011 s/d 2015'
				<?php
				}
				?>
			},
			yAxis: {
				allowDecimals: false,
				title: {
					text: 'Units'
				}
			},
			tooltip: {
				formatter: function () {
					return '<b>' + this.series.name + '</b><br/>' +
					this.point.y + ' ' + this.point.name.toLowerCase();
				}
			}
		});
	});
</script>