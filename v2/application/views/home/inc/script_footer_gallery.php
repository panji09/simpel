<!-- JS PINTEREST GRID -->
<!-- image load -->
 <script src="<?=$this->config->item('plugin')?>/imagesloaded/imagesloaded.pkgd.min.js"></script>
 <script src="<?=$this->config->item('home_js')?>/wookmark.js"></script>

 <script type="text/javascript">

  (function ($) {
    var loadedImages = 0, // Counter for loaded images
        $progressBar = $('.progress-bar'),
        container = '#container',
        $container = $(container),
		wookmark;

    // Initialize Wookmark
    wookmark = new Wookmark(container, {
      offset: 5, // Optional, the distance between grid items
      outerOffset: 10, // Optional, the distance to the containers border
      itemWidth: 210 // Optional, the width of a grid item
    });

    $container.imagesLoaded().always(function () {
		$progressBar.hide();
    })

	.done( function( instance ) {
		console.log('all images successfully loaded');
	})
	.progress(function (instance, image) {
        // Update progress bar after each image has loaded and remove loading state
        $(image.img).closest('li').removeClass('tile-loading');
        // $progressBar.css('width', (++loadedImages / tileCount * 100) + '%');
        // wookmark.updateOptions();

	  	$(".kosong").css("visibility", "visible");
	   	wookmark.updateOptions();
    });
  })(jQuery);
</script>