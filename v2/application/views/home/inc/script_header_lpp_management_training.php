<!-- BEGIN PAGE LEVEL PLUGINS -->

<link href="<?=$this->config->item('plugin')?>/select2/select2.css" rel="stylesheet" type="text/css">
<link href="<?=$this->config->item('plugin')?>/select2/select2-bootstrap.css" rel="stylesheet" type="text/css">
<link href="<?=$this->config->item('plugin')?>/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
<link href="<?=$this->config->item('plugin')?>/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css">

<link href="<?=$this->config->item('home_css')?>/progress-step.css" rel="stylesheet" type="text/css">
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet" media="screen">
<!-- END PAGE LEVEL PLUGINS -->
<link href="<?=$this->config->item('home_css')?>/rounded.css" rel="stylesheet" type="text/css">
