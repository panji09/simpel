<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="<?=$this->config->item('plugin')?>/select2/select2.min.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="<?=$this->config->item('plugin')?>/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript" src="<?=$this->config->item('plugin')?>/fancybox/source/jquery.fancybox.pack.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
<script type="text/javascript" src="<?=$this->config->item('plugin')?>/momentjs-business.js"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- Tinymce WYSIWYG Editors -->
<script type="text/javascript" src="<?=$this->config->item('plugin')?>/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
    function load_tinymce(){
	
	tinymce.init({
	    selector: "#wysiwyg-content",
		    theme: "modern",
		    skin: "light",
		    plugins:[
			    "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
			    "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
			    "save table contextmenu directionality emoticons template paste textcolor importcss"
		    ],
		    toolbar:[
			    "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media fullpage | forecolor backcolor emoticons table"
			    ],
		    file_browser_callback: function(field, url, type, win) {
		    tinyMCE.activeEditor.windowManager.open({
			file: '<?=$this->config->item("plugin")?>/kcfinder/browse.php?opener=tinymce4&field=' + field + '&type=' + type+'s',					
			title: 'KCFinder',
			width: 700,
			height: 500,
			inline: true,
			close_previous: false
		    }, {
			window: win,
			input: field
		    });
		    return false;
		    }
	});
	
	tinymce.init({
	    selector: "#wysiwyg-short",
		    theme: "modern",
		    skin: "light",
		    menubar:false,
		    statusbar: false,
		    toolbar:["bold italic underline"],
		    paste_data_images: true
	});
	
	tinymce.init({
	    selector: ".wysiwyg-short1",
		    theme: "modern",
		    skin: "light",
		    menubar:false,
		    statusbar: false,
		    toolbar:["bold italic underline | bullist numlist | link image"],
		    paste_data_images: true
	});
    }
    
</script>
<!-- END Tinymce editor -->

<script>
var URL_CONNECT_API = '<?=$this->config->item('connect_api_url')?>';
var URL_SIMPEL_API = '<?=site_url('api')?>';

var URL = null;
function getFormatDate(d){
    return d.getFullYear() + '-' + (d.getMonth() + 1) + '-' + d.getDate();
}


$(document).ready(function(){
        		/*------------------------------------------------------------------------*/
		/*	popup submission
		/*------------------------------------------------------------------------*/		
		var modal_id = '#review_training_submission';
		var modal_tab_id = '#review_training_submission_tab';
		var btn_class = '.review_training_submission';

		function load_ajax_submission(tab_id, training_id){
			var load_page = $(tab_id).find(modal_tab_id+' li.active a').attr('data-load_page');
			$(load_page).html('');
			console.log(tab_id+' '+modal_tab_id);
			$.get( "<?=site_url('lpp/management_training/training_ajax')?>/"+load_page+"/"+training_id,
			function(data) {		      
				$('#'+load_page).html( data );
			});

		}
		
		//tabnya
		$(document).on('click',modal_tab_id+' a',function(){
		    //ambil tab_id sm training_id
		    tab_id = $(modal_id).attr('data-tab_id');
		    training_id = $(modal_id).attr('data-training_id');


		    $(this).tab('show');
		    load_ajax_submission(tab_id, training_id);
		});
		
		
		//button kliknya
		$(document).on('click',btn_class,function(){
		    //setting init
		    //alert('uy');
		    $(modal_id+" form").attr('action',$(this).data('url-submit'));

		    tab_id = $(this).attr('data-target');
		    training_id = $(this).attr('data-training_id');

		    //simpen training_id sm tab_id
		    $(tab_id).attr('data-tab_id', tab_id);
		    $(tab_id).attr('data-training_id', training_id);

		    //load ajax
		    load_ajax_submission(tab_id, training_id);
		});
		
		
		//check sbmit
		$(modal_id+" form").submit(function (e) {

			// 		  e.preventDefault();
			var btn = $(this).find("button[type=submit]:focus" );
			var tab_id = $(modal_id).data('tab_id');
			var training_id = $(modal_id).data('training_id');
			var load_page = $(tab_id).find(modal_tab_id+' li.active a').data('load_page');


			if(btn.attr('id') == 'approved_training_submission_submit'){

				if(load_page != 'training_submission_approved'){
					$('#training_submission_review_note').html('');
					$('#training_submission_approved').html('');
					$(modal_tab_id+' a[href="#training_submission_approved"]').tab('show');
					load_ajax_submission(tab_id, training_id);
				}


				if($('[name=facilitator_user_id]').val() == null || $('[name=facilitator_user_id]').val() == ''){
					alert('harus diisi!');

					// 			  console.log(btn.attr('id'));
					e.preventDefault();
				}else{
					return;
				}
			}else if(btn.attr('id') == 'review_training_submission_submit'){

				if(load_page != 'training_submission_review_note'){
					$('#training_submission_review_note').html('');
					$('#training_submission_approved').html('');
					$(modal_tab_id+' a[href="#training_submission_review_note"]').tab('show');
					load_ajax_submission(tab_id, training_id);
				}


				if($('[name=review_note]').val() == null || $('[name=review_note]').val() == ''){
					alert('harus diisi!');

					// 			  console.log(btn.attr('id'));
					e.preventDefault();
				}else{
					return;
				}
			}

		});
		
		/*------------------------------------------------------------------------*/
		/*	./popup submission
		/*------------------------------------------------------------------------*/
    
    mdTemp = new Date(),
    
    maxDate = getFormatDate(new Date(mdTemp.setMonth(mdTemp.getMonth() + 3)));
    $('.rangedate').daterangepicker({
	locale: {
            format: 'YYYY-MM-DD',
	    "applyLabel": "Pilih",
	    "cancelLabel": "Batal",
	    "fromLabel": "Dari",
	    "toLabel": "Sampai",
	    "daysOfWeek": [
		"Min",
		"Sen",
		"Sel",
		"Rab",
		"Kam",
		"Jum",
		"Sab"
	    ],
	    "monthNames": [
		"Januari",
		"Februari",
		"Maret",
		"April",
		"Mei",
		"Juni",
		"Juli",
		"Agustus",
		"September",
		"Oktober",
		"November",
		"Desember"
	    ],
        },
        minDate: moment().businessAdd(14),
	maxDate: moment().add(4, 'months')
    },

    function(start, end, label) {
	$('#start_date').val(start.format('YYYY-MM-DD'));
	$('#end_date').val(end.format('YYYY-MM-DD'));
	
    }
    );
    
    $('.timepicker').datetimepicker({
	format: 'HH:mm',
	
    });
    $('.select2me').select2({
	placeholder: "-Pilih-",
	allowClear: true
    });
    
    $("#province").change(function() {
	
	$("#district").empty();
	$("#district").select2('data', {id: null, text:""});
	$("#district").append('<option value="">-pilih-</option>');
	$('#district').data("placeholder", "-Pilih-");
	$('#district').data("select2").setPlaceholder();
	
	$.get(URL_CONNECT_API+'/location/district/'+$(this).val(), function(data) {
		$.each(data, function(i, item){
		    $("#district").append(
			'<option value="' + item.id + '">'+item.name + '</option>'
		    );
		})
	    },
	    'json'
	);
    });
    
    $("#type_training").change(function() {
	$("#training_name").empty();
	$("#training_name").select2('data', {id: null, text:""});
	$("#training_name").append('<option value="">-pilih-</option>');
	$('#training_name').data("placeholder", "-Pilih-");
	$('#training_name').data("select2").setPlaceholder();
	
	
	$.get(URL_SIMPEL_API+'/training/name/'+$(this).val(), function(data) {
		$.each(data, function(i, item){
		    $("#training_name").append(
			'<option value="' + item.id + '">'+item.name + '</option>'
		    );
		})
	    },
	    'json'
	);
    });
    
	<?php
	/*
    $('.browse_files').click(function(){
		openKCFinderFiles(this);
	
    });
    */
	?>
	
	$(document).on('click','.browse_files',function(){
		openKCFinderFiles(this);
	
    });
	
    function openKCFinderFiles(div) {
// 	console.log(div);
	window.KCFinder = {
	    callBack: function(url) {
		window.KCFinder = null;
		$(div).siblings().val(url);
	    }
	};
	window.open('<?=$this->config->item("plugin")?>/kcfinder/browse.php?type=files',
	    'kcfinder_file', 'status=0, toolbar=0, location=0, menubar=0, ' +
	    'directories=0, resizable=1, scrollbars=0, width=800, height=600'
	);
    }
});
</script> 