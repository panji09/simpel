<?php
    $page = $this->uri->segment(2);
?>

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>

<!-- END PAGE LEVEL PLUGINS -->


<?php
   if($page == 'chart'):
?>
<script>
$(function () {
    $('#container').highcharts({
        chart: {
            type: 'bar'
        },
        title: {
            text: 'Narasumber / Pengajar'
        },
        subtitle: {
            text: 'Pengadaan Barang / Jasa (PBJ)'
        },
        xAxis: {
            categories: <?=json_encode($list_name)?>,
            title: {
                text: "Provinsi"
            }
        },
        yAxis: {
            
            title: {
                text: 'Total <?=$sum_instructor?> Pengajar <br> keterangan : klik bar dalam tabel untuk detail narasumber'
            }            
        },
        tooltip: {
            valueSuffix: ''
        },
        credits: {
            enabled: false
        },
        plotOptions: {
            series: {
                cursor: 'pointer',
                point: {
                    events: {
                        click: function() {
                            window.open(this.options.url);
                        }
                    }
                }
            },
            bar: {
                dataLabels: {
                    enabled: true
                }
            }
        },
        series: [{
            name: 'Jumlah Pengajar',
            data: <?=json_encode($list_value)?>
        }]
    });
});

</script>
<?php endif;?>