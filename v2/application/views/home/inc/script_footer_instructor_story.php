<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="<?=$this->config->item('plugin')?>/select2/select2.min.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="<?=$this->config->item('plugin')?>/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript" src="<?=$this->config->item('plugin')?>/fancybox/source/jquery.fancybox.pack.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
<script type="text/javascript" src="<?=$this->config->item('plugin')?>/momentjs-business.js"></script>
<!-- END PAGE LEVEL PLUGINS -->

<script>
var URL_CONNECT_API = '<?=$this->config->item('connect_api_url')?>';
var URL_SIMPEL_API = '<?=site_url('api')?>';

var URL = null;
function getFormatDate(d){
    return d.getFullYear() + '-' + (d.getMonth() + 1) + '-' + d.getDate();
}

$(document).ready(function(){
    /*
    popup submission
    */
    function load_ajax_submission(tab_id, training_id){

	var load_page = $(tab_id).find('#training_submission_tab li.active a').data('load_page');
	$(load_page).html('');
	console.log(load_page);
	$.get( "<?=site_url('lpp/management_training/training_ajax')?>/"+load_page+"/"+training_id, function( data ) {		      
	    $('#'+load_page).html( data );
	});
			
    }
    <?php
	/*
	//tabnya
    $('#training_submission_tab a').click(function(e){
		//ambil tab_id sm training_id
		tab_id = $('#training_submission').data('tab_id');
		training_id = $('#training_submission').data('training_id');
	
		$(this).tab('show');
		load_ajax_submission(tab_id, training_id);
    });
	
    //button kliknya
    $(".training_submission").click(function(){
		tab_id = $(this).data('target');
		training_id = $(this).data('training_id');
	
		//simpen training_id sm tab_id
		$(tab_id).attr('data-tab_id', tab_id);
		$(tab_id).attr('data-training_id', training_id);
		
		$('#training_submission form').attr('action',$(this).attr('data-url-submit')) ;
		//load ajax
		load_ajax_submission(tab_id, training_id);			
    });
	*/
	?>
	
	//tabnya
    $(document).on('click','#training_submission_tab a',function(e){
		//ambil tab_id sm training_id
		tab_id = $('#training_submission').data('tab_id');
		training_id = $('#training_submission').data('training_id');
	
		$(this).tab('show');
		load_ajax_submission(tab_id, training_id);
    });
	
    //button kliknya
    $(document).on('click',".training_submission",function(){
		tab_id = $(this).data('target');
		training_id = $(this).data('training_id');
	
		//simpen training_id sm tab_id
		$(tab_id).attr('data-tab_id', tab_id);
		$(tab_id).attr('data-training_id', training_id);
		
		$('#training_submission form').attr('action',$(this).attr('data-url-submit')) ;
		//load ajax
		load_ajax_submission(tab_id, training_id);			
    });
	
    /*
    ./ popup submission
    */
    
    mdTemp = new Date(),
    
    maxDate = getFormatDate(new Date(mdTemp.setMonth(mdTemp.getMonth() + 3)));
    $('.rangedate').daterangepicker({
	locale: {
            format: 'YYYY-MM-DD',
	    "applyLabel": "Pilih",
	    "cancelLabel": "Batal",
	    "fromLabel": "Dari",
	    "toLabel": "Sampai",
	    "daysOfWeek": [
		"Min",
		"Sen",
		"Sel",
		"Rab",
		"Kam",
		"Jum",
		"Sab"
	    ],
	    "monthNames": [
		"Januari",
		"Februari",
		"Maret",
		"April",
		"Mei",
		"Juni",
		"Juli",
		"Agustus",
		"September",
		"Oktober",
		"November",
		"Desember"
	    ],
        },
        minDate: moment().businessAdd(14),
	maxDate: moment().add(4, 'months')
    },

    function(start, end, label) {
	$('#start_date').val(start.format('YYYY-MM-DD'));
	$('#end_date').val(end.format('YYYY-MM-DD'));
	
    }
    );
    
    $('.timepicker').datetimepicker({
	format: 'HH:mm',
	
    });
    $('.select2me').select2({
	placeholder: "-Pilih-",
	allowClear: true
    });
    
    $("#province").change(function() {
	
	$("#district").empty();
	$("#district").select2('data', {id: null, text:""});
	$("#district").append('<option value="">-pilih-</option>');
	$('#district').data("placeholder", "-Pilih-");
	$('#district').data("select2").setPlaceholder();
	
	$.get(URL_CONNECT_API+'/location/district/'+$(this).val(), function(data) {
		$.each(data, function(i, item){
		    $("#district").append(
			'<option value="' + item.id + '">'+item.name + '</option>'
		    );
		})
	    },
	    'json'
	);
    });
    
    $("#type_training").change(function() {
	$("#training_name").empty();
	$("#training_name").select2('data', {id: null, text:""});
	$("#training_name").append('<option value="">-pilih-</option>');
	$('#training_name').data("placeholder", "-Pilih-");
	$('#training_name').data("select2").setPlaceholder();
	
	
	$.get(URL_SIMPEL_API+'/training/name/'+$(this).val(), function(data) {
		$.each(data, function(i, item){
		    $("#training_name").append(
			'<option value="' + item.id + '">'+item.name + '</option>'
		    );
		})
	    },
	    'json'
	);
    });
    
	<?php
	/*
    $('.browse_files').click(function(){
		openKCFinderFiles(this);
	
    });
    */
	?>
	
	$(document).on('click','.browse_files',function(){
		openKCFinderFiles(this);
	
    });
	
    function openKCFinderFiles(div) {
// 	console.log(div);
	window.KCFinder = {
	    callBack: function(url) {
		window.KCFinder = null;
		$(div).siblings().val(url);
	    }
	};
	window.open('<?=$this->config->item("plugin")?>/kcfinder/browse.php?type=files',
	    'kcfinder_file', 'status=0, toolbar=0, location=0, menubar=0, ' +
	    'directories=0, resizable=1, scrollbars=0, width=800, height=600'
	);
    }
});
</script> 