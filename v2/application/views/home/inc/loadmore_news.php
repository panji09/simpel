
		    <div class="list-event-item" data-created="<?=$row['created']?>">
                        <div class="box-content-inner clearfix">
                            <?php if($row['cover']!=''):?>
                            <div class="list-event-thumb">
                                <a href="<?=site_url('home/pages/news/'.$row['_id']->{'$id'})?>">
                                    <img src="<?=$row['cover']?>" alt="">
                                </a>
                            </div>
                            <?php endif;?>
                            <div class="list-event-header">
                                <span class="event-date small-text"><i class="fa fa-calendar-o"></i><?=date("F j, Y, g:i a",$row['created']) ?></span>
                                <div class="view-details"><a href="<?=site_url('home/pages/news/'.$row['_id']->{'$id'})?>" class="lightBtn">Lihat Lengkap</a></div>
                            </div>
                            <h5 class="event-title"><a href="<?=site_url('home/pages/news/'.$row['_id']->{'$id'})?>"><?=$row['title']?></a></h5>
                            <?=$row['content_short']?>
                        </div> <!-- /.box-content-inner -->
                    </div> <!-- /.list-event-item -->