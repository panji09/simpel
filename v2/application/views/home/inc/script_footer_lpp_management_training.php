<?php $training_id = (isset($training_id) ? $training_id : '')?>
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="<?=$this->config->item('plugin')?>/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript" src="<?=$this->config->item('plugin')?>/select2/select2.min.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="<?=$this->config->item('plugin')?>/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript" src="<?=$this->config->item('plugin')?>/fancybox/source/jquery.fancybox.pack.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
<script type="text/javascript" src="<?=$this->config->item('plugin')?>/momentjs-business.js"></script>

<script src="<?=$this->config->item('admin_js')?>/metronic.js" type="text/javascript"></script>
<script src="<?=$this->config->item('admin_js')?>/table-managed.js"></script>
<!-- END PAGE LEVEL PLUGINS -->

<!-- Tinymce WYSIWYG Editors -->
<script type="text/javascript" src="<?=$this->config->item('plugin')?>/tinymce/tinymce.min.js"></script>
<script>

    function load_tinymce(){
	tinyMCE.editors = [];
	tinymce.init({
	    selector: "#wysiwyg-content-full",
		    theme: "modern",
		    skin: "light",
		    plugins:[
			    "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
			    "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
			    "save table contextmenu directionality emoticons template paste textcolor importcss"
		    ],
		    toolbar:[
			    "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media fullpage | forecolor backcolor emoticons table"
			    ],
		    file_browser_callback: function(field, url, type, win) {
		    tinyMCE.activeEditor.windowManager.open({
			file: '<?=$this->config->item("plugin")?>/kcfinder/browse.php?opener=tinymce4&field=' + field + '&type=' + type+'s',					
			title: 'KCFinder',
			width: 700,
			height: 500,
			inline: true,
			close_previous: false
		    }, {
			window: win,
			input: field
		    });
		    return false;
		    }
	});
	
	tinymce.init({
	    selector: "#wysiwyg-content-full_1",
		    theme: "modern",
		    skin: "light",
		    plugins:[
			    "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
			    "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
			    "save table contextmenu directionality emoticons template paste textcolor importcss"
		    ],
		    toolbar:[
			    "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media fullpage | forecolor backcolor emoticons table"
			    ],
		    file_browser_callback: function(field, url, type, win) {
		    tinyMCE.activeEditor.windowManager.open({
			file: '<?=$this->config->item("plugin")?>/kcfinder/browse.php?opener=tinymce4&field=' + field + '&type=' + type+'s',					
			title: 'KCFinder',
			width: 700,
			height: 500,
			inline: true,
			close_previous: false
		    }, {
			window: win,
			input: field
		    });
		    return false;
		    }
	});
	
	tinymce.init({
	    selector: "#wysiwyg-content-full_2",
		    theme: "modern",
		    skin: "light",
		    plugins:[
			    "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
			    "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
			    "save table contextmenu directionality emoticons template paste textcolor importcss"
		    ],
		    toolbar:[
			    "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media fullpage | forecolor backcolor emoticons table"
			    ],
		    file_browser_callback: function(field, url, type, win) {
		    tinyMCE.activeEditor.windowManager.open({
			file: '<?=$this->config->item("plugin")?>/kcfinder/browse.php?opener=tinymce4&field=' + field + '&type=' + type+'s',					
			title: 'KCFinder',
			width: 700,
			height: 500,
			inline: true,
			close_previous: false
		    }, {
			window: win,
			input: field
		    });
		    return false;
		    }
	});
	
	tinymce.init({
	selector: ".wysiwyg-content-full",
		    theme: "modern",
		    skin: "light",
		    plugins:[
			    "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
			    "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
			    "save table contextmenu directionality emoticons template paste textcolor importcss"
		    ],
		    toolbar:[
			    "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media fullpage | forecolor backcolor emoticons table"
			    ],
		    file_browser_callback: function(field, url, type, win) {
		    tinyMCE.activeEditor.windowManager.open({
			file: '<?=$this->config->item("plugin")?>/kcfinder/browse.php?opener=tinymce4&field=' + field + '&type=' + type+'s',					
			title: 'KCFinder',
			width: 700,
			height: 500,
			inline: true,
			close_previous: false
		    }, {
			window: win,
			input: field
		    });
		    return false;
		    }
	});
	
	tinymce.init({
	    selector: "#wysiwyg-short",
		    theme: "modern",
		    skin: "light",
		    menubar:false,
		    statusbar: false,
		    toolbar:["bold italic underline"],
		    paste_data_images: true
	});
	
	tinymce.init({
	    selector: ".wysiwyg-short1",
		    theme: "modern",
		    skin: "light",
		    menubar:false,
		    statusbar: false,
		    toolbar:["bold italic underline | bullist numlist | link image"],
		    paste_data_images: true
	});
    }
    

</script>
<!-- END Tinymce editor -->
<script>
var URL_CONNECT_API = '<?=$this->config->item('connect_api_url')?>';
var URL_SIMPEL_API = '<?=site_url('api')?>';
var URL_MEDIA = '<?=base_url('media')?>';


var instructor_id = null;
var instructor_name = null;
var set_instructor_time_start = null;
var set_instructor_time_end = null;
var set_instructor_date = null;
var THIS_SELECT_INSTRUCTOR = null;
var THIS_SELECT_DATE = null;
var training_id = null;

var URL = null;
function getFormatDate(d){
    return d.getFullYear() + '-' + (d.getMonth() + 1) + '-' + d.getDate();
}

$(document).ready(function(){
    
    load_tinymce();
    Metronic.init(); // init metronic core components
    TableManaged.init();
    var modal_id = null;
    var load_page = null;
    var tab_id = null;
    var join_training_id = null;
    
	
	$(document).on('click','.delete-user',function(){
		var url_delete = $(this).data('url-delete');
		$("#modal-delete-ya").attr('onclick', 'location.href="'+url_delete+'"' );
    });

    /*
	load tab
    */
    <?php if($this->uri->segment(4) == 'detail'):?>
    
    function load_ajax_tab(tab_id, training_id){
	var load_page = $(tab_id).find('#overview_detail_training_tab li.active a').data('load_page');
	var load_tab = $(tab_id).find('#overview_detail_training_tab li.active a').attr('href');
	
	
	$('#overview_detail_training_tab li a').each(function(index, value){
	    $($(value).attr('href')).html('');
	});
	
	var tables = $.fn.dataTable.fnTables(true);
	$(tables).each(function () {
	    $(this).dataTable().fnDestroy();
	});
	
	$(load_tab).html('<div class="circles-loader" style="float: none;margin-left: auto; margin-right: auto;">Loading..!</div>');
	
	$.get( "<?=site_url('lpp/management_training/tab_ajax')?>/"+load_page+"/"+training_id,
	function(data) {		      
		$(load_tab).html( data );
		TableManaged.init();
		$("time.timeago").timeago();
		load_tinymce();
	});

    }
    training_id = '<?=$training_id?>';
    load_ajax_tab('#overview_detail_training',training_id);
    
    $(document).on('click','#overview_detail_training_tab',function(){
	training_id = '<?=$training_id?>';
	load_ajax_tab('#overview_detail_training',training_id);
    
    });
    <?php endif;?>
    /*
	./load tab
    */
    $(document).on('click','.overview_join_participant',function(){
	modal_id = $(this).attr('data-target');
// 	console.log(modal_id);
	load_page = $(modal_id).find('#participant_info_tab li.active a').data('load_page');
	tab_id = $(modal_id).find('#participant_info_tab li.active a').attr('href');

	join_training_id = $(this).attr('data-join_training_id');
	
	$(modal_id+' form').attr('action', $(this).data('url_submit') );
	
	if(load_page != ''){
	    $.get("<?=site_url('lpp/management_training/overview_participant_ajax')?>/"+load_page+"/"+join_training_id, function( data ) {		      
			$(modal_id+' '+tab_id).html( data );
	    });
	}
	
    });
    
    $(document).on('click', '#participant_info_tab', function(){
	load_page = $(modal_id).find('#participant_info_tab li.active a').data('load_page');
	tab_id = $(modal_id).find('#participant_info_tab li.active a').attr('href');
	
	if(load_page != ''){
	    $.get("<?=site_url('lpp/management_training/overview_participant_ajax')?>/"+load_page+"/"+join_training_id, function( data ) {		      
			$(modal_id+' '+tab_id).html( data );
	    });
	}
	
    });
    
    $(document).on('click', '#tab_action_participant_info', function(){
	load_page = $(this).find('li.active a').data('load_page');
	tab_id = $(this).find('li.active a').attr('href');
	
	if(load_page != ''){
	    $.get("<?=site_url('lpp/management_training/overview_participant_ajax')?>/"+load_page+"/"+join_training_id, function( data ) {		      
			$(modal_id+' '+tab_id).html( data );
			load_tinymce();
	    });
	}
	
    });
    
    
    		/*------------------------------------------------------------------------*/
		/*	popup submission
		/*------------------------------------------------------------------------*/		
		var modal_id = '#review_training_submission';
		var modal_tab_id = '#review_training_submission_tab';
		var btn_class = '.review_training_submission';

		function load_ajax_submission(tab_id, training_id){
			var load_page = $(tab_id).find(modal_tab_id+' li.active a').attr('data-load_page');
			$(load_page).html('');
			console.log(tab_id+' '+modal_tab_id);
			$.get( "<?=site_url('lpp/management_training/training_ajax')?>/"+load_page+"/"+training_id,
			function(data) {		      
				$('#'+load_page).html( data );
			});

		}
		
		//tabnya
		$(document).on('click',modal_tab_id+' a',function(){
		    //ambil tab_id sm training_id
		    tab_id = $(modal_id).attr('data-tab_id');
		    training_id = $(modal_id).attr('data-training_id');


		    $(this).tab('show');
		    load_ajax_submission(tab_id, training_id);
		});
		
		<?php 
		/*
		$(modal_tab_id+' a').click(function(e){
			//ambil tab_id sm training_id
			tab_id = $(modal_id).data('tab_id');
			training_id = $(modal_id).data('training_id');


			$(this).tab('show');
			load_ajax_submission(tab_id, training_id);
		});
		*/
		?>
		//button kliknya
		$(document).on('click',btn_class,function(){
		    //setting init
		    //alert('uy');
		    $(modal_id+" form").attr('action',$(this).data('url-submit'));

		    tab_id = $(this).attr('data-target');
		    training_id = $(this).attr('data-training_id');

		    //simpen training_id sm tab_id
		    $(tab_id).attr('data-tab_id', tab_id);
		    $(tab_id).attr('data-training_id', training_id);

		    //load ajax
		    load_ajax_submission(tab_id, training_id);
		});
		
		<?php /*
		$(btn_class).click(function(){
			//setting init
			$(modal_id+" form").attr('action',$(this).data('url-submit'));

			tab_id = $(this).data('target');
			training_id = $(this).data('training_id');

			//simpen training_id sm tab_id
			$(tab_id).attr('data-tab_id', tab_id);
			$(tab_id).attr('data-training_id', training_id);

			//load ajax
			load_ajax_submission(tab_id, training_id);

		});
		*/
		?>
		//check sbmit
		$(modal_id+" form").submit(function (e) {

			// 		  e.preventDefault();
			var btn = $(this).find("button[type=submit]:focus" );
			var tab_id = $(modal_id).data('tab_id');
			var training_id = $(modal_id).data('training_id');
			var load_page = $(tab_id).find(modal_tab_id+' li.active a').data('load_page');


			if(btn.attr('id') == 'approved_training_submission_submit'){

				if(load_page != 'training_submission_approved'){
					$('#training_submission_review_note').html('');
					$('#training_submission_approved').html('');
					$(modal_tab_id+' a[href="#training_submission_approved"]').tab('show');
					load_ajax_submission(tab_id, training_id);
				}


				if($('[name=facilitator_user_id]').val() == null || $('[name=facilitator_user_id]').val() == ''){
					alert('harus diisi!');

					// 			  console.log(btn.attr('id'));
					e.preventDefault();
				}else{
					return;
				}
			}else if(btn.attr('id') == 'review_training_submission_submit'){

				if(load_page != 'training_submission_review_note'){
					$('#training_submission_review_note').html('');
					$('#training_submission_approved').html('');
					$(modal_tab_id+' a[href="#training_submission_review_note"]').tab('show');
					load_ajax_submission(tab_id, training_id);
				}


				if($('[name=review_note]').val() == null || $('[name=review_note]').val() == ''){
					alert('harus diisi!');

					// 			  console.log(btn.attr('id'));
					e.preventDefault();
				}else{
					return;
				}
			}

		});
		
		/*------------------------------------------------------------------------*/
		/*	./popup submission
		/*------------------------------------------------------------------------*/
		
		/*------------------------------------------------------------------------*/
		/*	popup review lpp
		/*------------------------------------------------------------------------*/			
		function load_ajax(){
			param = $('#review_institution_tab li.active a').data('param');
			tab_content = $('#review_institution_tab li.active a').attr('href');
			user_id = $('#review_institution').data('user_id');

			// 		  $(tab_content).html('');
			$.get( "<?=site_url('lpp/management_training/institution_ajax')?>/"+user_id+"/"+param, function( data ) {		      
				$(tab_content).html( data );
			});

		}
		
		$(document).on('click','.review-institution',function(){
		    //set user_id
		    $('#review_institution').data('user_id', $(this).data('user_id'));
		    $('#review_institution textarea').val('');
		    //set url-accept
		    $('#review_institution #modal-accept').attr('onclick', 'location.href="'+$(this).data('url-accept')+'"');		  
		    //set url-reject
		    $('#review_institution #modal-reject').attr('onclick', 'location.href="'+$(this).data('url-reject')+'"');		  
		    //data-url-submit
		    $('#review_institution form').attr('action', $(this).data('url-submit'));
		    //data-url-submit		  
		    $('#review_institution_tab a[href="#review_tab_0"]').tab('show');		  
		    //load ajax
		    load_ajax();

		});
		
		<?php
		/*
		$(".review-institution").click(function(){
			//set user_id
			$('#review_institution').data('user_id', $(this).data('user_id'));
			$('#review_institution textarea').val('');
			//set url-accept
			$('#review_institution #modal-accept').attr('onclick', 'location.href="'+$(this).data('url-accept')+'"');		  
			//set url-reject
			$('#review_institution #modal-reject').attr('onclick', 'location.href="'+$(this).data('url-reject')+'"');		  
			//data-url-submit
			$('#review_institution form').attr('action', $(this).data('url-submit'));
			//data-url-submit		  
			$('#review_institution_tab a[href="#review_tab_0"]').tab('show');		  
			//load ajax
			load_ajax();

		});
		*/
		?>
		
		$(document).on('click','#review_institution_submit',function(){

			$('#review_tab_5').html('');
			$('#review_institution_tab a[href="#review_tab_4"]').tab('show');
		});

		$("#review_institution form").submit(function (e) {

			// 		  e.preventDefault();
			var btn = $(this).find("button[type=submit]:focus" );

			if(btn.attr('id') == 'approved_institution_submit'){
				$('#review_tab_4').html('');
				if($('[name=accreditation_level]').val() == null){

					$('#review_institution_tab a[href="#review_tab_5"]').tab('show');
					load_ajax();
					// 			  console.log(btn.attr('id'));
					e.preventDefault();
				}else{
					return;
				}
			}else if(btn.attr('id') == 'review_institution_submit'){
				$('#review_tab_5').html('');
				if($('[name=review_note]').val() == null){

					$('#review_institution_tab a[href="#review_tab_4"]').tab('show');
					load_ajax();
					// 			  console.log(btn.attr('id'));
					e.preventDefault();
				}else{
					return;
				}
			}

		});

		$(document).on('click','#review_institution_tab a',function(){
			$(this).tab('show');
			load_ajax();
		});
		
		/*------------------------------------------------------------------------*/
		/*	./popup review lpp
		/*------------------------------------------------------------------------*/
		
		/*------------------------------------------------------------------------*/
		/*	modal select instructor
		/*------------------------------------------------------------------------*/
		
		function select_instructor(){
		    
		    $(document).on('click','.select_instructor',function(){
				THIS_SELECT_INSTRUCTOR = this;
				training_id = $(this).data('training_id');
				$.get( "<?=site_url('fasilitator/management_training/select_instructor_ajax')?>/"+training_id, 
				function(data) {
					$('#select_instructor_modal .modal-body').html( data );
					TableManaged.init_table_choose_instructor();
					
					$(document).on('click','#submit_select_instructor',function(){
					
						$(THIS_SELECT_INSTRUCTOR).parent().siblings('input').attr('value',instructor_name);
						$(THIS_SELECT_INSTRUCTOR).parent().siblings('.set_instructor').val(instructor_id);
						
					});
					
					//set var global
					$(document).on('click','.instructor', function(){
						//set var global
						instructor_id = $(this).prop("checked", true).val();
						instructor_name = $(this).prop("checked", true).attr('data-instructor_name');
					});
				});
		    });
		    <?php
		    /*
		    $('.select_instructor').click(function(){
			    THIS_SELECT_INSTRUCTOR = this;
			    training_id = $(this).data('training_id');
			    $.get( "<?=site_url('fasilitator/management_training/select_instructor_ajax')?>/"+training_id, 
			    function(data) {      
				    $('#select_instructor_modal .modal-body').html( data );
				    TableManaged.init_table_choose_instructor();
				    
				    $('#submit_select_instructor').click(function(){
				    
					$(THIS_SELECT_INSTRUCTOR).parent().siblings('input').attr('value',instructor_name);
					$(THIS_SELECT_INSTRUCTOR).parent().siblings('.set_instructor').val(instructor_id);
					
				    });
				    
				    //set var global
				    $('.instructor').click(function(){
					//set var global
					instructor_id = $(this).prop("checked", true).val();
					instructor_name = $(this).prop("checked", true).attr('data-instructor_name');
				    });
			    });
		    });
		    
		    */
		    ?>
		    
		}
		
		select_instructor();
		/*------------------------------------------------------------------------*/
		/*	./modal select instructor
		/*------------------------------------------------------------------------*/		
		
		/*------------------------------------------------------------------------*/
		/*	pilih set tanggal jam narsum
		/*------------------------------------------------------------------------*/	
		function choose_instructor(){
		    
		    $(document).on('click','.choose_instructor',function(){
			THIS_SELECT_DATE = this;
			    
			training_id = $(this).data('training_id');
			instructor_id = $(this).parent().siblings().find('.set_instructor').val();
			
			$.get( "<?=site_url('fasilitator/management_training/set_date_instructor_ajax')?>/"+training_id+"/"+instructor_id, 
			function(data) {
				$('#choose_instructor_modal .modal-body').html( data );
				
				$('.datepicker').datepicker({
				    autoclose: true,
				    format: "yyyy-mm-dd",
				    <?php
					if($training_id)
					$query = $this->training_db->get($training_id); 
// 					    print_r($query);
					$training_start = $training_end = '';
					if($training_id && $query){
					    $training = $query[0];
					    
					    $training_data = explode(' - ',$training['training_date']);
					    
					    $training_start = (isset($training_data[0]) ? $training_data[0] : '');
					    $training_end = (isset($training_data[1]) ? $training_data[1] : '');
					    
					}
				    ?>
				    startDate: "<?=$training_start?>",
				    endDate: "<?=$training_end?>"
				});
				
				$('.timepicker').datetimepicker({
				      
				    format: 'HH:mm'
				});
				
				//set var global
				$('.set_instructor_date').change(function(){
				    set_instructor_date = $(this).val();

				});
				
				/*
				$('.set_instructor_time_start').on('click', function(){
				    set_instructor_time_start = $(this).find('input').val();
				});
				
				$('.set_instructor_time_end').on('click', function(){
				    set_instructor_time_end = $(this).find('input').val();
				});
				*/
				
			});
		    });
		    <?php
		    /*
		    $('.choose_instructor').click(function(){
			    
		    });
		    */
		    ?>
		    
		}
		choose_instructor();
		
		function remove_date(){
		    <?php
			/*
			$('.remove_date').click(function(){
				$(this).parent().parent().parent().remove();
		    });
		    */
			?>
			$(document).on('click','.remove_date',function(){
				$(this).parent().parent().parent().remove();
		    });
		}
		
		$(document).on('click','#submit_set_instructor',function(){
		    set_instructor_time_start = $(this).parent().parent().find('.set_instructor_time_start input').val();
		    set_instructor_time_end = $(this).parent().parent().find('.set_instructor_time_end input').val();
		    
		    var list_date = '<ul class="list-unstyled" >'+
					'<li class="margin-bottom-5">'+
					    '<span class="label label-sm label-info">'+set_instructor_date+', '+set_instructor_time_start+'-'+set_instructor_time_end+'</span>'+
						'<a href="javascript:;"><i class="fa fa-times font-red remove_date"></i></a>'+
						'<input type="hidden" name="set_date['+instructor_id+'][]" value="'+set_instructor_date+'">'+
						'<input type="hidden" name="set_time_range['+instructor_id+'][]" value="'+set_instructor_time_start+' - '+set_instructor_time_end+'">'+
					'</li>'+			
				    '</ul>';
		    $(THIS_SELECT_DATE).parent().prepend(list_date);
		    remove_date();
		});
		<?php
		/*
		$('#submit_set_instructor').click(function(){
			
			set_instructor_time_start = $(this).parent().parent().find('.set_instructor_time_start input').val();
			set_instructor_time_end = $(this).parent().parent().find('.set_instructor_time_end input').val();
			
			var list_date = '<ul class="list-unstyled" >'+
					    '<li class="margin-bottom-5">'+
						'<span class="label label-sm label-info">'+set_instructor_date+', '+set_instructor_time_start+'-'+set_instructor_time_end+'</span>'+
						    '<a href="javascript:;"><i class="fa fa-times font-red remove_date"></i></a>'+
						    '<input type="hidden" name="set_date['+instructor_id+'][]" value="'+set_instructor_date+'">'+
						    '<input type="hidden" name="set_time_range['+instructor_id+'][]" value="'+set_instructor_time_start+' - '+set_instructor_time_end+'">'+
					    '</li>'+			
					'</ul>';
			$(THIS_SELECT_DATE).parent().prepend(list_date);
			remove_date();
			
		});
		*/
		?>
		function remove_instructor(){
		    <?php
			/*
			$('.remove_instructor').click(function(){
				$(this).parent().parent().remove();
		    });
		    */
			?>
			
			$(document).on('click','.remove_instructor',function(){
				$(this).parent().parent().remove();
		    });
		}
		remove_instructor();
		
		$(document).on('click','.add_instructor',function(){
		    var content_tr = $('#template_table_set_instructor #table_set_instructor tbody').html();
		    $('#table_content #table_set_instructor tbody').append(content_tr);
		    
		    select_instructor();
		    choose_instructor();
		    remove_date();
		    remove_instructor();
		});
		<?php
		/*
		$('.add_instructor').click(function(){
		    var content_tr = $('#template_table_set_instructor #table_set_instructor tbody').html();
		    $('#table_content #table_set_instructor tbody').append(content_tr);
		    
		    select_instructor();
		    choose_instructor();
		    remove_date();
		    remove_instructor();
		});
		*/
		?>

		/*------------------------------------------------------------------------*/
		/*	end set tanggal jam narsum
		/*------------------------------------------------------------------------*/
		
	      /*
	      popup review instructor
	      */
	      var modal_id_instructor = '#review_instructor';
	      var modal_tab_id_instructor = '#review_instructor_tab';
	      var btn_class_instructor = '.review_instructor';
	      function load_ajax_instructor(modal_id_instructor, training_id, user_id){
		  
		  var load_page = $(modal_id_instructor).find(modal_tab_id_instructor+' li.active a').data('load_page');
		  var tab_id = $(modal_id_instructor).find(modal_tab_id_instructor+' li.active a').attr('href');
		  
		  $.get( "<?=site_url('lpp/management_training/instructor_ajax')?>/"+load_page+"/"+training_id+"/"+user_id, function( data ) {		      
		      $(modal_id_instructor+' '+tab_id).html( data );
		  });
				  
	      }
	      //tabnya
	      $(document).on('click',modal_tab_id_instructor+' a',function(){
		  //ambil tab_id sm training_id
		  training_id = $(modal_id_instructor).attr('data-training_id');
		  user_id = $(modal_id_instructor).attr('data-user_id');
		  
		  $(this).tab('show');
		  load_ajax_instructor(modal_id_instructor, training_id, user_id);
	      });
	      
	      //button kliknya
	      $(document).on('click',btn_class_instructor,function(){
		  user_id = $(this).data('user_id');
		  training_id = $(this).data('training_id');

		  //setting init
		  $(modal_id_instructor+" form").attr('action',$(this).attr('data-url-submit'));
		  $(modal_id_instructor+" input[name=instructor_id]").val(user_id);
		  
		  
		  //simpen training_id sm tab_id
		  $(modal_id_instructor).attr('data-training_id', training_id);
		  $(modal_id_instructor).attr('data-user_id', user_id);
		  
		  //load ajax
		  load_ajax_instructor(modal_id_instructor, training_id, user_id);
				  
	      });
	      
	      <?php /*
	      $(btn_class_instructor).click(function(){
		  user_id = $(this).data('user_id');
		  training_id = $(this).data('training_id');

		  //setting init
		  $(modal_id_instructor+" form").attr('action',$(this).data('url-submit'));
		  $(modal_id_instructor+" input[name=instructor_id]").val(user_id);
		  
		  
		  //simpen training_id sm tab_id
		  $(modal_id_instructor).attr('data-training_id', training_id);
		  $(modal_id_instructor).attr('data-user_id', user_id);
		  
		  //load ajax
		  load_ajax_instructor(modal_id_instructor, training_id, user_id);
				  
	      });
	      */
	      ?>
	      
	      //check sbmit
	      $(modal_id_instructor+" form").submit(function (e) {
		  
// 		  e.preventDefault();
		  var btn = $(this).find("button[type=submit]:focus" );
		  var tab_id = $(modal_id_instructor).data('tab_id');
		  var training_id = $(modal_id_instructor).data('training_id');
		  var load_page = $(tab_id).find(modal_tab_id_instructor+' li.active a').data('load_page');
		  
		  
		  if(btn.attr('id') == 'approved_training_submission_submit'){
		      
		      if(load_page != 'training_submission_approved'){
			  $('#training_submission_review_note').html('');
			  $('#training_submission_approved').html('');
			  $(modal_tab_id_instructor+' a[href="#training_submission_approved"]').tab('show');
			  load_ajax_instructor(tab_id, training_id);
		      }
		      
		      
		      if($('[name=facilitator_user_id]').val() == null || $('[name=facilitator_user_id]').val() == ''){
			  alert('harus diisi!');
			  
// 			  console.log(btn.attr('id'));
			  e.preventDefault();
		      }else{
			  return;
		      }
		  }else if(btn.attr('id') == 'review_training_submission_submit'){
		      
		      if(load_page != 'training_submission_review_note'){
			  $('#training_submission_review_note').html('');
			  $('#training_submission_approved').html('');
			  $(modal_tab_id_instructor+' a[href="#training_submission_review_note"]').tab('show');
			  load_ajax_instructor(tab_id, training_id);
		      }
		      
		      
		      if($('[name=review_note]').val() == null || $('[name=review_note]').val() == ''){
			  alert('harus diisi!');
			  
// 			  console.log(btn.attr('id'));
			  e.preventDefault();
		      }else{
			  return;
		      }
		  }
		  
	      });
	      
	      /*
	      ./ popup review instructor
	      */
	      
	      /*
		  set participant
	      */
		  $(document).on('click','.add_participant',function(){
		      var content_tr = $('#template_table_set_participant #table_set_participant tbody').html();
		      $('#table_content_set_participant #table_set_participant tbody').append(content_tr);
		      
		  });
		  
		  $(document).on('click','.remove_participant',function(){
		      $(this).parent().parent().remove();
		  });
	      /*
		  ./set participant
	      */
    mdTemp = new Date(),
//     minDate = getFormatDate(new Date(mdTemp.setDate(mdTemp.getDate() + 14))),
    
    maxDate = getFormatDate(new Date(mdTemp.setMonth(mdTemp.getMonth() + 3)));
//     console.log(minDate+' - '+maxDate);
    $('.rangedate').daterangepicker({
	locale: {
            format: 'YYYY-MM-DD',
	    "applyLabel": "Pilih",
	    "cancelLabel": "Batal",
	    "fromLabel": "Dari",
	    "toLabel": "Sampai",
	    "daysOfWeek": [
		"Min",
		"Sen",
		"Sel",
		"Rab",
		"Kam",
		"Jum",
		"Sab"
	    ],
	    "monthNames": [
		"Januari",
		"Februari",
		"Maret",
		"April",
		"Mei",
		"Juni",
		"Juli",
		"Agustus",
		"September",
		"Oktober",
		"November",
		"Desember"
	    ],
        },
        <?php $max_day = (isset($this->setting_db->get('max_bussinesday_submission_training')[0]['day']) ? $this->setting_db->get('max_bussinesday_submission_training')[0]['day'] : 0);?>
        minDate: moment().businessAdd(<?=$max_day?>),
	maxDate: moment().add(4, 'months')
    },
    function(start, end, label) {
	$('#start_date').val(start.format('YYYY-MM-DD'));
	$('#end_date').val(end.format('YYYY-MM-DD'));
	
    }
    );
    
    $('.timepicker').datetimepicker({
	format: 'HH:mm',
	
    });
    $('select.select2me').select2({
	placeholder: "-Pilih-",
	allowClear: true
    });
    
    $("#province").change(function() {
	
	$("#district").empty();
	$("#district").select2('data', {id: null, text:""});
	$("#district").append('<option value="">-pilih-</option>');
	$('#district').data("placeholder", "-Pilih-");
	$('#district').data("select2").setPlaceholder();
	
	$.get(URL_CONNECT_API+'/location/district/'+$(this).val(), function(data) {
		$.each(data, function(i, item){
		    $("#district").append(
			'<option value="' + item.id + '">'+item.name + '</option>'
		    );
		})
	    },
	    'json'
	);
    });
    
    $("#type_training").change(function() {
	$("#training_name").empty();
	$("#training_name").select2('data', {id: null, text:""});
	$("#training_name").append('<option value="">-pilih-</option>');
	$('#training_name').data("placeholder", "-Pilih-");
	$('#training_name').data("select2").setPlaceholder();
	
	
	$.get(URL_SIMPEL_API+'/training/name_simple/'+$(this).val(), function(data) {
		$.each(data, function(i, item){
		    $("#training_name").append(
			'<option value="' + item.id + '">'+item.name + '</option>'
		    );
		})
	    },
	    'json'
	);
    });
    
	<?php
	/*
    $('.browse_files').click(function(){
		openKCFinderFiles(this);	
    });
    */
	?>
	
	$(document).on('click','.browse_files',function(){
		openKCFinderFiles(this);	
    });
	
    function openKCFinderFiles(div) {
// 	console.log(div);
	window.KCFinder = {
	    callBack: function(url) {
		window.KCFinder = null;
		$(div).siblings().val(url);
	    }
	};
	window.open('<?=$this->config->item("plugin")?>/kcfinder/browse.php?type=files',
	    'kcfinder_file', 'status=0, toolbar=0, location=0, menubar=0, ' +
	    'directories=0, resizable=1, scrollbars=0, width=800, height=600'
	);
    }
	
	
});
</script> 