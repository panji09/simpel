<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="<?=$this->config->item('plugin')?>/select2/select2.min.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="<?=$this->config->item('plugin')?>/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript" src="<?=$this->config->item('plugin')?>/fancybox/source/jquery.fancybox.pack.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
<script type="text/javascript" src="<?=$this->config->item('plugin')?>/momentjs-business.js"></script>
<!-- END PAGE LEVEL PLUGINS -->

<!-- Tinymce WYSIWYG Editors -->
<script type="text/javascript" src="<?=$this->config->item('plugin')?>/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
    function load_tinymce(){

	tinymce.init({
	    selector: "#wysiwyg-content-full",
		    theme: "modern",
		    skin: "light",
		    plugins:[
			    "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
			    "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
			    "save table contextmenu directionality emoticons template paste textcolor importcss"
		    ],
		    toolbar:[
			    "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media fullpage | forecolor backcolor emoticons table"
			    ],
		    file_browser_callback: function(field, url, type, win) {
		    tinyMCE.activeEditor.windowManager.open({
			file: '<?=$this->config->item("plugin")?>/kcfinder/browse.php?opener=tinymce4&field=' + field + '&type=' + type+'s',					
			title: 'KCFinder',
			width: 700,
			height: 500,
			inline: true,
			close_previous: false
		    }, {
			window: win,
			input: field
		    });
		    return false;
		    }
	});
	
	tinymce.init({
	    selector: "#wysiwyg-short",
		    theme: "modern",
		    skin: "light",
		    menubar:false,
		    statusbar: false,
		    toolbar:["bold italic underline"],
		    paste_data_images: true
	});
	
	tinymce.init({
	    selector: ".wysiwyg-short1",
		    theme: "modern",
		    skin: "light",
		    menubar:false,
		    statusbar: false,
		    toolbar:["bold italic underline | bullist numlist | link image"],
		    paste_data_images: true
	});
    }
    
</script>
<!-- END Tinymce editor -->
<script>
var URL_CONNECT_API = '<?=$this->config->item('connect_api_url')?>';
var URL_SIMPEL_API = '<?=site_url('api')?>';

// var URL = null;
function getFormatDate(d){
    return d.getFullYear() + '-' + (d.getMonth() + 1) + '-' + d.getDate();
}

$(document).ready(function(){
    load_tinymce();
    
    /*
      check syarat
    */
    var load_page = null;
    var modal_id = null;
	<?php
	/*
    $('#register_training').click(function(){
		load_page = $(this).data('load_page');
		modal_id = $(this).attr('data-target');
		$(modal_id+" form").attr('action',$(this).data('url_submit'));
		$(modal_id+' .modal-body').html('');
		$.get(load_page, function( data ) {
			$(modal_id+' .modal-body').html( data );
			
			
			
			$(document).on('click','.browse_files',function(){
			openKCFinderFiles(this);
			
			//for container 
			
			
			});
			
			function openKCFinderFiles(div) {
		// 	console.log(div);
			window.KCFinder = {
				callBack: function(url) {
				window.KCFinder = null;
				$(div).siblings().val(url);
				}
			};
			window.open('<?=$this->config->item("plugin")?>/kcfinder/browse.php?type=files',
				'kcfinder_file', 'status=0, toolbar=0, location=0, menubar=0, ' +
				'directories=0, resizable=1, scrollbars=0, width=800, height=600'
			);
			}
		});
	
	
    });
    */
	?>
    
	$(document).on('click','#register_training',function(){
		load_page = $(this).data('load_page');
		modal_id = $(this).attr('data-target');
		$(modal_id+" form").attr('action',$(this).data('url_submit'));
		$(modal_id+' .modal-body').html('');
		$.get(load_page, function( data ) {
			$(modal_id+' .modal-body').html( data );
			
			
			
			$(document).on('click','.browse_files',function(){
			openKCFinderFiles(this);
			
			//for container 
			
			
			});
			
			function openKCFinderFiles(div) {
		// 	console.log(div);
			window.KCFinder = {
				callBack: function(url) {
				window.KCFinder = null;
				$(div).siblings().val(url);
				}
			};
			window.open('<?=$this->config->item("plugin")?>/kcfinder/browse.php?type=files',
				'kcfinder_file', 'status=0, toolbar=0, location=0, menubar=0, ' +
				'directories=0, resizable=1, scrollbars=0, width=800, height=600'
			);
			}
		});
	
	
    });
	
	$(document).on('click', 'button.add_new_document',function(){

	requirement = $(this).parent().parent().parent().clone().find("input:text").val("").end();
	$(this).parent().parent().parent().parent().append(requirement);
	
	
    });
    
    $(document).on('click','button.remove_new_document',function(){
	
	$(this).parent().parent().parent().remove();
	
    });
    /*
      ./check syarat
    */
    
    /*
    popup submission
    */
    function load_ajax_submission(tab_id, training_id){
		var load_page = $(tab_id).find('#training_submission_tab li.active a').data('load_page');
			$(load_page).html('');
			console.log(load_page);
			$.get("<?=site_url('lpp/management_training/training_ajax')?>/"+load_page+"/"+training_id, 
			function( data ) {		      
			    $('#'+load_page).html( data );
			});
	    }
		
		<?php
		/*
	    //tabnya
	    $('#training_submission_tab a').click(function(e){
			//ambil tab_id sm training_id
			tab_id = $('#training_submission').data('tab_id');
			training_id = $('#training_submission').data('training_id');


			$(this).tab('show');
			load_ajax_submission(tab_id, training_id);
		});
		
		//button kliknya
		$(".review_training_submission").click(function(){
			tab_id = $(this).data('target');
			training_id = $(this).data('training_id');

			//simpen training_id sm tab_id
			$(tab_id).attr('data-tab_id', tab_id);
			$(tab_id).attr('data-training_id', training_id);

			//load ajax
			load_ajax_submission(tab_id, training_id);
	    });
	    */
		?>
		
		//tabnya
	    $(document).on('click','#training_submission_tab a',function(e){
			//ambil tab_id sm training_id
			tab_id = $('#training_submission').data('tab_id');
			training_id = $('#training_submission').data('training_id');


			$(this).tab('show');
			load_ajax_submission(tab_id, training_id);
		});
		
		//button kliknya
		$(document).on('click',".review_training_submission",function(){
			tab_id = $(this).data('target');
			training_id = $(this).data('training_id');

			//simpen training_id sm tab_id
			$(tab_id).attr('data-tab_id', tab_id);
			$(tab_id).attr('data-training_id', training_id);

			//load ajax
			load_ajax_submission(tab_id, training_id);
	    });
		
    /*
    ./ popup submission
    */
    mdTemp = new Date(),
//     minDate = getFormatDate(new Date(mdTemp.setDate(mdTemp.getDate() + 14))),
    
    maxDate = getFormatDate(new Date(mdTemp.setMonth(mdTemp.getMonth() + 3)));
//     console.log(minDate+' - '+maxDate);
    $('.rangedate').daterangepicker({
	locale: {
            format: 'YYYY-MM-DD',
	    "applyLabel": "Pilih",
	    "cancelLabel": "Batal",
	    "fromLabel": "Dari",
	    "toLabel": "Sampai",
	    "daysOfWeek": [
		"Min",
		"Sen",
		"Sel",
		"Rab",
		"Kam",
		"Jum",
		"Sab"
	    ],
	    "monthNames": [
		"Januari",
		"Februari",
		"Maret",
		"April",
		"Mei",
		"Juni",
		"Juli",
		"Agustus",
		"September",
		"Oktober",
		"November",
		"Desember"
	    ],
        },
        minDate: moment().businessAdd(14),
	maxDate: moment().add(4, 'months')
    },
    function(start, end, label) {
	$('#start_date').val(start.format('YYYY-MM-DD'));
	$('#end_date').val(end.format('YYYY-MM-DD'));
	
    }
    );
    
    $('.timepicker').datetimepicker({
	format: 'HH:mm',
	
    });
    $('.select2me').select2({
	placeholder: "-Pilih-",
	allowClear: true
    });
    
    $("#province").change(function() {
	
	$("#district").empty();
	$("#district").select2('data', {id: null, text:""});
	$("#district").append('<option value="">-pilih-</option>');
	$('#district').data("placeholder", "-Pilih-");
	$('#district').data("select2").setPlaceholder();
	
	$.get(URL_CONNECT_API+'/location/district/'+$(this).val(), function(data) {
		$.each(data, function(i, item){
		    $("#district").append(
			'<option value="' + item.id + '">'+item.name + '</option>'
		    );
		})
	    },
	    'json'
	);
    });
    
    $("#type_training").change(function() {
	$("#training_name").empty();
	$("#training_name").select2('data', {id: null, text:""});
	$("#training_name").append('<option value="">-pilih-</option>');
	$('#training_name').data("placeholder", "-Pilih-");
	$('#training_name').data("select2").setPlaceholder();
	
	
	$.get(URL_SIMPEL_API+'/training/name/'+$(this).val(), function(data) {
		$.each(data, function(i, item){
		    $("#training_name").append(
			'<option value="' + item.id + '">'+item.name + '</option>'
		    );
		})
	    },
	    'json'
	);
    });
    
	<?php
	/*
    $('.browse_files').click(function(){
		openKCFinderFiles(this);
    });
    */
	?>
	
	$(document).on('click','.browse_files',function(){
		openKCFinderFiles(this);
    });
	
    function openKCFinderFiles(div) {
// 	console.log(div);
	window.KCFinder = {
	    callBack: function(url) {
		window.KCFinder = null;
		$(div).siblings().val(url);
	    }
	};
	window.open('<?=$this->config->item("plugin")?>/kcfinder/browse.php?type=files',
	    'kcfinder_file', 'status=0, toolbar=0, location=0, menubar=0, ' +
	    'directories=0, resizable=1, scrollbars=0, width=800, height=600'
	);
    }
	
	
	<?php
	/*
	$('#btn_save').click(function(){
	  event.preventDefault();
	  //first get data
	  var evaluation = new Object();
	  
	  evaluation.training_institute = $( 'input[name="training_institute"]' ).val();
	  evaluation.training_location_address = $( 'input[name="training_location_address"]' ).val();
	  evaluation.training_date = $( 'input[name="training_date"]' ).val();
	  evaluation.participant_amount = $( 'input[name="participant_amount"]' ).val();
	  
	  var sections_data = [];
	  var sections = $('[data-section="section_master"]');
	  var sectionCount = sections.length;
	  for(var i=0;i<sectionCount;i++){
		  var currentSection = sections[i];
		  var section = new Object();
		  section.name = $(currentSection).attr("data-section-title");		  
		  var currentQuestions = $('[data-item="item_'+i+'"]');
		  var currentQuestions_data = [];
		  jQuery.each(currentQuestions, function(j, question){
			  var iQuestion = new Object();
			  iQuestion.no = $(question).attr("data-item-id");		
			  iQuestion.question = $(question).attr("data-item-question");		
			  var input_data = $(question).find('input[type="text"]');
			  var data = []; 
			  jQuery.each(input_data, function(j, it){
				  var value = $(it).val();
				  data.push(parseInt(value));
			  });
			  iQuestion.data = data;
			  currentQuestions_data.push(iQuestion);
		  });
		  section.questions = currentQuestions_data;
		  sections_data.push(section);
	  }
	  
	  evaluation.sections = sections_data;

		//procudure for save data
		var URL_API = '<?=$this->config->item('base_url')?>';
		//get data
		var formUrl = URL_API+'/index.php/lpp/management_training/evaluation_post/<?=(isset($training_id) ? $training_id : '' )?>';

		var ukeh = jQuery.parseJSON(JSON.stringify(evaluation));
		var sendInfo = JSON.stringify(ukeh);
			$.ajax({
			           type: "POST",
			           url: formUrl,
			           contentType: "application/json",
			           success: function (msg) {
						   window.location.assign(URL_API+'/index.php/lpp/management_training/training');
			           },
					   error: function(xhr, status, error) {
						   console.log(xhr);
					    },

			           data: sendInfo
			       });	
	});
	*/
	?>
	
	$(document).on('click','#btn_save',function(){
	  event.preventDefault();
	  //first get data
	  var evaluation = new Object();
	  
	  evaluation.training_institute = $( 'input[name="training_institute"]' ).val();
	  evaluation.training_location_address = $( 'input[name="training_location_address"]' ).val();
	  evaluation.training_date = $( 'input[name="training_date"]' ).val();
	  evaluation.participant_amount = $( 'input[name="participant_amount"]' ).val();
	  
	  var sections_data = [];
	  var sections = $('[data-section="section_master"]');
	  var sectionCount = sections.length;
	  for(var i=0;i<sectionCount;i++){
		  var currentSection = sections[i];
		  var section = new Object();
		  section.name = $(currentSection).attr("data-section-title");		  
		  var currentQuestions = $('[data-item="item_'+i+'"]');
		  var currentQuestions_data = [];
		  jQuery.each(currentQuestions, function(j, question){
			  var iQuestion = new Object();
			  iQuestion.no = $(question).attr("data-item-id");		
			  iQuestion.question = $(question).attr("data-item-question");		
			  var input_data = $(question).find('input[type="text"]');
			  var data = []; 
			  jQuery.each(input_data, function(j, it){
				  var value = $(it).val();
				  data.push(parseInt(value));
			  });
			  iQuestion.data = data;
			  currentQuestions_data.push(iQuestion);
		  });
		  section.questions = currentQuestions_data;
		  sections_data.push(section);
	  }
	  
	  evaluation.sections = sections_data;

		//procudure for save data
		var URL_API = '<?=$this->config->item('base_url')?>';
		//get data
		var formUrl = URL_API+'/index.php/lpp/management_training/evaluation_post/<?=(isset($training_id) ? $training_id : '' )?>';

		var ukeh = jQuery.parseJSON(JSON.stringify(evaluation));
		var sendInfo = JSON.stringify(ukeh);
			$.ajax({
			           type: "POST",
			           url: formUrl,
			           contentType: "application/json",
			           success: function (msg) {
						   window.location.assign(URL_API+'/index.php/lpp/management_training/training');
			           },
					   error: function(xhr, status, error) {
						   console.log(xhr);
					    },

			           data: sendInfo
			       });	
	});
	
});
</script>  
