<?php 
	$page=$this->uri->segment(2); 	
	$page1=$this->uri->segment(3);
	$me = ($this->connect_auth->get_access_token() ? $this->connect_auth->get_me() : false);
	
	// echo'<pre>';
	// echo var_dump($page);
	// echo'</pre>';
?>
<div class="sidebar-container">
	<ul class="nav nav-sidebar">
    	<li <?=($page1=='' ? 'class="active"' : '')?>>
			<a href="<?=site_url('peserta/participant/')?>"><i class="glyphicon glyphicon-home"></i>&nbsp;&nbsp;Dashboard</a>
		</li>
        <li>
			<a target='_blank' href="<?=site_url('login/my_profile')?>"><i class="glyphicon glyphicon-user"></i>&nbsp;&nbsp;Profil <?=strtoupper($this->connect_auth->get_me()['entity'])?></a>
		</li>
		
		<li <?=($page == "management_training" ? 'class="active"' : '')?>>
		      <a role="button" data-toggle="collapse" href="#collapseMenu" <?=($page == "management_training" ? 'aria-expanded="true"' : 'aria-expanded="false"')?> aria-controls="collapseMenu" class="toggle">
			      <i class="glyphicon glyphicon-education"></i>&nbsp;&nbsp;Pelatihan <span class="pull-right"><i <?=($page == "management_training" ? 'class="fa fa-chevron-down"' : 'class="fa fa-chevron-right"')?>></i></span>
		      </a>
		      <div <?=($page == "management_training" ? 'class="collapse in"' : 'class="collapse"')?> id="collapseMenu">
		      <ul class="nav">
			      
			      
			      <li <?=($page1=='training' ? 'class="active"' : '')?>>
				      <a href="<?=site_url('peserta/management_training/training')?>">List Pelatihan</a>
			      </li>
			      
			     
		      </ul>
		      </div>
		  </li>
		
        
    </ul>
</div>