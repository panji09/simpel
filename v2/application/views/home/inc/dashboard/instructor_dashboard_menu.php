<?php 
	$page=$this->uri->segment(2);
	$page1=$this->uri->segment(3);

	$me = ($this->connect_auth->get_access_token() ? $this->connect_auth->get_me() : false);
	
	// Check active URI for second menu
	$active = '';
	if ($page == 'teach_history') {
		$active = 'class="active"';
	} else if($page == 'training_history') {
		$active = 'class="active"';
	} else if($page == 'certification_history') {
		$active = 'class="active"';
	} else if($page == 'procurement_history') {
		$active = 'class="active"';
	}
	
	// echo'<pre>';
	// echo var_dump($active);
	// echo'</pre>';
?>
<div class="sidebar-container">
	<ul class="nav nav-sidebar">
    	<li <?=($page=='dashboard' ? 'class="active"' : '')?>>
			<a href="<?=site_url('narasumber/dashboard')?>"><i class="glyphicon glyphicon-home"></i>&nbsp;&nbsp;Dashboard</a>
		</li>
        <li>
			<a target='_blank' href="<?=site_url('login/my_profile')?>"><i class="glyphicon glyphicon-user"></i>&nbsp;&nbsp;Profil <?=strtoupper($this->connect_auth->get_me()['entity'])?></a>
		</li>
		
	<li <?=($page == "management_training" ? 'class="active"' : '')?>>
	    <a role="button" data-toggle="collapse" href="#collapseMenu" <?=($page == "management_training" ? 'aria-expanded="true"' : 'aria-expanded="false"')?> aria-controls="collapseMenu" class="toggle">
		    <i class="glyphicon glyphicon-education"></i>&nbsp;&nbsp;Pelatihan <span class="pull-right"><i <?=($page == "management_training" ? 'class="fa fa-chevron-down"' : 'class="fa fa-chevron-right"')?>></i></span>
	    </a>
	    <div <?=($page == "management_training" ? 'class="collapse in"' : 'class="collapse"')?> id="collapseMenu">
	    <ul class="nav">
		    
		    
		    <li <?=($page1=='training' ? 'class="active"' : '')?>>
			    <a href="<?=site_url('narasumber/management_training/training')?>">List Pelatihan</a>
		    </li>

	    </ul>
	    </div>
	</li>
	<li <?=($page=='certificate' ? 'class="active"' : '')?>>
		<a href="<?=site_url('dashboard/instructor/certificate')?>"><i class="glyphicon glyphicon-education"></i>&nbsp;&nbsp;Sertifikat</a>
	</li>
        	
    </ul>
</div>