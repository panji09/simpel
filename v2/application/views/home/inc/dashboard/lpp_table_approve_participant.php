<table class="table table-bordered table-striped table-hover lpp_approval_participant" id="">
							<thead>
								<tr>
									<th>Tanggal Daftar</th>
									<th>Nama Peserta</th>
									<th>Pelatihan</th>
									<th>Tanggal Pelatihan</th>
									<th>tempat Pelatihan</th>
									<th>Status</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<?php
								    
								    
								    
								    
								    if($query)
								    foreach($query as $row):
								    
								?>
						    
								<tr>
									<td><time class="timeago" datetime="<?=date('c',$row['created'])?>"><?=date("F j, Y, g:i a",$row['created']) ?></td>
									<td><?=$row['user_join']['fullname']?></td>
									<td>
										<a href="#" 
										class="review_training_submission"
										data-toggle="modal"
										data-training_id="<?=$row['training_id']?>"
										data-target="#review_training_submission">
										<?=$row['training_log']['type_training']['name']?> / <?=$row['training_log']['training_name']['name']?></a>
									</td>
									<td><?=$row['training_log']['training_date']?></td>
									<td><?=$row['training_log']['training_location_address']?>, <?=$row['training_log']['training_location_district']['name']?> - <?=$row['training_log']['training_location_province']['name']?></td>
									<td>
									<?php
									    $verifikasi = array();
									    
									    if(isset($row['institute_approved']))
									    switch($row['institute_approved']){
										case 0 : $verifikasi = array('name' => 'Menunggu Verifikasi', 'label' => 'label-warning'); break;
										case 1 : $verifikasi = array('name' => 'Disetujui', 'label' => 'label-success'); break;
										case -1 : $verifikasi = array('name' => 'Klarifikasi', 'label' => 'label-warning'); break;
										case -2 : $verifikasi = array('name' => 'Tolak', 'label' => 'label-danger'); break;
										default : $verifikasi = array('name' => 'Menunggu Verifikasi', 'label' => 'label-warning'); break;
									    }
									?>
									<span class="label label-sm <?=$verifikasi['label']?>"> <?=$verifikasi['name']?>
									</td>
									<td>
										<div class="margin-bottom-5">
										    <button type="button" 
											class="btn btn-xs btn-primary overview_join_participant" 
											data-toggle="modal"
											data-join_user_id="<?=$row['user_join']['user_id']?>"
											data-join_training_id="<?=$row['_id']->{'$id'}?>"
											data-url_submit="<?=site_url('lpp/management_training/approval_participant_post/'.$row['_id']->{'$id'})?>"
											data-target="#participant_info_modal">Overview</button>
										</div>
										
									</td>
								</tr>
								<?php endforeach;?>
							</tbody>
						</table> 
