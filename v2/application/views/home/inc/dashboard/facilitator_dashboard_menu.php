<?php 
	$page=$this->uri->segment(3); 	
	$me = ($this->connect_auth->get_access_token() ? $this->connect_auth->get_me() : false);
	
	// echo'<pre>';
	// echo var_dump($page);
	// echo'</pre>';
?>
<div class="sidebar-container">
	<ul class="nav nav-sidebar">
    	<li <?=($page=='' ? 'class="active"' : '')?>>
			<a href="<?=site_url('dashboard/dashboard/'.$me['entity'])?>"><i class="glyphicon glyphicon-home"></i>&nbsp;&nbsp;Dashboard</a>
		</li>
        <li>
			<a target='_blank' href="<?=site_url('login/my_profile')?>"><i class="glyphicon glyphicon-user"></i>&nbsp;&nbsp;Profil <?=strtoupper($this->connect_auth->get_me()['entity'])?></a>
		</li>
        <li <?=($page=='scheduling' ? 'class="active"' : '')?>>
			<a href="<?=site_url('dashboard/facilitator/scheduling')?>"><i class="glyphicon glyphicon-time"></i>&nbsp;&nbsp;Penjadwalan Narasumber</a>
		</li>
		<li <?=($page=='nominate_instructor' ? 'class="active"' : '')?>>
			<a href="<?=site_url('dashboard/facilitator/nominate_instructor')?>"><i class="glyphicon glyphicon-star"></i>&nbsp;&nbsp;Nominasi Narasumber</a>
		</li>
        <li <?=($page=='notification' ? 'class="active"' : '')?>>
			<a href="<?=site_url('dashboard/facilitator/notification')?>"><i class="glyphicon glyphicon-info-sign"></i>&nbsp;&nbsp;Notifikasi</a>
		</li>
    </ul>
</div>