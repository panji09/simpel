
						  <table class="table table-bordered table-striped table-hover table_lpp_training" id="">
							  <thead>
								  <tr>
									  <th>Tanggal</th>
									  <th>Tanggal Pelatihan</th>
									  <th>Nama Pelatihan</th>
									  <th>No Surat Permohonan</th>
									  <th>Verifikasi</th>
									  <th>Status</th>
									  <th>Action</th>
								  </tr>
							  </thead>
							  <tbody>
								  <?php
						      
								      
						      
								      if($query):
									  foreach($query as $row):
							      
								  ?>
								  <tr class="odd gradeX">
								      <td><time class="" datetime="<?=date("d/m/Y H:i:s",$row['created'])?>"><?=date("d/m/Y H:i:s",$row['created'])?></time></td>
								      <?php
										$date_training = explode(' - ', $row['training_date']);
									  ?>
									  <td><?=tgl_indo($date_training[0]).' - '.tgl_indo($date_training[1])?></td>
								      <td><?=$row['type_training']['name'].' / '.$row['training_name']['name']?></td>
								      
								      <td><a href='<?=$row['mail_submission_upload']?>' target='_blank'><?=$row['mail_submission_id']?></a></td>
								      
								      <td>
									  <?php
									      $verifikasi = array();
									      
									      if(isset($row['verified_submission_training']))
									      switch($row['verified_submission_training']){
										  case 0 : $verifikasi = array('name' => 'Menunggu Verifikasi', 'label' => 'label-warning'); break;
										  case 1 : $verifikasi = array('name' => 'Disetujui', 'label' => 'label-success'); break;
										  case -1 : $verifikasi = array('name' => 'Klarifikasi', 'label' => 'label-warning'); break;
										  case -2 : $verifikasi = array('name' => 'Tolak', 'label' => 'label-danger'); break;
										  default : $verifikasi = array('name' => 'Menunggu Verifikasi', 'label' => 'label-warning'); break;
									      }
									  ?>
									  <span class="label label-sm <?=$verifikasi['label']?>"> <?=$verifikasi['name']?> </span>
								      </td>
								      
								      <td>
									  <?php
									      $status = array();
									      
									      if(isset($row['draft'])){
										  $status = array('name' => 'draft', 'label' => 'label-warning');
									      }
									      
									      if(isset($row['verified_submission_training']))
									      switch($row['verified_submission_training']){
										  case 0 : 
										  case -1 : 
										  case -2 : $status = array('name' => 'Tahap Pengajuan', 'label' => 'label-warning'); break;
										  case 1 : $status = array('name' => 'Tahap Persiapan', 'label' => 'label-warning'); break;
										  
										  
									      }
									  ?>
									  <span class="label label-sm <?=$status['label']?>"><?=$status['name']?></span>
									  
									  <?php if(isset($row['rescheduled']) && $row['rescheduled']):
									      $status = array('name' => 'Reschedule', 'label' => 'label-default');
									  ?>
									  
									  <span class="label label-sm <?=$status['label']?>"><?=$status['name']?></span>

									  <?php endif;?>
								      </td>
								      <td>
										  
										  
									    
									      <?php
										  //review admin
										  if($row['verified_submission_training'] == 0):
									      ?>
										  <div class="margin-bottom-5">
										      <button type="button" class="btn btn-xs purple review_training_submission" data-toggle="modal" data-training_id='<?=$row['_id']->{'$id'}?>' data-target="#review_training_submission">Ringkasan</button>
										  </div>
									      <?php endif;//review admin?>
									      
									      <?php
										  //klarifikasi
										  if($row['verified_submission_training'] == -1):
									      ?>
										  <div class="margin-bottom-5">
											  <button type="button" class="btn btn-xs btn-warning" onclick="location.href='<?=site_url('lpp/management_training/training/edit/'.$row['_id']->{'$id'})?>'">Edit</button>
										  </div>
									      
										  <div class="margin-bottom-5">
										      <button type="button" class="btn btn-xs btn-danger delete-user" data-url-delete="<?=site_url('lpp/management_training/training/delete/'.$row['_id']->{'$id'})?>" data-toggle="modal" data-target="#delete">Hapus</button>
										  </div>
									      <?php endif;//klarifikasi?>
										  
									      <?php
										  //tolak
										  if($row['verified_submission_training'] == -2):
									      ?>
									      
									      <?php endif;//tolak?>
									      
									      
									      <?php
										  //terima
										  if($row['verified_submission_training'] == 1):
									      ?>
										  <div class="margin-bottom-5">
											  <button type="button" class="btn btn-xs default" onclick="location.href='<?=site_url('lpp/management_training/training/reschedule/'.$row['_id']->{'$id'})?>'">Reschedule</button>											
										  </div>
										  
										  <div class="margin-bottom-5">
										      <button type="button" class="btn btn-xs purple review_training_submission" data-toggle="modal" data-training_id='<?=$row['_id']->{'$id'}?>' data-target="#review_training_submission">Ringkasan</button>
										  </div>
										  
										  <div class="margin-bottom-5">
											  <button type="button" class="btn btn-xs blue" onclick="location.href='<?=site_url('lpp/management_training/training/detail/'.$row['_id']->{'$id'})?>'">Detail</button>
										  </div>
										  
										  
										  
									      <?php endif;//terima?>
										  
									      
										  
										  
								      </td>
								  </tr>
							  
								      <?php endforeach;?>
								  <?php endif;?>
							  </tbody>
						  </table>
					   
