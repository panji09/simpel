<?php 
	$page=$this->uri->segment(2);
	$page1=$this->uri->segment(3);
	
	$me = ($this->connect_auth->get_access_token() ? $this->connect_auth->get_me() : false);
	
	// Check active URI for second menu
	$active = '';
	if ($page == 'training_submission') {
		$active = 'class="active"';
	} else if($page == 'training') {
		$active = 'class="active"';
	}
	
	
	// echo'<pre>';
	// echo var_dump($page);
	// echo'</pre>';
?>
<div class="sidebar-container">
	<ul class="nav nav-sidebar">
    	<li <?=($page=='dashboard' ? 'class="active"' : '')?>>
	    <a href="<?=site_url('lpp/dashboard')?>"><i class="glyphicon glyphicon-home"></i>&nbsp;&nbsp;Dashboard</a>
	</li>
        <li>
	    <a target='_blank' href="<?=site_url('login/my_profile')?>"><i class="glyphicon glyphicon-user"></i>&nbsp;&nbsp;Profil <?=strtoupper($this->connect_auth->get_me()['entity'])?></a>
	</li>
		
	<li <?=($page == "management_training" ? 'class="active"' : '')?>>
	    <a role="button" data-toggle="collapse" href="#collapseMenu" <?=($page == "management_training" ? 'aria-expanded="true"' : 'aria-expanded="false"')?> aria-controls="collapseMenu" class="toggle">
		    <i class="glyphicon glyphicon-education"></i>&nbsp;&nbsp;Pelatihan <span class="pull-right"><i <?=($page == "management_training" ? 'class="fa fa-chevron-down"' : 'class="fa fa-chevron-right"')?>></i></span>
	    </a>
	    <div <?=($page == "management_training" ? 'class="collapse in"' : 'class="collapse"')?> id="collapseMenu">
	    <ul class="nav">
		    
		    
		    <li <?=($page1=='training' ? 'class="active"' : '')?>>
			    <a href="<?=site_url('lpp/management_training/training')?>">List Pelatihan</a>
		    </li>
		    
		    <li <?=($page1=='approve_participant' ? 'class="active"' : '')?>>
			    <a href="<?=site_url('lpp/management_training/approve_participant')?>">Approve Peserta Pelatihan</a>
		    </li>
	    </ul>
	    </div>
	</li>
	
	<li <?=($page == 'pages' ? 'class="active"' : '')?>>
		<a role="button" data-toggle="collapse" href="#collapseMenu1" <?=($page == 'pages' ? 'aria-expanded="true"' : 'aria-expanded="false"')?> aria-controls="collapseMenu" class="toggle"><i class="glyphicon glyphicon-list-alt"></i>&nbsp;&nbsp;Halaman <span class="pull-right"><i <?=($page == 'pages' ? 'class="fa fa-chevron-down"' : 'class="fa fa-chevron-right"')?>></i></span>
		</a>
		<div <?=(in_array($page1, array('gallery','news')) ? 'class="collapse in"' : 'class="collapse"')?> id="collapseMenu1">
		    <ul class="nav">
			<li <?=($page1=='gallery' ? 'class="active"' : '')?>>
			    <a href="<?=site_url('lpp/pages/gallery')?>">Galeri Foto</a>
			</li>
			<li <?=($page1=='news' ? 'class="active"' : '')?>>
				<a href="<?=site_url('lpp/pages/news')?>">Berita</a>
			</li>
		    </ul>
		</div>
	</li>
		
		
		
        <li <?=($page=='manual_book' ? 'class="active"' : '')?>>
			<a target='_blank' href="<?=site_url('lpp/lpp/manual_book')?>"><i class="glyphicon glyphicon-book"></i>&nbsp;&nbsp;Manual</a>
		</li>
    </ul>
</div>