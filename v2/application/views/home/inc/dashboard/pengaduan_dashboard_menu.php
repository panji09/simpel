<?php 
	$page=$this->uri->segment(2);
	$page1=$this->uri->segment(3);

	$me = ($this->connect_auth->get_access_token() ? $this->connect_auth->get_me() : false);
	
	// echo'<pre>';
	// echo var_dump($active);
	// echo'</pre>';
?>
<div class="sidebar-container">
	<ul class="nav nav-sidebar">
		<li <?=($page=='dashboard' ? 'class="active"' : '')?>>
			<a href="<?=site_url('pengaduan/pengaduan/')?>"><i class="fa fa-exclamation-circle"></i>&nbsp;&nbsp;Pengaduan</a>
		</li>
		<li <?=($page=='pengaduan_online' ? 'class="active"' : '')?>>
			<a href="<?=site_url('pengaduan/pengaduan_online/')?>"><i class="fa fa-mail-forward"></i>&nbsp;&nbsp;Kirim Pengaduan</a>
		</li>
		<li <?=($page=='pengaduan_statistic' ? 'class="active"' : '')?>>
			<a href="<?=site_url('pengaduan/pengaduan_statistic/')?>"><i class="fa  fa-pie-chart"></i>&nbsp;&nbsp;Statistik Pengaduan</a>
		</li>
	</ul>
</div>