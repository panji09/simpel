<?php
    $page = $this->uri->segment(1);
    $page1 = $this->uri->segment(2);
    
?>



<script type="<?=$this->config->item('home_js')?>/jquery-1.10.2.min.js"></script>
<script src="<?=$this->config->item('plugin')?>/bootstrap/js/bootstrap.min.js"></script>

<!-- DATA TABLE PLUGIN -->
<script type="text/javascript" src="<?=$this->config->item('plugin')?>/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?=$this->config->item('plugin')?>/datatables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
<script type="text/javascript" src="<?=$this->config->item('plugin')?>/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<script type="text/javascript" src="http://timeago.yarp.com/jquery.timeago.js"></script>
<!-- END DATA TABLE PLUGIN -->

<!-- SCROLL ON NAVIGATION -->
<script type="text/javascript" src="<?=$this->config->item('home_js')?>/perfect-scrollbar.min.js"></script>
<!-- END SCROLL ON NAVIGATION -->

<!-- OWL CAROUSEL -->
<script src="<?=$this->config->item('home_js')?>/jquery.owl.carousel.js" type="text/javascript"></script>
<!-- END OWL CAROUSEL -->

<script src="<?=$this->config->item('home_js')?>/plugins.js"></script>
<script src="<?=$this->config->item('home_js')?>/custom.js"></script>
<script src="<?=$this->config->item('home_js')?>/simpel.js"></script>

<?php if($page == 'lpp' && $page1 == 'pages'):?>
<?php $this->load->view('home/inc/script_footer_lpp_pages');?>
<?php endif;?>

<?php if($page == 'instructor'):?>
<?php $this->load->view('home/inc/script_footer_instructor');?>
<?php endif;?>

<?php if($page == 'institution'):?>
<?php $this->load->view('home/inc/script_footer_institution');?>
<?php endif;?>

<?php if($page == 'training'):?>
<?php $this->load->view('home/inc/script_footer_training');?>
<?php endif;?>

<?php if($page == 'lpp' && $page1 == 'management_training'):?>
<?php $this->load->view('home/inc/script_footer_lpp_management_training');?>
<?php endif;?>

<?php if($page == 'peserta' && $page1 == 'management_training'):?>
<?php $this->load->view('home/inc/script_footer_peserta_management_training');?>
<?php endif;?>

<?php if($page == 'narasumber' && $page1 == 'management_training'):?>
<?php $this->load->view('home/inc/script_footer_instructor_management_training');?>
<?php endif;?>

<?php if($page == 'narasumber' && $page1 == 'history'):?>
<?php $this->load->view('home/inc/script_footer_instructor_story');?>
<?php endif;?>

<?php if($page == 'pengaduan'):?>
<?php $this->load->view('home/inc/script_footer_pengaduan');?>
<?php endif;?>

<?php if($page == 'gallery'):?>
<?php $this->load->view('home/inc/script_footer_gallery')?>
<?php endif; // ./gallery?>

<!--home-->
<script>
$(document).ready(function(){
    // Javascript to enable link to tab
    var url = document.location.toString();
    if (url.match('#')) {
	$('.nav-tabs a[href=#'+url.split('#')[1]+']').tab('show') ;
    } 

    // Change hash for page-reload
    $('.nav-tabs a').on('shown.bs.tab', function (e) {
	window.location.hash = e.target.hash;
    });
    $("time.timeago").timeago();
    
	<?php
	/*
    $(".delete-user").click(function(){
		var url_delete = $(this).data('url-delete');
		$("#modal-delete-ya").attr('onclick', 'location.href="'+url_delete+'"' );
    });
    */
	?>
	$(document).on('click',".delete-user",function(){
		var url_delete = $(this).data('url-delete');
		$("#modal-delete-ya").attr('onclick', 'location.href="'+url_delete+'"' );
    });
	
    $(document).on('click','#loadmore_news', function(){
		var last_created = $('.list-event-item').last().attr('data-created');
		console.log(last_created);
		$.get( "<?=site_url('home/pages/ajax_news_loadmore/')?>/"+last_created, 
			function( data ) {
		    	$( "#container-news" ).append( data);
			}
		);
    });
    
    $(document).on('click','#loadmore_search', function(){
		var last_created = $('.search-classic').last().attr('data-created');
		var search = $('#search').val();
		
		console.log(search+'----'+last_created);
		$.get( "<?=site_url('home/search/ajax_search_loadmore/')?>/"+last_created+"/"+search, 
			function( data ) {
		    	$( "#container-search" ).append( data);
			}
		);
    });
    
	<?php
	/*
    $('#loadmore_gallery').click(function(){
		var last_created = $('.gallery-album').last().attr('data-created');
		console.log(last_created);
		$.get( "<?=site_url('home/pages/ajax_album_loadmore/')?>/"+last_created, 
			function( data ) {
				$( "#container-gallery-album" ).append( data);
		
			});
	});
    
    $(".toggle").click(function () {
		
	    if ($(this).attr('aria-expanded') == 'false') {
		    // console.log('hello');
		    $(this).find( ".fa" ).removeClass( "fa-chevron-right" ).addClass( "fa-chevron-down" );
	    } else {
		    $(this).find( ".fa" ).removeClass( "fa-chevron-down" ).addClass( "fa-chevron-right" );
	    }
    });	
	*/
	?>
	
	$(document).on('click','#loadmore_gallery',function(){
		var last_created = $('.gallery-album').last().attr('data-created');
		console.log(last_created);
		$.get( "<?=site_url('home/pages/ajax_album_loadmore/')?>/"+last_created, 
			function( data ) {
				$( "#container-gallery-album" ).append( data);
		
			});
	});
    
    $(document).on('click',".toggle",function () {
		
	    if ($(this).attr('aria-expanded') == 'false') {
		    // console.log('hello');
		    $(this).find( ".fa" ).removeClass( "fa-chevron-right" ).addClass( "fa-chevron-down" );
	    } else {
		    $(this).find( ".fa" ).removeClass( "fa-chevron-down" ).addClass( "fa-chevron-right" );
	    }
    });
	
	// HANDLE SEARCH BUTTON
	
	<?php
	/*
    $('.search-btn').click(function () {            
        if($('.search-btn').hasClass('show-search-icon')){
            if ($(window).width()>767) {
                $('.search-box').fadeOut(300);
            } else {
                $('.search-box').fadeOut(0);
            }
            $('.search-btn').removeClass('show-search-icon');
        } else {
            if ($(window).width()>767) {
                $('.search-box').fadeIn(300);
            } else {
                $('.search-box').fadeIn(0);
            }
            $('.search-btn').addClass('show-search-icon');
        } 
    });
    */
	?>
    
	$(document).on('click','.search-btn',function () {            
        if($('.search-btn').hasClass('show-search-icon')){
            if ($(window).width()>767) {
                $('.search-box').fadeOut(300);
            } else {
                $('.search-box').fadeOut(0);
            }
            $('.search-btn').removeClass('show-search-icon');
        } else {
            if ($(window).width()>767) {
                $('.search-box').fadeIn(300);
            } else {
                $('.search-box').fadeIn(0);
            }
            $('.search-btn').addClass('show-search-icon');
        } 
    });
	
	// close search box on body click
    if($('.search-btn').size() != 0) {
        $('.search-box, .search-btn').on('click', function(e){
            e.stopPropagation();
        });

        $('body').on('click', function() {
            if ($('.search-btn').hasClass('show-search-icon')) {
                $('.search-btn').removeClass("show-search-icon");
                $('.search-box').fadeOut(300);
            }
        });
    }
	
});
</script>
<!--./home-->

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-68709704-1', 'auto');
  ga('send', 'pageview');

</script>

<!-- Piwik -->
<script type="text/javascript">
  var _paq = _paq || [];
  _paq.push(["setDomains", ["*.simpel.lkpp.go.id"]]);
  _paq.push(['trackPageView']);
  _paq.push(['enableLinkTracking']);
  (function() {
    var u="//piwik.ruangpanji.com/";
    _paq.push(['setTrackerUrl', u+'piwik.php']);
    _paq.push(['setSiteId', 3]);
    var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
    g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
  })();
</script>
<noscript><p><img src="//piwik.ruangpanji.com/piwik.php?idsite=3" style="border:0;" alt="" /></p></noscript>
<!-- End Piwik Code -->

<!-- Go to www.addthis.com/dashboard to customize your tools -->
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5678f29db4429d59" async="async"></script>
