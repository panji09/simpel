
		      <div class="portlet light">
			  <div class="portlet-title tabbable-line">
			      <div class="caption caption-md">
				  <i class="icon-globe theme-font-color hide"></i>
				  <span class="caption-subject theme-font-color bold uppercase">Peserta</span>
			      </div>
			      <ul class="nav nav-tabs pull-right">
				  <li class="active">
				      <a href="#tab_list_participant" data-toggle="tab">List Peserta </a>
				  </li>
				  
				  <li>
				      <a href="#tab_status_participant" data-toggle="tab">Status Peserta </a>
				  </li>
				  
				  <li>
				      <a href="#tab_set_participant" data-toggle="tab">Set Peserta </a>
				  </li>
			      </ul>
			  </div>
			  
			  <div class="portlet-body">
			      <div class="tab-content">
				  <div class="tab-pane active" id="tab_list_participant">
				      <div class="portlet light">
					    <div class="portlet-title">
					      <div class="caption font-green-sharp">
						  <span class="caption-subject bold uppercase"> List Peserta</span>						
					      </div>
					      <div class="actions">
						  <div class="btn-group">
							    <a class="btn btn-circle btn-default " href="javascript:;" data-toggle="dropdown">
							    <i class="fa fa-cloud-download"></i> Export <i class="fa fa-angle-down"></i>
							    </a>
							    <ul class="dropdown-menu pull-right">
								    <li>
									    <a href="<?=site_url('lpp/management_training/approve_participant/export/list_approved/?file=csv&training_id='.$training_id)?>">
									    <i class="fa fa-file-excel-o"></i> CSV </a>
								    </li>
								    
							    </ul>
						    </div>
						  <a href="javascript:;" class="btn btn-circle btn-default btn-icon-only fullscreen"></a>
					      </div>
					    </div>
					    <div class="portlet-body form portlet-empty">
					      <?php $this->load->view('home/inc/ajax/list_participant')?>
					    </div>
					</div>
						    
				  </div>
				  
				  <div class="tab-pane" id="tab_status_participant">
				       <div class="portlet light">
					    <div class="portlet-title">
					      <div class="caption font-green-sharp">
						  <span class="caption-subject bold uppercase"> Status Peserta</span>						
					      </div>
					      <div class="actions">
						    <div class="btn-group">
							    <a class="btn btn-circle btn-default " href="javascript:;" data-toggle="dropdown">
							    <i class="fa fa-cloud-download"></i> Export <i class="fa fa-angle-down"></i>
							    </a>
							    <ul class="dropdown-menu pull-right">
								    <li>
									    <a href="<?=site_url('lpp/management_training/approve_participant/export/list_waiting/?file=csv&training_id='.$training_id)?>">
									    <i class="fa fa-file-excel-o"></i> CSV </a>
								    </li>
								    
							    </ul>
						    </div>
						  <a href="javascript:;" class="btn btn-circle btn-default btn-icon-only fullscreen"></a>
					      </div>
					    </div>
					    <div class="portlet-body form portlet-empty">
					      <?php $this->load->view('home/inc/ajax/status_participant')?>
					    </div>
					</div>
				  </div>
				  
				  <div class="tab-pane" id="tab_set_participant">
						<?php
							$query = $this->training_db->get($training_id);
							if($query){
								$training = $query[0];
								
							}
							
							if(isset($training['type_registration_participant']['id']) && $training['type_registration_participant']['id'] == 'closed'):
						?>
						<?php $this->load->view('home/inc/ajax/set_participant')?>
						<?php $this->load->view('home/inc/ajax/table_invite_participant')?>
						
						<?php
							else:
						?>
							<strong>Disabled</strong>
						<?php
							endif;
						?>
				  </div>
				  
			      </div>  
			  </div>
			  
		      </div>
		   
