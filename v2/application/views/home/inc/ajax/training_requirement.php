<?php
    $query = $this->join_training_db->get($join_training_id);
    
    if($query):
	$join_training = $query[0];
?>
<div class="form-horizontal form-edit">
	<?php if(isset($join_training['letter_of_assignment_id'])):?>
	<div class="form-group">
		<label for="inputEmail3" class="col-sm-2 control-label">Surat Tugas</label>
		<div class="col-sm-10">
			<label class="text-content">
				 <a target='_blank' href='<?=$join_training['letter_of_assignment_upload']?>'><?=$join_training['letter_of_assignment_id']?></a>
			</label>
		</div>
	</div>
	<?php endif;?>
	
	<?php if(isset($join_training['certificate_procurement_expert_id'])):?>
	<div class="form-group">
		<label for="inputEmail3" class="col-sm-2 control-label">Sertifikat Ahli Pengadaan</label>
		<div class="col-sm-10">
			<label class="text-content">
				<a target='_blank' href='<?=$join_training['certificate_procurement_expert_upload']?>'><?=$join_training['certificate_procurement_expert_id']?></a> 
			</label>
		</div>
	</div>
	<?php endif;?>
	
	
	<?php if(isset($join_training['require_document_name'])):?>
	<h4>Syarat Upload</h4>
	<?php for($i=0; $i < count($join_training['require_document_name']); $i++):?>
	<div class="form-group">
		<label for="inputEmail3" class="col-sm-2 control-label"><?=$join_training['require_document_name'][$i]?></label>
		<div class="col-sm-10">
			<label class="text-content">
				 <a target='_blank' href='<?=$join_training['require_document_upload'][$i]?>'><?=urldecode(basename($join_training['require_document_upload'][$i]))?></a> 
			</label>
		</div>
	</div>
	<?php endfor;?>
	<?php endif;?>
	
</div> 
<?php endif;?>