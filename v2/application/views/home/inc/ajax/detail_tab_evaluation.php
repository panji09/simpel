
                     <div class="portlet light">
			  <div class="portlet-title tabbable-line">
			      <div class="caption caption-md">
				  <i class="icon-globe theme-font-color hide"></i>
				  <span class="caption-subject theme-font-color bold uppercase">Evaluasi</span>
			      </div>
			      <ul class="nav nav-tabs pull-right">
				  <li class="active">
				      <a href="#tab_evaluation_lpp" data-toggle="tab">Evaluasi Lembaga Pelatihan </a>
				  </li>
				  <li>
				      <a href="#tab_evaluation_instructor" data-toggle="tab">Evaluasi Pengajar </a>
				  </li>
			      </ul>
			  </div>
			  
			  <div class="portlet-body">
			      <div class="tab-content">
				  <div class="tab-pane active" id="tab_evaluation_lpp">
				      <!--evaluasi lpp-->
				      
						    <div class="portlet light">
							<div class="portlet-title">
							  <div class="caption font-green-sharp">
							      <span class="caption-subject bold uppercase"> Evaluasi Lembaga Pelatihan</span>						
							  </div>
							  <div class="actions">
							      
							      <a href="javascript:;" class="btn btn-circle btn-default btn-icon-only fullscreen"></a>
							  </div>
							</div>
							<div class="portlet-body form portlet-empty">
							  <?php $this->load->view('home/inc/widget/form_evaluation_lpp')?>
							</div>
						    </div>
						    
				      <!--./evaluasi lpp-->
				      
				  </div>
				  
				  <div class="tab-pane" id="tab_evaluation_instructor">
				      <!--evaluasi pengajar-->
				      <div class="portlet light">
					  <div class="portlet-title">
					    <div class="caption font-green-sharp">
						<span class="caption-subject bold uppercase"> Evaluasi Pengajar</span>						
					    </div>
					    <div class="actions">
						
						<a href="javascript:;" class="btn btn-circle btn-default btn-icon-only fullscreen"></a>
					    </div>
					  </div>
					  <div class="portlet-body form portlet-empty">
					    <?php $this->load->view('home/inc/widget/form_evaluation_instructor')?>
					  </div>
				      </div>
				      <!--evaluasi pengajar-->
				  </div>
				  
			      </div>  
			  </div>
			  
		      </div>
                   
