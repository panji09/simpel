<?php
    $query = $this->join_training_db->get($join_training_id);
    
    if($query):
	$join_training = $query[0];
	$user = $join_training['user_join'];
?>
<div class="form-horizontal form-edit">
      
	  <?php //print_r($user);?>
	  <div class="form-group">
	      <label for="inputEmail3" class="col-sm-2 control-label">Foto</label>
	      <div class="col-sm-10">
		      <label class="text-content">
			      <img style='max-height: 100px;' src='<?=$user['photo']?>'></img>
		      </label>
	      </div>
      </div>
      <div class="form-group">
	      <label for="inputEmail3" class="col-sm-2 control-label">Nama Peserta</label>
	      <div class="col-sm-10">
		      <label class="text-content">
			      <?=$user['fullname']?>
		      </label>
	      </div>
      </div>
	  
	  <div class="form-group">
	      <label for="inputEmail3" class="col-sm-2 control-label">NIP</label>
	      <div class="col-sm-10">
		      <label class="text-content">
			      <?=$user['nip_id']?>
		      </label>
	      </div>
      </div>
	  
	  <div class="form-group">
	      <label for="inputEmail3" class="col-sm-2 control-label">Institusi</label>
	      <div class="col-sm-10">
		      <label class="text-content">
			      <?=$user['institution']?>
		      </label>
	      </div>
      </div>
	  
	  <div class="form-group">
	      <label for="inputEmail3" class="col-sm-2 control-label">Lokasi Kantor</label>
	      <div class="col-sm-10">
		      <label class="text-content">
			      <?=$user['office_district']['name'].', '.$user['office_province']['name']?>
		      </label>
	      </div>
      </div>
	  
      <div class="form-group">
	      <label for="inputPassword3" class="col-sm-2 control-label">Jenis Kelamin</label>
	      <div class="col-sm-10">
		      <label class="text-content">
			      <?=(strtolower($user['gender']) == 'm' ? 'Laki-laki' : 'Perempuan')?>
		      </label>
	      </div>
      </div>
      <div class="form-group">
	      <label for="inputPassword3" class="col-sm-2 control-label">Email</label>
	      <div class="col-sm-10">
		      <label class="text-content">
			      <a href="mailto:<?=$user['email']?>"><?=$user['email']?></a>
		      </label>
	      </div>
      </div>
      <div class="form-group">
	      <label for="inputPassword3" class="col-sm-2 control-label">Tempat Lahir</label>
	      <div class="col-sm-10">
		      <label class="text-content">
			      <?=$user['birth_place']?>
		      </label>
	      </div>
      </div>
      <div class="form-group">
	      <label for="inputPassword3" class="col-sm-2 control-label">Tanggal Lahir</label>
	      <div class="col-sm-10">
		      <label class="text-content">
			      <?=$user['birth_date']?>
		      </label>
	      </div>
      </div>
      <div class="form-group">
	      <label for="inputPassword3" class="col-sm-2 control-label">No. Telp/HP</label>
	      <div class="col-sm-10">
		      <label class="text-content">
			      <?=$user['phone']?>
		      </label>
	      </div>
      </div>
	  
	  
</div> 
<?php endif;?>