<div class="portlet light">
							<div class="portlet-title">
							  <div class="caption font-green-sharp">
							  <span class="caption-subject bold uppercase"> Set Peserta</span>						
							  </div>
							  <div class="actions">
							  <a href="javascript:;" class="btn btn-circle red add_participant">
							  <i class="fa fa-plus"></i> Tambah Peserta </a>
							  <a href="javascript:;" class="btn btn-circle btn-default btn-icon-only fullscreen"></a>
							  </div>
							</div>
							<div class="portlet-body form portlet-empty">
							  
								<form method='post' action='<?=site_url('lpp/management_training/invite_participant/post/'.$training_id)?>'>
									<div class="table-responsive" id='table_content_set_participant' >
									  <?php $this->load->view('home/inc/ajax/table_set_participant');?>
									</div>
									<div class="form-actions">
									  <div class="row">
									  <div class="col-md-offset-2 col-md-10">
										<button type="submit" class="btn blue" name='submit' value='save'>Proses</button>
										
										
									  </div>
									  </div>
									</div>
								</form>
								
								<div id='template_table_set_participant' style='display:none'>
									<?php $this->load->view('home/inc/ajax/table_set_participant')?>
								</div>
							</div>
						</div>

 
