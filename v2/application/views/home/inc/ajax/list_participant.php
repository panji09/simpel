
<table class="table table-bordered table-striped table-hover lpp_approval_participant_status" id="">
<thead>
	<tr>
		<th>Tanggal Daftar</th>
		<th>Nama Peserta</th>
		<th>Pelatihan</th>
		<th>Tanggal Pelatihan</th>
		<th>tempat Pelatihan</th>
		<th>Action</th>
	</tr>
</thead>
<tbody>
	<?php
	    
	    
	    $query = $this->join_training_db->get_all(array(
		'training_id' => $training_id,
		'institute_approved' => 1
	    ));
	    
	    if($query)
	    foreach($query as $row):
	    
	?>

	<tr>
		<td><time class="timeago" datetime="<?=date('c',$row['created'])?>"><?=date("F j, Y, g:i a",$row['created']) ?></time></td>
		<td><?=$row['user_join']['fullname']?></td>
		<td>
			<a href="#" 
			class="review_training_submission"
			data-toggle="modal"
			data-training_id="<?=$row['training_id']?>"
			data-target="#review_training_submission">
			<?=$row['training_log']['type_training']['name']?> / <?=$row['training_log']['training_name']['name']?></a>
		</td>
		<td><?=$row['training_log']['training_date']?></td>
		<td><?=$row['training_log']['training_location_address']?>, <?=$row['training_log']['training_location_district']['name']?> - <?=$row['training_log']['training_location_province']['name']?></td>
		
		<td>
			<div class="margin-bottom-5">
			    <button type="button" 
				class="btn btn-xs btn-primary overview_join_participant" 
				data-toggle="modal"
				data-join_user_id="<?=$row['user_join']['user_id']?>"
				data-join_training_id="<?=$row['_id']->{'$id'}?>"
				data-url_submit="<?=site_url('lpp/management_training/approval_participant_post/'.$row['_id']->{'$id'})?>"
				data-target="#participant_info_modal">Overview</button>
			</div>
			
		</td>
	</tr>
	<?php endforeach;?>
</tbody>
</table> 
