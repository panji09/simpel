
                     <div class="portlet light">
			  <div class="portlet-title tabbable-line">
			      <div class="caption caption-md">
				  <i class="icon-globe theme-font-color hide"></i>
				  <span class="caption-subject theme-font-color bold uppercase">Todo List</span>
			      </div>
			      
			  </div>
			  
			  <div class="portlet-body">
			      <div class="row">
				<div class="col-md-12">
				    <?php $this->load->view('admin/inc/widget/todolist')?>
				</div>
			      </div>  
			  </div>
			  
		      </div>
                   
