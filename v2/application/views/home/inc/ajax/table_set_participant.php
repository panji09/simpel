<table class="table table-bordered table-striped table-hover" id="table_set_participant">
<thead>
	<tr>
		<th>Akun Connect</th>
		<th>Daftar</th>
		<th>Action</th>
	</tr>
</thead>
<tbody>
	
	<tr>
		<td>
		    <div class="form-group">
		      <select name='invite_via[]' class="form-control">
                <option value=''>-pilih-</option>
                <option value='email'>Email</option>
		      </select>
		    </div>
		    
		</td>
		<td>
		    <div class="form-group">
		      <input type="text" class="form-control" name='invite_value[]'>
		    </div>
		</td>
		<td>
		    <a href="javascript:;" class="btn default btn-xs black remove_participant">
			<i class="fa fa-trash-o "></i> Hapus 
		    </a>
		</td>
		
	</tr>

</tbody>
</table> 
 