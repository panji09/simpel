<div class="portlet light">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-speech"></i>
                                        <span class="caption-subject bold uppercase"> Invite</span>
                                    </div>
                                    <div class="actions">
                                        <a href="javascript:;" class="btn btn-circle btn-default btn-icon-only fullscreen"></a>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <table class="table table-striped table-bordered table-hover" id="table_instructor_invite">
                                    <thead>
                                    <tr>
                                        
                                        
                                        <th>
                                             Email
                                        </th>
                                        
                                        <th>
                                             Invitation Code
                                        </th>
                                        
                                        <th>
                                             Created
                                        </th>
                                        
                                        <th>
                                             Action
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                        $query = $this->invite_participant_db->get_all(array(
                                                                                    'training_id' => $training_id
                                                                                    ));
                                        foreach($query as $row):
                                    ?>
                                    <tr>
                                        <td><?=$row['email']?></td>
                                        <td><?=$row['invite_participant_code']?></td>
                                        <td>
                                            <?php
                                                $date = date("Y-m-j, h:i a",$row['created']);
                                                $date_ = explode(',',$date);
                                                echo $date_ = tgl_indo($date_[0]);
                                                echo ', '.$date = date("H:i",$row['created']);
                                            ?>
                                        </td>
                                        
                                        <td>
                                            <div class="margin-bottom-5">
                                                <a href="javascript:;" class="btn default btn-xs black delete-user" data-url-delete="<?=site_url('lpp/management_training/invite_participant/delete/'.$row['_id']->{'$id'})?>" data-toggle="modal" data-target="#delete">
                                                    <i class="fa fa-trash-o "></i> Hapus 
                                                </a>
                                                
                                            </div>
                                            
                                        </td>
                                        
                                    </tr>
                                    <?php endforeach;?>
                                    </tbody>
                                    </table>
                                </div>
                            </div>