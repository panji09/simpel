<div class="widget-main">
				<div class="widget-main-title">
				    <h4 class="widget-title">Galeri Foto</h4>
				</div>
				<div class="widget-inner">
				    <div class="gallery-small-thumbs clearfix">
					<?php
					    $query = $this->dynamic_pages_db->get_all(array('page_id'=>'gallery','published'=>1), $limit=10);

					    if($query)
					    foreach($query as $row):
								
					?>
					<div class="thumb-small-gallery closed" style="opacity: 1;">
					    <a class="fancybox" data-fancybox-group="gallery1" href="<?=site_url('gallery/'.$row['_id']->{'$id'})?>" title="<?=$row['title']?>">
						<img src="<?=$row['photo'][0]?>" alt="">
					    </a>
					</div>												
																			
					<?php endforeach;?>																			
				    </div> <!-- /.galler-small-thumbs -->
				</div> <!-- /.widget-inner -->
			    </div> 
