<?php
    //check var
    $config_training_id = (isset($config_training_id) ? $config_training_id : '');
	
	$input = $this->input->get();
	//print_r($input);
?>
<div class="course-search">
    <h3>Cari Pelatihan</h3>

    <form id="quick_form" class="course-search-form" action='<?=site_url('schedule/search')?>' method='get'>
	<div id="search_advanced" class="clearfix">
	    <div class="search-form-item" style="width: 28%; padding-right: 10px">
		<input name="query" value="<?=(isset($input['query']) ? $input['query'] : '')?>" placeholder="Cari Pelatihan" autocomplete="off"  title="Course Title">
	    </div>
						    
	    <div class="search-form-item" style="width: 28%; padding-right: 10px">
		<select class="searchselect" id="" name="type_training">
		    
		    <option value="">Jenis Pelatihan</option>
		    <?php 
			$query = $this->config_training_db->get_all(array('published' => 1));
			foreach($query as $row):
		    ?>
		    <option value="<?=$row['_id']->{'$id'}?>" <?=(isset($input['type_training']) && $input['type_training'] == $row['_id']->{'$id'} ? 'selected' : '') ?>><?=$row['name']?></option>
		    <?php endforeach;?>
		</select>
	    </div>
	    <div class="search-form-item">
		<select class="searchselect" id="Discipline" name="province">
		    <option value="">Provinsi</option>
		    <?php
			$query = file_get_contents($this->config->item('connect_api_url').'/location/province');
			$province = array();
			if($query){
			    $province = json_decode($query,true);
			}
			
			if($province)
			foreach($province as $row):
		    ?>
			<option value='<?=$row['id']?>' <?=(isset($input['province']) && $input['province'] == $row['id'] ? 'selected' : '')?>><?=$row['name']?></option>
		    <?php
		    endforeach;
		    ?>
		</select>
	    </div>
					    
	    <div class="search-form-item" style="width: 19%;">
		    <button type="submit" class="btn btn-primary pull-right">Cari Pelatihan</button>
	    </div>
	</div>
    </form>

</div>