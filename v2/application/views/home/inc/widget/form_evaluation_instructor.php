<?php
    $query = $this->training_db->get($training_id);
    $query_instructor = $this->hour_leasson_db->get_all(array('training_id' => $training_id, 'admin_approved' => 1, 'instructor_approved' => 1));
    if($query && $query_instructor):
    
    $content = $query[0];
    $form_id = $content['evaluation_form']['instructor'];
    $participant_amount = $this->join_training_db->count_approved_participant($training_id);
    //$content['participant_amount'];
    
    $query_evaluation_data = $this->evaluation_data_db->get_all(array(
	'training_id' => $training_id,
	'evaluation_form_id' => $content['evaluation_form']['instructor']
    ));
    $evaluation_data = array();
    $entity = $this->connect_auth->get_me()['entity'];
    if($query_evaluation_data && count($query_evaluation_data) == 1){
	$evaluation_data = $query_evaluation_data[0];
    }
?>
				<?php if(isset($content['verified_submission_training']) && $content['verified_submission_training']== -1):?>
				<div class="alert alert-warning alert-dismissible" role="alert">
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				  <?=nl2br($content['review_note'])?>
				</div>
				<?php endif;?>
				<!-- BEGIN PAGE CONTENT-->
				<form action="<?=site_url('lpp/management_training/evaluation_post/'.(isset($evaluation_data['_id']->{'$id'}) ? $evaluation_data['_id']->{'$id'} : '') )?>" id="form_sample_3" method='post' class="form-horizontal">
				
				<input type='hidden' name='training_id' value='<?=$training_id?>'>
				<input type='hidden' name='evaluation_form_id' value='<?=$content['evaluation_form']['instructor']?>'>
				
								<div class="form-body">									
									<h4 class="form-section">Data Evaluasi Penyelenggaraan Pelatihan</h4>
									
									<div class="form-group">
										<label class="control-label col-md-3">Nama Instansi 
										</label>
										<div class="col-md-7">
											<p class="form-control-static text-content"> <?=$content['user_submission']['training_institute']?> </p>
										</div>
										
									</div>
									<div class="form-group">
										<label class="control-label col-md-3">Tempat Pelatihan 
										</label>
										<div class="col-md-7">
											<p class="form-control-static text-content"> <?=$content['training_location_address']?> </p>
											
										</div>
										
									</div>									

									<div class="form-group">
										<label class="control-label col-md-3">Tanggal Pelatihan 
										</label>
										<div class="col-md-7">
											<div class='input-group date'>
											    
											    <p class="form-control-static text-content"> <?=$content['training_date']?> </p>
											</div>										      
										</div>										
									</div>
									
									<!-- input rekap here-->
									
			      <!-- begin evaluation form here-->
			      <?php 
				  $query_form = $this->evaluation_db->get($form_id);
				  foreach($query_instructor as $row_instructor):
			      ?>
			      <div class="table-responsive">
			      <table class="table table-striped">
				  <thead>
					    <tr>
						    <th rowspan="2">
							      No
						    </th>
						    <th colspan="2" rowspan="2">
							      Unsur Penilaian
						    </th>

						    <th colspan="<?=$participant_amount;?>" align='center'>
							      Responden
						    </th>
					    </tr>
					    <tr>
						    <?php
						    for($i=0; $i<$participant_amount; $i++):							
						    ?>
						    <th>
							      <?=$i+1;?>
						    </th>
						    <?php endfor;?>
					    </tr>
				    </thead>
				    <tbody>
				    
				    
				    <tr>
					<td colspan='3'><strong>Pengajar : <?=$row_instructor['instructor_approved_log']['fullname']?></strong></td>
					<td colspan='<?=$participant_amount;?>'></td>
					
				    </tr>
				    
				    <?php
					
					$no = $index_section = $index_question = 0;
					if($query_form):
					
					$content_form = $query_form[0];
					foreach($content_form['section'] as $section):
					
				    ?>
				    
				    <tr>
					<td colspan='3'><?=$section?></td>
					<td colspan='<?=$participant_amount;?>'></td>
					
				    </tr>
					<?php foreach($content_form['question'][$index_section] as $question):?>
					<tr>
					    <td><?=++$no?></td>
					    <td colspan='2'><?=$question?></td>
					    
					    <?php for($j=0; $j < $participant_amount; $j++):?>
					    <td>
						<input 
						    type="text" 
						    style='width:40px; border:1px solid;' 
						    name='data[<?=$row_instructor['instructor_approved_log']['user_id']?>][<?=$index_section?>][<?=$index_question?>][]'
						    
						    <?php
							$value_data = (isset($evaluation_data['data'][$row_instructor['instructor_approved_log']['user_id']][$index_section][$index_question][$j]) ? $evaluation_data['data'][$row_instructor['instructor_approved_log']['user_id']][$index_section][$index_question][$j] : '');
						    ?>
						    value='<?=$value_data?>'
						    
						    <?php if($entity != 'lpp'):?>
						    disabled
						    <?php endif;?>
						>
					    </td>
					    <?php endfor;?>
					    
					    
					</tr>
					<?php $index_question++; endforeach; $no=0; $index_question=0;?>
					
				    <?php  $index_section++; endforeach; endif;?>
				    
				    
				    </tbody>
			      </table>
			      </div>
			      <p>
										Kriteria Penilaian:<br />
										4: Baik Sekali<br />
										3: Baik<br />
										2: Kurang<br />
										1: Sangat Kurang<br />
									</p>
									      <div class="form-group"></div>
									      <div class="form-group">
										<label class="control-label col-md-3">Feedback 
										</label>
										<div class="col-md-9">
										<?php if($entity == 'lpp'):?>
											<textarea 
											name='feedback[<?=$row_instructor['instructor_approved_log']['user_id']?>]' 
											class='form-action wysiwyg-content-full' 
											id=''
											>
											<?=(isset($evaluation_data['feedback'][$row_instructor['instructor_approved_log']['user_id']]) ? $evaluation_data['feedback'][$row_instructor['instructor_approved_log']['user_id']] : '')?>
											</textarea>
										<?php else:?>
											<div class='form-action-static'>
											<?=(isset($evaluation_data['feedback'][$row_instructor['instructor_approved_log']['user_id']]) ? $evaluation_data['feedback'][$row_instructor['instructor_approved_log']['user_id']] : '')?>
											</div>
										<?php endif;?>
										
																				      
										</div>									
									</div>
				<?php endforeach; //end query instructor?>
									
								</div>
								<?php if($entity == 'lpp'):?>
								<div class="form-actions">
									<div class="row">
										<div class="col-md-offset-3 col-md-9">
											<button id="btn_save" type="submit" class="btn btn-primary" >Simpan</button>
											
											
										</div>
									</div>
								</div>
								<?php endif;?>
							</form>
				<!-- END PAGE CONTENT-->
				
				 
<?php endif;?>