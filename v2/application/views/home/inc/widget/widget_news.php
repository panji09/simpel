<div class="widget-main" >
	                        <div class="widget-main-title">
	                            <h4 class="widget-title"><a href='<?=site_url('news')?>'>Berita</a></h4>
	                        </div> <!-- /.widget-main-title -->
	                        <div class="widget-inner">
	                            <?php
					$query = $this->dynamic_pages_db->get_all(array('page_id'=>'news','published' => 1), $limit=5);
					
					if($query)
					foreach($query as $row):
				    ?>
	                            <div class="blog-list-post clearfix">
	                                <div class="blog-list-thumb">
	                                    <a href="<?=site_url('news/'.$row['_id']->{'$id'})?>"><img src="<?=(isset($row['cover']) ? $row['cover'] : 'http://placehold.it/65x65')?>" alt=""></a>
	                                </div>
	                                <div class="blog-list-details">
	                                    <h5 class="blog-list-title"><a href="<?=site_url('news/'.$row['_id']->{'$id'})?>"><?=$row['title']?></a></h5>
	                                    <p class="blog-list-meta small-text"><span><a href="#">
										<?php
										
										$date = date("Y-m-j, h:i a",$row['created']);
										$date_ = explode(',',$date);
										echo $date_ = tgl_indo($date_[0]);
										
										//echo ', '.$date = date("h:i a",$row['created']);
										echo ', '.$date = date("H:i",$row['created']);
										
										
										?>
										
										</a></span> </p>
	                                </div>
	                            </div> <!-- /.blog-list-post -->
	                            <?php endforeach;?>
	                        </div> <!-- /.widget-inner -->
	                    </div> <!-- /.widget-main --> 
