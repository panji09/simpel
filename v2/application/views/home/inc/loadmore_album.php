<div class="col-xs-12 col-sm-4 col-md-3 gallery-album" data-created='<?=$row['created']?>'>
	<div class="gallery-item fit-scale" style="background-image:url('<?=$row['photo'][0]?>')">
		<div class="photo-content">
			<a href="<?=site_url('home/pages/gallery/'.$row['_id']->{'$id'})?>" class="photo-over">&nbsp;</a>
			<div class="photo-overlay">
				<h4 class="gallery-title">
					<a href="<?=site_url('home/pages/gallery/'.$row['_id']->{'$id'})?>"><?=$row['title']?></a>
				</h4>			
			</div>
		</div>	            
	</div>
</div>
					