<?php
	$page = $this->uri->segment(1);
	$page1 = $this->uri->segment(2);
?>

<div class="modal fade" id="review_institution" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-user_id='1234'>
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<form method='post' action='#' class="form-horizontal">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
					<h4 class="modal-title">Review Lembaga Pelatihan</h4>
				</div>
				<div class="modal-body">
					<div>

						<!-- Nav tabs -->
						<ul class="nav nav-tabs" role="tablist" id='review_institution_tab'>
							<li role="presentation" class="active"><a href="#review_tab_0" role="tab" data-param='user_profile' data-toggle="tab">Profil Pengguna</a></li>
							<li role="presentation"><a href="#review_tab_1" role="tab" data-param='institution_office' data-toggle="tab">Data Lembaga</a></li>
							<li role="presentation"><a href="#review_tab_2" role="tab" data-param='institution_responsible_person' data-toggle="tab">Penanggung Jawab(Kepala LPP)</a></li>
							<li role="presentation"><a href="#review_tab_3" role="tab" data-param='institution_upload_data' data-toggle="tab">Upload data pelengkap</a></li>

							<?php if($this->connect_auth->is_logged_in() && $this->connect_auth->get_me()['entity'] == 'admin'):?>
								<li role="presentation"><a href="#review_tab_4" role="tab" data-param='review_note' data-toggle="tab">Pesan Tunda</a></li>
								<li role="presentation"><a href="#review_tab_5" role="tab" data-param='institution_approved' data-toggle="tab">Disetujui</a></li>
							<?php endif;?>
						</ul>

						<!-- Tab panes -->
						<div class="tab-content">
							<div role="tabpanel" class="tab-pane active" id="review_tab_0">
								<div class="spinner-loader">
									Loading..!
								</div>
							</div>
							<div role="tabpanel" class="tab-pane" id="review_tab_1">
								<div class="spinner-loader">
									Loading..!
								</div>
							</div>
							<div role="tabpanel" class="tab-pane" id="review_tab_2">
								<div class="spinner-loader">
									Loading..!
								</div>
							</div>
							<div role="tabpanel" class="tab-pane" id="review_tab_3">
								<div class="spinner-loader">
									Loading..!
								</div>
							</div>

							<?php if($this->connect_auth->is_logged_in() && $this->connect_auth->get_me()['entity'] == 'admin'):?>
								<div role="tabpanel" class="tab-pane" id="review_tab_4">
									<div class="spinner-loader">
										Loading..!
									</div>
								</div>
								<div role="tabpanel" class="tab-pane" id="review_tab_5">
									<div class="spinner-loader">
										Loading..!
									</div>
								</div>
							<?php endif;?>
						</div>

					</div>
				</div>
				<div class="modal-footer">
					<?php if($this->connect_auth->is_logged_in() && $this->connect_auth->get_me()['entity'] == 'admin'):?>

						<button type="submit" id='approved_institution_submit' class="btn blue" value='Disetujui'>Disetujui</button>
						<button type="submit" id='review_institution_submit' class="btn yellow" value='Tunda'>Tunda</button>
						<button type="button" class="btn red" onclick="location.href='#'" id='modal-reject'>Tolak</button>

					<?php endif;?>
					<button type="button" class="btn default" data-dismiss="modal">Tutup</button>
				</div>
			</form>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>

<?php if(in_array($page1, array('management_training')) ):?>

<div class="modal fade" id="choose_instructor_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-user_id='1234'>
	<div class="modal-dialog">
		<div class="modal-content">
			<form method='post' action='#' class="form-horizontal">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
					<h4 class="modal-title">Set Waktu Pengajar</h4>
				</div>
				<div class="modal-body"></div>
				<div class="modal-footer">
					<button type="button" class="btn blue" data-dismiss="modal" id='submit_set_instructor'>Set</button>
					<button type="button" class="btn default" data-dismiss="modal">Tutup</button>
				</div>
			</form>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>

<div class="modal fade" id="set_instructor_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-user_id='1234'>
	<div class="modal-dialog">
		<div class="modal-content">
			<form method='post' action='#' class="form-horizontal">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
					<h4 class="modal-title">Set Waktu Pengajar</h4>
				</div>
				<div class="modal-body"></div>
				<div class="modal-footer">
					<button type="button" class="btn blue" data-dismiss="modal" id='submit_set_instructor'>Set</button>
					<button type="button" class="btn default" data-dismiss="modal">Tutup</button>
				</div>
			</form>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>

<div class="modal fade" id="select_instructor_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-user_id='1234'>
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<form method='post' action='#' class="form-horizontal">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
					<h4 class="modal-title">Pilih Pengajar</h4>
				</div>
				<div class="modal-body"></div>
				<div class="modal-footer">
					<button type="button" class="btn blue" data-dismiss="modal" id='submit_select_instructor'>Set</button>
					<button type="button" class="btn default" data-dismiss="modal">Tutup</button>
				</div>
			</form>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>

	<div class="modal fade" id="review_training_submission" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-training_id='1234'>
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<form method='post' action='#' class="form-horizontal">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
						<h4 class="modal-title">Review Pengajuan Pelatihan</h4>
					</div>
					<div class="modal-body">
						<div>

							<!-- Nav tabs -->
							<ul class="nav nav-tabs" role="tablist" id='review_training_submission_tab'>
								<li role="presentation" class="active"><a href="#training_submission_progress" data-load_page='training_submission_progress' aria-controls="home" role="tab" data-toggle="tab">Progress</a></li>

								<li role="presentation"><a href="#training_submission_data" data-load_page='training_submission_data' aria-controls="profile" role="tab" data-toggle="tab">Data Pengajuan Pelatihan</a></li>

								<li role="presentation"><a href="#training_submission_location" data-load_page='training_submission_location' aria-controls="profile" role="tab" data-toggle="tab">Data Lokasi Pelatihan</a></li>

								<li role="presentation"><a href="#training_submission_participant" data-load_page='training_submission_participant' aria-controls="profile" role="tab" data-toggle="tab">Data Peserta</a></li>

								<li role="presentation"><a href="#training_submission_cp" data-load_page='training_submission_cp' aria-controls="profile" role="tab" data-toggle="tab">Data Panitia</a></li>
								
								<?php if($this->connect_auth->get_me()['entity']=='narasumber'):?>
								<li role="presentation"><a href="#training_submission_reject_instructor" data-load_page='' aria-controls="profile" role="tab" data-toggle="tab">Tolak</a></li>
								<?php endif;?>
								<?php if($this->connect_auth->get_me()['entity']=='admin'):?>
									<li role="presentation"><a href="#training_submission_review_note" role="tab" data-load_page='training_submission_review_note' data-toggle="tab"> Klarifikasi</a></li>

									<li role="presentation"><a href="#training_submission_approved" role="tab" data-load_page='training_submission_approved' data-toggle="tab">Disetujui</a></li>
								<?php endif;?>
							</ul>

							<!-- Tab panes -->
							<div class="tab-content">
								<div role="tabpanel" class="tab-pane active" id="training_submission_progress">
									<div class="spinner-loader">
										Loading..!
									</div>
								</div>
								<div role="tabpanel" class="tab-pane" id="training_submission_data">
									<div class="spinner-loader">
										Loading..!
									</div>
								</div>
								<div role="tabpanel" class="tab-pane" id="training_submission_location">
									<div class="spinner-loader">
										Loading..!
									</div>
								</div>
								<div role="tabpanel" class="tab-pane" id="training_submission_participant">
									<div class="spinner-loader">
										Loading..!
									</div>
								</div>

								<div role="tabpanel" class="tab-pane" id="training_submission_cp">
									<div class="spinner-loader">
										Loading..!
									</div>
								</div>
								<div role="tabpanel" class="tab-pane" id="training_submission_review_note">
									<div class="spinner-loader">
										Loading..!
									</div>
								</div>
								<div role="tabpanel" class="tab-pane" id="training_submission_approved">
									<div class="spinner-loader">
										Loading..!
									</div>
								</div>
								<div role="tabpanel" class="tab-pane" id="training_submission_reject_instructor">
								    <div class="form-group">
									<label for="inputEmail3" class="col-sm-2 control-label">Pesan</label>
									<div class="col-sm-10">
									    <textarea class="form-control" id='wysiwyg-content' rows="3" name='review_reject_note'><?=(isset($content['review_reject_note']) ? $content['review_reject_note'] : '')?></textarea>
									    
									</div>
								    </div>
								</div>
							</div>

						</div>
					</div>
					<div class="modal-footer">
						<?php if($this->connect_auth->get_me()['entity'] == 'admin'):?>
							<button type="submit" id='approved_training_submission_submit' class="btn blue" name='submit' value='approved'>Disetujui</button>
							<button type="submit" id='review_training_submission_submit' class="btn yellow" name='submit' value='clarification'>Klarifikasi</button>
							<button type="submit" class="btn red" name='submit' value='reject' id='modal-reject'>Tolak</button>

						<?php endif;?>
						
						<?php if($this->connect_auth->get_me()['entity']=='narasumber'):?>
							<button name='submit' type="submit" id='approved_training_submit' class="btn btn-primary"  value='approved'>Disetujui</button>
							<button type="submit" class="btn btn-red" name='submit' value='reject' id='modal-reject'>Tolak</button>
						<?php endif;?>
						<button type="button" class="btn default" data-dismiss="modal">Tutup</button>
					</div>
				</form>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	
	<div class="modal fade" id="review_instructor" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-training_id='1234'>
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<form method='post' action='#' class="form-horizontal">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
						<h4 class="modal-title">Review Pengajuan Pengajar</h4>
					</div>
					<div class="modal-body">
						<div>

							<!-- Nav tabs -->
							<ul class="nav nav-tabs" role="tablist" id='review_instructor_tab'>
								<li role="presentation" class="active">
								    <a href="#tab_instructor_0" data-load_page='user_profile' role="tab" data-toggle="tab">Data User</a>
								</li>

								<li role="presentation">
								    <a href="#tab_instructor_1" data-load_page='instructor_profile_detail'  role="tab" data-toggle="tab">Data User Detail</a>
								</li>

								
							</ul>

							<!-- Tab panes -->
							<div class="tab-content">
								<div role="tabpanel" class="tab-pane active" id="tab_instructor_0">
									<div class="spinner-loader">
										Loading..!
									</div>
								</div>
								<div role="tabpanel" class="tab-pane" id="tab_instructor_1">
									<div class="spinner-loader">
										Loading..!
									</div>
								</div>
								
							</div>

						</div>
					</div>
					<div class="modal-footer">
						<?php if($this->connect_auth->get_me()['entity'] == 'admin'):?>
							<input type='hidden' name='instructor_id' value=''>
							<button type="submit" id='' class="btn blue" name='submit' value='approved'>Disetujui</button>
							
							<button type="submit" class="btn red" name='submit' value='reject' id='modal-reject'>Tolak</button>

						<?php endif;?>
						<button type="button" class="btn default" data-dismiss="modal">Tutup</button>
					</div>
				</form>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
<?php endif; //management_training?>

<!-- Modal -->
<div class="modal fade" id="modalRegistration" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Registrasi</h4>
      </div>
      <div class="modal-body">
		<form>
			<div class="form-group">
				<a href="<?=site_url('login/register_institution/'.base64_encode(current_url()))?>" class="btn btn-primary btn-lg btn-block">Registrasi Lembaga Pelatihan</a>
				
			</div>
			<div class="form-group">
				<a href="<?=site_url('login/register_participant/'.base64_encode(current_url()))?>" class="btn btn-info btn-lg btn-block">Registrasi Peserta</a>
			</div>
		</form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Konfirmasi</h4>
			</div>
			<div class="modal-body">
				  Yakin akan dihapus?
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary"  onclick="location.href='#'" id='modal-delete-ya'>Ya</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>

<!-- TRAINING SUBMISSION MODAL -->
<div class="modal fade" id="training_submission" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<form method='post' action='#' class="form-horizontal form-edit">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">Konfirmasi</h4>
				</div>
				<div class="modal-body">
					<!-- Nav tabs -->
					<ul id='training_submission_tab' class="nav nav-tabs" role="tablist">
						<li role="presentation" class="active"><a href="#training_submission_progress" data-load_page='training_submission_progress' aria-controls="home" role="tab" data-toggle="tab">Progress</a></li>
						<li role="presentation"><a href="#training_submission_data" data-load_page='training_submission_data' aria-controls="profile" role="tab" data-toggle="tab">Data Pengajuan Pelatihan</a></li>
						<li role="presentation"><a href="#training_submission_location" data-load_page='training_submission_location' aria-controls="profile" role="tab" data-toggle="tab">Data Lokasi Pelatihan</a></li>
						<li role="presentation"><a href="#training_submission_participant" data-load_page='training_submission_participant' aria-controls="profile" role="tab" data-toggle="tab">Data Peserta</a></li>
						<li role="presentation"><a href="#training_submission_cp" data-load_page='training_submission_cp' aria-controls="profile" role="tab" data-toggle="tab">Data Panitia</a></li>
						
						<?php if($this->connect_auth->get_me()['entity']=='narasumber'):?>
						<li role="presentation"><a href="#training_submission_reject_instructor" data-load_page='' aria-controls="profile" role="tab" data-toggle="tab">Tolak</a></li>
						<?php endif;?>
					</ul>

					<!-- Tab panes -->
					<div class="tab-content">
						<div role="tabpanel" class="tab-pane active" id="training_submission_progress">

						</div>
						<div role="tabpanel" class="tab-pane" id="training_submission_data">
							<div class="spinner-loader">
								Loading..!
							</div>
						</div>
						<div role="tabpanel" class="tab-pane" id="training_submission_location">
							<div class="spinner-loader">
								Loading..!
							</div>
						</div>
						<div role="tabpanel" class="tab-pane" id="training_submission_participant">
							<div class="spinner-loader">
								Loading..!
							</div>
						</div>
						<div role="tabpanel" class="tab-pane" id="training_submission_cp">
							<div class="spinner-loader">
								Loading..!
							</div>
						</div>
						<div role="tabpanel" class="tab-pane" id="training_submission_reject_instructor">
						    <div class="form-group">
							<label for="inputEmail3" class="col-sm-2 control-label">Pesan</label>
							<div class="col-sm-10">
							    <textarea class="form-control" id='wysiwyg-content' rows="3" name='review_reject_note'><?=(isset($content['review_reject_note']) ? $content['review_reject_note'] : '')?></textarea>
							    
							</div>
						    </div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<?php if($this->connect_auth->get_me()['entity']=='narasumber'):?>
						<button name='submit' type="submit" id='approved_training_submit' class="btn btn-primary"  value='approved'>Disetujui</button>
						<button type="submit" class="btn btn-red" name='submit' value='reject' id='modal-reject'>Tolak</button>
					<?php endif;?>
					<button type="button" class="btn default" data-dismiss="modal">Tutup</button>

				</div>
			</form>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>

<!-- TRAINER INFO MODAL -->
<?php
/*
<div class="modal fade" id="participant_info_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<form method='post' action='#' class="form-horizontal">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">Informasi Peserta</h4>
				</div>
				<div class="modal-body">
                    <ul id="participant_info_tab" class="nav nav-tabs" data-tabs="tabs">
                        <li class="active"><a href="#participant_info_content" data-toggle="tab" data-load_page='participant_info'>Informasi Peserta</a></li>
                        <li><a href="#training_requirement_content" data-toggle="tab" data-load_page='training_requirement'>Persyaratan Peserta</a></li>
                    </ul>
                    <div id="" class="tab-content">
                        <div class="tab-pane fade in active" id="participant_info_content" >
							<!-- SPINNER -->
							 <div class="spinner-container">
								<div class="spinner-loader" style="text-align: center">
									Loading..!
								</div>
							</div> 
								
							
                        </div>
                        <div class="tab-pane fade" id="training_requirement_content">
							<!-- SPINNER -->
							 <div class="spinner-container">
								<div class="spinner-loader" style="text-align: center">
									Loading..!
								</div>
							</div> 
                            
							
							
                        </div>
                    </div>
				</div>
				<div class="modal-footer">
					<?php if($this->connect_auth->get_me()['entity']!='peserta'):?>
					<button type="submit" id='' class="btn btn-primary" name='submit' value='approved'>Disetujui</button>
					<button type="submit" class="btn btn-danger" id='' name='submit' value='reject'>Tolak</button>
					<?php endif;?>
					<button type="button" class="btn default" data-dismiss="modal">Tutup</button>

				</div>
			</form>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
*/
?>

<!--review participant-->
<div class="modal fade" id="participant_info_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-user_id='1234'>
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<form method='post' action='#' class="form-horizontal">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
					
				</div>
				<div class="modal-body slim">
				    <!-- modal-body-->
				    
				    
				<div class="portlet light">
				    <div class="portlet-title tabbable-custom ">
					<div class="caption caption-md">
					    <i class="icon-globe theme-font-color hide"></i>
					    <span class="caption-subject theme-font-color bold uppercase">Review Join Peserta</span>
					</div>
					<ul class="nav nav-tabs" id='tab_action_participant_info'>
					    <li class="active">
						<a href="#tab_content_review" data-toggle="tab" data-load_page='' >Review </a>
					    </li>
					    
					    <?php if($this->connect_auth->get_me()['entity'] == 'lpp'):?>
					    
					    <li>
						<a href="#tab_clarification" data-toggle="tab" data-load_page='review_note'>Klarifikasi </a>
					    </li>
					    <li>
						<a href="#tab_rejected" data-toggle="tab" data-load_page='review_rejected'>Tolak </a>
					    </li>
					    <?php endif;?>
					</ul>
				    </div>
				    
				    <div class="portlet-body">
					<div class="tab-content">
					    <div class="tab-pane active" id="tab_content_review">
						<!-- overview-->
						<div class="portlet light">
							
							<div class="portlet-body">
								<!--review-->
								
								<!-- Nav tabs -->
						<ul class="nav nav-tabs" role="tablist" id='participant_info_tab'>
							<li role="presentation" class="active"><a href="#participant_info_content" role="tab" data-load_page='participant_info' data-toggle="tab">Informasi Peserta</a></li>
							<li role="presentation"><a href="#training_requirement_content" role="tab" data-load_page='training_requirement' data-toggle="tab">Persyaratan Peserta</a></li>
							
							
						</ul>

						<!-- Tab panes -->
						<div class="tab-content">
							<div class="tab-pane fade in active" id="participant_info_content" >
							<!-- SPINNER -->
							 <div class="spinner-container">
								<div class="spinner-loader" style="text-align: center">
									Loading..!
								</div>
							</div> 
								
							
						      </div>
						      <div class="tab-pane fade" id="training_requirement_content">
								<!-- SPINNER -->
								<div class="spinner-container">
									<div class="spinner-loader" style="text-align: center">
										Loading..!
									</div>
								</div>    
						      </div>

							
						</div>
								
								<!--review-->
							</div>
						</div>
						<!-- ./overview-->
				
				
					    </div>
					    
					    
					    <div class="tab-pane" id="tab_clarification">
						<div class="spinner-loader" style="text-align: center">
							Loading..!
						</div>
					    </div>
					    
					    <div class="tab-pane" id="tab_rejected">
						<div class="spinner-loader" style="text-align: center">
							Loading..!
						</div>
					    </div>
					</div>  
				    </div>
				    
				</div>
				
				
				    
				    <!-- ./modal-body-->
				</div>
				<div class="modal-footer">
					<?php if(isset($this->connect_auth->get_me()['entity']) && $this->connect_auth->get_me()['entity'] == 'lpp'):?>

						<button type="submit" id='approved_institution_submit' class="btn blue" name='submit' value='approved'>Disetujui</button>
						<button type="submit" id='review_institution_submit' class="btn yellow" name='submit' value='clarification'>Klarifikasi</button>
						<button type="submit" class="btn red" id='reject_institution_submit' name='submit' value='rejected' >Tolak</button>

					<?php endif;?>
					<button type="button" class="btn default" data-dismiss="modal">Tutup</button>
				</div>
			</form>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<!--./review participant-->
<!--modal syarat pelatihan-->
<!-- Modal -->
<div class="modal fade" id="training_requirement" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<form method='post' action='#'>
		    <div class="modal-content">
			    <div class="modal-header">
				    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				    <h4 class="modal-title" id="myModalLabel">Silahkan Lengkapi Syarat Pelatihan</h4>
			    </div>
			    <div class="modal-body">
				    
			    </div>
			    <div class="modal-footer">
				    <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
				    <button type="submit" class="btn btn-primary">Daftar</button>
			    </div>
		    </div>
		</form>
	</div>
</div>
<!--./modal syarat pelatihan-->

<!--modal prevent login/register daftar pelatihan-->

<div class="modal fade" id="login_register_participant" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Kamu Belum Login, Silahkan Login terlebih dahulu</h4>
			</div>
			<div class="modal-body">
				<form>
					<div class="form-group">
						<a href="<?=site_url('login/index/'.base64_encode(current_url()))?>" class="btn btn-primary btn-lg btn-block">Login</a>
						
					</div>
					<div class="form-group">
						<a href="<?=site_url('login/register_participant/'.base64_encode(current_url()))?>" class="btn btn-info btn-lg btn-block">Registrasi Peserta</a>
					</div>
				</form>
			</div>
			
		</div>
	</div>
</div>
<!--./modal prevent login/register daftar pelatihan-->
