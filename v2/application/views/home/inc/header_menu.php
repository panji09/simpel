<?php 
$page=$this->uri->segment(1); 
$page_one=$this->uri->segment(2); 

$me = ($this->connect_auth->get_access_token() ? $this->connect_auth->get_me() : false);
?>
<!-- This one in here is responsive menu for tablet and mobiles -->
<div class="responsive-navigation visible-sm visible-xs">
	<a href="#" class="menu-toggle-btn">
		<i class="fa fa-bars"></i>
	</a>
	<div class="responsive_menu">
		<ul class="main_menu">
			<li><a href="<?=site_url('')?>">Home</a>
				<ul>
					<li><a href="<?=site_url('about')?>">Tentang Kami</a></li>
					<li><a href="<?=site_url('contact')?>">Kontak</a></li>
					<li><a href="<?=site_url('faq')?>">FAQ</a></li>
					<li><a href="<?=site_url('how_to')?>">Cara Penggunaan</a></li>
				</ul>
			</li>
			<li><a href="#">Pelatihan</a>
				<ul>
					<li><a href="<?=site_url('schedule/program')?>">Program</a></li>
					<li><a href="<?=site_url('schedule')?>">Jadwal</a></li>
					
				</ul>
			</li>
			<li><a href="<?=site_url('institution')?>">Lembaga Pelatihan</a></li>
			<li><a href="<?=site_url('instructor')?>">Pengajar</a></li>
			<li><a href="#">Publikasi</a>
			    <ul>
				<li><a href="<?=site_url('news')?>">Berita</a></li>
				<li><a href="<?=site_url('event')?>">Agenda</a></li>
				<li><a href="<?=site_url('regulatory_training')?>">Peraturan</a></li>
				<li><a href="<?=site_url('gallery')?>">Galeri</a></li>
			    </ul>
			</li>
			<?php if($_SERVER['SERVER_NAME'] != 'simpel.lkpp.go.id' ):?>
			<li><a href="<?=site_url('pengaduan')?>">Pengaduan</a></li>
			<?php endif;?>
			<?php if(!$me): ?>
			<li><a href="<?=site_url('lpp/management_training/training/add')?>">Pengajuan Pelatihan</a></li>
			<li><a href="<?=site_url('login/index/'.base64_encode(current_url()))?>">Login</a></li>
			<li>
			    <a href="#" >Registrasi</a>
			    <ul>
				<li><a href="<?=site_url('login/register_institution/'.base64_encode(current_url()))?>">Lembaga Pelatihan</a></li>
				<li><a href="<?=site_url('login/register_participant/'.base64_encode(current_url()))?>">Peserta</a></li>
				
			    </ul>
			</li>
			
			<?php else:?>
			<li><a href="<?=site_url($me['entity'])?>"><?=$me['fullname']?> - Dashboard</a></li>
			<li><a href="<?=site_url('login/logout')?>">Logout</a></li>
			
			<?php endif;?>
		</ul> <!-- /.main_menu -->
	</div> <!-- /.responsive_menu -->
</div> <!-- /responsive_navigation -->

<header class="site-header">
	<div class="container">
		<div class="row">
			<div class="col-md-6 logo-container">
				<div class="logo visible-md visible-lg">
					<a href="http://www.lkpp.go.id/" title="simpel" rel="home">
						<img src="<?=$this->config->item('home_img')?>/logo-lkpp.png" alt="LKPP">
					</a>
				</div> <!-- /.logo -->

				<div class="logo visible-sm visible-xs">
					<a href="http://www.lkpp.go.id/" title="simpel" rel="home">
						<!-- <img class="img-responsive" src="<?=$this->config->item('home_img')?>/logo-double.png" alt="LKPP" style="margin: 0 auto" > -->
						<img class='img-responsive' src="<?=$this->config->item('home_img')?>/logo-double.png" alt="LKPP" style="height: 52px">
					</a>
				</div> <!-- /.logo -->
			</div> <!-- /.header-left -->

			<div class="col-md-6 header-right">
				<div class="logo visible-md visible-lg" style="margin:0;">
					<a href="<?=site_url('')?>" title="simpel" rel="home">
						<img src="<?=$this->config->item('home_img')?>/logo.png" alt="simpel">
					</a>
				</div> <!-- /.logo -->

				<div class="search-form visible-sm visible-xs" style="margin-top: 20px">
					<form name="search_form" method="get" action="<?=site_url('search/query')?>" class="search_form">
						<input type="text" id='search' name="s" value='<?=(isset($search) ? $search : "")?>' placeholder="Search the site..." title="Search the site..." class="field_search">
					</form>
				</div>
				
				
			</div> <!-- /.header-right -->
		</div>
	</div> <!-- /.container -->

	<div class="nav-bar-main" role="navigation">
		<div class="container">
			<nav class="main-navigation clearfix visible-md visible-lg" role="navigation">
				<ul class="main-menu sf-menu">
					<li <?=($page=='' ? 'class="active"' : '')?>>
						<a href="<?=base_url();?>">Home</a>
						<ul class="sub-menu">
							<li><a href="<?=site_url('about')?>">Tentang Kami</a></li>
							<li><a href="<?=site_url('contact')?>">Kontak</a></li>
							<li><a href="<?=site_url('faq')?>">FAQ</a></li>
							<li><a href="<?=site_url('how_to')?>">Cara Penggunaan</a></li>
						</ul>
					</li>
					<li <?=($page=='schedule' ? 'class="active"' : '')?>>
						<a href="#">Pelatihan</a>
						<ul class="sub-menu">
							<li><a href="<?=site_url('schedule/program')?>">Program</a></li>
							<li><a href="<?=site_url('schedule')?>">Jadwal</a></li>
						</ul>
					</li>
					<li <?=($page=='institution' ? 'class="active"' : '')?>>
						<a href="<?=site_url('institution')?>">Lembaga Pelatihan</a>
					</li>
					<li <?=($page=='instructor' ? 'class="active"' : '')?>>
						<a href="<?=site_url('instructor')?>">Pengajar</a>
					</li>
					<li><a href="#">Publikasi</a>
						<ul class="sub-menu">
							<li><a href="<?=site_url('news')?>">Berita</a></li>
							<li><a href="<?=site_url('event')?>">Agenda</a></li>
							<li><a href="<?=site_url('regulatory_training')?>">Peraturan</a></li>
							<li><a href="<?=site_url('gallery')?>">Galeri</a></li>	
						</ul>
					</li>
					<?php if($_SERVER['SERVER_NAME'] != 'simpel.lkpp.go.id' ):?>
					<li <?=($page=='pengaduan' ? 'class="active"' : '')?>>
						<a href="<?=site_url('pengaduan')?>">Pengaduan</a>
					</li>
					<?php endif;?>
				</ul> <!-- /.main-menu -->
				<?php
				if(!$me):
					?>
					<!-- when user not logged in -->
					<ul class="right-menu pull-right">
						<li>
							<a href="<?=site_url('lpp/management_training/training/add')?>">Pengajuan Pelatihan</a>
						</li>
						<li>
							<a href="<?=site_url('login/index/'.base64_encode(current_url()))?>">Login</a>
						</li>
						<li>
							<a href="#" data-toggle="modal" data-target="#modalRegistration">Register</a>
						</li>
						
			            <!-- BEGIN TOP SEARCH -->
			            <li class="menu-search">
			              <i class="fa fa-search search-btn"></i>
			              <div class="search-box">
			                <form action="<?=site_url('search/query')?>" method='get'>
			                  <div class="input-group">
			                    <input type="text" placeholder="Search" name='s' class="form-control" value='<?=(isset($search) ? $search : "")?>'>
			                    <span class="input-group-btn">
			                      <button class="btn btn-primary" type="submit">Search</button>
			                    </span>
			                  </div>
			                </form>
			              </div> 
			            </li>
			            <!-- END TOP SEARCH -->
						
					</ul>


					<!-- When user logged in -->
				<?php else:?>

					<ul class="list-account-info pull-right">
					
						<?php 
						/*
						<li class="notification">
							<div class="notification-info item-click">
								<i class="icon glyphicon glyphicon-bell"></i>
								<span class="badge">2</span>
							</div>

							<div class="toggle-notification toggle-list">
								<div class="list-profile-title">
									<a href="#" class="pull-right">view all</a>
									<h4><strong>2 pending</strong> Notification</h4> 
								</div>
								<ul class="list-notification">
									<!-- LIST ITEM -->
									<li class="ac-new">
										<a href="#">
											<div class="list-body">
												<div class="author"> 
													<span>Kasi</span>
												</div>
												<p>telah menyetujui permohonan anda</p>
												<div class="time"> 
													<span>5 minutes ago</span> 
												</div>
											</div>
										</a>
									</li>
									<!-- END / LIST ITEM -->
									<!-- LIST ITEM -->
									<li class="ac-new">
										<a href="#">
											<div class="list-body">
												<div class="author"> 
													<span>LKPP</span>
												</div>
												<p>mengirim pesan permohonan submit ulang</p>
												<div class="time"> <span>5 minutes ago</span> </div>
											</div>
										</a>
									</li>
									<!-- END / LIST ITEM -->
									<!-- LIST ITEM -->
									<li>
										<a href="#">
											<div class="list-body">
												<div class="author"> <span>LKPP</span>
												</div>
												<p>attend Salary for newbie course</p>
												<div class="time"> <span>5 minutes ago</span> </div>
											</div>
										</a>
									</li>
									<!-- END / LIST ITEM -->
									<!-- LIST ITEM -->
									<li>
										<a href="#">
											<div class="list-body">
												<div class="author"> <span>LKPP</span>
												</div>
												<p>attend Salary for newbie course</p>
												<div class="time"> <span>5 minutes ago</span> </div>
											</div>
										</a>
									</li>
									<!-- END / LIST ITEM -->
									<!-- LIST ITEM -->
									<li>
										<a href="#">
											<div class="list-body">
												<div class="author"> <span>LKPP</span>
												</div>
												<p>attend Salary for newbie course</p>
												<div class="time"> <span>5 minutes ago</span> </div>
											</div>
										</a>
									</li>
									<!-- END / LIST ITEM -->
									<!-- LIST ITEM -->
									<li>
										<a href="#">
											<div class="list-body">
												<div class="author"> <span>LKPP</span>
												</div>
												<p>attend Salary for newbie course</p>
												<div class="time"> <span>5 minutes ago</span> </div>
											</div>
										</a>
									</li>
									<!-- END / LIST ITEM -->
								</ul>
							</div>

						</li>
						*/
						?>
						<li class="me"><a href="#"><?=$me['fullname']?></a></li>
						<li class="list-item account">
							<div class="account-info item-click">
								<?php
									$avatar = (isset($me['photo']) && $me['photo'] != '' ? $me['photo'] : $this->config->item('home_img')."/default_avatar.gif");
									
								?>
								<img src="<?=$avatar?>" alt="">
							</div>
							<div class="toggle-account toggle-list">
								<ul class="list-account">
									<li>

										<a href="<?=site_url($me['entity'])?>">
											<span class="icon glyphicon glyphicon-home" aria-hidden="true"></span>Dashboard
										</a>
									</li>

									<?php if($me['entity']=='lpp'):?>
										<li>
											<a href="<?=site_url('lpp/management_training/training/add')?>">
												<span class="icon glyphicon glyphicon-transfer" aria-hidden="true"></span>Pengajuan Pelatihan
											</a>
										</li>
									<?php endif;?>
									
									<?php
									/*
									<li>
										<a href="">
											<span class="icon glyphicon glyphicon-info-sign" aria-hidden="true"></span>Notif
										</a>
									</li>
									*/
									?>
									
									<li>

										<a target="_blank" href="<?=site_url('login/my_profile')?>">
											<span class="icon glyphicon glyphicon-user" aria-hidden="true"></span>Profil <?=strtoupper($this->connect_auth->get_me()['entity'])?>
										</a>
									</li>
									<li>
										<a href="<?=site_url('login/logout')?>">
											<span class="icon glyphicon glyphicon-arrow-right" aria-hidden="true"></span>Logout
										</a>
									</li>
								</ul>
							</div>
						</li>
						
			            <!-- BEGIN TOP SEARCH -->
			            <li class="menu-search">
							<span class="sep"></span>
			              <i class="fa fa-search search-btn"></i>
			              <div class="search-box">
			                <form action="<?=site_url('search/query')?>" method='get'>
			                  <div class="input-group">
			                    <input type="text" placeholder="Search" name='s' class="form-control" value="<?=(isset($search) ? $search : "")?>">
			                    <span class="input-group-btn">
			                      <button class="btn btn-primary" type="submit">Search</button>
			                    </span>
			                  </div>
			                </form>
			              </div> 
			            </li>
			            <!-- END TOP SEARCH -->
						
					</ul>
				<?php endif;?>

			</nav> <!-- /.main-navigation -->
		</div> <!-- /.container -->
	</div> <!-- /.nav-bar-main -->

</header> <!-- /.site-header -->
