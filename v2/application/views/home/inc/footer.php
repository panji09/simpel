<?php
$query = $this->static_pages_db->get('contact');
$content = array();
if($query){
    $content = $query[0];
}
?>
<footer class="site-footer">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-4">
                <div class="footer-widget">
                    <h4 class="footer-widget-title">Quick Link</h4>
                    <ul class="list-links">
                        <li><a href="#">Home</a></li>
						<li><a href="<?=site_url('regulatory_training')?>">Peraturan Pelatihan</a></li>
                        <li><a href="<?=site_url('schedule')?>">Jadwal Pelatihan</a></li>
                        <li><a href="<?=site_url('news')?>">Berita</a></li>
                        <li><a href="<?=site_url('gallery')?>">Galeri</a></li>
                        
                    </ul>
                </div>
            </div>
			
            <div class="col-md-4 col-sm-4">
                <div class="footer-widget">
                    <h4 class="footer-widget-title">Resource Link</h4>
                    <ul class="list-links">
                        <li><a target='_blank' href="http://www.lkpp.go.id/">LKPP</a></li>
						<li><a target='_blank' href="http://e-learning.lkpp.go.id/">E-Learning</a></li>
                        <li><a target='_blank' href="http://lrc.lkpp.go.id/">Learning Resources Center</a></li>
						<li><a target='_blank' href="http://sertifikasi.lkpp.go.id/">Sertifikasi</a></li>
						
					</ul>
                </div>
            </div>
            <div class="col-md-4 col-sm-4">
                <div class="footer-widget">
                    <h4 class="footer-widget-title">Hubungi Kami</h4>
                    <?=($content && $content['published'] ? $content['content'] : '')?>
                </div>
            </div>
        </div> <!-- /.row -->

        <div class="bottom-footer">
            <div class="row">
                <div class="col-md-12">
                    <p class="small-text"><?=$this->config->item('title_footer')?></p>
                </div> <!-- /.col-md-5 -->
            </div> <!-- /.row -->
        </div> <!-- /.bottom-footer -->

    </div> <!-- /.container -->
</footer> <!-- /.site-footer -->