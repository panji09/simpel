<?php
    $page = $this->uri->segment(2);
?>
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>

<!-- END PAGE LEVEL PLUGINS -->


<?php
   if($page == 'chart'):
?>
<script>
$(function () {

    $(document).ready(function () {

        // Build the chart
        $('#container').highcharts({
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                text: 'Lembaga Pelaksana Pelatihan (LPP)'
            },
            subtitle: {
                text: 'Total <?=$sum_lpp?>'
            },
            
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        format: '<b>{point.name}</b>: {point.y} ({point.percentage:.1f} %)',
                        style: {
                            color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                        }
                    }
                }
            },
            
            series: [{
                name: '',
                colorByPoint: true,
                data: <?=$data_chart_json?>
            }]
        });
    });
});

</script>
<?php endif;?>