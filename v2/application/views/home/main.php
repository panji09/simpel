<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> 
<![endif]-->
<!--[if IE 7]> <html class="no-js lt-ie9 lt-ie8" lang="en"> 
<![endif]-->
<!--[if IE 8]> <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->


<head>
    <title><?=(isset($title) ? $title : 'SIMPEL')?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Sistem Informasi Pelatihan"> 
    <meta name="author" content="it team nts - simpel.lkpp.go.id">
    <meta name="google-site-verification" content="sdkD4i1Q4bjpCIUIcMW89lGjIv4gXPGqbUtJji2VFK4" />

<?php
 //for socmed
 if (isset($meta['on'])):
?>
	<meta property="og:title" content="<?=$meta['title']?>">
	<meta property="og:description" content="<?=strip_tags($meta['description'])?>">
	<meta property="og:type" content="website">
	<meta property="og:image" content="<?=$meta['cover']?>"><!-- link to image for socio -->
	<meta property="og:url" content="<?=current_url()?>">
<?php
endif;
?>
	<meta charset="UTF-8">
    <?=$this->load->view('home/inc/script_header');?>
</head>
<body>
    <?php $this->load->view('home/inc/modal');?>
    <!-- <div class="alert alert-warning alert-dismissible" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
		  <span aria-hidden="true">&times;</span>
	  </button>
      <strong>Peringatan!</strong> Sistem ini dalam tahap pengembangan, kembali ke versi awal?
	  <a href='http://<?=$_SERVER['HTTP_HOST']?>'>
		  <strong>Ya.</strong>
	  </a>
    </div> -->
    <?=$this->load->view('home/inc/header_menu')?>
    
    
    <?=$this->load->view('home/content/'.$content)?>

    <!-- begin The Footer -->
    <?=$this->load->view('home/inc/footer')?>


    <?=$this->load->view('home/inc/script_footer')?>

</body>
</html> 
