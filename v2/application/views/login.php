<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.4
Version: 3.8.1
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
<meta charset="utf-8"/>
<title><?=$this->config->item('title')?> | Login</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta http-equiv="Content-type" content="text/html; charset=utf-8">
<meta content="" name="description"/>
<meta content="" name="author"/>
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
<link href="<?=$this->config->item('plugin')?>/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<link href="<?=$this->config->item('plugin')?>/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css"/>
<link href="<?=$this->config->item('plugin')?>/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="<?=$this->config->item('plugin')?>/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL STYLES -->
<link href="<?=$this->config->item('plugin')?>/select2/select2.css" rel="stylesheet" type="text/css"/>
<link href="<?=$this->config->item('login_css')?>/login-soft.css" rel="stylesheet" type="text/css"/>
<!-- END PAGE LEVEL SCRIPTS -->
<!-- BEGIN THEME STYLES -->
<link href="<?=$this->config->item('login_css')?>/components-rounded.css" id="style_components" rel="stylesheet" type="text/css"/>
<link href="<?=$this->config->item('login_css')?>/plugins.css" rel="stylesheet" type="text/css"/>
<link href="<?=$this->config->item('login_css')?>/layout.css" rel="stylesheet" type="text/css"/>
<link id="style_color" href="<?=$this->config->item('login_css')?>/themes/default.css" rel="stylesheet" type="text/css"/>
<link href="<?=$this->config->item('login_css')?>/custom.css" rel="stylesheet" type="text/css"/>
<!-- END THEME STYLES -->
<link rel="shortcut icon" href="favicon.ico"/>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="login">
<!-- BEGIN LOGO -->
<div class="logo">
	
<h3><?=$this->config->item('title_short')?></h3>
<h3><?=$this->config->item('title_long')?></h3>
</div>
<!-- END LOGO -->
<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
<div class="menu-toggler sidebar-toggler">
</div>
<!-- END SIDEBAR TOGGLER BUTTON -->
<!-- BEGIN LOGIN -->
<div class="content">
	<!-- BEGIN LOGIN FORM -->
	<form class="login-form" action="<?=site_url('login/post_login')?>" method="post">
		<h3 class="form-title">Masukan Akun Kamu</h3>
		
		<?php
		    $notif = $this->session->flashdata('notif');
		    if($notif):
		?>
		
		<div class="alert <?=($notif['status'] ? 'alert-success' : 'alert-danger')?>">
			<button class="close" data-close="alert"></button>
			<span>
			<?=$notif['msg']?> </span>
		</div>
		
		<?php endif;?>
		
		<div class="form-group">
			<!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
			<label class="control-label visible-ie8 visible-ie9">Username / Email</label>
			<div class="input-icon">
				<i class="fa fa-user"></i>
				<input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Username / Email" name="username"/>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label visible-ie8 visible-ie9">Kata Kunci</label>
			<div class="input-icon">
				<i class="fa fa-lock"></i>
				<input class="form-control placeholder-no-fix" type="password" autocomplete="off" placeholder="Kata Kunci" name="password"/>
			</div>
		</div>
		<div class="form-actions">
			<label class="checkbox">
			<input type="checkbox" name="remember" value="1"/> Ingatkan </label>
			<button type="submit" class="btn blue pull-right">
			Masuk <i class="m-icon-swapright m-icon-white"></i>
			</button>
		</div>
		<div class="login-options">
			<h4>Atau masuk melalui</h4>
			<ul class="social-icons">
				<li>
					<a class="facebook" data-original-title="facebook" href="javascript:;">
					</a>
				</li>
				<li>
					<a class="googleplus" data-original-title="Goole Plus" href="javascript:;">
					</a>
				</li>
				
			</ul>
		</div>
		<div class="forget-password">
			<h4>Lupa Kata Kunci ?</h4>
			<p>
				 silahkan klik <a href="javascript:;" id="forget-password">
				disini </a>
				untuk reset Kata Kunci kamu.
			</p>
		</div>
		
	</form>
	<!-- END LOGIN FORM -->
	<!-- BEGIN FORGOT PASSWORD FORM -->
	<form class="forget-form" action="index.html" method="post">
		<h3>Lupa Kata Kunci ?</h3>
		<p>
			 Masukan alamat Email atau Username kamu dibawah ini.
		</p>
		<div class="form-group">
			<div class="input-icon">
				<i class="fa fa-envelope"></i>
				<input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Email / Username" name="email"/>
			</div>
		</div>
		<div class="form-actions">
			<button type="button" id="back-btn" class="btn">
			<i class="m-icon-swapleft"></i> Kembali </button>
			<button type="submit" class="btn blue pull-right">
			Proses <i class="m-icon-swapright m-icon-white"></i>
			</button>
		</div>
	</form>
	<!-- END FORGOT PASSWORD FORM -->
	
</div>
<!-- END LOGIN -->
<!-- BEGIN COPYRIGHT -->
<div class="copyright">
	 <?=$this->config->item('title_footer')?>
</div>
<!-- END COPYRIGHT -->
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="<?=$this->config->item('plugin')?>/respond.min.js"></script>
<script src="<?=$this->config->item('plugin')?>/excanvas.min.js"></script> 
<![endif]-->
<script src="<?=$this->config->item('plugin')?>/jquery.min.js" type="text/javascript"></script>
<script src="<?=$this->config->item('plugin')?>/jquery-migrate.min.js" type="text/javascript"></script>
<script src="<?=$this->config->item('plugin')?>/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?=$this->config->item('plugin')?>/jquery.blockui.min.js" type="text/javascript"></script>
<script src="<?=$this->config->item('plugin')?>/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<script src="<?=$this->config->item('plugin')?>/jquery.cokie.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?=$this->config->item('plugin')?>/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
<script src="<?=$this->config->item('plugin')?>/backstretch/jquery.backstretch.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<?=$this->config->item('plugin')?>/select2/select2.min.js"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?=$this->config->item('login_js')?>/metronic.js" type="text/javascript"></script>
<script src="<?=$this->config->item('login_js')?>/layout.js" type="text/javascript"></script>
<script src="<?=$this->config->item('login_js')?>/demo.js" type="text/javascript"></script>
<script src="<?=$this->config->item('login_js')?>/login-soft.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<script>
jQuery(document).ready(function() {     
  Metronic.init(); // init metronic core components
Layout.init(); // init current layout
  Login.init();
  Demo.init();
       // init background slide images
       $.backstretch([
        "<?=$this->config->item('login_img')?>/bg/1.jpg",
        "<?=$this->config->item('login_img')?>/bg/2.jpg",
        "<?=$this->config->item('login_img')?>/bg/3.jpg",
        "<?=$this->config->item('login_img')?>/bg/4.jpg"
        ], {
          fade: 1000,
          duration: 8000
    }
    );
});
</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>