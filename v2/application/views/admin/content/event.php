	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			<!-- BEGIN PAGE HEAD -->
			<div class="page-head">
				<!-- BEGIN PAGE TITLE -->
				<div class="page-title">
					<h1><?=$title_page?></h1>
				</div>
				<!-- END PAGE TITLE -->
			</div>
			<!-- END PAGE HEAD -->
			<!-- BEGIN PAGE BREADCRUMB -->
			<ul class="page-breadcrumb breadcrumb">
				<li>
					<a href="#">Dashboard</a>
					<i class="fa fa-circle"></i>
				</li>
				<li>
					<a href="#">Halaman</a>
					<i class="fa fa-circle"></i>
				</li>
				
				<li>
					<a href="#"><?=$title_page?></a>
				</li>
			</ul>
			<!-- END PAGE BREADCRUMB -->
			<!-- END PAGE HEADER-->
			
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<?php callback_submit();?>
					<!-- BEGIN MAIN CONTENT -->
					<div class="portlet light">
						<div class="portlet-title">
							<div class="caption font-blue-sharp">
								<i class="icon-note font-blue-sharp"></i>
								<span class="caption-subject">Agenda</span>
								<span class="caption-helper">manajemen agenda</span>
							</div>
							
							<div class="actions">
								<a href="<?=site_url('superadmin/pages/event/add')?>" class="btn btn-circle btn-default">
								<i class="fa fa-plus"></i> Add </a>
								<a href="javascript:;" class="btn btn-circle btn-default btn-icon-only fullscreen"></a>
							</div>
							
						</div>
						<div class="portlet-body">
							<table class="table table-striped table-bordered table-hover" id="table_news">
								<thead>
									<tr>
										<th>Tanggal</th>
										<th>Judul</th>
										<th>Penulis</th>
										<th>Terbitkan</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
								<?php							    
								    $query = $this->dynamic_pages_db->get_all(array('page_id'=>'event'));
								    if($query):
									foreach($query as $row):
								?>
									<tr class="odd gradeX">
									    <td><time  datetime="<?=date("d/m/Y H:i:s",$row['created'])?>"><?=date("d/m/Y H:i:s",$row['created']) ?></time></td>
									    <td><?=$row['title']?></td>
									    <td><?=$row['author']['fullname']?></td>
									    <td>
											<span class="label label-sm <?=($row['published'] ? 'label-success' : 'label-danger')?>"> 
												<?=($row['published'] ? 'Ya' : 'Tidak')?> 
											</span>
									    </td>
									    <td>
										    <button type="button" class="btn btn-sm yellow" onclick="location.href='<?=site_url('superadmin/pages/event/edit/'.$row['_id']->{'$id'})?>'">Edit</button>
										    <button type="button" class="btn btn-sm red delete-user" data-url-delete="<?=site_url('superadmin/pages/event/delete/'.$row['_id']->{'$id'})?>" data-toggle="modal" data-target="#delete">Delete</button>
									    </td>
									</tr>
								
								    <?php endforeach;?>
								<?php endif;?>
							    
							
								</tbody>
							</table>
							
						</div>
					</div>
					<!-- END MAIN CONTENT -->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>