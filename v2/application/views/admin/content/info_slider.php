<?php
    $page = $this->uri->segment(3);
    
    $query = $user = array();
    if($page){
	$query = $this->static_pages_db->get($page_id);
	if($query)
	    $content = $query[0];
    }
?>

<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<div class="page-content">
		<!-- BEGIN PAGE HEADER-->
		<!-- BEGIN PAGE HEAD -->
		<div class="page-head">
			<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
				<h1>Slider <small>tambah & edit slider</small></h1>
			</div>
			<!-- END PAGE TITLE -->
		</div>
		<!-- END PAGE HEAD -->
		<!-- BEGIN PAGE BREADCRUMB -->
		<ul class="page-breadcrumb breadcrumb">
			<li>
				<a href="index.html">Dashboard</a>
				<i class="fa fa-circle"></i>
			</li>
			<li>
				<a href="#">Halaman</a>
				<i class="fa fa-circle"></i>
			</li>
			<li>
				<a href="<?=site_url('superadmin/pages/'.$page)?>"><?=$title_page?></a>
			</li>
		</ul>
		<!-- END PAGE BREADCRUMB -->
		<!-- END PAGE HEADER-->
		
		<!-- BEGIN PAGE CONTENT-->
		<div class="row">
			<div class="col-md-12">
				<form class="form-horizontal form-row-seperated" action="<?=site_url('superadmin/pages/single_content_post/'.$page_id)?>" method='post'>
					<div class="portlet light">
						<div class="portlet-title">
							<div class="caption">
								<i class="icon-picture font-blue-sharp"></i>
								<span class="caption-subject font-blue-sharp bold uppercase">
								Slider </span>
								<span class="caption-helper">manajemen slider</span>
							</div>
							<div class="actions btn-set">
								<button class="btn green-haze btn-circle"><i class="fa fa-check"></i> Save</button>
							</div>
						</div>
						<div class="portlet-body">
							<?php callback_submit();?>
							
							<table class="table table-bordered table-hover">
								<thead>
									<tr role="row" class="heading">
										<th width="15%">
											 Cover
										</th>
										<th width="45%">
											 Judul
										</th>
										<th width="35%">
											 Link
										</th>
										<th width="10%">
										</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>
											<a href="#">
												  <img class='change_cover img-responsive' src="<?=(isset($content['cover']) && isset($content['cover'][0]) && $content['cover'][0]!='' ? $content['cover'][0] : 'http://www.placehold.it/1128x437/EFEFEF/AAAAAA&text=1128 x 437' )?>" alt="...">
												  <input type='hidden' name='cover[0]'  value='<?=(isset($content['cover']) && isset($content['cover'][0]) ? $content['cover'][0] : '' )?>'>
											</a>											
										</td>
										<td>
											<input type="text" name="title[0]" value='<?=(isset($content['title']) && isset($content['title'][0]) ? $content['title'][0] : '' )?>' data-required="1" class="form-control input-sm" placeholder="masukkan judul"/>
										</td>
										<td>
											<input type="text" name="url[0]" value='<?=(isset($content['url']) && isset($content['url'][0]) ? $content['url'][0] : '' )?>' data-required="1" class="form-control input-sm" placeholder="masukkan link">
										</td>
										<td class='view_info'>
											<a href="<?=(isset($content['cover']) && isset($content['cover'][0]) && $content['cover'][0]!='' ? $content['cover'][0] : 'http://www.placehold.it/1128x437/EFEFEF/AAAAAA&text=1128 x 437' )?>" class="btn btn-sm default fancybox-button " data-rel="fancybox-button"><i class="fa fa-search"></i> View</a>
										</td>
									</tr>
									
									<tr>
										<td>											
											<a href="#">
												  <img class='change_cover img-responsive' src="<?=(isset($content['cover']) && isset($content['cover'][1]) && $content['cover'][1]!='' ? $content['cover'][1] : 'http://www.placehold.it/1128x437/EFEFEF/AAAAAA&text=1128 x 437' )?>" alt="...">
												  <input type='hidden' name='cover[1]'  value='<?=(isset($content['cover']) && isset($content['cover'][1]) ? $content['cover'][1] : '' )?>'>
											</a>
										</td>
										<td>											
											<input type="text" name="title[1]" value='<?=(isset($content['title']) && isset($content['title'][1]) ? $content['title'][1] : '' )?>' data-required="1" class="form-control input-sm" placeholder="masukkan judul"/>
											
										</td>
										<td>
											<input type="text" name="url[1]" value='<?=(isset($content['url']) && isset($content['url'][1]) ? $content['url'][1] : '' )?>' data-required="1" class="form-control input-sm" placeholder="masukkan link">				
										</td>
										<td class='view_info'>
											<a href="<?=(isset($content['cover']) && isset($content['cover'][1]) && $content['cover'][1]!='' ? $content['cover'][1] : 'http://www.placehold.it/1128x437/EFEFEF/AAAAAA&text=1128 x 437' )?>" class="btn btn-sm default fancybox-button" data-rel="fancybox-button"><i class="fa fa-search"></i> View</a>
										</td>
									</tr>
									
									<tr>
										<td>
											<a href="#">
												  <img class='change_cover img-responsive' src="<?=(isset($content['cover']) && isset($content['cover'][2]) && $content['cover'][2]!='' ? $content['cover'][2] : 'http://www.placehold.it/1128x437/EFEFEF/AAAAAA&text=1128 x 437' )?>" alt="...">
												  <input type='hidden' name='cover[2]'  value='<?=(isset($content['cover']) && isset($content['cover'][2]) ? $content['cover'][2] : '' )?>'>
											</a>
										</td>
										<td>
											<input type="text" name="title[2]" value='<?=(isset($content['title']) && isset($content['title'][2]) ? $content['title'][2] : '' )?>' data-required="1" class="form-control input-sm" placeholder="masukkan judul"/>				
										</td>
										<td>
											<input type="text" name="url[2]" value='<?=(isset($content['url']) && isset($content['url'][2]) ? $content['url'][2] : '' )?>' data-required="1" class="form-control input-sm" placeholder="masukkan link">				
										</td>
										<td class='view_info'>
											<a href="<?=(isset($content['cover']) && isset($content['cover'][2]) && $content['cover'][2]!='' ? $content['cover'][2] : 'http://www.placehold.it/1128x437/EFEFEF/AAAAAA&text=1128 x 437' )?>" class="btn btn-sm default fancybox-button" data-rel="fancybox-button"><i class="fa fa-search"></i> View</a>
										</td>
									</tr>
									
									<tr>
										<td>
											<a href="#">
												  <img class='change_cover img-responsive' src="<?=(isset($content['cover']) && isset($content['cover'][3]) && $content['cover'][3]!='' ? $content['cover'][3] : 'http://www.placehold.it/1128x437/EFEFEF/AAAAAA&text=1128 x 437' )?>" alt="...">
												  <input type='hidden' name='cover[3]'  value='<?=(isset($content['cover']) && isset($content['cover'][3]) ? $content['cover'][3] : '' )?>'>
											</a>
										</td>
										<td>
											<input type="text" name="title[3]" value='<?=(isset($content['title']) && isset($content['title'][3]) ? $content['title'][3] : '' )?>' data-required="1" class="form-control input-sm" placeholder="masukkan judul"/>
										</td>
										<td>
											<input type="text" name="url[3]" value='<?=(isset($content['url']) && isset($content['url'][3]) ? $content['url'][3] : '' )?>' data-required="1" class="form-control input-sm" placeholder="masukkan link">
										</td>
										<td class='view_info'>
											<a href="<?=(isset($content['cover']) && isset($content['cover'][3]) && $content['cover'][3]!='' ? $content['cover'][3] : 'http://www.placehold.it/1128x437/EFEFEF/AAAAAA&text=1128 x 437' )?>" class="btn btn-sm default fancybox-button" data-rel="fancybox-button"><i class="fa fa-search"></i> View</a>
										</td>
									</tr>
									
									<tr>
										<td>
											<a href="#">
												  <img class='change_cover img-responsive' src="<?=(isset($content['cover']) && isset($content['cover'][4]) && $content['cover'][4]!='' ? $content['cover'][4] : 'http://www.placehold.it/1128x437/EFEFEF/AAAAAA&text=1128 x 437' )?>" alt="...">
												  <input type='hidden' name='cover[4]'  value='<?=(isset($content['cover']) && isset($content['cover'][4]) ? $content['cover'][4] : '' )?>'>
											</a>
										</td>
										<td>
											<input type="text" name="title[4]" value='<?=(isset($content['title']) && isset($content['title'][4]) ? $content['title'][4] : '' )?>' data-required="1" class="form-control input-sm" placeholder="masukkan judul"/>
										</td>
										<td>
											<input type="text" name="url[4]" value='<?=(isset($content['url']) && isset($content['url'][4]) ? $content['url'][4] : '' )?>' data-required="1" class="form-control input-sm" placeholder="masukkan link">
										</td>
										<td class='view_info'>
											<a href="<?=(isset($content['cover']) && isset($content['cover'][4]) && $content['cover'][4]!='' ? $content['cover'][3] : 'http://www.placehold.it/1128x437/EFEFEF/AAAAAA&text=1128 x 437' )?>" class="btn btn-sm default fancybox-button" data-rel="fancybox-button"><i class="fa fa-search"></i> View</a>
										</td>
									</tr>
								</tbody>
							</table>
							<p class="text-warning">Image size 1128x437</p>
						</div>
					</div>
				</form>
			</div>
		</div>
		<!-- END PAGE CONTENT-->
	</div>
</div>
<!-- END CONTENT -->