<?php
    $page = $this->uri->segment(3);
    $token = $this->connect_auth->get_access_token();
?>
<div class="page-content-wrapper">
	<div class="page-content">
		<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
		<!-- /.modal -->
		<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
		<!-- BEGIN PAGE HEADER-->
		<!-- BEGIN PAGE HEAD -->
		<div class="page-head">
			<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
				<h1><?=$title_page?></h1>
			</div>
			<!-- END PAGE TITLE -->
		</div>
		<!-- END PAGE HEAD -->
		<!-- BEGIN PAGE BREADCRUMB -->
		<ul class="page-breadcrumb breadcrumb">
			<li>
				<a href="#">Approval</a>
				<i class="fa fa-circle"></i>
			</li>
			
			<li>
				<a href="#"><?=$title_page?></a>
			</li>
		</ul>
		<!-- END PAGE BREADCRUMB -->
		<!-- END PAGE HEADER-->
		<!-- BEGIN PAGE CONTENT-->
		<div class="row">
			<div class="col-md-12">
				<?php callback_submit();?>
				
				<!-- BEGIN EXAMPLE TABLE PORTLET-->
				<div class="portlet box grey-cascade">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-globe"></i><?=$title_page?>
						</div>
						<div class="tools">
							<a href="javascript:;" class="collapse">
							</a>
						</div>
					</div>
					<div class="portlet-body">
							<div class="table-toolbar">
								<div class="row">
									<div class="col-md-6">
										<div class="btn-group">
											<a href='<?=site_url('admin/evaluation/form/add')?>' id="sample_editable_1_new" class="btn green">
											Tambah <i class="fa fa-plus"></i>
											</a>
										</div>
									</div>
									<!--<div class="col-md-6">
										<div class="btn-group pull-right">
											<button class="btn dropdown-toggle" data-toggle="dropdown">Tools <i class="fa fa-angle-down"></i>
											</button>
											<ul class="dropdown-menu pull-right">
												<li>
													<a href="javascript:;">
													Print </a>
												</li>
												<li>
													<a href="javascript:;">
													Save as PDF </a>
												</li>
												<li>
													<a href="javascript:;">
													Export to Excel </a>
												</li>
											</ul>
										</div>
									</div>-->
								</div>
							</div>						
						
						<table class="table table-striped table-bordered table-hover" id="table_form_evaluation">
							<thead>
								<tr>
									<th>
										 Nama Form
									</th>
									<th>
										 Tipe Evaluasi
									</th>
									
									<th>
										 Last Update
									</th>
									<th>
										 Status
									</th>
									<th>
										 Action
									</th>
								</tr>
							</thead>
						<tbody>
						<?php
						    $query = $this->evaluation_db->get_all();
						    if($query):
							foreach($query as $row):
						?>
						<tr class="odd gradeX">
						    <td><?=$row['name']?></td>
						    <td><?=$row['type_evaluation']['name']?></td>
						    <td><time class="timeago" datetime="<?=date('c',$row['last_update']['time'])?>"><?=date("F j, Y, g:i a",$row['last_update']['time']) ?></time></td>
						    
									    <td>
											<span class="label label-sm <?=($row['published'] ? 'label-success' : 'label-danger')?>"> 
												<?=($row['published'] ? 'Ya' : 'Tidak')?> 
											</span>
									    </td>
									    <td>
										    <button type="button" class="btn btn-sm yellow" onclick="location.href='<?=site_url('admin/evaluation/form/edit/'.$row['_id']->{'$id'})?>'">Edit</button>
										    <button type="button" class="btn btn-sm red delete-user" data-url-delete="<?=site_url('admin/evaluation/form/delete/'.$row['_id']->{'$id'})?>" data-toggle="modal" data-target="#delete">Delete</button>
									    </td>
						</tr>
						<?php endforeach; endif;?>
						
						</tbody>
						</table>
					</div>
				</div>
				<!-- END EXAMPLE TABLE PORTLET-->
			</div>
		</div>
		<!-- END PAGE CONTENT-->
	</div>
</div>