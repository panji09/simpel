<?php
    $page = $this->uri->segment(3);
    $state = $this->uri->segment(4);
	$participant = 18;
	$query = $this->evaluation_db->get($form_id);
	if($query)
	$form = $query[0];
?>
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- /.modal -->
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE HEADER-->
			<!-- BEGIN PAGE HEAD -->
			<div class="page-head">
				<!-- BEGIN PAGE TITLE -->
				<div class="page-title">
					<h1><?=$title_content?> <small><?=$title_page?></small></h1>
				</div>
				<!-- END PAGE TITLE -->
				<!-- BEGIN PAGE TOOLBAR -->
				
				<!-- END PAGE TOOLBAR -->
			</div>
			<!-- END PAGE HEAD -->
			<!-- BEGIN PAGE BREADCRUMB -->
			<ul class="page-breadcrumb breadcrumb">
				<li>
					<a href="#">Halaman</a>
					<i class="fa fa-circle"></i>
				</li>
				<li>
					<a href="<?=site_url('superadmin/pages/'.$page)?>"><?=$title_page?></a>
					<i class="fa fa-circle"></i>
				</li>
				<li>
					<a href="#"><?=$title_content?></a>
				</li>
			</ul>
			<!-- END PAGE BREADCRUMB -->
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<?php callback_submit();?>
					
					<div class="portlet box blue" id="form_wizard_1">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-gift"></i> <?=$title_content?>
							</div>
							<div class="tools hidden-xs">
								<a href="javascript:;" class="collapse">
								</a>
								
							</div>
						</div>
						<div class="portlet-body form">
							<form action="" id="form_sample_3" method='post' class="form-horizontal">
								<div class="form-body">
									<h3 class="form-section">Lengkapi kolom dibawah ini</h3>
									<div class="alert alert-danger display-hide">
										<button class="close" data-close="alert"></button>
										You have some form errors. Please check below.
									</div>
									<div class="alert alert-success display-hide">
										<button class="close" data-close="alert"></button>
										Your form validation is successful!
									</div>
									
									<div class="form-group">
										<label class="control-label col-md-3">Kategori <span class="required">
										* </span>
										</label>
										<div class="col-md-4">
											<input value='<?=(isset($content['category']) ? $content['category'] : '' )?>' type="text" name="category" data-required="1" class="form-control"/>
										</div>
										
									</div>
									
									<div class="form-group">
										<label class="control-label col-md-3">Judul <span class="required">
										* </span>
										</label>
										<div class="col-md-4">
											<input value='<?=(isset($content['title']) ? $content['title'] : '' )?>' type="text" name="title" data-required="1" class="form-control"/>
										</div>
										
									</div>
									
									<div class="form-group">
										<label class="control-label col-md-3">Dokumen <span class="required">
										* </span>
										</label>
										<div class="col-md-4">
											<input value='<?=(isset($content['file']) ? $content['file'] : '' )?>' type="text" name="file" data-required="1" class="form-control change_file"/>
										</div>
										
									</div>
									
									<!-- begin evaluation form here-->
						<table class="table table-striped table-bordered table-hover" id="table_approval_institution">
							<thead>
								<tr>
									<th rowspan="2">
										 No
									</th>
									<th colspan="2" rowspan="2">
										 Unsur Penilaian
									</th>

									<th colspan="3">
										 Responden
									</th>
								</tr>
								<tr>
									<?php
									for($i=0; $i<$participant; $i++){								
									?>
									<th>
										 <?=$i+1;?>
									</th>
									<?php }?>
								</tr>
							</thead>
						<tbody>
						
						<!-- begin table here
						this for lpp
						-->
						<?php
						$sec = 'A';
						$lpp_questions = $form['lpp'];
						foreach($lpp_questions as $category){ ?>
							<tr><td><?=$sec?></td>  <td colspan="2"><?=$category['name']?></td> 
							<?php
							for($i=0; $i<$participant; $i++){								
							?>
							<td>
								 <?//=$i+1;?>
							</td>
							<?php }?>
							</tr>
							<!-- row 2-->
							<?php foreach($category['lpp'] as $question){?>
							<tr> 
								<td> </td> <td> <?=$question['no']?></td> <td> <?=$question['question']?></td>
								<!-- set input here-->
								<?php
								for($i=0; $i<$participant; $i++){								
								?>
								<td>
									 <?//=$i+1;?>
									 <!-- <input type="text" value='' name="row" data-required="1" style="width:30px;height:30px"> -->
								</td>
								<?php }?>
							</tr>
							<?php }?>
																					
						<?php
						$sec++;
							}
						?>
						
						<tr class="odd gradeX">
						</tr>
						
						</tbody>
						</table>
									
								</div>
								<div class="form-actions">
									<div class="row">
										<div class="col-md-offset-3 col-md-9">
											<button type="submit" class="btn green">Simpan</button>
											<a href='<?=site_url($this->session->userdata('redirect'))?>' class="btn default">Batal</a>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>