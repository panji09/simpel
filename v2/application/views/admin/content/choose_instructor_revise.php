<?php
   $token = $this->connect_auth->get_access_token();
   $query = $this->training_db->get($training_id);
   if($query){
   	$training = $query[0];
   	$user = $training['creator'];
   }
   
   $on_progress = ($this->uri->segment(6) ? true : false);
   ?>
<style>
   .top-buffer { margin-top:10px; }
   .table-toolbar .form-group { width: 400px }
</style>
<div class="page-content-wrapper">
   <div class="page-content">
      <!-- BEGIN PAGE HEADER-->
      <!-- BEGIN PAGE HEAD -->
      <div class="page-head">
         <!-- BEGIN PAGE TITLE -->
         <div class="page-title">
            <h1>Manajemen Pengajar Pelatihan</h1>
            <small style="color: #9eacb4;">Pelatihan Keahlian PBJP</small>
         </div>
         <!-- END PAGE TITLE -->
      </div>
      <!-- END PAGE HEAD -->
      <!-- BEGIN PAGE BREADCRUMB -->
      <ul class="page-breadcrumb breadcrumb">
         <li>
            <a href="#">Pelatihan</a>
            <i class="fa fa-circle"></i>
         </li>
         <li>
            <a href="#"><?=$title_page?></a>
            <i class="fa fa-circle"></i>
         </li>
         <li>
            <a href="#">Manajemen Pengajar Pelatihan</a>
         </li>
      </ul>
      <!-- END PAGE BREADCRUMB -->
      <!-- END PAGE HEADER-->
      <!-- BEGIN PAGE CONTENT-->		
      <div class="row">
         <div class="col-md-12">
            <?php callback_submit();?>
            <!-- BEGIN Tab Info -->
            <div class="tabbable tabbable-custom tabbable-noborder">
               <ul class="nav nav-tabs ">
                  <li class="active">
                     <a href="#tab_15_1" data-toggle="tab" class="bold uppercase">
                     Overview
                     </a>
                  </li>
                  <li>
                     <a href="<?=($on_progress ? '#progress_instructor' : '#tab_15_3')?>" data-toggle="tab" class="bold uppercase">
                     Pengajar 
                     </a>
                  </li>
                  
               </ul>
               <div class="tab-content">
                  <div class="tab-pane active" id="tab_15_1">
                     <!-- BEGIN Portlet Lembaga Pelatihan -->
                     <div class="portlet">
                        <div class="portlet-body portlet-empty" style="padding: 0 10px">
							<div class="form">
	 							<!-- BEGIN PROGRESS -->
	                             <div class="margin-top-20">
	                                <h3>Progress</h3>
	                             </div>
				    <div style="padding: 20px">
					    <?php $this->load->view('admin/inc/widget/progress')?>
				    </div>
	 							<!-- END PROGRESS -->
							
	 							<!-- BEGIN INFO PENGAJUAN PELATIHAN -->
	                             <div class="margin-top-20">
	                                <h3>Info Pengajuan Pelatihan</h3>
	                             </div>
	                             <div class="form-horizontal">
	                                <div class="form-body">
	                                   <div class="form-group form-md-line-input">
	                                      <label class="col-md-2 control-label" for="form_control_1">Nama Pelatihan</label>
	                                      <div class="col-md-10">
	                                         <label class="form-control">
	                                         <?=(isset($training['training_name']['name']) ? $training['training_name']['name'] : '' )?>
	                                         </label>
	                                      </div>
	                                   </div>
	                                   
	                                   <div class="form-group form-md-line-input">
	                                      <label class="col-md-2 control-label" for="form_control_1">Jenis Pelatihan</label>
	                                      <div class="col-md-10">
	                                         <label class="form-control">
	                                         <?=(isset($training['type_training']['name']) ? $training['type_training']['name'] : '' )?>
	                                         </label>
	                                      </div>
	                                   </div>
	                                   
	                                   <div class="form-group form-md-line-input">
	                                      <label class="col-md-2 control-label" for="form_control_1">Modul Bahan Ajar</label>
	                                      <div class="col-md-10">
	                                         <label class="form-control">
	                                         <?php
						    $query_program_training = $this->config_training_db->get($training['training_name']['id']);
						    
						    if($query_program_training):
							$program_training = $query_program_training[0];
	                                         ?>
	                                         
	                                         <?=(isset($program_training['teaching_material_upload']) ? anchor($program_training['teaching_material_upload'], basename(urldecode($program_training['teaching_material_upload'])), array('target' => '_blank')) : '' )?>
	                                         
	                                         <?php endif;?>
	                                         </label>
	                                      </div>
	                                   </div>
	                                   
	                                   <div class="form-group form-md-line-input">
	                                      <label class="col-md-2 control-label" for="form_control_1">Tanggal Pelatihan</label>
	                                      <div class="col-md-10">
	                                         <label class="form-control">
	                                         <?=(isset($training['training_date']) ? $training['training_date'] : '' )?>
	                                         </label>
	                                      </div>
	                                   </div>
	                                   <div class="form-group form-md-line-input">
	                                      <label class="col-md-2 control-label" for="form_control_1">&nbsp;</label>
	                                      <div class="col-md-10">
	                                         <button 
	                                            type="button" 
	                                            class="btn btn-circle btn-default  review_training_submission"
	                                            data-url-submit="<?=site_url('lpp/management_training/training_post/'.$training['_id']->{'$id'})?>" 
	                                            data-toggle="modal" 
	                                            data-training_id="<?=$training['_id']->{'$id'}?>" 
	                                            data-target="#review_training_submission">
	                                         Detail Pengajuan Pelatihan 
	                                         </button>
	                                      </div>
	                                   </div>
	                                </div>
	                             </div>
	 							<!-- END INFO PENGAJUAN PELATIHAN -->
							
	 							<!-- BEGIN  INFO LEMBAGA PELATIHAN -->
	                            <div class="margin-top-20">
	                               <h3>Info Lembaga pelatihan</h3>
	                            </div>
	                            <div class="form-horizontal">
	                               <div class="form-body">
	                                  <div class="form-group form-md-line-input">
	                                     <label class="col-md-2 control-label" for="form_control_1">Nama Lembaga</label>
	                                     <div class="col-md-10">
	                                        <label class="form-control">
	                                        <?=(isset($user['training_institute']) && $user['training_institute'] ? $user['training_institute'] : '')?>
	                                        </label>
	                                     </div>
	                                  </div>
	                                  <div class="form-group form-md-line-input">
	                                     <label class="col-md-2 control-label" for="form_control_1">Status Lembaga Pelatihan:</label>
	                                     <div class="col-md-10">
	                                        <label class="form-control">
	                                        <?=(isset($user['training_institute_status']) && $user['training_institute_status'] ? $user['training_institute_status'] : '')?>
	                                        </label>
	                                     </div>
	                                  </div>
	                                  <div class="form-group form-md-line-input">
	                                     <label class="col-md-2 control-label" for="form_control_1">Akreditasi</label>
	                                     <div class="col-md-10">
	                                        <label class="form-control">
	                                        <?=(isset($user['accreditation_level']) && $user['accreditation_level'] ? $user['accreditation_level'] : '')?>
	                                        </label>
	                                     </div>
	                                  </div>
	                                  <div class="form-group form-md-line-input">
	                                     <label class="col-md-2 control-label" for="form_control_1">Alamat Kantor Pelatihan:</label>
	                                     <div class="col-md-10">
	                                        <label class="form-control">
	                                        <?=(isset($user['office_address']) && $user['office_address'] ? $user['office_address'] : '')?>
	                                        </label>
	                                     </div>
	                                  </div>
	                                  <div class="form-group form-md-line-input">
	                                     <label class="col-md-2 control-label" for="form_control_1">&nbsp;</label>
	                                     <div class="col-md-10">
	                                       <button 
	                                          type="button" 
	                                          class="btn btn-circle btn-default review-institution" 
	                                          data-toggle="modal" 
	                                          data-user_id="<?=(isset($training['creator']['user_id']) ? $training['creator']['user_id'] : '' )?>" 
	                                          data-target="#review_institution"
	                                          >Detail Lembaga Pelatihan</button>
	                                     </div>
	                                  </div>
	                               </div>
	                            </div>
	 						   <!-- END INFO LEMBAGA PELATIHAN -->
							</div>
                        </div>
                        <!-- END Portlet Lembaga Pelatihan -->
                     </div>
                  </div>
                  <!-- pengajar-->
                  <div class="tab-pane" id="tab_15_3">
                     <div class="portlet light">
                        <div class="portlet-title">
                           <div class="caption font-green-sharp">
                              <span class="caption-subject bold uppercase"> Set Pengajar Pelatihan</span>						
                           </div>
                           <div class="actions">
                              <a href="javascript:;" class="btn btn-circle red add_instructor">
                              <i class="fa fa-plus"></i> Tambah Pengajar </a>
                              <a href="javascript:;" class="btn btn-circle btn-default btn-icon-only fullscreen"></a>
                           </div>
                        </div>
                        <div class="portlet-body form portlet-empty">
                           <form method='post' action='<?=site_url('fasilitator/management_training/hour_leasson_post/'.$training_id)?>'>
                              <div class="table-scrollable form-horizontal margin-bottom-20" id='table_content'>
                                 <?php $this->load->view('admin/inc/table_set_instructor')?>
                              </div>
                              <div class="form-actions">
                                 <div class="row">
                                    <div class="col-md-offset-2 col-md-10">
                                       <button type="submit" class="btn blue" name='submit' value='save'>Proses</button>
                                       <button type="submit" class="btn red" name='submit' value='draft'>Draft</button>
                                       <button type="button" class="btn default">Cancel</button>
                                    </div>
                                 </div>
                              </div>
                           </form>
                           <div id='template_table_set_instructor' style='display:none'>
                              <?php $this->load->view('admin/inc/table_set_instructor')?>
                           </div>
                        </div>
                     </div>
                  </div>
                  <!-- ./pengajar-->
                  <!-- peserta-->
                  <div class="tab-pane" id="tab_15_4">
                     peserta
                  </div>
                  <!-- ./peserta-->
                  <!-- evaluasi-->
                  <div class="tab-pane" id="tab_15_5">
                     evaluasi
                  </div>
                  <!-- ./evaluasi-->
                  <!-- evaluasi-->
                  <div class="tab-pane" id="progress_instructor">
                     <table class="table table-striped table-hover" id='table_set_instructor'>
                        <thead>
                           <tr>
                              <th>
                                 Nama Pengajar
                              </th>
                              <th>
                                 Tanggal 
                              </th>
                              <th>
                                 Jam Pelajaran
                              </th>
                              <th>
                                 Status Pengajar
                              </th>
                              <th>
                                 Status
                              </th>
                           </tr>
                        </thead>
                        <tbody>
                           <?php
                              $query = $this->hour_leasson_db->get_all(array(
                              	'training_id' => $training_id
                              ));
                              
                              if($query)
                              foreach($query as $row):
                              	?>
                           <tr>
                              <td style="width: 250px;"></td>
                              <td></td>
                              <td></td>
                              <td></td>
                              <td></td>
                           </tr>
                           <?php endforeach;?>
                        </tbody>
                     </table>
                  </div>
                  <!-- ./evaluasi-->
               </div>
            </div>
            <!-- END Tab Info -->
         </div>
      </div>
   </div>
   <!-- END PAGE CONTENT-->
</div>
<!--page-content-wrapper-->