	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			<!-- BEGIN PAGE HEAD -->
			<div class="page-head">
				<!-- BEGIN PAGE TITLE -->
				<div class="page-title">
					<h1><?=$title_page?></h1>
				</div>
				<!-- END PAGE TITLE -->
			</div>
			<!-- END PAGE HEAD -->
			<!-- BEGIN PAGE BREADCRUMB -->
			<ul class="page-breadcrumb breadcrumb">
				<li>
					<a href="#">Dashboard</a>
					<i class="fa fa-circle"></i>
				</li>
				<li>
					<a href="#">Pengaturan</a>
					<i class="fa fa-circle"></i>
				</li>
				
				<li>
					<a href="#"><?=$title_page?></a>
					<i class="fa fa-circle"></i>
				</li>
			</ul>
			<!-- END PAGE BREADCRUMB -->
			<!-- END PAGE HEADER-->
			
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<?php callback_submit();?>
					<div class="portlet light">
						<div class="portlet-title">
							<div class="caption font-blue-sharp">
								<i class="icon-globe font-blue-sharp"></i>
								<span class="caption-subject">Pengaturan</span>
								<span class="caption-helper"><?=$title_page?></span>
							</div>
							
							<div class="actions">
								<a href="<?=site_url('superadmin/setting/type_training/add')?>" class="btn btn-circle btn-default">
								<i class="fa fa-plus"></i> Add </a>
								<a href="javascript:;" class="btn btn-circle btn-default btn-icon-only fullscreen"></a>
							</div>
							
						</div>
						<div class="portlet-body">
							<table class="table table-striped table-bordered table-hover" id="table_news">
							<thead>
							<tr>
								<th>Tanggal</th>
								<th>Jenis Pelatihan</th>
								<th>Penulis</th>
								<th>Terbitkan</th>
								<th>Action</th>
							</tr>
							</thead>
							<tbody>
							<?php
							    
							    $query = $this->type_training_db->get_all();
							    
							    if($query):
								foreach($query as $row):
								    
							?>
							<tr class="odd gradeX">
							    <td><time class="timeago" datetime="<?=date('c',$row['created'])?>"><?=date("F j, Y, g:i a",$row['created']) ?></time></td>
							    <td><?=$row['name']?></td>
							    <td><?=$row['author']['fullname']?></td>
							    <td>
								<span class="label label-sm <?=($row['published'] ? 'label-success' : 'label-danger')?>"> <?=($row['published'] ? 'Ya' : 'Tidak')?> </span>
							    </td>
							    <td>
								<div class="btn-group">
								    <button type="button" class="btn btn-default" onclick="location.href='<?=site_url('superadmin/setting/type_training/edit/'.$row['_id']->{'$id'})?>'">Edit</button>
								    <button type="button" class="btn btn-default delete-user" data-url-delete="<?=site_url('superadmin/setting/type_training/delete/'.$row['_id']->{'$id'})?>" data-toggle="modal" data-target="#delete">Delete</button>
								</div>
							    </td>
							</tr>
								
							    <?php endforeach;?>
							<?php endif;?>
							    
							
							</tbody>
							</table>
							
						</div>
					</div>
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>