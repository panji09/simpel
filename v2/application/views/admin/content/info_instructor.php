<div class="page-content-wrapper">
	<div class="page-content">
		<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
		<!-- /.modal -->
		<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
		<!-- BEGIN PAGE HEADER-->
		<!-- BEGIN PAGE HEAD -->
		<div class="page-head">
			<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
				<h1><?=$title_page?></h1>
			</div>
			<!-- END PAGE TITLE -->
		</div>
		<!-- END PAGE HEAD -->
		<!-- BEGIN PAGE BREADCRUMB -->
		<ul class="page-breadcrumb breadcrumb">
			<li>
				<a href="#">Management User</a>
				<i class="fa fa-circle"></i>
			</li>
			
			<li>
				<a href="#"><?=$title_page?></a>
			</li>
		</ul>
		<!-- END PAGE BREADCRUMB -->
		<!-- END PAGE HEADER-->
		<!-- BEGIN PAGE CONTENT-->
		<div class="row">
			<div class="col-md-12">
				<?php callback_submit();?>
				
				<!-- BEGIN EXAMPLE TABLE PORTLET-->
				<div class="portlet box grey-cascade">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-globe"></i><?=$title_page?>
						</div>
						<div class="tools">
							<a href="javascript:;" class="collapse">
							</a>
						</div>
					</div>
					<div class="portlet-body">
						<div class="table-toolbar">
							<div class="row">
								<div class="col-md-12">
									<form class="form-inline">
									    <div class="form-group">
										<label for="exampleInputName2">Nama</label>
										<input type="text" class="form-control" id="exampleInputName2" placeholder="Jane Doe">
									    </div>
									    <div class="form-group">
										<label for="exampleInputEmail2">Provinsi</label>
										<select class='form-control'>
										    <option>-pilih-</option>
										</select>
									    </div>
									    <div class="form-group">
										<label for="exampleInputEmail2">Kab / Kota</label>
										<select class='form-control'>
										    <option>-pilih-</option>
										</select>
									    </div>
									    <button type="submit" class="btn btn-default">Filter</button>
									</form>
								</div>
							</div>
						</div>
						
						<table class="table table-striped table-bordered table-hover" id="sample_1">
							<thead>
								<tr>
									<th>
										 Nama
									</th>
									<th>
										 Lokasi
									</th>
									<th>
										 JP
									</th>
									<th>
										 Pelatihan Terakhir
									</th>
									<th>
										 Spesialisasi
									</th>
								</tr>
							</thead>
						<tbody>
						
						<tr>
						    <td>sutrisno</td>
						    <td>jawa barat, </td>
						    <td>20</td>
						    <td>10</td>
						    <td></td>
						</tr>
						</tbody>
						</table>
					</div>
				</div>
				<!-- END EXAMPLE TABLE PORTLET-->
			</div>
		</div>
		<!-- END PAGE CONTENT-->
	</div>
</div>