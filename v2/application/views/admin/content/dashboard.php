<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN PAGE HEAD -->
			<div class="page-head">
				<!-- BEGIN PAGE TITLE -->
				<div class="page-title">
					<h1>Dashboard <small>Statistik & Report</small></h1>
					
				</div>
				<!-- END PAGE TITLE -->
				<!-- BEGIN PAGE TOOLBAR -->
				
				<!-- END PAGE TOOLBAR -->
			</div>
			<!-- END PAGE HEAD -->
			<!-- BEGIN PAGE BREADCRUMB -->
			<ul class="page-breadcrumb breadcrumb hide">
				<li>
					<a href="javascript:;">Home</a><i class="fa fa-circle"></i>
				</li>
				<li class="active">
					 Dashboard
				</li>
			</ul>
			<!-- END PAGE BREADCRUMB -->
			<!-- BEGIN PAGE CONTENT INNER -->
			<div class="row margin-top-10">
			
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
					<div class="dashboard-stat2">
						<div class="display">
							<div class="number">
							<?php
							  $response = call_api_get($this->config->item('connect_api_url').'/user/count_verified_institute_lpp/1');
							  $response_body = json_decode($response['body'], true);
							  
							  $count_lpp = 0;
							  if($response['header_info']['http_code'] == 200){
							      $count_lpp = $response_body['count'];
							  }
							?>
								<h3 class="font-green-sharp"><?=$count_lpp?></h3>
								<small>TOTAL Lembaga Pelatihan</small>
							</div>
							<div class="icon">
								<i class="icon-user"></i>
							</div>
						</div>
						
					</div>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
					<div class="dashboard-stat2">
						<div class="display">
							<div class="number">
							<?php
							  $response = call_api_get($this->config->item('connect_api_url').'/user/count_entity/narasumber');
							  $response_body = json_decode($response['body'], true);
							  
							  $count_narasumber = 0;
							  if($response['header_info']['http_code'] == 200){
							      $count_narasumber = $response_body['count'];
							  }
							?>
							
								<h3 class="font-red-haze"><?=$count_narasumber?></h3>
								<small>Total Pengajar</small>
							</div>
							<div class="icon">
								<i class="icon-user"></i>
							</div>
						</div>
						
					</div>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
					<div class="dashboard-stat2">
						<div class="display">
							<div class="number">
							
							<?php
							  $response = call_api_get($this->config->item('connect_api_url').'/user/count_entity/peserta');
							  $response_body = json_decode($response['body'], true);
							  
							  $count_peserta = 0;
							  if($response['header_info']['http_code'] == 200){
							      $count_peserta = $response_body['count'];
							  }
							?>
							
								<h3 class="font-blue-sharp"><?=$count_peserta?></h3>
								<small>Total Peserta</small>
							</div>
							<div class="icon">
								<i class="icon-user"></i>
							</div>
						</div>
						
					</div>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
					<div class="dashboard-stat2">
						<div class="display">
							<div class="number">
							<?php
							    $query = $this->training_db->get_all(array('admin_approved' => 1));
							?>
								<h3 class="font-purple-soft"><?=count($query)?></h3>
								<small>Total Pelatihan</small>
							</div>
							<div class="icon">
								<i class="icon-graduation"></i>
							</div>
						</div>
						
					</div>
				</div>
			</div>
			
			<div class="row">
				
				
				<div class='col-md-6 col-sm-12'>
						<div id="container-visitor" style="min-width: 310px; height: 300px; margin: 0 auto"></div>
				</div>
				
				<div class='col-md-6 col-sm-12'>
						<div id="container-now" style="min-width: 310px; height: 300px; margin: 0 auto"></div>
				</div>
				
			</div>
			
			<!-- END PAGE CONTENT INNER -->
		</div>
	</div>