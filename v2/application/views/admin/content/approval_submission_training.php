<?php
    $page = $this->uri->segment(3);
    $token = $this->connect_auth->get_access_token();
?>
<div class="page-content-wrapper">
	<div class="page-content">
		<!-- BEGIN PAGE HEADER-->
		<!-- BEGIN PAGE HEAD -->
		<div class="page-head">
			<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
				<h1><?=$title_page?></h1>
			</div>
			<!-- END PAGE TITLE -->
		</div>
		<!-- END PAGE HEAD -->
		<!-- BEGIN PAGE BREADCRUMB -->
		<ul class="page-breadcrumb breadcrumb">
			<li>
				<a href="#">Approval</a>
				<i class="fa fa-circle"></i>
			</li>
			
			<li>
				<a href="#"><?=$title_page?></a>
			</li>
		</ul>
		<!-- END PAGE BREADCRUMB -->
		<!-- END PAGE HEADER-->
		<!-- BEGIN PAGE CONTENT-->
		<div class="row">
			<div class="col-md-12">
				<?php callback_submit();?>
				
				<div class="portlet light">
				    <div class="portlet-title tabbable-line">
					<div class="caption caption-md">
					    <i class="icon-globe theme-font-color hide"></i>
					    <span class="caption-subject theme-font-color bold uppercase">Pengajuan Pelatihan</span>
					</div>
					<ul class="nav nav-tabs">
					    <li class="active">
						<a href="#tab_submission_training_approval" data-toggle="tab">Menunggu Approval </a>
					    </li>
					    <li>
						<a href="#tab_submission_training_history" data-toggle="tab">Riwayat </a>
					    </li>
					</ul>
				    </div>
				    
				    <div class="portlet-body">
					<div class="tab-content">
					    <div class="tab-pane active" id="tab_submission_training_approval">
						<!-- approval-->
						<div class="portlet light">
							<div class="portlet-title">
								<div class="caption">
									<i class="icon-speech"></i>
									<span class="caption-subject bold uppercase"> Approval</span>
								</div>
								<div class="actions">
									<a href="javascript:;" class="btn btn-circle btn-default btn-icon-only fullscreen"></a>
								</div>
							</div>
							<div class="portlet-body">
								<?php
								if($this->connect_auth->get_me()['entity']=='admin'){
									  $query = $this->training_db->get_all(array(
									      'draft' => 0,
									      'verified_submission_training' => 0
									  ));
								  }

								  if($this->connect_auth->get_me()['entity']=='fasilitator'){
									  $query = $this->training_db->get_all(
									  array(
										  'draft' => 0, 
										  'verified_submission_training' => 1,
										  'facilitator_user_id' => $this->connect_auth->get_me()['user_id']
										  )
									  );
								  }
								  
								  $this->load->view('admin/inc/table_approval_submission_training', array('query' => $query));
								?>
								
							</div>
						</div>
						<!-- ./approval-->
				
				
					    </div>
					    
					    <div class="tab-pane" id="tab_submission_training_history">
						<!-- history-->
						<div class="portlet light">
							<div class="portlet-title">
								<div class="caption">
									<i class="icon-speech"></i>
									<span class="caption-subject bold uppercase"> History</span>
								</div>
								<div class="actions">
									<a href="javascript:;" class="btn btn-circle btn-default btn-icon-only fullscreen"></a>
								</div>
							</div>
							<div class="portlet-body">
							      <?php
							      if($this->connect_auth->get_me()['entity']=='admin'){
								      $query = $this->training_db->get_all(array(
									  'draft' => 0,
									  'verified_submission_training_ne' => 0
								      ));
							      }

							      if($this->connect_auth->get_me()['entity']=='fasilitator'){
								      $query = $this->training_db->get_all(
								      array(
									      'draft' => 0, 
									      'verified_submission_training' => 1,
									      'facilitator_user_id' => $this->connect_auth->get_me()['user_id']
									      )
								      );
							      }
							      
							      $this->load->view('admin/inc/table_approval_submission_training', array('query' => $query));
							      ?>
							</div>
						</div>
						<!-- ./history-->
					    </div>
					    
					</div>  
				    </div>
				    
				</div>
				
				
				
				
				
				
			</div>
		</div>
		<!-- END PAGE CONTENT-->
	</div>
</div>