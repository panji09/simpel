<?php
    $page = $this->uri->segment(3);
    
    $query = $content = array();
    if($page){
	$query = $this->static_pages_db->get($page_id);
	if($query)
	    $content = $query[0];
    }
?>
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- /.modal -->
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE HEADER-->
			<!-- BEGIN PAGE HEAD -->
			<div class="page-head">
				<!-- BEGIN PAGE TITLE -->
				<div class="page-title">
					<h1><?=$title_content?> <small><?=$title_page?></small></h1>
				</div>
				<!-- END PAGE TITLE -->
				<!-- BEGIN PAGE TOOLBAR -->
				
				<!-- END PAGE TOOLBAR -->
			</div>
			<!-- END PAGE HEAD -->
			<!-- BEGIN PAGE BREADCRUMB -->
			<ul class="page-breadcrumb breadcrumb">
				<li>
					<a href="#">Halaman</a>
					<i class="fa fa-circle"></i>
				</li>
				<li>
					<a href="<?=site_url('superadmin/pages/'.$page)?>"><?=$title_page?></a>
					<i class="fa fa-circle"></i>
				</li>
				<li>
					<a href="#"><?=$title_content?></a>
				</li>
			</ul>
			<!-- END PAGE BREADCRUMB -->
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<?php callback_submit();?>
					
					<div class="portlet light" id="form_wizard_1">
						<div class="portlet-title">
							
							<div class="caption font-blue-sharp">
								<i class="fa fa-gift font-blue-sharp"></i>
								<span class="caption-subject"><?=$title_content?></span>
								</div>
							<!--
							<div class="tools hidden-xs">
								<a href="javascript:;" class="collapse">
								</a>
								
							</div>
							-->
						</div>
						<div class="portlet-body">
							<form action="<?=site_url('superadmin/pages/single_content_post/'.$page_id)?>" id="form_sample_3" method='post' class="form-horizontal">
								<div class="form-body">
									<h3 class="form-section">Lengkapi kolom dibawah ini</h3>
									<div class="alert alert-danger display-hide">
										<button class="close" data-close="alert"></button>
										You have some form errors. Please check below.
									</div>
									<div class="alert alert-success display-hide">
										<button class="close" data-close="alert"></button>
										Your form validation is successful!
									</div>
									<?php
									/*
									<!--<div class="form-group">
										<label class="control-label col-md-3">Judul <span class="required">
										* </span>
										</label>
										<div class="col-md-4">
											<input type="text" name="title" value='<?=(isset($content['title']) ? $content['title'] : '' )?>' data-required="1" class="form-control"/>
											
										</div>
										
									</div>
									
									<div class="form-group">			
									    <label class="control-label col-md-3">Cover Foto 
									    </label>
									    <div class="col-md-4">
										<div class="row">
										    <div class="col-xs-6 col-md-3">
										      <a href="#" class="thumbnail" style="width: 500px; height: auto;">
											<img id='change_cover' src="<?=(isset($content['cover']) && $content['cover']!='' ? $content['cover'] : 'http://www.placehold.it/500x150/EFEFEF/AAAAAA&text=pilih foto' )?>" alt="...">
											<input type='hidden' name='cover' id='cover' value='<?=(isset($content['cover']) ? $content['cover'] : '' )?>'>
										      </a>
										    </div>
										</div>
									    </div>
									</div>-->
									*/
									?>
									<div id='content_faq'>
									    <?php if(isset($content) && $content):?>
										<?php if(isset($content['question']) &&  count($content['question'])):?>
										    <?php for($i=0; $i < count($content['question']); $i++):?>
									    <div class='faq'>
										<div class="form-group">
											<label class="control-label col-md-3">Pertanyaan <span class="required">
											* </span>
											</label>
											<div class="col-md-4">
												<input type="text" name="question[]" value='<?=$content['question'][$i]?>' data-required="1" class="form-control"/>
												
											</div>
											
										</div>
										
										<div class="form-group last">
											<label class="control-label col-md-3">Jawaban <span class="required">
											* </span>
											</label>
											<div class="col-md-9">
												<textarea class="form-control wysiwyg-content" name="answer[]" rows="6" data-error-container="#editor2_error"><?=$content['answer'][$i]?></textarea>
												<div id="editor2_error">
												</div>
											</div>
										</div>
										
										<div class="form-group">
											<label class="control-label col-md-3">
											</label>
											<div class="col-md-9">
												<button type="button" class="delete_faq btn green">Hapus</button>
											</div>
										</div>
									    </div>
										    <?php endfor;?>
										<?php endif;?>
									    <?php else:?>
									    <div class='faq'>
										<div class="form-group">
											<label class="control-label col-md-3">Pertanyaan <span class="required">
											* </span>
											</label>
											<div class="col-md-4">
												<input type="text" name="question[]" value='' data-required="1" class="form-control"/>
												
											</div>
											
										</div>
										
										<div class="form-group last">
											<label class="control-label col-md-3">Jawaban <span class="required">
											* </span>
											</label>
											<div class="col-md-9">
												<textarea class="form-control wysiwyg-content" name="answer[]" rows="6" data-error-container="#editor2_error"></textarea>
												<div id="editor2_error">
												</div>
											</div>
										</div>
										
										<div class="form-group">
											<label class="control-label col-md-3">
											</label>
											<div class="col-md-9">
												<button type="button" class="delete_faq btn green">Hapus</button>
											</div>
										</div>
									    </div>
									    <?php endif;?>
									</div>
									
									<h3 class="form-section"></h3>
									<div class="form-group">
										<label class="control-label col-md-3">Terbitkan <span class="required">
										* </span>
										</label>
										<div class="col-md-4">
											<div class="checkbox-list" data-error-container="#form_2_services_error">
												<label>
												<input type="checkbox" value="1" name="published" <?=(isset($content['published']) && $content['published'] ? 'checked' : '' )?>/> Ya </label>
												
											</div>
											
											<div id="form_2_services_error">
											</div>
										</div>
									</div>
									
								</div>
								<div class="form-actions">
									<div class="row">
										<div class="col-md-offset-3 col-md-9">
											<button type="button" id='add_faq' class="btn green">Tambah</button>
											<button type="submit" class="btn green">Simpan</button>
											<button type="button" class="btn default">Batal</button>
										</div>
									</div>
								</div>
							</form>
							
							<div class='hide'>
							    <div id='template_faq'>
								<div class='faq'>
								    <div class="form-group">
									    <label class="control-label col-md-3">Pertanyaan <span class="required">
									    * </span>
									    </label>
									    <div class="col-md-4">
										    <input type="text" name="question[]" value='' data-required="1" class="form-control"/>
										    
									    </div>
									    
								    </div>
								    
								    <div class="form-group">
									    <label class="control-label col-md-3">Jawaban <span class="required">
									    * </span>
									    </label>
									    <div class="col-md-9">
										    <textarea class="form-control wysiwyg-content" name="answer[]" rows="6" data-error-container="#editor2_error"></textarea>
										    <div id="editor2_error">
										    </div>
									    </div>
								    </div>
								    
								    <div class="form-group">
									    <label class="control-label col-md-3">
									    </label>
									    <div class="col-md-9">
										    <button type="button" class="btn green delete_faq">Hapus</button>
									    </div>
								    </div>
								</div>
							    </div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>