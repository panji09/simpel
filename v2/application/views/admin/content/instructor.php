<div class="page-content-wrapper">
	<div class="page-content">
		<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
		<!-- /.modal -->
		<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
		<!-- BEGIN PAGE HEADER-->
		<!-- BEGIN PAGE HEAD -->
		<div class="page-head">
			<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
				<h1><?=$title_page?></h1>
			</div>
			<!-- END PAGE TITLE -->
		</div>
		<!-- END PAGE HEAD -->
		<!-- BEGIN PAGE BREADCRUMB -->
		<ul class="page-breadcrumb breadcrumb">
			<li>
				<a href="#">Management User</a>
				<i class="fa fa-circle"></i>
			</li>
			
			<li>
				<a href="#"><?=$title_page?></a>
			</li>
		</ul>
		<!-- END PAGE BREADCRUMB -->
		<!-- END PAGE HEADER-->
		<!-- BEGIN PAGE CONTENT-->
		<div class="row">
			<div class="col-md-12">
				<?php callback_submit();?>
				
				<!-- BEGIN EXAMPLE TABLE PORTLET-->
				<div class="portlet box grey-cascade">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-globe"></i><?=$title_page?>
						</div>
						<div class="tools">
							<a href="javascript:;" class="collapse">
							</a>
						</div>
					</div>
					<div class="portlet-body">
						<div class="table-toolbar">
							<div class="row">
								<div class="col-md-6">
									<div class="btn-group">
										<a href='<?=site_url('superadmin/management_user/instructor/add')?>' id="sample_editable_1_new" class="btn green">
										Tambah <i class="fa fa-plus"></i>
										</a>
									</div>
								</div>
							</div>
						</div>
						
						<table class="table table-striped table-bordered table-hover" id="sample_1">
							<thead>
								<tr>
									<th>
										 No
									</th>
									<th>
										 Username
									</th>
									<th>
										 Email
									</th>
									<th>
										 Join
									</th>
									<th>
										 Status
									</th>
									<th>
										 Action
									</th>
								</tr>
							</thead>
						<tbody>
						<?php
						    $i=0;
						    foreach($this->user_db->get_all(array('user' => 'admin')) as $row):
						?>
						<tr class="odd gradeX">
						    <td><?=++$i?></td>
						    <td><?=$row['username']?></td>
						    <td>
							<a href="mailto:<?=$row['email']?>"><?=$row['email']?> </a>
						    </td>
						    <td><time class="timeago" datetime="<?=date('c',$row['created'])?>"><?=date("F j, Y, g:i a",$row['created']) ?></time></td>
						    <td>
							<span class="label label-sm <?=($row['activated'] ? 'label-success' : 'label-danger')?>"> <?=($row['activated'] ? 'Aktif' : 'Non-Aktif')?> </span>
						    </td>
						    <td>
							<div class="btn-group">
							    <button type="button" class="btn btn-default" onclick="location.href='<?=site_url('admin/management_user/user_super_admin/edit/'.$row['_id']->{'$id'})?>'">Edit</button>
							    <button type="button" class="btn btn-default delete-user" data-url-delete="<?=site_url('admin/management_user/user_super_admin/delete/'.$row['_id']->{'$id'})?>" data-toggle="modal" data-target="#delete">Delete</button>
							</div>
						    </td>
						</tr>
						<?php endforeach;?>
						
						</tbody>
						</table>
					</div>
				</div>
				<!-- END EXAMPLE TABLE PORTLET-->
			</div>
		</div>
		<!-- END PAGE CONTENT-->
	</div>
</div>