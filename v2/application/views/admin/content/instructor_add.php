<?php  
	$page = $this->uri->segment(4);
	$query = $user = array();
  	if($page == 'edit'){
		$query = $this->user_db->get($user_id);
		if($query)
    		$user = $query[0];
  	}
?>

<div class="page-content-wrapper">
	<div class="page-content">
		<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
		<!-- /.modal -->
		<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
		<!-- BEGIN PAGE HEADER-->
		<!-- BEGIN PAGE HEAD -->
		<div class="page-head">
			<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
				<h1><?=$title_content?> <small><?=$title_page?></small></h1>
			</div>
			<!-- END PAGE TITLE -->
		</div>
		<!-- END PAGE HEAD -->
		<!-- BEGIN PAGE BREADCRUMB -->
		<ul class="page-breadcrumb breadcrumb">
			<li>
				<a href="#">Management User</a>
				<i class="fa fa-circle"></i>
			</li>
			<li>
				<a href="<?=site_url('admin/management_user/user_admin')?>"><?=$title_page?></a>
				<i class="fa fa-circle"></i>
			</li>
			<li>
				<a href="#"><?=$title_content?></a>
			</li>
		</ul>
		<!-- END PAGE BREADCRUMB -->
		<!-- END PAGE HEADER-->
		<!-- BEGIN PAGE CONTENT-->
		<div class="row">
			<div class="col-md-12">
				<div class="portlet box blue" id="form_wizard_1">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-gift"></i> <?=$title_content?> - <span class="step-title">
							Step 1 of 3 </span>
						</div>
						<div class="tools hidden-xs">
							<a href="javascript:;" class="collapse">
							</a>
							
						</div>
					</div>
					<div class="portlet-body form" >
						<form action="<?=site_url('superadmin/management_user/post_user_admin/narasumber')?>" class="form-horizontal coach_registration" id="submit_form" method="POST">
							<div class="form-wizard">
								<div class="form-body">
									<ul class="nav nav-pills nav-justified steps">
										<li>
											<a href="#tab1" data-toggle="tab" class="step">
											<span class="number">
											1 </span>
											<span class="desc">
											<i class="fa fa-check"></i> Profil Pengajar </span>
											</a>
										</li>
										<li>
											<a href="#tab2" data-toggle="tab" class="step">
											<span class="number">
											2 </span>
											<span class="desc">
											<i class="fa fa-check"></i> Profil Detil </span>
											</a>
										</li>
										
										<li>
											<a href="#tab3" data-toggle="tab" class="step">
											<span class="number">
											3 </span>
											<span class="desc">
											<i class="fa fa-check"></i> Konfirmasi </span>
											</a>
										</li>
									</ul>
									<div id="bar" class="progress progress-striped" role="progressbar">
										<div class="progress-bar progress-bar-success">
										</div>
									</div>
									<div class="tab-content">
										<div class="alert alert-danger display-none">
											<button class="close" data-dismiss="alert"></button>
											Ups, sepertinya ada error. Silahkan cek dibawah ini.
										</div>
										<div class="alert alert-success display-none">
											<button class="close" data-dismiss="alert"></button>
											Sukses!
										</div>
										
										<div class="tab-pane active" id="tab1">
											<h3 class="block">Lengkapi Profil Pengajar</h3>
											

											<div class="form-group">
												
												<label class="control-label col-md-3">Foto 
												</label>
												<div class="col-md-4">
													<div class="row">
													    <div class="col-xs-6 col-md-3">
													      <a href="#" class="thumbnail" style="width: 200px; height: auto;">
														<img id='avatar' src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&text=pilih foto" data-default='http://www.placehold.it/200x150/EFEFEF/AAAAAA&text=pilih foto' alt="...">
														<input type='hidden' value='<?=(isset($user['photo']) ? $user['photo'] : '')?>' name='photo'>
													      </a>
													    </div>
													    
													  </div>
													  <input id='clear_avatar' type='button' class='btn' value='Hapus'>
												</div>
											</div>						
											<div class="form-group">
												<label class="control-label col-md-3">Username <span class="required">
												* </span>
												</label>
												<div class="col-md-4">
													<input type="text" class="form-control" name="username" placeholder="cahyogumilang"/>
													<span class="help-block">
													Isi username </span>
												</div>
											</div>
											<div class="form-group">
												<label class="control-label col-md-3">Password <span class="required">
												* </span>
												</label>
												<div class="col-md-4">
													<input type="password" class="form-control" name="password" id="submit_form_password"/>
													<span class="help-block">
													Isi kata sandi </span>
												</div>
											</div>
											<div class="form-group">
												<label class="control-label col-md-3">Konfirmasi Password <span class="required">
												* </span>
												</label>
												<div class="col-md-4">
													<input type="password" class="form-control" name="rpassword"/>
													<span class="help-block">
													Konfirmasi kata sandi </span>
												</div>
											</div>
											<div class="form-group">
												<label class="control-label col-md-3">Email <span class="required">
												* </span>
												</label>
												<div class="col-md-4">
													<input type="text" class="form-control" name="email" placeholder="cahyo@gmail.com"/>
													<span class="help-block">
													Isi email </span>
												</div>
											</div>
											<div class="form-group">
												<label class="control-label col-md-3">Nama <span class="required">
												* </span>
												</label>
												<div class="col-md-4">
													<input type="text" class="form-control" name="fullname" value="<?=(isset($user['fullname']) ? $user['fullname'] : '')?>" placeholder="Cahyo Gumilang"/>
													<span class="help-block">
													Isi nama lengkap</span>
												</div>
											</div>
											<div class="form-group">
												<label class="control-label col-md-3">Tempat Lahir <span class="required">
												* </span>
												</label>
												<div class="col-md-4">
													<input type="text" class="form-control" name="birth_place" value='<?=(isset($user['birth_place']) ? $user['birth_place'] : '')?>' placeholder="Blitar"/>
													<span class="help-block">
													Isi tempat lahir</span>
												</div>
											</div>
											<div class="form-group">
												<label class="control-label col-md-3">Tanggal Lahir <span class="required">
												* </span>
												</label>
												<div class="col-md-4">
													<div class='input-group date' id='datetimepicker1'>
													    <input type='text' class="form-control" name='birth_date' value='<?=(isset($user['birth_date']) ? $user['birth_date'] : '')?>' placeholder="1945-12-12"/>
													    <span class="input-group-addon">
														<span class="glyphicon glyphicon-calendar"></span>
													    </span>
													</div>
													<div id="form_birth_date_error"></div>
													<span class="help-block">
													Isi tanggal lahir</span>
												</div>
											</div>
											<div class="form-group">
												<label class="control-label col-md-3">No. Telp/HP
													<span class="required">*</span>
												</label>
												<div class="col-md-4">
													<input type="text" class="form-control" name="phone" placeholder="081279221166"/>
													<span class="help-block">
													Isi no.Telp </span>
												</div>
											</div>
											<div class="form-group">
												<label class="control-label col-md-3">Jenis Kelamin <span class="required">
												* </span>
												</label>
												<div class="col-md-4">
													<div class="radio-list">
														<label>
														<input type="radio" name="gender" value="M" data-title="Laki-laki"/>
														Laki-laki </label>
														<label>
														<input type="radio" name="gender" value="F" data-title="Perempuan"/>
														Perempuan </label>
													</div>
													<div id="form_gender_error">
													</div>
												</div>		
											</div>
											
										</div>
										
										<div class="tab-pane" id="tab2">
											<h3 class="block">Lengkapi detail data profil Pengajar</h3>
											<h4 class="form-section">Data Pendidikan dan Kepegawaian</h4>
											<div class="form-group">
												<label class="control-label col-md-3">Pendidikan Terakhir 
													<span class="required">*</span>
												</label>
												<div class="col-md-4">
													<select class="form-control select2me" name="last_education" id="list_last_education">
													</select>
													<span class="help-block">
													Isi pendidikan terakhir </span>
												</div>
											</div>
											<div class="form-group">
												<label class="control-label col-md-3">Gelar Akademis
													<span class="required">*</span>
												</label>
												<div class="col-md-4">
														<input type="text" class="form-control" name="academic_degree" id="academic_degree"  placeholder="contoh: S.T."/>
													<span class="help-block">
													Isi gelar akademis </span>
												</div>
											</div>
											<div class="form-group">
												<label class="control-label col-md-3">Status Kepegawaian
													<span class="required">*</span>
												</label>
												
												<div class="col-md-4">
														<select class="form-control select2me" name="employment" id="status_kepegawaian_list">
															<option value=""></option>
														</select>
													<div id="training_institute_status_error"></div>
													<span class="help-block">
													Pilih status kepegawaian </span>
												</div>
												
											</div>
												<div class="form-group pns">
													<label class="control-label col-md-3">NIP/NRP <span class="required">*</span></label>
													
													<div class="col-md-4">
														<input type="text" class="form-control" name="nip_id" placeholder="contoh: 197604012001121001"/>
														<span class="help-block">
														Isi NIP/NRP</span>
													</div>
												</div>
											<div class="form-group">
												<label class="control-label col-md-3">No.KTP
													<span class="required">*</span>
												</label>
												<div class="col-md-4">
													<input type="text" class="form-control" name="ktp_id" placeholder="1215125451123456"/>
													<span class="help-block">
													Isi No KTP</span>
												</div>
											</div>
												<h4 class="form-section">Data kantor</h4>
												<div class="form-group">
													<label class="control-label col-md-3">Nama Instansi/Institusi/Perusahaan <span class="required">* </span> </label>
													<div class="col-md-4">													
														<input type="text" class="form-control" name="institution" id="institution" placeholder="contoh: Dewan Perwakilan Daerah"/>
														<span class="help-block">
														Isi nama Instansi/Institusi/Perusahaan Anda berasal</span>
													</div>
												</div>
												<div class="form-group">
												      <label class="control-label col-md-3">Satuan Kerja</label>
												      <div class="col-md-4">
													      <input type="text" class="form-control" name="work_unit" placeholder="Isi satuan kerja"/>
													      <span class="help-block">
													      Isi satuan kerja </span>
												      </div>
											      </div>	
												<div class="form-group">
													<label class="control-label col-md-3">Provinsi <span class="required">* </span></label>
													<div class="col-md-4">
														<select class="form-control select2me" name="office_province" id="domisili_provinsi_list_kerja">
															<option value=""></option>
														</select>															
													</div>
												</div>												
												<div class="form-group">
													<label class="control-label col-md-3">Kab/Kota <span class="required">* </span></label>
													<div class="col-md-4">
														<select class="form-control select2me" name="office_district" id="domisili_kabupaten_list_kerja">
															<option value=""></option>
														</select>	
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">Alamat
														<span class="required">* </span>
													</label>
													<div class="col-md-4">
														<textarea class="form-control" name="office_address" placeholder="contoh: Jalan Kemang Timur no 3"/></textarea>
														<span class="help-block">
														Isi alamat kantor </span>
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">Kode Pos
														<span class="required">* </span>
													</label>
													<div class="col-md-4">
														<input type="text" class="form-control" name="office_zip_code" placeholder="contoh: 16911"/>
														<span class="help-block">
														Isi kode pos </span>
													</div>
												</div>
																						
												<h4 class="form-section">Data Atasan</h4>												
											<div class="form-group">
												<label class="control-label col-md-3">Nama Atasan</label>
												
												<div class="col-md-4">
													<input type="text" class="form-control" name="head_name" placeholder="Parjiman Dwi Marno"/>
													<span class="help-block">
													Isi nama atasan </span>
												
												</div>
											</div>
											<div class="form-group">
												<label class="control-label col-md-3">Email Atasan</label>
												
												<div class="col-md-4">
													<input type="text" class="form-control" name="head_email" placeholder="parjiman@gmail.com"/>
													<span class="help-block">
													Isi email atasan </span>
												
												</div>
											</div>
											<div class="form-group">
												<label class="control-label col-md-3">No. Telpon Atasan</label>
												
												<div class="col-md-4">
													<input type="text" class="form-control" name="head_phone" placeholder="081277341199"/>
													<span class="help-block">
													Isi no. telpon atasan </span>
												
												</div>
											</div>											
											<div class="form-group">
												<label class="control-label col-md-3">Status 
												</label>
												<div class="col-md-4">
													<div class="checkbox">
													  <label>
													    <input type="checkbox" name='activated' value='1' <?=(isset($user['activated']) && $user['activated'] ? 'checked' : '')?> Aktif
													  </label>
													</div>
												</div>
											</div>											
										</div>																			
										<div class="tab-pane" id="tab3">
											<h3 class="block">Konfirmasi Data Pengajar</h3>

											<h4 class="form-section">Profil Pengajar</h4>
											<div class="form-group">
												<label class="control-label col-md-3">Username:</label>
												<div class="col-md-4">
													<p class="form-control-static" data-display="username">
													</p>
												</div>
											</div>
											<div class="form-group">
												<label class="control-label col-md-3">Email:</label>
												<div class="col-md-4">
													<p class="form-control-static" data-display="email">
													</p>
												</div>
											</div>
											<div class="form-group">
												<label class="control-label col-md-3">Nama:</label>
												<div class="col-md-4">
													<p class="form-control-static" data-display="fullname">
													</p>
												</div>
											</div>
											<div class="form-group">
												<label class="control-label col-md-3">Tempat Lahir:</label>
												<div class="col-md-4">
													<p class="form-control-static" data-display="birth_place">
													</p>
												</div>
											</div>
											<div class="form-group">
												<label class="control-label col-md-3">Tanggal Lahir:</label>
												<div class="col-md-4">
													<p class="form-control-static" data-display="birth_date">
													</p>
												</div>
											</div>
											<div class="form-group">
												<label class="control-label col-md-3">No. Telp/HP</label>
												<div class="col-md-4">
													<p class="form-control-static" data-display="phone">
													</p>
												</div>
											</div>
											<div class="form-group">
												<label class="control-label col-md-3">Jenis Kelamin </label>
												<div class="col-md-4">
													<p class="form-control-static" data-display="gender">
													</p>
												</div>
											</div>
											<!-- <div class="form-group">
												<label class="control-label col-md-3">Alamat </label>
												<div class="col-md-4">
													<p class="form-control-static" data-display="address">
													</p>
												</div>
											</div>
											<div class="form-group">
												<label class="control-label col-md-3">Provinsi </label>
												<div class="col-md-4">
													<p class="form-control-static" data-display="province">
													</p>
												</div>
											</div>
											<div class="form-group">
												<label class="control-label col-md-3">Kab/Kota </label>
												<div class="col-md-4">
													<p class="form-control-static" data-display="district">
													</p>
												</div>
											</div>
											<div class="form-group">
												<label class="control-label col-md-3">Kecamatan </label>
												<div class="col-md-4">
													<p class="form-control-static" data-display="subdistrict"></p>
												</div>
											</div> -->
											
											
											<h4 class="form-section">Data pendidikan dan kepegawaian</h4>
											
											<div class="form-group">
												<label class="control-label col-md-3">Pendidikan Terakhir:</label>
												<div class="col-md-4">
													<p class="form-control-static" data-display="last_education">
													</p>
												</div>
											</div>
											<div class="form-group">
												<label class="control-label col-md-3">Gelar Akademis:</label>
												<div class="col-md-4">
													<p class="form-control-static" data-display="academic_degree">
													</p>
												</div>
											</div>
											<div class="form-group">
												<label class="control-label col-md-3">Status Kepegawaian:</label>
												<div class="col-md-4">
													<p class="form-control-static" data-display="employment">
													</p>
												</div>
											</div>
											<div class="form-group pns">
												<label class="control-label col-md-3">NIP/NRP:</label>
												<div class="col-md-4">
													<p class="form-control-static" data-display="nip_id">
													</p>
												</div>
											</div>
											<div class="form-group">
												<label class="control-label col-md-3">No.KTP:</label>
												<div class="col-md-4">
													<p class="form-control-static" data-display="ktp_id">
													</p>
												</div>
											</div>
												<h4 class="form-section">Data Kantor</h4>
												<div class="form-group">
													<label class="control-label col-md-3">Nama Instansi/Institusi/Perusahaan:</label>
													<div class="col-md-4">
														<p class="form-control-static" data-display="institution">
														</p>
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">Satuan Kerja:</label>
													<div class="col-md-4">
														<p class="form-control-static" data-display="work_unit">
														</p>
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">Provinsi:</label>
													<div class="col-md-4">
														<p class="form-control-static" data-display="office_province">
														</p>
													</div>
												</div>												
												<div class="form-group">
													<label class="control-label col-md-3">Kab/Kota:</label>
													<div class="col-md-4">
														<p class="form-control-static" data-display="office_district">
														</p>
													</div>
												</div>	
												<div class="form-group">
													<label class="control-label col-md-3">Alamat:</label>
													<div class="col-md-4">
														<p class="form-control-static" data-display="office_address">
														</p>
													</div>
												</div>	
												<div class="form-group">
													<label class="control-label col-md-3">Kode Pos:</label>
													<div class="col-md-4">
														<p class="form-control-static" data-display="office_zip_code">
														</p>
													</div>
												</div>
											
												<h4 class="form-section">Data Atasan</h4>											
											<div class="form-group">
												<label class="control-label col-md-3">Nama Atasan:</label>
												<div class="col-md-4">
													<p class="form-control-static" data-display="head_name">
													</p>
												</div>
											</div>
											<div class="form-group">
												<label class="control-label col-md-3">Email Atasan:</label>
												<div class="col-md-4">
													<p class="form-control-static" data-display="head_email">
													</p>
												</div>
											</div>
											<div class="form-group">
												<label class="control-label col-md-3">No. Telpon Atasan:</label>
												<div class="col-md-4">
													<p class="form-control-static" data-display="head_phone"></p>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="form-actions">
									<div class="row">
										<div class="col-md-offset-3 col-md-9">
											<a href="javascript:;" class="btn default button-previous">
											<i class="m-icon-swapleft"></i> Kembali </a>
											<a href="javascript:;" class="btn blue button-next">
											Lanjut <i class="m-icon-swapright m-icon-white"></i>
											</a>
											<button type='submit' class="btn green button-submit">
											Proses <i class="m-icon-swapright m-icon-white"></i>
											</button>
										</div>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<!-- END PAGE CONTENT-->
	</div>
</div>