	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			<!-- BEGIN PAGE HEAD -->
			<div class="page-head">
				<!-- BEGIN PAGE TITLE -->
				<div class="page-title">
					<h1><?=$title_page?></h1>
				</div>
				<!-- END PAGE TITLE -->
			</div>
			<!-- END PAGE HEAD -->
			<!-- BEGIN PAGE BREADCRUMB -->
			<ul class="page-breadcrumb breadcrumb">
				<li>
					<a href="#">Dashboard</a>
					<i class="fa fa-circle"></i>
				</li>
				<li>
					<a href="#">Pengaturan</a>
					<i class="fa fa-circle"></i>
				</li>
				
				<li>
					<a href="#"><?=$title_page?></a>
					<i class="fa fa-circle"></i>
				</li>
			</ul>
			<!-- END PAGE BREADCRUMB -->
			<!-- END PAGE HEADER-->
			
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<?php callback_submit();?>
					<div class="portlet light">
						<div class="portlet-title">
							<div class="caption font-blue-sharp">
								<i class="icon-globe font-blue-sharp"></i>
								<span class="caption-subject">Pengaturan</span>
								<span class="caption-helper"><?=$title_page?></span>
							</div>
							
							
							
						</div>
						<div class="portlet-body">
							<table class="table table-striped table-bordered table-hover" id="table_default_jp_instructor">
						<thead>
							<tr>
								<th>
									  Nama
								</th>
								<th>
									  Lokasi Kantor
								</th>
								<th>
									  Jam Pelajaran/tahun
								</th>
								
								<th>
									  Kompetensi
								</th>
								<th>
									  Action
								</th>
							</tr>
						</thead>
						<tbody>
						<?php
						    $token = $this->connect_auth->get_access_token();
						    $response = call_api_get($this->config->item('connect_api_url').'/user/all/'.$token.'/narasumber');
						    $response_body = json_decode($response['body'], true);
						    
						    if($response['header_info']['http_code'] == 200):
						    foreach($response_body['data'] as $row):
						?>
						<tr>
						    <td><?=$row['fullname']?></td>
						    <td><?=$row['office_province']['name'].', '.$row['office_district']['name']?></td>
						    <td><?php
							$query = $this->hour_leasson_db->get_default_jp($row['user_id']);
							$hour_leasson_id = null;
							if($query){
							    $result = $query[0];
							    echo $result['hour_leasson'];
							}
						    ?></td>
						    
						    <td><?=(isset($row['competence']) ? $row['competence'] : '') ?></td>
						    <td>
							<button type='button' 
							  class="btn btn-default choose_instructor_set_jp" 
							  data-toggle="modal" 
							  data-instructor_id="<?=$row['user_id']?>" 
							  data-instructor_name="<?=$row['fullname']?>" 
							  data-url_submit="<?=site_url('superadmin/setting/set_default_hour_leasson_instructor_post')?>"
							  data-target="#choose_instructor_modal_set_default"
							>Edit Jam Pelajaran</button>
							
							<button type="button" class="btn btn-default" onclick="location.href='<?=$this->config->item('connect_url').'/user/edit/'.$row['user_id'].'?access_token='.$this->connect_auth->get_access_token()?>'">Edit Kompetensi</button>
						    </td>
						</tr>
						<?php endforeach; endif;?>
						</tbody>
						</table>
							
						</div>
					</div>
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>