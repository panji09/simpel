<?php
    $token = $this->connect_auth->get_access_token();
    
    $query = $this->training_db->get($training_id);
    if($query){
	$training = $query[0];
	
	
    }
?>
<style>
.top-buffer { margin-top:10px; }
.table-toolbar .form-group { width: 400px }
</style>
<div class="page-content-wrapper">
	<div class="page-content">
		<!-- BEGIN PAGE HEADER-->
		<!-- BEGIN PAGE HEAD -->
		<div class="page-head">
			<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
				<h1><?=$title_page?></h1>
			</div>
			<!-- END PAGE TITLE -->
		</div>
		<!-- END PAGE HEAD -->
		<!-- BEGIN PAGE BREADCRUMB -->
		<ul class="page-breadcrumb breadcrumb">
			<li>
				<a href="#">Management User</a>
				<i class="fa fa-circle"></i>
			</li>
			
			<li>
				<a href="#"><?=$title_page?></a>
			</li>
		</ul>
		<!-- END PAGE BREADCRUMB -->
		<!-- END PAGE HEADER-->
		<!-- BEGIN PAGE CONTENT-->
		<div class="row">
			
			<div class="col-md-12">
				<?php callback_submit();?>
				
				<!-- BEGIN EXAMPLE TABLE PORTLET-->
				<div class="portlet box grey-cascade">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-globe"></i><?=$title_page?>
						</div>
						<div class="tools">
							<a href="javascript:;" class="collapse">
							</a>
						</div>
					</div>
					<div class="portlet-body">
						
						<h3>Data Pengajuan</h3>
						<div class="table-toolbar">
							<div class="row">
								<div class="col-md-12 form-inline top-buffer">
									    <div class="form-group">
									      <label for="exampleInputName2">Lembaga Pelatihan: </label>
									      <button 
											type="button" 
											class="btn btn-default review-institution" 
											data-toggle="modal" 
											data-user_id="<?=(isset($training['creator']['user_id']) ? $training['creator']['user_id'] : '' )?>"	
											data-target="#review_institution"
									      >Review</button>
									    </div>
									    
									    <div class="form-group">
									      <label for="exampleInputName2">Pengajuan Pelatihan: </label>
									      <button 
										type="button" 
										class="btn btn-default review_training_submission" 
										
										data-url-submit="<?=site_url('lpp/management_training/training_post/'.$training_id)?>" 
										data-toggle="modal" 
										data-training_id="<?=$training_id?>" 
										data-target="#review_training_submission"
									      >Review</button>
									    </div>
								</div>
								
							</div>
						</div>
						
						
						<div id='list_choose_instructor'>
						    <div id='template_choose_instructor' style='display:none'>
							<table>
							    <tbody>
								<tr>
								    <td class='instructor_name'>a</td>
								    <td class='date'>b</td>
								    <td><input type='text' class='form-control hour_leasson' name='hour_leasson[]' value='' > </td>
								    <td>
									<div class='set_date_container'>
									</div>
									<input type='hidden' name='instructor_id[]' class='instructor_id' value='' >
									<button type="button" class="btn btn-default delete_selected_instructor">Hapus</button>
								    </td>
								    
								</tr>
							    </tbody>
							    
							</table>
							
						    </div>
						<hr>
						<h3>Pilih Pengajar</h3>
						<div class="table-toolbar">
							<div class="row">
								<form method='post' action='<?=site_url('fasilitator/management_training/hour_leasson_post/'.$training_id)?>' id='form_choose_instructor'>
								    <div class='row'>
									<div class='col-md-10 col-md-offset-1'>
									    <table class="table table-hover" >
										<thead>
										    <th>Nama Pengajar</th>
										    <th>Tanggal</th>
										    <th>Jam Pelajaran</th>
										    <th>Action</th>
										    
										</thead>
										<tbody>
										    
										</tbody>
									    </table>
								      
									</div>
								    </div>
								    
								    
								    <div class="col-md-12 form-inline top-buffer">
									<button type="submit" class="btn btn-default">Proses</button>
									<button type="submit" class="btn btn-default">Draft</button>
									<a href='<?=($this->session->userdata('current_url') ? $this->session->userdata('current_url') : site_url('fasilitator/management_training/training') )?>' class="btn btn-default">Batal</a>
									
									
								    </div>
								</form>
								
							</div>
						</div>
						</div>
						
						
					</div>
				</div>
				<!-- END EXAMPLE TABLE PORTLET-->
			</div>
			
			<div class="col-md-12">
				<?php callback_submit();?>
				
				<!-- BEGIN EXAMPLE TABLE PORTLET-->
				<div class="portlet box grey-cascade">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-globe"></i><?=$title_page?>
						</div>
						<div class="tools">
							<a href="javascript:;" class="collapse">
							</a>
						</div>
					</div>
					<div class="portlet-body">
						
						<h3>Filter Data Pengajar</h3>
						<div class="table-toolbar">
							<div class="row">
								
								<div class="col-md-12">
								    <form class="form-inline">
								      
								      <div class="form-group">
									<label class="col-sm-3 control-label">Kompetensi</label>
									<div class="col-sm-9">
									  <input type="text" class="form-control column_filter" data-search_col='4'  placeholder="Kompetensi">
									</div>
								      </div>
								      
								      <div class="form-group">
									<label class="col-sm-3 control-label">Nama</label>
									<div class="col-sm-9">
									  <input type="text" class="form-control column_filter" data-search_col='0' placeholder="Nama">
									</div>
								      </div>
								      <div class="form-group">
									<label for="inputPassword3" class="col-sm-3 control-label">Provinsi</label>
									<div class="col-sm-9">
									  <select name='training_location_province' class="form-control select2me column_filter_select" data-search_col='1' id='province' style='width:100%'>
									    <option value=''>-Pilih-</option>
									    <?php
										$query = file_get_contents($this->config->item('connect_api_url').'/location/province');
										$province = array();
										if($query){
										    $province = json_decode($query,true);
										}
										
										if($province)
										foreach($province as $row):
									    ?>
										<option value='<?=$row['id']?>'  ><?=$row['name']?></option>
									    <?php
									    endforeach;
									    ?>
									    
									  </select>
									</div>
								      </div>
								      <div class="form-group">
									<label for="inputPassword3" class="col-sm-3 control-label ">kab / Kota</label>
									<div class="col-sm-9">
									  <select name='training_location_district' class="column_filter_select form-control select2me" data-search_col='1' id='district' style='width:100%'>
									      <option value=''>-Pilih-</option>
									      
									      
									    </select>
									</div>
								      </div>
								      
								    </form>
									
								</div>
								
							</div>
						</div>
						
						<table class="table table-striped table-bordered table-hover" id="table_choose_instructor">
						<thead>
							<tr>
								<th>
									  Nama
								</th>
								<th>
									  Lokasi Kantor
								</th>
								<th>
									  Jumlah Jam Pelajaran/tahun
								</th>
								<th>
									  Pelatihan Terakhir
								</th>
								<th>
									  Kompetensi
								</th>
								<th>
									  Action
								</th>
							</tr>
						</thead>
						
						<tbody>
						<?php
						$response = call_api_get($this->config->item('connect_api_url').'/user/all/'.$token.'/narasumber');
						$response_body = json_decode($response['body'], true);

						//check unavailable user
						$temp_unavailable = array();
						$unavailable_instructor_id = array();
						$query = $this->training_db->get($training_id);

						if($query){
							$training = $query[0];

							$training_date = explode(' - ',$training['training_date']);

							$training_date_start = $training_date[0];
							$training_date_end = $training_date[1];

							for($i=strtotime($training_date_start); $i <= strtotime($training_date_end); $i+= 60 * 60 * 24  ){
								$query_agenda = $this->agenda_instructor_db->get(date("Y-m-d", $i));

								if($query_agenda)
								foreach($query_agenda as $row_agenda){
									if($row_agenda['agenda'])
									foreach($row_agenda['agenda'] as $row_instructor){
										$temp_unavailable[$row_instructor['instructor_id']] = 1;

									}
								}
							}
						}

						if($temp_unavailable){
							foreach($temp_unavailable as $instructor_id => $row){
								$unavailable_instructor_id[]= $instructor_id;
							}
						}


						if($response['header_info']['http_code'] == 200):
							foreach($response_body['data'] as $row):
								if(!in_array($row['user_id'], $unavailable_instructor_id ) ):
						?>
						<tr>
						    <td><?=$row['fullname']?></td>
						    <td><?=$row['office_province']['name'].', '.$row['office_district']['name']?></td>
						    <td><?=$this->hour_leasson_db->get_count($instructor_id = $row['user_id'], $year = date('Y'))?></td>
						    <td>-</td>
						    <td><?=(isset($row['competence']) ? $row['competence'] : '') ?></td>
						    <td>
							<button type='button' 
							  class="btn btn-default choose_instructor" 
							  data-toggle="modal" 
							  data-instructor_id="<?=$row['user_id']?>" 
							  data-instructor_name="<?=$row['fullname']?>" 
							  data-training_id="<?=$training_id?>" 
							  
							  data-target="#choose_instructor_modal"
							>Pilih</button>
						    </td>
						</tr>
						<?php endif; endforeach; endif;?>
						</tbody>
						</table>
					</div>
				</div>
				<!-- END EXAMPLE TABLE PORTLET-->
			</div>
		</div>
		<!-- END PAGE CONTENT-->
	</div>
</div> 
