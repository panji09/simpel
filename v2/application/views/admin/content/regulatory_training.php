	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- /.modal -->
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE HEADER-->
			<!-- BEGIN PAGE HEAD -->
			<div class="page-head">
				<!-- BEGIN PAGE TITLE -->
				<div class="page-title">
					<h1><?=$title_page?></h1>
				</div>
				<!-- END PAGE TITLE -->
				<!-- BEGIN PAGE TOOLBAR -->
				
				<!-- END PAGE TOOLBAR -->
			</div>
			<!-- END PAGE HEAD -->
			<!-- BEGIN PAGE BREADCRUMB -->
			<ul class="page-breadcrumb breadcrumb">
				<li>
					<a href="#">Dashboard</a>
					<i class="fa fa-circle"></i>
				</li>
				<li>
					<a href="#">Halaman</a>
					<i class="fa fa-circle"></i>
				</li>
				
				<li>
					<a href="#"><?=$title_page?></a>
					<i class="fa fa-circle"></i>
				</li>
			</ul>
			<!-- END PAGE BREADCRUMB -->
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					
					<?php callback_submit();?>
					
					<!-- BEGIN EXAMPLE TABLE PORTLET-->
					<div class="portlet light">
						<div class="portlet-title">
							<div class="caption font-blue-sharp">
								<i class="icon-globe font-blue-sharp"></i>
								<span class="caption-subject"><?=$title_page?></span>
								<span class="caption-helper">manajemen peraturan pelatihan</span>
							</div>
							<div class="actions">
								<a href="<?=site_url('superadmin/pages/regulatory_training/add')?>" class="btn btn-circle btn-default">
								<i class="fa fa-plus"></i> Add </a>
								<a href="javascript:;" class="btn btn-circle btn-default btn-icon-only fullscreen"></a>
							</div>
						</div>
						<div class="portlet-body">
							
							
							<table class="table table-striped table-bordered table-hover" id="table_regulatory_training">
							<thead>
							<tr>
								<th>Tanggal</th>
								<th>Kategori</th>
								<th>Judul</th>
								<th>Penulis</th>
								<th>Terbitkan</th>
								<th>Action</th>
							</tr>
							</thead>
							<tbody>
							<?php
							    
							    $query = $this->dynamic_pages_db->get_all(array('page_id'=>'regulatory_training'));
							    
							    if($query):
								foreach($query as $row):
							?>
							<tr class="odd gradeX">
							    <td><time  datetime="<?=date("d/m/Y H:i:s",$row['created'])?>"><?=date("d/m/Y H:i:s",$row['created']) ?></time></td>
							    <td><?=$row['category']?></td>
							    <td><?=$row['title']?></td>
							    <td><?=$row['author']['fullname']?></td>
							    <td>
								<span class="label label-sm <?=($row['published'] ? 'label-success' : 'label-danger')?>"> <?=($row['published'] ? 'Ya' : 'Tidak')?> </span>
							    </td>
							    <td>
								<div class="btn-group">
								    <button type="button" class="btn btn-default" onclick="location.href='<?=site_url('superadmin/pages/regulatory_training/edit/'.$row['_id']->{'$id'})?>'">Edit</button>
								    <button type="button" class="btn btn-default delete-user" data-url-delete="<?=site_url('superadmin/pages/regulatory_training/delete/'.$row['_id']->{'$id'})?>" data-toggle="modal" data-target="#delete">Delete</button>
								</div>
							    </td>
							</tr>
							    <?php endforeach;?>
							<?php endif;?>
							    
							
							</tbody>
							</table>
						</div>
					</div>
					<!-- END EXAMPLE TABLE PORTLET-->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>