<?php
    $page = $this->uri->segment(3);
    $token = $this->connect_auth->get_access_token();
?>
<div class="page-content-wrapper">
	<div class="page-content">
		<!-- BEGIN PAGE HEADER-->
		<!-- BEGIN PAGE HEAD -->
		<div class="page-head">
			<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
				<h1><?=$title_page?></h1>
			</div>
			<!-- END PAGE TITLE -->
		</div>
		<!-- END PAGE HEAD -->
		<!-- BEGIN PAGE BREADCRUMB -->
		<ul class="page-breadcrumb breadcrumb">
			<li>
				<a href="#">Approval</a>
				<i class="fa fa-circle"></i>
			</li>
			
			<li>
				<a href="#"><?=$title_page?></a>
			</li>
		</ul>
		<!-- END PAGE BREADCRUMB -->
		<!-- END PAGE HEADER-->
		<!-- BEGIN PAGE CONTENT-->
		<div class="row">
			<div class="col-md-12">
				<?php callback_submit();?>
				
				<div class="portlet light">
				    <div class="portlet-title tabbable-line">
					<div class="caption caption-md">
					    <i class="icon-globe theme-font-color hide"></i>
					    <span class="caption-subject theme-font-color bold uppercase">Pengajuan Pelatihan</span>
					</div>
					<ul class="nav nav-tabs">
					    <li class="active">
						<a href="#tab_submission_training_approval" data-toggle="tab">Set Pengajar </a>
					    </li>
					    <li>
						<a href="#tab_submission_training_history" data-toggle="tab">History </a>
					    </li>
					</ul>
				    </div>
				    
				    <div class="portlet-body">
					<div class="tab-content">
					    <div class="tab-pane active" id="tab_submission_training_approval">
						<!-- approval-->
						<div class="portlet light">
							<div class="portlet-title">
								<div class="caption">
									<i class="icon-speech"></i>
									<span class="caption-subject bold uppercase"> Set Pengajar</span>
								</div>
								<div class="actions">
									<a href="javascript:;" class="btn btn-circle btn-default btn-icon-only fullscreen"></a>
								</div>
							</div>
							<div class="portlet-body">
							  <form method='post' action='<?=site_url('fasilitator/management_training/elearning_proccess_instructor/')?>'>								
								<table class="table table-striped table-bordered table-hover standard-font table_approval_submission_training" id="">
									<thead>
										<tr>
											<th>
												Tanggal
											</th>
											<th>
												Nama Pelatihan
											</th>
											<th>
												Jenis Pelatihan
											</th>

											<th>
												Tanggal Pelatihan
											</th>
											<th>
												Narasumber
											</th>
											<th>
												Status
											</th>
											<th>
												Action
											</th>
										</tr>
									</thead>
									<tbody>										
										<?php
										
										$input = array(
										    'access_token' => '21'
										);
										
										$lms_api_url = $this->config->item('lms_api_url');		
										$response = curl_get($lms_api_url."/training", $input);
										
										if($response && $response['header_info']['http_code'] == 200){
										    $query = json_decode($response['body'],true);	
										}else{
											$query = array();
										}
										
										if($query)
										foreach($query as $row):
											?>
											<tr class="odd gradeX">
												<td><time class="timeago" datetime="<?=date('c',$row['created'])?>"><?=date("F j, Y, g:i a",$row['created']) ?></time>
												<input type="hidden" name="training_id[]" value="<?=$row['training_id']?>">
												</td>
												<td><?=$row['class_name']?></td>
												<td>
													<?=$row['type_training']['name']?>
												</td>


												<td><?=$row['training_date']?></td>
								<td style="width: 250px;">
									<div class="input-group" style="padding-right: 20px">
										<input 
										type="text" class="form-control input-sm" placeholder="Pilih Pengajar" readonly="">
										<input type='hidden' name='instructor_id[]' value='' class='set_instructor'>
										<span class="input-group-btn">
										<a 
										href="javascript:;" 
										class="btn default btn-sm select_instructor" 
										data-target="#select_instructor_modal"
										data-training_id = "<?=$row['training_id']?>"
										data-toggle="modal" >
										<i class="fa fa-user"></i> </a>
										</span>
									</div>
								</td>

												<td>													
													<?php
													if($row['published'] == 1){
														echo 'diterbitkan';
													}else{
														echo 'belum diterbitkan';
													}
														
													?>
												</td>
												<td>
													<div class="margin-bottom-5">
														<a href='' class='btn default btn-xs'>Detail</a>
													</div>
													
													
												</td>
											</tr>
										<?php endforeach; ?>
									</tbody>
								</table>
							      <div class="form-actions">
								<div class="row">
								    <div class="col-md-offset-2 col-md-10">
								      <button type="submit" class="btn blue" name='submit' value='save'>Proses</button>
								      
								      
								    </div>
								</div>
							      </div>
							  </form>
							</div>
						</div>
						<!-- ./approval-->
				
				
					    </div>
					    
					    <div class="tab-pane" id="tab_submission_training_history">
						<!-- history-->
						<div class="portlet light">
							<div class="portlet-title">
								<div class="caption">
									<i class="icon-speech"></i>
									<span class="caption-subject bold uppercase"> History</span>
								</div>
								<div class="actions">
									<a href="javascript:;" class="btn btn-circle btn-default btn-icon-only fullscreen"></a>
								</div>
							</div>
							<div class="portlet-body">
								<table class="table table-striped table-bordered table-hover standard-font table_approval_submission_training" id="">
									<thead>
										<tr>
											<th>
												Tanggal
											</th>
											<th>
												Lembaga Pelatihan/LPP
											</th>
											<th>
												Nama Pelatihan
											</th>

											<th>
												Tanggal Pelatihan
											</th>
											<th>
												Verifikasi
											</th>
											<th>
												Status
											</th>
											<th>
												Action
											</th>
										</tr>
									</thead>
									<tbody>
										<?php

										$query = $this->training_db->get_all(array(
											'verified_submission_training' => 1,
											'facilitator_user_id' => $this->connect_auth->get_me()['user_id'],
											'select_instructor_finished' => 1
										));
										
										if($query)
										foreach($query as $row):

											?>
											<tr class="odd gradeX">
												<td><time class="timeago" datetime="<?=date('c',$row['created'])?>"><?=date("F j, Y, g:i a",$row['created']) ?></time></td>
												<td><?=$row['user_submission']['training_institute']?></td>
												<td>
													<?=$row['type_training']['name'].' / '.$row['training_name']['name']?>
												</td>


												<td><?=$row['training_date']?></td>
												<td>
													<?php
													$status = array();


													if(isset($row['verified_submission_training']))
													switch($row['verified_submission_training']){
														case 0 : $status = array('name' => 'Menunggu Verifikasi', 'label' => 'label-warning'); break;
														case 1 : $status = array('name' => 'Disetujui', 'label' => 'label-success'); break;
														case -1 : $status = array('name' => 'Klarifikasi', 'label' => 'label-warning'); break;
														case -2 : $status = array('name' => 'Tolak', 'label' => 'label-danger'); break;
														default : $status = array('name' => 'Menunggu Verifikasi', 'label' => 'label-warning'); break;
													}
													?>
													<span class="label label-sm <?=$status['label']?>"> <?=$status['name']?> </span>
												</td>

												<td>
													<?php
													$status = array();

													if(isset($row['draft'])){
														$status = array('name' => 'draft', 
														'label' => 'label-warning');
													}

													if(isset($row['verified_submission_training']))
													switch($row['verified_submission_training']){
														case 0 : 
														case -1 : 
														case -2 : $status = array('name' => 'Tahap Pengajuan',
														'label' => 'label-warning'); break;
														case 1 : $status = array('name' => 'Tahap Persiapan',
														'label' => 'label-warning'); break;
													}
													?>
													
													<div class="margin-bottom-10">
														<span class="label label-sm
														<?=$status['label']?>"><?=$status['name']?></span>
													</div>
													<?php 
													if(isset($row['rescheduled']) && $row['rescheduled']):
														$status = array('name' => 'Reschedule', 
														'label' => 'label-default');
													?>

													<div class="margin-bottom-10">
														<span class="label label-sm 
														<?=$status['label']?>"><?=$status['name']?></span>
													</div>
												<?php endif;?>

													<?php 
													if(isset($row['facilitator_user_id'])):
														$status = array('name' => 'Pemilihan Pengajar', 
														'label' => 'label-default');
													?>
													<div class="margin-bottom-10">
														<span class="label label-sm 
														<?=$status['label']?>"><?=$status['name']?></span>
													</div>
													<?php endif;?>

												</td>
												<td>
													<div class="margin-bottom-5">
														<button 
														type="button" 
														class="btn default btn-xs purple review_training_submission"
														data-url-submit="<?=site_url('lpp/management_training/training_post/'.$row['_id']->{'$id'})?>" 
														data-toggle="modal" 
														data-training_id="<?=$row['_id']->{'$id'}?>" 
														data-target="#review_training_submission">
															<i class="fa fa-eye"></i> Review 
														</button>
													</div>
													
													<div class="margin-bottom-5">
														<a href='<?=site_url('fasilitator/management_training/training/detail/'.$row['_id']->{'$id'})?>' class='btn default btn-xs'>Detail</a>
													</div>
													
													
												</td>
											</tr>
										<?php endforeach; ?>
									</tbody>
								</table>
								
							</div>
						</div>
						<!-- ./history-->
					    </div>
					    
					</div>  
				    </div>
				    
				</div>
				
				
				
				
				
				
			</div>
		</div>
		<!-- END PAGE CONTENT-->
	</div>
</div> 
