<?php
    $page = $this->uri->segment(3);
    $token = $this->connect_auth->get_access_token();
?>
<div class="page-content-wrapper">
	<div class="page-content">
		<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
		<!-- /.modal -->
		<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
		<!-- BEGIN PAGE HEADER-->
		<!-- BEGIN PAGE HEAD -->
		<div class="page-head">
			<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
				<h1><?=$title_page?></h1>
			</div>
			<!-- END PAGE TITLE -->
		</div>
		<!-- END PAGE HEAD -->
		<!-- BEGIN PAGE BREADCRUMB -->
		<ul class="page-breadcrumb breadcrumb">
			<li>
				<a href="#">Approval</a>
				<i class="fa fa-circle"></i>
			</li>
			
			<li>
				<a href="#"><?=$title_page?></a>
			</li>
		</ul>
		<!-- END PAGE BREADCRUMB -->
		<!-- END PAGE HEADER-->
		<!-- BEGIN PAGE CONTENT-->
		<div class="row">
			<div class="col-md-12">
				<?php callback_submit();?>
				
				<!-- BEGIN EXAMPLE TABLE PORTLET-->
				<div class="portlet box grey-cascade">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-globe"></i><?=$title_page?>
						</div>
						<div class="tools">
							<a href="javascript:;" class="collapse">
							</a>
						</div>
					</div>
					<div class="portlet-body">
						
						
						<table class="table table-striped table-bordered table-hover" id="table_approval_institution">
							<thead>
								<tr>
									<th>
										 Lembaga Pelatihan/LPP
									</th>
									<th>
										 Email
									</th>
									
									<th>
										 Join
									</th>
									<th>
										 Last Update
									</th>
									<th>
										 Verifikasi
									</th>
									<th>
										 Status
									</th>
									<th>
										 Action
									</th>
								</tr>
							</thead>
						<tbody>
						<?php
						    $entity = 'lpp';
						    $response = call_api_get($this->config->item('connect_api_url').'/user/all/'.$token.'/'.$entity);
						    $response_body = json_decode($response['body'], true);
						    
						    if($response['header_info']['http_code'] == 200):
						    foreach($response_body['data'] as $row):
							if($row['verified_email']):
						?>
						<tr class="odd gradeX">
						    <td><?=$row['training_institute']?></td>
						    <td>
							<a href="mailto:<?=$row['email']?>"><?=$row['email']?> </a>
						    </td>
						    
						    <td><time class="timeago" datetime="<?=date('c',$row['created'])?>"><?=date("F j, Y, g:i a",$row['created']) ?></time></td>
						    <td><time class="timeago" datetime="<?=date('c',$row['last_update']['time'])?>"><?=date("F j, Y, g:i a",$row['last_update']['time']) ?></time></td>
						    <td>
							<?php
							    
							    if(isset($row['verified_institute']))
							    switch($row['verified_institute']){
								case 0 : $label = 'label-warning'; $msg = 'Menunggu Verifikasi'; break;
								case 1 : $label = 'label-success'; $msg = 'Disetujui'; break;
								case -1 : $label = 'label-warning'; $msg = 'Tunda'; break;
								case -2 : $label = 'label-danger'; $msg = 'Tolak'; break;
								default : $label = 'label-warning'; $msg = 'Menunggu Verifikasi'; break;
							    }
							?>
							<span class="label label-sm <?=(isset($row['verified_institute'])  ? $label : 'label-warning')?>"> <?=(isset($row['verified_institute'])  ? $msg : 'Menunggu Verifikasi')?> </span>
						    </td>
						    
						    <td>
							<span class="label label-sm <?=($row['activated'] ? 'label-success' : ($row['verified_email'] ? 'label-danger' : 'label-warning'))?>"> <?=($row['activated'] ? 'Aktif' : ($row['verified_email'] ? 'Non-Aktif' : 'Menunggu verifikasi email'))?> </span>
						    </td>
						    <td>
							<div class="btn-group">
							    <?php
							    /*
							    <button type="button" class="btn btn-default" onclick="location.href='<?=site_url('superadmin/management_user/'.$page.'/edit/'.$row['user_id'])?>'">Edit</button>
							    */
							    ?>
							    <button type="button" class="btn btn-default delete-user" data-url-delete="<?=site_url('admin/evaluation/training/rekap')?>" data-toggle="modal" data-target="#delete">Evaluasi</button>

							    <button type="button" class="btn btn-default status-user" data-url-active="<?=site_url('superadmin/management_user/'.$page.'/status/'.$row['user_id'].'/?activated=1')?>" data-url-nonactive="<?=site_url('superadmin/management_user/'.$page.'/status/'.$row['user_id'].'/?activated=0')?>" data-toggle="modal" data-target="#status">Status</button>
							    
							    <button type="button" class="btn btn-default delete-user" data-url-delete="<?=site_url('superadmin/management_user/'.$page.'/delete/'.$row['user_id'])?>" data-toggle="modal" data-target="#delete">Delete</button>
							</div>
						    </td>
						</tr>
						<?php endif; endforeach; endif;?>
						
						</tbody>
						</table>
					</div>
				</div>
				<!-- END EXAMPLE TABLE PORTLET-->
			</div>
		</div>
		<!-- END PAGE CONTENT-->
	</div>
</div>