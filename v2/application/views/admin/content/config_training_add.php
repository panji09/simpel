<?php
    $page = $this->uri->segment(3);
    $state = $this->uri->segment(4);
    $query = $user = array();
    if($state == 'edit'){
	$query = $this->config_training_db->get($config_training_id);
	if($query)
	    $content = $query[0];
    }
?>
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<?php $this->load->view('admin/inc/modal');?>
			<!-- /.modal -->
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE HEADER-->
			<!-- BEGIN PAGE HEAD -->
			<div class="page-head">
				<!-- BEGIN PAGE TITLE -->
				<div class="page-title">
					<h1><?=$title_content?> <small><?=$title_page?></small></h1>
				</div>
				<!-- END PAGE TITLE -->
				<!-- BEGIN PAGE TOOLBAR -->
				
				<!-- END PAGE TOOLBAR -->
			</div>
			<!-- END PAGE HEAD -->
			<!-- BEGIN PAGE BREADCRUMB -->
			<ul class="page-breadcrumb breadcrumb">
				<li>
					<a href="<?=site_url('superadmin/pages')?>">Halaman</a>
					<i class="fa fa-circle"></i>
				</li>
				<li>
					<a href="<?=site_url('superadmin/pages/'.$page)?>"><?=$title_page?></a>
					<i class="fa fa-circle"></i>
				</li>
				<li>
					<a href="#"><?=$title_content?></a>
				</li>
			</ul>
			<!-- END PAGE BREADCRUMB -->
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					
					
					<div class="portlet box blue" id="form_wizard_1">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-gift"></i> <?=$title_content?>
							</div>
							<div class="tools hidden-xs">
								<a href="javascript:;" class="collapse">
								</a>
								
							</div>
						</div>
						<div class="portlet-body form">
							<form action="<?=site_url('superadmin/setting/config_training_post/'.$config_training_id)?>" id="form_sample_3" method='post' class="form-horizontal">
								<div class="form-body">
									<h3 class="form-section">Lengkapi kolom dibawah ini</h3>
									<div class="alert alert-danger display-hide">
										<button class="close" data-close="alert"></button>
										You have some form errors. Please check below.
									</div>
									<div class="alert alert-success display-hide">
										<button class="close" data-close="alert"></button>
										Your form validation is successful!
									</div>
									
									<div class="form-group">
										<label class="control-label col-md-3">Jenis Pelatihan / Diklat <span class="required">
										* </span>
										</label>
										<div class="col-md-4">
											<select id='type_training' name='type_training' class="select2me form-control">
											  <option value=''>-Pilih-</option>
											  <?php
											      $query = $this->type_training_db->get_all();
											      foreach($query as $row):
											  ?>
											      <option value='<?=$row['_id']->{'$id'}?>' <?=(isset($content['type_training']['id']) && $content['type_training']['id'] == $row['_id']->{'$id'} ? 'selected' : '' )?>><?=$row['name']?></option>
											  <?php endforeach;?>
											</select>
										</div>
										
									</div>
									
									<div class="form-group">
										<label class="control-label col-md-3">Nama Pelatihan <span class="required">
										* </span>
										</label>
										<div class="col-md-4">
											<input value='<?=(isset($content['name']) ? $content['name'] : '' )?>' type="text" name="name" data-required="1" class="form-control"/>
										</div>
										
									</div>
									
									
									<div class="form-group">
										<label class="control-label col-md-3">Jumlah Jam Pelajaran (JP) <span class="required">
										* </span>
										</label>
										<div class="col-md-4">
											<input value='<?=(isset($content['hour_leasson']) ? $content['hour_leasson'] : '' )?>' type="text" name="hour_leasson" data-required="1" class="form-control"/>
										</div>
										
									</div>
									
									<div class="form-group last">
										<label class="control-label col-md-3">Deskripsi Persyaratan Peserta <span class="required">
										* </span>
										</label>
										<div class="col-md-8">
											<textarea 
											class=" form-control wysiwyg-content" 
											name="requirement_description" 
											rows="6" 
											data-error-container="#editor2_error" 
											>
											<?=(isset($content['requirement_description']) ? $content['requirement_description'] : '' )?>
											</textarea>
											<div id="editor2_error">
											</div>
										</div>
									</div>
									
									<div class="form-group last">
										<label class="control-label col-md-3">Deskripsi Singkat Pelatihan <span class="required">
										* </span>
										</label>
										<div class="col-md-8">
											<textarea class="wysiwyg-content form-control" name="training_description_short" rows="6" data-error-container="#editor2_error" ><?=(isset($content['training_description_short']) ? $content['training_description_short'] : '' )?></textarea>
											<div id="editor2_error">
											</div>
										</div>
									</div>
									
									<div class="form-group last">
										<label class="control-label col-md-3">Deskripsi Pelatihan <span class="required">
										* </span>
										</label>
										<div class="col-md-8">
											<textarea class="wysiwyg-content form-control" name="training_description" rows="6" data-error-container="#editor2_error" ><?=(isset($content['training_description']) ? $content['training_description'] : '' )?></textarea>
											<div id="editor2_error">
											</div>
										</div>
									</div>
									
									<div class="form-group">
										<label class="control-label col-md-3">Upload Bahan Ajar <span class="required">
										* </span>
										</label>
										<div class="col-md-4">
											<div class="input-group">
												<input type="text" class="form-control " placeholder="klik upload button" name="teaching_material_upload" id="teaching_material_upload" value="<?=(isset($content['teaching_material_upload']) ? $content['teaching_material_upload'] : '')?>" readonly>
												<span class="change_file input-group-btn">
													<button class="btn btn-danger" type="button" id="mail_submission_handler">Upload</button>
												</span>
												
												
											</div>
											
											
											
										</div>
										
									</div>
									<div class="form-group">
										<label class="control-label col-md-3">Terbitkan <span class="required">
										* </span>
										</label>
										<div class="col-md-4">
											<div class="checkbox-list" data-error-container="#form_2_services_error">
												<label>
												<input type="checkbox" value="1" name="published" <?=(isset($content['published']) && $content['published'] ? 'checked' : '' )?>/> Ya </label>
												
											</div>
											
											<div id="form_2_services_error">
											</div>
										</div>
									</div>
									
									<h4 class="form-section">Pengaturan</h4>
									<div class="form-group">
										<label class="control-label col-md-3">Memiliki Sertifikat Ahli Pengadaan? <span class="required">
										* </span>
										</label>
										<div class="col-md-4">
											<div class="checkbox-list" data-error-container="#form_2_services_error">
												<label>
												<input type="checkbox" value="1" name="requirement_certificate" <?=(isset($content['requirement_certificate']) && $content['requirement_certificate'] ? 'checked' : '' )?>/> Ya </label>
												
											</div>
											
											<div id="form_2_services_error">
											</div>
										</div>
									</div>
									
									<div class="form-group">
										<label class="control-label col-md-3">Persyaratan Pendidikan? <span class="required">
										* </span>
										</label>
										<div class="col-md-4">
											<div class="checkbox-list" data-error-container="#form_2_services_error">
												<?php
												    $query = file_get_contents($this->config->item('connect_api_url').'/educational_level');
												    $educational_level = array();
												    if($query){
													$educational_level = json_decode($query,true);
												    }
												    
												    if($educational_level)
												    foreach($educational_level as $row):
												?>
												    <label>
												<input type="checkbox" value="<?=$row?>" <?=(isset($content['requirement_educational']) && in_array($row, $content['requirement_educational']) ? 'checked' : '' )?> name="requirement_educational[]" /> <?=$row?> </label>
												<?php
												endforeach;
												?>
												
											</div>
											
											<div id="form_2_services_error">
											</div>
										</div>
									</div>
									
									<div class="form-group">
										<label class="control-label col-md-3">Mempunyai Surat Tugas dari Instansi/minimal Pejabat Es. II untuk mengikuti Pelatihan? <span class="required">
										* </span>
										</label>
										<div class="col-md-4">
											<div class="checkbox-list" data-error-container="#form_2_services_error">
												<label>
												<input type="checkbox" value="1" name="requirement_letter_assignment" <?=(isset($content['requirement_letter_assignment']) && $content['requirement_letter_assignment'] ? 'checked' : '' )?>/> Ya </label>
												
											</div>
											
											<div id="form_2_services_error">
											</div>
										</div>
									</div>
									
									
									<div class="form-group">
										<label class="control-label col-md-3">Aktifkan Upload Dokumen Pendukung? <span class="required">
										* </span>
										</label>
										<div class="col-md-4">
											<div class="checkbox-list" data-error-container="#form_2_services_error">
												<label>
												<input type="checkbox" value="1" name="enable_support_document" <?=(isset($content['enable_support_document']) && $content['enable_support_document'] ? 'checked' : '' )?>/> Ya </label>
												
											</div>
											
											<div id="form_2_services_error">
											</div>
										</div>
									</div>
									
								</div>
								<div class="form-actions">
									<div class="row">
										<div class="col-md-offset-3 col-md-9">
											<button type="submit" class="btn green">Simpan</button>
											<a href='<?=($this->session->userdata('redirect') ? $this->session->userdata('redirect') : site_url('superadmin/setting/config_training') )?>' class="btn default">Batal</a>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>