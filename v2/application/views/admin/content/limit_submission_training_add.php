 
<?php
    $page = $this->uri->segment(3);
    $state = $this->uri->segment(4);
    
    $limit_submission = (isset($this->setting_db->get('limit_submission_training')[0]['limit']) ? $this->setting_db->get('limit_submission_training')[0]['limit'] : 0);
?>
<div class="page-content-wrapper">
	<div class="page-content">
		<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
		<?php $this->load->view('admin/inc/modal');?>
		<!-- /.modal -->
		<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
		<!-- BEGIN PAGE HEADER-->
		<!-- BEGIN PAGE HEAD -->
		<div class="page-head">
			<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
				<h1><?=$title_page?> <small><?=$title_content?></small></h1>
			</div>
			<!-- END PAGE TITLE -->
		</div>
		<!-- END PAGE HEAD -->
		<!-- BEGIN PAGE BREADCRUMB -->
		<ul class="page-breadcrumb breadcrumb">
			<li>
				<a href="#">Halaman</a>
				<i class="fa fa-circle"></i>
			</li>
			<li>
				<a href="#"><?=$title_page?></a>
				<i class="fa fa-circle"></i>
			</li>
			<li>
				<a href="#"><?=$title_content?></a>
			</li>
		</ul>
		<!-- END PAGE BREADCRUMB -->
		<!-- END PAGE HEADER-->

		<!-- BEGIN PAGE CONTENT-->
		<div class="row">
			<?php callback_submit();?>
			<form action="<?=site_url('superadmin/setting/limit_submission_training_post')?>" id="" method='post'>
				<div class="col-md-8">
					<div class="portlet light">
						<div class="portlet-body">
							<div class="margin-bottom-20">						
								<h4>Set Limit Pengajuan Pelatihan oleh LPP</h4>
								<input type="text" class="form-control" placeholder="masukan jumlah hari" value='<?=$limit_submission?>' name="limit" data-required="1">
							</div>
							
							
							
						</div>
					</div>
										
					
				</div>
				
				<!-- BEGIN EDITOR SIDEBAR -->
				<div class="col-md-4 editor-sidebar">
					<!-- BEGIN SIDEBAR ROW -->
					
					
					<div class="row">
						<div class="col-md-12 hidden-sm hidden-xs">
							<!-- Begin: publish news -->
							<div class="portlet light">
								<div class="portlet-title">
									<div class="caption">
										<i class="icon-speech font-blue-sharp"></i>
										<span class="caption-subject font-blue-sharp bold uppercase">Action</span>
									</div>
								</div>
								<div class="portlet-body">										
									
									<div class="publish-container">
									    <button type="submit" id="btn_save" class="btn btn-success btn-block">
										    <i class="fa fa-folder-open"></i> Simpan 
									    </button>
										
									</div>
									<p class="text-muted">
									    <cite>Klik <strong>button</strong> untuk menyimpan pengaturan</cite>
									</p>
								</div>
							</div>
							<!-- End: publish news -->
						</div>
					</div>
					<!-- END SIDEBAR ROW -->					
				
				</div>
				<!-- END EDITOR SIDEBAR -->
			</form>			
		</div>
		<!-- END PAGE CONTENT-->


		<!-- BEGIN HIDDEN SECTION -->
		<div class='hide'>
		    <div id='template_section'>
				<!-- BEGIN Portlet SECTION-->
									
					<?php //$this->load->view('admin/inc/template_section_todolist', array('set_template' => true))?>
					
				<!-- END Portlet SECTION-->
				
			</div>
		</div>
		<!-- END HIDDEN SECTION -->


		<!-- BEGIN HIDDEN SEGMENT MODULE-->
		<div class="hidden">
			<div id="template_segment">
				<!-- BEGIN Portlet SEGMENT-->
				<?php $this->load->view('admin/inc/template_segment_question_todolist')?>
				<!-- END Portlet SEGMENT-->		
			</div>
		</div>
		<!-- END HIDDEN SEGMENT MODULE -->

		<!-- BEGIN HIDDEN SEGMENT QUESTION-->
		<div class="hidden">
			<div id="template_segment_question">
				<!-- BEGIN Portlet SEGMENT-->
				<?php $this->load->view('admin/inc/template_segment_question')?>
				<!-- END Portlet SEGMENT-->		
			</div>
		</div>
		<!-- END HIDDEN SEGMENT QUESTION -->
		
		<!-- BEGIN HIDDEN SEGMENT QUESTION  -->
		<div class="hidden">
			<tr class="question_row">
				<td>
					1
				</td>
				<td>
					Pilihan Ganda
				</td>
				<td>
					Modul Pelatihan Yes
				</td>
				<td>
					A
				</td>
				<td>
					4
				</td>
				<td>
					<a href="javascript:;" class="btn default btn-xs purple">
					<i class="fa fa-eye"></i> View </a>
				</td>
			</tr>
		</div>
		<!-- END HIDDEN SEGMENT QUESTION  -->
	</div>
</div>