<?php  
	$page = $this->uri->segment(4);
	$query = $user = array();
  	if($page == 'edit'){
  	
	    $url = $this->config->item('connect_api_url').'/user/index/'.$this->connect_auth->get_access_token().'/'.$user_id;
	    $response = call_api_get($url);
	    if($response && $response['header_info']['http_code'] == 200){
		$user = json_decode($response['body'], true);
		
	    }
  	}
?>
<div class="page-content-wrapper">
	<div class="page-content">
		<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
		<!-- /.modal -->
		<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
		<!-- BEGIN PAGE HEADER-->
		<!-- BEGIN PAGE HEAD -->
		<div class="page-head">
			<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
				<h1><?=$title_content?> <small><?=$title_page?></small></h1>
			</div>
			<!-- END PAGE TITLE -->
		</div>
		<!-- END PAGE HEAD -->
		<!-- BEGIN PAGE BREADCRUMB -->
		<ul class="page-breadcrumb breadcrumb">
			<li>
				<a href="#">Management User</a>
				<i class="fa fa-circle"></i>
			</li>
			<li>
				<a href="<?=site_url('admin/management_user/user_admin')?>"><?=$title_page?></a>
				<i class="fa fa-circle"></i>
			</li>
			<li>
				<a href="#"><?=$title_content?></a>
			</li>
		</ul>
		<!-- END PAGE BREADCRUMB -->
		<!-- END PAGE HEADER-->
		<!-- BEGIN PAGE CONTENT-->
		<div class="row">
			<div class="col-md-12">
				<div class="portlet box blue" id="form_wizard_1">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-gift"></i> <?=$title_content?> - <span class="step-title">
							Step 1 of 3 </span>
						</div>
						<div class="tools hidden-xs">
							<a href="javascript:;" class="collapse">
							</a>
							
						</div>
					</div>
					<div class="portlet-body form">
						<form action="<?=site_url('superadmin/management_user/post_user_admin')?>/<?=$entity?>/<?=(isset($user_id) ? $user_id : '')?>" class="form-horizontal" id="submit_form" method="POST">
							<div class="form-wizard">
								<div class="form-body">
									<ul class="nav nav-pills nav-justified steps">
										<li>
											<a href="#tab1" data-toggle="tab" class="step">
											<span class="number">
											1 </span>
											<span class="desc">
											<i class="fa fa-check"></i> Profil </span>
											</a>
										</li>
										<li>
											<a href="#tab2" data-toggle="tab" class="step">
											<span class="number">
											2 </span>
											<span class="desc">
											<i class="fa fa-check"></i> Akun </span>
											</a>
										</li>
										
										<li>
											<a href="#tab3" data-toggle="tab" class="step">
											<span class="number">
											3 </span>
											<span class="desc">
											<i class="fa fa-check"></i> Konfirmasi </span>
											</a>
										</li>
									</ul>
									<div id="bar" class="progress progress-striped" role="progressbar">
										<div class="progress-bar progress-bar-success">
										</div>
									</div>
									<div class="tab-content">
										<div class="alert alert-danger display-none">
											<button class="close" data-dismiss="alert"></button>
											Ups, sepertinya ada error. Silahkan cek dibawah ini.
										</div>
										<div class="alert alert-success display-none">
											<button class="close" data-dismiss="alert"></button>
											Sukses!
										</div>
										
										<div class="tab-pane active" id="tab1">
											<h3 class="block">Lengkapi Profile Kamu</h3>
											<div class="form-group">
												
												<label class="control-label col-md-3">Foto 
												</label>
												<div class="col-md-4">
													<div class="row">
													    <div class="col-xs-6 col-md-3">
													      <a href="#" class="thumbnail" style="width: 200px; height: auto;">
														<img id='avatar' src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&text=pilih foto" data-default='http://www.placehold.it/200x150/EFEFEF/AAAAAA&text=pilih foto' alt="...">
														<input type='hidden' value='<?=(isset($user['photo']) ? $user['photo'] : '')?>' name='photo'>
													      </a>
													    </div>
													    
													  </div>
													  <input id='clear_avatar' type='button' class='btn' value='Hapus'>
												</div>
											</div>
											
											<div class="form-group">
												<label class="control-label col-md-3">Nama <span class="required">
												* </span>
												</label>
												<div class="col-md-4">
													<input type="text" class="form-control" name="fullname" value="<?=(isset($user['fullname']) ? $user['fullname'] : '')?>"/>
													<span class="help-block">
													Isi nama lengkap</span>
												</div>
											</div>
											
											<div class="form-group">
												<label class="control-label col-md-3">Tempat Lahir <span class="required">
												* </span>
												</label>
												<div class="col-md-4">
													<input type="text" class="form-control" name="birth_place" value='<?=(isset($user['birth_place']) ? $user['birth_place'] : '')?>'/>
													<span class="help-block">
													Isi tempat lahir</span>
												</div>
											</div>
											
											<div class="form-group">
												<label class="control-label col-md-3">Tanggal Lahir <span class="required">
												* </span>
												</label>
												<div class="col-md-4">
													<div class='input-group date' id='datetimepicker1'>
													    <input type='text' class="form-control" name='birth_date' value='<?=(isset($user['birth_date']) ? $user['birth_date'] : '')?>' />
													    <span class="input-group-addon">
														<span class="glyphicon glyphicon-calendar"></span>
													    </span>
													</div>
													<div id="form_birth_date_error"></div>
													<span class="help-block">
													Isi tanggal lahir</span>
												</div>
											</div>
											
											<div class="form-group">
												<label class="control-label col-md-3">No. Telp / HP <span class="required">
												* </span>
												</label>
												<div class="col-md-4">
													<input type="text" class="form-control" name="phone" value='<?=(isset($user['phone']) ? $user['phone'] : '')?>'/>
													<span class="help-block">
													Isi no. telp</span>
												</div>
											</div>
											<div class="form-group">
												<label class="control-label col-md-3">Jenis Kelamin <span class="required">
												* </span>
												</label>
												<div class="col-md-4">
													<div class="radio-list">
														<label>
														<input type="radio" name="gender" value="M" data-title="Laki-laki" <?=(isset($user['gender']) && $user['gender'] == 'M'  ? 'checked' : '')?>/>
														Laki-laki </label>
														<label>
														<input type="radio" name="gender" value="F" data-title="Perempuan" <?=(isset($user['gender']) && $user['gender'] == 'F'  ? 'checked' : '')?>/>
														Perempuan </label>
													</div>
													
													
													<div id="form_gender_error">
													</div>
												</div>
											</div>
											
										</div>
										
										<div class="tab-pane" id="tab2">
											<h3 class="block">Silahkan Lengkapi Akun Kamu</h3>
											<div class="form-group">
												<label class="control-label col-md-3">Username <span class="required">
												* </span>
												</label>
												<div class="col-md-4">
													<input type="text" class="form-control" name="username" value='<?=(isset($user['username']) ? $user['username'] : '')?>'/>
													<span class="help-block">
													Isi username min 5 karakter. </span>
												</div>
											</div>
											<div class="form-group">
												<label class="control-label col-md-3">Password <span class="required">
												* </span>
												</label>
												<div class="col-md-4">
													<input type="password" class="form-control" name="password" value='' id="submit_form_password"/>
													<span class="help-block">
													Isi kata kunci min 7 karakter. </span>
												</div>
											</div>
											<div class="form-group">
												<label class="control-label col-md-3">Konfirmasi Password <span class="required">
												* </span>
												</label>
												<div class="col-md-4">
													<input type="password" class="form-control" name="rpassword" value=''/>
													<span class="help-block">
													Ulangi isi kata kunci. </span>
												</div>
											</div>
											<div class="form-group">
												<label class="control-label col-md-3">Email <span class="required">
												* </span>
												</label>
												<div class="col-md-4">
													<input type="text" class="form-control" name="email" value='<?=(isset($user['email']) ? $user['email'] : '')?>'/>
													<span class="help-block">
													Isi email.</span>
												</div>
											</div>
											<div class="form-group">
												<label class="control-label col-md-3">Aktif 
												</label>
												<div class="col-md-4">
													<div class="checkbox">
													  <label>
													    <input type="checkbox" name='activated' value='1' <?=(isset($user['activated']) && $user['activated'] ? 'checked' : '')?> Aktif
													  </label>
													</div>
												</div>
											</div>
											
										</div>
										
										
										<div class="tab-pane" id="tab3">
											<h3 class="block">Konfirmasi data kamu</h3>
											
											<h4 class="form-section">Akun</h4>
											<div class="form-group">
												<label class="control-label col-md-3">Username:</label>
												<div class="col-md-4">
													<p class="form-control-static" data-display="username">
													</p>
												</div>
											</div>
											<div class="form-group">
												<label class="control-label col-md-3">Email:</label>
												<div class="col-md-4">
													<p class="form-control-static" data-display="email">
													</p>
												</div>
											</div>
											
											<h4 class="form-section">Profil</h4>
											<div class="form-group">
												<label class="control-label col-md-3">Nama:</label>
												<div class="col-md-4">
													<p class="form-control-static" data-display="fullname">
													</p>
												</div>
											</div>
											<div class="form-group">
												<label class="control-label col-md-3">No. Telp:</label>
												<div class="col-md-4">
													<p class="form-control-static" data-display="phone">
													</p>
												</div>
											</div>
											<div class="form-group">
												<label class="control-label col-md-3">Tempat Lahir:</label>
												<div class="col-md-4">
													<p class="form-control-static" data-display="birth_place">
													</p>
												</div>
											</div>
											<div class="form-group">
												<label class="control-label col-md-3">Tanggal Lahir:</label>
												<div class="col-md-4">
													<p class="form-control-static" data-display="birth_date">
													</p>
												</div>
											</div>
											<div class="form-group">
												<label class="control-label col-md-3">Gender:</label>
												<div class="col-md-4">
													<p class="form-control-static" data-display="gender">
													</p>
												</div>
											</div>
											
											
										</div>
									</div>
								</div>
								<div class="form-actions">
									<div class="row">
										<div class="col-md-offset-3 col-md-9">
											<a href="javascript:;" class="btn default button-previous">
											<i class="m-icon-swapleft"></i> Kembali </a>
											<a href="javascript:;" class="btn blue button-next">
											Lanjut <i class="m-icon-swapright m-icon-white"></i>
											</a>
											<button type='submit' class="btn green button-submit">
											Proses <i class="m-icon-swapright m-icon-white"></i>
											</button>
										</div>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<!-- END PAGE CONTENT-->
	</div>
</div>