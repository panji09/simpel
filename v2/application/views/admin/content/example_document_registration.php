<?php
    $page = $this->uri->segment(3);
    
    $query = $user = array();
    if($page){
	$query = $this->setting_db->get($setting_id);
	if($query)
	    $content = $query[0];
    }
?>

<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<div class="page-content">
		<!-- BEGIN PAGE HEADER-->
		<!-- BEGIN PAGE HEAD -->
		<div class="page-head">
			<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
				<h1><?=$title_page?> <small>tambah & edit contoh dukumen registrasi</small></h1>
			</div>
			<!-- END PAGE TITLE -->
		</div>
		<!-- END PAGE HEAD -->
		<!-- BEGIN PAGE BREADCRUMB -->
		<ul class="page-breadcrumb breadcrumb">
			<li>
				<a href="index.html">Dashboard</a>
				<i class="fa fa-circle"></i>
			</li>
			<li>
				<a href="#">Halaman</a>
				<i class="fa fa-circle"></i>
			</li>
			<li>
				<a href="<?=site_url('superadmin/pages/'.$page)?>"><?=$title_page?></a>
			</li>
		</ul>
		<!-- END PAGE BREADCRUMB -->
		<!-- END PAGE HEADER-->
		
		<!-- BEGIN PAGE CONTENT-->
		<div class="row">
			<div class="col-md-12">
				<form class="form-horizontal form-row-seperated" action="<?=site_url('superadmin/setting/example_document_registration_post')?>" method='post'>
					<div class="portlet light">
						<div class="portlet-title">
							<div class="caption">
								<i class="icon-picture font-blue-sharp"></i>
								<span class="caption-subject font-blue-sharp bold uppercase">
								<?=$title_page?> </span>
								<span class="caption-helper">manajemen <?=$title_page?></span>
							</div>
							<div class="actions btn-set">
								<button class="btn green-haze btn-circle"><i class="fa fa-check"></i> Save</button>
							</div>
						</div>
						<div class="portlet-body">
							<?php callback_submit();?>
							
							<table class="table table-bordered table-hover">
								<thead>
									<tr role="row" class="heading">
										<th width="15%">
											 Dokumen
										</th>
										<th width="45%">
											 Link
										</th>
										
										<th width="10%">
										</th>
									</tr>
								</thead>
								<tbody>
									<?php
										$input_data = array(
															array(
																  'name' => 'Sistem Manajemen Mutu',
																  'id' => 'quality_management_example'
																  ),
															array(
																  'name' => 'Standar Pelatihan',
																  'id' => 'standart_training_example'
																  ),
															array(
																  'name' => 'Pernyataan Komitmen',
																  'id' => 'commitment_example'
																  ),
															
															);
									
										foreach($input_data as $row):
									?>
									<tr>
										<td>
											<?=$row['name']?>											
										</td>
										
										<td>
											<div class="input-group file_from_upload" style="text-align:left">
												<input type="text" class="form-control" name="example_doc[<?=$row['id']?>]" id='file_container' value='<?=(isset($content['example_doc'][$row['id']]) ? $content['example_doc'][$row['id']] : '')?>' readonly>
												<span class="input-group-btn change_file">
													<a href="javascript:;" class="btn red-haze " >
														<i class="fa fa-files-o"></i> Pilih </a>
												</span>
											</div>
											
										</td>
										<td class='view_info'>
											<a target='_blank' href="<?=(isset($content['example_doc'][$row['id']]) && $content['example_doc'][$row['id']]!='' ? $content['example_doc'][$row['id']] : '#' )?>" class="btn btn-sm default  " ><i class="fa fa-search"></i> View</a>
										</td>
									</tr>
									<?php endforeach;?>
								</tbody>
							</table>
							
						</div>
					</div>
				</form>
			</div>
		</div>
		<!-- END PAGE CONTENT-->
	</div>
</div>
<!-- END CONTENT -->