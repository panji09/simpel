<?php
    $page = $this->uri->segment(3);
    $token = $this->connect_auth->get_access_token();
?>
<div class="page-content-wrapper">
	<div class="page-content">
		<!-- BEGIN PAGE HEADER-->
		<!-- BEGIN PAGE HEAD -->
		<div class="page-head">
			<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
				<h1><?=$title_page?></h1>
			</div>
			<!-- END PAGE TITLE -->
		</div>
		<!-- END PAGE HEAD -->
		<!-- BEGIN PAGE BREADCRUMB -->
		<ul class="page-breadcrumb breadcrumb">
			<li>
				<a href="#">Approval</a>
				<i class="fa fa-circle"></i>
			</li>
			
			<li>
				<a href="#"><?=$title_page?></a>
			</li>
		</ul>
		<!-- END PAGE BREADCRUMB -->
		<!-- END PAGE HEADER-->
		<!-- BEGIN PAGE CONTENT-->
		
	
		
		<div class="row">
			<div class="col-md-12">
				<?php callback_submit();?>
				
				
				<!-- BEGIN Portlet PORTLET-->
				<div class="portlet light">
				    <div class="portlet-title tabbable-line">
					<div class="caption caption-md">
					    <i class="icon-globe theme-font-color hide"></i>
					    <span class="caption-subject theme-font-color bold uppercase">Approval Pengajuan Pengajar</span>
					</div>
					<ul class="nav nav-tabs">
					    <li class="active">
						<a href="#tab_instructor_approval" data-toggle="tab">Menunggu Approval </a>
					    </li>
					    <li>
						<a href="#tab_instructor_history" data-toggle="tab">Riwayat </a>
					    </li>
					</ul>
				    </div>
				    
				    <div class="portlet-body">
					<div class="tab-content">
					    <div class="tab-pane active" id="tab_instructor_approval">
						<div class="portlet light">
							<div class="portlet-title">
								<div class="caption">
									<i class="icon-speech"></i>
									<span class="caption-subject bold uppercase"> Menunggu Approval</span>
								</div>
								<div class="actions">
									<a href="javascript:;" class="btn btn-circle btn-default btn-icon-only fullscreen"></a>
								</div>
							</div>
							<div class="portlet-body">
							    <?php
								$query = $this->hour_leasson_db->get_all(array(
								    'admin_approved' => 0
								));
								
								$this->load->view('admin/inc/table_approval_instructor', array('query' => $query));
							    ?>
							</div>
						</div>
					    </div>
					    
					    <div class="tab-pane" id="tab_instructor_history">
						<div class="portlet light">
							<div class="portlet-title">
								<div class="caption">
									<i class="icon-speech"></i>
									<span class="caption-subject bold uppercase"> Riwayat</span>
								</div>
								<div class="actions">
									<a href="javascript:;" class="btn btn-circle btn-default btn-icon-only fullscreen"></a>
								</div>
							</div>
							<div class="portlet-body">
							    <?php
								$query = $this->hour_leasson_db->get_all(array(
								    'admin_approved_ne' => 0
								));
								
								$this->load->view('admin/inc/table_approval_instructor', array('query' => $query));
							    ?>
								
								
								
							</div>
						</div>
					    </div>
					    
					</div>
				    </div>
				
				
				<!-- END Portlet PORTLET-->
				
				
				
				
				
				
				
			</div>
		</div>
		<!-- END PAGE CONTENT-->
	</div>
</div>
</div>