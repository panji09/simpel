<?php
    $page = $this->uri->segment(3);
    $state = $this->uri->segment(4);
    $query = $user = array();
    if($state == 'edit'){
	$query = $this->dynamic_pages_db->get($content_id);
	if($query)
	    $content = $query[0];
    }
?>
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- /.modal -->
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE HEADER-->
			<!-- BEGIN PAGE HEAD -->
			<div class="page-head">
				<!-- BEGIN PAGE TITLE -->
				<div class="page-title">
					<h1><?=$title_content?> <small><?=$title_page?></small></h1>
				</div>
				<!-- END PAGE TITLE -->
				<!-- BEGIN PAGE TOOLBAR -->
				
				<!-- END PAGE TOOLBAR -->
			</div>
			<!-- END PAGE HEAD -->
			<!-- BEGIN PAGE BREADCRUMB -->
			<ul class="page-breadcrumb breadcrumb">
				<li>
					<a href="#">Halaman</a>
					<i class="fa fa-circle"></i>
				</li>
				<li>
					<a href="<?=site_url('superadmin/pages/'.$page)?>"><?=$title_page?></a>
					<i class="fa fa-circle"></i>
				</li>
				<li>
					<a href="#"><?=$title_content?></a>
				</li>
			</ul>
			<!-- END PAGE BREADCRUMB -->
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					
					
					<div class="portlet box blue" id="form_wizard_1">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-gift"></i> <?=$title_content?>
							</div>
							<div class="tools hidden-xs">
								<a href="javascript:;" class="collapse">
								</a>
								
							</div>
						</div>
						<div class="portlet-body form">
							<form action="<?=site_url('superadmin/pages/gallery_post/'.$content_id)?>" id="form_sample_3" method='post' class="form-horizontal">
								<div class="form-body">
									<h3 class="form-section">Lengkapi kolom dibawah ini</h3>
									<div class="alert alert-danger display-hide">
										<button class="close" data-close="alert"></button>
										You have some form errors. Please check below.
									</div>
									<div class="alert alert-success display-hide">
										<button class="close" data-close="alert"></button>
										Your form validation is successful!
									</div>
									
									
									<div class="form-group">
										<label class="control-label col-md-3">Judul Album <span class="required">
										* </span>
										</label>
										<div class="col-md-4">
											<input value='<?=(isset($content['title']) ? $content['title'] : '' )?>' type="text" name="title" data-required="1" class="form-control"/>
										</div>
										
									</div>
									
									<div class="form-group">
										<label class="control-label col-md-3">Terbitkan <span class="required">
										* </span>
										</label>
										<div class="col-md-4">
											<div class="checkbox-list" data-error-container="#form_2_services_error">
												<label>
												<input type="checkbox" value="1" name="published" <?=(isset($content['published']) && $content['published'] ? 'checked' : '' )?>/> Ya </label>
												
											</div>
											
											<div id="form_2_services_error">
											</div>
										</div>
									</div>
									<h3 class="form-section">Foto</h3>
									
									<div class='row' id='upload_gallery'>
									  
									    <div class="col-md-3">
										
										<div class="row">
										    <div class="col-xs-6 col-md-3">
										      <a href="#" class="thumbnail" style="width: 250px; height: auto;">
											<img id='change_cover_multi' src="<?=(isset($content['cover']) && $content['cover']!='' ? $content['cover'] : 'http://www.placehold.it/250x250/EFEFEF/AAAAAA&text= Pilih Foto' )?>" alt="...">
											
										      </a>
										    </div>
										</div>
										
										
									    </div>
									    
									    
									    <?php if(isset($content['photo'])): for($i=0; $i < count($content['photo']); $i++):?>
									    <div class="col-md-3">
							    
										<div class="row">
										    <div class="col-xs-6 col-md-3">
										      <a href="#" class="thumbnail" style="width: 250px; height: 250px; background-color:black;">
											<img style="max-width: 250px; max-height: 250px;" id='change_cover' src="<?=(isset($content['photo'][$i]) && $content['photo'][$i]!='' ? $content['photo'][$i] : 'http://www.placehold.it/250x250/EFEFEF/AAAAAA&text= Pilih Foto' )?>" alt="...">
											<input type='hidden' name='photo[]' id='cover' value='<?=(isset($content['photo'][$i]) && $content['photo'][$i]!='' ? $content['photo'][$i] : 'http://www.placehold.it/250x250/EFEFEF/AAAAAA&text= Pilih Foto' )?>'>
										      </a>
										    </div>
										</div>
										<div class='row'>
										    
										    <div class="col-md-10">
											<button type="button" class="btn green delete_photo">Hapus</button>
											<input id='title' value='<?=(isset($content['title_photo'][$i]) && $content['title_photo'][$i]!='' ? $content['title_photo'][$i] : '' )?>' type="text" name="title_photo[]" data-required="1" class="form-control"/>
										    </div>
											      
										      <br>
										</div>
									    </div>
									    <?php endfor; endif;?>
									    
									    
									    
									</div>
									
								</div>
								<div class="form-actions">
									<div class="row">
										<div class="col-md-offset-3 col-md-9">
											<button type="submit" class="btn green">Simpan</button>
											<a href='<?=site_url($this->session->userdata('redirect'))?>' class="btn default">Batal</a>
										</div>
									</div>
								</div>
							</form>
							
							<div class='hide'>
							    <div id='template_cover'>
								<div class="col-md-3">
							    
								    <div class="row">
									<div class="col-xs-6 col-md-3">
									  <a href="#" class="thumbnail" style="width: 250px; height: 250px; background-color:black;">
									    <img style="max-width: 250px; max-height: 250px;" id='change_cover' src="<?=(isset($content['cover']) && $content['cover']!='' ? $content['cover'] : 'http://www.placehold.it/250x250/EFEFEF/AAAAAA&text= Pilih Foto' )?>" alt="...">
									    <input type='hidden' name='photo[]' id='cover' value=''>
									  </a>
									</div>
								    </div>
								    <div class='row'>
									
									<div class="col-md-10">
									    <button type="button" class="btn green delete_photo">Hapus</button>
									    <input id='title' value='' type="text" name="title_photo[]" data-required="1" class="form-control"/>
									</div>
										  
									  <br>
								    </div>
								</div>
							    </div>
							</div>
							
						</div>
					</div>
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>