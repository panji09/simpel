<?php
    $page = $this->uri->segment(3);
    $token = $this->connect_auth->get_access_token();
?>
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			<!-- BEGIN PAGE HEAD -->
			<div class="page-head">
				<!-- BEGIN PAGE TITLE -->
				<div class="page-title">
					<h1><?=$title_page?></h1>
				</div>
				<!-- END PAGE TITLE -->
			</div>
			<!-- END PAGE HEAD -->
			<!-- BEGIN PAGE BREADCRUMB -->
			<ul class="page-breadcrumb breadcrumb">
				<li>
					<a href="#">Dashboard</a>
					<i class="fa fa-circle"></i>
				</li>
				<li>
					<a href="#">Management User</a>
					<i class="fa fa-circle"></i>
				</li>
				
				<li>
					<a href="#"><?=$title_page?></a>
					<i class="fa fa-circle"></i>
				</li>
			</ul>
			<!-- END PAGE BREADCRUMB -->
			<!-- END PAGE HEADER-->
			
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<?php callback_submit();?>
					<div class="portlet light">
						<div class="portlet-title">
							<div class="caption font-blue-sharp">
								<i class="icon-globe font-blue-sharp"></i>
								<span class="caption-subject"><?=$title_page?></span>
								<span class="caption-helper">Management User</span>
							</div>
							
							<div class="actions">
								<?php if(in_array($page, array('admin','superadmin','facilitator','instructor'))):?>
								
								<a href="<?=site_url('superadmin/management_user/'.$page.'/add')?>" class="btn btn-circle btn-default">
								<i class="fa fa-plus"></i> Tambah </a>
								<?php endif;?>
								
								<?php if(in_array($page, array('institution','participant','instructor'))):?>
								
								<a data-url="<?=site_url('superadmin/management_user/invite/'.$entity)?>" data-toggle="modal" data-target="#invite_user_modal" data-title='Invite <?=$title_page?>' class="btn btn-circle btn-default invite_user">
								<i class="fa fa-plus"></i> Invite </a>
								<?php endif;?>
								
								
								<a href="javascript:;" class="btn btn-circle btn-default btn-icon-only fullscreen"></a>
							</div>
							
						</div>
						<div class="portlet-body">
							
						<?php
							$param = array(
										   'token' => $token,
										   'page' => $page
										   );
							$this->load->view('admin/inc/table_user', $param);
						?>
					
							
						</div>
					</div>
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>