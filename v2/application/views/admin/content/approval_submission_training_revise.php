<?php
    $page = $this->uri->segment(3);
    $token = $this->connect_auth->get_access_token();
?>
<div class="page-content-wrapper">
	<div class="page-content">
		<!-- BEGIN PAGE HEADER-->
		<!-- BEGIN PAGE HEAD -->
		<div class="page-head">
			<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
				<h1><?=$title_page?></h1>
			</div>
			<!-- END PAGE TITLE -->
		</div>
		<!-- END PAGE HEAD -->
		<!-- BEGIN PAGE BREADCRUMB -->
		<ul class="page-breadcrumb breadcrumb">
			<li>
				<a href="#">Approval</a>
				<i class="fa fa-circle"></i>
			</li>
			
			<li>
				<a href="#"><?=$title_page?></a>
			</li>
		</ul>
		<!-- END PAGE BREADCRUMB -->
		<!-- END PAGE HEADER-->
		<!-- BEGIN PAGE CONTENT-->
		<div class="row">
			<div class="col-md-12">
				<?php callback_submit();?>
				
				
				<!-- BEGIN Portlet PORTLET-->
				<div class="portlet light">
					<div class="portlet-title">
						<div class="caption">
							<i class="icon-speech"></i>
							<span class="caption-subject bold uppercase"> <?=$title_page?></span>
						</div>
						<div class="actions">
							<a href="javascript:;" class="btn btn-circle btn-default btn-icon-only fullscreen"></a>
						</div>
					</div>
					<div class="portlet-body">
						<table class="table table-striped table-bordered table-hover standard-font" id="table_approval_submission_training">
							<thead>
								<tr>
									<th>
										Tanggal
									</th>
									<th>
										Lembaga Pelatihan/LPP
									</th>
									<th>
										Nama Pelatihan
									</th>

									<th>
										Tanggal Pelatihan
									</th>
									<th>
										Verifikasi
									</th>
									<th>
										Status
									</th>
									<th>
										Action
									</th>
								</tr>
							</thead>
							<tbody>
								<?php

								if($this->connect_auth->get_me()['entity']=='admin'){
									$query = $this->training_db->get_all(array('draft' => 0));
								}

								if($this->connect_auth->get_me()['entity']=='fasilitator'){
									$query = $this->training_db->get_all(
									array(
										'draft' => 0, 
										'verified_submission_training' => 1,
										'facilitator_user_id' => $this->connect_auth->get_me()['user_id']
										)
									);
								}
								if($query)
								foreach($query as $row):

									?>
									<tr class="odd gradeX">
										<td><time class="timeago" datetime="<?=date('c',$row['created'])?>"><?=date("F j, Y, g:i a",$row['created']) ?></time></td>
										<td><?=$row['creator']['training_institute']?></td>
										<td>
											<?=$row['type_training']['name'].' / '.$row['training_name']['name']?>
										</td>


										<td><?=$row['training_date']?></td>
										<td>
											<?php
											$status = array();


											if(isset($row['verified_submission_training']))
											switch($row['verified_submission_training']){
												case 0 : $status = array('name' => 'Menunggu Verifikasi', 'label' => 'label-warning'); break;
												case 1 : $status = array('name' => 'Disetujui', 'label' => 'label-success'); break;
												case -1 : $status = array('name' => 'Klarifikasi', 'label' => 'label-warning'); break;
												case -2 : $status = array('name' => 'Tolak', 'label' => 'label-danger'); break;
												default : $status = array('name' => 'Menunggu Verifikasi', 'label' => 'label-warning'); break;
											}
											?>
											<span class="label label-sm <?=$status['label']?>"> <?=$status['name']?> </span>
										</td>

										<td>
											<?php
											$status = array();

											if(isset($row['draft'])){
												$status = array('name' => 'draft', 
												'label' => 'label-warning');
											}

											if(isset($row['verified_submission_training']))
											switch($row['verified_submission_training']){
												case 0 : 
												case -1 : 
												case -2 : $status = array('name' => 'Tahap Pengajuan',
												'label' => 'label-warning'); break;
												case 1 : $status = array('name' => 'Tahap Persiapan',
												'label' => 'label-warning'); break;
											}
											?>
											
											<div class="margin-bottom-10">
												<span class="label label-sm
												<?=$status['label']?>"><?=$status['name']?></span>
											</div>
											<?php 
											if(isset($row['rescheduled']) && $row['rescheduled']):
												$status = array('name' => 'Reschedule', 
												'label' => 'label-default');
											?>

											<div class="margin-bottom-10">
												<span class="label label-sm 
												<?=$status['label']?>"><?=$status['name']?></span>
											</div>
										<?php endif;?>

											<?php 
											if(isset($row['facilitator_user_id'])):
												$status = array('name' => 'Pemilihan Pengajar', 
												'label' => 'label-default');
											?>
											<div class="margin-bottom-10">
												<span class="label label-sm 
												<?=$status['label']?>"><?=$status['name']?></span>
											</div>
											<?php endif;?>

										</td>
										<td>
											<div class="margin-bottom-5">
												<button 
												type="button" 
												class="btn default btn-xs purple review_training_submission"
												data-url-submit="<?=site_url('lpp/management_training/training_post/'.$row['_id']->{'$id'})?>" 
												data-toggle="modal" 
												data-training_id="<?=$row['_id']->{'$id'}?>" 
												data-target="#review_training_submission">
													<i class="fa fa-eye"></i> Review 
												</button>
											</div>
											<?php if($this->connect_auth->get_me()['entity'] ==
											'fasilitator'):?>
											<?php $query_jp = $this->hour_leasson_db->get_all(
											array(
												'training_id' => $row['_id']->{'$id'}
											));
											$on_progress = false;
											if($query_jp){
												$result = $query_jp[0];
												$on_progress = (isset($result['instructor_approved'])
													 && in_array($result['instructor_approved'],
												 array(1,0)) ? true : false);
											}
											?>
												
											<?php if(!$on_progress):?>
												<div class="margin-bottom-5">
													<a href="<?=site_url('fasilitator/management_training/instructor/choose/'.$row['_id']->{'$id'})?>"
														class="btn default btn-xs red">
													<i class="fa fa-exchange"></i> 
													Pilih Pengajar
													</a>
												</div>

											<?php else:?>
												<div class="margin-bottom-5">
													<a href = "<?=site_url('fasilitator/management_training/instructor/choose/'.$row['_id']->{'$id'}.'/progress')?>"
														class="btn default default btn-xs green">
														<i class="fa fa-circle-o-notch"></i>
														Progress Pengajar
													</a>
												</div>
											<?php endif;?>
												
											<?php endif;?>
										</td>
									</tr>
								<?php endforeach; ?>
							</tbody>
						</table>
						
					</div>
				</div>
				<!-- END Portlet PORTLET-->
				
				
				
				
				
				
				<?php /* 
				<!-- BEGIN EXAMPLE TABLE PORTLET-->
				<div class="portlet box grey-cascade">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-globe"></i><?=$title_page?>
						</div>
						<div class="tools">
							<a href="javascript:;" class="collapse">
							</a>
						</div>
					</div>
					<div class="portlet-body">
						
						
						<table class="table table-striped table-bordered table-hover" id="table_approval_submission_training">
							<thead>
								<tr>
									<th>
										 Tanggal
									</th>
									<th>
										 Lembaga Pelatihan/LPP
									</th>
									<th>
										 Nama Pelatihan
									</th>
									
									<th>
										 Tanggal Pelatihan
									</th>
									<th>
										 Verifikasi
									</th>
									<th>
										 Status
									</th>
									<th>
										 Action
									</th>
								</tr>
							</thead>
						<tbody>
						<?php
						    
						    if($this->connect_auth->get_me()['entity']=='admin'){
							$query = $this->training_db->get_all(array('draft' => 0));
						    }
						    
						    if($this->connect_auth->get_me()['entity']=='fasilitator'){
							$query = $this->training_db->get_all(
							    array(
								'draft' => 0, 
								'verified_submission_training' => 1,
								'facilitator_user_id' => $this->connect_auth->get_me()['user_id']
							    )
							);
						    }
						    if($query)
							foreach($query as $row):
					    
						?>
						<tr class="odd gradeX">
						    <td><time class="timeago" datetime="<?=date('c',$row['created'])?>"><?=date("F j, Y, g:i a",$row['created']) ?></time></td>
						    <td><?=$row['creator']['training_institute']?></td>
						    <td>
							<?=$row['type_training']['name'].' / '.$row['training_name']['name']?>
						    </td>
						    
						    
						    <td><?=$row['training_date']?></td>
						    <td>
							<?php
							    $status = array();
							    
							    
							    if(isset($row['verified_submission_training']))
							    switch($row['verified_submission_training']){
								case 0 : $status = array('name' => 'Menunggu Verifikasi', 'label' => 'label-warning'); break;
								case 1 : $status = array('name' => 'Disetujui', 'label' => 'label-success'); break;
								case -1 : $status = array('name' => 'Klarifikasi', 'label' => 'label-warning'); break;
								case -2 : $status = array('name' => 'Tolak', 'label' => 'label-danger'); break;
								default : $status = array('name' => 'Menunggu Verifikasi', 'label' => 'label-warning'); break;
							    }
							?>
							<span class="label label-sm <?=$status['label']?>"> <?=$status['name']?> </span>
						    </td>
						    
						    <td>
							<?php
							    $status = array();
							    
							    if(isset($row['draft'])){
								$status = array('name' => 'draft', 'label' => 'label-warning');
							    }
							    
							    if(isset($row['verified_submission_training']))
							    switch($row['verified_submission_training']){
								case 0 : 
								case -1 : 
								case -2 : $status = array('name' => 'Tahap Pengajuan', 'label' => 'label-warning'); break;
								case 1 : $status = array('name' => 'Tahap Persiapan', 'label' => 'label-warning'); break;
								
								
							    }
							?>
							<span class="label label-sm <?=$status['label']?>"><?=$status['name']?></span>
							
							<?php if(isset($row['rescheduled']) && $row['rescheduled']):
							    $status = array('name' => 'Reschedule', 'label' => 'label-default');
							?>
							
							<span class="label label-sm <?=$status['label']?>"><?=$status['name']?></span>
							<?php endif;?>
							
							<?php if(isset($row['facilitator_user_id'])):
							    $status = array('name' => 'Pemilihan Pengajar', 'label' => 'label-default');
							?>
							
							<span class="label label-sm <?=$status['label']?>"><?=$status['name']?></span>
							<?php endif;?>
							
						    </td>
						    <td>
							<div class="btn-group">
							    <button 
							      type="button" 
							      class="btn btn-default review_training_submission" 
							       
							      data-url-submit="<?=site_url('lpp/management_training/training_post/'.$row['_id']->{'$id'})?>" 
							      data-toggle="modal" 
							      data-training_id="<?=$row['_id']->{'$id'}?>" 
							      data-target="#review_training_submission"
							    >Review</button>
							    
							    <?php if($this->connect_auth->get_me()['entity'] == 'fasilitator'):?>
							    
							    <?php
								$query_jp = $this->hour_leasson_db->get_all(array(
								    'training_id' => $row['_id']->{'$id'}
								));
								$on_progress = false;
								if($query_jp){
								    $result = $query_jp[0];
								    
								    
								    $on_progress = (isset($result['instructor_approved']) &&   in_array($result['instructor_approved'], array(1,0)) ? true : false);
								}
							    
								
							    ?>
							    
							    <?php if(!$on_progress):?>
							    
							    <a 
							      class="btn btn-default" 
							      href = "<?=site_url('fasilitator/management_training/instructor/choose/'.$row['_id']->{'$id'})?>"
							      
							    >Pilih Pengajar</a>
							    
							    <?php else:?>
							    
							    <a 
							      class="btn btn-default" 
							      href = "<?=site_url('fasilitator/management_training/instructor/choose/'.$row['_id']->{'$id'})?>"
							      
							    >Progress Pengajar</a>
							    
							    <?php endif;?>
							    
							    
							    <a 
							      class="btn btn-default create_schedule" 
							      href = "<?=site_url('fasilitator/management_training/create_schedule/'.$row['_id']->{'$id'})?>"
							      
							    >Buat Susunan Agenda</a>
							    <?php endif;?>
							</div>
						    </td>
						</tr>
						<?php endforeach; ?>
						
						
						</tbody>
						</table>
					</div>
				</div>
				<!-- END EXAMPLE TABLE PORTLET-->
				*/ ?>
			</div>
		</div>
		<!-- END PAGE CONTENT-->
	</div>
</div>