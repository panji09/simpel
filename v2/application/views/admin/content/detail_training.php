<?php
   $token = $this->connect_auth->get_access_token();
   $query = $this->training_db->get($training_id);
   if($query){
   	$training = $query[0];
   	$user = $training['user_submission'];
   }
   
   $on_progress = ($this->uri->segment(6) ? true : false);
   ?>
<style>
   .top-buffer { margin-top:10px; }
   .table-toolbar .form-group { width: 400px }
</style>
<div class="page-content-wrapper">
   <div class="page-content">
      <!-- BEGIN PAGE HEADER-->
      <!-- BEGIN PAGE HEAD -->
      <div class="page-head">
         <!-- BEGIN PAGE TITLE -->
         <div class="page-title">
            <h1>Manajemen Pelatihan</h1>
            
         </div>
         <!-- END PAGE TITLE -->
      </div>
      <!-- END PAGE HEAD -->
      <!-- BEGIN PAGE BREADCRUMB -->
      <ul class="page-breadcrumb breadcrumb">
         
         <li>
            <a href="#"><?=$title_page?></a>
            <i class="fa fa-circle"></i>
         </li>
         <li>
            <a href="#"><?=$title_content?></a>
         </li>
      </ul>
      <!-- END PAGE BREADCRUMB -->
      <!-- END PAGE HEADER-->
      <!-- BEGIN PAGE CONTENT-->		
      <div class="row">
         <div class="col-md-12">
            <?php callback_submit();?>
            <!-- BEGIN Tab Info -->
            <div class="tabbable tabbable-custom tabbable-noborder" id='overview_detail_training'>
               <ul class="nav nav-tabs " id='overview_detail_training_tab'>
                  <li class="active">
                     <a href="#tab_overview" data-toggle="tab" class="bold uppercase" data-load_page='detail_tab_overview'>
                     Overview
                     </a>
                  </li>
                  
                  <li>
                     <a href="#tab_todolist" data-toggle="tab" class="bold uppercase" data-load_page='detail_tab_todolist'>
                     Todolist
                     </a>
                  </li>
                  
                  <li>
                     <a href="#tab_instructor" data-toggle="tab" class="bold uppercase" data-load_page='detail_tab_instructor'>
                     Pengajar 
                     </a>
                  </li>
                  <li>
                     <a href="#tab_participant" data-toggle="tab" class="bold uppercase" data-load_page='detail_tab_participant'>
                     Peserta 
                     </a>
                  </li>
                  
                  <li>
                     <a href="#tab_score_training" data-toggle="tab" class="bold uppercase" data-load_page='detail_tab_score_training'>
                     Nilai Pelatihan 
                     </a>
                  </li>
                  
                  <li>
                     <a href="#tab_evaluation" data-toggle="tab" class="bold uppercase" data-load_page='detail_tab_evaluation'>
                     Evaluasi 
                     </a>
                  </li>
                  
                  <li>
                     <a href="#tab_certificate_request" data-toggle="tab" class="bold uppercase" data-load_page='detail_tab_certificate_request'>
                     Permintaan Sertifikat 
                     </a>
                  </li>
                  
               </ul>
               
               <!--tab overview-->
               <div class="tab-content">
                  <div class="tab-pane active" id="tab_overview">
                  
                  </div>
                  <!--./tab overview-->
                  
                  <!-- pengajar-->
                  <div class="tab-pane" id="tab_instructor">
                  
                  </div>
                  <!-- ./pengajar-->
                  
                  <!-- peserta-->
                   <div class="tab-pane" id="tab_participant">
                  
		  </div>
                  <!-- ./peserta-->
                  
                  <!-- evaluasi-->
                  <div class="tab-pane" id="tab_evaluation">
                  
                  </div>
                  <!-- ./evaluasi-->
                  
                  <!--todolist-->
                  <div class="tab-pane" id="tab_todolist">
                  
                  </div>
                  <!--./todolist-->
                  
                  <!--score training-->
                   <div class="tab-pane" id="tab_score_training">
                     
				    
                  </div>
                  <!--./score training-->
		  
		  <div class='tab-pane' id='tab_certificate_request'>
		  
		  </div>
               </div>
            </div>
            <!-- END Tab Info -->
         </div>
      </div>
   
   </div>
   <!-- END PAGE CONTENT-->
</div>
<!--page-content-wrapper--> 
