<?php
   // $page = $this->uri->segment(3);
    $token = $this->connect_auth->get_access_token();
	//$this->load->model('Pengaduan');
?>
<div class="page-content-wrapper">
	<div class="page-content">
		<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
		<!-- /.modal -->
		<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
		<!-- BEGIN PAGE HEADER-->
		<!-- BEGIN PAGE HEAD -->
		<div class="page-head">
			<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
				<h1><?=$title_page?></h1>
			</div>
			<!-- END PAGE TITLE -->
		</div>
		<!-- END PAGE HEAD -->
		<!-- BEGIN PAGE BREADCRUMB -->
		<ul class="page-breadcrumb breadcrumb">
			<li>
				<a href="#">Pengaduan</a>
				<i class="fa fa-circle"></i>
			</li>
			
			<li>
				<a href="#"><?=$title_page?></a>
			</li>
		</ul>
		<!-- END PAGE BREADCRUMB -->
		<!-- END PAGE HEADER-->
		<!-- BEGIN PAGE CONTENT-->
		<div class="row">
			<div class="col-md-12">
				<?php callback_submit();?>
				
				<!-- BEGIN EXAMPLE TABLE PORTLET-->
				<div class="portlet box grey-cascade">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-globe"></i><?=$title_page?>
						</div>
						<div class="tools">
							<a href="javascript:;" class="collapse">
							</a>
						</div>
					</div>
					<div class="portlet-body">
							<table class="table table-striped table-bordered table-hover" id="complaint_table">
						<thead>
						<tr>
							
							<th>
								 Nama Complainant
							</th>
							<th>
								 Email
							</th>
							<th>
								 created
							</th>
							<th>
								 Provinsi
							</th>
                                                        <th>
								 Deskripsi (Pertanyaan/saran dll)
							</th>
                                                        <th>
                                                                 Status
                                                        </th>
							<th>
								 Action
							</th>
						</tr>
						</thead>
						<tbody>
						<?php
						   
                                                $data_complaint = $this->pengaduan_db->get_all(array('verification'=>'1'));
                                                
						    foreach($data_complaint as $row):
						?>
						<tr class="odd gradeX">
						    <td><?=$row['name']?></td>
						    <td>
							<?= $row['email']?>
						    </td>
						    <td><time class="timeago" datetime="<?=date('c',$row['created'])?>"><?=date("F j, Y, g:i a",$row['created']) ?></time></td>
						    <td><?= $row['province']['name']?> </td>
                                                    <td><?= $row['description']?> </td>
                                                    <td><?php if (isset($row['answer_time'])) 
                                                    {
                                                        echo 'terjawab'; 
                                                    ?>
                                                        <time class="timeago" datetime="<?=date('c',$row['answer_time'])?>"><?=date("F j, Y, g:i a",$row['created']) ?></time>
                                                        <?php
                                                        
                                                    }
                                                        else 
                                                        {
                                                            echo 'belum dijawab';
                                                        }?></td>
						    
                                                    <td>
							<div class="btn-group">
							    <?php
							    /*
							    <button type="button" class="btn btn-default" onclick="location.href='<?=site_url('superadmin/management_user/'.$page.'/edit/'.$row['user_id'])?>'">Edit</button>
							    */
							    ?>
							    <button type="button" class="btn btn-default" data-toggle="modal" data-target="#view_<?= $row['_id'];?>">View</button>

							    <button type="button" class="btn btn-default" data-toggle="modal" data-target="#<?= $row['_id'];?>">Jawab</button>
							    
							    <button type="button" class="btn btn-default delete-user" data-url-delete="<?=site_url('admin/complaint_handling/delete_complaint/'.$row['_id'])?>" data-toggle="modal" data-target="#delete">Delete</button>
							</div>
						    </td>
						</tr>
                                                
                                                <!-- Modal untuk Jawaban-->

                                                <div class="modal fade" id="<?= $row['_id'];?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                        <div class="modal-dialog">
                                                                <div class="modal-content">
                                                                        <div class="modal-header">
                                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                                                <h4 class="modal-title">FORM JAWAB PERTANYAAN</h4>
                                                                        </div>
                                                                        <div class="modal-body">
                                                                            
                                                                        <?php echo form_open("admin/complaint_handling/post_answer/".$row['_id']);?>
                                                                            <div class="form-group">
                                                                                <label>Nama :</label>
                                                                                <p><?= $row['name'];?></p>
                                                                            </div>
                                                                         
                                                                            <div class="form-group">
                                                                                <label>Email :</label>
                                                                                <p><?= $row['email'];?></p>
                                                                            </div>
                                                                            
                                                                             <div class="form-group">
                                                                                <label>Telepon/Hp :</label>
                                                                                <p><?= $row['phone'];?></p>
                                                                            </div>
                                                                            
                                                                            <div class="form-group">
                                                                                <label>Alamat :</label>
                                                                                <p><?= $row['address'];?></p>
                                                                            </div>
                                                                                                                                                     
                                                                            <div class="form-group">
                                                                                <label>Pertanyaan :</label>
                                                                                <p class="bg-success"><?= $row['description'];?></p>
                                                                            </div>
                                                                            
                                                                            <div class="form-group">
                                                                                <label>Jawaban :</label>
                                                                                <textarea class="form-control" rows="4" name="answer"><?php if (isset($row['answer'])) echo $row['answer']; ?></textarea>
                                                                            </div>
                                                                            
                                                                            
                                                                        <div class="modal-footer">
                                                                                <button type="submit" class="btn blue" >Update</button>
                                                                                <button type="button" class="btn default" data-dismiss="modal">Tutup</button>
                                                                        </div>
                                                                <?php echo form_close();?>
                                                                </div>
                                                                <!-- /.modal-content -->
                                                        </div>
                                                        <!-- /.modal-dialog -->
                                                    </div>
                                                </div>    

                                                <!-- end modal jawab-->
                                                
                                                
                                                <!-- Modal untuk Jawaban-->

                                                <div class="modal fade" id="view_<?= $row['_id'];?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                        <div class="modal-dialog">
                                                                <div class="modal-content">
                                                                        <div class="modal-header">
                                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                                                <h4 class="modal-title">FORM JAWAB PERTANYAAN</h4>
                                                                        </div>
                                                                        <div class="modal-body">
                                                                            
                                                                            <div class="form-group">
                                                                                <label>Nama :</label>
                                                                                <p><?= $row['name'];?></p>
                                                                            </div>
                                                                         
                                                                            <div class="form-group">
                                                                                <label>Email :</label>
                                                                                <p><?= $row['email'];?></p>
                                                                            </div>
                                                                            
                                                                             <div class="form-group">
                                                                                <label>Telepon/Hp :</label>
                                                                                <p><?= $row['phone'];?></p>
                                                                            </div>
                                                                            
                                                                            <div class="form-group">
                                                                                <label>Alamat :</label>
                                                                                <p><?= $row['address'];?></p>
                                                                            </div>
                                                                                                                                                     
                                                                            <div class="form-group">
                                                                                <label>Pertanyaan :</label>
                                                                                <p class="bg-success"><?= $row['description'];?></p>
                                                                            </div>
                                                                            
                                                                            <div class="form-group">
                                                                                <label>Jawaban :</label>
                                                                                <p><?php if (isset($row['answer'])) echo $row['answer']; ?></p>
                                                                            </div>
                                                                            
                                                                            
                                                                        <div class="modal-footer">
                                                                                <button type="button" class="btn default" data-dismiss="modal">Tutup</button>
                                                                        </div>
                                                               
                                                                </div>
                                                                <!-- /.modal-content -->
                                                        </div>
                                                        <!-- /.modal-dialog -->
                                                    </div>
                                                </div>  
                                                
                                                
						<?php endforeach; ?>
						
						</tbody>
						</table>
					
					</div>
		
				</div>
				<!-- END EXAMPLE TABLE PORTLET-->
			</div>
		</div>
		<!-- END PAGE CONTENT-->
	</div>
</div>

