<?php
    $page = $this->uri->segment(3);
    $state = $this->uri->segment(4);
    $query = $user = array();
    if($state == 'edit'){
	$query = $this->dynamic_pages_db->get($content_id);
	if($query)
	    $content = $query[0];
    }
?>
<div class="page-content-wrapper">
	<div class="page-content">
		<!-- BEGIN PAGE HEADER-->
		<!-- BEGIN PAGE HEAD -->
		<div class="page-head">
			<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
				<h1><?=$title_content?> <small><?=$title_page?></small></h1>
			</div>
			<!-- END PAGE TITLE -->
		</div>
		<!-- END PAGE HEAD -->
		<!-- BEGIN PAGE BREADCRUMB -->
		<ul class="page-breadcrumb breadcrumb">
			<li>
				<a href="#">Dashboard</a>
				<i class="fa fa-circle"></i>
			</li>
			<li>
				<a href="#">Halaman</a>
				<i class="fa fa-circle"></i>
			</li>
			<li>
				<a href="<?=site_url('superadmin/pages/'.$page)?>"><?=$title_page?></a>
				<i class="fa fa-circle"></i>
			</li>
			<li>
				<a href="#"><?=$title_content?></a>
			</li>
		</ul>
		<!-- END PAGE BREADCRUMB -->
		<!-- END PAGE HEADER-->
		
		
		<!-- BEGIN CONTENT ROW -->
		<div class="row">
			<form action="<?=site_url('superadmin/pages/event_post/'.$content_id)?>" id="form_sample_3" method='post'>					
				<div class="col-md-8">
					<!-- show this notification when error -->
					<!-- <div class="alert alert-block alert-danger fade in">
						<button type="button" class="close" data-dismiss="alert"></button>
						<h4 class="alert-heading">Error!</h4>
						<p>
							 Terjadi kesalahan, mohon cek ulang apakah semua field sudah terisi dengan benar.
						</p>
					</div> -->
						
					<h3>Judul Agenda</h3>
					<input type="text" class="form-control" placeholder="judul agenda" value='<?=(isset($content['title']) ? $content['title'] : '' )?>' name="title" data-required="1">
					<br />
											
					<div class="editor-container">
						<textarea class="form-control" id="wysiwyg-content" name="content" rows="20">
							<?=(isset($content['content']) ? $content['content'] : '' )?>
						</textarea>							
					</div>
	
				</div>
				
				<!-- BEGIN EDITOR SIDEBAR -->
				<div class="col-md-4 editor-sidebar">
					<!-- BEGIN SIDEBAR ROW -->
					<div class="row">
						<div class="col-md-12">
							<!-- Begin: publish news -->
							<div class="portlet light">
								<div class="portlet-title">
									<div class="caption">
										<i class="icon-speech font-blue-sharp"></i>
										<span class="caption-subject font-blue-sharp bold uppercase">Publish</span>
									</div>
								</div>
								<div class="portlet-body">	
									
									<div class="portlet-body form">
										<div class="form-body">	
											<div class="form-group">
											    
											    
												<input type='text' class="form-control" name='location_name' value='<?=(isset($content['location_name']) ? $content['location_name'] : '')?>' placeholder='Contoh : Bogor'></input>
												<span class="help-block"><cite>Lokasi</cite></span>
											</div>
											
											<div class="form-group">
												<!-- <label>Tanggal</label> -->
												<div class='input-group date datetimepicker'>
												    <input type='text' class="form-control" name='date' value='<?=(isset($content['date']) ? $content['date'] : '')?>' placeholder="1945-12-12"/>
												    <span class="input-group-addon">
													<span class="glyphicon glyphicon-calendar"></span>
												    </span>
												</div>
												<span class="help-block"><cite>Tanggal agenda dilaksanakan</cite></span>
											</div>
											
											<div class="form-group">
												<!-- <label>&nbsp;</label> -->
												<div class="checkbox-list">
													<label>
														<div class="checkbox-list">
																															<input type="checkbox" value="1" name="published" <?=(isset($content['published']) && $content['published'] ? 'checked' : '' )?>/> Publish
														</div>
														
													</label>
													<span class="help-block"><cite>Centang checkbox diatas untuk publish agenda</cite></span>
												</div>
											</div>
											
										</div>
										<div class="form-actions">
											<button type="submit" class="btn btn-success btn-sm btn-block">
												Simpan
											</button>
										</div>
									</div>
								</div>
							</div>
							<!-- End: publish news -->
						</div>
						
						<div class="col-md-12">
							<!-- Begin: Thumbnail news -->
							<div class="portlet light">
								<div class="portlet-title">
									<div class="caption">
										<i class="icon-picture font-blue-sharp"></i>
										<span class="caption-subject font-blue-sharp bold uppercase">Thumbnail</span>
									</div>
								</div>
								<div class="portlet-body">
									<a href="#" class="thumbnail">
										<img class='change_cover' src="<?=(isset($content['cover']) && $content['cover']!='' ? $content['cover'] : 'http://www.placehold.it/300x200/EFEFEF/AAAAAA&text=pilih foto' )?>" alt="...">
										<input type='hidden' name='cover' id='cover' value='<?=(isset($content['cover']) ? $content['cover'] : '' )?>'>
									 </a>
		  							<p class="text-muted">
		  								<cite>Ukuran Gambar 640x640</cite>
		  							</p>
								</div>
							</div>
							<!-- End: Thumbnail news -->
						</div>					
						<div class="col-md-12">
							<!-- Begin: Deskripsi news -->
							<div class="portlet light">
								<div class="portlet-title">
									<div class="caption">
										<i class="icon-cup font-blue-sharp"></i>
										<span class="caption-subject font-blue-sharp bold uppercase">Deskrpisi</span>
									</div>
								</div>
								<div class="portlet-body">
									<textarea class="form-control" id="wysiwyg-short" name="content_short">
										<?=(isset($content['content_short']) ? $content['content_short'] : '' )?>
									</textarea>																	
		 							<p class="text-muted">
										<cite>Deskripsi 400 karakter</cite>
		 							</p>
								</div>
							</div>
							<!-- End: Deskripsi news -->
						</div>			
					</div>
					<!-- END SIDEBAR ROW -->
					
				</div>
				<!-- END EDITOR SIDEBAR -->
				
			</form>			
		</div>
		<!-- END CONTENT ROW -->
	</div>
</div>