<div class="page-footer">
	<div class="page-footer-inner">
		<?=$this->config->item('title_footer')?>
	</div>
	<div class="scroll-to-top">
		<i class="icon-arrow-up"></i>
	</div>
</div>