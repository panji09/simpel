
						<table class="table table-striped table-bordered table-hover table_approval_instructor" id="">
							<thead>
								<tr>
									<th>
										 Tanggal
									</th>
									<th>
										 Tanggal Pelatihan
									</th>
									
									
									
									<th>
										 Nama Pelatihan
									</th>
									<th>
										 Jumlah Jam Pelajaran
									</th>
									
									<th>
										 Tanggal Ngajar
									</th>
									
									<th>
										 Fasilitator
									</th>
									
									<th>
										 Nama Pengajar
									</th>
									
									<th>
										Lembaga Pleatihan/LPP
									</th>
									
									<th>
										Akreditasi
									</th>
									
									
									<th>
										 Konfirmasi Pengajar
									</th>
									
									<th>
										 Konfirmasi Atasan Pengajar
									</th>
									
									<th>
										 Status
									</th>
									<th>
										 Action
									</th>
								</tr>
							</thead>
						<tbody>
						<?php
						    
				   
						    if($query)
							foreach($query as $row):
							// var_dump($row['training_log']['user_submission']);
							
							
							//print_r($query); exit();
						?>
						<tr class="odd gradeX">
						    <td>
							<time class="" datetime="<?=date('c',$row['created'])?>"><?=date("d/m/Y H:i:s",$row['created']) ?></time>
						    </td>
						    
						    <td>
							
							<?php
								$temp = explode(' - ',$row['training_log']['training_date']);
								$training_date_start = $temp[0];
								$training_date_end = $temp[1];
								
							?>
						    <?=tgl_indo($training_date_start)?> - <?=tgl_indo($training_date_end)?>
							</td>
							
						    
						    <td>
							<?=$row['training_log']['type_training']['name'].' / '.$row['training_log']['training_name']['name']?>
						    </td>
						    
						    <td>
							<?=$row['hour_leasson']?>
						    </td>
						    
						    <td>
							
							<?php foreach($row['submission_date'] as $row1):?>
								<?=tgl_indo($row1).' '?>
							<?php endforeach;?>
						    </td>
						    
							<td>
								<?php $user_id = $row['training_log']['facilitator_user_id'];?>
								<?php
								 $token = $this->connect_auth->get_access_token();
								 $response = call_api_get($this->config->item('connect_api_url').'/user/index/'.$token.'/'.$user_id);
								 $response_body = json_decode($response['body'], true);
								 //	var_dump($response_body);
								
								echo $response_body['fullname'].'/'.$response_body['username'];
								
								//var_dump($response_body);
								?>
							</td>
							
							<td>
								<?php $user_id = $row['instructor_id'];?>
								<?php
								 $token = $this->connect_auth->get_access_token();
								 $response = call_api_get($this->config->item('connect_api_url').'/user/index/'.$token.'/'.$user_id);
								 $response_body = json_decode($response['body'], true);
								 //	var_dump($response_body);
								
								echo $response_body['fullname'].'/'.$response_body['username'];
								?>
							</td>
							
							<td><?= $row['training_log']['user_submission']['training_institute'];?></td>
							
							<?php
							//var_dump($row['training_log']['user_submission']);
							?>
							
							<!-- Akreditasi -->
												<?php if($row['training_log']['user_submission']['verified_institute']==1):?>
							      
												<?php if($row['training_log']['user_submission']['accreditation_level'] == 'terdaftar'):?>
												<td><span class="label label-info"> <?=strtoupper($row['training_log']['user_submission']['accreditation_level'])?></span></td>
												<?php else:?>
												<td><span class="label label-success">Akreditasi <?=strtoupper($row['training_log']['user_submission']['accreditation_level'])?></span></td>
												<?php endif;?>
												
											  <?php elseif($row['training_log']['user_submission']['verified_institute']==0):?>
											  <td><span class="label label-default">Verifikasi</span></td>
											  <?php endif;?>
												<!-- end akreditasi-->
							
						
												
							
							
							
						    <td>
							<?php
							    $verifikasi = array('name' => 'Menunggu Persetujuan', 'label' => 'label-warning');
							    if(isset($row['instructor_approved']))
							    switch($row['instructor_approved']){
									case 0 : $verifikasi = array('name' => 'Menunggu Persetujuan', 'label' => 'label-warning'); break;
									case 1 : $verifikasi = array('name' => 'Disetujui', 'label' => 'label-success'); break;
									case -1 : $verifikasi = array('name' => 'Klarifikasi', 'label' => 'label-warning'); break;
									case -2 : $verifikasi = array('name' => 'Tolak', 'label' => 'label-danger'); break;
									default : $verifikasi = array('name' => 'Menunggu Persetujuan', 'label' => 'label-warning'); break;
							    }
							?>
							<span class="label label-sm <?=$verifikasi['label']?>"> <?=$verifikasi['name']?> </span>
						    </td>
						    
						    <td>
							<?php
							    $verifikasi = array('name' => 'Menunggu Persetujuan', 'label' => 'label-warning');
							    
							    if(isset($row['head_instructor_approved']))
							    switch($row['head_instructor_approved']){
								case 0 : $verifikasi = array('name' => 'Menunggu Persetujuan', 'label' => 'label-warning'); break;
								case 1 : $verifikasi = array('name' => 'Disetujui', 'label' => 'label-success'); break;
								case -1 : $verifikasi = array('name' => 'Klarifikasi', 'label' => 'label-warning'); break;
								case -2 : $verifikasi = array('name' => 'Tolak', 'label' => 'label-danger'); break;
								default : $verifikasi = array('name' => 'Menunggu Persetujuan', 'label' => 'label-warning'); break;
							    }
							?>
							<span class="label label-sm <?=$verifikasi['label']?>"> <?=$verifikasi['name']?> </span>
						    </td>
						    
						    <td>
							<?php
							    $verifikasi = array('name' => 'Menunggu Persetujuan', 'label' => 'label-warning');
							    
							    if(isset($row['admin_approved']))
							    switch($row['admin_approved']){
								case 0 : $verifikasi = array('name' => 'Menunggu Persetujuan', 'label' => 'label-warning'); break;
								case 1 : $verifikasi = array('name' => 'Disetujui', 'label' => 'label-success'); break;
								case -1 : $verifikasi = array('name' => 'Klarifikasi', 'label' => 'label-warning'); break;
								case -2 : $verifikasi = array('name' => 'Tolak', 'label' => 'label-danger'); break;
								default : $verifikasi = array('name' => 'Menunggu Persetujuan', 'label' => 'label-warning'); break;
							    }
							?>
							<span class="label label-sm <?=$verifikasi['label']?>"> <?=$verifikasi['name']?> </span>
						    </td>
						    <td>
							<div class="btn-group" role="group" aria-label="...">
							    <button 
							    type="button" 
							    class="btn default btn-xs purple review_instructor"
							    data-url-submit="<?=site_url('narasumber/management_training/confirm_post/'.$row['training_id'])?>"
							    data-toggle="modal" 
							    data-training_id="<?=$row['training_id']?>" 
							    data-user_id="<?=$row['instructor_id']?>" 
							    
							    data-target="#review_instructor">
								    <i class="fa fa-eye"></i> Review 
							    </button>
								
								<div class="margin-bottom-5">
														<a href='<?=site_url('admin/management_training/training/detail/'.$row['training_id'])?>' class='btn default btn-xs'><i class="fa fa-graduation-cap"></i> Detail</a>
								</div>
							</div>
						    </td>
						</tr>
						<?php endforeach;?>
						
						</tbody>
						</table>
						 
