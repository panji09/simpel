
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="<?=$this->config->item('plugin')?>/select2/select2.min.js"></script>
<script type="text/javascript" src="<?=$this->config->item('plugin')?>/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?=$this->config->item('plugin')?>/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<script type="text/javascript" src="<?=$this->config->item('connect_api_base_url')?>/assets/global/plugins/typeahead/typeahead.bundle.min.js"></script>
<script type="text/javascript" src="<?=$this->config->item('connect_api_base_url')?>/assets/global/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js"></script>
<script type="text/javascript" src="<?=$this->config->item('plugin')?>/jquery-validation/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?=$this->config->item('plugin')?>/jquery-validation/js/additional-methods.min.js"></script>
<script type="text/javascript" src="<?=$this->config->item('plugin')?>/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
<script type="text/javascript" src="<?=$this->config->item('plugin')?>/moment.min.js"></script>
<script type="text/javascript" src="<?=$this->config->item('plugin')?>/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>

<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?=$this->config->item('admin_js')?>/metronic.js" type="text/javascript"></script>
<script src="<?=$this->config->item('admin_js')?>/layout.js" type="text/javascript"></script>
<script src="<?=$this->config->item('admin_js')?>/demo.js" type="text/javascript"></script>
<script src="<?=$this->config->item('admin_js')?>/table-managed.js"></script>
<script src="<?=$this->config->item('admin_js')?>/form-wizard.js"></script>
<script>



var URL_API = '<?=$this->config->item('connect_api_url')?>';
var handleTwitterTypeahead = function() {
	// Gelar akademis
    var gelarAkademis = new Bloodhound({
      datumTokenizer: function(d) { return Bloodhound.tokenizers.whitespace(d.name); },
      queryTokenizer: Bloodhound.tokenizers.whitespace,
      limit: 10,
      prefetch: {
        url: URL_API+'/academic/academic_degree/sort_name',
        filter: function(list) {
          return $.map(list, function(country) { return { name: country }; });
        }
      }
    });

    gelarAkademis.initialize();

    if (Metronic.isRTL()) {
      $('#academic_degree').attr("dir", "rtl");
    }
	
	
    // $('#academic_degree').typeahead(null, {
    //   name: 'academic_degree',
    //   displayKey: 'name',
    //   hint: (Metronic.isRTL() ? false : true),
    //   source: gelarAkademis.ttAdapter()
    // });

	$('#academic_degree').tagsinput({
	  typeaheadjs: {
	    name: 'academic_degree',
	    displayKey: 'name',
	    valueKey: 'name',
		hint: (Metronic.isRTL() ? false : true),
	    source: gelarAkademis.ttAdapter()
	  }
	});
	
	
	// Institusi
    var institution = new Bloodhound({
      datumTokenizer: function(d) { return Bloodhound.tokenizers.whitespace(d.name); },
      queryTokenizer: Bloodhound.tokenizers.whitespace,
      limit: 10,
      prefetch: {
        url: URL_API+'/institution',
        filter: function(list) {
          return $.map(list, function(country) { return { name: country }; });
        }
      }
    });

    institution.initialize();

    if (Metronic.isRTL()) {
      $('#institution').attr("dir", "rtl");
    }
    $('#institution').typeahead(null, {
      name: 'institution',
      displayKey: 'name',
      hint: (Metronic.isRTL() ? false : true),
      source: institution.ttAdapter()
    });
}

var handleProvinceData = function(){	
	
	//init province first
	$.ajax({
	    url: URL_API+"/location/province",
		type: "GET",
	    dataType: "jsonp",
	    // Work with the response
	    success: function( response ) {
			console.log(response);
			$.each(response, function(index, item){
				$("#domisili_provinsi_list_kerja").append("<option value='"+item.id+"'>"+item.name+"</option>");						
			});
	    }
	});
}
	
var getKota = function(id, component){
			
		$.ajax({
		    url: URL_API+"/location/district/" + id,
			type: "GET",
		    dataType: "jsonp",

		    // Work with the response
		    success: function( response ) {
				$.each(response, function(index, item){
					$(component).append("<option value='"+item.id+"'>"+item.name+"</option>");
				});
		    }
		});				
	}	
	


var handleStatusKepegawaian = function(){
	$.ajax({
	    url: URL_API+"/employment",
		type: "GET",
	    dataType: "jsonp",

	    // Work with the response
	    success: function( response ) {
			console.log(response);
			$.each(response, function(index, item){
				$("#status_kepegawaian_list").append("<option value='"+item+"'>"+item+"</option>");
			});
	    }
	});			
}

var handleLatestEducation = function(){
	
	$.ajax({
	    url: URL_API+"/academic/educational_level",
		type: "GET",
	    dataType: "jsonp",

	    // Work with the response
	    success: function( response ) {
			$.each(response, function(index, item){
				$("#list_last_education").append("<option value='"+item+"'>"+item+"</option>");
			});
	    }
	});			
}

var handleStatusEmployee = function(){
	$('.pns').hide();		
	$('#status_kepegawaian_list').on('change', function() {
		//clear old data
		var $this = $(this);
		if($this.val() == 'PNS' || $this.val() == 'TNI' || $this.val() == 'POLRI'){
			$('.pns').show();
		}else{
			$('.pns').hide();				
		}
	});
}
	  
	  jQuery(document).ready(function() {

		  Metronic.init(); // init metronic core components
		  Layout.init(); // init current layout
		  Demo.init(); // init demo features
		  TableManaged.init();
		  FormWizard.init();
		  //init
		  handleStatusKepegawaian();
		  handleStatusEmployee();
		  handleTwitterTypeahead();
		  handleLatestEducation();
		  handleProvinceData();
	  
		  $(document).on('click','.invite_user',function(){
		      modal = $(this).data('target');
		      $(modal).find('.modal-header h4').html($(this).data('title'));
		      $(modal).find('form').attr('action',$(this).data('url'));
		      
		  });
		  <?php
		  /*
		  $('#clear_avatar').click(function(){
		      $('#avatar').attr('src',$('#avatar').attr('data-default'));
		      $('#avatar').next().val('');
		  });
		  
		  $('#avatar').click(function(){
		      openKCFinder(this);
		  });
		  */
		  ?>
		  
		  $(document).on('click','#clear_avatar',function(){
		      $('#avatar').attr('src',$('#avatar').attr('data-default'));
		      $('#avatar').next().val('');
		  });
		  
		  $(document).on('click','#avatar',function(){
		      openKCFinder(this);
		  });
		  
		  function openKCFinder(div) {
		      window.KCFinder = {
			  callBack: function(url) {
			      window.KCFinder = null;
			      console.log(url);
			      $('#avatar').attr('src',url);
			      $(div).next().val(url);
			  }
		      };
			  
		      window.open('<?=$this->config->item("plugin")?>/kcfinder/browse.php?type=images',
			  'kcfinder_image', 'status=0, toolbar=0, location=0, menubar=0, ' +
			  'directories=0, resizable=1, scrollbars=0, width=800, height=600'
		      );
		  }
		  $('#datetimepicker1').datetimepicker({
			format: 'YYYY-MM-DD',
			viewMode : 'years'
		    });
			
			<?php
			/*
			$("input[name$='training_institute_status']").click(function() {
				if($(this).attr("value")=="pns"){
					$(".kotak").not(".negeri").hide();
					$("input.swasta").val("");	
					$(".negeri").show();
				} else {
					$(".kotak").not(".swasta").hide();
					$("input.negeri").val("");
					$(".swasta").show();
				}
			});
			*/
			?>
			$(document).on('click',"input[name$='training_institute_status']",function() {
				if($(this).attr("value")=="pns"){
					$(".kotak").not(".negeri").hide();
					$("input.swasta").val("");	
					$(".negeri").show();
				} else {
					$(".kotak").not(".swasta").hide();
					$("input.negeri").val("");
					$(".swasta").show();
				}
			});
			
			//set selection tool on change callback
			$('#domisili_provinsi_list_kerja').on('change', function() {
				//clear old data
				var $this = $(this);
				$('#domisili_kabupaten_list_kerja').empty();
				$("#domisili_kabupaten_list_kerja").select2('data', {id: null, text:""});
				$("#domisili_kabupaten_list_kerja").append("<option></option>");
			    getKota($this.val(), "#domisili_kabupaten_list_kerja");
				$('#domisili_kabupaten_list_kerja').attr("data-placeholder", "Pilih");
				$('#domisili_kabupaten_list_kerja').data("select2").setPlaceholder();
				//now clear kabupaten
				$("#domisili_kabupaten_list_kerja").empty();
				$("#domisili_kabupaten_list_kerja").select2('data', {id: null, text:""});
				$("#domisili_kabupaten_list_kerja").append("<option></option>");
				$('#domisili_kabupaten_list_kerja').attr("data-placeholder", "Pilih");
			$('#domisili_kabupaten_list_kerja').data("select2").setPlaceholder();
		});
	  
	  
	  
	  });
	  
	 
  
</script>