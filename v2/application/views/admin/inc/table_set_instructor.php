<table class="table table-striped table-hover" id='table_set_instructor'>
							<thead>
							<tr>
								
								<th>
									 Nama Pengajar
								</th>
								<th>
									 Tanggal 
								</th>
								<th>
									 Jam Pelajaran
								</th>
								<th>
									 Status Pengajar
								</th>
								<th>
									 Action
								</th>
							</tr>
							</thead>
							<tbody>
							<tr>
								<td style="width: 250px;">
									<div class="input-group" style="padding-right: 20px">
										<input 
										type="text" class="form-control input-sm" placeholder="Pilih Pengajar" readonly="">
										<input type='hidden' name='instructor_id[]' value='' class='set_instructor'>
										<span class="input-group-btn">
										<a 
										href="javascript:;" 
										class="btn default btn-sm select_instructor" 
										data-target="#select_instructor_modal"
										data-training_id = "<?=$training_id?>"
										data-toggle="modal" >
										<i class="fa fa-user"></i> </a>
										</span>
									</div>
								</td>
								<td>
									
									<a href="#"
	  							  	class="choose_instructor"
									data-target="#choose_instructor_modal"
	  							  	data-toggle="modal"
								  	data-training_id="<?=$training_id?>">
										<i class="fa fa-plus"></i> Tambah Tanggal
									</a>
									
								</td>
								<td>
									<input 
									type="text" name='hour_leasson[]' class="form-control input-sm" placeholder="ex: 3">
								</td>
								<td>
 									
 									<select name='main_instructor[]' class='form-control'>
									    <?php
										foreach($this->init_config_db->get_main_instructor() as $row):
									    ?>
									    <option value='<?=$row['id']?>'><?=$row['name']?></option>
									    <?php endforeach;?>
									    
 									</select>
								</td>
								<td>
									<a href="javascript:;" class="btn default btn-xs black remove_instructor">
									<i class="fa fa-trash-o "></i> Delete </a>
								</td>
							</tr>
							
							
							</tbody>
							</table>