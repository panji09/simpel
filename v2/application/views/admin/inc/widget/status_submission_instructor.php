
<table class="table table-striped table-bordered table-hover" id="table_approval_instructor_status">
	<thead>
		<tr>
			<th>
				  Tanggal
			</th>
			<th>
				  Tanggal Pelatihan
			</th>
			
			<th>
				  Nama Pelatihan
			</th>
			<th>
				  Jumlah Jam Pelajaran
			</th>
			
			<th>
				  Tanggal Ngajar
			</th>
			
			<th>
				  Konfirmasi Pengajar
			</th>
			
			<th>
				  Konfirmasi Atasan Pengajar
			</th>
			
			<th>
				  Konfirmasi Admin
			</th>
			<th>
				  Action
			</th>
		</tr>
	</thead>
<tbody>
<?php
    $query = $this->hour_leasson_db->get_all(array(
	'training_id' => $training_id
    ));

    if($query)
	foreach($query as $row):
?>
<tr class="odd gradeX">
    <td>
	<time class="timeago" datetime="<?=date('c',$row['created'])?>"><?=date("F j, Y, g:i a",$row['created']) ?></time>
    </td>
    
    <td>
	<?=$row['training_log']['training_date']?>
    </td>
    
    <td>
	<?=$row['training_log']['type_training']['name'].' / '.$row['training_log']['training_name']['name']?>
    </td>
    
    <td>
	<?=$row['hour_leasson']?>
    </td>
    
    <td>
	<?=implode(', ',$row['submission_date'])?>
    </td>
    
    <td>
	<?php
	    $verifikasi = array();
	    
	    if(isset($row['instructor_approved']))
	    switch($row['instructor_approved']){
		case 0 : $verifikasi = array('name' => 'Menunggu Persetujuan', 'label' => 'label-warning'); break;
		case 1 : $verifikasi = array('name' => 'Disetujui', 'label' => 'label-success'); break;
		case -1 : $verifikasi = array('name' => 'Klarifikasi', 'label' => 'label-warning'); break;
		case -2 : $verifikasi = array('name' => 'Tolak', 'label' => 'label-danger'); break;
		default : $verifikasi = array('name' => 'Menunggu Persetujuan', 'label' => 'label-warning'); break;
	    }
	?>
	<span class="label label-sm <?=$verifikasi['label']?>"> <?=$verifikasi['name']?> </span>
    </td>
    
    <td>
	<?php
	    $verifikasi = array('name' => 'Menunggu Persetujuan', 'label' => 'label-warning');
	    
	    if(isset($row['head_instructor_approved']))
	    switch($row['head_instructor_approved']){
		case 0 : $verifikasi = array('name' => 'Menunggu Persetujuan', 'label' => 'label-warning'); break;
		case 1 : $verifikasi = array('name' => 'Disetujui', 'label' => 'label-success'); break;
		case -1 : $verifikasi = array('name' => 'Klarifikasi', 'label' => 'label-warning'); break;
		case -2 : $verifikasi = array('name' => 'Tolak', 'label' => 'label-danger'); break;
		default : $verifikasi = array('name' => 'Menunggu Persetujuan', 'label' => 'label-warning'); break;
	    }
	?>
	<span class="label label-sm <?=$verifikasi['label']?>"> <?=$verifikasi['name']?> </span>
    </td>
    
    <td>
	<?php
	    $verifikasi = array('name' => 'Menunggu Persetujuan', 'label' => 'label-warning');
	    
	    if(isset($row['admin_approved']))
	    switch($row['admin_approved']){
		case 0 : $verifikasi = array('name' => 'Menunggu Persetujuan', 'label' => 'label-warning'); break;
		case 1 : $verifikasi = array('name' => 'Disetujui', 'label' => 'label-success'); break;
		case -1 : $verifikasi = array('name' => 'Klarifikasi', 'label' => 'label-warning'); break;
		case -2 : $verifikasi = array('name' => 'Tolak', 'label' => 'label-danger'); break;
		default : $verifikasi = array('name' => 'Menunggu Persetujuan', 'label' => 'label-warning'); break;
	    }
	?>
	<span class="label label-sm <?=$verifikasi['label']?>"> <?=$verifikasi['name']?> </span>
    </td>
    <td>
	<div class="btn-group" role="group" aria-label="...">
	    <button 
	    type="button" 
	    class="btn default btn-xs purple review_instructor"
	    data-url-submit="<?=site_url('narasumber/management_training/confirm_post/'.$row['training_id'])?>"
	    data-toggle="modal" 
	    data-training_id="<?=$row['training_id']?>" 
	    data-user_id="<?=$row['instructor_id']?>" 
	    
	    data-target="#review_instructor">
		    <i class="fa fa-eye"></i> Review 
	    </button>
	</div>
    </td>
</tr>
<?php endforeach;?>

</tbody>
</table> 
 
