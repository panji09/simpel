<?php
    $query = $this->training_db->get($training_id);
    if($query):
	$content = $query[0];
?>

<form id='todolist_form' method='post' action='<?=site_url('lpp/management_training/todolist_post/'.$training_id.'/'.$content['todolist'])?>'>
<div class="list-group todolist">
    <?php
	if(isset($content['todolist']))
	$query_todolist = $this->todolist_db->get($content['todolist']);
	$todolist = $query_todolist[0];
	$i=0;
	
	$todolist_user = (isset($content['todolist_user']) ? $content['todolist_user'] : array() );
	
// 	print_r($todolist_user );
	if(isset($todolist['tasklist']))
	foreach($todolist['tasklist'] as $row):
    ?>
    <li class="list-group-item"> <div class="checkbox">
  <label><input type="hidden" name='todolist_user[<?=$i?>][name]' value="<?=$row?>"> <input type="checkbox" class='todolist_task' name='todolist_user[<?=$i?>][data]' value="1" <?=(isset($todolist_user[$i]['data']) && $todolist_user[$i]['data'] == 1 ? 'checked' : '' ) 
?> <?=($this->connect_auth->get_me()['entity'] == 'lpp' ? '' : 'disabled')?>><?=$row?></label>
</div></li>
    <?php $i++; endforeach;?>
    
    <?php if($this->connect_auth->get_me()['entity'] == 'lpp'):?>
     <li class="list-group-item"> 
	<button type='submit' class='btn btn-primary btn-block'>Simpan</button>
    </li>
    <?php endif;?>
</div>
</form>
<?php endif;?>