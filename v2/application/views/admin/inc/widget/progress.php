<?php
    $query = $this->training_db->get($training_id);
    if($query):
// 	print_r($response_body);
	$content = $query[0];
?>
<div class="row bs-wizard" style="border-bottom:0; margin-top: 0">

    <?php
    //complete
    //active
    //disabled
    ?>

    <?php
	$step1 = 'disabled';
	$step2 = 'disabled';
	$step3 = 'disabled';
	$step4 = 'disabled';
	if($content['verified_submission_training'] == '1'){
	    $step1 = 'complete';
	    $step2 = 'active';
	}elseif($content['verified_submission_training'] == '-2'){
	    $step1 = 'disabled';
	}else{
	    $step1 = 'active';
	}
    ?>
    <div class="col-xs-3 bs-wizard-step <?=$step1?>">
      <div class="text-center bs-wizard-stepnum">Tahap Pengajuan</div>
      <div class="progress"><div class="progress-bar"></div></div>
      <a href="#" class="bs-wizard-dot"></a>
      <div class="bs-wizard-info text-center">Pengajuan Pelatihan direview oleh admin</div>
    </div>

    <div class="col-xs-3 bs-wizard-step <?=$step2?>"><!-- complete -->
      <div class="text-center bs-wizard-stepnum">Tahap Persiapan</div>
      <div class="progress"><div class="progress-bar"></div></div>
      <a href="#" class="bs-wizard-dot"></a>
      <div class="bs-wizard-info text-center">Pengajuan sudah disetujui, Lembaga Pelatihan mempersiapkan data peserta, Pihak LKPP sedang menyiapkan Narasumber/Pengajar</div>
    </div>

    <div class="col-xs-3 bs-wizard-step <?=$step3?>"><!-- complete -->
      <div class="text-center bs-wizard-stepnum">Tahap Pelaksanaan</div>
      <div class="progress"><div class="progress-bar"></div></div>
      <a href="#" class="bs-wizard-dot"></a>
      <div class="bs-wizard-info text-center">Pelatihan sedang berlangsung</div>
    </div>

    <div class="col-xs-3 bs-wizard-step <?=$step4?>"><!-- active -->
      <div class="text-center bs-wizard-stepnum">Tahap Evaluasi</div>
      <div class="progress"><div class="progress-bar"></div></div>
      <a href="#" class="bs-wizard-dot"></a>
      <div class="bs-wizard-info text-center"> Lembaga Pelatihan menginputkan evaluasi pelatihan</div>
    </div>
</div>

<?php endif;?> 
