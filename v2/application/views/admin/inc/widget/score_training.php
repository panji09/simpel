<?php
    $query = $this->training_db->get($training_id);
    if($query):
	$content = $query[0];

    $query_score_training = $this->score_training_db->get_all(array(
	'training_id' => $training_id
    ));
    $score_training = array();
    if($query_score_training && count($query_score_training) == 1){
	$score_training = $query_score_training[0];
    }
?>

<form id='score_training_form' method='post' action='<?=site_url('lpp/management_training/score_training_post/'.(isset($score_training['_id']->{'$id'}) ? $score_training['_id']->{'$id'} : ''))?>'>
<input type='hidden' name='training_id' value='<?=$training_id?>'>
<table class="table table-striped">
<thead>
    <th>Nama</th>
    <th>Nilai Pre-test</th>
    <th>Nilai Post-test</th>
    
</thead>
<tbody>
    <?php
	$query = $this->join_training_db->get_all(array(
	    'training_id' => $training_id,
	    'institute_approved' => 1
	));
	
	if($query)
	foreach($query as $row):
	
    ?>
    <tr>
	<td><p class='form-control-static'><?=$row['user_join']['fullname']?></p></td>
	<td>
	    <div class="form-group">
	      <input 
		  type="text" 
		  class="form-control" 
		  name='data[<?=$row['user_join']['user_id']?>][pretest]' 
		  <?php
		      $value_data = (isset($score_training['data'][$row['user_join']['user_id']]['pretest']) ? $score_training['data'][$row['user_join']['user_id']]['pretest'] : '');
		  ?>
		  value='<?=$value_data?>'
		  
		  <?php if($this->connect_auth->get_me()['entity'] != 'lpp'):?>
		  disabled
		  <?php endif;?>
	      >
	    
	    </div>
	</td>
	<td>
	    <div class="form-group">
	      <input 
		  type="text" 
		  class="form-control" 
		  name='data[<?=$row['user_join']['user_id']?>][posttest]' 
		  <?php
		      $value_data = (isset($score_training['data'][$row['user_join']['user_id']]['posttest']) ? $score_training['data'][$row['user_join']['user_id']]['posttest'] : '');
		  ?>
		  value='<?=$value_data?>'
		  
		  <?php if($this->connect_auth->get_me()['entity'] != 'lpp'):?>
		  disabled
		  <?php endif;?>
	      >
	    </div>
	</td>
	
	
    </tr>
    <?php endforeach;?>
</tbody>
</table>

<?php if($this->connect_auth->get_me()['entity'] == 'lpp'):?>
<div class="form-actions">
	<div class="row">
		<div class="col-md-offset-3 col-md-9">
			<button id="btn_save" type="submit" class="btn btn-primary" >Simpan</button>
			
			
		</div>
	</div>
</div>
<?php endif;?>
</form>
<?php endif;?> 
