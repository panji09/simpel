
	                               <div class="form-body">
	                                  <div class="form-group form-md-line-input">
	                                     <label class="col-md-2 control-label" for="form_control_1">Nama Lembaga</label>
	                                     <div class="col-md-10">
	                                        <p class="form-control-static">
	                                        <?=(isset($user['training_institute']) && $user['training_institute'] ? $user['training_institute'] : '')?>
	                                        </p>
	                                     </div>
	                                  </div>
	                                  <div class="form-group form-md-line-input">
	                                     <label class="col-md-2 control-label" for="form_control_1">Status Lembaga Pelatihan:</label>
	                                     <div class="col-md-10">
	                                        <p class="form-control-static">
	                                        <?=(isset($user['training_institute_status']) && $user['training_institute_status'] ? $user['training_institute_status'] : '')?>
	                                        </p>
	                                     </div>
	                                  </div>
	                                  <div class="form-group form-md-line-input">
	                                     <label class="col-md-2 control-label" for="form_control_1">Akreditasi</label>
	                                     <div class="col-md-10">
	                                        <p class="form-control-static">
	                                        <?=(isset($user['accreditation_level']) && $user['accreditation_level'] ? $user['accreditation_level'] : '')?>
	                                        </p>
	                                     </div>
	                                  </div>
	                                  <div class="form-group form-md-line-input">
	                                     <label class="col-md-2 control-label" for="form_control_1">Alamat Kantor Pelatihan:</label>
	                                     <div class="col-md-10">
	                                        <p class="form-control-static">
	                                        <?=(isset($user['office_address']) && $user['office_address'] ? $user['office_address'] : '')?>
	                                        </p>
	                                     </div>
	                                  </div>
	                                  <div class="form-group form-md-line-input">
	                                     <label class="col-md-2 control-label" for="form_control_1">&nbsp;</label>
	                                     <div class="col-md-10">
	                                       <button 
	                                          type="button" 
	                                          class="btn btn-circle btn-default review-institution" 
	                                          data-toggle="modal" 
	                                          data-user_id="<?=(isset($training['creator']['user_id']) ? $training['creator']['user_id'] : '' )?>" 
	                                          data-target="#review_institution"
	                                          >Detail Lembaga Pelatihan</button>
	                                     </div>
	                                  </div>
	                               </div>
	                             
