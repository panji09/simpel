
<div class="form-body">
    <div class="form-group form-md-line-input">
      <label class="col-md-2 control-label" for="form_control_1">Nama Pelatihan</label>
      <div class="col-md-10">
	  <p class="form-control-static">
	  <?=(isset($training['training_name']['name']) ? $training['training_name']['name'] : '' )?>
	  </p>
      </div>
    </div>
    <div class="form-group form-md-line-input">
      <label class="col-md-2 control-label" for="form_control_1">Jenis Pelatihan</label>
      <div class="col-md-10">
	  <p class="form-control-static">
	  <?=(isset($training['type_training']['name']) ? $training['type_training']['name'] : '' )?>
	  </p>
      </div>
    </div>
    <div class="form-group form-md-line-input">
      <label class="col-md-2 control-label" for="form_control_1">Tanggal Pelatihan Pelatihan:</label>
      <div class="col-md-10">
	  <p class="form-control-static">
	  <?=(isset($training['training_date']) ? $training['training_date'] : '' )?>
	  </p>
      </div>
    </div>
    
    <div class="form-group form-md-line-input">
      <label class="col-md-2 control-label" for="form_control_1">Modul Bahan Ajar</label>
      <div class="col-md-10">
	  <p class="form-control-static">
	  <?php
	    $query_program_training = $this->config_training_db->get($training['training_name']['id']);
	    
	    if($query_program_training):
		$program_training = $query_program_training[0];
	  ?>
	  
	  <?=(isset($program_training['teaching_material_upload']) ? anchor($program_training['teaching_material_upload'], basename(urldecode($program_training['teaching_material_upload'])), array('target' => '_blank')) : '' )?>
	  
	  <?php endif;?>
	  </p>
      </div>
    </div>
	                                   
    <div class="form-group form-md-line-input">
      <label class="col-md-2 control-label" for="form_control_1">&nbsp;</label>
      <div class="col-md-10">
	  <button 
	    type="button" 
	    class="btn btn-circle btn-default  review_training_submission"
	    data-url-submit="<?=site_url('lpp/management_training/training_post/'.$training['_id']->{'$id'})?>" 
	    data-toggle="modal" 
	    data-training_id="<?=$training['_id']->{'$id'}?>" 
	    data-target="#review_training_submission">
	  Detail Pengajuan Pelatihan 
	  </button>
      </div>
    </div>
    
</div>
	                              
