<?php $page=$this->uri->segment(2); $page1 = $this->uri->segment(3);?>
<div class="page-sidebar-wrapper">
    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
    <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
    <div class="page-sidebar navbar-collapse collapse">
        <!-- BEGIN SIDEBAR MENU -->
        <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
        <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
        <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
        <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
        <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
        <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
        <ul class="page-sidebar-menu " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
            <li <?=($page=='dashboard' ? 'class="start active "' : '')?>>
                <a href="<?=site_url('admin/dashboard')?>">
                    <i class="icon-home"></i>
                    <span class="title">Dashboard</span>
                </a>
            </li>
            <li <?=($page=='approval' ? 'class="open active "' : '')?>>
                <a href="javascript:;">
                    <i class="icon-fire"></i>
                    <span class="title">Approval</span>
                    <span class="arrow "></span>
                </a>
                <ul class="sub-menu">
                    <li <?=($page1=='institution' ? 'class="active "' : '')?>>
			<?php
			//get lpp yang blom di verifikasi
			$response = call_api_get($this->config->item('connect_api_url').'/user/count_verified_institute_lpp/0');
			$response_body = json_decode($response['body'], true);    
// 			print_r($response_body);
			$count_unverified_institute_lpp = 0;
			if($response['header_info']['http_code'] == 200){
			    $count_unverified_institute_lpp = $response_body['count'];
			}
			?>
                        <a href="<?=site_url('admin/approval/institution')?>">Lembaga Pelatihan / LPP <?=($count_unverified_institute_lpp ? '<span class="badge">'.$count_unverified_institute_lpp.'</span>' : '')?></a>
                    </li>

                    <li <?=($page1=='submission_training' ? 'class="active "' : '')?>>
                    
			<?php
			//get pengajuan pelatihan yg blom di verifikasi
			$query = $this->training_db->get_all(array('verified_submission_training' => 0));
			$count_unverified_submission_training = 0;
			if($query){
			    $count_unverified_submission_training = count($query);
			}
			?>
			
                        <a href="<?=site_url('admin/approval/submission_training')?>">Pengajuan Pelatihan <?=($count_unverified_submission_training ? '<span class="badge">'.$count_unverified_submission_training.'</span>' : '')?></a>
                    </li>
                    
                    <li <?=($page1=='instructor' ? 'class="active "' : '')?>>
                        
                        <?php
			//get pengajuan pelatihan yg blom di verifikasi
			$query = $this->hour_leasson_db->get_all(array('admin_approved' => 0));
			$count_unverified_hour_leasson_instructor = 0;
			if($query){
			    $count_unverified_hour_leasson_instructor = count($query);
			}
			?>
			
                        <a href="<?=site_url('admin/approval/instructor')?>">Pengajuan Pengajar <?=($count_unverified_hour_leasson_instructor ? '<span class="badge">'.$count_unverified_hour_leasson_instructor.'</span>' : '')?></a>
                    </li>

                </ul>
            </li>
            <li <?=($page=='evaluation' ? 'class="open active "' : '')?>>
                <a href="javascript:;">
                    <i class="icon-check"></i>
                    <span class="title">Evaluasi</span>
                    <span class="arrow "></span>
                </a>
                <ul class="sub-menu">
                    <li <?=($page1=='training' ? 'class="active "' : '')?>>
                        <a href="<?=site_url('admin/evaluation/training')?>">Rekap </a>
                    </li>

                    <li <?=($page1=='form' ? 'class="active "' : '')?>>
                        <a href="<?=site_url('admin/evaluation/form')?>">Form Evaluasi </a>
                    </li>                                   

                </ul>
            </li>			
			
			<li <?=($page=='complaint_handling' ? 'class="start active "' : '')?>>
                <a href="<?=site_url('admin/complaint_handling')?>">
                    <i class="fa fa-bullhorn"></i>
                    <span class="title">Pengaduan</span>
                </a>
            </li>
			
            <li <?=($page=='statistic' ? 'class="start active "' : '')?>>
                <a href="<?=site_url('admin/statistic')?>">
                    <i class="icon-bar-chart"></i>
                    <span class="title">Statistik</span>
                </a>
            </li>
            

        </ul>
        <!-- END SIDEBAR MENU -->
    </div>
</div>