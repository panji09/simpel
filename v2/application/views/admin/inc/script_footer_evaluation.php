
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="<?=$this->config->item('plugin')?>/select2/select2.min.js"></script>
<script type="text/javascript" src="<?=$this->config->item('plugin')?>/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?=$this->config->item('plugin')?>/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>

<script type="text/javascript" src="<?=$this->config->item('plugin')?>/jquery-validation/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?=$this->config->item('plugin')?>/jquery-validation/js/additional-methods.min.js"></script>
<script type="text/javascript" src="<?=$this->config->item('plugin')?>/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
<script type="text/javascript" src="<?=$this->config->item('plugin')?>/moment.min.js"></script>
<script type="text/javascript" src="<?=$this->config->item('plugin')?>/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>

<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?=$this->config->item('admin_js')?>/metronic.js" type="text/javascript"></script>
<script src="<?=$this->config->item('admin_js')?>/layout.js" type="text/javascript"></script>
<script src="<?=$this->config->item('admin_js')?>/demo.js" type="text/javascript"></script>
<script src="<?=$this->config->item('admin_js')?>/table-managed.js"></script>
<script src="<?=$this->config->item('admin_js')?>/form-wizard.js"></script>
<script>
	  
	  $(document).ready(function() {

		  Metronic.init(); // init metronic core components
		  Layout.init(); // init current layout
		  Demo.init(); // init demo features
		  TableManaged.init();
		  FormWizard.init();
		  
	  });
	  
	  var param = null;
	  var tab_content = null;
	  var user_id = null;
	  var segmentContainer = null;
	  
	  $(document).ready(function() {

		  
		  
		  $(document).on('click','.remove_section', function(){
		      $(this).parent().parent().parent().parent().remove();
		  });
		  
		  $(document).on('click','.remove_question', function(){
		      $(this).parent().parent().remove();
		  });
		  
		  $(document).on('click','.add_question', function(){
		      var i = $('.add_question').index(this);
// 		      alert(i);
		      var segment = $('#template_segment_question').children().clone(true,true);	
		      $(segment).find('input').attr("name","question["+i+"][]");
		      $(segment).find('input').val('');
		      segmentContainer = $(this).parent().parent().parent().parent();
		      segmentContainer.find('.content_segment').append(segment);
		      
		  });
		  
		  
		  $(document).on('click','#add_section',function(){
		      var section = $('#template_section').children().clone(true,true);			
		      $('#content_section').append(section);
		  })
		  
		  <?php
		  /*
		  $('#clear_avatar').click(function(){
		      $('#avatar').attr('src',$('#avatar').attr('data-default'));
		      $('#avatar').next().val('');
		  });
		  
		  $('#avatar').click(function(){
		      openKCFinder(this);
		  });
		  */
		  ?>
		  
		  $(document).on('click','#clear_avatar',function(){
		      $('#avatar').attr('src',$('#avatar').attr('data-default'));
		      $('#avatar').next().val('');
		  });
		  
		  $(document).on('click','#avatar',function(){
		      openKCFinder(this);
		  });
		  
		  function openKCFinder(div) {
		      window.KCFinder = {
			  callBack: function(url) {
			      window.KCFinder = null;
			      console.log(url);
			      $('#avatar').attr('src',url);
			      $(div).next().val(url);
			  }
		      };
		      window.open('<?=$this->config->item("plugin")?>/kcfinder/browse.php?type=images',
			  'kcfinder_image', 'status=0, toolbar=0, location=0, menubar=0, ' +
			  'directories=0, resizable=1, scrollbars=0, width=800, height=600'
		      );
		  }
	  
		  $('#datetimepicker1').datetimepicker({
			format: 'YYYY-MM-DD',
			viewMode : 'years'
		    });
			


			
		    
		  
	  });//end document
	  
  
</script>