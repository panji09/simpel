<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link rel="shortcut icon" href="<?=$this->config->item('home_img')?>/ico/favicon.png">
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
<link href="<?=$this->config->item('plugin')?>/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<link href="<?=$this->config->item('plugin')?>/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css"/>
<link href="<?=$this->config->item('plugin')?>/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="<?=$this->config->item('plugin')?>/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
<link href="<?=$this->config->item('plugin')?>/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css"/>
<!-- END GLOBAL MANDATORY STYLES -->

<!-- BEGIN PAGE LEVEL PLUGIN STYLES -->
<link href="<?=$this->config->item('plugin')?>/bootstrap-daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css"/>
<link href="<?=$this->config->item('plugin')?>/fullcalendar/fullcalendar.min.css" rel="stylesheet" type="text/css"/>
<link href="<?=$this->config->item('plugin')?>/jqvmap/jqvmap/jqvmap.css" rel="stylesheet" type="text/css"/>
<link href="<?=$this->config->item('plugin')?>/morris/morris.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="<?=$this->config->item('plugin')?>/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="<?=$this->config->item('plugin')?>/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
<link rel="stylesheet" type="text/css" href="<?=$this->config->item('plugin')?>/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css"/>
<link href="<?=$this->config->item('plugin')?>/fancybox/source/jquery.fancybox.css" rel="stylesheet" type="text/css"/>



<link rel="stylesheet" type="text/css" href="<?=$this->config->item('plugin')?>/bootstrap-datepicker/css/bootstrap-datepicker3.min.css"/>
<link rel="stylesheet" type="text/css" href="<?=$this->config->item('plugin')?>/bootstrap-timepicker/css/bootstrap-timepicker.min.css"/>
<!-- <link rel="stylesheet" type="text/css" href="<?=$this->config->item('plugin')?>/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css"/> -->
<!-- END PAGE LEVEL PLUGIN STYLES -->

<!-- BEGIN PAGE STYLES -->
<link href="<?=$this->config->item('admin_css')?>/tasks.css" rel="stylesheet" type="text/css"/>
<!-- END PAGE STYLES -->

<!-- BEGIN THEME STYLES -->
<link href="<?=$this->config->item('admin_css')?>/components-rounded.css" id="style_components" rel="stylesheet" type="text/css"/>
<link href="<?=$this->config->item('admin_css')?>/plugins.css" rel="stylesheet" type="text/css"/>
<link href="<?=$this->config->item('admin_css')?>/layout.css" rel="stylesheet" type="text/css"/>
<link href="<?=$this->config->item('admin_css')?>/themes/light.css" rel="stylesheet" type="text/css" id="style_color"/>
<link href="<?=$this->config->item('admin_css')?>/custom.css" rel="stylesheet" type="text/css"/>
<!-- css for tags and bloodhund -->
<link href="<?=$this->config->item('connect_api_base_url')?>/assets/global/plugins/typeahead/typeahead.css" rel="stylesheet" type="text/css"/>
<link href="<?=$this->config->item('connect_api_base_url')?>/assets/global/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css" rel="stylesheet" type="text/css"/>
<link href="<?=$this->config->item('home_css')?>/progress-step.css" rel="stylesheet" type="text/css">

<link rel="stylesheet" href="<?=$this->config->item('plugin')?>/css-spinners/css/spinner/spinner.css" type="text/css">
<link rel="stylesheet" href="<?=$this->config->item('plugin')?>/css-spinners/css/spinner/ball.css" type="text/css">
<link rel="stylesheet" href="<?=$this->config->item('plugin')?>/css-spinners/css/spinner/circles.css" type="text/css">
<!-- END THEME STYLES -->

<style>
.portlet.light{
    padding: 12px 5px 15px;
}
.checkbox{
    padding-left:10px;
}

.tab_approved{
    background-color:#3598dc;
    color:white;
}

.tab_clarification{
    background-color:#c49f47;
    color:white;
}

.tab_rejected{
    background-color:#cb5a5e;
    color:white;
}
.modal .modal-body.slim{
    padding:5px;
}
.modal .portlet.light {
    padding: 5px;
}
</style>