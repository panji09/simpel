<?php
    $set_template = (isset($set_template) ? $set_template : false);
    $query = $content = array();
    if($form_id){
	$query = $this->evaluation_db->get($form_id);
	if($query)
	    $content = $query[0];
    
	
    }
    
?>

<?php
    $i = 0;
    if(!$set_template && isset($content['section'])):
    foreach($content['section'] as $section):
?>
<!-- BEGIN Portlet SECTION-->
<div class="portlet light" data-template="section" data-subject="lpp">
	<div class="portlet-title">
		<div class="caption" style="padding: 5px 0; width: 220px;">
			<!-- <i class="fa fa-puzzle-piece"></i>
			<span class="caption-subject bold uppercase"> Section </span> -->
			<input data-section="section_title" name='section[]' type="text" class="form-control input-sm" placeholder="contoh: Sarana dan Prasarana" value='<?=( $section ? $section : '' )?>'>
		</div>
		<div class="actions">					
			<div class="btn-group">
				<button type="button" class="btn btn-sm blue add_question"><i class="fa fa-plus"></i> Tambah Pertanyaan </i></button>
				
				<button type="button" class="btn btn-sm red remove_section"><i class="fa fa-minus"></i> Hapus Section </i></button>
			</div>
		</div>
	</div>

	<div class="portlet-body">
		<div class='content_segment form-horizontal'>
		    <?php
			if(isset($content['question'][$i]))
			foreach($content['question'][$i] as $question):
		    
			$this->load->view('admin/inc/template_segment_question', array('index_section' => $i, 'question' => $question));
			
		    ?>
			
		    <?php endforeach;?>
		</div>
	</div>
</div>
<!-- END Portlet SECTION-->
<?php $i++; endforeach;?>

<?php else:?>

<!-- BEGIN Portlet SECTION-->
<div class="portlet light" data-template="section" data-subject="lpp">
	<div class="portlet-title">
		<div class="caption" style="padding: 5px 0; width: 220px;">
			<!-- <i class="fa fa-puzzle-piece"></i>
			<span class="caption-subject bold uppercase"> Section </span> -->
			<input data-section="section_title" name='section[]' type="text" class="form-control input-sm" placeholder="contoh: Sarana dan Prasarana" value=''>
		</div>
		<div class="actions">					
			<div class="btn-group">
				<button type="button" class="btn btn-sm blue add_question"><i class="fa fa-plus"></i> Tambah Pertanyaan </i></button>
				
				<button type="button" class="btn btn-sm red remove_section"><i class="fa fa-minus"></i> Hapus Section </i></button>
			</div>
		</div>
	</div>

	<div class="portlet-body">
		<div class='content_segment form-horizontal'></div>
	</div>
</div>
<!-- END Portlet SECTION-->

<?php endif;?>
