<table class="table table-striped table-bordered table-hover" id="table_user_admin">
						<thead>
						<tr>
							
							<th>
								 Fullname
							</th>
							<th>
								 Username
							</th>
							<th>
								 Email
							</th>
							<th>
								 Join
							</th>
							
							<th>
								 Last Login
							</th>
							
							<th>
								 Status
							</th>
							<th>
								 Action
							</th>
						</tr>
						</thead>
						<tbody>
						<?php
						    $response = call_api_get($this->config->item('connect_api_url').'/user/all/'.$token.'/'.$entity);
						    $response_body = json_decode($response['body'], true);
						    
						    if($response['header_info']['http_code'] == 200):
						    foreach($response_body['data'] as $row):
						?>
						<tr class="odd gradeX">
						    <td><?=$row['fullname']?></td>
						    <td><?=$row['username']?></td>
						    <td>
							<a href="mailto:<?=$row['email']?>"><?=$row['email']?> </a>
						    </td>
						    <td><time  datetime="<?=date("d/m/Y H:i:s",$row['created'])?>"><?= date("d/m/Y H:i:s",$row['created']) ?></time></td>
						    
							<td><time  datetime="<?php
																		if(isset($row['last_login']['time']))
																		   echo date("d/m/Y H:i:s",$row['last_login']['time']);
																		else
																		   echo '-'; ?>">
							
							<?php
																		if(isset($row['last_login']['time']))
																		   echo date("d/m/Y H:i:s",$row['last_login']['time']);
																		else
																		   echo '-';
							
							?></time></td>
						    
							<td>
							<span class="label label-sm <?=($row['activated'] ? 'label-success' : ($row['verified_email'] ? 'label-danger' : 'label-warning'))?>"> <?=($row['activated'] ? 'Aktif' : ($row['verified_email'] ? 'Non-Aktif' : 'Menunggu verifikasi email'))?> </span>
						    </td>
						    <td>
							<div class="btn-group">
							    <?php
							    /*
							    <button type="button" class="btn btn-default" onclick="location.href='<?=site_url('superadmin/management_user/'.$page.'/edit/'.$row['user_id'])?>'">Edit</button>
							    */
							    ?>
							    <button type="button" class="btn btn-default" onclick="location.href='<?=$this->config->item('connect_url').'/user/edit/'.$row['user_id'].'?access_token='.$this->connect_auth->get_access_token()?>'">Edit</button>

							    <button type="button" class="btn btn-default status-user" data-url-active="<?=site_url('superadmin/management_user/'.$page.'/status/'.$row['user_id'].'/?activated=1')?>" data-url-nonactive="<?=site_url('superadmin/management_user/'.$page.'/status/'.$row['user_id'].'/?activated=0')?>" data-toggle="modal" data-target="#status">Status</button>
							    
							    <button type="button" class="btn btn-default delete-user" data-url-delete="<?=site_url('superadmin/management_user/'.$page.'/delete/'.$row['user_id'])?>" data-toggle="modal" data-target="#delete">Delete</button>
							</div>
						    </td>
						</tr>
						<?php endforeach; endif;?>
						
						</tbody>
						</table>