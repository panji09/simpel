<?php 
    $value = '';
    $name = 'question';
    if(isset($index_section) && isset($question) ){
	$value = $question;
	$name = "question[".$index_section."][]";
    }

?>

<div class="form-group">
    <label class="col-sm-2 control-label">Pertanyaan</label>
    <div class="col-sm-9">
	<input type="text" class="form-control" placeholder="Isi pertanyaan" value='<?=$value?>' name="<?=$name?>" data-required="1">
    </div>
    <div class="col-sm-1">
	<button type='button' class='btn btn-sm red btn-block remove_question'>x</button>
    </div>
    
</div>
