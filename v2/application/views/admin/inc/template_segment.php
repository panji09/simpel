<div class="portlet light bg-inverse" data-template="segment" data-type="module">
	<div class="portlet-title">
		<div class="caption">
			Segment
		</div>
		<div class="tools">
			<a href="javascript:;" class="collapse">
			</a>
			<a href="#portlet-config" data-toggle="modal" class="config">
			</a>
			<a href="javascript:;" class="remove">
			</a>
		</div>
	</div>
	<div class="portlet-body">		
		<!-- BEGIN SEGMENT CONTENT -->
		<div class="form-body">
			<div class="form-group">
				<!-- BEGIN SEGMENT CONTENT -->
				<div class="form-body">
					<table class="table table-hover">
						<thead>
							<tr>
								<th>
									 Tipe Modul
								</th>
								<th>
									 Nama
								</th>
								<th>
									 Action
								</th>
							</tr>
						</thead>
						<tbody>
							<tr class="module_id">
								<td>
									 <label class="module_type"></label>
								</td>
								<td>
									<label class="module_name"></label>
								</td>
								<td>
									<a href="javascript:;" class="btn default btn-xs black delete_module">
									<i class="fa fa-trash-o"></i> Delete </a>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<!-- END SEGMENT CONTENT -->
				
			</div>			
		</div>
		
	</div>
</div>












