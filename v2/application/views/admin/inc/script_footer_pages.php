<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="<?=$this->config->item('plugin')?>/select2/select2.min.js"></script>
<script type="text/javascript" src="<?=$this->config->item('plugin')?>/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?=$this->config->item('plugin')?>/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<script src="<?=$this->config->item('plugin')?>/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?=$this->config->item('plugin')?>/moment.min.js"></script>
<script type="text/javascript" src="<?=$this->config->item('plugin')?>/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript" src="<?=$this->config->item('plugin')?>/fancybox/source/jquery.fancybox.pack.js"></script>

<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?=$this->config->item('admin_js')?>/metronic.js" type="text/javascript"></script>
<script src="<?=$this->config->item('admin_js')?>/layout.js" type="text/javascript"></script>
<script src="<?=$this->config->item('admin_js')?>/demo.js" type="text/javascript"></script>
<script src="<?=$this->config->item('admin_js')?>/table-managed.js"></script>


<!-- Tinymce WYSIWYG Editors -->
<script type="text/javascript" src="<?=$this->config->item('plugin')?>/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
    function load_tinymce(){
	tinyMCE.editors = [];
	
    
	tinymce.init({
	    selector: "#wysiwyg-content",
		    theme: "modern",
		    skin: "light",
		    plugins:[
			    "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
			    "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
			    "save table contextmenu directionality emoticons template paste textcolor importcss"
		    ],
		    toolbar:[
			    "undo redo | styleselect | sizeselect | bold italic |  fontsizeselect | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media fullpage | forecolor backcolor emoticons table"
			    ],
		    file_browser_callback: function(field, url, type, win) {
		    tinyMCE.activeEditor.windowManager.open({
			file: '<?=$this->config->item("plugin")?>/kcfinder/browse.php?opener=tinymce4&field=' + field + '&type=' + type+'s',					
			title: 'KCFinder',
			width: 700,
			height: 500,
			inline: true,
			close_previous: false
		    }, {
			window: win,
			input: field
		    });
		    return false;
		    }
	});
	
	tinymce.init({
	    selector: ".wysiwyg-content",
		    theme: "modern",
		    skin: "light",
		    plugins:[
			    "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
			    "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
			    "save table contextmenu directionality emoticons template paste textcolor importcss"
		    ],
		    toolbar:[
			    "undo redo | styleselect | bold italic | fontsizeselect | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media fullpage | forecolor backcolor emoticons table"
			    ],
		    file_browser_callback: function(field, url, type, win) {
		    tinyMCE.activeEditor.windowManager.open({
			file: '<?=$this->config->item("plugin")?>/kcfinder/browse.php?opener=tinymce4&field=' + field + '&type=' + type+'s',					
			title: 'KCFinder',
			width: 700,
			height: 500,
			inline: true,
			close_previous: false
		    }, {
			window: win,
			input: field
		    });
		    return false;
		    }
	});
	
	tinymce.init({
	    selector: "#wysiwyg-short",
		    theme: "modern",
		    skin: "light",
		    menubar:false,
		    statusbar: false,
		    toolbar:["bold italic underline"],
		    paste_data_images: true
	});
    
    }
</script>

<!-- END Tinymce editor -->
	
<script>
var URL = null;
jQuery(document).ready(function() {       
    Metronic.init(); // init metronic core components
    Layout.init(); // init current layout
    Demo.init(); // init demo features
    TableManaged.init();
    load_tinymce();
    
	<?php
	/*
    $('#add_faq').click(function(){
		var faq = $('#template_faq').children().clone(true,true);
		$('#content_faq').append(faq);
		load_tinymce();
    });
    
    $('.delete_faq').click(function(){
		$(this).parent().parent().parent().remove();
    });
    
    $('.change_file').click(function(){
		openKCFinderFiles(this);
    });
    
    $('.change_cover').click(function(){
		openKCFinderImages(this);
	
    });

    $('.delete_photo').click(function(){
		$(this).parent().parent().parent().remove();
	// 	alert('uy');
    });
    $('#change_cover_multi').click(function(){
		openKCFinderImagesMulti(this);
    });
    */
	?>
	
	$(document).on('click','#add_faq',function(){
		var faq = $('#template_faq').children().clone(true,true);
		$('#content_faq').append(faq);
		load_tinymce();
    });
    
    $(document).on('click','.delete_faq',function(){
		$(this).parent().parent().parent().remove();
    });
    
    $(document).on('click','.change_file',function(){
		openKCFinderFiles(this);
    });
    
    $(document).on('click','.change_cover',function(){
		openKCFinderImages(this);
	
    });

    $(document).on('click','.delete_photo',function(){
		$(this).parent().parent().parent().remove();
	// 	alert('uy');
    });
	
    $(document).on('click','#change_cover_multi',function(){
		openKCFinderImagesMulti(this);
    });
	
    $('.datetimepicker').datetimepicker({
		format: 'YYYY-MM-DD'
    });
    
    $('.datetimepicker1').datetimepicker({
		format: 'YYYY-MM-DD HH:mm'
    });
    function openKCFinderImages(div) {
		window.KCFinder = {
			callBack: function(url) {
			window.KCFinder = null;
			URL = url;
			console.log(URL);
			$(div).attr('src',URL);
			$(div).next().val(URL);
			$(div).parent().parent().siblings('.view_info').children('a').attr('href',URL);
			}
		};
		window.open('<?=$this->config->item("plugin")?>/kcfinder/browse.php?type=images',
			'kcfinder_image', 'status=0, toolbar=0, location=0, menubar=0, ' +
			'directories=0, resizable=1, scrollbars=0, width=800, height=600'
		);
    }
    function openKCFinderFiles(div) {
// 	console.log(div);
	window.KCFinder = {
	    callBack: function(url) {
		window.KCFinder = null;
		$(div).val(url);
	    }
	};
	window.open('<?=$this->config->item("plugin")?>/kcfinder/browse.php?type=files',
	    'kcfinder_file', 'status=0, toolbar=0, location=0, menubar=0, ' +
	    'directories=0, resizable=1, scrollbars=0, width=800, height=600'
	);
    }
    function basename(str){
      var base = new String(str).substring(str.lastIndexOf('/') + 1); 
	if(base.lastIndexOf(".") != -1)       
	    base = base.substring(0, base.lastIndexOf("."));
      return base;
    }
    function openKCFinderImagesMulti(div) {
	console.log(div);
	window.KCFinder = {
	    callBackMultiple: function(url) {
		window.KCFinder = null;
// 		console.log(url);
		
// 		window.KCFinder = null;
// 		textarea.value = "";
// 		var cover = $('#template_cover').clone();
// 		console.log(url);
		for (var i = 0; i < url.length; i++){
		    $('#template_cover img').attr('src',url[i]);
		    $('#template_cover #title').val(basename(decodeURIComponent(url[i])));
		    $('#template_cover #cover').val(url[i]);
		    
		    var cover = $('#template_cover').children().clone(true,true);
		    $('#upload_gallery').append(cover);
		    
// 		    console.log(i);
		}
	    }
	};
	window.open('<?=$this->config->item("plugin")?>/kcfinder/browse.php?type=images',
	    'kcfinder_image', 'status=0, toolbar=0, location=0, menubar=0, ' +
	    'directories=0, resizable=1, scrollbars=0, width=800, height=600'
	);
    }
});
</script> 
