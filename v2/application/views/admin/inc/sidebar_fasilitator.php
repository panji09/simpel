<?php $page=$this->uri->segment(2); $page1 = $this->uri->segment(3);?>
<div class="page-sidebar-wrapper">
	<div class="page-sidebar navbar-collapse collapse">
		<ul class="page-sidebar-menu " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
			<li <?=($page=='dashboard' ? 'class="start active "' : '')?>>
				<a href="<?=site_url('fasilitator/dashboard')?>">
					<i class="icon-home"></i>
					<span class="title">Dashboard</span>
				</a>
			</li>
			<li <?=($page=='management_training' ? 'class="open active "' : '')?>>
				<a href="javascript:;">
					<i class="icon-graduation"></i>
					<span class="title">Pelatihan</span>
					<span class="arrow "></span>
				</a>
				<ul class="sub-menu">

					<?php
					
					$lms_api_url = $this->config->item('lms_api_url');
					$input = array();
					$response = curl_get($lms_api_url."/training", $input);
					if($response && $response['header_info']['http_code'] == 200){
					    $query_elearning = json_decode($response['body'],true);
						
					}					
					if(isset($query_elearning)){
						$count_select_instructor_elearning = count($query_elearning);
					}else{
						$count_select_instructor_elearning = 0;
					}
					
					$count_select_instructor_unfinished = 0;
					$query = $this->training_db->get_all(
					array(
						'draft' => 0, 
						'verified_submission_training' => 1,
						'facilitator_user_id' => $this->connect_auth->get_me()['user_id'],
						'select_instructor_finished' => 0
						)
					);

					if($query){
						$count_select_instructor_unfinished = count($query);
					}
					?>
					<li <?=($page1=='training' ? 'class="active "' : '')?>>
						<a href="<?=site_url('fasilitator/management_training/training')?>">List Pelatihan <?=($count_select_instructor_unfinished ? '<span class="badge">'.$count_select_instructor_unfinished.'</span>' : '')?></a>
					</li>

					<li <?=($page1=='elearning' ? 'class="active "' : '')?>>
						<a href="<?=site_url('fasilitator/management_training/elearning')?>">List eLearning <?=($count_select_instructor_elearning ? '<span class="badge">'.$count_select_instructor_elearning.'</span>' : '')?></a>
					</li>
					
				</ul>
			</li>
			


		</ul>
		<!-- END SIDEBAR MENU -->
	</div>
</div>