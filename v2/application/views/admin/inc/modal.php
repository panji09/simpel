<?php
	$page = $this->uri->segment(1);
	$page1 = $this->uri->segment(2);
?>
<div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title">Konfirmasi</h4>
			</div>
			<div class="modal-body">
				Yakin akan dihapus?
			</div>
			<div class="modal-footer">
				<button type="button" class="btn blue"  onclick="location.href='#'" id='modal-delete-ya'>Ya</button>
				<button type="button" class="btn default" data-dismiss="modal">Tidak</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>

<div class="modal fade" id="published" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title">Status</h4>
			</div>
			<div class="modal-body">
				Ubah status berita
			</div>
			<div class="modal-footer">
				<button type="button" class="btn blue"  onclick="location.href='#'" id='modal-published'>Terbitkan</button>

				<button type="button" class="btn red" onclick="location.href='#'" id='modal-unpublished'>Tidak</button>

				<button type="button" class="btn default" data-dismiss="modal">Tutup</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>

<div class="modal fade" id="status" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title">Status</h4>
			</div>
			<div class="modal-body">
				Ubah status User
			</div>
			<div class="modal-footer">
				<button type="button" class="btn blue"  onclick="location.href='#'" id='modal-active'>Aktif</button>

				<button type="button" class="btn red" onclick="location.href='#'" id='modal-nonactive'>Non-Aktif</button>

				<button type="button" class="btn default" data-dismiss="modal">Tutup</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>

<?php
/*
<div class="modal fade" id="review_institution" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-user_id='1234'>
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<form method='post' action='#' class="form-horizontal">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
					<h4 class="modal-title">Review Lembaga Pelatihan</h4>
				</div>
				<div class="modal-body">
					<div>

						<!-- Nav tabs -->
						<ul class="nav nav-tabs" role="tablist" id='review_institution_tab'>
							<li role="presentation" class="active"><a href="#review_tab_0" role="tab" data-param='user_profile' data-toggle="tab">Profil Pengguna</a></li>
							<li role="presentation"><a href="#review_tab_1" role="tab" data-param='institution_office' data-toggle="tab">Data Lembaga</a></li>
							<li role="presentation"><a href="#review_tab_2" role="tab" data-param='institution_responsible_person' data-toggle="tab">Penanggung Jawab(Kepala LPP)</a></li>
							<li role="presentation"><a href="#review_tab_3" role="tab" data-param='institution_upload_data' data-toggle="tab">Upload data pelengkap</a></li>

							<?php if($this->connect_auth->get_me()['entity'] == 'admin'):?>
								<li role="presentation"><a href="#review_tab_4" role="tab" data-param='review_note' data-toggle="tab" class='tab_clarification'>Pesan Tunda</a></li>
								<li role="presentation"><a href="#review_tab_5" role="tab" data-param='institution_approved' data-toggle="tab" class='tab_approved'>Disetujui</a></li>
							<?php endif;?>
						</ul>

						<!-- Tab panes -->
						<div class="tab-content">
							<div role="tabpanel" class="tab-pane active" id="review_tab_0">
								<div class="spinner-loader">
									Loading..!
								</div>
							</div>
							<div role="tabpanel" class="tab-pane" id="review_tab_1">
								<div class="spinner-loader">
									Loading..!
								</div>
							</div>
							<div role="tabpanel" class="tab-pane" id="review_tab_2">
								<div class="spinner-loader">
									Loading..!
								</div>
							</div>
							<div role="tabpanel" class="tab-pane" id="review_tab_3">
								<div class="spinner-loader">
									Loading..!
								</div>
							</div>

							<?php if($this->connect_auth->get_me()['entity'] == 'admin'):?>
								<div role="tabpanel" class="tab-pane" id="review_tab_4">
									<div class="spinner-loader">
										Loading..!
									</div>
								</div>
								<div role="tabpanel" class="tab-pane" id="review_tab_5">
									<div class="spinner-loader">
										Loading..!
									</div>
								</div>
							<?php endif;?>
						</div>

					</div>
				</div>
				<div class="modal-footer">
					<?php if($this->connect_auth->get_me()['entity'] == 'admin'):?>

						<button type="submit" id='approved_institution_submit' class="btn blue" value='Disetujui'>Disetujui</button>
						<button type="submit" id='review_institution_submit' class="btn yellow" value='Tunda'>Tunda</button>
						<button type="button" class="btn red" onclick="location.href='#'" id='modal-reject'>Tolak</button>

					<?php endif;?>
					<button type="button" class="btn default" data-dismiss="modal">Tutup</button>
				</div>
			</form>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
*/
?>

<!--review institution revisi-->
<div class="modal fade" id="review_institution" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-user_id='1234'>
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<form method='post' action='#' class="form-horizontal">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
					
				</div>
				<div class="modal-body slim">
				    <!-- modal-body-->
				    
				    
				<div class="portlet light">
				    <div class="portlet-title tabbable-custom ">
					<div class="caption caption-md">
					    <i class="icon-globe theme-font-color hide"></i>
					    <span class="caption-subject theme-font-color bold uppercase">Review Lembaga Pelatihan</span>
					</div>
					<ul class="nav nav-tabs" id='review_institution_action_tab'>
					    <li class="active">
						<a href="#tab_lpp_review" data-toggle="tab" data-param='' >Review </a>
					    </li>
					    
					    <?php if($this->connect_auth->get_me()['entity'] == 'admin'):?>
					    <li>
						<a href="#tab_approved" data-toggle="tab" data-param='institution_approved' >Disetujui </a>
					    </li>
					    <li>
						<a href="#tab_clarification" data-toggle="tab" data-param='review_note'>Klarifikasi </a>
					    </li>
					    <li>
						<a href="#tab_rejected" data-toggle="tab" data-param='review_rejected'>Tolak </a>
					    </li>
					    <?php endif;?>
					</ul>
				    </div>
				    
				    <div class="portlet-body">
					<div class="tab-content">
					    <div class="tab-pane active" id="tab_lpp_review">
						<!-- overview-->
						<div class="portlet light">
							
							<div class="portlet-body">
								<!--review-->
								
								<!-- Nav tabs -->
						<ul class="nav nav-tabs" role="tablist" id='review_institution_tab'>
							<li role="presentation" class="active"><a href="#review_tab_0" role="tab" data-param='user_profile' data-toggle="tab">Profil Pengguna</a></li>
							<li role="presentation"><a href="#review_tab_1" role="tab" data-param='institution_office' data-toggle="tab">Data Lembaga</a></li>
							<li role="presentation"><a href="#review_tab_2" role="tab" data-param='institution_responsible_person' data-toggle="tab">Penanggung Jawab(Kepala LPP)</a></li>
							<li role="presentation"><a href="#review_tab_3" role="tab" data-param='institution_upload_data' data-toggle="tab">Upload data pelengkap</a></li>

							
						</ul>

						<!-- Tab panes -->
						<div class="tab-content">
							<div role="tabpanel" class="tab-pane active" id="review_tab_0">
								<div class="spinner-loader">
									Loading..!
								</div>
							</div>
							<div role="tabpanel" class="tab-pane" id="review_tab_1">
								<div class="spinner-loader">
									Loading..!
								</div>
							</div>
							<div role="tabpanel" class="tab-pane" id="review_tab_2">
								<div class="spinner-loader">
									Loading..!
								</div>
							</div>
							<div role="tabpanel" class="tab-pane" id="review_tab_3">
								<div class="spinner-loader">
									Loading..!
								</div>
							</div>

							<?php if($this->connect_auth->get_me()['entity'] == 'admin'):?>
								<div role="tabpanel" class="tab-pane" id="review_tab_4">
									<div class="spinner-loader">
										Loading..!
									</div>
								</div>
								<div role="tabpanel" class="tab-pane" id="review_tab_5">
									<div class="spinner-loader">
										Loading..!
									</div>
								</div>
							<?php endif;?>
						</div>
								
								<!--review-->
							</div>
						</div>
						<!-- ./overview-->
				
				
					    </div>
					    
					    <div class="tab-pane" id="tab_approved">
						
					    </div>
					    
					    <div class="tab-pane" id="tab_clarification">
						
					    </div>
					    
					    <div class="tab-pane" id="tab_rejected">
						
					    </div>
					</div>  
				    </div>
				    
				</div>
				
				
				    
				    <!-- ./modal-body-->
				</div>
				<div class="modal-footer">
					<?php if($this->connect_auth->get_me()['entity'] == 'admin'):?>

						<button type="submit" id='approved_institution_submit' class="btn blue" name='submit' value='approved'>Disetujui</button>
						<button type="submit" id='review_institution_submit' class="btn yellow" name='submit' value='clarification'>Klarifikasi</button>
						<button type="submit" class="btn red" id='reject_institution_submit' name='submit' value='rejected' >Tolak</button>

					<?php endif;?>
					<button type="button" class="btn default" data-dismiss="modal">Tutup</button>
				</div>
			</form>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<!--./review institution revisi-->

<?php if(in_array($page1, array('management_training', 'approval')) ):?>
	<div class="modal fade" id="review_training_submission" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-training_id='1234'>
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<form method='post' action='#' class="form-horizontal">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
						<h4 class="modal-title">Review Pengajuan Pelatihan</h4>
					</div>
					<div class="modal-body">
						<div>

							<!-- Nav tabs -->
							<ul class="nav nav-tabs" role="tablist" id='review_training_submission_tab'>
								<li role="presentation" class="active"><a href="#training_submission_progress" data-load_page='training_submission_progress' aria-controls="home" role="tab" data-toggle="tab">Progress</a></li>

								<li role="presentation"><a href="#training_submission_data" data-load_page='training_submission_data' aria-controls="profile" role="tab" data-toggle="tab">Data Pengajuan Pelatihan</a></li>

								<li role="presentation"><a href="#training_submission_location" data-load_page='training_submission_location' aria-controls="profile" role="tab" data-toggle="tab">Data Lokasi Pelatihan</a></li>

								<li role="presentation"><a href="#training_submission_participant" data-load_page='training_submission_participant' aria-controls="profile" role="tab" data-toggle="tab">Data Peserta</a></li>

								<li role="presentation"><a href="#training_submission_cp" data-load_page='training_submission_cp' aria-controls="profile" role="tab" data-toggle="tab">Data Panitia</a></li>

								<?php if($this->connect_auth->get_me()['entity']=='admin'):?>
									<li role="presentation"><a href="#training_submission_approved" role="tab" data-load_page='training_submission_approved' data-toggle="tab">Disetujui</a></li>
									
									<li role="presentation"><a href="#training_submission_review_note" role="tab" data-load_page='training_submission_review_note' data-toggle="tab"> Klarifikasi</a></li>

									<li role="presentation"><a href="#training_submission_review_reject" role="tab" data-load_page='training_submission_review_reject' data-toggle="tab"> Tolak</a></li>

								<?php endif;?>
							</ul>

							<!-- Tab panes -->
							<div class="tab-content">
								<div role="tabpanel" class="tab-pane active" id="training_submission_progress">
									<div class="spinner-loader">
										Loading..!
									</div>
								</div>
								<div role="tabpanel" class="tab-pane" id="training_submission_data">
									<div class="spinner-loader">
										Loading..!
									</div>
								</div>
								<div role="tabpanel" class="tab-pane" id="training_submission_location">
									<div class="spinner-loader">
										Loading..!
									</div>
								</div>
								<div role="tabpanel" class="tab-pane" id="training_submission_participant">
									<div class="spinner-loader">
										Loading..!
									</div>
								</div>

								<div role="tabpanel" class="tab-pane" id="training_submission_cp">
									<div class="spinner-loader">
										Loading..!
									</div>
								</div>
								<div role="tabpanel" class="tab-pane" id="training_submission_review_note">
									<div class="spinner-loader">
										Loading..!
									</div>
								</div>
								<div role="tabpanel" class="tab-pane" id="training_submission_review_reject">
									<div class="spinner-loader">
										Loading..!
									</div>
								</div>
								<div role="tabpanel" class="tab-pane" id="training_submission_approved">
									<div class="spinner-loader">
										Loading..!
									</div>
								</div>
							</div>

						</div>
					</div>
					<div class="modal-footer">
						<?php if($this->connect_auth->get_me()['entity'] == 'admin'):?>
							<button type="submit" id='approved_training_submission_submit' class="btn blue" name='submit' value='approved'>Disetujui</button>
							<button type="submit" id='review_training_submission_submit' class="btn yellow" name='submit' value='clarification'>Klarifikasi</button>
							<button type="submit" class="btn red" name='submit' value='reject' id='modal-reject'>Tolak</button>

						<?php endif;?>
						<button type="button" class="btn default" data-dismiss="modal">Tutup</button>
					</div>
				</form>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	
	<div class="modal fade" id="review_instructor" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-training_id='1234'>
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<form method='post' action='#' class="form-horizontal">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
						<h4 class="modal-title">Review Pengajuan Pengajar</h4>
					</div>
					<div class="modal-body">
						<div>

							<!-- Nav tabs -->
							<ul class="nav nav-tabs" role="tablist" id='review_instructor_tab'>
								<li role="presentation" class="active">
								    <a href="#tab_instructor_0" data-load_page='user_profile' role="tab" data-toggle="tab">Data User</a>
								</li>

								<li role="presentation">
								    <a href="#tab_instructor_1" data-load_page='instructor_profile_detail'  role="tab" data-toggle="tab">Data User Detail</a>
								</li>

								
							</ul>

							<!-- Tab panes -->
							<div class="tab-content">
								<div role="tabpanel" class="tab-pane active" id="tab_instructor_0">
									<div class="spinner-loader">
										Loading..!
									</div>
								</div>
								<div role="tabpanel" class="tab-pane" id="tab_instructor_1">
									<div class="spinner-loader">
										Loading..!
									</div>
								</div>
								
							</div>

						</div>
					</div>
					<div class="modal-footer">
						<?php if($this->connect_auth->get_me()['entity'] == 'admin'):?>
							<input type='hidden' name='instructor_id' value=''>
							<button type="submit" id='' class="btn blue" name='submit' value='approved'>Disetujui</button>
							
							<button type="submit" class="btn red" name='submit' value='reject' id='modal-reject'>Tolak</button>

						<?php endif;?>
						<button type="button" class="btn default" data-dismiss="modal">Tutup</button>
					</div>
				</form>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
<?php endif;?>


			

<div class="modal fade" id="choose_instructor_modal_set_default" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-user_id='1234'>
	<div class="modal-dialog">
		<div class="modal-content">
			<form method='post' action='#' class="form-horizontal">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
					<h4 class="modal-title">Set Jumlah JP</h4>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label for="inputEmail3" class="col-sm-4 control-label">Jumlah Jam Pelajaran</label>
						<div class="col-sm-8">
							<input type='text' class="form-control" name='hour_leasson' id='hour_leasson_modal'>
							<input type='hidden' class="form-control" name='instructor_id' value=''>
							<input type='hidden' class="form-control" name='training_id' value='default_jp'>

						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn blue"  id='submit_set_instructor_default'>Set</button>
					<button type="button" class="btn default" data-dismiss="modal">Tutup</button>
				</div>
			</form>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>

<div class="modal fade" id="choose_instructor_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-user_id='1234'>
	<div class="modal-dialog">
		<div class="modal-content">
			<form method='post' action='#' class="form-horizontal">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
					<h4 class="modal-title">Set Waktu Pengajar</h4>
				</div>
				<div class="modal-body"></div>
				<div class="modal-footer">
					<button type="button" class="btn blue" data-dismiss="modal" id='submit_set_instructor'>Set</button>
					<button type="button" class="btn default" data-dismiss="modal">Tutup</button>
				</div>
			</form>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>

<div class="modal fade" id="set_instructor_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-user_id='1234'>
	<div class="modal-dialog">
		<div class="modal-content">
			<form method='post' action='#' class="form-horizontal">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
					<h4 class="modal-title">Set Waktu Pengajar</h4>
				</div>
				<div class="modal-body"></div>
				<div class="modal-footer">
					<button type="button" class="btn blue" data-dismiss="modal" id='submit_set_instructor'>Set</button>
					<button type="button" class="btn default" data-dismiss="modal">Tutup</button>
				</div>
			</form>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>

<div class="modal fade" id="select_instructor_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-user_id='1234'>
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<form method='post' action='#' class="form-horizontal">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
					<h4 class="modal-title">Pilih Pengajar</h4>
				</div>
				<div class="modal-body"></div>
				<div class="modal-footer">
					<button type="button" class="btn blue" data-dismiss="modal" id='submit_select_instructor'>Set</button>
					<button type="button" class="btn default" data-dismiss="modal">Tutup</button>
				</div>
			</form>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>

<div class="modal fade" id="invite_user_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-user_id='1234'>
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<form method='post' action='#' class="form-horizontal">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
					<h4 class="modal-title">Invite User</h4>
					
				</div>
				<div class="modal-body">
				    <div class="form-group">
					    <label for="inputEmail3" class="col-sm-2 control-label">Email</label>
					    <div class="col-sm-10">
						    <input type='email' class="form-control" name='email' id='invite_email'>
					    </div>
				    </div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn blue"  >Kirim</button>
					<button type="button" class="btn default" data-dismiss="modal">Tutup</button>
				</div>
			</form>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>