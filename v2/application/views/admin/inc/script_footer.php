<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-68709704-1', 'auto');
  ga('send', 'pageview');

</script>

<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="<?=$this->config->item('plugin')?>/respond.min.js"></script>
<script src="<?=$this->config->item('plugin')?>/excanvas.min.js"></script> 
<![endif]-->
<script src="<?=$this->config->item('plugin')?>/jquery.min.js" type="text/javascript"></script>
<script src="<?=$this->config->item('plugin')?>/jquery-migrate.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="<?=$this->config->item('plugin')?>/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
<script src="<?=$this->config->item('plugin')?>/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?=$this->config->item('plugin')?>/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="<?=$this->config->item('plugin')?>/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="<?=$this->config->item('plugin')?>/jquery.blockui.min.js" type="text/javascript"></script>
<script src="<?=$this->config->item('plugin')?>/jquery.cokie.min.js" type="text/javascript"></script>
<script src="<?=$this->config->item('plugin')?>/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<script src="<?=$this->config->item('plugin')?>/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<script type="text/javascript" src="http://timeago.yarp.com/jquery.timeago.js"></script>
<!-- END CORE PLUGINS -->


<script>
$(document).ready(function() {
    $("time.timeago").timeago();
    
	<?php
	/*
    $(".delete-user").click(function(){
	  var url_delete = $(this).data('url-delete');
	  $("#modal-delete-ya").attr('onclick', 'location.href="'+url_delete+'"' );
    });
    
    $(".status-user").click(function(){
	  var url_active = $(this).data('url-active');
	  var url_nonactive = $(this).data('url-nonactive');
	
	  $("#modal-active").attr('onclick', 'location.href="'+url_active+'"' );
	  $("#modal-nonactive").attr('onclick', 'location.href="'+url_nonactive+'"' );
	
    });
    
    $(".published-user").click(function(){
	  var url_published = $(this).data('url-published');
	  var url_unpublished = $(this).data('url-unpublished');
	
	  $("#modal-published").attr('onclick', 'location.href="'+url_published+'"' );
	  $("#modal-unpublished").attr('onclick', 'location.href="'+url_unpublished+'"' );
	
    });
    */
	?>
	
	$(document).on('click',".delete-user",function(){
	  var url_delete = $(this).data('url-delete');
	  $("#modal-delete-ya").attr('onclick', 'location.href="'+url_delete+'"' );
    });
    
    $(document).on('click',".status-user",function(){
	  var url_active = $(this).data('url-active');
	  var url_nonactive = $(this).data('url-nonactive');
	
	  $("#modal-active").attr('onclick', 'location.href="'+url_active+'"' );
	  $("#modal-nonactive").attr('onclick', 'location.href="'+url_nonactive+'"' );
	
    });
    
    $(document).on('click',".published-user",function(){
	  var url_published = $(this).data('url-published');
	  var url_unpublished = $(this).data('url-unpublished');
	
	  $("#modal-published").attr('onclick', 'location.href="'+url_published+'"' );
	  $("#modal-unpublished").attr('onclick', 'location.href="'+url_unpublished+'"' );
	
    });
});

</script>
<?php $page = $this->uri->segment(2);?>

<?php if($page == 'dashboard'):?>
<?php $this->load->view('admin/inc/script_footer_dashboard');?>
<?php endif;?>

<?php if($page == 'pages'):?>
<?php $this->load->view('admin/inc/script_footer_pages');?>
<?php endif;?>

<?php if($page == 'setting'):?>
<?php $this->load->view('admin/inc/script_footer_setting');?>
<?php endif;?>

<?php if($page == 'management_user'):?>
<?php $this->load->view('admin/inc/script_footer_management_user');?>
<?php endif;?>

<?php if($page == 'approval'):?>
<?php $this->load->view('admin/inc/script_footer_approval');?>
<?php endif;?>

<?php if($page == 'management_training'):?>
<?php $this->load->view('admin/inc/script_footer_management_training');?>
<?php endif;?>

<?php if($page == 'evaluation'):?>
<?php $this->load->view('admin/inc/script_footer_evaluation');?>
<?php endif;?>

<?php if($page == 'complaint_handling'):?>
<?php $this->load->view('admin/inc/script_footer_complaint_handling');?>
<?php endif;?>
