<table class="table table-striped table-bordered table-hover table_approval_institution" id="">
							<thead>
								<tr>
									<th>
										 Lembaga Pelatihan/LPP
									</th>
									<th>
										 Email
									</th>
									
									<th>
										 Join
									</th>
									<th>
										 Last Update
									</th>
									<th>
										 Verifikasi
									</th>
									<th>
										 Status
									</th>
									<th>
										 Action
									</th>
								</tr>
							</thead>
						<tbody>
						<?php
						    $entity = 'lpp';
						    $response = call_api_get($url);
						    $response_body = json_decode($response['body'], true);
						    
						    if($response['header_info']['http_code'] == 200):
						    foreach($response_body['data'] as $row):
							if($row['verified_email']):
						?>
						<tr class="odd gradeX">
						    <td><?=$row['training_institute']?></td>
						    <td>
							<a href="mailto:<?=$row['email']?>"><?=$row['email']?> </a>
						    </td>
						    
						    <td><time class="" datetime="<?=date('c',$row['created'])?>"><?=date("d/m/Y H:i:s",$row['created']) ?></time></td>
						    <td><time class="timeago" datetime="<?php echo date('c',$row['last_update']['time'])?>"><?php  echo date("d/m/Y H:i:s",$row['last_update']['time']) ?> <?php// hitung_mundur($row['last_update']['time']);?></time></td>
						    <td>
							<?php
							    
							    if(isset($row['verified_institute']))
							    switch($row['verified_institute']){
								case 0 : $label = 'label-warning'; $msg = 'Menunggu Verifikasi'; break;
								case 1 : $label = 'label-success'; $msg = 'Disetujui'; break;
								case -1 : $label = 'label-warning'; $msg = 'Tunda'; break;
								case -2 : $label = 'label-danger'; $msg = 'Tolak'; break;
								default : $label = 'label-warning'; $msg = 'Menunggu Verifikasi'; break;
							    }
							?>
							<span class="label label-sm <?=(isset($row['verified_institute'])  ? $label : 'label-warning')?>"> <?=(isset($row['verified_institute'])  ? $msg : 'Menunggu Verifikasi')?> </span>
						    </td>
						    
						    <td>
							<span class="label label-sm <?=($row['activated'] ? 'label-success' : ($row['verified_email'] ? 'label-danger' : 'label-warning'))?>"> <?=($row['activated'] ? 'Aktif' : ($row['verified_email'] ? 'Non-Aktif' : 'Menunggu verifikasi email'))?> </span>
						    </td>
						    <td>
							<div class="btn-group">
							    <?php
							    /*
							    <button type="button" class="btn btn-default" onclick="location.href='<?=site_url('superadmin/management_user/'.$page.'/edit/'.$row['user_id'])?>'">Edit</button>
							    */
							    ?>
							    <button type="button" class="btn default btn-xs purple review-institution" data-url-accept="<?=site_url('admin/approval/'.$page.'/status/'.$row['user_id'].'/?verified_institute=1')?>" data-url-reject="<?=site_url('admin/approval/'.$page.'/status/'.$row['user_id'].'/?verified_institute=-2')?>" data-url-submit="<?=site_url('admin/approval/'.$page.'_post/'.$row['user_id'])?>" data-toggle="modal" data-user_id="<?=$row['user_id']?>" data-target="#review_institution"><i class="fa fa-eye"></i> Review</button>

							    <button type="button" class="btn default btn-xs yellow status-user" data-url-active="<?=site_url('superadmin/management_user/'.$page.'/status/'.$row['user_id'].'/?activated=1')?>" data-url-nonactive="<?=site_url('superadmin/management_user/'.$page.'/status/'.$row['user_id'].'/?activated=0')?>" data-toggle="modal" data-target="#status">
							    <i class="fa fa-minus-circle"></i> Status</button>
							    
							    <button type="button" class="btn default btn-xs red delete-user" data-url-delete="<?=site_url('superadmin/management_user/'.$page.'/delete/'.$row['user_id'])?>" data-toggle="modal" data-target="#delete"><i class="fa fa-trash-o"></i> Delete</button>
							</div>
						    </td>
						</tr>
						<?php endif; endforeach; endif;?>
						
						</tbody>
						</table> 
