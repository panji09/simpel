<table class="table table-striped table-bordered table-hover standard-font table_approval_submission_training" id="">
									<thead>
										<tr>
											<th>
												Tanggal
											</th>
											<th>
												Lembaga Pelatihan/LPP
											</th>
											<th>
												Akreditasi
											</th>
											
											<th>
												Nama Pelatihan
											</th>

											<th>
												Tanggal Pelatihan
											</th>
											
											<th>
												Fasilitator
											</th>
											
											<th>
												Verifikasi
											</th>
											<th>
												Status
											</th>
											<th>
												Action
											</th>
										</tr>
									</thead>
									<tbody>
										<?php

										
										if($query)
										//var_dump($query);
										foreach($query as $row):

											?>
											<tr class="odd gradeX">
												<td><time class="" datetime="<?=date('c',$row['created'])?>"><?=date("d/m/Y H:i:s",$row['created']) ?></time></td>
												<td><?=$row['user_submission']['training_institute']?></td>
												<!-- Akreditasi -->
												<?php if($row['user_submission']['verified_institute']==1):?>
							      
												<?php if($row['user_submission']['accreditation_level'] == 'terdaftar'):?>
												<td><span class="label label-info"> <?=strtoupper($row['user_submission']['accreditation_level'])?></span></td>
												<?php else:?>
												<td><span class="label label-success">Akreditasi <?=strtoupper($row['user_submission']['accreditation_level'])?></span></td>
												<?php endif;?>
												
											  <?php elseif($row['user_submission']['verified_institute']==0):?>
											  <td><span class="label label-default">Verifikasi</span></td>
											  <?php endif;?>
												<!-- end akreditasi-->
												<td>
													<?=$row['type_training']['name'].' / '.$row['training_name']['name']?>
												</td>
												<?php
												    $date_training = explode(' - ', $row['training_date']);
												?>
												<td><?=tgl_indo($date_training[0]).' - '.tgl_indo($date_training[1])?></td>
												
												<td>
									                <?php
													
													if (isset( $row['facilitator_user_id']) && $row['facilitator_user_id'] !='')
													{
													$fasilitator_id = $row['facilitator_user_id'];
													$token = $this->connect_auth->get_access_token();
									                $response = call_api_get($this->config->item('connect_api_url').'/user/index/'.$token.'/'.$fasilitator_id);
									                $response_body = json_decode($response['body'], true);
									//	var_dump($response_body);
								   
									                     echo $response_body['fullname'].'/'.$response_body['username'];
													
													}
													else
													{
									                     echo '-'; 
																		
													}
													
													
													?>
												</td>
												
												<td>
													<?php
													$status = array();


													if(isset($row['verified_submission_training']))
													switch($row['verified_submission_training']){
														case 0 : $status = array('name' => 'Menunggu Verifikasi', 'label' => 'label-warning'); break;
														case 1 : $status = array('name' => 'Disetujui', 'label' => 'label-success'); break;
														case -1 : $status = array('name' => 'Klarifikasi', 'label' => 'label-warning'); break;
														case -2 : $status = array('name' => 'Tolak', 'label' => 'label-danger'); break;
														default : $status = array('name' => 'Menunggu Verifikasi', 'label' => 'label-warning'); break;
													}
													?>
													<span class="label label-sm <?=$status['label']?>"> <?=$status['name']?> </span>
												</td>
												
												
												

												<td>
													<?php
													$status = array();

													if(isset($row['draft'])){
														$status = array('name' => 'draft', 
														'label' => 'label-warning');
													}

													if(isset($row['verified_submission_training']))
													switch($row['verified_submission_training']){
														case 0 : 
														case -1 : 
														case -2 : $status = array('name' => 'Tahap Pengajuan',
														'label' => 'label-warning'); break;
														case 1 : $status = array('name' => 'Tahap Persiapan',
														'label' => 'label-warning'); break;
													}
													?>
													
													<div class="margin-bottom-10">
														<span class="label label-sm
														<?=$status['label']?>"><?=$status['name']?></span>
													</div>
													<?php 
													if(isset($row['rescheduled']) && $row['rescheduled']):
														$status = array('name' => 'Reschedule', 
														'label' => 'label-default');
													?>

													<div class="margin-bottom-10">
														<span class="label label-sm 
														<?=$status['label']?>"><?=$status['name']?></span>
													</div>
												<?php endif;?>

													<?php 
													if(isset($row['facilitator_user_id'])):
														$status = array('name' => 'Pemilihan Pengajar', 
														'label' => 'label-default');
													?>
													<div class="margin-bottom-10">
														<span class="label label-sm 
														<?=$status['label']?>"><?=$status['name']?></span>
													</div>
													<?php endif;?>

												</td>
												<td>
													<div class="margin-bottom-5">
														<button 
														type="button" 
														class="btn default btn-xs purple review_training_submission"
														data-url-submit="<?=site_url('lpp/management_training/training_post/'.$row['_id']->{'$id'})?>" 
														data-toggle="modal" 
														data-training_id="<?=$row['_id']->{'$id'}?>" 
														data-target="#review_training_submission">
															<i class="fa fa-eye"></i> Review 
														</button>
													</div>
													
													<div class="margin-bottom-5">
														<a href='<?=site_url('admin/management_training/training/detail/'.$row['_id']->{'$id'})?>' class='btn default btn-xs'><i class="fa fa-graduation-cap"></i> Detail</a>
													</div>
													
													
												</td>
											</tr>
										<?php endforeach; ?>
									</tbody>
								</table> 
