
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="<?=$this->config->item('plugin')?>/select2/select2.min.js"></script>
<script type="text/javascript" src="<?=$this->config->item('plugin')?>/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?=$this->config->item('plugin')?>/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.8.4/moment.min.js"></script>
<script type="text/javascript" src="//cdn.datatables.net/plug-ins/1.10.10/sorting/datetime-moment.js"></script>

<script type="text/javascript" src="<?=$this->config->item('plugin')?>/jquery-validation/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?=$this->config->item('plugin')?>/jquery-validation/js/additional-methods.min.js"></script>
<script type="text/javascript" src="<?=$this->config->item('plugin')?>/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
<script type="text/javascript" src="<?=$this->config->item('plugin')?>/moment.min.js"></script>
<script type="text/javascript" src="<?=$this->config->item('plugin')?>/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>

<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?=$this->config->item('admin_js')?>/metronic.js" type="text/javascript"></script>
<script src="<?=$this->config->item('admin_js')?>/layout.js" type="text/javascript"></script>
<script src="<?=$this->config->item('admin_js')?>/demo.js" type="text/javascript"></script>
<script src="<?=$this->config->item('admin_js')?>/table-managed.js"></script>
<script src="<?=$this->config->item('admin_js')?>/form-wizard.js"></script>

<!-- Tinymce WYSIWYG Editors -->
<script type="text/javascript" src="<?=$this->config->item('plugin')?>/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
    
    function load_tinymce(){
	tinyMCE.editors = [];
	tinymce.init({
	    selector: "#wysiwyg-content-full",
		    theme: "modern",
		    skin: "light",
		    plugins:[
			    "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
			    "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
			    "save table contextmenu directionality emoticons template paste textcolor importcss"
		    ],
		    toolbar:[
			    "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media fullpage | forecolor backcolor emoticons table"
			    ],
		    file_browser_callback: function(field, url, type, win) {
		    tinyMCE.activeEditor.windowManager.open({
			file: '<?=$this->config->item("plugin")?>/kcfinder/browse.php?opener=tinymce4&field=' + field + '&type=' + type+'s',					
			title: 'KCFinder',
			width: 700,
			height: 500,
			inline: true,
			close_previous: false
		    }, {
			window: win,
			input: field
		    });
		    return false;
		    }
	});
	
	tinymce.init({
	    selector: "#wysiwyg-content-full_1",
		    theme: "modern",
		    skin: "light",
		    plugins:[
			    "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
			    "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
			    "save table contextmenu directionality emoticons template paste textcolor importcss"
		    ],
		    toolbar:[
			    "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media fullpage | forecolor backcolor emoticons table"
			    ],
		    file_browser_callback: function(field, url, type, win) {
		    tinyMCE.activeEditor.windowManager.open({
			file: '<?=$this->config->item("plugin")?>/kcfinder/browse.php?opener=tinymce4&field=' + field + '&type=' + type+'s',					
			title: 'KCFinder',
			width: 700,
			height: 500,
			inline: true,
			close_previous: false
		    }, {
			window: win,
			input: field
		    });
		    return false;
		    }
	});
	
	tinymce.init({
	    selector: "#wysiwyg-content-full_2",
		    theme: "modern",
		    skin: "light",
		    plugins:[
			    "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
			    "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
			    "save table contextmenu directionality emoticons template paste textcolor importcss"
		    ],
		    toolbar:[
			    "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media fullpage | forecolor backcolor emoticons table"
			    ],
		    file_browser_callback: function(field, url, type, win) {
		    tinyMCE.activeEditor.windowManager.open({
			file: '<?=$this->config->item("plugin")?>/kcfinder/browse.php?opener=tinymce4&field=' + field + '&type=' + type+'s',					
			title: 'KCFinder',
			width: 700,
			height: 500,
			inline: true,
			close_previous: false
		    }, {
			window: win,
			input: field
		    });
		    return false;
		    }
	});
	
	tinymce.init({
	selector: ".wysiwyg-content-full",
		    theme: "modern",
		    skin: "light",
		    plugins:[
			    "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
			    "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
			    "save table contextmenu directionality emoticons template paste textcolor importcss"
		    ],
		    toolbar:[
			    "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media fullpage | forecolor backcolor emoticons table"
			    ],
		    file_browser_callback: function(field, url, type, win) {
		    tinyMCE.activeEditor.windowManager.open({
			file: '<?=$this->config->item("plugin")?>/kcfinder/browse.php?opener=tinymce4&field=' + field + '&type=' + type+'s',					
			title: 'KCFinder',
			width: 700,
			height: 500,
			inline: true,
			close_previous: false
		    }, {
			window: win,
			input: field
		    });
		    return false;
		    }
	});
	
	tinymce.init({
	    selector: "#wysiwyg-short",
		    theme: "modern",
		    skin: "light",
		    menubar:false,
		    statusbar: false,
		    toolbar:["bold italic underline"],
		    paste_data_images: true
	});
	
	tinymce.init({
	    selector: ".wysiwyg-short1",
		    theme: "modern",
		    skin: "light",
		    menubar:false,
		    statusbar: false,
		    toolbar:["bold italic underline | bullist numlist | link image"],
		    paste_data_images: true
	});
    }
</script>
<!-- END Tinymce editor -->

<script>
	  $(document).ready(function() {
			$.fn.dataTable.moment( 'DD/MM/YYYY HH:mm:ss' );
			
		  Metronic.init(); // init metronic core components
		  Layout.init(); // init current layout
		  Demo.init(); // init demo features
		  TableManaged.init();
		  FormWizard.init();
		  
		  load_tinymce();
	  });
	  
	  var param = null;
	  var tab_content = null;
	  var user_id = null;
	  
	  $(document).ready(function() {
	      
	       /*
	      popup review instructor
	      */
	      var modal_id_instructor = '#review_instructor';
	      var modal_tab_id_instructor = '#review_instructor_tab';
	      var btn_class_instructor = '.review_instructor';
	      function load_ajax_instructor(modal_id_instructor, training_id, user_id){
		  
			var load_page = $(modal_id_instructor).find(modal_tab_id_instructor+' li.active a').data('load_page');
			var tab_id = $(modal_id_instructor).find(modal_tab_id_instructor+' li.active a').attr('href');
			
			$.get( "<?=site_url('admin/approval/instructor_ajax')?>/"+load_page+"/"+training_id+"/"+user_id, function( data ) {		      
				$(modal_id_instructor+' '+tab_id).html( data );
			});
				  
	      }
	      //tabnya
		  $(document).on('click',modal_tab_id_instructor+' a',function(){
			//ambil tab_id sm training_id
			training_id = $(modal_id_instructor).attr('data-training_id');
			user_id = $(modal_id_instructor).attr('data-user_id');
			
			$(this).tab('show');
			load_ajax_instructor(modal_id_instructor, training_id, user_id);
	      });
	      
	      //button kliknya
		  
	      $(document).on('click',btn_class_instructor,function(){
			user_id = $(this).data('user_id');
			console.log(user_id);
			training_id = $(this).data('training_id');
  
			//setting init
			$(modal_id_instructor+" form").attr('action',$(this).data('url-submit'));
			$(modal_id_instructor+" input[name=instructor_id]").val(user_id);
			
			
			//simpen training_id sm tab_id
			$(modal_id_instructor).attr('data-training_id', training_id);
			$(modal_id_instructor).attr('data-user_id', user_id);
			
			//load ajax
			load_ajax_instructor(modal_id_instructor, training_id, user_id);
				  
	      });
	      
	      
	      //check sbmit
	      $(modal_id_instructor+" form").submit(function (e) {
		  
// 		  e.preventDefault();
		  var btn = $(this).find("button[type=submit]:focus" );
		  var tab_id = $(modal_id_instructor).data('tab_id');
		  var training_id = $(modal_id_instructor).data('training_id');
		  var load_page = $(tab_id).find(modal_tab_id_instructor+' li.active a').data('load_page');
		  
		  
		  if(btn.attr('id') == 'approved_training_submission_submit'){
		      
		      if(load_page != 'training_submission_approved'){
			  $('#training_submission_review_note').html('');
			  $('#training_submission_approved').html('');
			  $(modal_tab_id_instructor+' a[href="#training_submission_approved"]').tab('show');
			  load_ajax_instructor(tab_id, training_id);
		      }
		      
		      
		      if($('[name=facilitator_user_id]').val() == null || $('[name=facilitator_user_id]').val() == ''){
			  alert('harus diisi!');
			  
// 			  console.log(btn.attr('id'));
			  e.preventDefault();
		      }else{
			  return;
		      }
		  }else if(btn.attr('id') == 'review_training_submission_submit'){
		      
		      if(load_page != 'training_submission_review_note'){
			  $('#training_submission_review_note').html('');
			  $('#training_submission_approved').html('');
			  $(modal_tab_id_instructor+' a[href="#training_submission_review_note"]').tab('show');
			  load_ajax_instructor(tab_id, training_id);
		      }
		      
		      
		      if($('[name=review_note]').val() == null || $('[name=review_note]').val() == ''){
			  alert('harus diisi!');
			  
// 			  console.log(btn.attr('id'));
			  e.preventDefault();
		      }else{
			  return;
		      }
		  }
		  
	      });
	      
	      /*
	      ./ popup review instructor
	      */
	  
	      /*
	      popup submission
	      */
	      var modal_id = '#review_training_submission';
	      var modal_tab_id = '#review_training_submission_tab';
	      var btn_class = '.review_training_submission';
	      function load_ajax_submission(tab_id, training_id){

		  var load_page = $(tab_id).find(modal_tab_id+' li.active a').data('load_page');
		  $(load_page).html('');
// 		  console.log(load_page);
		  $.get( "<?=site_url('admin/approval/training_submission_ajax')?>/"+load_page+"/"+training_id, function( data ) {		      
		      $('#'+load_page).html( data );
		      load_tinymce();
		  });
				  
	      }
	      //tabnya
	      <?php
		  /*
		  $(modal_tab_id+' a').click(function(e){
			//ambil tab_id sm training_id
			tab_id = $(modal_id).data('tab_id');
			training_id = $(modal_id).data('training_id');
			
			
			$(this).tab('show');
			load_ajax_submission(tab_id, training_id);
	      });
	      */
		  ?>
		  $(document).on('click',modal_tab_id+' a',function(e){
			//ambil tab_id sm training_id
			tab_id = $(modal_id).data('tab_id');
			training_id = $(modal_id).data('training_id');
			
			
			$(this).tab('show');
			load_ajax_submission(tab_id, training_id);
	      });
	      
		  //button kliknya
		  <?php
		  /*
	      $(btn_class).click(function(){
			//setting init
			$(modal_id+" form").attr('action',$(this).data('url-submit'));
			
			tab_id = $(this).data('target');
			training_id = $(this).data('training_id');
  
			//simpen training_id sm tab_id
			$(tab_id).attr('data-tab_id', tab_id);
			$(tab_id).attr('data-training_id', training_id);
			
			//load ajax
			load_ajax_submission(tab_id, training_id);
				  
	      });
	      */
		  ?>
	      
		  $(document).on('click',btn_class, function(){
			//setting init
			$(modal_id+" form").attr('action',$(this).data('url-submit'));
			
			tab_id = $(this).data('target');
			training_id = $(this).data('training_id');
  
			//simpen training_id sm tab_id
			$(tab_id).attr('data-tab_id', tab_id);
			$(tab_id).attr('data-training_id', training_id);
			
			//load ajax
			load_ajax_submission(tab_id, training_id);
				  
	      });
		  
	      //check sbmit
	      $(modal_id+" form").submit(function (e) {
		  
// 		  e.preventDefault();
		  var btn = $(this).find("button[type=submit]:focus" );
		  var tab_id = $(modal_id).data('tab_id');
		  var training_id = $(modal_id).data('training_id');
		  var load_page = $(tab_id).find(modal_tab_id+' li.active a').data('load_page');
		  
		  
		  if(btn.attr('id') == 'approved_training_submission_submit'){
		      
		      if(load_page != 'training_submission_approved'){
			  $('#training_submission_review_note').html('');
			  $('#training_submission_approved').html('');
			  $(modal_tab_id+' a[href="#training_submission_approved"]').tab('show');
			  load_ajax_submission(tab_id, training_id);
		      }
		      
		      
		      if($('[name=facilitator_user_id]').val() == null || $('[name=facilitator_user_id]').val() == ''){
			  alert('harus diisi!');
			  
// 			  console.log(btn.attr('id'));
			  e.preventDefault();
		      }else{
			  return;
		      }
		  }else if(btn.attr('id') == 'review_training_submission_submit'){
		      
		      if(load_page != 'training_submission_review_note'){
			  $('#training_submission_review_note').html('');
			  $('#training_submission_approved').html('');
			  $(modal_tab_id+' a[href="#training_submission_review_note"]').tab('show');
			  load_ajax_submission(tab_id, training_id);
		      }
		      
		      
		      if($('[name=review_note]').val() == null || $('[name=review_note]').val() == ''){
			  alert('harus diisi!');
			  
// 			  console.log(btn.attr('id'));
			  e.preventDefault();
		      }else{
			  return;
		      }
		  }
		  
	      });
	      
	      /*
	      ./ popup submission
	      */
	      
	      function load_action_ajax(){
		  
		      param = $('#review_institution_action_tab li.active a').data('param');
		      tab_content = $('#review_institution_action_tab li.active a').attr('href');
		      user_id = $('#review_institution').data('user_id');
		      
		      $('#review_institution_action_tab li.active a').tab('show');
		      if(param !=''){
			  $(tab_content).html('<div class="spinner-loader">Loading..!</div>');
			  $.get( "<?=site_url('admin/approval/institution_ajax')?>/"+user_id+"/"+param, function( data ) {		      
			      $(tab_content).html( data );
			      load_tinymce();
			  });
		      }
		      
		      
		  
		  
	      }
	      $(document).on('click','#review_institution_action_tab',function(){
		  load_action_ajax();
	      });
	      
	      
	      function load_ajax(){
		  param = $('#review_institution_tab li.active a').data('param');
		  tab_content = $('#review_institution_tab li.active a').attr('href');
		  user_id = $('#review_institution').data('user_id');
		  
		  
		  $(tab_content).html('<div class="spinner-loader">Loading..!</div>');
		  $.get( "<?=site_url('admin/approval/institution_ajax')?>/"+user_id+"/"+param, function( data ) {		      
		      $(tab_content).html( data );
		  });
		  		  
	      }
	      
	      /*
	      function load_ajax_training(){
		  param = $('#review_training_submission_tab li.active a').data('param');
		  tab_content = $('#review_training_submission_tab li.active a').attr('href');
		  training_id = $('#review_training_submission').data('training_id');
		  $(tab_content).html('');
		  $.get( "<?=site_url('admin/approval/training_submission_ajax')?>/"+training_id+"/"+param, function( data ) {		      
		      $(tab_content).html( data );
		  });
		  		  
	      }
	      */
	      $(document).on('click','.review-institution', function(){
			//set user_id
			$('#review_institution').data('user_id', $(this).data('user_id'));
			$('#review_institution textarea').val('');
			//set url-accept
			$('#review_institution #modal-accept').attr('onclick', 'location.href="'+$(this).data('url-accept')+'"');		  
			//set url-reject
			$('#review_institution #modal-reject').attr('onclick', 'location.href="'+$(this).data('url-reject')+'"');		  
			//data-url-submit
			$('#review_institution form').attr('action', $(this).data('url-submit'));
			//data-url-submit		  
			$('#review_institution_tab a[href="#review_tab_0"]').tab('show');		  
			//load ajax
			load_ajax();
			
		  });
		  
	      $(document).on('click','#review_institution_submit', function(){
			  
			$('#review_tab_5').html('');
			$('#review_institution_tab a[href="#review_tab_4"]').tab('show');
	      });

	      $("#review_institution form").submit(function (e) {
		  
// 		  e.preventDefault();
		  var btn = $(this).find("button[type=submit]:focus" );
		  if(btn.attr('id') == 'approved_institution_submit'){
		      
		      if($('[name=accreditation_level]').val() == null || $('[name=accreditation_level]').val() == ''){
			  alert('harus diisi!');
			  $('#review_institution_action_tab a[href="#tab_approved"]').tab('show');
			  load_action_ajax();
// 			  console.log(btn.attr('id'));
			  e.preventDefault();
		      }else{
			  return;
		      }
		  }else if(btn.attr('id') == 'review_institution_submit'){
		      $('#review_tab_5').html('');
		      if($('[name=review_note]').val() == null){
			  
			  $('#review_institution_tab a[href="#review_tab_4"]').tab('show');
			  load_ajax();
// 			  console.log(btn.attr('id'));
			  e.preventDefault();
		      }else{
			  return;
		      }
		  }
		  
	      });
		  
		  <?php
		  /*
	      $('#review_institution_tab a').click(function(e){
			$(this).tab('show');
			load_ajax();
	      });
		  */
		  ?>
		  $(document).on('click','#review_institution_tab a',function(e){
			$(this).tab('show');
			load_ajax();
	      });
		  //review training submission
	      /*
	      $(".review-training_submission").click(function(){
		  //set user_id
		  $('#review_training_submission').data('training_id', $(this).data('training_id'));
		  $('#review_training_submission textarea').val('');
		  //set url-accept
		  $('#review_training_submission #modal-accept').attr('onclick', 'location.href="'+$(this).data('url-accept')+'"');

		  //set url-reject
		  $('#review_training_submission #modal-reject').attr('onclick', 'location.href="'+$(this).data('url-reject')+'"');

		  //data-url-submit
		  $('#review_training_submission form').attr('action', $(this).data('url-submit'));

		  $('#review_training_submission_tab a[href="#review_training_0"]').tab('show');

		  //load ajax
		 	load_ajax_training();
	      });

	      $("#review_training_submission_submit").click(function(e){
		  $('#review_training_tab_5').html('');
		  $('#review_training_submission_tab a[href="#review_training_tab_4"]').tab('show');
	      });
	      
	      $("#review_training_submission form").submit(function (e) {		  
// 		  e.preventDefault();
		  var btn = $(this).find("button[type=submit]:focus" );
		  
		  if(btn.attr('id') == 'approved_training_submission_submit'){
// 		      alert(btn.attr('id'));

		      $('#review_training_tab_4').html('');
		      if($('[name=accreditation_level]').val() == null){
				  			  
			  $('#review_training_submission_tab a[href="#review_training_tab_5"]').tab('show');
			  	load_ajax_training();
// 			  console.log(btn.attr('id'));
			  e.preventDefault();
		      }else{
			  return;
		      }
		  }else if(btn.attr('id') == 'review_training_submission_submit'){
		      $('#review_training_tab_5').html('');
		      if($('[name=review_note]').val() == null){
			  
			  $('#review_training_submission_tab a[href="#review_training_tab_4"]').tab('show');
			  	load_ajax_training();
// 			  console.log(btn.attr('id'));
			  e.preventDefault();
		      }else{
			  return;
		      }
		  }
		  
	      });
	      $('#review_training_submission_tab a').click(function(e){
		  $(this).tab('show');
		 	load_ajax_training();
	      });
	      */
	      
	  });
	  <?php
	  /*
	  $('#clear_avatar').click(function(){
	      $('#avatar').attr('src',$('#avatar').attr('data-default'));
	      $('#avatar').next().val('');
	  });
	  
	  $('#avatar').click(function(){
	      openKCFinder(this);
	  });
	  */
	  ?>
	  
	  $(document).on('click','#clear_avatar',function(){
	      $('#avatar').attr('src',$('#avatar').attr('data-default'));
	      $('#avatar').next().val('');
	  });
	  
	  $(document).on('click','#avatar',function(){
	      openKCFinder(this);
	  });
	  
	  function openKCFinder(div) {
	      window.KCFinder = {
		  callBack: function(url) {
		      window.KCFinder = null;
		      console.log(url);
		      $('#avatar').attr('src',url);
		      $(div).next().val(url);
		  }
	      };
	      window.open('<?=$this->config->item("plugin")?>/kcfinder/browse.php?type=images',
		  'kcfinder_image', 'status=0, toolbar=0, location=0, menubar=0, ' +
		  'directories=0, resizable=1, scrollbars=0, width=800, height=600'
	      );
	  }
  
	  $('#datetimepicker1').datetimepicker({
		format: 'YYYY-MM-DD',
		viewMode : 'years'
	    });
		
		
		<?php
		/*
		$("input[name$='training_institute_status']").click(function() {
			if($(this).attr("value")=="pns"){
				$(".kotak").not(".negeri").hide();
				$("input.swasta").val("");	
				$(".negeri").show();
			} else {
				$(".kotak").not(".swasta").hide();
				$("input.negeri").val("");
				$(".swasta").show();
			}
		});
		*/
		?>
		
		$(document).on('click',"input[name$='training_institute_status']",function() {
			if($(this).attr("value")=="pns"){
				$(".kotak").not(".negeri").hide();
				$("input.swasta").val("");	
				$(".negeri").show();
			} else {
				$(".kotak").not(".swasta").hide();
				$("input.negeri").val("");
				$(".swasta").show();
			}
		});
</script>