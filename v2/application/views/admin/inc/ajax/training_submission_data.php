<?php
    
    $query = $this->training_db->get($training_id);
    if($query):
// 	print_r($response_body);
	$content = $query[0];
?>

<div class="form-body">

    <div class="form-group">
	<label class="col-sm-2 control-label">No Surat Permohonan:</label>
	<div class="col-sm-10">
	    
	    <p class="form-control-static text-content"> <?=(isset($content['mail_submission_id']) && $content['mail_submission_id'] ? '<a target="_blank" href="'.$content['mail_submission_upload'].'">'.$content['mail_submission_id'].'</a>' : '')?> </p>
	</div>
    </div>
    
    <div class="form-group">
	<label class="col-sm-2 control-label">Jenis Pengajuan:</label>
	<div class="col-sm-10">
	    
	    <p class="form-control-static text-content"> <?=(isset($content['type_submission_training']['name']) ? $content['type_submission_training']['name'] : '')?> </p>
	</div>
    </div>
    
    <div class="form-group">
	<label class="col-sm-2 control-label">Jenis Pelatihan:</label>
	<div class="col-sm-10">
	    
	    <p class="form-control-static text-content"> <?=(isset($content['type_training']['name'])  ? $content['type_training']['name'] : '')?> </p>
	</div>
    </div>
    
    <div class="form-group">
	<label class="col-sm-2 control-label">Nama Pelatihan:</label>
	<div class="col-sm-10">
	    <p class="form-control-static text-content"> <?=(isset($content['training_name']['name'])  ? $content['training_name']['name'] : '')?> </p>
	</div>
    </div>

    <div class="form-group">
	<label class="col-sm-2 control-label">Tanggal Pelatihan:</label>
	<div class="col-sm-10">
	    <p class="form-control-static text-content"> <?=(isset($content['training_date']) && $content['training_date'] ? $content['training_date'] : '')?> </p>
	</div>
    </div>

    <div class="form-group">
	<label class="col-sm-2 control-label">Jam Mulai Pelatihan:</label>
	<div class="col-sm-10">
	    <p class="form-control-static text-content"> <?=(isset($content['start_training_time']) && $content['start_training_time'] ? $content['start_training_time'] : '')?> </p>
	</div>
    </div>

    <div class="form-group">
	<label class="col-sm-2 control-label">Jumlah Kelas:</label>
	<div class="col-sm-10">
	    <p class="form-control-static text-content"> <?=(isset($content['office_phone']) && $content['office_phone'] ? $content['office_phone'] : '')?> </p>
	</div>
    </div>    
</div>

<?php endif; //200?>