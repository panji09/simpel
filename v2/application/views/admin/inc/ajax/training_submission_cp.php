<?php
    $query = $this->training_db->get($training_id);
    if($query):
// 	print_r($response_body);
	$content = $query[0];
?>

<div class="form-body">

    <div class="form-group">
	<label class="col-sm-2 control-label">Contact Person Nama:</label>
	<div class="col-sm-10">
	    
	    <p class="form-control-static text-content"> <?=(isset($content['contact_person_name'])  ? $content['contact_person_name'] : '')?> </p>
	</div>
    </div>
    
    <div class="form-group">
	<label class="col-sm-2 control-label">Contact Person No. HP:</label>
	<div class="col-sm-10">
	    
	    <p class="form-control-static text-content"> <?=(isset($content['contact_person_phone'])  ? $content['contact_person_phone'] : '')?> </p>
	</div>
    </div>  
</div>

<?php endif; //200?>