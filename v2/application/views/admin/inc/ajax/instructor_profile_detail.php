<?php
    $token = $this->connect_auth->get_access_token();
    $response = call_api_get($this->config->item('connect_api_url').'/user/index/'.$token.'/'.$user_id);
    $response_body = json_decode($response['body'], true);
    
    if($response['header_info']['http_code'] == 200):
// 	print_r($response_body);
	$user = $response_body;
?>
<div class="form-body">

<h4 class="form-section">Data Pendidikan dan Kepegawaian</h4>
	<div class="form-group">
		<label class="control-label col-md-3">Pendidikan Terakhir:</label>
		<div class="col-md-9">
			<p class="form-control-static">
				  <?php if(isset($user['last_education'])) echo $user['last_education'];?>
			</p>
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-3">Gelar Akademis:</label>
		<div class="col-md-9">
			<p class="form-control-static">
				  <?php if(isset($user['academic_degree'])) echo $user['academic_degree'];?>
			</p>
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-3">Status Kepegawaian:</label>
		<div class="col-md-9">
			<p class="form-control-static">
				  <?php if(isset($user['employment'])) echo $user['employment'];?>
			</p>
		</div>
	</div>
	<?php if(isset($user['nip_id']) && $user['nip_id']!=''){?>
	<div class="form-group">
		<label class="control-label col-md-3">NIP/NRP:</label>
		<div class="col-md-9">
			<p class="form-control-static">
				  <?php echo $user['nip_id'];?>
			</p>
		</div>
	</div>
	<?php }?>
	<div class="form-group">
		<label class="control-label col-md-3">No.KTP:</label>
		<div class="col-md-9">
			<p class="form-control-static">
				  <?php if(isset($user['ktp_id'])) echo $user['ktp_id'];?>
			</p>
		</div>
	</div>
	
	<h4 class="form-section">Data Kantor</h4>
	<div class="form-group">
		<label class="control-label col-md-3">Nama Instansi/Institusi/Perusahaan:</label>
		<div class="col-md-9">
			<p class="form-control-static">
				  <?php if(isset($user['institution'])) echo $user['institution'];?>
			</p>
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-3">Satuan Kerja:</label>
		<div class="col-md-9">
			<p class="form-control-static">
				  <?php if(isset($user['work_unit'])) echo $user['work_unit'];?>
			</p>
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-3">Provinsi:</label>
		<div class="col-md-9">
			<p class="form-control-static">
				  <?php if(isset($user['office_province']['name'])) echo $user['office_province']['name'];?>
			</p>
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-3">Kab/Kota:</label>
		<div class="col-md-9">
			<p class="form-control-static">
				  <?php if(isset($user['office_district']['name'])) echo $user['office_district']['name'];?>
			</p>
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-3">Alamat:</label>
		<div class="col-md-9">
			<p class="form-control-static">
		  <?=(isset($user['office_address']) ? $user['office_address'] : '');?>
			</p>
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-3">Kode Pos:</label>
		<div class="col-md-9">
			<p class="form-control-static">
		  <?=(isset($user['office_zip_code']) ? $user['office_zip_code'] : '');?>
			</p>
		</div>
	</div>
	
	<h4 class="form-section">Data Atasan</h4>
	<div class="form-group">
		<label class="control-label col-md-3">Nama:</label>
		<div class="col-md-9">
			<p class="form-control-static">
				  <?php if(isset($user['head_name'])) echo $user['head_name'];?>
			</p>
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-3">Email:</label>
		<div class="col-md-9">
			<p class="form-control-static">
				  <?php if(isset($user['head_email'])) echo $user['head_email'];?>
			</p>
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-3">No. Telp:</label>
		<div class="col-md-9">
			<p class="form-control-static">
				  <?php if(isset($user['head_phone'])) echo $user['head_phone'];?>
			</p>
		</div>
	</div>
</div>
<?php endif; //200?> 
