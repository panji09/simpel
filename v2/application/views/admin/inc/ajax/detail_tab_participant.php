 
		      <div class="portlet light">
			  <div class="portlet-title tabbable-line">
			      <div class="caption caption-md">
				  <i class="icon-globe theme-font-color hide"></i>
				  <span class="caption-subject theme-font-color bold uppercase">Peserta</span>
			      </div>
			      <ul class="nav nav-tabs">
				  <li class="active">
				      <a href="#tab_list_participant" data-toggle="tab">List Peserta </a>
				  </li>
				  <li>
				      <a href="#tab_set_participant" data-toggle="tab">Set Peserta </a>
				  </li>
			      </ul>
			  </div>
			  
			  <div class="portlet-body">
			      <div class="tab-content">
				  <div class="tab-pane active" id="tab_list_participant">
				      
				      
				      <div class="portlet light">
					  <div class="portlet-title">
					    <div class="caption font-green-sharp">
						<span class="caption-subject bold uppercase"> List Peserta</span>						
					    </div>
					    <div class="actions">
						
						<a href="javascript:;" class="btn btn-circle btn-default btn-icon-only fullscreen"></a>
					    </div>
					  </div>
					  <div class="portlet-body form portlet-empty">
					    <?php $this->load->view('home/inc/ajax/list_participant')?>
					    
					  </div>
				      </div>
				  </div>
				  
				  <div class="tab-pane" id="tab_list_participant">
				      set peserta
				  </div>
				  
			      </div>  
			  </div>
			  
		      </div>
		   
