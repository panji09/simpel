<?php
    $token = $this->connect_auth->get_access_token();
    $response = call_api_get($this->config->item('connect_api_url').'/user/index/'.$token.'/'.$user_id);
    $response_body = json_decode($response['body'], true);
    
    if($response['header_info']['http_code'] == 200):
// 	print_r($response_body);
	$user = $response_body;

    
?>
<div class="form-body">

    <div class="form-group">
	<label class="col-sm-4 control-label">Surat Permohonan Terdaftar/Terakreditasi:</label>
	<div class="col-sm-8">
	    
	    <p class="form-control-static"> <?=(isset($user['accreditation_upload']) && $user['accreditation_upload'] ? anchor($user['accreditation_upload'], basename($user['accreditation_upload']), array('target' => '_blank')) : '')?> </p>
	</div>
    </div>

    <div class="form-group">
	<label class="col-sm-4 control-label">Pernyataan Komitmen:</label>
	<div class="col-sm-8">
	    <p class="form-control-static"> <?=(isset($user['commitment_upload']) && $user['commitment_upload'] ? anchor($user['commitment_upload'], basename($user['commitment_upload']), array('target' => '_blank')) : '')?> </p>
	</div>
    </div>
    
    <?php if(isset($user['training_institute_status']) && strtolower($user['training_institute_status']) == 'negeri'):?>
    <div class="form-group">
	<label class="col-sm-4 control-label">Upload Bukti Dukung Memiliki Tugas Fungsi (TUPOKSI) Pelatihan:</label>
	<div class="col-sm-8">
	    
		<p class="form-control-static"> <?=(isset($user['training_task_upload']) && $user['training_task_upload'] ? anchor($user['training_task_upload'], basename($user['training_task_upload']), array('target' => '_blank')) : '')?> </p>

	</div>
    </div>
    
    <div class="form-group">
	<label class="col-sm-4 control-label">Persyaratan Keuangan:</label>
	<div class="col-sm-8">
	    <p class="form-control-static"> <?=(isset($user['financial_requirement_upload']) && $user['financial_requirement_upload'] ? anchor($user['financial_requirement_upload'], basename($user['financial_requirement_upload']), array('target' => '_blank')) : '')?> </p>
	</div>
    </div>
    <?php endif;//negeri?>
    
    <?php if(isset($user['training_institute_status']) && strtolower($user['training_institute_status']) == 'swasta'):?>
    <div class="form-group">
	<label class="col-sm-4 control-label">Upload Surat Tugas Melaksanakan Pendidikan/Pelatihan:</label>
	<div class="col-sm-8">
	    
		<p class="form-control-static"> <?=(isset($user['training_task_upload']) && $user['training_task_upload'] ? anchor($user['training_task_upload'], basename($user['training_task_upload']), array('target' => '_blank')) : '')?> </p>
	    
	</div>
    </div>
    
    <div class="form-group">
	<label class="col-sm-4 control-label">Persyaratan Keuangan:</label>
	<div class="col-sm-8">
	    <p class="form-control-static"> <?=(isset($user['financial_npwp_number']) && $user['financial_npwp_number'] ? $user['financial_npwp_number'] : '')?> </p>
	    <p class="form-control-static"> <?=(isset($user['financial_requirement_npwp_upload']) && $user['financial_requirement_npwp_upload'] ? anchor($user['financial_requirement_npwp_upload'], basename($user['financial_requirement_npwp_upload']), array('target' => '_blank')) : '')?> </p>
	    
	</div>
    </div>
    <?php endif;//negeri?>
    
    <div class="form-group">
	<label class="col-sm-4 control-label">Sistem Manajemen Mutu:</label>
	<div class="col-sm-8">
	    <p class="form-control-static"> <?=(isset($user['quality_management_upload']) && $user['quality_management_upload'] ? anchor($user['quality_management_upload'], basename($user['quality_management_upload']), array('target' => '_blank')) : '')?> </p>
	</div>
    </div>
    
    <div class="form-group">
	<label class="col-sm-4 control-label">Standar Pelatihan:</label>
	<div class="col-sm-8">
	    <p class="form-control-static"> <?=(isset($user['standart_training_upload']) && $user['standart_training_upload'] ? anchor($user['standart_training_upload'], basename($user['standart_training_upload']), array('target' => '_blank')) : '')?> </p>
	</div>
    </div>
    
</div>
<?php endif; //200?>