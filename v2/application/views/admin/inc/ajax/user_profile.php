<?php
    $token = $this->connect_auth->get_access_token();
    $response = call_api_get($this->config->item('connect_api_url').'/user/index/'.$token.'/'.$user_id);
    $response_body = json_decode($response['body'], true);
    
    if($response['header_info']['http_code'] == 200):
// 	print_r($response_body);
	$user = $response_body;
?>
<div class="form-body">

    <div class="form-group">
	<label class="col-sm-2 control-label">Foto:</label>
	<div class="col-sm-10">
	    
	    <img class='img-responsive' width='200' src="<?=(isset($user['photo']) && $user['photo'] ? $user['photo'] : 'http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image')?>" alt="">
	</div>
    </div>

    <div class="form-group">
	<label class="col-sm-2 control-label">Username:</label>
	<div class="col-sm-10">
	    <p class="form-control-static"> <?=(isset($user['username']) && $user['username'] ? $user['username'] : '')?> </p>
	</div>
    </div>

    <div class="form-group">
	<label class="col-sm-2 control-label">Email:</label>
	<div class="col-sm-10">
	    <p class="form-control-static"> <?=(isset($user['email']) && $user['email'] ? $user['email'] : '')?> </p>
	</div>
    </div>

    <div class="form-group">
	<label class="col-sm-2 control-label">Nama:</label>
	<div class="col-sm-10">
	    <p class="form-control-static"> <?=(isset($user['fullname']) && $user['fullname'] ? $user['fullname'] : '')?> </p>
	</div>
    </div>

    <div class="form-group">
	<label class="col-sm-2 control-label">Tempat Lahir:</label>
	<div class="col-sm-10">
	    <p class="form-control-static"> <?=(isset($user['birth_place']) && $user['birth_place'] ? $user['birth_place'] : '')?> </p>
	</div>
    </div>

    <div class="form-group">
	<label class="col-sm-2 control-label">Tanggal Lahir:</label>
	<div class="col-sm-10">
	    <p class="form-control-static"> <?=(isset($user['birth_date']) && $user['birth_date'] ? $user['birth_date'] : '')?> </p>
	</div>
    </div>

    <div class="form-group">
	<label class="col-sm-2 control-label">No. Telp/HP:</label>
	<div class="col-sm-10">
	    <p class="form-control-static"> <?=(isset($user['phone']) && $user['phone'] ? $user['phone'] : '')?> </p>
	</div>
    </div>

    <div class="form-group">
	<label class="col-sm-2 control-label">Jenis Kelamin:</label>
	<div class="col-sm-10">
	    <p class="form-control-static"> <?=(isset($user['gender']) && $user['gender'] ? (strtolower($user['gender'])=='m' ? 'Laki - Laki' : 'Perempuan') : '')?> </p>
	</div>
    </div>
</div>
<?php endif; //200?>