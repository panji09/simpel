<?php
   $token = $this->connect_auth->get_access_token();
   $query = $this->training_db->get($training_id);
   if($query){
   	$training = $query[0];
   	$user = $training['user_submission'];
   }
   
   $on_progress = ($this->uri->segment(6) ? true : false);
?>
		      <div class="portlet light">
			  <div class="portlet-title tabbable-line">
			      <div class="caption caption-md">
				  <i class="icon-globe theme-font-color hide"></i>
				  <span class="caption-subject theme-font-color bold uppercase">Overview</span>
			      </div>
			      
			  </div>
			  
			  <div class="portlet-body">
			      <div class="container">
				<div class="col-md-12">
				    <div class="list-group todolist">
				      <!--content overview-->
				      
				      <div class="form">
	 							<!-- BEGIN PROGRESS -->
	                             <div class="margin-top-20">
	                                <h3>Progress</h3>
	                             </div>
					<div style="padding: 20px">
					    <?php $this->load->view('admin/inc/widget/progress')?>
					</div>
					<!-- END PROGRESS -->
				  
					<!-- BEGIN INFO PENGAJUAN PELATIHAN -->
	                             <div class="margin-top-20">
	                                <h3>Info Pengajuan Pelatihan</h3>
	                             </div>
	                             <div class="form-horizontal">
	                                <?php $this->load->view('admin/inc/widget/overview_info_submission_training', array('training' => $training))?>
	                             </div>
	 							<!-- END INFO PENGAJUAN PELATIHAN -->
							
	 							<!-- BEGIN  INFO LEMBAGA PELATIHAN -->
	                            <div class="margin-top-20">
	                               <h3>Info Lembaga pelatihan</h3>
	                            </div>
	                            <div class="form-horizontal">
					<?php $this->load->view('admin/inc/widget/overview_info_institute', array('training' => $training, 'user' => $user))?>
	                            </div>
	 						   <!-- END INFO LEMBAGA PELATIHAN -->
							</div>
                        
				      
				      <!--./content overview-->
				    </div>
				</div>
			      </div>  
			  </div>
			  
		    </div>
                      
