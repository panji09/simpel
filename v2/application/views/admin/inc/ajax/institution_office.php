<?php
    $token = $this->connect_auth->get_access_token();
    $response = call_api_get($this->config->item('connect_api_url').'/user/index/'.$token.'/'.$user_id);
    $response_body = json_decode($response['body'], true);
    
    if($response['header_info']['http_code'] == 200):
// 	print_r($response_body);
	$user = $response_body;
?>
<div class="form-body">

    <div class="form-group">
	<label class="col-sm-2 control-label">Nama Lembaga:</label>
	<div class="col-sm-10">
	    
	    <p class="form-control-static"> <?=(isset($user['training_institute']) && $user['training_institute'] ? $user['training_institute'] : '')?> </p>
	</div>
    </div>
    
    <div class="form-group">
	<label class="col-sm-2 control-label">Status Lembaga Pelatihan:</label>
	<div class="col-sm-8">
	    
	    <p class="form-control-static"> <?=(isset($user['training_institute_status']) && $user['training_institute_status'] ? $user['training_institute_status'] : '')?> </p>
	</div>
    </div>
    
    <div class="form-group">
	<label class="col-sm-2 control-label">Akreditasi:</label>
	<div class="col-sm-8">
	    
	    <p class="form-control-static"> <?=(isset($user['accreditation_level']) && $user['accreditation_level'] ? $user['accreditation_level'] : '')?> </p>
	</div>
    </div>
    
    <div class="form-group">
	<label class="col-sm-2 control-label">Alamat Kantor:</label>
	<div class="col-sm-10">
	    <p class="form-control-static"> <?=(isset($user['office_address']) && $user['office_address'] ? $user['office_address'] : '')?> </p>
	</div>
    </div>

    <div class="form-group">
	<label class="col-sm-2 control-label">Provinsi Kantor:</label>
	<div class="col-sm-10">
	    <p class="form-control-static"> <?=(isset($user['office_province']) && $user['office_province'] ? $user['office_province']['name'] : '')?> </p>
	</div>
    </div>

    <div class="form-group">
	<label class="col-sm-2 control-label">Kab/Kota Kantor:</label>
	<div class="col-sm-10">
	    <p class="form-control-static"> <?=(isset($user['office_district']) && $user['office_district'] ? $user['office_district']['name'] : '')?> </p>
	</div>
    </div>

    <div class="form-group">
	<label class="col-sm-2 control-label">No.Telp Kantor:</label>
	<div class="col-sm-10">
	    <p class="form-control-static"> <?=(isset($user['office_phone']) && $user['office_phone'] ? $user['office_phone'] : '')?> </p>
	</div>
    </div>

    <div class="form-group">
	<label class="col-sm-2 control-label">No.Fax Kantor:</label>
	<div class="col-sm-10">
	    <p class="form-control-static"> <?=(isset($user['office_fax']) && $user['office_fax'] ? $user['office_fax'] : '')?> </p>
	</div>
    </div>

    
</div>
<?php endif; //200?>