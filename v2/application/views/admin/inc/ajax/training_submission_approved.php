<?php
    $token = $this->connect_auth->get_access_token();
    $response = call_api_get($this->config->item('connect_api_url').'/user/all/'.$token.'/fasilitator');
    $response_body = json_decode($response['body'], true);
    
    $fasilitator = array();
    if($response['header_info']['http_code'] == 200){
	$fasilitator = $response_body['data'];
    }
    
    $query = $this->training_db->get($training_id);
    
    if($query){
	$content = $query[0];
    }
    
?>

<div class="form-group">
    <label for="inputEmail3" class="col-sm-2 control-label">Penugasan Fasilitator</label>
    <div class="col-sm-10">
	<select class="form-control" name='facilitator_user_id'>
	    <option value="">-Pilih-</option>
	    <?php foreach($fasilitator as $row ):?>
	    <option value='<?=$row['user_id']?>' <?=(isset($content['facilitator_user_id']) && $content['facilitator_user_id'] == $row['user_id']  ? 
	    'selected' : '' )?>><?=$row['fullname']?></option>
	    <?php endforeach;?>
	</select>
	
    </div>
</div>

<div class="form-group">
    <label for="inputEmail3" class="col-sm-2 control-label">Form Evaluasi untuk LPP</label>
    <div class="col-sm-10">
	<select class="form-control" name='evaluation_form[institute]'>
	    <option value="">-Pilih-</option>
	    <?php
		$query = $this->evaluation_db->get_all(array('published' => 1));
		if($query)
		foreach($query as $row):
	    ?>
	    <option value="<?=$row['_id']->{'$id'}?>" <?=(isset($content['evaluation_form']['institute']) && $content['evaluation_form']['institute'] == $row['_id']->{'$id'}  ? 
	    'selected' : '' )?>><?=$row['name']?></option>
	    
	    <?php
		endforeach;
	    ?>
	    
	</select>
	
    </div>
</div>

<div class="form-group">
    <label for="inputEmail3" class="col-sm-2 control-label">Form Evaluasi untuk Pengajar</label>
    <div class="col-sm-10">
	<select class="form-control" name='evaluation_form[instructor]'>
	    <option value="">-Pilih-</option>
	    <?php
		$query = $this->evaluation_db->get_all(array('published' => 1));
		if($query)
		foreach($query as $row):
	    ?>
	    
	    <option value="<?=$row['_id']->{'$id'}?>" <?=(isset($content['evaluation_form']['instructor']) && $content['evaluation_form']['instructor'] == $row['_id']->{'$id'}  ? 
	    'selected' : '' )?>><?=$row['name']?></option>
	    <?php
		endforeach;
	    ?>
	</select>
	
    </div>
</div>

<div class="form-group">
    <label for="inputEmail3" class="col-sm-2 control-label">Checklist Pelatihan</label>
    <div class="col-sm-10">
	<select class="form-control" name='todolist'>
	    <option value="">-Pilih-</option>
	    <?php
		$query = $this->todolist_db->get_all(array('published' => 1));
		if($query)
		foreach($query as $row):
	    ?>
	    
	    <option value="<?=$row['_id']->{'$id'}?>" <?=(isset($content['todolist']) && $content['todolist'] == $row['_id']->{'$id'}  ? 
	    'selected' : '' )?>><?=$row['name']?></option>
	    <?php
		endforeach;
	    ?>
	</select>
	
    </div>
</div>
