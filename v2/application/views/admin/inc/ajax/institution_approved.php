<?php
    $token = $this->connect_auth->get_access_token();
    $response = call_api_get($this->config->item('connect_api_url').'/user/index/'.$token.'/'.$user_id);
    $response_body = json_decode($response['body'], true);
    
    $user = array();
    if($response['header_info']['http_code'] == 200){
	$user = $response_body;
    }
?>
<div class="form-group">
    <label for="inputEmail3" class="col-sm-2 control-label">Akreditasi</label>
    <div class="col-sm-10">
	<select class="form-control" name='accreditation_level'>
	    <option value="">-Pilih-</option>
	    <?php foreach($this->init_config_db->get_accreditation_level() as $row ):?>
	    <option value='<?=$row['id']?>' <?=(isset($user['accreditation_level']) && $user['accreditation_level'] == $row['id'] ? 'selected' : '')?>><?=ucfirst($row['name'])?></option>
	    <?php endforeach;?>
	</select>
	<input type='hidden' name='verified_institute' value='1'>
    </div>
</div>