<?php
    $query = $this->training_db->get($training_id);
   
    if($query):
		$training = $query[0];
	
	$training_date = explode(' - ',$training['training_date']);
	$training_date_start = $training_date[0];
	$training_date_end = $training_date[1];
?> 

<div class="form-horizontal">
	<div class="form-group">
		<label class="control-label col-md-4">Tanggal</label>
		<div class="col-md-8">
			<div class="input-group input-medium date datepicker" data-date-format="yyyy-mm-dd" data-date-start-date="+0d">
				<input type="text" class="form-control set_instructor_date" readonly>
				<span class="input-group-btn">
					<button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
				</span>
			</div>
			<!-- /input-group -->
			<span class="help-block">
				pilih tanggal 
			</span>
		</div>
	</div>
		
	<div class="form-group">
		<label class="control-label col-md-4">Dari jam</label>
		<div class="col-md-8">
			
			<div class="form-group">
			    <div class='input-group date timepicker set_instructor_time_start' >
				<input type='text' class="form-control" />
				<span class="input-group-addon">
				    <span class="glyphicon glyphicon-time"></span>
				</span>
			    </div>
			</div>
		</div>
	</div>
	
	<div class="form-group">
		<label class="control-label col-md-4">Sampai Jam</label>
		<div class="col-md-8">
			<div class="form-group">
			    <div class='input-group date timepicker set_instructor_time_end' >
				<input type='text' class="form-control" />
				<span class="input-group-addon">
				    <span class="glyphicon glyphicon-time"></span>
				</span>
			    </div>
			</div>
		</div>
	</div>
	
</div>



<?php /* COMMENTED BY YOZE
<div class="form-group">
 	<label class="col-sm-4 control-label">Tanggal</label>   
	<div class="col-sm-8">
		<?php for($i=strtotime($training_date_start); $i <= strtotime($training_date_end); $i+= 60 * 60 * 24  ):?>
		<div class="checkbox">
			<label>
				<input type="checkbox" class='set_date' name='set_date[]' value='<?=date("Y-m-d", $i)?>'> <?=date("Y-m-d", $i); ?>
	  	  	</label>
		</div>
		<?php endfor;?>
	</div>
</div>

<div class="form-group">
    <label for="inputEmail3" class="col-sm-4 control-label">Jumlah Jam Pelajaran</label>
    <div class="col-sm-8">
	<input type='text' class="form-control" name='hour_leasson' id='hour_leasson_modal'></div>
</div>
*/ ?>

<?php endif;?>