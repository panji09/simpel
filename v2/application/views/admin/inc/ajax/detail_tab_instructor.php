
		      <div class="portlet light">
			  <div class="portlet-title tabbable-line">
				  <div class="caption caption-md">
					  <i class="icon-globe theme-font-color hide"></i>
					  <span class="caption-subject theme-font-color bold uppercase">Pengajar</span>
				  </div>
				  <ul class="nav nav-tabs">
					   <li class="active">
						  <a href="#tab_list_instructor" data-toggle="tab">
						  List Pengajar </a>
					  </li>
					  <li>
						  <a href="#tab_status_instructor" data-toggle="tab">
						  Status Pengajar </a>
					  </li>
					  <li>
						  <a href="#tab_set_instructor" data-toggle="tab">
						  Set Pengajar </a>
					  </li>
				  </ul>
			  </div>
			  <div class="portlet-body">
					<!--BEGIN TABS-->
					<div class="tab-content">
						<div class="tab-pane active" id="tab_list_instructor">
						    <!--list pengajar-->
						    <div class="portlet light">
							<div class="portlet-title">
							  <div class="caption font-green-sharp">
							      <span class="caption-subject bold uppercase"> List Pengajar</span>						
							  </div>
							  <div class="actions">
							      
							      <a href="javascript:;" class="btn btn-circle btn-default btn-icon-only fullscreen"></a>
							  </div>
							</div>
							<div class="portlet-body form portlet-empty">
							  <?php $this->load->view('admin/inc/widget/submission_instructor')?>
							  
							</div>
						    </div>
						    <!--./list pengajar-->
						</div>
						<div class="tab-pane" id="tab_status_instructor">
						    <!--status pengajar-->
						    
						    <div class="portlet light">
							<div class="portlet-title">
							  <div class="caption font-green-sharp">
							      <span class="caption-subject bold uppercase"> Status Pengajar</span>						
							  </div>
							  <div class="actions">
							      
							      <a href="javascript:;" class="btn btn-circle btn-default btn-icon-only fullscreen"></a>
							  </div>
							</div>
							<div class="portlet-body form portlet-empty">
							  <?php $this->load->view('admin/inc/widget/status_submission_instructor')?>
							</div>
						    </div>
						    
						    <!--./status pengajar-->
						</div>
						<div class="tab-pane" id="tab_set_instructor">
						    <!--set instructor-->
						    <div class="portlet light">
							<div class="portlet-title">
							  <div class="caption font-green-sharp">
							      <span class="caption-subject bold uppercase"> Set Pengajar Pelatihan</span>						
							  </div>
							  <div class="actions">
							      <a href="javascript:;" class="btn btn-circle red add_instructor">
							      <i class="fa fa-plus"></i> Tambah Pengajar </a>
							      <a href="javascript:;" class="btn btn-circle btn-default btn-icon-only fullscreen"></a>
							  </div>
							</div>
							<div class="portlet-body form portlet-empty">
							  <form method='post' action='<?=site_url('fasilitator/management_training/hour_leasson_post/'.$training_id)?>'>
							      <div class="table-scrollable form-horizontal margin-bottom-20" id='table_content'>
								<?php $this->load->view('admin/inc/table_set_instructor')?>
							      </div>
							      <div class="form-actions">
								<div class="row">
								    <div class="col-md-offset-2 col-md-10">
								      <button type="submit" class="btn blue" name='submit' value='save'>Proses</button>
								      
								      
								    </div>
								</div>
							      </div>
							  </form>
							  <div id='template_table_set_instructor' style='display:none'>
							      <?php $this->load->view('admin/inc/table_set_instructor')?>
							  </div>
							</div>
						    </div>
						    <!--./set instructor-->
						</div>
					</div>
					<!--END TABS-->
				</div>
		      </div>
                   
