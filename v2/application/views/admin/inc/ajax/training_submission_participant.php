<?php
     $query = $this->training_db->get($training_id);
    if($query):
// 	print_r($response_body);
	$content = $query[0];
?>

<div class="form-body">

    <div class="form-group">
	<label class="col-sm-2 control-label">Pendaftaran Peserta:</label>
	<div class="col-sm-10">
	    
	    <p class="form-control-static text-content"> <?=(isset($content['type_registration_participant']['name'])  ? $content['type_registration_participant']['name'] : '')?> </p>
	</div>
    </div>
    
    <div class="form-group">
	<label class="col-sm-2 control-label">Jumlah Peserta:</label>
	<div class="col-sm-10">
	    
	    <p class="form-control-static text-content"> <?=(isset($content['participant_amount'])  ? $content['participant_amount'] : '')?> </p>
	</div>
    </div>  
</div>

<?php endif; //200?>