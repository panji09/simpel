<?php
    $token = $this->connect_auth->get_access_token();
    $response = call_api_get($this->config->item('connect_api_url').'/user/index/'.$token.'/'.$user_id);
    $response_body = json_decode($response['body'], true);
    
    if($response['header_info']['http_code'] == 200):
// 	print_r($response_body);
	$user = $response_body;
?>
<div class="form-body">

    

    <div class="form-group">
	<label class="col-sm-4 control-label">Nama Penanggung Jawab:</label>
	<div class="col-sm-8">
	    <p class="form-control-static"> <?=(isset($user['responsible_person_name']) && $user['responsible_person_name'] ? $user['responsible_person_name'] : '')?> </p>
	</div>
    </div>

    <div class="form-group">
	<label class="col-sm-4 control-label">Jabatan Penanggung Jawab:</label>
	<div class="col-sm-8">
	    <p class="form-control-static"> <?=(isset($user['responsible_person_position']) && $user['responsible_person_position'] ? $user['responsible_person_position'] : '')?> </p>
	</div>
    </div>
    
    <?php if(isset($user['training_institute_status']) && strtolower($user['training_institute_status']) == 'negeri'):?>
    <div class="form-group">
	<label class="col-sm-4 control-label">Nip Penanggung Jawab:</label>
	<div class="col-sm-8">
	    <p class="form-control-static"> <?=(isset($user['responsible_person_nip']) && $user['responsible_person_nip'] ? $user['responsible_person_nip'] : '')?> </p>
	</div>
    </div>
    <?php endif;//negeri?>
    
    <?php if(isset($user['training_institute_status']) && strtolower($user['training_institute_status']) == 'swasta'):?>
    <div class="form-group">
	<label class="col-sm-4 control-label">No.KTP Penanggung Jawab:</label>
	<div class="col-sm-8">
	    <p class="form-control-static"> <?=(isset($user['responsible_person_ktp']) && $user['responsible_person_ktp'] ? $user['responsible_person_ktp'] : '')?> </p>
	</div>
    </div>

    <div class="form-group">
	<label class="col-sm-4 control-label">KTP Penanggung Jawab:</label>
	<div class="col-sm-8">
	    <p class="form-control-static"> <?=(isset($user['responsible_person_ktp_upload']) && $user['responsible_person_ktp_upload'] ? anchor($user['responsible_person_ktp_upload'], basename($user['responsible_person_ktp_upload']), array('target' => '_blank')) : '')?> </p>
	</div>
    </div>
    <?php endif;//swasta?>
    
    <div class="form-group">
	<label class="col-sm-4 control-label">Struktur Organisasi:</label>
	<div class="col-sm-8">
	    <p class="form-control-static"> <?=(isset($user['structure_organization_upload']) && $user['structure_organization_upload'] ? anchor($user['structure_organization_upload'], basename($user['structure_organization_upload']), array('target' => '_blank')) : '')?> </p>
	</div>
    </div>
    
</div>
<?php endif; //200?>