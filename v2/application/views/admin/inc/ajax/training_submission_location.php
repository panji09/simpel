<?php
    $query = $this->training_db->get($training_id);
    if($query):
// 	print_r($response_body);
	$content = $query[0];
?>

<div class="form-body">

    <div class="form-group">
	<label class="col-sm-2 control-label">Tempat Pelatihan:</label>
	<div class="col-sm-10">
	    
	    <p class="form-control-static text-content"> <?=(isset($content['training_location_address']) && $content['training_location_address'] ? $content['training_location_address'] : '')?> </p>
	</div>
    </div>
    
    <div class="form-group">
	<label class="col-sm-2 control-label">Provinsi:</label>
	<div class="col-sm-10">
	    
	    <p class="form-control-static text-content"> <?=(isset($content['training_location_province']['name']) ? $content['training_location_province']['name'] : '')?> </p>
	</div>
    </div>
    
    <div class="form-group">
	<label class="col-sm-2 control-label">Kab/Kota:</label>
	<div class="col-sm-10">
	    <p class="form-control-static text-content"> <?=(isset($content['training_location_district']['name'])  ? $content['training_location_district']['name'] : '')?> </p>
	</div>
    </div>    
</div>

<?php endif; //200?>