<?php
    $token = $this->connect_auth->get_access_token();
    $response = call_api_get($this->config->item('connect_api_url').'/user/index/'.$token.'/'.$user_id);
    $response_body = json_decode($response['body'], true);
    
    $user = array();
    if($response['header_info']['http_code'] == 200){
	$user = $response_body;
    }
?>
<div class="form-group">
    <label for="inputEmail3" class="col-sm-2 control-label">Pesan</label>
    <div class="col-sm-10">
	<textarea class="form-control wysiwyg-content-full" rows="3" name='review_note'><?=(isset($user['review_note']) ? $user['review_note'] : '')?></textarea>
	<input type='hidden' name='verified_institute' value='-1'>
    </div>
</div>