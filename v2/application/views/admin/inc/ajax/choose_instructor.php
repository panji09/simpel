<h3>Filter Data Pengajar</h3>
<div class="table-toolbar">
	<div class="row">
		
		<div class="col-md-12">
		    <form class="form-inline">
		      
		      <div class="form-group">
			<label class="col-sm-3 control-label">Kompetensi</label>
			<div class="col-sm-9">
			  <input type="text" class="form-control column_filter" data-search_col='4'  placeholder="Kompetensi">
			</div>
		      </div>
		      
		      <div class="form-group">
			<label class="col-sm-3 control-label">Nama</label>
			<div class="col-sm-9">
			  <input type="text" class="form-control column_filter" data-search_col='0' placeholder="Nama">
			</div>
		      </div>
		      <div class="form-group">
			<label for="inputPassword3" class="col-sm-3 control-label">Provinsi</label>
			<div class="col-sm-9">
			  <select name='training_location_province' class="form-control select2me column_filter_select" data-search_col='1' id='province' style='width:100%'>
			    <option value=''>-Pilih-</option>
			    <?php
				$query = file_get_contents($this->config->item('connect_api_url').'/location/province');
				$province = array();
				if($query){
				    $province = json_decode($query,true);
				}
				
				if($province)
				foreach($province as $row):
			    ?>
				<option value='<?=$row['id']?>'  ><?=$row['name']?></option>
			    <?php
			    endforeach;
			    ?>
			    
			  </select>
			</div>
		      </div>
		      <div class="form-group">
			<label for="inputPassword3" class="col-sm-3 control-label ">kab / Kota</label>
			<div class="col-sm-9">
			  <select name='training_location_district' class="column_filter_select form-control select2me" data-search_col='1' id='district' style='width:100%'>
			      <option value=''>-Pilih-</option>
			      
			      
			    </select>
			</div>
		      </div>
		      
		    </form>
			
		</div>
		
	</div>
</div>

<table class="table table-striped table-bordered table-hover" id="table_choose_instructor">
<thead>
	<tr>
		<th>
			  Nama
		</th>
		<th>
			  Lokasi Kantor
		</th>
		<th>
			  Jumlah Jam Pelajaran/tahun
		</th>
		<th>
			  Pelatihan Terakhir
		</th>
		<th>
			  Kompetensi
		</th>
		<th>
			  Action
		</th>
	</tr>
</thead>

<tbody>
<?php
    $response = call_api_get($this->config->item('connect_api_url').'/user/all/'.$token.'/narasumber');
    $response_body = json_decode($response['body'], true);
    
    //check unavailable user
    $temp_unavailable = array();
    $unavailable_instructor_id = array();
    $query = $this->training_db->get($training_id);

    if($query){
	$training = $query[0];
	
	$training_date = explode(' - ',$training['training_date']);
	
	$training_date_start = $training_date[0];
	$training_date_end = $training_date[1];
	
	for($i=strtotime($training_date_start); $i <= strtotime($training_date_end); $i+= 60 * 60 * 24  ){
	    $query_agenda = $this->agenda_instructor_db->get(date("Y-m-d", $i));
	    
	    if($query_agenda)
	    foreach($query_agenda as $row_agenda){
		if($row_agenda['agenda'])
		foreach($row_agenda['agenda'] as $row_instructor){
		    $temp_unavailable[$row_instructor['instructor_id']] = 1;
		
		}
	    }
	}
    }
    
    if($temp_unavailable){
	foreach($temp_unavailable as $instructor_id => $row){
	    $unavailable_instructor_id[]= $instructor_id;
	}
    }
    
// 						    print_r($unavailable_instructor_id);
    //end check unavailable user
    
    if($response['header_info']['http_code'] == 200):
    foreach($response_body['data'] as $row):
	if(!in_array($row['user_id'], $unavailable_instructor_id ) ):
?>
<tr>
    <td><?=$row['fullname']?></td>
    <td><?=$row['office_province']['name'].', '.$row['office_district']['name']?></td>
    <td><?=$this->hour_leasson_db->get_count($instructor_id = $row['user_id'], $year = date('Y'))?></td>
    <td>-</td>
    <td><?=(isset($row['competence']) ? $row['competence'] : '') ?></td>
    <td>
	<button type='button' 
	  class="btn btn-default choose_instructor" 
	  data-toggle="modal" 
	  data-instructor_id="<?=$row['user_id']?>" 
	  data-instructor_name="<?=$row['fullname']?>" 
	  data-training_id="<?=$training_id?>" 
	  
	  data-target="#choose_instructor_modal"
	>Pilih</button>
    </td>
</tr>
<?php endif; endforeach; endif;?>
</tbody>
</table> 
