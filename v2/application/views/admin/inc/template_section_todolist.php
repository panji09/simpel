
<!-- BEGIN Portlet SECTION-->
<div class="portlet light" data-template="section" data-subject="lpp">
	<div class="portlet-title">
		<div class="caption" style="padding: 5px 0; width: 220px;">
			<!-- <i class="fa fa-puzzle-piece"></i>
			<span class="caption-subject bold uppercase"> Section </span> -->
			
		</div>
		<div class="actions">					
			<div class="btn-group">
				<button type="button" class="btn btn-sm blue add_todolist"><i class="fa fa-plus"></i> Tambah Tugas </i></button>
				
				
			</div>
		</div>
	</div>

	<div class="portlet-body">
		<div class='content_segment form-horizontal'>
		<?php
		    if($todolist_id):
			$query = $this->todolist_db->get($todolist_id);
			$todolist = $query[0];
			
		    if(isset($todolist['tasklist']))
		    foreach($todolist['tasklist'] as $row):
		?>
		
		<?php $this->load->view('admin/inc/template_segment_question_todolist', array('value' => $row))?>
		
		<?php endforeach;?>
		<?php else:?>
		
		<?php $this->load->view('admin/inc/template_segment_question_todolist')?>
		
		<?php endif;?>
		</div>
	</div>
</div>
<!-- END Portlet SECTION-->


 
