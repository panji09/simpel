<!-- BEGIN PAGE LEVEL PLUGINS -->

<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<!-- IMPORTANT! fullcalendar depends on jquery-ui.min.js for drag & drop support -->

<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?=$this->config->item('admin_js')?>/metronic.js" type="text/javascript"></script>
<script src="<?=$this->config->item('admin_js')?>/layout.js" type="text/javascript"></script>
<script src="<?=$this->config->item('admin_js')?>/demo.js" type="text/javascript"></script>
<script src="<?=$this->config->item('admin_js')?>/index3.js" type="text/javascript"></script>
<script src="<?=$this->config->item('admin_js')?>/tasks.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<script>
jQuery(document).ready(function() {    
   Metronic.init(); // init metronic core componets
   Layout.init(); // init layout
   //Demo.init(); // init demo features 
    //Index.init(); // init index page
 //Tasks.initDashboardWidget(); // init tash dashboard widget  
});


//visitor
<?php
   $visitor = file_get_contents(site_url('cache/visitor_summary'));
   $visitor = json_decode($visitor,true);
   $visitor_key = $visitor_value = array();
   if($visitor){
      foreach($visitor as $key => $value){
         $visitor_key[] = $key;
         $visitor_value[] = $value;
         
      }
   }
?>
$(function () {
    $('#container-visitor').highcharts({
        title: {
            text: 'Ringkasan Pengunjung',
            x: -20 //center
        },
        subtitle: {
            text: '',
            x: -20
        },
        xAxis: {
            categories: <?=json_encode($visitor_key)?>
        },
        yAxis: {
            title: {
                text: ''
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            valueSuffix: ''
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
        series: [{
            name: 'Pengunjung',
            data: <?=json_encode($visitor_value)?>
        }]
    });
});
// ./visitor

// now
<?php
   $visitor_live = file_get_contents(site_url('cache/visitor_hourly'));
   $visitor_live = json_decode($visitor_live,true);
   $visitor_live_key = $visitor_live_value = array();
   if($visitor_live){
      foreach($visitor_live as $row){
         $visitor_live_key[] = $row['label'];
         $visitor_live_value[] = $row['nb_uniq_visitors'];
         
      }
   }
?>
$(function () {
    $('#container-now').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'Pengunjung hari ini'
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            categories: <?=json_encode($visitor_live_key)?>,
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: ''
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.0f} </b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [{
            name: 'pengunjung',
            data: <?=json_encode($visitor_live_value)?>

        }]
    });
});
// ./now
</script>

