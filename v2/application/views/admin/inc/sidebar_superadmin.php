<?php $page=$this->uri->segment(2); $page1 = $this->uri->segment(3);?>
<div class="page-sidebar-wrapper">
    <div class="page-sidebar navbar-collapse collapse">
        <ul class="page-sidebar-menu " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
            <li <?=($page=='dashboard' ? 'class="start active "' : '')?>>
                <a href="<?=site_url('superadmin/dashboard')?>">
                    <i class="icon-home"></i>
                    <span class="title">Dashboard</span>
                </a>
            </li>
            <li <?=($page=='management_user' ? 'class="open active "' : '')?>>
                <a href="javascript:;">
                    <i class="icon-users"></i>
                    <span class="title">Management user</span>
                    <span class="arrow "></span>
                </a>
                <ul class="sub-menu">
                    <li <?=($page1=='institution' ? 'class="active "' : '')?>>
                        <a href="<?=site_url('superadmin/management_user/institution')?>">Lembaga Pelatihan / LPP</a>
                    </li>

                    <li <?=($page1=='participant' ? 'class="active "' : '')?>>
                        <a href="<?=site_url('superadmin/management_user/participant')?>">Peserta</a>
                    </li>

                    <li <?=($page1=='instructor' ? 'class="active "' : '')?>>
                        <a href="<?=site_url('superadmin/management_user/instructor')?>">Pengajar</a>
                    </li>
                    <li <?=($page1=='facilitator' ? 'class="active "' : '')?>>
                        <a href="<?=site_url('superadmin/management_user/facilitator')?>">Fasilitator</a>
                    </li>
                    <li <?=($page1=='admin' ? 'class="active "' : '')?>>
                        <a href="<?=site_url('superadmin/management_user/admin')?>">Admin</a>
                    </li>
                    <li <?=($page1=='superadmin' ? 'class="active "' : '')?>>
                        <a href="<?=site_url('superadmin/management_user/superadmin')?>">Superadmin</a>
                    </li>

                </ul>
            </li>
            <li <?=($page=='pages' ? 'class="open active "' : '')?>>
                <a href="javascript:;">
                    <i class="icon-docs"></i>
                    <span class="title">Halaman</span>
                    <span class="arrow "></span>
                </a>
                <ul class="sub-menu">
					<li <?=($page1=='info_slider' ? 'class="active "' : '')?>>
                        <a href="<?=site_url('superadmin/pages/info_slider')?>">Info Slider</a>
                    </li>
                    
                    <li <?=($page1=='event' ? 'class="active "' : '')?>>
                        <a href="<?=site_url('superadmin/pages/event')?>">Agenda</a>
                    </li>
                    
                    <li <?=($page1=='news' ? 'class="active "' : '')?>>
                        <a href="<?=site_url('superadmin/pages/news')?>">Berita</a>
                    </li>
                    <li <?=($page1=='gallery' ? 'class="active "' : '')?>>
                        <a href="<?=site_url('superadmin/pages/gallery')?>">Galeri Foto</a>
                    </li>
                    <li <?=($page1=='regulatory_training' ? 'class="active "' : '')?>>
                        <a href="<?=site_url('superadmin/pages/regulatory_training')?>">Peraturan Pelatihan</a>
                    </li>
                    <li <?=($page1=='faq' ? 'class="active "' : '')?>>
                        <a href="<?=site_url('superadmin/pages/faq')?>">FAQ</a>
                    </li>
                    <li <?=($page1=='contact' ? 'class="active "' : '')?>>
                        <a href="<?=site_url('superadmin/pages/contact')?>">Kontak</a>
                    </li>
                    <li <?=(in_array($page1, array('how_to_submission_accreditation','how_to_submission_training_facilitator','how_participant_registration')) ? 'class="active "' : '')?>>
                        <a href="javascript:;">
			    
			    <span class="title">Cara Penggunaan</span>
			    <span class="arrow "></span>
			</a>
			<ul class="sub-menu">
			    <li <?=($page1=='how_to_submission_accreditation' ? 'class="active "' : '')?>>
				<a href="<?=site_url('superadmin/pages/how_to_submission_accreditation')?>">Pengajuan Akreditasi LPP</a>
			    </li>

			    <li <?=($page1=='how_to_submission_training_facilitator' ? 'class="active "' : '')?>>
				<a href="<?=site_url('superadmin/pages/how_to_submission_training_facilitator')?>">Pengajuan Fasilitasi Pelatihan dan Narasumber</a>
			    </li>

			    <li <?=($page1=='how_participant_registration' ? 'class="active "' : '')?>>
				<a href="<?=site_url('superadmin/pages/how_participant_registration')?>">Pendaftaran Peserta Pelatihan</a>
			    </li>

			</ul>
                    </li>
                    <li <?=($page1=='about' ? 'class="active "' : '')?>>
                        <a href="<?=site_url('superadmin/pages/about')?>">Tentang Kami</a>
                    </li>
                    
                    <!--<li <?=($page1=='terms_of_service' ? 'class="active "' : '')?>>
                        <a href="<?=site_url('superadmin/pages/terms_of_service')?>">Persyaratan Layanan (Terms of Service)</a>
                    </li>-->
                    
                </ul>
            </li>
            
            <li <?=($page=='setting' ? 'class="open active "' : '')?>>
                <a href="javascript:;">
                    <i class="icon-settings"></i>
                    <span class="title">Pengaturan</span>
                    <span class="arrow "></span>
                </a>
                <ul class="sub-menu">
		    <li <?=($page1=='config_training' ? 'class="active "' : '')?>>
                        <a href="<?=site_url('superadmin/setting/config_training')?>">Nama Pelatihan</a>
                    </li>
                    
                    <li <?=($page1=='type_training' ? 'class="active "' : '')?>>
                        <a href="<?=site_url('superadmin/setting/type_training')?>">Jenis Pelatihan</a>
                    </li>
                    
                    <li <?=($page1=='set_default_hour_leasson_instructor' ? 'class="active "' : '')?>>
                        <a href="<?=site_url('superadmin/setting/set_default_hour_leasson_instructor')?>">Set Default JP Pengajar</a>
                    </li>
                    
                    <li <?=($page1=='todolist' ? 'class="active "' : '')?>>
                        <a href="<?=site_url('superadmin/setting/todolist')?>">Todolist</a>
                    </li>
                    
                    <li <?=($page1=='max_bussinesday_submission_training' ? 'class="active "' : '')?>>
                        <a href="<?=site_url('superadmin/setting/max_bussinesday_submission_training')?>">Set Maksimal Hari Pengajuan Pelatihan</a>
                    </li>
					
					<li <?=($page1=='limit_submission_training' ? 'class="active "' : '')?>>
                        <a href="<?=site_url('superadmin/setting/limit_submission_training')?>">Set Limit Pengajuan Pelatihan oleh LPP</a>
                    </li>
					
					<li <?=($page1=='example_document_registration' ? 'class="active "' : '')?>>
                        <a href="<?=site_url('superadmin/setting/example_document_registration')?>">Contoh Dokumen Registrasi</a>
                    </li>
                    
                </ul>
            </li>
            <!-- <li>
                <a href="index.html">
                    <i class="icon-home"></i>
                    <span class="title">Folder</span>
                </a>

            </li> -->

        </ul>
        <!-- END SIDEBAR MENU -->
    </div>
</div>