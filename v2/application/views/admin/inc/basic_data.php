
<div class="form-group">
	<label class="control-label col-md-3">Foto</label>
	<div class="col-md-9">
		<div class="fileinput fileinput-new" data-provides="fileinput">
			<div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
				<img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=Pilih Foto" alt=""/>
			</div>
			
		</div>
	</div>
</div>
<div class="form-group">
	<label class="control-label col-md-3">Username <span class="required">
	* </span>
	</label>
	<div class="col-md-4">
		<input type="text" class="form-control" name="username" placeholder="cahyogumilang"/>
		<span class="help-block">
		Isi username </span>
	</div>
</div>
<div class="form-group">
	<label class="control-label col-md-3">Password <span class="required">
	* </span>
	</label>
	<div class="col-md-4">
		<input type="password" class="form-control" name="password" id="submit_form_password"/>
		<span class="help-block">
		Isi kata sandi </span>
	</div>
</div>
<div class="form-group">
	<label class="control-label col-md-3">Konfirmasi Password <span class="required">
	* </span>
	</label>
	<div class="col-md-4">
		<input type="password" class="form-control" name="rpassword"/>
		<span class="help-block">
		Konfirmasi kata sandi </span>
	</div>
</div>
<div class="form-group">
	<label class="control-label col-md-3">Email <span class="required">
	* </span>
	</label>
	<div class="col-md-4">
		<input type="text" class="form-control" name="email" placeholder="contoh: cahyo@gmail.com" id="submit_form_email"/>
		<span class="help-block">
		Isi email </span>
	</div>
</div>
<div class="form-group">
	<label class="control-label col-md-3">Konfirmasi Email <span class="required">
	* </span>
	</label>
	<div class="col-md-4">
		<input type="text" class="form-control" name="remail"/>
		<span class="help-block">
		Konfirmasi email </span>
	</div>
</div>
<div class="form-group">
	<label class="control-label col-md-3">Nama
		<span class="required">*</span>
	</label>
	<div class="col-md-4">
		<input type="text" class="form-control" name="fullname" placeholder="contoh: Cahyo Gumilang"/>
		<span class="help-block">Isi nama lengkap</span>
	</div>
</div>
<div class="form-group">
	<label class="control-label col-md-3">Tempat Lahir
		 <span class="required">*</span>
	</label>
	<div class="col-md-4">
		<input type="text" class="form-control" name="birth_place" id="submit_tempat_lahir" placeholder="contoh: Blitar"/>
		<span class="help-block">
		Isi Tempat Lahir </span>
	</div>
</div>
<div class="form-group">
	<label class="control-label col-md-3">Tanggal Lahir
		<span class="required">*</span>
	</label>
	<div class="col-md-3">
		<div class="input-group input-medium date date-picker tanggal-wizard" data-date-format="dd-mm-yyyy">
			<input type="text" class="form-control" name="birth_date" id="birth_date" placeholder="contoh: 12-12-1945">
			<span class="input-group-btn">
			<button class="btn default" type="button">
				<i class="fa fa-calendar"></i></button>
			</span>
		</div>
		<div id="form_tgl_lahir_error">
		</div>
		<span class="help-block">
		Pilih Tanggal Lahir </span>
	</div>
</div>
<div class="form-group">
	<label class="control-label col-md-3">No. Telp/HP
		<span class="required">*</span>
	</label>
	<div class="col-md-4">
		<input type="text" class="form-control" name="phone" placeholder="contoh: 081279221166"/>
		<span class="help-block">
		Isi no.Telp </span>
	</div>
</div>
<div class="form-group">
	<label class="control-label col-md-3">Jenis Kelamin <span class="required">
	* </span>
	</label>
	<div class="col-md-4">
		<div class="radio-list">
			<label>
			<input type="radio" name="gender" value="M" data-title="Laki-laki"/>
			Laki-laki </label>
			<label>
			<input type="radio" name="gender" value="F" data-title="Perempuan"/>
			Perempuan </label>
		</div>
		<div id="form_gender_error">
		</div>
	</div>
		
</div>
<!-- <div class="form-group">
	<label class="control-label col-md-3">Alamat
		<span class="required">*</span>
	</label>
	<div class="col-md-4">
		<input type="text" class="form-control" name="address" placeholder="contoh: jl. Ahmad yani no 3"/>
		<span class="help-block">
		Isi alamat lengkap </span>
	</div>
</div>
<div class="form-group">
	<label class="control-label col-md-3">Provinsi <span class="required">
	* </span>
	</label>
	<div class="col-md-4">
		<select class="form-control select2me select-province" name="province" id="user_provinsi_list">
			<option value=""></option>
		</select>
	</div>
</div>
<div class="form-group">
	<label class="control-label col-md-3">Kab/Kota <span class="required">
	* </span>
	</label>
	<div class="col-md-4">
		<select class="form-control select2me" name="district" id="user_kabupaten_list">
			<option value=""></option>
		</select>
	</div>
</div>
<div class="form-group">
	<label class="control-label col-md-3">Kecamatan
	</label>
	<div class="col-md-4">
		<select class="form-control select2me" name="subdistrict" id="user_kecamatan_list">
			<option value=""></option>
		</select>
	</div>
</div> -->