<?php
    $value = ( isset($value) ? $value : '');
?>
<div class="form-group">
    <label class="col-sm-2 control-label">Nama Tugas</label>
    <div class="col-sm-9">
	<input type="text" class="form-control" placeholder="Isi tugas" value='<?=$value?>' name="tasklist[]" data-required="1">
    </div>
    <div class="col-sm-1">
	<button type='button' class='btn btn-sm red btn-block remove_todolist'>x</button>
    </div>
    
</div>
 
