<?php
	/**
	 * GIT DEPLOYMENT SCRIPT
	 *
	 * Used for automatically deploying websites via github or bitbucket, more deets here:
	 *
	 *		https://gist.github.com/1809044
	 */

	// The commands
	$commands = array(
		'echo $PWD',
		'whoami',
		'git fetch --all',
		'git reset --hard origin/devel',
		'git pull origin devel',
		'git status'
	);

	// Run the commands for output
	$output = '';
	foreach($commands AS $command){
		// Run it
		$tmp = shell_exec($command);
		// Output
		$output .= "<span style=\"color: #6BE234;\">\$</span> <span style=\"color: #729FCF;\">{$command}\n</span>";
		$output .= htmlentities(trim($tmp)) . "\n";
	}

	// Make it pretty for manual user access (and why not?)
?>
<!DOCTYPE HTML>
<html lang="en-US">
<head>
	<meta charset="UTF-8">
	<title>GIT DEPLOYMENT SCRIPT</title>
</head>
<body style="background-color: #000000; color: #FFFFFF; font-weight: bold; padding: 0 10px;">
<pre>
<small>       _       _     _                          _ _                 
 _ __ (_) __ _| |__ | |_ ___ _ __ __ ___      _| (_)_ __   __ _ ____
| '_ \| |/ _` | '_ \| __/ __| '__/ _` \ \ /\ / / | | '_ \ / _` |_  /
| | | | | (_| | | | | || (__| | | (_| |\ V  V /| | | | | | (_| |/ / 
|_| |_|_|\__, |_| |_|\__\___|_|  \__,_| \_/\_/ |_|_|_| |_|\__, /___|
         |___/                                            |___/     
</small>

<?php echo $output; ?>
</pre>
</body>
</html> 
