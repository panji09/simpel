<?php
require_once __DIR__ . '/../vendor/autoload.php';
use PhpAmqpLib\Connection\AMQPConnection;

include(__DIR__ . '/../application/config/rabbitmq.php');

$connection = new AMQPConnection($config['amq_host'], $config['amq_port'], $config['amq_username'], $config['amq_password']);
$channel = $connection->channel();

$channel->queue_declare('task_send_email', false, false, false, false);

$callback = function($msg){
  echo " [x] Received ", $msg->body, "\n";
  $data = json_decode($msg->body, true);
//   print_r($data);
  
  echo " [x] Send Email\n";

  include( __DIR__ . '/../application/config/daemon.php');
  $url = $config['connect_url'].'/api/send_email/index';
  echo " [x] Send API ".$url."\n";
  $curl = curl_init();

  curl_setopt_array($curl, array(
      CURLOPT_URL => $url,
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 400,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "POST",
      CURLOPT_POSTFIELDS => json_encode($data),
      CURLOPT_HTTPHEADER => array(
	"content-type: application/json"
      ),
  ));

  $response = curl_exec($curl);
  $httpcode = curl_getinfo($curl);
  $err = curl_error($curl);
  
  if(!$err){
    print_r(array('header_info' => $httpcode, 'body' => $response));
    echo " [x] All Done", "\n";
    $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
  }else{
    echo $err;
  }
  
};

$channel->basic_qos(null, 1, null);
$channel->basic_consume('task_send_email', '', false, false, false, false, $callback);

while(count($channel->callbacks)) {
    $channel->wait();
}
?> 
