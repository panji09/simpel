<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH.'/libraries/REST_Controller.php';

class Email extends REST_Controller{
    function index_post(){
	
	$input = json_decode(json_encode($this->post()), true);
	
	$to = $input['to'];
	$subject = $input['subject'];
	$message = $input['message'];
	
	//save token success
	$this->email->from('noreply@ruangpanji.com','SIMPEL');
	$this->email->to($to);
	$this->email->subject($subject);
	
	$this->email->message($message);
	
	if($this->email->send()){
	    //ok
	    $this->response(array(
		'status'=>'OK'
	    ),200);
	}else{
	    //internal server error
	    $this->response(array(
		'error'=>'send_fail',
		'error_description' => 'failed to send mail'
	    ),500);
	}
    }
    
    function queuemail_post(){
	$Task = new Task();
	
	$input = json_decode(json_encode($this->post()), true);
	
	$to = $input['to'];
	$subject = $input['subject'];
	$message = $input['message'];
	
	$Task->send_email($to, $subject, $message);
    }
}
