<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Example
 *
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array.
 *
 * @package		CodeIgniter
 * @subpackage	Rest Server
 * @category	Controller
 * @author		Phil Sturgeon
 * @link		http://philsturgeon.co.uk/code/
*/

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require_once APPPATH.'/libraries/REST_Controller.php';

class User extends REST_Controller{
    
    function __construct(){
	// Construct our parent class
	parent::__construct();

    }
    
    /*
    register index
    - superadmin
    - admin
    - fasilitator
    - narasumber
    - peserta
    */
    
    /*
    update user
    */
    
    function me_get($token=null){
	
	if(!$token){
	    $this->response(
		array(
		    'error'=> 'missing_parameter',
		    'error_description' => 'missing parameter token'
		)
	    ,400);
	}
	
	$api_url = 'http://simpel.ruangpanji.com/v2/connect/oauth/check_token?access_token='.$token;
	$response = call_api_get($api_url);
	
	if($response['header_info']['http_code'] == 200){
	    
	    $response_body = json_decode($response['body'], true);
	    $user_id = $response_body['user_id'];
	    $query = $this->user_db->get($user_id);
	    if(!$query){
		$this->response(
		    array(
			'error'=> 'empty_user',
			'error_description' => 'failed to retrieve user'
		    )
		,204); //no content
	    }
	    $user = $query[0];
	    unset($user['_id']);
	    unset($user['log_last_login']);
	    unset($user['log_last_update']);
	    
	    $user['user_id'] = $user_id;
	    $this->response(
		$user, 
		200
	    );
	    
	    
	}else{
	    $this->response(
		json_decode($response['body'], true),
		$response['header_info']['http_code']
	    );
	}
    }
    
    function me_put($token=null){
	
	if(!$token){
	    $this->response(
		array(
		    'error'=> 'missing_parameter',
		    'error_description' => 'missing parameter token'
		)
	    ,400);
	}
	
	$api_url = 'http://simpel.ruangpanji.com/v2/connect/oauth/check_token?access_token='.$token;
	$response = call_api_get($api_url);
	
	if($response['header_info']['http_code'] == 200){
	    
	    $response_body = json_decode($response['body'], true);
	    $user_id = $response_body['user_id'];
	    
	    $input = json_decode(json_encode($this->put()), true);
	    $data_input = $input;
	    
	    $query = $this->user_db->get($user_id);
	    if(!$query){
		$this->response(
		    array(
			'error'=> 'empty_user',
			'error_description' => 'failed to retrieve user'
		    )
		,204); //no content
	    }
	    
	    $user = $query[0];
	    
	    if(isset($input['username']) && $user['username'] != $input['username']){
		if($this->user_db->user_exist($input['username'])){
		    
		    $this->response(
			array(
			    'error'=> 'username_exist',
			    'error_description' => 'username exist'
			)
		    ,403); //forbiden
		}
	    }
	    
	    if(isset($input['email']) && $user['email'] != $input['email']){
		if($this->user_db->email_exist($input['email'])){
		    $this->response(
			array(
			    'error'=> 'email_exist',
			    'error_description' => 'email exist'
			)
		    ,403); //forbiden
		}
		
		$data_input['verified_email'] = 0;
		$data_input['verified_email_code'] = sha1_salt(time());
	    }
	    
	    if(isset($input['password']))
		$data_input['password'] = sha1_salt($input['password']);
	    
	    if($callback = $this->user_db->save($user_id, $data_input)){
		$this->response(
		    array(
			'status' => 'OK',
			'user_id' => $user_id,
			'role' => $user['role']
		    ), 
		    200
		);
	    }else{
		$this->response(
		array(
		    'error'=> 'update_failed',
		    'error_description' => 'unable to save data'
		), 500);
	    }
	
	}else{
	    $this->response(
		json_decode($response['body'], true),
		$response['header_info']['http_code']
	    );
	}
    }
    function index_put($user_id=null){
	$input = json_decode(json_encode($this->put()), true);
	$data_input = $input;
	
	if(!$user_id){
	    $this->response(
		array(
		    'error'=> 'missing_parameter',
		    'error_description' => 'missing parameter entity'
		)
	    ,400);
	}
	
	$query = $this->user_db->get($user_id);
	if(!$query){
	    $this->response(
		array(
		    'error'=> 'empty_user',
		    'error_description' => 'failed to retrieve user'
		)
	    ,204); //no content
	}
	
	$user = $query[0];
	
	if(isset($input['username']) && $user['username'] != $input['username']){
	    if($this->user_db->user_exist($input['username'])){
		
		$this->response(
		    array(
			'error'=> 'username_exist',
			'error_description' => 'username exist'
		    )
		,403); //forbiden
	    }
	}
	
	if(isset($input['email']) && $user['email'] != $input['email']){
	    if($this->user_db->email_exist($input['email'])){
		$this->response(
		    array(
			'error'=> 'email_exist',
			'error_description' => 'email exist'
		    )
		,403); //forbiden
	    }
	    
	    $data_input['verified_email'] = 0;
	    $data_input['verified_email_code'] = sha1_salt(time());
	}
	
	if(isset($input['password']))
	    $data_input['password'] = sha1_salt($input['password']);
	
	if($callback = $this->user_db->save($user_id, $data_input)){
	    $this->response(
		array(
		    'status' => 'OK',
		    'user_id' => $user_id,
		    'role' => $user['role']
		), 
		200
	    );
	}else{
	    $this->response(
	    array(
		'error'=> 'update_failed',
		'error_description' => 'unable to save data'
	    ), 500);
	}
    }
    
    function index_get($user_id=null){
	if(!$user_id){
	    $this->response(
		array(
		    'error'=> 'missing_parameter',
		    'error_description' => 'missing parameter entity'
		)
	    ,400);
	}
	
	$query = $this->user_db->get($user_id);
	if(!$query){
	    $this->response(
		array(
		    'error'=> 'empty_user',
		    'error_description' => 'failed to retrieve user'
		)
	    ,204); //no content
	}
	$user = $query[0];
	unset($user['_id']);
	unset($user['log_last_login']);
	unset($user['log_last_update']);
	
	$user['user_id'] = $user_id;
	$this->response(
	    $user, 
	    200
	);
    }
    
}