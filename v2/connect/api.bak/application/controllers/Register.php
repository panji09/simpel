<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Example
 *
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array.
 *
 * @package		CodeIgniter
 * @subpackage	Rest Server
 * @category	Controller
 * @author		Phil Sturgeon
 * @link		http://philsturgeon.co.uk/code/
*/

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require_once APPPATH.'/libraries/REST_Controller.php';

class Register extends REST_Controller{
    
    function __construct(){
	// Construct our parent class
	parent::__construct();
	
	// Configure limits on our controller methods. Ensure
	// you have created the 'limits' table and enabled 'limits'
	// within application/config/rest.php
	
    //         $this->methods['user_get']['limit'] = 500; //500 requests per hour per user/key
    //         $this->methods['user_post']['limit'] = 100; //100 requests per hour per user/key
    //         $this->methods['user_delete']['limit'] = 50; //50 requests per hour per user/key
    }
    
    /*
    register index
    - superadmin
    - admin
    - fasilitator
    - narasumber
    - peserta
    */
    
    function index_post($entity=null){
	$input = json_decode(json_encode($this->post()), true);
// 	print_r($input);
	if(!$entity){
	    $this->response(
		array(
		    'error'=> 'missing_parameter',
		    'error_description' => 'missing parameter entity'
		)
	    ,400);
	}
	
	//check field
	$array_field = array('username', 'password', 'email');
	if(!check_request($input, $array_field)){
	    $this->response(
		array(
		    'error'=> 'invalid_field',
		    'error_description' => 'missing field [username, password, email]'
		)
	    ,400); //bad request
	}
	
	if(filter_var($input['username'], FILTER_VALIDATE_EMAIL))
	    $query = $this->user_db->get_all($filter = array('email' => $input['username']));
	else
	    $query = $this->user_db->get_all($filter = array('username' => $input['username']));
	
	
	if(count($query)==1){
	    $this->response(
		array(
		    'error'=> 'username_exist',
		    'error_description' => 'username exist'
		)
	    ,403); //forbiden
	}else{
	    $data_input = $input;
	    
	    $data_input['password'] = sha1_salt($input['password']);
	    
	    $data_input['entity'] = $entity;
	    $data_input['role'] = array(strtolower($entity));
	    $data_input['verified_email'] = 0;
	    $data_input['verified_email_code'] = sha1_salt(time());
	    
	    $data_input['deleted'] = 0;
	    $data_input['activated'] = 1;
	    
	    if($callback = $this->user_db->save(null, $data_input)){
		$this->response(
		    array(
			'status' => 'OK',
			'user_id' => $callback->{'$id'},
			'role' => $data_input['role']
		    ), 
		    200
		);
	    }else{
		$this->response(
		array(
		    'error'=> 'register_failed',
		    'error_description' => 'unable to save data'
		), 500);
	    }
	    
	}
    }
    
    function check_username_post(){
	
	$input = json_decode(json_encode($this->post()), true);
	//check field
	$array_field = array('username');
	if(!check_request($input, $array_field)){
	    $this->response(
		array(
		    'error'=> 'invalid_field',
		    'error_description' => 'invalid field, field allowed [username]'
		)
	    ,400);
	}
	
	if(filter_var($input['username'], FILTER_VALIDATE_EMAIL))
	    $query=$this->user_db->get_all($filter = array('email' => $input['username']));
	else
	    $query=$this->user_db->get_all($filter = array('username' => $input['username']));
	
	
	if(count($query)==1)
	    $this->response(
		array(
		    'error'=> 'username_exist',
		    'error_description' => 'username exist'
		)
	    ,404);
	else
	    $this->response(array('status' => 'OK'), 200);
    }
    
    
    
}