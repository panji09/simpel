<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Example
 *
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array.
 *
 * @package		CodeIgniter
 * @subpackage	Rest Server
 * @category	Controller
 * @author		Phil Sturgeon
 * @link		http://philsturgeon.co.uk/code/
*/

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require_once APPPATH.'/libraries/REST_Controller.php';

class Institution extends REST_Controller{
    function __construct(){
	// Construct our parent class
	parent::__construct();
    }
    
    function index_get(){
	$institution = $this->init_config_db->get_institution();
	
	$province = $this->location_db->get_province();
	$province_name = array();
	foreach($province as $row){
	    $province_name[] = 'Provinsi '.trim($row['name']);
	}
	
	$district = $this->location_db->get_district();
	$district_name = array();
	foreach($district as $row){
	    $district_name[] = trim($row['name']);
	}
	
	$output = array_merge($institution, $province_name);
	$output = array_merge($output, $district_name);
	
	$this->response($output, 200);
    }
}