<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Example
 *
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array.
 *
 * @package		CodeIgniter
 * @subpackage	Rest Server
 * @category	Controller
 * @author		Phil Sturgeon
 * @link		http://philsturgeon.co.uk/code/
*/

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require_once APPPATH.'/libraries/REST_Controller.php';

class Location extends REST_Controller{
    function __construct(){
	// Construct our parent class
	parent::__construct();
    }
    
    function province_get(){
	$output = $this->location_db->get_province();
	
	$this->response($output, 200);
    }
    
    function district_get($province_id=null){
	$district = $this->location_db->get_district();
	
	$output = array();
	foreach($district as $row){
	    if($row['provinsi_id'] == $province_id){
		$output[] = $row;
	    }
	}
	
	$this->response($output, 200);
    }
    
    function subdistrict_get($district_id=null){
	$subdistrict = $this->location_db->get_subdistrict();
	
	$output = array();
	foreach($subdistrict as $row){
	    if($row['kabkota_id'] == $district_id){
		$output[] = $row;
	    }
	}
	
	$this->response($output, 200);
    }
}