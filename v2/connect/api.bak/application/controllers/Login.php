<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Example
 *
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array.
 *
 * @package		CodeIgniter
 * @subpackage	Rest Server
 * @category	Controller
 * @author		Phil Sturgeon
 * @link		http://philsturgeon.co.uk/code/
*/

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require_once APPPATH.'/libraries/REST_Controller.php';

class Login extends REST_Controller{
    function __construct(){
	// Construct our parent class
	parent::__construct();
	
	// Configure limits on our controller methods. Ensure
	// you have created the 'limits' table and enabled 'limits'
	// within application/config/rest.php
	
    //         $this->methods['user_get']['limit'] = 500; //500 requests per hour per user/key
    //         $this->methods['user_post']['limit'] = 100; //100 requests per hour per user/key
    //         $this->methods['user_delete']['limit'] = 50; //50 requests per hour per user/key
    }
    
    function index_post(){
	
	$input = json_decode(json_encode($this->post()), true);
	
	//check field
	$array_field = array('username','password');
	if(!check_request($input, $array_field)){
	    $this->response(
	    array(
		'error'=> 'invalid_field',
		'error_description' => 'invalid field, field allowed [username, password]'
	    )
	    ,400);
	}
	
	$data_user =array(
	    'username' => $input['username'],
	    'password' => $input['password']
	);
	
	$output = array();
	if($callback = $this->login_db->login_user($data_user)){
	    //echo 'ok';
	    $output = $callback;
	}else{
	
	    $this->response(
	    array(
		'error'=> 'login_failed',
		'error_description' => 'username or password failed to login'
	    )
	    ,400);
	
	}
// 	print_r($this->mongoci->LastQuery());
	$this->response($output, 200);
    }
    
    function update_auth_code_put($authorization_code=null){
    
	$input = json_decode(json_encode($this->put()), true);
	//check field
	$array_field = array('user_id', 'scope');
	if(!check_request($input, $array_field)){
	    $this->response(
	    array(
		'error'=> 'invalid_field',
		'error_description' => 'invalid field, field allowed [user_id, scope]'
	    )
	    ,400);
	}
	
	//input
	$data_input = array(
	    'user_id' => $input['user_id'],
	    'scope' => $input['scope']
	);
	
	if($this->oauth_db->authorization_codes_save($authorization_code, $data_input)){
	    $this->response(array('status' => 'OK'), 200);
	}else{
	    $this->response(
	    array(
		'error'=> 'update_failed',
		'error_description' => 'failed to update authorization code'
	    )
	    ,500);
	}
    }
}