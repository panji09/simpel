<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Location_db extends CI_Model{
    function __construct(){
        // Call the Model constructor
        parent::__construct();
    }
    
    function get(){
	$query = $this->mongoci->get('location');
	return $query[0];
    }
    
    function get_province(){
	return $this->get()['provinsi'];
    }
    
    function get_district(){
	return $this->get()['kabkota'];
    }
    
    function get_subdistrict(){
	return $this->get()['kecamatan'];
    }
}
?>