<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Academic_db extends CI_Model{
    function __construct(){
        // Call the Model constructor
        parent::__construct();
    }
    
    function get(){
	$query = $this->mongoci->get('academic');
	return $query[0];
    }
    
    function get_academic_degree(){
	return $this->get()['academic_degree'];
    }
    
    function get_educational_level(){
	return $this->get()['educational_level'];
    }
}
?>