<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Init_config_db extends CI_Model{
    function __construct(){
        // Call the Model constructor
        parent::__construct();
    }
    
    function get(){
	$query = $this->mongoci->get('init_config');
	return $query[0];
    }
    
    function get_entity(){
	return $this->get()['entity'];
    }
    
    function get_institution(){
	return $this->get()['institution'];
    }
    
    function get_employment(){
	return $this->get()['employment'];
    }
    
    function get_educational_level(){
	return $this->get()['educational_level'];
    }
}
?>