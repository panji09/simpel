<?php
/**
 * Description of MY_Composer
 *
 * @author Rana
 */
class MY_Model
{
	private $_qb;
	
    function __construct() 
    {
         $this->qb = $this->defaultConnect(true);
    }
	
    function defaultConnect($connect = true)
    {
            return new \MongoQB\Builder(array(
                    'dsn'   =>      'mongodb://ttsindonesia.com:27017/tts',
                    'query_safety'  =>      null
            ), $connect);
    }
	
}