<!-- BEGIN PAGE CONTAINER -->
<div class="page-container">
    <!-- BEGIN PAGE CONTENT -->
    <div class="page-content">
        <div class="container">
           
            <!-- BEGIN PAGE CONTENT INNER -->
            <div class="row margin-top-10">
                <div class="col-md-12">
                    <!-- BEGIN PROFILE SIDEBAR -->
					<?php if(isset($sidebar)) $this->load->view('pages/dashboard/sidebar', $sidebar)?>
                    <!-- END BEGIN PROFILE SIDEBAR -->										
                    <!-- BEGIN PROFILE CONTENT -->
					<?php if(isset($content)) $this->load->view('pages/dashboard/content/'.$content);?>
                    <!-- END PROFILE CONTENT -->
                </div>
            </div>
            <!-- END PAGE CONTENT INNER -->
        </div>
    </div>
    <!-- END PAGE CONTENT -->
</div>

<?php if(isset($token)){?>
<script>
	var CODE = '<?=$token;?>';
	var ID = '<?=$user_id?>';		
</script>
<?php }?>	