<h4 class="form-section">Data Social Media</h4>
<div class="form-group">
    <label class="control-label col-md-3">Facebook:</label>
    <div class="col-md-9">
        <p class="form-control-static">
             <?php if(isset($user['link_fb'])) echo $user['link_fb'];?>
        </p>
    </div>
</div>
<div class="form-group">
    <label class="control-label col-md-3">Twitter:</label>
    <div class="col-md-9">
        <p class="form-control-static">
             <?php if(isset($user['link_twitter'])) echo $user['link_twitter'];?>
        </p>
    </div>
</div><div class="form-group">
    <label class="control-label col-md-3">Website:</label>
    <div class="col-md-9">
        <p class="form-control-static">
             <?php if(isset($user['link_web'])) echo $user['link_web'];?>
        </p>
    </div>
</div>