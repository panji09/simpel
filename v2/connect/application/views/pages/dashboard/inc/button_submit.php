<div class="form-actions">
	<div class="row">
		<div class="col-md-6">
			<div class="row">
				<div class="col-md-offset-3 col-md-9">
					<button type="submit" class="btn blue"><i class="fa fa-pencil"></i> Simpan</button>
					<?php if(isset($user['entity']) && $user['entity']=='lpp'):?>
					    <?php if(isset($user['verified_institute']) && $user['verified_institute'] == -1):?>
					    
					    <button type='button' data-url='<?=site_url('dashboard/verified_institute_post/'.$user['user_id'])?>' data-toggle="modal" data-target="#modal_verified_institute" class="btn red"><i class="fa fa-pencil"></i> Verifikasi</button>
					    
					    <?php endif;?>
					<?php endif;?>
					<button type="button" onclick="location.href='<?=site_url('dashboard/account_overview')?>'" class="btn default">Batal</button>
				</div>
			</div>
		</div>
		<div class="col-md-6">
		</div>
	</div>
</div>