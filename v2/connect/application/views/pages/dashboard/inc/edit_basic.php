														<?php $this->load->view('pages/dashboard/inc/alert_form')?>
														<h4 class="form-section">Data Pengguna</h4>
														
														
														<div class="form-group">
															<label class="control-label col-md-3">Username <span class="required">
															* </span>
															</label>
															<div class="col-md-4">
																<input type="text" class="form-control" name="username" placeholder="cahyogumilang" value="<?php if(isset($user['username'])) echo $user['username'];?>"/>
																<span class="help-block">
																Isi username </span>
															</div>
														</div>
														<div class="form-group">
															<label class="control-label col-md-3">Email <span class="required">
															* </span>
															</label>
															<div class="col-md-4">
																<input type="text" class="form-control" name="email" placeholder="contoh: cahyo@gmail.com" value="<?php if(isset($user['email'])) echo $user['email'];?>"/>
																<span class="help-block">
																Isi email </span>
															</div>
														</div>
														<div class="form-group">
															<label class="control-label col-md-3">Nama
																<span class="required">*</span>
															</label>
															<div class="col-md-4">
																<input type="text" class="form-control" name="fullname" placeholder="contoh: Cahyo Gumilang" value="<?php if(isset($user['fullname'])) echo $user['fullname'];?>"/>
																<span class="help-block">Isi nama lengkap</span>
															</div>
														</div>
														<div class="form-group">
															<label class="control-label col-md-3">Tempat Lahir
																 <span class="required">*</span>
															</label>
															<div class="col-md-4">
																<input type="text" class="form-control" name="birth_place" id="birth_place" placeholder="contoh: Blitar" value="<?php if(isset($user['birth_place'])) echo $user['birth_place'];?>"/>
																<span class="help-block">
																Isi Tempat Lahir </span>
															</div>
														</div>
														<div class="form-group">
															<label class="control-label col-md-3">Tanggal Lahir
																<span class="required">*</span>
															</label>
															<div class="col-md-3">
																<div class="input-group input-medium date date-picker tanggal" data-date-format="yyyy-mm-dd">
																	<input id='birth_date' type="text" class="form-control" name="birth_date" placeholder="contoh: 12-12-1945" value="<?php if(isset($user['birth_date'])) echo $user['birth_date'];?>">
																	<span class="input-group-btn">
																	<button class="btn default" type="button">
																		<i class="fa fa-calendar"></i></button>
																	</span>
																</div>
																<div id="form_tgl_lahir_error">
																</div>
																<span class="help-block">
																Pilih Tanggal Lahir </span>
															</div>
														</div>
														<div class="form-group">
															<label class="control-label col-md-3">No. Telp/HP
																<span class="required">*</span>
															</label>
															<div class="col-md-4">
																<input id='phone' type="text" class="form-control" name="phone" placeholder="contoh: 081279221166" value="<?php if(isset($user['phone'])) echo $user['phone'];?>"/>
																<span class="help-block">
																Isi no.Telp </span>
															</div>
														</div>
														<div class="form-group">
															<label class="control-label col-md-3">Jenis Kelamin <span class="required">
															* </span>
															</label>
															<div class="col-md-4">
																<div class="radio-list">
																	<label>
																	<input type="radio" name="gender" value="M" data-title="Laki-laki" <?php if($user['gender'] == 'M'){ ?>checked="checked" <?php }?>/>
																	Laki-laki </label>
																	<label>
																	<input type="radio" name="gender" value="F" data-title="Perempuan" <?php if($user['gender'] == 'F'){ ?>checked="checked" <?php }?>/>
																	Perempuan </label>
																</div>
																<div id="form_gender_error">
																</div>
															</div>
														</div>