<!-- Modal -->
<div class="modal fade" id="modal_verified_institute" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Konfirmasi</h4>
      </div>
      <div class="modal-body">
        Mohon pastikan data anda sudah benar, lanjutkan verifikasi?
      </div>
      <div class="modal-footer">
        <a id='link_verified_institute' href='<?=site_url('')?>' class="btn btn-primary">Ya</a>
        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
        
      </div>
    </div>
  </div>
</div>