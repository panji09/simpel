<div class="alert alert-danger display-none">
															<button class="close" data-dismiss="alert"></button>
															Terjadi kesalahan, cek ulang data yang anda masukkan.
														</div>
														<div class="alert alert-success display-none">
															<button class="close" data-dismiss="alert"></button>
															Data yang anda masukkan sudah benar!
														</div>
														
														<?php if(isset($user['verified_institute']) && $user['verified_institute'] == -1):?>
														<div class="alert alert-warning">
															<button class="close" data-dismiss="alert"></button>
															<?=nl2br($user['review_note'])?>
														</div>
														<?php endif;?>