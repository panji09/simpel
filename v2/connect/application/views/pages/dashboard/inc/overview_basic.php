														<h4 class="form-section">Data Pengguna</h4>
														<div class="form-group">
															<label class="control-label col-md-3">Username:</label>
															<div class="col-md-4">
																<p class="form-control-static">
																	<?php if(isset($user['username'])) echo $user['username'];?>
																</p>
															</div>
														</div>
														<div class="form-group">
															<label class="control-label col-md-3">Email:</label>
															<div class="col-md-4">
																<p class="form-control-static">
																	<?php if(isset($user['email'])) echo $user['email'];?>
																</p>
															</div>
														</div>
														<div class="form-group">
															<label class="control-label col-md-3">Nama:</label>
															<div class="col-md-4">
																<p class="form-control-static">
																	<?php if(isset($user['fullname'])) echo $user['fullname'];?>
																</p>
															</div>
														</div>
														<div class="form-group">
															<label class="control-label col-md-3">Tempat Lahir:</label>
															<div class="col-md-4">
																<p class="form-control-static">
																	<?php if(isset($user['birth_place'])) echo $user['birth_place'];?>
																</p>
															</div>
														</div>
														<div class="form-group">
															<label class="control-label col-md-3">Tanggal Lahir:</label>
															<div class="col-md-4">
																<p class="form-control-static">
																	<?php if(isset($user['birth_date'])) echo $user['birth_date'];?>
																</p>
															</div>
														</div>
														<div class="form-group">
															<label class="control-label col-md-3">No. Telp/HP:</label>
															<div class="col-md-4">
																<p class="form-control-static">
																	<?php if(isset($user['phone'])) echo $user['phone'];?>
																</p>
															</div>
														</div>
														<div class="form-group">
															<label class="control-label col-md-3">Jenis Kelamin:</label>
															<div class="col-md-4">
																<p class="form-control-static">
																	<?php if(isset($user['gender'])) {
																		if($user['gender'] == 'M'){
																			echo 'Laki-laki';
																		}else{
																			echo 'Perempuan';
																		}														
																	}?>
																</p>
															</div>
														</div>