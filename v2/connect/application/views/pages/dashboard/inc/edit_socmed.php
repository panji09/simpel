<div class="form-group">
    <label class="control-label col-md-3">Facebook</label>
    
    <div class="col-md-4">
        <input type="text" class="form-control" name="link_fb" placeholder="" value="<?php if(isset($user['link_fb'])) echo $user['link_fb'];?>"/>
        <span class="help-block">
        Isi link facebook</span>
    </div>
</div>

<div class="form-group">
    <label class="control-label col-md-3">Twitter</label>
    
    <div class="col-md-4">
        <input type="text" class="form-control" name="link_twitter" placeholder="" value="<?php if(isset($user['link_twitter'])) echo $user['link_twitter'];?>"/>
        <span class="help-block">
        Isi link twitter</span>
    </div>
</div>

<div class="form-group">
    <label class="control-label col-md-3">Website</label>
    
    <div class="col-md-4">
        <input type="text" class="form-control" name="link_web" placeholder="" value="<?php if(isset($user['link_web'])) echo $user['link_web'];?>"/>
        <span class="help-block">
        Isi link website</span>
    </div>
</div>