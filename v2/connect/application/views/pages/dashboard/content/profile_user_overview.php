                    <div class="profile-content">
                        <div class="row">
							<div class="col-md-12">
								<div class="portlet light">
									<div class="portlet-title tabbable-line">
										<div class="caption caption-md">
											<i class="icon-globe theme-font hide"></i>
											<span class="caption-subject font-green-sharp bold uppercase">Profil Akun</span>
										</div>
										<ul class="nav nav-tabs">
											<li class="active">
												<a href="#tab_1_1" data-toggle="tab">Akun Info</a>
											</li>
											<li>
												<a href="#tab_1_2" data-toggle="tab">Profil User</a>
											</li>
										</ul>
									</div>
									<div class="portlet-body">
										<div class="tab-content">
											<!-- PERSONAL INFO TAB -->
											<div class="tab-pane form active" id="tab_1_1">
												<div class="form-horizontal registrasi-lkpp">
													<div class="form-body">
														<?php $this->load->view('pages/dashboard/inc/overview_basic')?>										
													</div>
													<div class="form-actions">
														<div class="row">
															<div class="col-md-6">
																<div class="row">
																	<div class="col-md-offset-3 col-md-9">
																		<a href='<?=site_url('dashboard/account_settings')?>' class="btn green"><i class="fa fa-pencil"></i> Edit</a>
																	</div>
																</div>
															</div>
															<div class="col-md-6">
															</div>
														</div>
													</div>
												</div>
												
											</div>
											<!-- END PERSONAL INFO TAB -->
											<div class="tab-pane form" id="tab_1_2">
												<!-- BEGIN FORM-->
												<div class="form-horizontal">
													<div class="form-body">
														
														<?php $this->load->view('pages/dashboard/inc/overview_socmed')?>
													</div>
													
													<div class="form-actions">
														<div class="row">
															<div class="col-md-6">
																<div class="row">
																	<div class="col-md-offset-3 col-md-9">
																		<a href='<?=site_url('dashboard/account_settings')?>' class="btn green"><i class="fa fa-pencil"></i> Edit</a>
																		
																	</div>
																</div>
															</div>
															<div class="col-md-6">
															</div>
														</div>
													</div>
													
												</div>
												<!-- END FORM-->
											</div>
											<!-- END ROFILE DETAIL TAB -->
										</div>
									</div>
								</div>
							</div>
                        </div>
                    </div>