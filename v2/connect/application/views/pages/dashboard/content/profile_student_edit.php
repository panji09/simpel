                    
                    <div class="profile-content">
                        <div class="row">
							<div class="col-md-12">
								<div class="portlet light">
									<div class="portlet-title tabbable-line">
										<div class="caption caption-md">
											<i class="icon-globe theme-font hide"></i>
											<span class="caption-subject font-green-sharp bold uppercase">Profil Akun</span>
										</div>
										<ul class="nav nav-tabs">
											<li class="active">
												<a href="#tab_1_1" data-toggle="tab">Akun Info</a>
											</li>
											<li>
												<a href="#tab_1_2" data-toggle="tab">Detil Profil</a>
											</li>
											<li>
												<a href="#tab_1_3" data-toggle="tab">Ganti Foto Profil</a>
											</li>
											<li>
												<a href="#tab_1_4" data-toggle="tab">Ganti Password</a>
											</li>
											
											<li>
												<a href="#tab_1_6" data-toggle="tab">Social Media</a>
											</li>
										</ul>
									</div>
									<div class="portlet-body">
										<div class="tab-content">
											<!-- PERSONAL INFO TAB -->
											<div class="tab-pane form active" id="tab_1_1">
												
												<form action="javascript:;" id="form_user_basic" class="form-horizontal">
													<div class="form-body">																											
															<?php $this->load->view('pages/dashboard/inc/edit_basic')?>
													</div>
													
													<?php $this->load->view('pages/dashboard/inc/button_submit');?>
												</form>
											</div>
											<!-- END PERSONAL INFO TAB -->
											<!-- DETAIL PROFILE TAB -->
											<div class="tab-pane form" id="tab_1_2">
												<form action="javascript:;" class="form-horizontal" id="form_user_detail">
													<div class="form-body">
														<?php $this->load->view('pages/dashboard/inc/alert_form')?>
														<h4 class="form-section">Data Pendidikan dan Kepegawaian</h4>
														
														<div class="form-group">
															<label class="control-label col-md-3">Pendidikan Terakhir<span class="required">* </span></label>
															
															<div class="col-md-4">
																<select class="form-control select2me" name="last_education" id="pendidikan_list">
																	<?php foreach($user_last_educations as $row): ?>
																	    <?php echo $row."\n"?>
																	<?php endforeach;?>
																</select>
																<span class="help-block">
																Isi pendidikan terakhir </span>
															</div>
														</div>
														<div class="form-group">
															<label class="control-label col-md-3">Gelar Akademis<span class="required">* </span></label>
															
															<div class="col-md-4">
																<input type="text" class="form-control" name="academic_degree" id="academic_degree" value="<?php echo $user['academic_degree'];?>" />
																<span class="help-block">
																Isi gelar akademis </span>
															</div>
														</div>
														<div class="form-group">
															<label class="control-label col-md-3">Status Kepegawaian<span class="required">* </span>
															</label>
															
															<div class="col-md-4">
																<select class="form-control select2me" name="employment" id="status_kepegawaian_list">
																	<?php foreach($user_employements as $emp){
																		echo $emp;
																	}?>
																</select>
																<span class="help-block">
																Isi status kepegawaian </span>
															</div>
														</div>
														
														<div class="form-group pns">
															<label class="control-label col-md-3">NIP/NRP<span class="required">* </span></label>
															
															<div class="col-md-4">
																<input type="text" class="form-control" name="nip_id" placeholder="contoh: 197604012001121001" value="<?php if(isset($user['nip_id'])) echo $user['nip_id'];?>"/>
																<span class="help-block">
																Jika anda PNS, isi NIP</span>
															</div>
														</div>
														
														<div class="form-group">
															<label class="control-label col-md-3">No.KTP<span class="required">* </span></label>
															
															<div class="col-md-4">
																<input type="text" class="form-control" name="ktp_id" placeholder="contoh: 1215125451123456" value="<?php if(isset($user['ktp_id'])) echo $user['ktp_id'];?>"/>
																<span class="help-block">
																Isi No KTP</span>
															</div>
														</div>
														
														<h4 class="form-section">Data Kantor</h4>
														<div class="form-group">
															<label class="control-label col-md-3">Nama Instansi/Institusi/Perusahaan<span class="required">* </span></label>
															
															<div class="col-md-4">
																<input id='institution' type="text" class="form-control" name="institution" placeholder="contoh: Kementerian Agama" value="<?php if(isset($user['institution'])) echo $user['institution'];?>"/>
																<span class="help-block">
																Isi nama Instansi/Institusi/Perusahaan Anda berasal</span>
															</div>
														</div>
														<div class="form-group">
															<label class="control-label col-md-3">Provinsi<span class="required">* </span></label>
															
															<div class="col-md-4">
																<select class="form-control select2me" name="office_province" id="domisili_provinsi_list_kerja">
																	<?php foreach($user_office_provinces['user_office_provinces'] as $prov){
																		echo $prov;
																	}?>
																</select>	
													
															</div>
														</div>																										
														<div class="form-group">
															<label class="control-label col-md-3">Kab/Kota<span class="required">* </span></label>
															
															<div class="col-md-4">
																<select class="form-control select2me" name="office_district" id="domisili_kabupaten_list_kerja">
																	<?php foreach($user_office_provinces['user_office_districts'] as $dist){
																		echo $dist;
																	}?>
																</select>	
															</div>
														</div>
												<div class="form-group">
													<label class="control-label col-md-3">Alamat
														<span class="required">* </span>
													</label>
													<div class="col-md-4">
														<textarea class="form-control" name="office_address" placeholder="contoh: Jalan Kemang Timur no 3" /><?php if(isset($user['office_address'])) echo $user['office_address'];?></textarea>
														<span class="help-block">
														Isi alamat kantor </span>
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">Kode Pos
														<span class="required">* </span>
													</label>
													<div class="col-md-4">
														<input type="text" class="form-control" name="office_zip_code" placeholder="contoh: 16911" value="<?php if(isset($user['office_zip_code'])) echo $user['office_zip_code'];?>"/>
														<span class="help-block">
														Isi kode pos </span>
													</div>
												</div>														
													</div>
													
													<?php $this->load->view('pages/dashboard/inc/button_submit');?>
												</form>
											</div>
											<!-- END DETAIL PROFILE TAB -->
											<!-- CHANGE AVATAR TAB -->
											<div class="tab-pane form" id="tab_1_3">
												
												<div class="form-horizontal">
																									    <form action="javascript:;" class="form-horizontal" id="edit_change_photo">			
													<div class="form-body">
														<?php $this->load->view('pages/dashboard/inc/alert_form')?>
														<h4 class="form-section">Ganti Foto</h4>
														
														
														<div class="form-group">
															<div class="col-md-6">
																<div class="row">
																	<div class="col-md-offset-3 col-md-9">
																	    
																		<?php $this->load->view('pages/dashboard/inc/edit_photo')?>
																	</div>
																</div>
															</div>
															<div class="col-md-6"></div>
														</div>
													</div>
													<?php $this->load->view('pages/dashboard/inc/button_submit');?>
													 </form>													
												</div>
											</div>
											<!-- END CHANGE AVATAR TAB -->
											<!-- CHANGE PASSWORD TAB -->
											<div class="tab-pane form" id="tab_1_4">
												
												<form action="javascript:;" class="form-horizontal" id="edit_change_password">
													
													<div class="form-body">
														<?php $this->load->view('pages/dashboard/inc/alert_form')?>
														<h4 class="form-section">Ganti Password</h4>
														
														<?php $this->load->view('pages/dashboard/inc/edit_password')?>
													</div>
													
													<?php $this->load->view('pages/dashboard/inc/button_submit');?>
													
												</form>
											</div>
											<!-- END CHANGE PASSWORD TAB -->
											
											<!-- SOCMED TAB -->
											<div class="tab-pane form" id="tab_1_6">
												
												<form action="javascript:;" class="form-horizontal" id="edit_social_media">
													
													<div class="form-body">
														<?php $this->load->view('pages/dashboard/inc/alert_form')?>
														<h4 class="form-section">Social Media</h4>
														
														<?php $this->load->view('pages/dashboard/inc/edit_socmed')?>
													</div>
													
													<?php $this->load->view('pages/dashboard/inc/button_submit');?>
													
												</form>
											</div>
											<!-- END SOCMED TAB -->
										</div>
									</div>
								</div>
							</div>
                        </div>
                    </div>