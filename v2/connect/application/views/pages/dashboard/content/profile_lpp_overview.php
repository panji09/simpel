                    <div class="profile-content">
                        <div class="row">
							<div class="col-md-12">
								<div class="portlet light">
									<div class="portlet-title tabbable-line">
										<div class="caption caption-md">
											<i class="icon-globe theme-font hide"></i>
											<span class="caption-subject font-green-sharp bold uppercase">Profil Akun</span>
										</div>
										<ul class="nav nav-tabs">
											<li class="active">
												<a href="#tab_1_1" data-toggle="tab">Akun Info</a>
											</li>
											<li>
												<a href="#tab_1_2" data-toggle="tab">Profil LPP</a>
											</li>
										</ul>
									</div>
									<div class="portlet-body">
										<div class="tab-content">
											<!-- PERSONAL INFO TAB -->
											<div class="tab-pane form active" id="tab_1_1">
												<div class="form-horizontal registrasi-lkpp">
													<div class="form-body">
														<?php $this->load->view('pages/dashboard/inc/overview_basic')?>
														
													</div>
													<div class="form-actions">
														<div class="row">
															<div class="col-md-6">
																<div class="row">
																	<div class="col-md-offset-3 col-md-9">
																		<a href='<?=site_url('dashboard/account_settings')?>' class="btn green"><i class="fa fa-pencil"></i> Edit</a>
																	</div>
																</div>
															</div>
															<div class="col-md-6">
															</div>
														</div>
													</div>
												</div>
												
											</div>
											<!-- END PERSONAL INFO TAB -->
											<!-- PROFILE DETAIL TAB -->
											<div class="tab-pane form" id="tab_1_2">
												<!-- BEGIN FORM-->
												<div class="form-horizontal">
													<div class="form-body">														
														<h4 class="form-section">Data Lembaga</h4>
												
														<!-- detail data LPP -->
														<div class="form-group">
															<label class="control-label col-md-3">Nama Lembaga:</label>
															<div class="col-md-4">
																<p class="form-control-static">
																	<?php if(isset($user['training_institute'])) echo $user['training_institute'];?>
																</p>
															</div>
														</div>
														<div class="form-group">
															<label class="control-label col-md-3">Status Lembaga Pelatihan:</label>
															<div class="col-md-4">
																<p class="form-control-static">
																	<?php if(isset($user['training_institute_status'])) echo $user['training_institute_status'];?>
																</p>
															</div>
														</div>
														<div class="form-group">
															<label class="control-label col-md-3">Alamat Kantor:</label>
															<div class="col-md-4">
																<p class="form-control-static">
																	<?php if(isset($user['office_address'])) echo $user['office_address'];?>
																</p>
															</div>
														</div>
														<div class="form-group">
															<label class="control-label col-md-3">Kode Pos:</label>
															<div class="col-md-4">
																<p class="form-control-static">
																	<?php if(isset($user['office_zip_code'])) echo $user['office_zip_code'];?>
																</p>
															</div>
														</div>														
														<div class="form-group">
															<label class="control-label col-md-3">Provinsi:</label>
															<div class="col-md-4">
																<p class="form-control-static">
																	<?php if(isset($user['office_province'])) echo $user['office_province']['name'];?>
																</p>
															</div>
														</div>
														<div class="form-group">
															<label class="control-label col-md-3">Kab / Kota:</label>
															<div class="col-md-4">
																<p class="form-control-static">
																	<?php if(isset($user['office_district'])) echo $user['office_district']['name'];?>
																</p>
															</div>
														</div>
														<div class="form-group">
															<label class="control-label col-md-3">No.Telp:</label>
															<div class="col-md-4">
																<p class="form-control-static">
																	<?php if(isset($user['office_phone'])) echo $user['office_phone'];?>
																</p>
															</div>
														</div>
														<div class="form-group">
															<label class="control-label col-md-3">No.Fax:</label>
															<div class="col-md-4">
																<p class="form-control-static">
																	<?php if(isset($user['office_fax'])) echo $user['office_fax'];?>
																</p>
															</div>
														</div>
												
														<h4 class="form-section">Penanggung Jawab (Kepala Lembaga Pelatihan)</h4>
														
														<div class="form-group">
															<label class="control-label col-md-3">Nama Penanggung Jawab:</label>
															<div class="col-md-4">
																<p class="form-control-static">
																	<?php if(isset($user['responsible_person_name'])) echo $user['responsible_person_name'];?>
																</p>
															</div>
														</div>
														<div class="form-group">
															<label class="control-label col-md-3">Jabatan Penanggung Jawab:</label>
															<div class="col-md-4">
																<p class="form-control-static">
<?php if(isset($user['responsible_person_position'])) echo $user['responsible_person_position'];?>
																</p>
															</div>
														</div>
<?php if($user['training_institute_status'] == 'negeri'){ ?>														
														<div class="form-group negeri box">
															<label class="control-label col-md-3">NIP Penanggung Jawab:</label>
															<div class="col-md-4">
																<p class="form-control-static">
<?php if(isset($user['responsible_person_nip'])) echo $user['responsible_person_nip'];?>
																</p>
															</div>
														</div>
<?php }else{ ?>															
														<div class="form-group swasta box">
															<label class="control-label col-md-3">No.KTP Penanggung Jawab:</label>
															<div class="col-md-4">
																<p class="form-control-static">
<?php if(isset($user['responsible_person_ktp'])) echo $user['responsible_person_ktp'];?>
																</p>
															</div>
														</div>
														<div class="form-group swasta box">
															<label class="control-label col-md-3">KTP Penanggung Jawab:</label>
															<div class="col-md-4">
																<p class="form-control-static">
<?php if(isset($user['responsible_person_ktp_upload'])) echo anchor($user['responsible_person_ktp_upload'], basename($user['responsible_person_ktp_upload']), 'target="_blank"');?>
																</p>
															</div>
														</div>
														
														
														
														<?php } ?>
														
														<div class="form-group">
															<label class="control-label col-md-3">Struktur Organisasi:</label>
															<div class="col-md-4">
																<p class="form-control-static">
<?php if(isset($user['structure_organization_upload'])) echo anchor($user['structure_organization_upload'], basename($user['structure_organization_upload']), 'target="_blank"');?>
																</p>
															</div>
														</div>
												
														<h4 class="form-section">Data Pelengkap</h4>
														<div class="form-group">
															<label class="control-label col-md-3">Surat Permohonan Terdaftar/Terakreditasi:</label>
															<div class="col-md-4">
																<p class="form-control-static">
<?php if(isset($user['accreditation_upload'])) echo anchor($user['accreditation_upload'], basename($user['accreditation_upload']), 'target="_blank"');?>
																</p>
															</div>
														</div>
														<div class="form-group">
															<label class="control-label col-md-3">Surat Pernyataan Komitmen Terdaftar/Terakreditasi:</label>
															<div class="col-md-4">
																<p class="form-control-static">
<?php if(isset($user['commitment_upload'])) echo anchor($user['commitment_upload'], basename($user['commitment_upload']), 'target="_blank"');?>
																</p>
															</div>
														</div>
														<div class="form-group">
															<label class="control-label col-md-3"><?=(isset($user['training_institute_status']) && $user['training_institute_status'] == 'negeri' ? 'Apakah Memiliki Tugas Fungsi (TUPOKSI) Pelatihan?' : 'Apakah Memiliki Surat Tugas Melaksanakan Pendidikan/Pelatihan?')?>:</label>
															<div class="col-md-4">
																<p class="form-control-static">
<?php if(isset($user['training_task'])) echo $user['training_task'];?>
																</p>
															</div>
														</div>
<?php if($user['training_task'] == 'ya'){?>														
														<div class="form-group ya box">
															<label class="control-label col-md-3"></label>
															<div class="col-md-4">
																<p class="form-control-static">
<?php if(isset($user['training_task_upload'])) echo anchor($user['training_task_upload'], basename($user['training_task_upload']), 'target="_blank"');?>
																</p>
															</div>
														</div>
														<?php }?>
												
														<!-- negeri -->
														<?php if(isset($user['training_institute_status']) && strtolower($user['training_institute_status']) == 'negeri'):?>
														<div class="form-group negeri box">
															<label class="control-label col-md-3">Persyaratan Keuangan (PNBP/BLU/DIPA):</label>
															<div class="col-md-4">
																<p class="form-control-static">
<?php if(isset($user['financial_requirement_upload'])) echo anchor($user['financial_requirement_upload'], basename($user['financial_requirement_upload']), 'target="_blank"');?>
																</p>
															</div>
														</div>
														<?php endif;?>
														
														
														<!-- swasta -->
														<?php if(isset($user['training_institute_status']) && strtolower($user['training_institute_status']) == 'swasta'):?>
														<div class="form-group swasta box">
															<label class="control-label col-md-3">Persyaratan Keuangan (NPWP):</label>
															<div class="col-md-4">
																<p class="form-control-static">
<?php if(isset($user['financial_npwp_number'])) echo $user['financial_npwp_number'];?>
																</p>
															</div>
														</div>
														<div class="form-group swasta box">
															<label class="control-label col-md-3"></label>
															<div class="col-md-4">
																<p class="form-control-static">
<?php if(isset($user['financial_requirement_npwp_upload'])) anchor($user['financial_requirement_npwp_upload'], basename($user['financial_requirement_npwp_upload']), 'target="_blank"');?>
																</p>
															</div>
														</div>
														<?php endif;?>
														
														<div class="form-group">
															<label class="control-label col-md-3">Sistem Manajemen Mutu:</label>
															<div class="col-md-4">
																<p class="form-control-static">
<?php if(isset($user['quality_management_upload'])) echo anchor($user['quality_management_upload'], basename($user['quality_management_upload']), 'target="_blank"');?>
																</p>
															</div>
														</div>	
														<div class="form-group">
															<label class="control-label col-md-3">Standar Pelatihan:</label>
															<div class="col-md-4">
																<p class="form-control-static">
<?php if(isset($user['standart_training_upload'])) echo anchor($user['standart_training_upload'], basename($user['standart_training_upload']), 'target="_blank"');?>
																</p>
															</div>
														</div>	
														
														<?php $this->load->view('pages/dashboard/inc/overview_socmed')?>
													</div>
													
													<div class="form-actions">
														<div class="row">
															<div class="col-md-6">
																<div class="row">
																	<div class="col-md-offset-3 col-md-9">
																		<a href='<?=site_url('dashboard/account_settings')?>' class="btn green"><i class="fa fa-pencil"></i> Edit</a>
																	</div>
																</div>
															</div>
															<div class="col-md-6">
															</div>
														</div>
													</div>
													
												</div>
												<!-- END FORM-->
											</div>
											<!-- END ROFILE DETAIL TAB -->
										</div>
									</div>
								</div>
							</div>
                        </div>
                    </div>