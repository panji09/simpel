                    <div class="profile-content">
                        <div class="row">
							<div class="col-md-12">
								<div class="portlet light">
									<div class="portlet-title tabbable-line">
										<div class="caption caption-md">
											<i class="icon-globe theme-font hide"></i>
											<span class="caption-subject font-green-sharp bold uppercase">Akun Profil</span>
										</div>
										<ul class="nav nav-tabs">
											<li class="active">
												<a href="#tab_1_1" data-toggle="tab">Akun Info</a>
											</li>
											<li>
												<a href="#tab_1_2" data-toggle="tab">Ganti Foto Profil</a>
											</li>
											<li>
												<a href="#tab_1_3" data-toggle="tab">Ganti Password</a>
											</li>
											
											<li>
												<a href="#tab_1_6" data-toggle="tab">Social Media</a>
											</li>
										</ul>
									</div>
									<div class="portlet-body">
										<div class="tab-content">
											<!-- PERSONAL INFO TAB -->
											<div class="tab-pane form active" id="tab_1_1">
												
												<form action="javascript:;" id="form_user_basic" class="form-horizontal">
													<div class="form-body">
														
															<?php $this->load->view('pages/dashboard/inc/edit_basic')?>
													</div>
													
													<?php $this->load->view('pages/dashboard/inc/button_submit');?>
												</form>
											</div>
											<!-- END PERSONAL INFO TAB -->
											<!-- CHANGE AVATAR TAB -->
											<div class="tab-pane form" id="tab_1_2">
												
												<div class="form-horizontal">
												    <form action="javascript:;" class="form-horizontal" id="edit_change_photo">
													<div class="form-body">
														<?php $this->load->view('pages/dashboard/inc/alert_form')?>
														<h4 class="form-section">Ganti Foto</h4>
														
														
														<div class="form-group">
															<div class="col-md-6">
																<div class="row">
																	<div class="col-md-offset-3 col-md-9">
																		
																		<?php $this->load->view('pages/dashboard/inc/edit_photo')?>
																	</div>
																</div>
															</div>
															<div class="col-md-6"></div>
														</div>
													</div>
													<?php $this->load->view('pages/dashboard/inc/button_submit');?>
												    </form>
												</div>
											</div>
											<!-- END CHANGE AVATAR TAB -->
											<!-- CHANGE PASSWORD TAB -->
											<div class="tab-pane form" id="tab_1_3">
												
												<form action="javascript:;" class="form-horizontal" id="edit_change_password">
													
													<div class="form-body">
														<?php $this->load->view('pages/dashboard/inc/alert_form')?>
														<h4 class="form-section">Ganti Password</h4>
														
														<?php $this->load->view('pages/dashboard/inc/edit_password')?>
													</div>
													
													<?php $this->load->view('pages/dashboard/inc/button_submit');?>
													
												</form>
											</div>
											<!-- END CHANGE PASSWORD TAB -->
											
											<!-- SOCMED TAB -->
											<div class="tab-pane form" id="tab_1_6">
												
												<form action="javascript:;" class="form-horizontal" id="edit_social_media">
													
													<div class="form-body">
														<?php $this->load->view('pages/dashboard/inc/alert_form')?>
														<h4 class="form-section">Social Media</h4>
														
														<?php $this->load->view('pages/dashboard/inc/edit_socmed')?>
													</div>
													
													<?php $this->load->view('pages/dashboard/inc/button_submit');?>
													
												</form>
											</div>
											<!-- END SOCMED TAB -->
										</div>
									</div>
								</div>
							</div>
                        </div>
                    </div>