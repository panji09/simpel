<?php
    $this->session->set_userdata('current_url',current_url());
?>
                    <div class="profile-content">
                        <div class="row">
							<div class="col-md-12">
								<div class="portlet light">
									<div class="portlet-title tabbable-line">
										<div class="caption caption-md">
											<i class="icon-globe theme-font hide"></i>
											<span class="caption-subject font-green-sharp bold uppercase">Profil Akun</span>
										</div>
										<ul class="nav nav-tabs">
											<li class="active">
												<a href="#tab_1_1" data-toggle="tab">Akun Admin Info</a>
											</li>
											<li>
												<a href="#tab_1_2" data-toggle="tab">Profil LPP</a>
											</li>
											<li>
												<a href="#tab_1_3" data-toggle="tab">Ganti Foto Profil</a>
											</li>
											<li>
												<a href="#tab_1_4" data-toggle="tab">Ganti Password</a>
											</li>
											
											<li>
												<a href="#tab_1_6" data-toggle="tab">Social Media</a>
											</li>
											
										</ul>
									</div>
									<div class="portlet-body">
										<div class="tab-content">
											<!-- PERSONAL INFO TAB -->
											<div class="tab-pane form active" id="tab_1_1">
												
												<form action="javascript:;" id="form_user_basic" class="form-horizontal">
													<div class="form-body">
														
														
															<?php $this->load->view('pages/dashboard/inc/edit_basic')?>
													</div>
													
													<?php $this->load->view('pages/dashboard/inc/button_submit');?>
												</form>
											</div>
											<!-- END PERSONAL INFO TAB -->
											<!-- DETAIL PROFILE TAB -->
											<div class="tab-pane form registrasi-lkpp" id="tab_1_2">
												<form action="javascript:;" class="form-horizontal" id="edit_profile_lpp">
													<div class="form-body">
														
														<?php $this->load->view('pages/dashboard/inc/alert_form')?>
														<h4 class="form-section">Data kantor</h4>
														
														<div class="form-group">
															<label class="control-label col-md-3">Nama Lembaga 
																<span class="required">* </span>
															</label>
															<div class="col-md-4">
																<input type="text" class="form-control" name="training_institute" placeholder="contoh: Lembaga Pelatihan Bakti" value="<?php if(isset($user['training_institute'])) echo $user['training_institute'];?>" />
																<span class="help-block">
																Isi nama lembaga </span>
															</div>
														</div>
														<div class="form-group">
															<label class="control-label col-md-3">Status Lembaga Pelatihan <span class="required">
															* </span>
															</label>
															<div class="col-md-4">
														<div class="radio-list">
															<label>

															<input type="radio" name="training_institute_status" value="negeri" data-title="Negeri"  <?php if($user['training_institute_status'] == 'negeri'){ ?>checked="checked" <?php }?>/>
															Negeri</label>
															<label>
															<input type="radio" name="training_institute_status" value="swasta" data-title="Swasta"  <?php if($user['training_institute_status'] == 'swasta'){ ?>checked="checked" <?php }?>/>
															Swasta</label>
														</div>
																<div id="form_status_lpp_error">
																</div>
															</div>
														</div>
														<div class="form-group">
															<label class="control-label col-md-3">Alamat
																<span class="required">* </span>
															</label>
															<div class="col-md-4">
																<input type="text" class="form-control" name="office_address" placeholder="contoh: Jalan Kemang Timur no 3" value="<?php if(isset($user['office_address'])) echo $user['office_address'];?>"/>
																<span class="help-block">
																Isi alamat kantor </span>
															</div>
														</div>
														<div class="form-group">
															<label class="control-label col-md-3">Kode Pos
																<span class="required">* </span>
															</label>
															<div class="col-md-4">
																<input type="text" class="form-control" name="office_zip_code" placeholder="contoh: 16911" value="<?php if(isset($user['office_zip_code'])) echo $user['office_zip_code'];?>"/>
																<span class="help-block">
																Isi kode pos </span>
															</div>
														</div>														
														<div class="form-group">
															<label class="control-label col-md-3">Provinsi 
																<span class="required">* </span>
															</label>
															<div class="col-md-4">
														<select class="form-control select2me" name="office_province" id="provinsi_kantor_list">
																	<?php  foreach($user_office_provinces['user_office_provinces'] as $prov){
																		echo $prov;
																	}?>
														</select>
																<span class="help-block">
																Isi provinsi kantor </span>
															</div>
														</div>
														<div class="form-group">
															<label class="control-label col-md-3">Kab / Kota
															<span class="required">* </span>
															</label>
															<div class="col-md-4">
														<select class="form-control select2me" name="office_district" id="kabupaten_kantor_list">
																	<?php foreach($user_office_provinces['user_office_districts'] as $dist){
																		echo $dist;
																	}?>
														</select>
																<span class="help-block">
																Isi kabupaten kantor </span>
															</div>
														</div>
														<div class="form-group">
															<label class="control-label col-md-3">No.Telp
															<span class="required">* </span>
															</label>
															<div class="col-md-4">
																<input type="text" class="form-control" name="office_phone" placeholder="contoh: 02133805" value="<?php if(isset($user['office_phone'])) echo $user['office_phone'];?>"/>
																<span class="help-block">
																	Isi no telpon kantor</span>
															</div>
														</div>
														<div class="form-group">
															<label class="control-label col-md-3">No.Fax
															<span class="required">* </span>
															</label>
															<div class="col-md-4">
																<input type="text" class="form-control" name="office_fax" placeholder="contoh: 02133804" value="<?php if(isset($user['office_fax'])) echo $user['office_fax'];?>"/>
																<span class="help-block">
																	Isi no fax kantor</span>
															</div>
														</div>
												
														<h4 class="form-section">Penanggung Jawab (Kepala Lembaga Pelatihan)</h4>
														
												
														<div class="form-group">
															<label class="control-label col-md-3">Nama Penanggung Jawab
															<span class="required">* </span>
															</label>
															<div class="col-md-4">
																<input type="text" class="form-control" name="responsible_person_name" placeholder="contoh: Sutrisno" value="<?php if(isset($user['responsible_person_name'])) echo $user['responsible_person_name'];?>"/>
																<span class="help-block">
																Isi Nama penanggung jawab</span>
															</div>
														</div>
														<div class="form-group">
															<label class="control-label col-md-3">Jabatan Penanggung Jawab
															<span class="required">* </span>
															</label>
															<div class="col-md-4">
																<input type="text" class="form-control" name="responsible_person_position" placeholder="contoh: Kepala Pelaksana" value="<?php if(isset($user['responsible_person_position'])) echo $user['responsible_person_position'];?>"/>
																<span class="help-block">
																Isi Jabatan penanggung jawab</span>
															</div>
														</div>
																										
												<!-- tampilkan jika status pegawai negeri -->
												<div class="form-group negeri box">
													<label class="control-label col-md-3">NIP Penanggung Jawab
													<span class="required">* </span>
													</label>
													<div class="col-md-4">
														<input type="text" class="form-control negeri" name="responsible_person_nip" placeholder="contoh: 3938484884990" value="<?php if(isset($user['responsible_person_nip'])) echo $user['responsible_person_nip'];?>"/>
														<span class="help-block">
														Isi NIP penanggung jawab</span>
													</div>
												</div>														
												
														<!-- tampilkan jika status swasta -->
														<div class="form-group swasta box">
															<label class="control-label col-md-3">No.KTP Penanggung Jawab
															<span class="required">* </span>
															</label>
															<div class="col-md-4">
																<input type="text" class="form-control swasta" name="responsible_person_ktp" placeholder="contoh: 223322311456" value="<?php if(isset($user['responsible_person_ktp'])) echo $user['responsible_person_ktp'];?>"/>
																<span class="help-block">
																Isi No.KTP penanggung jawab</span>
															</div>
														</div>
												
														<!-- tampilkan jika status swasta -->
														<div class="form-group swasta box">
															<label class="control-label col-md-3">KTP Penanggung Jawab
															<span class="required">* </span>
															</label>
															<div class="col-md-4">
																<div class="input-group">
																	<input type="text" class="form-control swasta" placeholder="klik upload button" name="responsible_person_ktp_upload" id="upload-ktp-penanggung-jawab" value="<?php if(isset($user['responsible_person_ktp_upload'])) echo $user['responsible_person_ktp_upload'];?>" readonly>
																	<span class="browse_files input-group-btn">
																		<button class="btn red" type="button" id="upload-ktp-penanggungjawab-field">Upload</button>
																	</span>
																</div>
																<div id="form_ktp_penanggungjawab_error"></div>
  															
																<span class="help-block">
																Upload KTP penanggung jawab </span>
															</div>
														</div>
														
														
														
												<div class="form-group">
													<label class="control-label col-md-3">Struktur Organisasi
													<span class="required">* </span>
													</label>
													<div class="col-md-4">
														<div class="input-group">

																	<input type="text" class="form-control swasta" placeholder="klik upload button" name="structure_organization_upload" id="structure_organization_upload_handler" value="<?php if(isset($user['structure_organization_upload'])) echo $user['structure_organization_upload'];?>" readonly>
																	<span class="browse_files input-group-btn">
																		<button class="btn red" type="button" id="structure_org_upload">Upload</button></span>															
														</div>
														<div id="form_structure_organization_error"></div> 															
														<span class="help-block">
														Upload dokumen struktur organisasi </span>
													</div>
												</div>																
												
														<h4 class="form-section">Upload data pelengkap</h4>
												
														<div class="form-group">
															<label class="control-label col-md-3">Surat Permohonan Terdaftar/Terakreditasi
															<span class="required">* </span>
															</label>
															<div class="col-md-4">
													
																<div class="input-group">
																	<input type="text" class="form-control" placeholder="klik upload button" name="accreditation_upload" id="upload-data-text" value="<?php if(isset($user['accreditation_upload'])) echo $user['accreditation_upload'];?>" readonly>
																	<span class=" browse_files input-group-btn">
																		<button class="btn red" type="button" id="upload-data-field">Upload</button>
																	</span>
																</div>
																<div id="form_surat_permohonan_error"></div>
																<span class="help-block">
																Upload Surat Permohonan Terdaftar/Terakreditasi </span>
															</div>
														</div>
												
														<div class="form-group">
															<label class="control-label col-md-3">Surat Pernyataan Komitmen
															<span class="required">* </span>
															</label>
															<div class="col-md-4">
																<div class="input-group">
																	<input type="text" class="form-control" placeholder="klik upload button" name="commitment_upload" id="upload-data-komitmen-text" value="<?php if(isset($user['accreditation_upload'])) echo $user['accreditation_upload'];?>" readonly>
																	<span class="browse_files input-group-btn">
																		<button class="btn red" type="button" id="upload-data-komitmen">Upload</button>
																	</span>
																</div>
																<div id="form_surat_pernyataan_error"></div>
																<span class="help-block">
																Klik <a href="">disini</a> untuk download contoh surat</span>
															</div>
														</div>
														<!-- new code here-->
																							

												<div class="form-group negeri box">
													<label class="control-label col-md-3">Apakah Memiliki Tugas Fungsi (TUPOKSI) Pelatihan? <span class="required">
													* </span>
													</label>
													<div class="col-md-4">
														<div class="radio-list">
															<label>
															<input type="radio" name="training_task_negeri" value="ya" data-title="Ya" <?php if($user['training_task'] == 'ya'){ ?>checked="checked" <?php }?>/>
															Ya</label>
															<label>
															<input type="radio" name="training_task_negeri" value="tidak" data-title="Tidak" <?php if($user['training_task'] == 'tidak'){ ?>checked="checked" <?php }?>/>
															Tidak</label>
														</div>
														<div id="form_surat_tugas_negeri_error">
														</div>
													</div>
												</div>
												
												<div class="form-group ya_negeri box negeri">
													<label class="control-label col-md-3"><!--Surat Tugas Pelatihan
													<span class="required">* </span>-->
													</label>
													<div class="col-md-4">
														<div class="input-group">
															<input type="text" class="form-control" placeholder="klik upload button" name="training_task_negeri_upload" id="training_task_negeri_upload" value="<?php if(isset($user['training_task_upload'])) echo $user['training_task_upload'];?>" readonly>
															<span class="browse_files input-group-btn">
																<button class="btn red" type="button" id="upload-data-komitmen">Upload</button>
															</span>
														</div>
														<div id="surat_tugas_negeri_error"></div>
														<span class="help-block">
														Upload dokumen pendukung</span>
													</div>
												</div>												
												
												<div class="form-group swasta box">
													<label class="control-label col-md-3">Apakah Memiliki Surat Tugas Melaksanakan Pendidikan/Pelatihan? <span class="required">
													* </span>
													</label>
													<div class="col-md-4">
														<div class="radio-list">
															<label>
															<input type="radio" name="training_task_swasta" value="ya" data-title="Ya"  <?php if($user['training_task'] == 'ya'){ ?>checked="checked" <?php }?>/>
															Ya</label>
															<label>
															<input type="radio" name="training_task_swasta" value="tidak" data-title="Tidak" <?php if($user['training_task'] == 'tidak'){ ?>checked="checked" <?php }?>/>
															Tidak</label>
														</div>
														<div id="form_surat_tugas_swasta_error">
														</div>
													</div>
												</div>
												
												<div class="form-group ya_swasta box swasta">
													<label class="control-label col-md-3"><!--Surat Tugas Pelatihan
													<span class="required">* </span>-->
													</label>
													<div class="col-md-4">
														<div class="input-group">		
															<input type="text" class="form-control" placeholder="klik upload button" name="training_task_swasta_upload" id="training_task_swasta_upload" value="<?php if(isset($user['training_task_upload'])) echo $user['training_task_upload'];?>" readonly>
															<span class="browse_files input-group-btn">
																<button class="btn red" type="button" id="upload-data-komitmen">Upload</button>
															</span>															
														</div>
														<div id="surat_tugas_swasta_error"></div>
														<span class="help-block">
														Upload ijin resmi </span>
													</div>
												</div>
												
												<!-- tampilkan jika pegawai negeri -->
												<div class="form-group negeri box">
													<label class="control-label col-md-3">Persyaratan Keuangan
													<span class="required">* </span>
													</label>
													<div class="col-md-4">
														<div class="input-group">
															<input type="text" class="form-control" placeholder="klik upload button" name="financial_requirement_upload" id="financial_requirement_upload" value="<?php if(isset($user['financial_requirement_upload'])) echo $user['financial_requirement_upload'];?>" readonly>
															<span class="browse_files input-group-btn">
																<button class="btn red" type="button" id="upload-data-komitmen">Upload</button>
															</span>																	
														</div>
														<div id="form_keuangan_pns_error"></div>
														<span class="help-block">
														Upload dokumen PNBP/BLU/DIPA</span>
													</div>
												</div>
												
												<!-- tampilkan jika swasta -->
												<div class="form-group swasta box">
													<label class="control-label col-md-3">Persyaratan Keuangan
													<span class="required">* </span>
													</label>
													<div class="col-md-4">
														<input type="text" class="form-control" name="financial_npwp_number" placeholder="contoh: 33423498938493434" value="<?php if(isset($user['financial_npwp_number'])) echo $user['financial_npwp_number'];?>"/>
														<span class="help-block">
														Nomor NPWP</span>
													</div>
												</div>
												
												<div class="form-group swasta box">
													<label class="control-label col-md-3">&nbsp;</label>
													<div class="col-md-4">
														<?php
														/*
														<div class="input-group">
															<input id="financial_requirement_npwp_upload_handler" type="file" name="files" data-url="<?=base_url()?>assets/upload/index.php">
				<input type='hidden' name='financial_requirement_npwp_upload' id='financial_requirement_npwp_upload' value=''>
														</div>
														*/
														?>
														<div class="input-group">
															<input type="text" class="form-control" placeholder="klik upload button" name="financial_requirement_npwp_upload" id="financial_requirement_npwp_upload" value="<?php if(isset($user['financial_requirement_upload'])) echo $user['financial_requirement_upload'];?>" readonly>
															<span class="browse_files input-group-btn">
																<button class="btn red" type="button" id="upload-data-komitmen">Upload</button>
															</span>
															
														</div>
														<div id="form_keuangan_swasta_error"></div>
														<span class="help-block">
														Upload NPWP</span>
													</div>
												</div>
											
														<div class="form-group">
															<label class="control-label col-md-3">Sistem Manajemen Mutu
															<span class="required">* </span>
															</label>
															<div class="col-md-4">
																<div class="input-group">
																	<input type="text" class="form-control" placeholder="klik upload button" name="quality_management_upload" id="upload-manajemen-mutu-field" value="<?php if(isset($user['quality_management_upload'])) echo $user['quality_management_upload'];?>" readonly>
																	<span class="browse_files input-group-btn">
																		<button class="btn red" type="button" id="upload-manajemen-mutu" >Upload</button>
																	</span>
																</div>
																<div id="form_manajemen_mutu_error"></div>
																<span class="help-block">
																	Klik <a href="">disini</a> untuk download contoh dokument
																</span>
															</div>
														</div>
														<div class="form-group">
															<label class="control-label col-md-3">Standar Pelatihan
															<span class="required">* </span>
															</label>
															<div class="col-md-4">
																<div class="input-group">
																	<input type="text" class="form-control" placeholder="klik upload button" name="standart_training_upload" id="standart_training_upload" value="<?php if(isset($user['standart_training_upload'])) echo $user['standart_training_upload'];?>" readonly>
																	<span class="browse_files input-group-btn">
																		<button class="btn red" type="button" id="upload-standar-training" >Upload</button>
																	</span>
																</div>
																<div id="form_standart_training_error"></div>
																<span class="help-block">
																	Klik <a href="">disini</a> untuk download contoh dokument
																</span>
															</div>
														</div>													
													</div>
													
													<?php $this->load->view('pages/dashboard/inc/button_submit');?>
												</form>
											</div>
											<!-- END DETAIL PROFILE TAB -->
											<!-- CHANGE AVATAR TAB -->
											<div class="tab-pane form" id="tab_1_3">
												
												<div class="form-horizontal">
												    <form action="javascript:;" class="form-horizontal" id="edit_change_photo">													
													<div class="form-body">
														
														<?php $this->load->view('pages/dashboard/inc/alert_form')?>
														<h4 class="form-section">Ganti Foto</h4>
														<div class="form-group">
															<div class="col-md-6">
																<div class="row">
																	<div class="col-md-offset-3 col-md-9">
																		
																	<?php $this->load->view('pages/dashboard/inc/edit_photo')?>
																	</div>
																</div>
															</div>
															<div class="col-md-6"></div>
														</div>
													</div>
													<?php $this->load->view('pages/dashboard/inc/button_submit');?>
												    </form>													
												</div>
											</div>
											<!-- END CHANGE AVATAR TAB -->
											<!-- CHANGE PASSWORD TAB -->
											<div class="tab-pane form" id="tab_1_4">
												
												<form action="javascript:;" class="form-horizontal" id="edit_change_password">
													
													<div class="form-body">
														<?php $this->load->view('pages/dashboard/inc/alert_form')?>
														<h4 class="form-section">Ganti Password</h4>
														
														<?php $this->load->view('pages/dashboard/inc/edit_password')?>
													</div>
													
													<?php $this->load->view('pages/dashboard/inc/button_submit');?>
													
												</form>
											</div>
											<!-- END CHANGE PASSWORD TAB -->
											
											<!-- SOCMED TAB -->
											<div class="tab-pane form" id="tab_1_6">
												
												<form action="javascript:;" class="form-horizontal" id="edit_social_media">
													
													<div class="form-body">
														<?php $this->load->view('pages/dashboard/inc/alert_form')?>
														<h4 class="form-section">Social Media</h4>
														
														<?php $this->load->view('pages/dashboard/inc/edit_socmed')?>
													</div>
													
													<?php $this->load->view('pages/dashboard/inc/button_submit');?>
													
												</form>
											</div>
											<!-- END SOCMED TAB -->
										</div>
									</div>
								</div>
							</div>
                        </div>
                    </div>
                    
                    <?php $this->load->view('pages/dashboard/inc/modal')?>