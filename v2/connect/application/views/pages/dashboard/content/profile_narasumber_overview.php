                    <div class="profile-content">
                        <div class="row">
							<div class="col-md-12">
								<div class="portlet light">
									<div class="portlet-title tabbable-line">
										<div class="caption caption-md">
											<i class="icon-globe theme-font hide"></i>
											<span class="caption-subject font-green-sharp bold uppercase">Profil Akun</span>
										</div>
										<ul class="nav nav-tabs">
											<li class="active">
												<a href="#tab_1_1" data-toggle="tab">Akun Info</a>
											</li>
											<li>
												<a href="#tab_1_2" data-toggle="tab">Detil Profil</a>
											</li>
										</ul>
									</div>
									<div class="portlet-body">
										<div class="tab-content">
											<!-- PERSONAL INFO TAB -->
											<div class="tab-pane form active" id="tab_1_1">
												<div class="form-horizontal registrasi-lkpp">
													<div class="form-body">														
														<?php $this->load->view('pages/dashboard/inc/overview_basic')?>														
													</div>
													<div class="form-actions">
														<div class="row">
															<div class="col-md-6">
																<div class="row">
																	<div class="col-md-offset-3 col-md-9">
																		<a href='<?=site_url('dashboard/account_settings')?>' class="btn green"><i class="fa fa-pencil"></i> Edit</a>
																		
																	</div>
																</div>
															</div>
															<div class="col-md-6">
															</div>
														</div>
													</div>
												</div>
												
											</div>
											<!-- END PERSONAL INFO TAB -->
											<!-- PROFILE DETAIL TAB -->
											<div class="tab-pane form" id="tab_1_2">
												<!-- BEGIN FORM-->
												<div class="form-horizontal">
													<div class="form-body">												<h4 class="form-section">Data Pendidikan dan Kepegawaian</h4>
														<div class="form-group">
															<label class="control-label col-md-3">Pendidikan Terakhir:</label>
															<div class="col-md-9">
																<p class="form-control-static">
																	 <?php if(isset($user['last_education'])) echo $user['last_education'];?>
																</p>
															</div>
														</div>
														<div class="form-group">
															<label class="control-label col-md-3">Gelar Akademis:</label>
															<div class="col-md-9">
																<p class="form-control-static">
																	 <?php if(isset($user['academic_degree'])) echo $user['academic_degree'];?>
																</p>
															</div>
														</div>
														<div class="form-group">
															<label class="control-label col-md-3">Status Kepegawaian:</label>
															<div class="col-md-9">
																<p class="form-control-static">
																	 <?php if(isset($user['employment'])) echo $user['employment'];?>
																</p>
															</div>
														</div>
														<?php if(isset($user['nip_id']) && $user['nip_id']!=''){?>
														<div class="form-group">
															<label class="control-label col-md-3">NIP/NRP:</label>
															<div class="col-md-9">
																<p class="form-control-static">
																	 <?php echo $user['nip_id'];?>
																</p>
															</div>
														</div>
														<?php }?>
														<div class="form-group">
															<label class="control-label col-md-3">No.KTP:</label>
															<div class="col-md-9">
																<p class="form-control-static">
																	 <?php if(isset($user['ktp_id'])) echo $user['ktp_id'];?>
																</p>
															</div>
														</div>
														
														<h4 class="form-section">Data Kantor</h4>
														<div class="form-group">
															<label class="control-label col-md-3">Nama Instansi/Institusi/Perusahaan:</label>
															<div class="col-md-9">
																<p class="form-control-static">
																	 <?php if(isset($user['institution'])) echo $user['institution'];?>
																</p>
															</div>
														</div>
														<div class="form-group">
															<label class="control-label col-md-3">Satuan Kerja:</label>
															<div class="col-md-9">
																<p class="form-control-static">
																	 <?php if(isset($user['work_unit'])) echo $user['work_unit'];?>
																</p>
															</div>
														</div>
														<div class="form-group">
															<label class="control-label col-md-3">Provinsi:</label>
															<div class="col-md-9">
																<p class="form-control-static">
																	 <?php if(isset($user['office_province']['name'])) echo $user['office_province']['name'];?>
																</p>
															</div>
														</div>
														<div class="form-group">
															<label class="control-label col-md-3">Kab/Kota:</label>
															<div class="col-md-9">
																<p class="form-control-static">
																	 <?php if(isset($user['office_district']['name'])) echo $user['office_district']['name'];?>
																</p>
															</div>
														</div>
														<div class="form-group">
															<label class="control-label col-md-3">Alamat:</label>
															<div class="col-md-9">
																<p class="form-control-static">
															  <?=(isset($user['office_address']) ? $user['office_address'] : '');?>
																</p>
															</div>
														</div>
														<div class="form-group">
															<label class="control-label col-md-3">Kode Pos:</label>
															<div class="col-md-9">
																<p class="form-control-static">
															  <?=(isset($user['office_zip_code']) ? $user['office_zip_code'] : '');?>
																</p>
															</div>
														</div>
														
														<h4 class="form-section">Data Atasan</h4>
														<div class="form-group">
															<label class="control-label col-md-3">Nama:</label>
															<div class="col-md-9">
																<p class="form-control-static">
																	 <?php if(isset($user['head_name'])) echo $user['head_name'];?>
																</p>
															</div>
														</div>
														<div class="form-group">
															<label class="control-label col-md-3">Email:</label>
															<div class="col-md-9">
																<p class="form-control-static">
																	 <?php if(isset($user['head_email'])) echo $user['head_email'];?>
																</p>
															</div>
														</div>
														<div class="form-group">
															<label class="control-label col-md-3">No. Telp:</label>
															<div class="col-md-9">
																<p class="form-control-static">
																	 <?php if(isset($user['head_phone'])) echo $user['head_phone'];?>
																</p>
															</div>
														</div>
														
														<?php $this->load->view('pages/dashboard/inc/overview_socmed')?>	
														
													</div>
													
													<div class="form-actions">
														<div class="row">
															<div class="col-md-6">
																<div class="row">
																	<div class="col-md-offset-3 col-md-9">
																		<a href='<?=site_url('dashboard/account_settings')?>' class="btn green"><i class="fa fa-pencil"></i> Edit</a>
																		
																	</div>
																</div>
															</div>
															<div class="col-md-6">
															</div>
														</div>
													</div>
													
												</div>
												<!-- END FORM-->
											</div>
											<!-- END ROFILE DETAIL TAB -->
										</div>
									</div>
								</div>
							</div>
                        </div>
                    </div>