					<div class="profile-sidebar" style="width: 250px;">
					    <!-- PORTLET MAIN -->
					    <div class="portlet light profile-sidebar-portlet">
					        <!-- SIDEBAR USERPIC -->
					        <div class="profile-userpic">
					            <img src="<?php if(isset($profile['avatar'])) echo $profile['avatar'];
								;?>" class="img-responsive" alt="">
					        </div>
					        <!-- END SIDEBAR USERPIC -->
					        <!-- SIDEBAR USER TITLE -->
					        <div class="profile-usertitle">
					            <div class="profile-usertitle-name">
					                <?php if(isset($profile['name'])) echo $profile['name'];?>
					            </div>
					            <div class="profile-usertitle-job">
					                <?php if(isset($profile['status']) && strtolower($profile['status']) == 'narasumber'){ echo 'pengajar';} else {echo $profile['status'];}?>
					            </div>
					        </div>
					        <!-- END SIDEBAR USER TITLE -->
					        <!-- SIDEBAR BUTTONS -->
					        <div class="profile-userbuttons">
					            <a href='<?=site_url('dashboard/account_settings')?>' class="btn btn-circle btn-danger btn-sm">Edit Profil</a>
					        </div>
					        <!-- END SIDEBAR BUTTONS -->
					        <!-- SIDEBAR MENU -->
					        <div class="profile-usermenu">
					            <ul class="nav">
									<?php if(isset($sidebar_menu)) foreach($sidebar_menu as $menu){ ?>
					                <li <?php if($menu['tag'] == $active){ ?> class="active" <?php } ?>>
					                    <a href="<?php if(isset($menu['url'])) echo $menu['url']; ?>">
					                        <i class="<?php if(isset($menu['icon'])) echo $menu['icon']; ?>"></i> <?php if(isset($menu['title'])) echo $menu['title'];?> </a>
					                </li>										
									<?php }?>
					            </ul>
					        </div>
					        <!-- END MENU -->
					    </div>
					    <!-- END PORTLET MAIN -->
					</div>	