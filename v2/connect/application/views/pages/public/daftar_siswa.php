<!-- <input id="fileupload" type="file" name="files[]" data-url="<?=base_url()?>assets/upload/index.php" multiple> -->
<!-- BEGIN PAGE CONTAINER -->
<div class="page-container">

	<!-- BEGIN PAGE CONTENT -->
	<div class="page-content">
		<div class="container">
			
			<!-- BEGIN PAGE CONTENT INNER -->
			<div class="row">
				<div class="col-md-12">
					<div class="portlet light" id="form_wizard_1">
						<div class="portlet-title">
							<div class="caption">
								<span class="caption-subject font-green-sharp bold uppercase">Registrasi Peserta - 
									<span class="step-title">Step 1 of 3 </span>
								</span>
							</div>
						</div>
						<div class="portlet-body form">
							<form action="javascript:;" class="form-horizontal" id="submit_form" method="POST">
								<div class="form-wizard">
									<div class="form-body">
										<ul class="nav nav-pills nav-justified steps">
											<li>
												<a href="#tab1" data-toggle="tab" class="step">
												<span class="number">
												1 </span>
												<span class="desc">
												<i class="fa fa-check"></i> Profil Pengguna </span>
												</a>
											</li>
											<li>
												<a href="#tab2" data-toggle="tab" class="step">
												<span class="number">
												2 </span>
												<span class="desc">
												<i class="fa fa-check"></i> Profil Detail </span>
												</a>
											</li>
											<li>
												<a href="#tab3" data-toggle="tab" class="step">
												<span class="number">
												3 </span>
												<span class="desc">
												<i class="fa fa-check"></i> Konfirmasi </span>
												</a>
											</li>
										</ul>
										<div id="bar" class="progress progress-striped" role="progressbar">
											<div class="progress-bar progress-bar-success">
											</div>
										</div>
										<div class="tab-content">
											<?php $this->load->view('pages/public/notif_wizard')?>
											<!-- Tab satu- data global User -->
											
											<div class="tab-pane active" id="tab1">
												<h3 class="block">Lengkapi data profil anda</h3>
												<div class="form-group">
												      <label class="control-label col-md-3">Masukan No Sertifikat Keahlian PBJP (Jika ada) 
												      </label>
												      <div class="col-md-4">
													      
													      <input type="text" class="form-control" name="procurement_certificate" placeholder="misal : 092234223522334"/>
													      <span class="help-block">
													      Data akan otomatis terisi jika memiliki Sertifikat Keahlian PBJP yang sudah teregister di  <a href='http://logbook.lkpp.go.id/logbook2/index.php'>logbook</a> </span>
												      </div>
												      <div class="col-md-2">
													      <button id='check_procurement_certificate' type='button' class='btn btn-default'>Cek Sertifikat</button>
													</div>
												      
												</div>
												<?php $this->load->view('pages/public/basic_data'); ?>
											</div>
											
											<div class="tab-pane" id="tab2">
												<h3 class="block">Lengkapi detail data profil anda</h3>
												<h4 class="form-section">Data pendidikan dan kepegawaian</h4>
												<div class="form-group">
													<label class="control-label col-md-3">
														Pendidikan Terakhir <span class="required">* </span>
													</label>
													<div class="col-md-4">
														<select class="form-control select2me" name="last_education" id="pendidikan_list">
															<option value=""></option>
														</select>
														<span class="help-block">
														Isi pendidikan terakhir </span>
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">Gelar Akademis</label>
													<div class="col-md-4">
														<input type="text" class="form-control" name="academic_degree" id="academic_degree"  placeholder="contoh: S.T."/>
														<span class="help-block">
														Isi gelar akademis </span>
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">Status Kepegawaian <span class="required">* </span>
													</label>
													<div class="col-md-4">
														<select class="form-control select2me" name="employment" id="status_kepegawaian_list">
															<option value=""></option>
														</select>
														<span class="help-block">
														Isi status kepegawaian </span>
													</div>
												</div>
												<div class="form-group pns">
													<label class="control-label col-md-3">NIP/NRP <span class="required">* </span></label> 
													<div class="col-md-4">
														<input type="text" class="form-control" name="nip_id" placeholder="contoh: 197604012001121001"/>
														<span class="help-block">
														Isi NIP/NRP</span>
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">No.KTP <span class="required">* </span></label> 
													<div class="col-md-4">
														<input type="text" class="form-control" name="ktp_id" placeholder="contoh: 1215125451123456"/>
														<span class="help-block">
														Isi No KTP</span>
													</div>
												</div>												
												<h4 class="form-section">Data kantor</h4>
												<div class="form-group">
													<label class="control-label col-md-3">Nama Instansi/Institusi/Perusahaan <span class="required">* </span> </label>
													<div class="col-md-4">													
														<input type="text" class="form-control" name="institution" id="institution" placeholder="contoh: Dewan Perwakilan Daerah"/>
														<span class="help-block">
														Isi nama Instansi/Institusi/Perusahaan Anda berasal</span>
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">Provinsi <span class="required">* </span></label>
													<div class="col-md-4">
														<select class="form-control select2me" name="office_province" id="domisili_provinsi_list_kerja">
															<option value=""></option>
														</select>															
													</div>
												</div>												
												<div class="form-group">
													<label class="control-label col-md-3">Kab/Kota <span class="required">* </span></label>
													<div class="col-md-4">
														<select class="form-control select2me" name="office_district" id="domisili_kabupaten_list_kerja">
															<option value=""></option>
														</select>	
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">Alamat
														<span class="required">* </span>
													</label>
													<div class="col-md-4">
														<textarea class="form-control" name="office_address" placeholder="contoh: Jalan Kemang Timur no 3"/></textarea>
														<span class="help-block">
														Isi alamat kantor </span>
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">Kode Pos
														<span class="required">* </span>
													</label>
													<div class="col-md-4">
														<input type="text" class="form-control" name="office_zip_code" placeholder="contoh: 16911"/>
														<span class="help-block">
														Isi kode pos </span>
													</div>
												</div>
											</div>
											<div class="tab-pane" id="tab3">
												<h3 class="block">Konfirmasi data anda</h3>
												<?php $this->load->view('pages/public/confim_data_basic')?>
											
												<h4 class="form-section">Data Pendidikan dan Kepegawaian</h4>
												<div class="form-group">
													<label class="control-label col-md-3">Pendidikan Terakhir:</label>
													<div class="col-md-4">
														<p class="form-control-static" data-display="last_education">
														</p>
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">Gelar Akademis:</label>
													<div class="col-md-4">
														<p class="form-control-static" data-display="academic_degree">
														</p>
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">Status Kepegawaian:</label>
													<div class="col-md-4">
														<p class="form-control-static" data-display="employment">
														</p>
													</div>
												</div>
												<div class="form-group pns">
													<label class="control-label col-md-3">NIP/NRP:</label>
													<div class="col-md-4">
														<p class="form-control-static" data-display="nip_id">
														</p>
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">No.KTP:</label>
													<div class="col-md-4">
														<p class="form-control-static" data-display="ktp_id">
														</p>
													</div>
												</div>

												<h4 class="form-section">Data Kantor</h4>
												<div class="form-group">
													<label class="control-label col-md-3">Nama Instansi/Institusi/Perusahaan:</label>
													<div class="col-md-4">
														<p class="form-control-static" data-display="institution">
														</p>
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">Provinsi:</label>
													<div class="col-md-4">
														<p class="form-control-static" data-display="office_province">
														</p>
													</div>
												</div>												
												<div class="form-group">
													<label class="control-label col-md-3">Kab/Kota:</label>
													<div class="col-md-4">
														<p class="form-control-static" data-display="office_district">
														</p>
													</div>
												</div>	
												<div class="form-group">
													<label class="control-label col-md-3">Alamat:</label>
													<div class="col-md-4">
														<p class="form-control-static" data-display="office_address">
														</p>
													</div>
												</div>	
												<div class="form-group">
													<label class="control-label col-md-3">Kode Pos:</label>
													<div class="col-md-4">
														<p class="form-control-static" data-display="office_zip_code">
														</p>
													</div>
												</div>	
											</div>
										</div>
									</div>
									<div class="form-actions">
										<div class="row">
											<div class="col-md-offset-3 col-md-9">
												<a href="javascript:;" class="btn default button-previous">
												<i class="m-icon-swapleft"></i> Kembali </a>
												<a href="javascript:;" class="btn blue button-next">
												Lanjut <i class="m-icon-swapright m-icon-white"></i>
												</a>
												<a href="javascript:;" class="btn green button-submit">
												Proses <i class="m-icon-swapright m-icon-white"></i>
												</a>
											</div>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
			<!-- END PAGE CONTENT INNER -->
		</div>
	</div>
	<!-- END PAGE CONTENT -->
</div>
<!-- END PAGE CONTAINER -->