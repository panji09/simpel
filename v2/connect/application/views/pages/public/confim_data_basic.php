<h4 class="form-section">Data Pengguna</h4>
												<div class="form-group">
													<label class="control-label col-md-3">Foto:</label>
													<div class="col-md-4">
														<p class="form-control-static" data-display="photo">
														
														</p>
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">Username:</label>
													<div class="col-md-4">
														<p class="form-control-static" data-display="username">
														</p>
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">Email:</label>
													<div class="col-md-4">
														<p class="form-control-static" data-display="email">
														</p>
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">Tempat Lahir:</label>
													<div class="col-md-4">
														<p class="form-control-static" data-display="birth_place">
														</p>
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">Tanggal Lahir:</label>
													<div class="col-md-4">
														<p class="form-control-static" data-display="birth_date">
														</p>
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">No. Telp/HP</label>
													<div class="col-md-4">
														<p class="form-control-static" data-display="phone">
														</p>
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">Jenis Kelamin </label>
													<div class="col-md-4">
														<p class="form-control-static" data-display="gender">
														</p>
													</div>
												</div>