<?php
	$example_doc = array();
	$query = file_get_contents($this->config->item('simpel_api_url').'/example_document');
	//print_r($query); exit();
	if($query){
		$example_doc = json_decode($query,true);
	}
?>
<div class="page-container">

	<!-- BEGIN PAGE CONTENT -->
	<div class="page-content">
		<div class="container">
			
			<!-- BEGIN PAGE CONTENT INNER -->
			<div class="row">
				<div class="col-md-12">
					<div class="portlet light" id="form_wizard_1">
						<div class="portlet-title">
							<div class="caption">
								<span class="caption-subject font-green-sharp bold uppercase">Registrasi Lembaga Pelatihan - 
									<span class="step-title">Step 1 of 3 </span>
								</span>
							</div>
						</div>
						<div class="portlet-body form">
							<form action="javascript:;" class="form-horizontal registrasi-lkpp" id="submit_form" method="POST">
								<div class="form-wizard">
									<div class="form-body">
										<ul class="nav nav-pills nav-justified steps">
											<li>
												<a href="#tab1" data-toggle="tab" class="step">
												<span class="number">
												1 </span>
												<span class="desc">
												<i class="fa fa-check"></i> Profil Pengguna </span>
												</a>
											</li>
											<li>
												<a href="#tab2" data-toggle="tab" class="step">
												<span class="number">
												2 </span>
												<span class="desc">
												<i class="fa fa-check"></i> Profil Lembaga Pelatihan </span>
												</a>
											</li>
											<li>
												<a href="#tab3" data-toggle="tab" class="step">
												<span class="number">
												3 </span>
												<span class="desc">
												<i class="fa fa-check"></i> Konfirmasi </span>
												</a>
											</li>
										</ul>
										<div id="bar" class="progress progress-striped" role="progressbar">
											<div class="progress-bar progress-bar-success">
											</div>
										</div>
										<div class="tab-content">
											<?php $this->load->view('pages/public/notif_wizard')?>

											<!-- Tab satu- data global User -->
											<div class="tab-pane active" id="tab1">
												<h3 class="block">Lengkapi data Admin penanggung jawab dari Lembaga Pelatihan</h3>
												<?php $this->load->view('pages/public/basic_data'); ?>
											</div>
											
											<div class="tab-pane" id="tab2">
												<h3 class="block">Lengkapi detail data profil Lembaga Pelatihan</h3>
												<h4 class="form-section">Data Lembaga</h4>
												<div class="form-group">
													<label class="control-label col-md-3">Nama Lembaga 
														<span class="required">* </span>
													</label>
													<div class="col-md-4">
														<input type="text" class="form-control" name="training_institute" placeholder="contoh: Lembaga Pelatihan Bakti"/>
														<span class="help-block">
														Isi nama lembaga </span>
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">Status Lembaga Pelatihan <span class="required">
													* </span>
													</label>
													<div class="col-md-4">
														<div class="radio-list">
															<label>
															<input type="radio" name="training_institute_status" value="negeri" data-title="Negeri"/>
															Negeri</label>
															<label>
															<input type="radio" name="training_institute_status" value="swasta" data-title="Swasta"/>
															Swasta</label>
														</div>
														<div id="form_status_lpp_error">
														</div>
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">Alamat
														<span class="required">* </span>
													</label>
													<div class="col-md-4">
														<textarea class="form-control" name="office_address" placeholder="contoh: Jalan Kemang Timur no 3"/></textarea>
														<span class="help-block">
														Isi alamat kantor </span>
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">Kode Pos
														<span class="required">* </span>
													</label>
													<div class="col-md-4">
														<input type="text" class="form-control" name="office_zip_code" placeholder="contoh: 16911"/>
														<span class="help-block">
														Isi kode pos </span>
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">Provinsi 
														<span class="required">* </span>
													</label>
													<div class="col-md-4">
														<select class="form-control select2me" name="office_province" id="provinsi_kantor_list">
															<option value=""></option>
														</select>
														<span class="help-block">
														Isi provinsi kantor </span>
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">Kab / Kota
													<span class="required">* </span>
													</label>
													<div class="col-md-4">
														<select class="form-control select2me" name="office_district" id="kabupaten_kantor_list">
															<option value=""></option>
														</select>
														<span class="help-block">
														Isi Kab / Kota kantor </span>
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">No.Telp
													<span class="required">* </span>
													</label>
													<div class="col-md-4">
														<input type="text" class="form-control" name="office_phone" placeholder="contoh: 02133805"/>
														<span class="help-block">
															Isi no telpon kantor</span>
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">No.Fax
													<span class="required">* </span>
													</label>
													<div class="col-md-4">
														<input type="text" class="form-control" name="office_fax" placeholder="contoh: 02133804"/>
														<span class="help-block">
															Isi no fax kantor</span>
													</div>
												</div>
												
												<h4 class="form-section">Penanggung Jawab (Kepala Lembaga Pelatihan)</h4>
												
												
												<div class="form-group">
													<label class="control-label col-md-3">Nama Penanggung Jawab
													<span class="required">* </span>
													</label>
													<div class="col-md-4">
														<input type="text" class="form-control" name="responsible_person_name" placeholder="contoh: Sutrisno"/>
														<span class="help-block">
														Isi Nama penanggung jawab</span>
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">Jabatan Penanggung Jawab
													<span class="required">* </span>
													</label>
													<div class="col-md-4">
														<input type="text" class="form-control" name="responsible_person_position" placeholder="contoh: Kepala Pelaksana"/>
														<span class="help-block">
														Isi Jabatan penanggung jawab</span>
													</div>
												</div>
												
												<!-- tampilkan jika status pegawai negeri -->
												<div class="form-group negeri box">
													<label class="control-label col-md-3">NIP Penanggung Jawab
													<span class="required">* </span>
													</label>
													<div class="col-md-4">
														<input type="text" class="form-control negeri" name="responsible_person_nip" placeholder="contoh: 3938484884990"/>
														<span class="help-block">
<!-- 														Isi NIP penanggung jawab</span> -->
													</div>
												</div>
												
												<!-- tampilkan jika status swasta -->
												<div class="form-group swasta box">
													<label class="control-label col-md-3">No.KTP Penanggung Jawab
													<span class="required">* </span>
													</label>
													<div class="col-md-4">
														<input type="text" class="form-control swasta" name="responsible_person_ktp" placeholder="contoh: 223322311456" />
														<span class="help-block">
														Isi No.KTP penanggung jawab </span>
													</div>
												</div>
												
												<!-- tampilkan jika status swasta -->
												<div class="form-group swasta box">
													<label class="control-label col-md-3">KTP Penanggung Jawab
													<span class="required">* </span>
													</label>
													<div class="col-md-4">
														<div class="input-group">
															<input type="file" name='files' class='fileupload' id="responsible_person_ktp_upload_handler" data-url="<?=base_url()?>assets/upload/index.php">
															<input id='responsible_person_ktp_upload' type='hidden' value='' name='responsible_person_ktp_upload' class='output'>
															<label style='display:none' class='loading'>loading..</label>
															<!-- <label style='display:none' class='filename'></label> -->
															<a target='_blank' style='display:none' class='filename' href=''></a><br>
															
															<input style='display:none' type='button' class='btn red delete-fileupload' value='Hapus'></input>
														</div>
														<div id="form_ktp_penanggungjawab_error"></div>  															
														<span class="help-block">
														Upload KTP penanggung jawab </span>
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">Struktur Organisasi
													<span class="required">* </span>
													</label>
													<div class="col-md-4">
														<div class="input-group">
															<input class='fileupload' type="file" name='files' id="structure_rganization_upload_handler" data-url="<?=base_url()?>assets/upload/index.php">
															<input class='output' id='structure_org_upload' type='hidden' value='' name='structure_organization_upload' data-display="structure_organization_upload" data-is_url=1>
															<label style='display:none' class='loading'>loading..</label>
															<a target='_blank' style='display:none' class='filename' href=''></a><br>
															<input style='display:none' type='button' class='btn red delete-fileupload' value='Hapus'></input>
															
														</div>
														<div id="form_structure_organization_error"></div> 															
														<span class="help-block">
														Upload dokumen struktur organisasi </span>
													</div>
												</div>												
												
												<h4 class="form-section">Data Pelengkap</h4>
												
												<div class="form-group">
													<label class="control-label col-md-3">Surat Permohonan Terdaftar/Terakreditasi
													<span class="required">* </span>
													</label>
													<div class="col-md-4">
													
														<?php
														/*
														<div class="input-group">
															<input id="accreditation_upload_handler" type="file" name="files" data-url="<?=base_url()?>assets/upload/index.php">
															<input type='hidden' name='accreditation_upload' id='accreditation_upload' value=''>
				
														</div>
														*/
														?>
														<div class="input-group">
															<input class='fileupload' type="file" name='files' id="structure_organization_upload_handler" data-url="<?=base_url()?>assets/upload/index.php">
															<input class='output' id='accreditation_upload' type='hidden' value='' name='accreditation_upload' data-display='accreditation_upload' data-is_url=1>
															<label style='display:none' class='loading'>loading..</label>
															<a target='_blank' style='display:none' class='filename' href=''></a><br>
															<input style='display:none' type='button' class='btn red delete-fileupload' value='Hapus'></input>
															
														</div>
														<div id="form_surat_permohonan_error"></div>
														<span class="help-block">
														Upload Surat Permohonan Terdaftar/Terakreditasi </span>
													</div>
												</div>
												
												<div class="form-group">
													<label class="control-label col-md-3">Pernyataan Komitmen
													<span class="required">* </span>
													</label>
													<div class="col-md-4">
														
														<?php
														/*
														<div class="input-group">
															<input id="commitment_upload_handler" type="file" name="files" data-url="<?=base_url()?>assets/upload/index.php">
															<input type='hidden' name='commitment_upload' id='commitment_upload' value=''>
															</span>
														</div>
														*/
														?>
														
														<div class="input-group">
															<input class='fileupload' type="file" name='files' id="structure_organization_upload_handler" data-url="<?=base_url()?>assets/upload/index.php">
															<input class='output' id='commitment_upload' type='hidden' value='' name='commitment_upload' data-display='commitment_upload' data-is_url=1>
															<label style='display:none' class='loading'>loading..</label>
															<a target='_blank' style='display:none' class='filename' href=''></a><br>
															<input style='display:none' type='button' class='btn red delete-fileupload' value='Hapus'></input>
															
														</div>
														
														<div id="form_surat_pernyataan_error"></div>
														<span class="help-block">
														Klik <a target='_blank' href="<?=isset($example_doc['commitment_example']) && $example_doc['commitment_example'] != '' ? $example_doc['commitment_example'] : '#'?>">disini</a> untuk download contoh surat</span>
													</div>
												</div>

												<div class="form-group negeri box hide">
													<label class="control-label col-md-3">Apakah Memiliki Tugas Fungsi (TUPOKSI) Pelatihan? <span class="required">
													* </span>
													</label>
													<div class="col-md-4">
														<div class="radio-list">
															<label>
															<input type="radio" name="training_task_negeri" value="ya" data-title="Ya" checked="checked"/>
															Ya</label>
															<label>
															<input type="radio" name="training_task_negeri" value="tidak" data-title="Tidak"/>
															Tidak</label>
														</div>
														<div id="form_surat_tugas_negeri_error">
														</div>
													</div>
												</div>
												
												<div class="form-group box negeri">
													<label class="control-label col-md-3">Upload Bukti Dukung Memiliki Tugas Fungsi (TUPOKSI) Pelatihan
													<span class="required">* </span>
													</label>
													<div class="col-md-4">
														<div class="input-group">
								<input class='fileupload' type="file" name='files' id="structure_organization_upload_handler" data-url="<?=base_url()?>assets/upload/index.php">
								<input class='output' id='training_task_negeri_upload' type='hidden' value='' name='training_task_negeri_upload' data-display='training_task_negeri_upload' data-is_url=1>
															<label style='display:none' class='loading'>loading..</label>
															<a target='_blank' style='display:none' class='filename' href=''></a><br>
															<input style='display:none' type='button' class='btn red delete-fileupload' value='Hapus'></input>
															
														</div>
														<div id="surat_tugas_negeri_error"></div>
														<span class="help-block">
														Upload dokumen pendukung</span>
													</div>
												</div>												
												
												<div class="form-group swasta box hide">
													<label class="control-label col-md-3">Apakah Memiliki Surat Tugas Melaksanakan Pendidikan/Pelatihan? <span class="required">
													* </span>
													</label>
													<div class="col-md-4">
														<div class="radio-list">
															<label>
															<input type="radio" name="training_task_swasta" value="ya" data-title="Ya" checked="checked"/>
															Ya</label>
															<label>
															<input type="radio" name="training_task_swasta" value="tidak" data-title="Tidak"/>
															Tidak</label>
														</div>
														<div id="form_surat_tugas_swasta_error">
														</div>
													</div>
												</div>
												
												<div class="form-group box swasta">
													<?php /*sblmnya:Upload Dokumen Ijin Resmi */?>
													<label class="control-label col-md-3">Upload Surat Tugas Melaksanakan Pendidikan/Pelatihan
													<span class="required">* </span>
													</label>
													<div class="col-md-4">
														<div class="input-group">
															<input class='fileupload' type="file" name='files' id="structure_organization_upload_handler" data-url="<?=base_url()?>assets/upload/index.php">
															<input class='output' id='training_task_swasta_upload' type='hidden' value='' name='training_task_swasta_upload' data-display='training_task_swasta_upload' data-is_url=1>
															<label style='display:none' class='loading'>loading..</label>
															<a target='_blank' style='display:none' class='filename' href=''></a><br>
															<input style='display:none' type='button' class='btn red delete-fileupload' value='Hapus'></input>
															
														</div>
														<div id="surat_tugas_swasta_error"></div>
														<span class="help-block">
														Upload ijin resmi </span>
													</div>
												</div>
												
												<!-- tampilkan jika pegawai negeri -->
												<div class="form-group negeri box">
													<label class="control-label col-md-3">Persyaratan Keuangan
													<span class="required">* </span>
													</label>
													<div class="col-md-4">
														<?php
														/*
														<div class="input-group">
															<input id="financial_requirement_upload_handler" type="file" name="files" data-url="<?=base_url()?>assets/upload/index.php">
				<input type='hidden' name='financial_requirement_upload' id='financial_requirement_upload' value=''>
														</div>
														*/
														?>
														<div class="input-group">
															<input class='fileupload' type="file" name='files' id="structure_organization_upload_handler" data-url="<?=base_url()?>assets/upload/index.php">
															<input class='output' id='financial_requirement_upload' type='hidden' value='' name='financial_requirement_upload' data-display='financial_requirement_upload' data-is_url=1>
															<label style='display:none' class='loading'>loading..</label>
															<a target='_blank' style='display:none' class='filename' href=''></a><br>
															<input style='display:none' type='button' class='btn red delete-fileupload' value='Hapus'></input>
															
														</div>
														<div id="form_keuangan_pns_error"></div>
														<span class="help-block">
														Upload dokumen PNBP/BLU/DIPA</span>
													</div>
												</div>
												
												<!-- tampilkan jika swasta -->
												<div class="form-group swasta box">
													<label class="control-label col-md-3">Persyaratan Keuangan
													<span class="required">* </span>
													</label>
													<div class="col-md-4">
														<input type="text" class="form-control" name="financial_npwp_number" placeholder="contoh: 33423498938493434"/>
														<span class="help-block">
														Nomor NPWP</span>
													</div>
												</div>
												
												<div class="form-group swasta box">
													<label class="control-label col-md-3">&nbsp;</label>
													<div class="col-md-4">
														<?php
														/*
														<div class="input-group">
															<input id="financial_requirement_npwp_upload_handler" type="file" name="files" data-url="<?=base_url()?>assets/upload/index.php">
				<input type='hidden' name='financial_requirement_npwp_upload' id='financial_requirement_npwp_upload' value=''>
														</div>
														*/
														?>
														<div class="input-group">
															<input class='fileupload' type="file" name='files' id="structure_organization_upload_handler" data-url="<?=base_url()?>assets/upload/index.php">
															<input class='output' id='financial_requirement_npwp_upload' type='hidden' value='' name='financial_requirement_npwp_upload' data-display='financial_requirement_npwp_upload' data-is_url=1>
															<label style='display:none' class='loading'>loading..</label>
															<a target='_blank' style='display:none' class='filename' href=''></a><br>
															<input style='display:none' type='button' class='btn red delete-fileupload' value='Hapus'></input>
															
														</div>
														<div id="form_keuangan_swasta_error"></div>
														<span class="help-block">
														Upload NPWP</span>
													</div>
												</div>
												
												<div class="form-group">
													<label class="control-label col-md-3">Sistem Manajemen Mutu
													<span class="required">* </span>
													</label>
													<div class="col-md-4">
														<?php
														/*
														<div class="input-group">
															<input id="quality_management_upload_handler" type="file" name="files" data-url="<?=base_url()?>assets/upload/index.php">
				<input type='hidden' name='quality_management_upload' id='quality_management_upload' value=''>
														</div>
														*/
														?>
														<div class="input-group">
															<input class='fileupload' type="file" name='files' id="structure_organization_upload_handler" data-url="<?=base_url()?>assets/upload/index.php">
															<input class='output' id='quality_management_upload' type='hidden' value='' name='quality_management_upload' data-display='quality_management_upload' data-is_url=1>
															<label style='display:none' class='loading'>loading..</label>
															<a target='_blank' style='display:none' class='filename' href=''></a><br>
															<input style='display:none' type='button' class='btn red delete-fileupload' value='Hapus'></input>
															
														</div>
														
														<div id="form_manajemen_mutu_error"></div>
														<span class="help-block">
															Klik <a target='_blank' href="<?=isset($example_doc['quality_management_example']) && $example_doc['quality_management_example'] != '' ? $example_doc['quality_management_example'] : '#'?>">disini</a> untuk download contoh dokument
														</span>
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">Standar Pelatihan
													<span class="required">* </span>
													</label>
													<div class="col-md-4">
														<?php
														/*
														<div class="input-group">
															<input id="standart_training_upload_handler" type="file" name="files" data-url="<?=base_url()?>assets/upload/index.php">
				<input type='hidden' name='standart_training_upload' id='standart_training_upload' value=''>
														</div>
														*/
														?>
														<div class="input-group">
															<input class='fileupload' type="file" name='files' id="structure_organization_upload_handler" data-url="<?=base_url()?>assets/upload/index.php">
															<input class='output' id='standart_training_upload' type='hidden' value='' name='standart_training_upload' data-display='standart_training_upload' data-is_url=1>
															<label style='display:none' class='loading'>loading..</label>
															<a target='_blank' style='display:none' class='filename' href=''></a><br>
															<input style='display:none' type='button' class='btn red delete-fileupload' value='Hapus'></input>
															
														</div>
														<div id="form_standart_training_error"></div>
														<span class="help-block">
															Klik <a target='_blank' href="<?=isset($example_doc['standart_training_example']) && $example_doc['standart_training_example'] != '' ? $example_doc['standart_training_example'] : '#'?>">disini</a> untuk download contoh dokument
														</span>
													</div>
												</div>												
											</div>
											<div class="tab-pane" id="tab3">
												<h3 class="block">Konfirmasi data Lembaga Pelatihan</h3>
												<?php $this->load->view('pages/public/confim_data_basic')?>
												
												<!-- detail data LPP -->
												<h4 class="form-section">Data Lembaga</h4>
												<div class="form-group">
													<label class="control-label col-md-3">Nama Lembaga:</label>
													<div class="col-md-4">
														<p class="form-control-static" data-display="training_institute">
														</p>
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">Status Lembaga Pelatihan:</label>
													<div class="col-md-4">
														<p class="form-control-static" data-display="training_institute_status">
														</p>
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">Alamat:</label>
													<div class="col-md-4">
														<p class="form-control-static" data-display="office_address">
														</p>
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">Kode Pos:</label>
													<div class="col-md-4">
														<p class="form-control-static" data-display="office_zip_code">
														</p>
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">Provinsi:</label>
													<div class="col-md-4">
														<p class="form-control-static" data-display="office_province">
														</p>
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">Kab / kota:</label>
													<div class="col-md-4">
														<p class="form-control-static" data-display="office_district">
														</p>
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">No.Telp:</label>
													<div class="col-md-4">
														<p class="form-control-static" data-display="office_phone">
														</p>
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">No.Fax:</label>
													<div class="col-md-4">
														<p class="form-control-static" data-display="office_fax">
														</p>
													</div>
												</div>
												
												<h4 class="form-section">Penanggung Jawab(Kepala Lembaga Pelatihan)</h4>
												
												<div class="form-group">
													<label class="control-label col-md-3">Nama Penanggung Jawab:</label>
													<div class="col-md-4">
														<p class="form-control-static" data-display="responsible_person_name">
														</p>
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">Jabatan Penanggung Jawab:</label>
													<div class="col-md-4">
														<p class="form-control-static" data-display="responsible_person_position">
														</p>
													</div>
												</div>
												<div class="form-group negeri box">
													<label class="control-label col-md-3">NIP Penanggung Jawab:</label>
													<div class="col-md-4">
														<p class="form-control-static" data-display="responsible_person_nip">
														</p>
													</div>
												</div>
												
												<div class="form-group swasta box">
													<label class="control-label col-md-3">No.KTP Penanggung Jawab:</label>
													<div class="col-md-4">
														<p class="form-control-static" data-display="responsible_person_ktp">
														</p>
													</div>
												</div>
												
												<div class="form-group swasta box">
													<label class="control-label col-md-3">KTP Penanggung Jawab:</label>
													<div class="col-md-4">
														<p class="form-control-static" data-display="responsible_person_ktp_upload">
														</p>
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">Struktur Organisasi:</label>
													<div class="col-md-4">
														<p class="form-control-static" data-display="structure_organization_upload">
														</p>
													</div>
												</div>												
												
												<h4 class="form-section">Data Pelengkap</h4>
												<div class="form-group">
													<label class="control-label col-md-3">Surat Permohonan Terdaftar/Terakreditasi:</label>
													<div class="col-md-4">
														<p class="form-control-static" data-display="accreditation_upload">
														</p>
													</div>
												</div>
												
												<div class="form-group">
													<label class="control-label col-md-3">Surat Pernyataan Komitmen Terdaftar/Terakreditasi:</label>
													<div class="col-md-4">
														<p class="form-control-static" data-display="commitment_upload">
														</p>
													</div>
												</div>
												
												<?php
												/*
												<div class="form-group">
													<label class="control-label col-md-3">Apakah Memiliki Surat Tugas Pelatihan?:</label>
													<div class="col-md-4">
														<p class="form-control-static negeri box" data-display="training_task_negeri">
														<p class="form-control-static swasta box" data-display="training_task_swasta">															
														</p>
													</div>
												</div>
												*/
												?>
												
												<div class="form-group negeri box">
													<label class="control-label col-md-3">Apakah Memiliki Tugas Fungsi (TUPOKSI) Pelatihan?:</label>
													<div class="col-md-4">
							    <p class="form-control-static" data-display="training_task_negeri"></p>
							    															
														
													</div>
												</div>
												
												<div class="form-group ya_negeri box negeri">
													<label class="control-label col-md-3">Bukti Dukung Memiliki Tugas Fungsi (TUPOKSI) Pelatihan</label>
													<div class="col-md-4">
														<p class="form-control-static" id="text_training_task" data-display="training_task_negeri_upload">
														</p>
													</div>
												</div>
												
												<div class="form-group swasta box">
													<label class="control-label col-md-3">Apakah Memiliki Surat Tugas Melaksanakan Pendidikan/Pelatihan?:</label>
													<div class="col-md-4">
							    <p class="form-control-static" data-display="training_task_swasta">
							    															
														
													</div>
												</div>
												
												<div class="form-group ya_swasta box swasta">
													<label class="control-label col-md-3">Upload Dokumen Ijin Resmi</label>
													<div class="col-md-4">
														<p class="form-control-static" id="text_training_task" data-display="training_task_swasta_upload">
														</p>
													</div>
												</div>
												<!-- negeri -->
												<div class="form-group negeri box">
													<label class="control-label col-md-3">Persyaratan Keuangan:</label>
													<div class="col-md-4">
														<p class="form-control-static" data-display="financial_requirement_upload">
														</p>
													</div>
												</div>
												
												<!-- swasta -->
												<div class="form-group swasta box">
													<label class="control-label col-md-3">Persyaratan Keuangan:</label>
													<div class="col-md-4">
														<p class="form-control-static" data-display="financial_npwp_number">
														</p>
													</div>
												</div>												
												<div class="form-group swasta box">
													<label class="control-label col-md-3"><!--Persyaratan Keuangan:--></label>
													<div class="col-md-4">
														<p class="form-control-static" data-display="financial_requirement_npwp_upload">
														</p>
													</div>
												</div>
												
												<div class="form-group">
													<label class="control-label col-md-3">Sistem Manajemen Mutu:</label>
													<div class="col-md-4">
														<p class="form-control-static" data-display="quality_management_upload">
														</p>
													</div>
												</div>
												
												<div class="form-group">
													<label class="control-label col-md-3">Standar Pelatihan:</label>
													<div class="col-md-4">
														<p class="form-control-static" data-display="standart_training_upload">
														</p>
													</div>
												</div>												
												
											</div>
										</div>
									</div>
									<div class="form-actions">
										<div class="row">
											<div class="col-md-offset-3 col-md-9">
												<a href="javascript:;" class="btn default button-previous">
												<i class="m-icon-swapleft"></i> Kembali </a>
												<a href="javascript:;" class="btn blue button-next">
												Lanjut <i class="m-icon-swapright m-icon-white"></i>
												</a>
												<a href="javascript:;" class="btn green button-submit">
												Proses <i class="m-icon-swapright m-icon-white"></i>
												</a>
											</div>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
			<!-- END PAGE CONTENT INNER -->
		</div>
	</div>
	<!-- END PAGE CONTENT -->
</div>
<!-- END PAGE CONTAINER -->