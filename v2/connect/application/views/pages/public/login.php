
<!-- BEGIN BODY -->
<body class="login">
    <!-- BEGIN SIDEBAR TOGGLER BUTTON -->

    <div class="menu-toggler sidebar-toggler"></div>
    <!-- END SIDEBAR TOGGLER BUTTON -->
	
    <!-- BEGIN LOGO -->
    <div class="logo">
        <a href=""><img alt="" src="<?php echo site_url();?>assets/images/lkpp.png"></a>
    </div><!-- END LOGO -->
	
    <!-- BEGIN LOGIN -->
    <div class="content">
        <!-- BEGIN LOGIN FORM -->

        <form action="" class="login-form" method="post">
            <h3 class="form-title">Login</h3>
			<?php		
			if($this->session->userdata('message')){
			?>
            <div class="alert alert-danger">
                <button class="close" data-close="alert"></button> <span><?=$this->session->userdata('message');?></span>
            </div>
			<?php
			$this->session->unset_userdata('message');
		 	}else{ ?>
            <div class="alert alert-danger display-hide">
                <button class="close" data-close="alert"></button> <span>Masukan Username dan Password anda.</span>
            </div>		 		
			<?php }?>
	    <?php
	    $notif=$this->session->userdata('notif');
	    $this->session->unset_userdata('notif');
	    if($notif):
	    ?>
	    <div class="alert <?=($notif['status'] ? 'alert-success' : 'alert-danger')?>">
                <button class="close" data-close="alert"></button>
		    <p><?=$notif['msg']?></p>
            </div>
            <?php endif;?>
            <div class="form-group">
                <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                <label class=
                "control-label visible-ie8 visible-ie9">Username</label>
                <input autocomplete="off" class="form-control placeholder-no-fix" name=
                "username" placeholder="Username" type="text">
            </div>

            <div class="form-group">
                <label class=
                "control-label visible-ie8 visible-ie9">Password</label>
                <input autocomplete="off" class="form-control placeholder-no-fix" name="password" placeholder="Password" type="password">
            </div>

            <div class="form-actions">
                <button class="btn btn-success uppercase" type="submit">Login</button> 
				<a class="forget-password" href="javascript:;" id="help-trigger">Bantuan?</a>
            </div>

			<br />
			<div class="login-options" style="margin-bottom: 20px">
				<h4>Kembali ke</h4>
				
				<ul class="back-to">
					<li><a href="<?=$this->config->item('simpel_url')?>">SIMPEL</a></li>
					<li><span class="sep"></span></li>
					<li><a href="<?=$this->config->item('lrc_url')?>">LRC</a></li>
					<li><span class="sep"></span></li>
					<li><a href="<?=$this->config->item('elearning_url')?>">e-Learning</a></li>
				</ul>
			</div>
			
			
            <div class="create-account">
                <p>
					<a href="javascript:;" id="register-btn" href="" class="uppercase">Registrasi</a>
				</p>
            </div>
        </form><!-- END LOGIN FORM -->
		
        <!-- BEGIN FORGOT PASSWORD FORM -->
        <form action="<?=site_url('login/forgot_password_post')?>" class="forget-form" method="post">
            <h3>Lupa Password ?</h3>
            <p>Masukan Email kamu di bawah ini.</p>
            <div class="form-group">
                <input autocomplete="off" class=
                "form-control placeholder-no-fix" name="email" placeholder=
                "Email" type="text">
            </div>
			
            <div class="form-actions">
                <button class="btn btn-default" id="back-btn" type="button">Kembali</button> 
				<button class="btn btn-success uppercase pull-right" type="submit">Proses</button>
            </div>
        </form><!-- END FORGOT PASSWORD FORM -->
		
		
		<!-- BEGIN REGISTRATION FORM -->
		<form class="register-form" action="" method="post">
			<h3>Register sebagai?</h3>
			
			<div class="form-group up-dikit">
				<button type="submit" id="register-peserta-submit-btn" class="btn btn-success uppercase btn-block btn-register-as" value="<?=site_url('register/peserta');?>">Peserta</button>
			</div>
			<div class="form-group">
				<button type="submit" id="register-lpp-submit-btn" class="btn btn-success uppercase btn-block btn-register-as" value="<?=site_url('register/lpp');?>">Lembaga pelatihan</button>
			</div>
			
			<div class="form-actions">
				<button type="button" id="register-back-btn" class="btn btn-default">Kembali</button>
			</div>
		</form>
		<!-- END REGISTRATION FORM -->		


		<!-- BEGIN HELP CONTAINER -->
		<div class="help-form" action="" method="post">
			<h3>Pilih Bantuan?</h3>
			
			<div class="form-group up-dikit">
				<a href="javascript:;" class="btn btn-success uppercase btn-block" id="forget-password">Lupa password?</a>
			</div>
			<div class="form-group">
				<a href="javascript:;" id="activate" class="btn btn-success uppercase btn-block" value="">Kirim ulang aktifasi email</a>
			</div>
			
			<div class="form-actions">
				<button type="button" id="help-back-btn" class="btn btn-default">Kembali</button>
			</div>
		</div>
		<!-- END HELP CONTAINER -->		
		
		
        <!-- BEGIN ACTIVATE FORM -->
        <form action="<?=site_url('login/activated_email_post')?>" class="activate-form" method="post">
            <h3>Aktifasi User</h3>
            <p>Masukan Email kamu di bawah ini.</p>
            <div class="form-group">
                <input autocomplete="off" class=
                "form-control placeholder-no-fix" name="email" placeholder=
                "Email" type="text">
            </div>
			
            <div class="form-actions">
                <button class="btn btn-default" id="activate-back-btn" type="button">Kembali</button> 
				<button class="btn btn-success uppercase pull-right" type="submit">Proses</button>
            </div>
        </form><!-- END ACTIVATE FORM -->
    </div>			