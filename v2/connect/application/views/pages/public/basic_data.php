<h4 class="form-section">Data Pengguna</h4>
<div class="form-group">
	<label class="control-label col-md-3">Foto</label>
	<div class="col-md-9">
		<div class="fileinput fileinput-new" data-provides="fileinput">
			<div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
				<img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt=""/>
			</div>
			<div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;">
			</div>
			<div>
				<span class="btn default btn-file">
				<span class="fileinput-new">
				Pilih foto </span>
				<span class="fileinput-exists">
				Ganti </span>
				<input id="photo_upload" type="file" name="files" data-url="<?=base_url()?>assets/upload/index.php">
				<input type='hidden' name='photo' id='photo' value='' data-is_img=1>
				</span>
				<a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput">
				Hapus </a>
			</div>
		</div>
	</div>
</div>
<div class="form-group">
	<label class="control-label col-md-3">Username <span class="required">
	* </span>
	</label>
	<div class="col-md-4">
		
		<input type="text" class="form-control" name="username" placeholder="cahyogumilang"/>
		<span class="help-block">
		Isi username </span>
	</div>
</div>
<div class="form-group">
	<label class="control-label col-md-3">Password <span class="required">
	* </span>
	</label>
	<div class="col-md-4">
		<input type="password" class="form-control" name="password" id="submit_form_password"/>
		<span class="help-block">
		Isi kata sandi </span>
	</div>
</div>
<div class="form-group">
	<label class="control-label col-md-3">Konfirmasi Password <span class="required">
	* </span>
	</label>
	<div class="col-md-4">
		<input type="password" class="form-control" name="rpassword"/>
		<span class="help-block">
		Konfirmasi kata sandi </span>
	</div>
</div>
<div class="form-group">
	<label class="control-label col-md-3">Email <span class="required">
	* </span>
	</label>
	<div class="col-md-4">
		<input type="text" class="form-control" name="email" placeholder="contoh: cahyo@gmail.com" id="submit_form_email"/>
		<span class="help-block">
		Isi email </span>
	</div>
</div>
<div class="form-group">
	<label class="control-label col-md-3">Konfirmasi Email <span class="required">
	* </span>
	</label>
	<div class="col-md-4">
		<input type="text" class="form-control" name="remail"/>
		<span class="help-block">
		Konfirmasi email </span>
	</div>
</div>
<div class="form-group">
	<label class="control-label col-md-3">Nama
		<span class="required">*</span>
	</label>
	<div class="col-md-4">
		<input type="text" class="form-control" name="fullname" placeholder="contoh: Cahyo Gumilang"/>
		<span class="help-block">Isi nama lengkap</span>
	</div>
</div>
<div class="form-group">
	<label class="control-label col-md-3">Tempat Lahir
		 <span class="required">*</span>
	</label>
	<div class="col-md-4">
		<input type="text" class="form-control" name="birth_place" id="submit_tempat_lahir" placeholder="contoh: Blitar"/>
		<span class="help-block">
		Isi Tempat Lahir </span>
	</div>
</div>
<div class="form-group">
	<label class="control-label col-md-3">Tanggal Lahir
		<span class="required">*</span>
	</label>
	<div class="col-md-3">
		<div class="input-group input-medium date date-picker tanggal-wizard" data-date-format="yyyy-mm-dd">
			<input type="text" class="form-control" name="birth_date" id="birth_date" placeholder="contoh: 1945-12-20">
			<span class="input-group-btn">
			<button class="btn default" type="button">
				<i class="fa fa-calendar"></i></button>
			</span>
		</div>
		<div id="form_tgl_lahir_error">
		</div>
		<span class="help-block">
		Pilih Tanggal Lahir </span>
	</div>
</div>
<div class="form-group">
	<label class="control-label col-md-3">No. Telp/HP
		<span class="required">*</span>
	</label>
	<div class="col-md-4">
		<input type="text" class="form-control" name="phone" placeholder="contoh: 081279221166"/>
		<span class="help-block">
		Isi no.Telp </span>
	</div>
</div>
<div class="form-group">
	<label class="control-label col-md-3">Jenis Kelamin <span class="required">
	* </span>
	</label>
	<div class="col-md-4">
		<div class="radio-list">
			<label>
			<input type="radio" name="gender" value="M" data-title="Laki-laki"/>
			Laki-laki </label>
			<label>
			<input type="radio" name="gender" value="F" data-title="Perempuan"/>
			Perempuan </label>
		</div>
		<div id="form_gender_error">
		</div>
	</div>
		
</div>