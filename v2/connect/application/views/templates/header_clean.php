<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->

<html lang="en">
<!--<![endif]-->

<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8">

    <title><?php if(isset($title)) echo $title;?></title>
    <meta content="IE=edge" http-equiv="X-UA-Compatible">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="text/html; charset=utf-8" http-equiv="Content-type">
    <meta content="" name="description">
    <meta content="" name="author"><!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css">
    <link href="<?php echo site_url();?>assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo site_url();?>assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo site_url();?>assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo site_url();?>assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css">
	<!-- END GLOBAL MANDATORY STYLES -->

	<!-- BEGIN PAGE LEVEL STYLES -->
    <link href="<?php echo site_url();?>assets/admin/pages/css/login.css" rel="stylesheet" type="text/css">
	<!-- END PAGE LEVEL SCRIPTS -->
    
	<!-- BEGIN THEME STYLES -->
    <link href="<?php echo site_url();?>assets/global/css/components-rounded.css" id="style_components" rel="stylesheet" type="text/css">
    <link href="<?php echo site_url();?>assets/global/css/plugins.css" rel="stylesheet" type="text/css">
    <link href="<?php echo site_url();?>assets/admin/layout3/css/layout.css" rel="stylesheet" type="text/css">
    <link href="<?php echo site_url();?>assets/admin/layout3/css/themes/default.css" id="style_color" rel="stylesheet" type="text/css">
    <link href="<?php echo site_url();?>assets/admin/layout3/css/custom.css" rel="stylesheet" type="text/css">
	<!-- END THEME STYLES -->
	
	
   
    
	<link href="<?php echo site_url();?>assets/images/favicon.ico" rel="shortcut icon">
</head><!-- END HEAD -->
