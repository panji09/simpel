    <!-- BEGIN FOOTER -->
    <div class="page-footer">
        <div class="container">
            <?=$this->config->item('title_footer')?>
        </div>
    </div>

	<div class="scroll-to-top">
		<i class="icon-arrow-up"></i>
	</div>
	
    <!-- END FOOTER -->
    <!-- BEGIN JAVASCRIPTS (Load javascripts at bottom, this will reduce page load time) -->
    <!-- BEGIN CORE PLUGINS -->
    <!--[if lt IE 9]>
		<script src="assets/global/plugins/respond.min.js"></script>
		<script src="assets/global/plugins/excanvas.min.js"></script> 
	<![endif]-->
    <script src="<?php echo site_url();?>assets/global/plugins/jquery.min.js" type="text/javascript"></script> 
	<script src="<?php echo site_url();?>assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script> 
	<!-- IMPORTANT! Load jquery-ui.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
    <script src="<?php echo site_url();?>assets/global/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script> 
	<script src="<?php echo site_url();?>assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script> 
	<script src="<?php echo site_url();?>assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script> 
	<script src="<?php echo site_url();?>assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script> 
	<script src="<?php echo site_url();?>assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script> 
	<script src="<?php echo site_url();?>assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script> 
	<script src="<?php echo site_url();?>assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script> 
	<!-- END CORE PLUGINS -->
    
	<!-- BEGIN PAGE LEVEL PLUGINS -->
	<script type="text/javascript" src="<?php echo site_url();?>assets/global/plugins/jquery-validation/js/jquery.validate.min.js"></script>
	<script type="text/javascript" src="<?php echo site_url();?>assets/global/plugins/jquery-validation/js/additional-methods.min.js"></script>
	<script type="text/javascript" src="<?php echo site_url();?>assets/global/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>

	<script type="text/javascript" src="<?php echo site_url();?>assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
	<script type="text/javascript" src="<?php echo site_url();?>assets/global/plugins/select2/select2.min.js"></script>
	<script type="text/javascript" src="<?php echo site_url();?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js"></script>
	<script type="text/javascript" src="<?php echo site_url();?>assets/global/plugins/holder.js"></script>
	
	<!-- END PAGE LEVEL PLUGINS -->
		
	<!-- BEGIN PAGE LEVEL SCRIPTS -->
	<script src="<?php echo site_url();?>assets/global/scripts/metronic.js" type="text/javascript"></script>
	<script src="<?php echo site_url();?>assets/admin/layout3/scripts/layout.js" type="text/javascript"></script>
	<script src="<?php echo site_url();?>assets/admin/layout3/scripts/demo.js" type="text/javascript"></script>
	<script src="<?php echo site_url();?>assets/admin/pages/scripts/components-pickers.js"></script>
	<script src="<?php echo site_url();?>assets/admin/pages/scripts/components-form-tools.js"></script>
	<script src="<?php echo site_url();?>assets/global/plugins/bootbox/bootbox.min.js" type="text/javascript"></script>
	<!-- The main application script -->
	<!-- The XDomainRequest Transport is included for cross-domain file deletion for IE 8 and IE 9 -->
	<!--[if (gte IE 8)&(lt IE 10)]>
	    <script src="<?php echo site_url();?>assets/global/plugins/jquery-file-upload/js/cors/jquery.xdr-transport.js"></script>
	    <![endif]-->
	<!-- get javascript by pages -->
	<?php if(isset($js)){
		foreach($js as $javascript => $function){?>
			<script src="<?php echo $javascript;?>"></script>
			<?php				
			}
		}?>
	<!-- END PAGE LEVEL SCRIPTS -->
	
	
	
	<script>
		var URL_API = '<?=$this->config->item('api_url')?>';
		var URL_BASE = '<?php echo site_url();?>';
		var PATH_PLUGIN = '<?=$this->config->item('plugin')?>'
		$(document).ready(function() {
			
			//register callback each 
			<?php if(isset($js)){
				foreach($js as $javascript => $function){
					echo $function."\n";
				}
			}?>		
		    Metronic.init(); // init metronic core components
		    Layout.init(); // init current layout
		    Demo.init(); // init demo features
		    ComponentsPickers.init();

		});
		
		$(document).ready(function(){
		    function openKCFinderImages(div) {
			window.KCFinder = {
			    callBack: function(url) {
				window.KCFinder = null;
				URL = url;
// 				console.log(URL);
				$('#photo_img').attr('src',URL);
				$('#photo_img').next().val(URL);
// 				$(div).parent().parent().siblings('.view_info').children('a').attr('href',URL);
			    }
			};
			window.open(PATH_PLUGIN+'/kcfinder/browse.php?type=images',
			    'kcfinder_image', 'status=0, toolbar=0, location=0, menubar=0, ' +
			    'directories=0, resizable=1, scrollbars=0, width=800, height=600'
			);
		    }
		    function openKCFinderFiles(div) {
			window.KCFinder = {
			    callBack: function(url) {
				window.KCFinder = null;
				$(div).siblings('input').val(url);
			    }
			};
			window.open(PATH_PLUGIN+'/kcfinder/browse.php?type=files',
			    'kcfinder_file', 'status=0, toolbar=0, location=0, menubar=0, ' +
			    'directories=0, resizable=1, scrollbars=0, width=800, height=600'
			);
		    }
		    
		    $('#remove_pic').click(function(){
			$('#photo_img').attr('src','http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image');
			$('#photo_img').next().val('');
		    });
		    $('#choose_pic').click(function(){
			openKCFinderImages(this);
		    });
		    $('.change_cover').click(function(){
			openKCFinderImages(this);
			
		    });
		    $('.browse_files').click(function(){
			openKCFinderFiles(this);
		    });
		
		    $('#modal_verified_institute').on('show.bs.modal', function (event) {
			var button = $(event.relatedTarget) // Button that triggered the modal
			var url = button.data('url') // Extract info from data-* attributes
			// If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
			// Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
			var modal = $(this)
			modal.find('#link_verified_institute').attr('href',url);
		    })
		});
		jQuery.extend(jQuery.validator.messages, {
		    required: "Lengkapi kotak isian",
		   
		});
		
	</script>
	
	
	
	<!-- END JAVASCRIPTS -->
    <!-- END BODY -->
</body>
</html>