<!DOCTYPE html>	
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->

<html class="no-js" lang="en">
<!--<![endif]-->

<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8">
    <title><?php if(isset($title)) echo $title;?></title>
    <meta content="IE=edge" http-equiv="X-UA-Compatible">
    <meta content="width=device-width, initial-scale=1" name="viewport">
    <meta content="" name="description">
    <meta content="" name="author">
	
	<!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css">
    <link href="<?php echo site_url();?>assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo site_url();?>assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo site_url();?>assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo site_url();?>assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css">
	<!-- END GLOBAL MANDATORY STYLES -->
    	
    <!-- BEGIN PAGE LEVEL STYLES -->
    <link rel="stylesheet" type="text/css" href="<?php echo site_url();?>assets/global/plugins/select2/select2.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo site_url();?>assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css"/>
    <link href="<?php echo site_url();?>assets/admin/pages/css/tasks.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="<?php echo site_url();?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css"/>

    <link href="<?php echo site_url();?>assets/global/plugins/jquery-file-upload/blueimp-gallery/blueimp-gallery.min.css" rel="stylesheet"/>
    <link href="<?php echo site_url();?>assets/global/plugins/jquery-file-upload/css/jquery.fileupload.css" rel="stylesheet"/>
    <link href="<?php echo site_url();?>assets/global/plugins/jquery-file-upload/css/jquery.fileupload-ui.css" rel="stylesheet"/>
    <!-- END PAGE LEVEL SCRIPTS -->
    
   
	<link rel="stylesheet" type="text/css" href="<?php echo site_url();?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css"/>
	 <!--load css-->
	<?php if(isset($css)){
		foreach($css as $style){?>
			<link rel="stylesheet" type="text/css" href="<?php echo $style;?>" >
			<?php				
			}
		}?>
	<!--./ load css-->
	<!-- END PAGE LEVEL SCRIPTS -->
		
	<!-- BEGIN THEME STYLES -->
    <!-- DOC: To use 'rounded corners' style just load 'components-rounded.css' stylesheet instead of 'components.css' in the below style tag -->
    <link href="<?php echo site_url();?>assets/global/css/components-rounded.css" id="style_components" rel="stylesheet" type="text/css">
    <link href="<?php echo site_url();?>assets/global/css/plugins.css" rel="stylesheet" type="text/css">
    <link href="<?php echo site_url();?>assets/admin/layout3/css/layout.css" rel="stylesheet" type="text/css">
    <link href="<?php echo site_url();?>assets/admin/layout3/css/themes/default.css" id="style_color" rel="stylesheet" type="text/css">
    <link href="<?php echo site_url();?>assets/admin/layout3/css/custom.css" rel="stylesheet" type="text/css">
	<!-- END THEME STYLES -->
    <link href="<?php echo site_url();?>assets/images/favicon.ico" rel="shortcut icon">
</head>
<!-- END HEAD -->

<!-- BEGIN BODY -->
<!-- DOC: Apply "page-header-menu-fixed" class to set the mega menu fixed  -->
<!-- DOC: Apply "page-header-top-fixed" class to set the top menu fixed  -->
<body class ="page-header-top-fixed">
    <!-- BEGIN HEADER -->

    <div class="page-header">
        <!-- BEGIN HEADER TOP -->
        <div class="page-header-top">
            <div class="container">
                <!-- BEGIN LOGO -->
                <div class="page-logo">
                    <a href="<?php echo site_url();?>"><img alt="logo" class="logo-default"
                    src="<?php echo site_url();?>assets/images/lkpp.png"></a>
                </div><!-- END LOGO -->
				
                <!-- BEGIN RESPONSIVE MENU TOGGLER -->
                <a class="menu-toggler" href="javascript:;"></a> 
                <!-- END RESPONSIVE MENU TOGGLER -->
				
                <!-- BEGIN TOP NAVIGATION MENU -->
                <div class="top-menu">
                    <ul class="nav navbar-nav pull-right">
						<li class="droddown">
							<a target='_blank' href="<?=$this->config->item('lkpp_url')?>" >LKPP</a>
						</li>
						
						<li class="dropdown">
							<a target='_blank' href="<?=$this->config->item('simpel_url')?>" >Simpel</a>
						</li>
						
						<li class="dropdown">
							<a target='_blank' href="<?=$this->config->item('elearning_url')?>" >E-learning</a>
						</li>
						
						<li class="dropdown dropdown-separator">
							<span class="separator"></span>
						</li>
						<?php if(isset($user_nav['username'])){?>
                        <!-- BEGIN USER LOGIN DROPDOWN -->
                        <li class="dropdown dropdown-user dropdown-dark">
                            <a class="dropdown-toggle" href="">
							<img alt="" class="img-circle" src="<?php if(isset($user_nav['photo'])) echo $user_nav['photo'];?>">
                            <span class="username username-hide-mobile"><?php if(isset($user_nav['username'])) echo $user_nav['username'];?></span></a>

                        </li><!-- END USER LOGIN DROPDOWN -->												
							<?php }else{ ?>						
						<li class="dropdown">
							<a href="<?php echo site_url();?>" >Login</a>
						</li>												
							<?php }?>

                    </ul>
                </div><!-- END TOP NAVIGATION MENU -->
            </div>
        </div><!-- END HEADER TOP -->
    </div><!-- END HEADER -->