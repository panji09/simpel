<?php
    $config['title'] = 'LKPP Connect';
    $config['title_short'] = 'LKPP Connect';
    $config['title_long'] = 'Sistem Informasi Manajemen Pelatihan';
    $config['title_footer'] = 'Dikembangkan oleh Direktorat Pelatihan Kompetensi Deputi PPSDM <br /> Direktorat Pelatihan Kompetensi. &copy; Copyright '.(date('Y')==2015 ? date('Y') : '2015 - '.date('Y')).' LKPP';
?>