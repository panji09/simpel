<?php
    $config['base_url'] = $this->config['base_url'];
    $CI =& get_instance();
    
	
    
	if(in_array(strtolower($_SERVER['SERVER_NAME']), array('simpel.lkpp.site','simpel.ruangpanji.com'))){
		//local & devel
		$config['lkpp_url'] = 'http://www.lkpp.go.id/';
		$config['simpel_url'] = 'http://simpel.ruangpanji.com/v2';
		$config['simpel_api_url'] = 'http://simpel.ruangpanji.com/v2/index.php/api';
		$config['elearning_url'] = 'http://lms.ruangpanji.com/';
		$config['lrc_url'] = 'http://lrc.ruangpanji.com/';
		
		$config['confirmation_link'] = 'http://simpel.ruangpanji.com/v2/connect/index.php/oauth/confirmation/';
		$config['image_url'] = 'http://simpel.ruangpanji.com/v2/connect';
		$config['simple_avatar'] = 'http://simpel.ruangpanji.com/v2/connect/assets/admin/pages/media/profile/avatar_user.png';
		
	}else{
		//master
		$config['lkpp_url'] = 'http://www.lkpp.go.id/';
		$config['simpel_url'] = 'http://simpel.lkpp.go.id';
		$config['simpel_api_url'] = 'http://simpel.lkpp.go.id/index.php/api';
		$config['elearning_url'] = 'http://e-learning.lkpp.go.id/';
		$config['lrc_url'] = 'http://lrc.lkpp.go.id/';
		
		$config['confirmation_link'] = 'http://simpel.lkpp.go.id/connect/index.php/oauth/confirmation/';
		$config['image_url'] = 'http://simpel.lkpp.go.id/connect';
		$config['simple_avatar'] = 'http://simpel.lkpp.go.id/connect/assets/admin/pages/media/profile/avatar_user.png';
		
	}
	
	$config['api_url'] = $config['base_url'].'/api';
	$config['connect_url'] = $config['base_url'];
    
	$config['users'] = $config['base_url']."/assets/users";
    $config['upload'] = $config['base_url']."/assets/upload";
    $config['plugin'] = $config['base_url']."/assets/global/plugins";
    $config['img'] = $config['base_url']."/assets/images";
    
	
    // $config['base_url'] = $this->config['base_url'];
    // $config['plugin'] = $config['base_url']."/media/plugin";
    // $config['upload'] = $config['base_url']."/media/upload";
?>
