<?php

	//whether username and password is required to access mongodb
	$config['auth'] = false;

	//enter your mongodb user name
	$config['user'] = '';
	
	//mongodb password
	$config['password'] = '';
	
	//mongodb database name
	$config['db_name'] = 'db_lkpp_connect';
	
	//ip address where mongodb is hosted
	$config['host'] = 'localhost';
	
	//the port being used by mongodb (default 27017)
	$config['port'] = '27017';
	

?>