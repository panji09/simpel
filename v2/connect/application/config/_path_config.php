<?php
    $config['base_url'] = $this->config['base_url'];
    $CI =& get_instance();
    if($CI->input->ip_address() == '127.0.0.1'){
	$config['api_url'] = $config['base_url'].'/api';
	$config['connect_url'] = $config['base_url'];
    }else{
	$config['api_url'] = 'http://simpel.lkpp.go.id/connect/index.php/api';
	$config['connect_url'] = 'http://simpel.lkpp.go.id/connect';
    }
    
    $config['lkpp_url'] = 'http://www.lkpp.go.id/';
    $config['simpel_url'] = 'http://simpel.lkpp.go.id';
    $config['simpel_api_url'] = 'http://simpel.lkpp.go.id/index.php/api';
    
    $config['elearning_url'] = 'http://pustakakata.com/';
    
    $config['confirmation_link'] = 'http://simpel.lkpp.go.id/connect/index.php/oauth/confirmation/';
    $config['image_url'] = 'http://simpel.lkpp.go.id/connect';
    $config['simple_avatar'] = 'http://simpel.lkpp.go.id/connect/assets/admin/pages/media/profile/avatar_user.png';
    
    $config['users'] = $config['base_url']."/assets/users";
    $config['upload'] = $config['base_url']."/assets/upload";
    $config['plugin'] = $config['base_url']."/assets/global/plugins";
    $config['img'] = $config['base_url']."/assets/images";
    
	
    // $config['base_url'] = $this->config['base_url'];
    // $config['plugin'] = $config['base_url']."/media/plugin";
    // $config['upload'] = $config['base_url']."/media/upload";
?>
