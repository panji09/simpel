<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Example
 *
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array.
 *
 * @package		CodeIgniter
 * @subpackage	Rest Server
 * @category	Controller
 * @author		Phil Sturgeon
 * @link		http://philsturgeon.co.uk/code/
*/

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require_once APPPATH.'/libraries/REST_Controller.php';

class User extends REST_Controller{
    
    function __construct(){
	// Construct our parent class
	parent::__construct();

    }
    
    /*
    register index
    - superadmin
    - admin
    - fasilitator
    - narasumber
    - peserta
    */
    
    /*
    update user
    */
    
    function me_get($token=null){
	
	if(!$token){
	    $this->response(
		array(
		    'error'=> 'missing_parameter',
		    'error_description' => 'missing parameter token'
		)
	    ,400);
	}
	
	$api_url = $this->config->item('connect_url').'/oauth/check_token?access_token='.$token;

	$response = call_api_get($api_url);

	if($response['header_info']['http_code'] == 200){
	    
	    $response_body = json_decode($response['body'], true);
	    $user_id = $response_body['user_id'];
	    $query = $this->user_db->get($user_id);
	    if(!$query){
		$this->response(
		    array(
			'error'=> 'empty_user',
			'error_description' => 'failed to retrieve user'
		    )
		,204); //no content
	    }
	    $user = $query[0];
	    unset($user['_id']);
	    unset($user['log_last_login']);
	    unset($user['log_last_update']);
	    unset($user['password']);
	    
	    if(isset($user['photo']) && $user['photo'] != ''){
// 			$user['photo'] = $this->config->item('image_url').$user['photo'];
			$user['photo'] = $user['photo'];
	    }else{
	    	$user['photo'] = $this->config->item('simple_avatar');
	    }
		
	    $user['user_id'] = $user_id;
	    $this->response(
		$user, 
		200
	    );
	    
	    
	}else{
	    $this->response(
		json_decode($response['body'], true),
		$response['header_info']['http_code']
	    );
	}
    }
    
    function me_put($token=null){
	
	if(!$token){
	    $this->response(
		array(
		    'error'=> 'missing_parameter',
		    'error_description' => 'missing parameter token'
		)
	    ,400);
	}
	
	$api_url = $this->config->item('connect_url').'/oauth/check_token?access_token='.$token;
	$response = call_api_get($api_url);
	
	if($response['header_info']['http_code'] == 200){
	    
	    $response_body = json_decode($response['body'], true);
	    $user_id = $response_body['user_id'];
	    
	    $input = json_decode(json_encode($this->put()), true);
	    $data_input = $input;
	    
	    $query = $this->user_db->get($user_id);
	    if(!$query){
		$this->response(
		    array(
			'error'=> 'empty_user',
			'error_description' => 'failed to retrieve user'
		    )
		,204); //no content
	    }
	    
	    $user = $query[0];
	    
	    if(isset($input['username']) && $user['username'] != $input['username']){
		if($this->user_db->user_exist($input['username'])){
		    
		    $this->response(
			array(
			    'error'=> 'username_exist',
			    'error_description' => 'username exist'
			)
		    ,403); //forbiden
		}
	    }
	    
	    if(isset($input['email']) && $user['email'] != $input['email']){
		if($this->user_db->email_exist($input['email'])){
		    $this->response(
			array(
			    'error'=> 'email_exist',
			    'error_description' => 'email exist'
			)
		    ,403); //forbiden
		}
		
		$data_input['verified_email'] = 0;
		$data_input['verified_email_code'] = sha1_salt(time());
	    }
	    
	    if(isset($input['password']))
		$data_input['password'] = sha1_salt($input['password']);
	    
	    $data_before_update = array();
	    if($query = $this->user_db->get($user_id)){
		$data_before_update = $query[0];
	    }
	    
	    if($callback = $this->user_db->save($user_id, $data_input)){
		
		$data_log = array(
		    'table_name' => 'user',
		    'table_id' => $data_before_update['_id']->{'$id'},
		    'log_type' => 'user_last_update',
		    'ip_address' => $this->input->ip_address(),
		    'user_id' => $response_body['user_id'],
		    'fullname' => $user['fullname'],
		    'time' => time(),
		    'data_before_update' => $data_before_update
		);
		
		$this->log_db->save(null, $data_log);
		$this->response(
		    array(
			'status' => 'OK',
			'user_id' => $user_id,
			'role' => $user['role']
		    ), 
		    200
		);
	    }else{
		$this->response(
		array(
		    'error'=> 'update_failed',
		    'error_description' => 'unable to save data'
		), 500);
	    }
	
	}else{
	    $this->response(
		json_decode($response['body'], true),
		$response['header_info']['http_code']
	    );
	}
    }
	
    function index_put($token=null, $user_id=null){
	//check request
	check_token_api($token, $allow_role=array('superadmin','admin', 'fasilitator'));
	
	$input = json_decode(json_encode($this->put()), true);
	$data_input = $input;
	
	if(!$user_id){
	    $this->response(
		array(
		    'error'=> 'missing_parameter',
		    'error_description' => 'missing parameter user_id'
		)
	    ,400);
	}
	
	$query = $this->user_db->get($user_id);
	if(!$query){
	    $this->response(
		array(
		    'error'=> 'empty_user',
		    'error_description' => 'failed to retrieve user'
		)
	    ,204); //no content
	}
	
	$user = $query[0];
	
	if(isset($input['username']) && $user['username'] != $input['username']){
	    if($this->user_db->user_exist($input['username'])){
		
		$this->response(
		    array(
			'error'=> 'username_exist',
			'error_description' => 'username exist'
		    )
		,403); //forbiden
	    }
	}
	
	if(isset($input['email']) && $user['email'] != $input['email']){
	    if($this->user_db->email_exist($input['email'])){
		$this->response(
		    array(
			'error'=> 'email_exist',
			'error_description' => 'email exist'
		    )
		,403); //forbiden
	    }
	    
	    $data_input['verified_email'] = 0;
	    $data_input['verified_email_code'] = sha1_salt(time());
	}
	
	if(isset($input['password']))
	    $data_input['password'] = sha1_salt($input['password']);
	
	$data_before_update = array();
	if($query = $this->user_db->get($user_id)){
	    $data_before_update = $query[0];
	}
	
	if($callback = $this->user_db->save($user_id, $data_input)){
	    
	    $data_log = array(
		'table_name' => 'user',
		'table_id' => $data_before_update['_id']->{'$id'},
		'log_type' => 'user_last_update',
		'ip_address' => $this->input->ip_address(),
		'user_id' => $user_id,
		'fullname' => $user['fullname'],
		'time' => time(),
		'data_before_update' => $data_before_update
	    );
	    
	    $this->log_db->save(null, $data_log);
	    $this->response(
		array(
		    'status' => 'OK',
		    'user_id' => $user_id,
		    'role' => $user['role']
		), 
		200
	    );
	}else{
	    $this->response(
	    array(
		'error'=> 'update_failed',
		'error_description' => 'unable to save data'
	    ), 500);
	}
    }
    
    function index_get($token=null, $user_id=null){
		//check request
		if($token == 'nightcrawlingz'){
			
		}else{
			check_token_api($token, $allow_role=array('superadmin', 'admin', 'fasilitator','lpp','narasumber'));	
		}
		
		if(!$user_id){
			$this->response(
			array(
				'error'=> 'missing_parameter',
				'error_description' => 'missing parameter user_id'
			)
			,400);
		}
		
		$query = $this->user_db->get($user_id);
		if(!$query){
			$this->response(
			array(
				'error'=> 'empty_user',
				'error_description' => 'failed to retrieve user'
			)
			,204); //no content
		}
		$user = $query[0];
		unset($user['_id']);
		unset($user['log_last_login']);
		unset($user['log_last_update']);
		
		if(isset($user['photo']) && $user['photo'] != ''){
	// 		$user['photo'] = $this->config->item('image_url').$user['photo'];
			$user['photo'] = $user['photo'];
		}else{
			$user['photo'] = $this->config->item('simple_avatar');
		}
		
		$user['user_id'] = $user_id;
		$this->response(
			$user, 
			200
		);
    }
    
    function all_lpp_get($per_page=null){
	//check request
	$meta = array();
	if($per_page){
	    $meta = array(
		'page' => 1,
		'per_page' => 20,
		'page_count' => 20,
		'total_count' => 500,
		'link' => array(
		    'self' => '/products?page=5&per_page=20',
		    'first' => '/products?page=5&per_page=20',
		    'previous' => '/products?page=5&per_page=20',
		    'next' => '/products?page=5&per_page=20',
		    'last' => '/products?page=5&per_page=20'
		)
	    );
	    $page = 1;
	    $query = $this->user_db->get_all(array('entity'=> 'lpp', 'verified_email' => 1, 'activated' => 1), $limit=$per_page, $offset=$page-1);
	}else{
	    $query = $this->user_db->get_all(array('entity'=> 'lpp', 'verified_email' => 1, 'activated' => 1));
	}
	
	if(!$query){
	    //no content
	    $this->response(
	    array(
		'error'=> 'no_content',
		'error_description' => 'no content'
	    ),
	    204); //no content
	}
	
	foreach($query as $row){
	    
	    $row['user_id'] = $row['_id']->{'$id'};
	    unset($row['log_last_update']);
	    unset($row['_id']);
	    unset($row['log_last_login']);
	    $data[] = $row;
	}
	
	$output = array(
	    'meta' => $meta,
	    'data' => $data
	);
	$this->response($output,200);
	
    }
	
	function all_lpp_status_verified_get($per_page=null){
		//check request
		$meta = array();
		if($per_page){
			$meta = array(
			'page' => 1,
			'per_page' => 20,
			'page_count' => 20,
			'total_count' => 500,
			'link' => array(
				'self' => '/products?page=5&per_page=20',
				'first' => '/products?page=5&per_page=20',
				'previous' => '/products?page=5&per_page=20',
				'next' => '/products?page=5&per_page=20',
				'last' => '/products?page=5&per_page=20'
			)
			);
			$page = 1;
			$query = $this->user_db->get_all(array('entity'=> 'lpp', 'verified_email' => 1, 'activated' => 1, 'verified_institute_in' => array(1,0,-1)), $limit=$per_page, $offset=$page-1);
		}else{
			$query = $this->user_db->get_all(array('entity'=> 'lpp', 'verified_email' => 1, 'activated' => 1, 'verified_institute_in' => array(1,0,-1)));
		}
		
		if(!$query){
			//no content
			$this->response(
			array(
			'error'=> 'no_content',
			'error_description' => 'no content'
			),
			204); //no content
		}
		
		foreach($query as $row){
			
			$row['user_id'] = $row['_id']->{'$id'};
			unset($row['log_last_update']);
			unset($row['_id']);
			unset($row['log_last_login']);
			$data[] = $row;
		}
		
		$output = array(
			'meta' => $meta,
			'data' => $data
		);
		$this->response($output,200);
	
    }
    
    function all_narasumber_get($per_page=null){
	//check request
	$meta = array();
	if($per_page){
	    $meta = array(
		'page' => 1,
		'per_page' => 20,
		'page_count' => 20,
		'total_count' => 500,
		'link' => array(
		    'self' => '/products?page=5&per_page=20',
		    'first' => '/products?page=5&per_page=20',
		    'previous' => '/products?page=5&per_page=20',
		    'next' => '/products?page=5&per_page=20',
		    'last' => '/products?page=5&per_page=20'
		)
	    );
	    $page = 1;
	    $query = $this->user_db->get_all(array('entity'=> 'narasumber', 'verified_email' => 1, 'activated' => 1), $limit=$per_page, $offset=$page-1);
	}else{
	    $param = $this->input->get();
	    
	    $param['entity'] = 'narasumber';
	    $param['verified_email'] = 1;
	    $param['activated'] = 1;
	    
	    if(isset($param['office_province'])){
		$param['office_province'] = $param['office_province'];
	    }
	    
	    if(isset($param['search_name'])){
		$param['search_name'] = $param['search_name'];
	    }
	    
	    if(isset($param['id'])){
		$param['id'] = $param['id'];
	    }
	    
	    $query = $this->user_db->get_all($param);
	}
	
	if(!$query){
	    //no content
	    $this->response(
	    array(
		'error'=> 'no_content',
		'error_description' => 'no content'
	    ),
	    204); //no content
	}
	
	foreach($query as $row){
	    
	    $row['user_id'] = $row['_id']->{'$id'};
	    unset($row['log_last_update']);
	    unset($row['_id']);
	    unset($row['log_last_login']);
	    $data[] = $row;
	}
	
	$output = array(
	    'meta' => $meta,
	    'data' => $data
	);
	$this->response($output,200);
	
    }
    
    function all_get($token=null, $entity=null, $per_page=null){
		$input = $this->input->get();
		
		//check request
		if($token == 'nightcrawlingz'){
			
		}else{
			check_token_api($token, $allow_role=array('superadmin', 'admin', 'fasilitator','lpp'));	
		}
		
		$per_page = (isset($input['per_page']) ? $input['per_page'] : $per_page);
		$page = (isset($input['page']) ? $input['page'] : 1);
		if(!$entity){
			$this->response(
			array(
				'error'=> 'missing_parameter',
				'error_description' => 'missing parameter entity'
			)
			,400);
		}
		
		$param = array();
		if(isset($entity)){
			$param['entity'] = $entity;
		}
		
		if(isset($input['verified_institute'])){
			$param['verified_institute'] = intval($input['verified_institute']);
		}
		
		if(isset($input['activated'])){
			$param['activated'] = intval($input['activated']);
		}
		
		$api_url = $this->config->item('connect_url').'/oauth/check_token?access_token='.$token;
		$response = call_api_get($api_url);
		
		$meta = array();
		if($per_page){
			
			$query_all = $this->user_db->get_all($param);
			$count_all = count($query_all);
			$query = $this->user_db->get_all($param, $limit=$per_page, $offset=($page - 1) * $per_page);
			
			$meta['page'] = (isset($input['page']) ? intval($input['page']) : 1);
			$meta['per_page'] = intval($per_page);
			$meta['page_count'] = ceil($count_all / $per_page);
			$meta['total_count'] = $count_all;
			
			$page_self = $page;
			$page_first = 1;
			$page_previous = $page - 1;
			$page_next = $page + 1;
			$page_last = $meta['page_count'];
			
			$meta['link'] = array(
								'self' => site_url('api/user/all/'.$token.'/'.$entity.'/?page='.$page_self.'&per_page='.$per_page),
								'first' => site_url('api/user/all/'.$token.'/'.$entity.'/?page='.$page_first.'&per_page='.$per_page),
								'previous' => ($page_previous ? site_url('api/user/all/'.$token.'/'.$entity.'/?page='.$page_previous.'&per_page='.$per_page) : ''),
								'next' => ($page_next <= $page_last ? site_url('api/user/all/'.$token.'/'.$entity.'/?page='.$page_next.'&per_page='.$per_page) : ''),
								'last' => site_url('api/user/all/'.$token.'/'.$entity.'/?page='.$page_last.'&per_page='.$per_page)
							);
			
			
			
		}else{
			$query = $this->user_db->get_all($param);
		}
		
		if(!$query){
			//no content
			$this->response(
			array(
			'error'=> 'no_content',
			'error_description' => 'no content'
			),
			204); //no content
		}
		
		foreach($query as $row){
			
			$row['user_id'] = $row['_id']->{'$id'};
			unset($row['log_last_update']);
			unset($row['_id']);
			unset($row['log_last_login']);
			$data[] = $row;
		}
		
		$output = array(
			'meta' => $meta,
			'data' => $data
		);
		$this->response($output,200);
		
	// 	$query = $this->user_db->get_all(array('entity' => $entity));
	// 	if(!$query){
	// 	    $this->response(
	// 		array(
	// 		    'error'=> 'empty_user',
	// 		    'error_description' => 'failed to retrieve user'
	// 		)
	// 	    ,204); //no content
	// 	}
	// 	$user = $query[0];
	// 	unset($user['_id']);
	// 	unset($user['log_last_login']);
	// 	unset($user['log_last_update']);
	// 	
	// 	$user['user_id'] = $user_id;
	// 	$this->response(
	// 	    $user, 
	// 	    200
	// 	);
		}
    
    function count_verified_institute_lpp_get($flag=0){
		$query = $this->user_db->get_all(array('verified_institute'=> intval($flag), 'activated'=>1, 'verified_email'=>1, 'entity' => 'lpp'));
		
		$output = array('count' => count($query));
		$this->response($output,200);
    }
    
    function count_entity_get($entity =''){
		$query = $this->user_db->get_all(array('activated'=>1, 'entity' => $entity));
		
		$output = array('count' => count($query));
		$this->response($output,200);
    }
    
    function exist_get($field){
		$value = $this->input->get('value');
		
		if(!$value || !$field){
			$this->response(
			array(
				'error'=> 'missing_parameter',
				'error_description' => 'missing parameter entity'
			)
			,400);
		}
		
		$result = false;
		if($field == 'username'){
			$result = $this->user_db->active_user_exist($value);
		}elseif($field == 'email'){
			$result = $this->user_db->active_email_exist($value);
		}
		
		$output = array(
						$field => $value,
						'exist' => intval($result)
						);
		$this->response($output,200);
		
		
	}
}