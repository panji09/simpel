<?php defined('BASEPATH') OR exit('No direct script access allowed');
#require_once APPPATH.'/controllers/api/Task.php';
require_once APPPATH.'/controllers/Notification.php';
/**
 * Example
 *
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array.
 *
 * @package		CodeIgniter
 * @subpackage	Rest Server
 * @category	Controller
 * @author		Phil Sturgeon
 * @link		http://philsturgeon.co.uk/code/
*/

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require_once APPPATH.'/libraries/REST_Controller.php';

class Register extends REST_Controller{
    
    function __construct(){
	// Construct our parent class
	parent::__construct();
	header('Access-Control-Allow-Origin: *');
	// Configure limits on our controller methods. Ensure
	// you have created the 'limits' table and enabled 'limits'
	// within application/config/rest.php
	
    //         $this->methods['user_get']['limit'] = 500; //500 requests per hour per user/key
    //         $this->methods['user_post']['limit'] = 100; //100 requests per hour per user/key
    //         $this->methods['user_delete']['limit'] = 50; //50 requests per hour per user/key
    }
    
    /*
    register index
    - superadmin
    - admin
    - fasilitator
    - narasumber
    - peserta
    */
    
    function create_folder($user_id){
	
	//create folder user
	$path_user = getcwd().'/assets/users/'.$user_id;
	if(!file_exists($path_user)){
	    mkdir($path_user);
	    //chmod($path_user, "0775");
	    shell_exec('chmod 777 '.$path_user);

	    $path_user = getcwd().'/assets/users/'.$user_id.'/files';
	    if(!file_exists($path_user)){
		mkdir($path_user);
		//chmod($path_user, "0775");
		shell_exec('chmod 777 '.$path_user);
	    }
	}
	
	
	// $path_user = getcwd().'/assets/users/'.$user_id;
	// if (!file_exists($path_user.'/')) {
	//     mkdir($path_user, 0777);
	// }
	return is_dir($path_user);
    }
    
    function index_post($entity=null, $token=null){
// 	print_r($this->post());
	$input = json_decode(json_encode($this->post()), true);
// 	print_r($input);
	if(!$entity){
	    $this->response(
		array(
		    'error'=> 'missing_parameter',
		    'error_description' => 'missing parameter entity'
		)
	    ,400);
	}
	
	//check field
	$array_field = array('username', 'password', 'email');
	if(!check_request($input, $array_field)){
	    $this->response(
		array(
		    'error'=> 'invalid_field',
		    'error_description' => 'missing field [username, password, email]',
		    'input' => $input
		)
	    ,400); //bad request
	}
	
	$query = $this->user_db->get_all($filter = array('username' => $input['username']));
	if(count($query)==1){
	    $this->response(
		array(
		    'error'=> 'username_exist',
		    'error_description' => 'username exist'
		)
	    ,403); //forbiden
	}

	$query = $this->user_db->get_all($filter = array('email' => $input['email']));
	if(count($query)==1){
	    $this->response(
		array(
		    'error'=> 'email_exist',
		    'error_description' => 'email exist'
		)
	    ,403); //forbiden
	}
	
	$data_input = $input;
	    
	$data_input['password'] = sha1_salt($input['password']);
	
	$data_input['entity'] = $entity;
	$data_input['role'] = array(strtolower($entity));
	$data_input['verified_email'] = 0;
	$data_input['verified_email_code'] = sha1_salt(time());
	
	$data_input['deleted'] = 0;
	$data_input['activated'] = 0;
	
	if($callback = $this->user_db->save(null, $data_input)){
	    
	    //create folder
	    if(!$this->create_folder($callback->{'$id'})){
		//fail
		$this->response(
		array(
		    'error'=> 'mkdir_failed',
		    'error_description' => 'create folder failed'
		), 500);
	    }
	    
	    //copy file to folder user
	    $data_update = array();
	    foreach($data_input as $key => $val){
		if(filter_var($val, FILTER_VALIDATE_URL)){
		    //valid url
		    $file = $val;
		    
		    $relative_path = '/assets/users/'.$callback->{'$id'}.'/files/'.urldecode(basename($file));
		    $newfile = getcwd() . $relative_path;
		    if ( copy($file, $newfile) ) {
			//success
			$data_update[$key] = base_url($relative_path);
		    }else{
			//fail
			$this->response(
			array(
			    'error'=> 'copy_failed',
			    'error_description' => 'copy file failed'
			), 500);
		    }
		}
		
	    }
	    
	    if($data_update){
			$this->user_db->save($callback->{'$id'}, $data_update);
	    }
		
	    //send email confirmation here
	    $Notification = new Notification();
	    $Notification->email_confirmation($email = $data_input['email'], $fullname = $data_input['fullname'], $verification_email_code = $data_input['verified_email_code']);
		
	    //copy file
	    $this->response(
		array(
		    'status' => 'OK',
		    'user_id' => $callback->{'$id'},
		    'role' => $data_input['role']
		), 
		200
	    );
	}else{
	    $this->response(
	    array(
		'error'=> 'register_failed',
		'error_description' => 'unable to save data'
	    ), 500);
	}
   
    }
    
    function check_username_post(){
	$input = json_decode(json_encode($this->post()), true);
	//check field
	$array_field = array('username');
	if(!check_request($input, $array_field)){
	    $this->response(
		array(
		    'error'=> 'invalid_field',
		    'error_description' => 'invalid field, field allowed [username]'
		)
	    ,400);
	}
	
	if(filter_var($input['username'], FILTER_VALIDATE_EMAIL))
	    $query=$this->user_db->get_all($filter = array('email' => $input['username']));
	else
	    $query=$this->user_db->get_all($filter = array('username' => $input['username']));
	
	
	if(count($query)==1)
	    $this->response(
		array(
		    'error'=> 'username_exist',
		    'error_description' => 'username exist'
		)
	    ,404);
	else
	    $this->response(array('status' => 'OK'), 200);
    }
	

	function check_confirmation_code_post(){
		$input = json_decode(json_encode($this->post()), true);
		//check field
		$array_field = array('verification_code');
		if(!check_request($input, $array_field)){
		    $this->response(
			array(
			    'error'=> 'invalid_field',
			    'error_description' => 'invalid field, field allowed [verification_code]'
			)
		    ,400);
		}
		
		$query=$this->user_db->get_all($filter = array('verified_email_code' => $input['verification_code']));
				
		if(count($query)!=1){
		    $this->response(
			array(
			    'error'=> 'verification_not_valid',
			    'error_description' => 'Verification code not valid'
			)
		    ,404);
		}
		
		$user = $query[0];		
		//this verification code active so change user's status
		$user_id = $user['_id']->{'$id'};
		
		$data_input['verified_email'] = 1;
		$data_input['verified_email_code'] = '';
		$data_input['activated'] = 1;
		
		//tambah field verified_institute
		if(strtolower($user['entity'])=='lpp'){
		    $data_input['verified_institute'] = 0; //mengunggu verifikasi
		}
		
	    if($callback = $this->user_db->save($user_id, $data_input)){
		
		//kirim notif ke admin untuk verifikasi
		if(strtolower($user['entity'])=='lpp'){
		    
		    $Notification = new Notification();
		    $Notification->review_admin_approval_lpp($user_id);
		    /*
		    $query = $this->user_db->get_all(array('entity'=>'admin', 'activated' => 1));
		    if($query){
			$user_admin = $query;
			
			
			$Task = new Task();
			foreach($user_admin as $row){
			    
			    $subject = 'Pendaftaran LPP baru, menunggu verifikasi';
			    $message = '
				Nama Lembaga : '.$user['training_institute'].'<br>
				Status Lembaga  : '.$user['training_institute_status'].'<br>
				email : '.$user['email'].'<br>
			    ';
			    $Task->send_email($to=$row['email'], $subject, $message);
			}
			
			
		    }
		    */
		    
		}
		
		$user_output = array(
		    'user_id' => $user['_id']->{'$id'},
		    'fullname' => $user['fullname'],
		    'email' => $user['email']
		);
		
		
		$this->response(array('status' => 'OK','user' => $user_output), 200);			
	    }else{
		$this->response(
		    array(
			'error'=> 'verivication_not_valid',
			'error_description' => 'Verification code not valid'
		    )
		,404);
	    }					
	}
	
	function fix_user_get(){
            $query = $this->user_db->get_all(array('entity'=>'peserta','verified_email'=>0));
            $Notification = new Notification();

            foreach($query as $user){
                $this->create_folder($user['_id']->{'$id'});
                $Notification->email_confirmation($email = $user['email'], $fullname = $user['fullname'], $user['verified_email_code']);
            }
        }

	
	/*
	private function send_email_confirmation($to, $name, $verification_code){
		$data['name'] = $name;
		$data['confirmation_link'] = $this->config->item('confirmation_link') . $verification_code;
		$message = $this->load->view('templates/email_registration_confirmation', $data,TRUE);
		
		$subject = 'Verifikasi Pendaftaran - '.$this->config->item('title_short');
		// $this->send_email($to, $subject, $message);
						
		$Task = new Task();
		$Task->send_email($to, $subject, $message);
		
	}
	
	
	private function send_email($to, $subject, $message){
		//save token success
		$this->email->from('noreply@ruangpanji.com','SIMPEL');
		$this->email->to($to);
		$this->email->subject($subject);
	
		$this->email->message($message);
	
		if($this->email->send()){
		    //ok
		    $this->response(array(
			'status'=>'OK'
		    ),200);
		}else{
		    //internal server error
		    $this->response(array(
			'error'=>'send_fail',
			'error_description' => 'failed to send mail'
		    ),500);
		}
	}
	*/
}