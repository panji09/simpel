<?php defined('BASEPATH') OR exit('No direct script access allowed');

use PhpAmqpLib\Connection\AMQPConnection;
use PhpAmqpLib\Message\AMQPMessage;

class Task{
    
    /*
    function __construct(){
        parent::__construct();
        
        $this->host = 'ruangpanji.com';
        $this->port = 5672;
        $this->username = 'admin';
        $this->password = 'adminrabbitmq123';
    }
    */
    
    /*
    public $host = 'localhost';
    public $port = 5672;
    public $username = 'admin';
    public $password = 'adminrabbitmq123';
    */
    public $connection;
    public $channel;
    
    
    function __construct($config=array()){
// 	parent::__construct();
	
	if($config){
	    $this->connection = new AMQPConnection($config['host'], $config['port'], $config['username'], $config['password']);
        
	}
        $this->channel = $this->connection->channel();
    }
    
    function __destruct(){
	$this->channel->close();
	$this->connection->close();
    }
    
    function send_email($to=null, $subject=null, $message=null, $cc=null, $bcc=null){		
	
	$result = false;
	
	if($to && $message && $subject){
	    
	    $queue_name = 'task_send_email';
	    $this->channel->queue_declare($queue_name, false, false, false, false);

	    $data = array(
		'to' => $to,
		'subject' => $subject,
		'message' => $message,
		'cc' => $cc,
		'bcc' => $bcc
	    );
	    $data = json_encode($data);
	    $msg = new AMQPMessage($data,
				    array('delivery_mode' => 2) # make message persistent
				  );

	    $this->channel->basic_publish($msg, '', $queue_name);
	
	    $result = json_encode($data);
	}
	
	
	return $result;
    }
    
}