
<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Example
 *
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array.
 *
 * @package		CodeIgniter
 * @subpackage	Rest Server
 * @category	Controller
 * @author		Phil Sturgeon
 * @link		http://philsturgeon.co.uk/code/
*/

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH.'/libraries/REST_Controller.php';

class Certificate extends REST_Controller{
    function __construct(){
	// Construct our parent class
	parent::__construct();
	
    }
    function index_get($certificate_id){
	if($certificate_id){
		$domain = 'sertifikasi.lkpp.go.id';
		$output = file_get_contents('http://'.$domain.'/logbook2/get_ahli.php?user=pelatihan&pass=lkpppelatihan2015&nosert='.$certificate_id);
		
		$output = json_decode($output, true);
	// 	print_r($output);
		if(isset($output[0]['error'])){
			$this->response(array('error' => 'no_content'),200);
		}else{
			$output[0]['path_photo'] = 'http://'.$domain.$output[0]['path_photo'];
			$output[0]['path_skep'] = 'http://'.$domain.$output[0]['path_skep'];
			$output[0]['path_sertifikat'] = 'http://'.$domain.$output[0]['path_sertifikat'];
			
			$this->response($output,200);
		}
	
	}
    }
}
