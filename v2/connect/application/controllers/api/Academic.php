<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Example
 *
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array.
 *
 * @package		CodeIgniter
 * @subpackage	Rest Server
 * @category	Controller
 * @author		Phil Sturgeon
 * @link		http://philsturgeon.co.uk/code/
*/

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH.'/libraries/REST_Controller.php';

class Academic extends REST_Controller{
    function __construct(){
	// Construct our parent class
	parent::__construct();
	
    }
    
    function educational_level_get(){
	$output = $this->academic_db->get_educational_level();
	
	$this->response($output, 200);
    }
    
    function academic_degree_get($filter=null){
	$output = $this->academic_db->get_academic_degree();
	
	if($filter=='sort_name'){
	    $temp = json_decode(json_encode($output), true);
	    foreach($temp as $row){
		$out_filter[] = $row['sort_name'];
	    }
	    $output = $out_filter;
	}
	header('content-type: application/json; charset=utf-8');
	header("access-control-allow-origin: *");
	$this->response($output, 200);
    }
}