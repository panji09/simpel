<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH.'/libraries/REST_Controller.php';
require_once APPPATH.'/controllers/api/Task.php';

class Send_email extends REST_Controller{
	function __construct(){
	// Construct our parent class
		parent::__construct();
	}
	
	function index_post(){
	//$this->load->library('email');  
		$input = json_decode(json_encode($this->post()), true);
		
		$to = $input['to'];
		$subject = $input['subject'];
		$message = $input['message'];
		
	//save token success
		$from = $this->config->item('email_from');
		$reply_to = $this->config->item('email_reply_to');
		
		$this->email->from($from['from'], $from['name']);
		$this->email->reply_to($reply_to['from'], $reply_to['name']);
		$this->email->to($to);
		$this->email->subject($subject);
		
		$this->email->message($message);
		
		if($this->email->send()){
	    //ok
			$this->response(array(
				'status'=>'OK'
				),200);
		}else{
			
	    //internal server error
			$this->response(array(
				'error'=>'send_fail',
				'error_description' => 'failed to send mail'
				),500);
		}
	}
	
	function queuemail_post(){
		$input = json_decode(json_encode($this->post()), true);
		
		$to = $input['to'];
		$subject = $input['subject'];
		$message = $input['message'];
		
		$this->queuemail($to, $subject, $message);
	}
	
	private function queuemail($to=null, $subject=null, $message=null){
		
		if($to && $subject && $message){
			
			$config = array(
				'username' => $this->config->item('amq_username'),
				'password' => $this->config->item('amq_password'),
				'host' => $this->config->item('amq_host'),
				'port' => $this->config->item('amq_port')
				
				);
			$Task = new Task($config);
			
			$Task->send_email($to, $subject, $message);
		}
		
	}
	function broadcast_admin_post(){
		$input = json_decode(json_encode($this->post()), true);
		
		$subject = $input['subject'];
		$message = $input['message'];
		
		$query = $this->user_db->get_all(array('entity' => 'admin', 'activated' => 1));
		
		if($query)
			foreach($query as $row){
				$message_replace = str_replace("%fullname%", $row['fullname'], $message);
				$this->queuemail($to = $row['email'], $subject, $message_replace);
			}
			
		}
		
		function broadcast_superadmin_post(){
			$input = json_decode(json_encode($this->post()), true);
			
			$subject = $input['subject'];
			$message = $input['message'];
			
			$query = $this->user_db->get_all(array('entity' => 'superadmin', 'activated' => 1));
			
			if($query)
				foreach($query as $row){
					$message_replace = str_replace("%fullname%", $row['fullname'], $message);
					$this->queuemail($to = $row['email'], $subject, $message_replace);
				}
			}
			
			function broadcast_fasilitator_post(){
				
				$input = json_decode(json_encode($this->post()), true);
				
				$subject = $input['subject'];
				$message = $input['message'];
				
				$query = $this->user_db->get_all(array('entity' => 'fasilitator', 'activated' => 1));
				
				if($query)
					foreach($query as $row){
						$message_replace = str_replace("%fullname%", $row['fullname'], $message);
						$this->queuemail($to = $row['email'], $subject, $message_replace);
					}
				}
			}
