<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Example
 *
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array.
 *
 * @package		CodeIgniter
 * @subpackage	Rest Server
 * @category	Controller
 * @author		Phil Sturgeon
 * @link		http://philsturgeon.co.uk/code/
*/

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require_once APPPATH.'/libraries/REST_Controller.php';

class Educational_level extends REST_Controller{
    function __construct(){
	// Construct our parent class
	parent::__construct();
	header('Access-Control-Allow-Origin: *');
    }
    
    function index_get(){
	$output = $this->init_config_db->get_educational_level();
	
	$this->response($output, 200);
    }
}