<?php
class Backend extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
	
	}
	
	function index(){
		$this->view('login');
	}
	
	
	public function view($page = 'login', $data = array())
	{

		if ( ! file_exists(APPPATH.'/views/pages/public/'.$page.'.php'))
		{
			// Whoops, we don't have a page for that!
			show_404();
		}
		
		if($page == 'login'){
			$this->load->view('templates/header_clean', $data);
			$this->load->view('pages/public/'.$page, $data);	
			$this->load->view('templates/footer_clean', $data);			
		}else{
			$this->load->view('templates/header', $data);
			$this->load->view('pages/public/'.$page, $data);
			$this->load->view('templates/footer', $data);			
		}
	}		
}
?>