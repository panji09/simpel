<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mysql_database extends CI_Controller {
    

    function __construct(){
      parent::__construct();
    }
    
    function index(){
		$this->user_db->generate_mysql();
		$this->location_db->generate_mysql();
		$this->academic_db->generate_mysql();	
		$this->init_config_db->generate_mysql();
		echo 'ok';
	}
    
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */