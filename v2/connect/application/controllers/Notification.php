<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once APPPATH.'/controllers/api/Task.php';

class Notification extends CI_Controller {
    
//     Task = null;
    
    function __construct(){
	
    }
    
    function send_task_email($to, $subject, $message){
	
	$CI =& get_instance();
	
	$config = array(
	    'username' => $CI->config->item('amq_username'),
	    'password' => $CI->config->item('amq_password'),
	    'host' => $CI->config->item('amq_host'),
	    'port' => $CI->config->item('amq_port')
	    
	);
	
	$Task = new Task($config);

	$Task->send_email($to, $subject, $message);
    }
    
    private function queuemail($to=null, $subject=null, $message=null){
	
	if($to && $subject && $message){
	
	    $config = array(
		'username' => $this->config->item('amq_username'),
		'password' => $this->config->item('amq_password'),
		'host' => $this->config->item('amq_host'),
		'port' => $this->config->item('amq_port')
		
	    );
	    $Task = new Task($config);
	
	    $Task->send_email($to, $subject, $message);
	}
	
    }
    
    function forgot_password_confirmation($email=null, $fullname=null, $verification_code=null){
	$CI =& get_instance();
	
	if($email && $fullname && $verification_code){
	    //template email
	    
	    /*
			$data['name'] = $user['fullname'];
			$data['confirmation_link'] = site_url('login/confirm_forgot_password/'.$generate_code);
			$message = $this->load->view('templates/email_forgot_password_confirmation', $data,TRUE);

		    //send email
		    $subject = 'Konfirmasi Reset Password '.$this->config->item('title_short');
		    $Task = new Task();
		    $Task->send_email($to = $this->input->post('email'), $subject, $message);
		    */
	    
	    $subject = 'Konfirmasi Reset Password - '.$CI->config->item('title_short');
	    $data['title'] = 'Hai '.$fullname.',';
	    $data['title_overview'] = 'Kami mendapatkan permintaan untuk melakukan reset password pada akun Anda.';
	    $data['title_detail'] = 'Jika Anda benar ingin melakukan reset password terhadap akun Anda, klik tombol di bawah. Jika tidak, abaikan email ini.';
	    $data['link'] = array(
		'url' => site_url('login/confirm_forgot_password/'.$verification_code),
		'name' => 'Konfirmasi Permintaan Password Baru'
	    );
	    
	    $message = $CI->load->view('templates/email_template', $data,TRUE);
	    
	    $this->send_task_email($to = $email, $subject, $message);
	}
    }
    
    function forgot_password_success($email=null, $fullname=null, $generate_password=null){
	$CI =& get_instance();
	
	if($email && $fullname && $generate_password){
	    //template email
	    
	    /*
	    $data['password'] = $data_input['password'];
	    $message = $this->load->view('templates/email_new_password', $data,TRUE);				
	    $subject = 'Konfirmasi Reset Password '.$this->config->item('title_short');
	    $Task = new Task();
	    $Task->send_email($to = $user['email'], $subject, $message);
	    */
	    
	    $subject = 'Konfirmasi Reset Password - '.$CI->config->item('title_short');
	    $data['title'] = 'Hai '.$fullname.',';
	    $data['title_overview'] = 'Konfirmasi permintaaan password baru sukses!';
	    $data['title_detail'] = 'Password baru kamu adalah <strong>'.$generate_password.'</strong>. Silahkan login dan ganti password anda dengan mengk klik dibawah ini.';
	    $data['link'] = array(
		'url' => site_url(''),
		'name' => 'Login dan Ganti Password kamu'
	    );
	    
	    $message = $CI->load->view('templates/email_template', $data,TRUE);
	    
	    $this->send_task_email($to = $email, $subject, $message);
	}
    }
    
    
    
    function email_confirmation($email = null, $fullname = null, $verification_email_code = null){
	$CI =& get_instance();
	
	if($email && $fullname && $verification_email_code){
	    
	    $subject = 'Verifikasi Pendaftaran - '.$CI->config->item('title_short');
	    
	    //template email
	    $data['title'] = 'Selamat datang di LKPP Connect';
	    $data['title_overview'] = 'Hai '.$fullname.', lengkapi proses registrasi anda!';
	    $data['title_detail'] = 'Proses registrasi anda hampir selesai, klik link konfirmasi dibawah ini untuk menyelesaikan proses registrasi.';
	    $data['link'] = array(
		'url' => site_url('oauth/confirmation/'.$verification_email_code),
		'name' => 'Konfirmasi Email'
	    );
	    
	    $message = $CI->load->view('templates/email_template', $data,TRUE);
	    
	    $this->send_task_email($to = $email, $subject, $message);
	}
    }
    
    function review_admin_approval_lpp($user_lpp_id){
	$CI =& get_instance();
	
	$query_lpp = $CI->user_db->get_all(array('user_id' => $user_lpp_id));
	$query = $CI->user_db->get_all(array('entity'=>'admin', 'activated' => 1));
	if($query && $query_lpp){
	    $user_admin = $query;
	    $user_lpp = $query_lpp[0];
	    foreach($user_admin as $row){
		
		
		/*
		$message = '
		    Nama Lembaga : '.$user['training_institute'].'<br>
		    Status Lembaga  : '.$user['training_institute_status'].'<br>
		    email : '.$user['email'].'<br>
		';
		$this->send_task_email($to=$row['email'], $subject, $message);
		*/
		
		$subject = 'Pendaftaran LPP baru, menunggu verifikasi';
		//template email
		$data['title'] = 'Review Registrasi Lembaga Pelatihan';
		$data['title_overview'] = 'Hai '.$row['fullname'].', Lembaga Pelatihan <strong>'.$user_lpp['training_institute'].'</strong> menunggu review kamu';
		$data['title_detail'] = 'Untuk lebih detail silahkan klik di bawah ini';
		$data['link'] = array(
		    'url' => $CI->config->item('simpel_url').'/index.php/admin/approval/institution',
		    'name' => 'Review Lembaga Pelatihan'
		);
		
		$message = $CI->load->view('templates/email_template', $data,TRUE);
		
		$this->send_task_email($to = $row['email'], $subject, $message);
	    }
	    
	    
	}
    }
    
    function send_confirm_email_success($user_id=null){
	
	$CI =& get_instance();
	if($user_id){
	    $query = $CI->user_db->get($user_id);
	    if($query){
		$user = $query[0];
		
		$to = $user['email'];
		
		if($user['entity'] == 'lpp'){
		    $data['name'] = $user['training_institute'];
		    $data['progress_registration_lpp_link'] = $CI->config->item('simpel_url').'/index.php/institution';
		    
		    //template email
		    $data['title'] = 'Konfirmasi Email Sukses!';
		    $data['title_overview'] = 'Hai '.$user['training_institute'].', Akun kamu sedang direview oleh Admin';
		    $data['title_detail'] = 'Progres registrasi bisa dilihat di bawah ini';
		    $data['link'] = array(
			'url' => $CI->config->item('simpel_url').'/index.php/institution',
			'name' => 'Progres Registrasi'
		    );
		    
		    $message = $CI->load->view('templates/email_template', $data,TRUE);
		
		}else{
		    $data['name'] = $user['fullname'];
		
		}
		
		
		$subject = 'Verifikasi Email Sukses - '.$CI->config->item('title_short');
		
		$this->send_task_email($to, $subject, $message);
	  
		
	    }
	}
    }
    function send_status_approval_institute($user_id=null, $verified_institute=null){
	
	//konfirmasi ke admin untuk cek lagi
	$CI =& get_instance();
	if($user_id && $verified_institute == 0){
	    $query = $CI->user_db->get($user_id);
	    if($query){
		$user = $query[0];
		
		
		$query = $CI->user_db->get_all(array('entity'=>'admin', 'activated' => 1));
		if($query){
		    $user_admin = $query;
		    
		    foreach($user_admin as $row){
			
			$subject = 'Lembaga Pelatihan Menunggu Verifikasi';
			$message = '
			    Nama Lembaga : '.$user['training_institute'].'<br>
			    Status Lembaga  : '.$user['training_institute_status'].'<br>
			    email : '.$user['email'].'<br>
			';
			$this->send_task_email($to=$row['email'], $subject, $message);
		    }
		    
		}
	    }
	}
	
    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */