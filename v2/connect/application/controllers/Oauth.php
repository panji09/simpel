<?php
#require_once APPPATH.'/controllers/api/Task.php';
require_once APPPATH.'/controllers/Notification.php';

use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Post\PostBody;
class Oauth extends CI_Controller
{
	
	var $server;
	var $storage;
	var $user_id;
	var $user_scopes;
	
	function __construct()
	{
		parent::__construct();
		// error reporting (this is a demo, after all!)
		ini_set('display_errors',1);error_reporting(E_ALL);
		
		// // setting database for mysql
		// $dsn      = 'mysql:dbname=db_simpel_connect;host=localhost';
		// $username = 'root';
		// $password = '';
		// $this->storage = new OAuth2\Storage\Pdo(array('dsn' => $dsn, 'username' => $username, 'password' => $password));

		$auth = $this->config->item('auth');
		$user = $this->config->item('user');
		$password = $this->config->item('password');
		$db = $this->config->item('db_name');
		$host = $this->config->item('host');
		$port = $this->config->item('port');
		
		if ($auth === true) {
			$dsn = "mongodb://$user:$password@$host:$port/$db";	
		}
		else {
			$dsn = "mongodb://$host:$port/$db";	
		}
		
		$mongo = new MongoClient($dsn);
		
		$db = $mongo->{$this->config->item('db_name')};
		
		$this->storage = new OAuth2\Storage\Mongo($db);


		// Pass a storage object or array of storage objects to the OAuth2 server class
		$this->server = new OAuth2\Server($this->storage);

		// // Add the "Client Credentials" grant type (it is the simplest of the grant types)
		$this->server->addGrantType(new OAuth2\GrantType\ClientCredentials($this->storage));
		//
		// // Add the "Authorization Code" grant type (this is where the oauth magic happens)
		$this->server->addGrantType(new OAuth2\GrantType\AuthorizationCode($this->storage));
		//
		// //Add user credetial
		$this->server->addGrantType(new OAuth2\GrantType\UserCredentials($this->storage));
		
		// //Add user credetial
		$this->server->addGrantType(new OAuth2\GrantType\RefreshToken($this->storage, array(
		  'always_issue_new_refresh_token' => true
		)));
		
	}

	function index()
	{
		echo 'halaman masih dalam tahap pengembangan';
	}
	
	
	// this function for third party login
	function authorize(){
		header("access-control-allow-origin: *");
		$this->session->set_userdata('state',$this->input->get('state'));
		$this->session->set_userdata('client_id',$this->input->get('client_id'));
		
		$request = OAuth2\Request::createFromGlobals();
		$response = new OAuth2\Response();

		// validate the authorize request
		if (!$this->server->validateAuthorizeRequest($request, $response)) {
// 		    $response->send();
// 		    die;
		    $this->session->set_userdata('message',$response->getParameters()['error_description']);
// 		    redirect('');
		}
		// display an authorization form
		if (empty($_POST)) {
		//call login view
			$data['title'] = 'Single Sign On - '.$this->config->item('title_short');
			$this->view('login', $data);
			return;
		}
		//get current uri
		$current_url = basename($_SERVER['PHP_SELF']) . "?" . $_SERVER['QUERY_STRING'];
		//get user input here
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		//check user validation
		$is_authorized = self::check_validation($username, $password);
		$this->server->handleAuthorizeRequest($request, $response, $is_authorized);		
		if ($is_authorized) {
		  $code = substr($response->getHttpHeader('Location'), strpos($response->getHttpHeader('Location'), 'code=')+5, 40);
		  //call API update user_id & scope by authorize code
		  self::update_user_verification_code($code, $this->user_id);
		  	// exit("SUCCESS! Authorization Code: $code");
		  
		}else{
		    $this->session->set_userdata('message','Login gagal!');
		    redirect('');
		    

		}
		$response->send();
	}
	
	function token(){
		$this->server->handleTokenRequest(OAuth2\Request::createFromGlobals())->send();
	}
	
	
	function register($role = "peserta"){
		
		if($role == 'narasumber'){
		    $invite_code = $this->input->get('invite_code');
		    
		    if(!$invite_code){
			echo 'forbidden';
			exit();
		    }
		    
		    $url = $this->config->item('simpel_api_url').'/invite_code/index/'.$invite_code;
		    $response = call_api_get($url);
		    
		    $response_body = json_decode($response['body'], true);
		    if($response['header_info']['http_code'] != 200){
			echo 'forbidden';
			exit();
		    }
		}
		
		//get referall
		if(!$this->session->userdata('state')){
			if($this->input->get('state')){
				$state = base64_decode($this->input->get('state'));
				$this->session->set_userdata('state', $state);	
			}			
		}
		
		$js = array();
				
		if($role == 'peserta'){
			$data['title'] = 'Registrasi Peserta - '.$this->config->item('title_short');
		
			$data['css'] = array(
				base_url()."assets/global/plugins/typeahead/typeahead.css",
				base_url()."assets/global/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css"						
			);
			$data['js'] = array(
				base_url()."assets/global/scripts/register-student.js"=>"Register.init();",
				base_url()."assets/global/plugins/jquery-file-upload/js/vendor/jquery.ui.widget.js"=>"",
				base_url()."assets/global/plugins/jquery-file-upload/js/vendor/load-image.min.js"=>"",
				base_url()."assets/global/plugins/jquery-file-upload/js/vendor/canvas-to-blob.min.js"=>"",
				base_url()."assets/global/plugins/jquery-file-upload/blueimp-gallery/jquery.blueimp-gallery.min.js"=>"",
				base_url()."assets/global/plugins/jquery-file-upload/js/jquery.iframe-transport.js"=>"",
				base_url()."assets/global/plugins/jquery-file-upload/js/jquery.fileupload.js"=>"",
				base_url()."assets/global/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js"=>"",				
				base_url()."assets/global/plugins/typeahead/handlebars.min.js"=>"",
				base_url()."assets/global/plugins/typeahead/typeahead.bundle.min.js"=>""
			);		
			
			$this->view('daftar_siswa', $data);			
		}elseif($role == 'narasumber'){
			$data['title'] = 'Registrasi Pengajar - '.$this->config->item('title_short');
		
			$data['css'] = array(
				base_url()."assets/global/plugins/typeahead/typeahead.css",
				base_url()."assets/global/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css"						
			);
			$data['js'] = array(
				base_url()."assets/global/scripts/register-narasumber.js"=>"Register.init();",
				base_url()."assets/global/plugins/jquery-file-upload/js/vendor/jquery.ui.widget.js"=>"",
				base_url()."assets/global/plugins/jquery-file-upload/js/vendor/load-image.min.js"=>"",
				base_url()."assets/global/plugins/jquery-file-upload/js/vendor/canvas-to-blob.min.js"=>"",
				base_url()."assets/global/plugins/jquery-file-upload/blueimp-gallery/jquery.blueimp-gallery.min.js"=>"",
				base_url()."assets/global/plugins/jquery-file-upload/js/jquery.iframe-transport.js"=>"",
				base_url()."assets/global/plugins/jquery-file-upload/js/jquery.fileupload.js"=>"",
				base_url()."assets/global/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js"=>"",				
				base_url()."assets/global/plugins/typeahead/handlebars.min.js"=>"",
				base_url()."assets/global/plugins/typeahead/typeahead.bundle.min.js"=>""
			);		
			
			$this->view('daftar_pengajar', $data);			
		}else if($role == 'lpp'){
			$data['title'] = 'Registrasi Lembaga Pelatihan - '.$this->config->item('title_short');			
			$data['css'] = array(

			);
			$data['js'] = array(
				base_url()."assets/global/scripts/register-lpp.js"=>"Register.init();",
				base_url()."assets/global/plugins/jquery-file-upload/js/vendor/jquery.ui.widget.js"=>"",
				base_url()."assets/global/plugins/jquery-file-upload/js/vendor/load-image.min.js"=>"",
				base_url()."assets/global/plugins/jquery-file-upload/js/vendor/canvas-to-blob.min.js"=>"",
				base_url()."assets/global/plugins/jquery-file-upload/blueimp-gallery/jquery.blueimp-gallery.min.js"=>"",
				base_url()."assets/global/plugins/jquery-file-upload/js/jquery.iframe-transport.js"=>"",
				base_url()."assets/global/plugins/jquery-file-upload/js/jquery.fileupload.js"=>""
			);
			
			$this->view('daftar_lpp', $data);
		}else{
			//this default layers
			$data['title'] = 'Registrasi Pengguna - '.$this->config->item('title_short');
			$this->view('daftar_siswa', $data);		
		}
	}
	
	function handle_register($entity = 'peserta'){
		
		$inputJSON = file_get_contents('php://input');
 	   	$irequest = json_decode($inputJSON);
//  	   	print_r($irequest);
		//api for registration
		$api_url = $this->config->item('api_url').'/register/index/'. $entity;
// 		echo $api_url;
		//unset data				
		unset($irequest->rpassword);
		unset($irequest->remail);
		if($entity == 'peserta'){
			self::proccess_peserta($irequest);
		}elseif($entity == 'lpp'){
			self::proccess_lpp($irequest);			
		}elseif($entity == 'narasumber'){
			self::proccess_peserta($irequest);			
		}
		//call registration function
		
		$response = call_api_post($api_url, $irequest);
		    
		$response_body = json_decode($response['body'], true);
		if($response['header_info']['http_code'] != 200){
		    http_response_code($response['header_info']['http_code']);
		    echo json_encode($response_body);
		}else{
		    if($this->session->userdata('state')){
			    $refferal = $this->session->userdata('state');
			    $this->session->unset_userdata('state');
		    }else{
			    $refferal = site_url('');
		    }
		    $response_body['refferal'] = $refferal;
		    echo json_encode($response_body);
		}
		/*
		$response = self::call_api_post($api_url, $irequest);
		$status_code = $response->getStatusCode();
		
		$r = $response->json();
// 		print_r($response);
		if($status_code != 200){
			http_response_code($status_code);
			echo $r['error_description'];
		}else{
			if($this->session->userdata('state')){
				$refferal = $this->session->userdata('state');
				$this->session->unset_userdata('state');
			}else{
				$refferal = site_url('');
			}
			$r['refferal'] = $refferal;
			
			echo json_encode($r);
		}
		*/
	}
	
	function confirmation($confirmation_code){
		$api_url = $this->config->item('api_url').'/register/check_confirmation_code';
		$object = new StdClass();
		$object->verification_code = $confirmation_code;
		$response = self::call_api_post($api_url, $object);
		$status_code = $response->getStatusCode();
		$r = $response->json();	

		if($status_code == 200){			
// 			$this->session->set_userdata('message', "Confirmation code success");
			$this->session->set_userdata('notif', array('status' => true, 'msg' => 'Konfirmasi email sukses!.'));
			
			//kirim email progress
			$Notification = new Notification();
			$Notification->send_confirm_email_success($user_id=$r['user']['user_id']);
			
			redirect(site_url(''));
			
			
		}else{
// 			$this->session->set_userdata('message', $r['error_description']);
			$this->session->set_userdata('notif', array('status' => false, 'msg' => 'Konfirmasi email gagal!.'));
			redirect(site_url(''));
		}
	}

	private function proccess_peserta($data){
		
	}
	
	private function proccess_lpp($data){
		//check training status
		if($data->training_institute_status == 'negeri'){
			//proccess negeri lpp
			unset($data->responsible_person_ktp);
			unset($data->responsible_person_ktp_upload);
			//eliminate surat tugas
			unset($data->training_task_swasta);
			$data->training_task = $data->training_task_negeri;
			unset($data->training_task_negeri);
			if($data->training_task == 'ya'){
				$data->training_task_upload = $data->training_task_negeri_upload;
				unset($data->training_task_negeri_upload);
				unset($data->training_task_swasta_upload);
			}else{
				//eliminate training task upload field
				unset($data->training_task_negeri_upload);
				unset($data->training_task_swasta_upload);
			}
			//financial_requirement_upload
			unset($data->financial_npwp_number);
			unset($data->financial_requirement_npwp_upload);
						
		}else{
			//this swasta
			//eliminate nip			
			unset($data->responsible_person_nip);
			//surat tugas
			unset($data->training_task_negeri);
			$data->training_task = $data->training_task_swasta;
			unset($data->training_task_swasta);			
			if($data->training_task == 'ya'){
				$data->training_task_upload = $data->training_task_swasta_upload;
				unset($data->training_task_negeri_upload);
				unset($data->training_task_swasta_upload);
			}else{
				unset($data->training_task_negeri_upload);
				unset($data->training_task_swasta_upload);				
			}
			//financial requirement
			$data->financial_requirement_upload = $data->financial_requirement_npwp_upload;
			unset($data->financial_requirement_npwp_upload);						
		}
	}
	
	
	function callback(){
		$url = site_url().'oauth/token';
		$code = $this->input->get('code');
		
		$b = new PostBody();
		$b->setField('grant_type', 'authorization_code');
		$b->setField('code', $code);
				
		if(!$code){
			$this->session->set_userdata('message', "Login gagal!");
			redirect(site_url(''));
		}
		// $data = 'grant_type=authorization_code&code=7d43273134cdc0ffba2b1f0cc5dafba79bacd7d0';
		$client = new GuzzleHttp\Client();		
		try{
			$client_id = $this->config->item('client_id');
			$client_secret = $this->config->item('client_secret');
			$response = $client->post($url, ['auth' => [$client_id, $client_secret], 'headers'=>['Content-Type'=>'application/x-www-form-urlencoded'], 'body' => $b]);
// 			print_r($response);exit;
		} catch (RequestException $e) {
    		if ($e->hasResponse()) {
        		$response = $e->getResponse();
    		}
		}
		
		$result = $response->json();		
// 		print_r($result);exit;
		try{			
		//request user detail
		$response_server = $client->get(site_url()."oauth/check_token?access_token=".$result['access_token']);
// 		print_r($response_server);exit;
		}catch (RequestException $e) {
    		if ($e->hasResponse()) {
        		$response_server = $e->getResponse();
    		}
		}
		
		$user_info = $response_server->json();
		
		if($user_info['status'] == 'OK'){
			$data =  new StdClass();
			$data->id = $user_info['user_id'];
			$data->access_token = $result['access_token'];
			$this->connect_auth->_set_session($data);
			redirect('dashboard');			
		}else{
// 			echo 'login fail';
			$this->session->set_userdata('message', "Login gagal");
			redirect(site_url(''));
		}
	}
	
	function check_token(){
		// Handle a request to a resource and authenticate the access token
		if (!$this->server->verifyResourceRequest(OAuth2\Request::createFromGlobals())) {
		    $this->server->getResponse()->send();
		    die;
		}
		$token = $this->server->getAccessTokenData(OAuth2\Request::createFromGlobals());
		//set data callback
		$data['status'] = "OK";
		$data['user_id'] = $token['user_id'];
		$data['role'] = $token['scope'];
		header('Content-Type: application/json');
		echo json_encode($data);
	}	
	
	public function view($page = 'login', $data = array())
	{

		if ( ! file_exists(APPPATH.'/views/pages/public/'.$page.'.php'))
		{
			// Whoops, we don't have a page for that!
			show_404();
		}
		
		if($page == 'login'){
			$this->load->view('templates/header_clean', $data);
			$this->load->view('pages/public/'.$page, $data);	
			$this->load->view('templates/footer_clean', $data);			
		}else{
			$this->load->view('templates/header', $data);
			$this->load->view('pages/public/'.$page, $data);
			$this->load->view('templates/footer', $data);			
		}
	}	
	
	private function check_validation($username, $password){
		$api_url = $this->config->item('api_url').'/login';
		$user = new StdClass();
		$user->username = $username;
		$user->password = $password;
		$response = self::call_api_post($api_url, $user);
		$status_code = $response->getStatusCode();
		
		$r = $response->json();	
		if($status_code == 200){
			$this->user_id = $r['user_id'];
			$this->user_scopes = $r['role'];
			return TRUE;
		}else{
			$this->session->set_flashdata('message', $r['error_description']);
			return FALSE;
		}
	}
	
	private function update_user_verification_code($code, $user_id){
		$api_url = $this->config->item('api_url').'/login/update_auth_code/'.$code;
		$user = new StdClass();
		$user->user_id = $user_id;
		$user->scope = $this->user_scopes;
		$client = new GuzzleHttp\Client();
		
		try{
			$response = $client->put($api_url, ['json' => $user]);
		} catch (RequestException $e) {
    		if ($e->hasResponse()) {
        		$response = $e->getResponse();
    		}
		}	
	}
	
	public function update_user($code, $user_id){
		$api_url = $this->config->item('api_url').'/login/update_auth_code/'.$code;
		$user = new StdClass();
		$user->user_id = $user_id;
		$user->scope = array("satu", "dua");
		$client = new GuzzleHttp\Client();
		
		try{
			$response = $client->put($api_url, ['json' => json_encode($user)]);
		} catch (RequestException $e) {
    		if ($e->hasResponse()) {
        		$response = $e->getResponse();
    		}
		}
		echo $response;		
	}
	
	public function test_curl(){
		$api_url = $this->config->item('api_url').'/login';
		$user = new StdClass();
		$user->username = 'lkppadmin';
		$user->password = 'lkppadmin';
		$response = self::call_api_post($api_url, $user);
		$status_code = $response->getStatusCode();
		$r = $response->json();	
		print_r($r);
	}
	
	public function test_login(){
		$data['username'] = 'lkppadmin';
		$data['password'] = 'lkppadmin';
		$query = $this->user_db->get_all(array('username'=>'lkppadmin'));
		print_r($query);die;
		if($this->login_db->login_user($data)){
			echo 'sukes';
		}else{
			echo 'not ok';
		}
	}
	
	private function call_api_post($url, $data){
		//push puzzles to server
		$client = new GuzzleHttp\Client();
		try{
			$response = $client->post($url, ['json' => $data]);
		} catch (RequestException $e) {
    		if ($e->hasResponse()) {
        		return $e->getResponse();
    		}
		}
		return $response;
	}
	
	//helper function
	private function _objectToArray($d) {
			if (is_object($d)) {
				// Gets the properties of the given object
				// with get_object_vars function
				$d = get_object_vars($d);
			}
 
			if (is_array($d)) {
				/*
				* Return array converted to object
				* Using __FUNCTION__ (Magic constant)
				* for recursive call
				*/
				return array_map(array($this, '_objectToArray'), $d);
				
			}
			else {
				// Return array
				return $d;
			}
		}
}
?>