<?php
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Post\PostBody;
class User extends CI_Controller
{
	
	var $server;
	var $storage;
	var $user;
	var $token;
	var $data;
	
	function __construct()
	{
		parent::__construct();
		// error reporting (this is a demo, after all!)
		// ini_set('display_errors',1);error_reporting(E_ALL);
		
		$auth = $this->config->item('auth');
		$user = $this->config->item('user');
		$password = $this->config->item('password');
		$db = $this->config->item('db_name');
		$host = $this->config->item('host');
		$port = $this->config->item('port');
		
		if ($auth === true) {
			$dsn = "mongodb://$user:$password@$host:$port/$db";	
		}
		else {
			$dsn = "mongodb://$host:$port/$db";	
		}
		
		$mongo = new MongoClient($dsn);
		
		$db = $mongo->{$this->config->item('db_name')};
		
		$this->storage = new OAuth2\Storage\Mongo($db);


		// Pass a storage object or array of storage objects to the OAuth2 server class
		$this->server = new OAuth2\Server($this->storage);

		// // Add the "Client Credentials" grant type (it is the simplest of the grant types)
		$this->server->addGrantType(new OAuth2\GrantType\ClientCredentials($this->storage));
		//
		// // Add the "Authorization Code" grant type (this is where the oauth magic happens)
		$this->server->addGrantType(new OAuth2\GrantType\AuthorizationCode($this->storage));
		//
		// //Add user credetial
		$this->server->addGrantType(new OAuth2\GrantType\UserCredentials($this->storage));
		
		// //Add user credetial
		$this->server->addGrantType(new OAuth2\GrantType\RefreshToken($this->storage, array(
		  'always_issue_new_refresh_token' => true
		)));
		session_start();
		
	}
	
	public function index(){
		
	}
	
	public function edit($id){
		//check if user have grant to access this page
		$this->is_authorized('superadmin');
		//get user data
		$this->user = $this->get_user_me();			
		//set session
		if(!$this->session->userdata('refferal')){
			$this->session->set_userdata('refferal', $this->agent->referrer());
		}
		
		config_kcfinder(array('user_id' => $id));
		
		$this->data['title'] = 'Edit User - LKPP Connect';
		$this->data['token'] = $this->token;
		$this->data['user_id'] = $id;
		$this->data['user_nav'] = $this->user;		
		//get user
		$user = $this->get_user($id);
		$this->data['user'] = $user;
		
		$entity = $user['entity'];
		if($entity == 'peserta'){
			$this->load_view_peserta();
		}else if($entity == 'superadmin' || $entity == 'fasilitator' || $entity == 'admin'){
			$this->load_view_admin();
		}else if($entity == 'narasumber'){
			$this->load_view_narasumber();
		}else if($entity == 'lpp'){
			$this->load_view_lpp();
		}
	}
	
	public function update($id){				
		$this->is_authorized('superadmin');		
		//get request
		$inputJSON = file_get_contents('php://input');
 	   	$irequest = json_decode($inputJSON);
		
		//check if is update 
		if(isset($irequest->edit_password)){
			$irequest->password = $irequest->edit_password;
			unset($irequest->edit_password);
			unset($irequest->r_edit_password);			
		}		
		
		//check if this update is lpp
		if(isset($irequest->training_institute)){
			$this->proccess_lpp($irequest);
		}
		
		$api_url = $this->config->item('api_url').'/user/index/'. $this->token .'/'. $id;	
			
		$client = new GuzzleHttp\Client();
		try{
			$response = $client->put($api_url, ['json' => $irequest]);
		} catch (RequestException $e) {
	    		if ($e->hasResponse()) {
	        		$response = $e->getResponse();
	    		}
		}
		
		// $status = $response->json();
		
		header('Content-Type: application/json');
		echo json_encode($response);		
	}
	
	public function back_to_refferal(){
		$back_url = $this->session->userdata('refferal');
		$this->session->unset_userdata('refferal');
		redirect($back_url);
	}
	
	private function is_authorized($role){
			// Handle a request to a resource and authenticate the access token
			if (!$this->server->verifyResourceRequest(OAuth2\Request::createFromGlobals())) {
			    $this->server->getResponse()->send();
			    die;
			}
			$token = $this->server->getAccessTokenData(OAuth2\Request::createFromGlobals());
			//set data callback
			$this->user['status'] = "OK";
			$this->user['user_id'] = $token['user_id'];
			$this->user['role'] = $token['scope'];
			$this->token = $token['access_token'];
			$this->session->set_userdata('Connect_token', $this->token);
			if(!in_array($role, $this->user['role'])){
				echo 'Not granted to access this page';
				die;
			}			
	}
	
	private function get_user_me(){
		$api_url = $this->config->item('api_url').'/user/me/'. $this->token;
		return $this->get($api_url);
	}
	
	private function get_user($id){
		$api_url = $this->config->item('api_url').'/user/index/'. $this->token .'/'. $id;
		return $this->get($api_url);
	}
	
	private function getUserLastEducationData(){
		$api_url = $this->config->item('api_url').'/academic/educational_level';
		$client = new GuzzleHttp\Client();
		try{
			$response = $client->get($api_url);
		} catch (RequestException $e) {
		    		if ($e->hasResponse()) {
		        		$response = $e->getResponse();
		    		}
		}
		

		$educations = $response->json();	
		$count = count($educations);
		$edudata = array();
		
		for($i=0;$i < $count; $i++){
			if($this->data['user']['last_education'] == $educations[$i]){
				$edudata[$i] = '<option value="'.$educations[$i].'" selected="selected">'.$educations[$i].'</option>';
			}else{
				$edudata[$i] = '<option value="'.$educations[$i].'">'.$educations[$i].'</option>';
			}
		}
		
		return $edudata;
	}
	
	private function getUserEmploymentData(){
		$api_url = $this->config->item('api_url').'/employment';
		$client = new GuzzleHttp\Client();
		try{
			$response = $client->get($api_url);
		} catch (RequestException $e) {
		    		if ($e->hasResponse()) {
		        		$response = $e->getResponse();
		    		}
		}
		

		$employments = $response->json();	
		$count = count($employments);
		$employments_data = array();
		
		for($i=0;$i < $count; $i++){
			if($this->data['user']['employment'] == $employments[$i]){
				$employments_data[$i] = '<option value="'.$employments[$i].'" selected="selected">'.$employments[$i].'</option>';
			}else{
				$employments_data[$i] = '<option value="'.$employments[$i].'">'.$employments[$i].'</option>';
			}
		}
		
		return $employments_data;		
	}
	
	private function getUserOfficeProvinceData(){
		$client = new GuzzleHttp\Client();
		try
		{			
			//request user detail
			$response_server = $client->get(base_url().'api/location/province');
		}catch (RequestException $e) {
    		if ($e->hasResponse()) {
        		$response_server = $e->getResponse();
    		}
		}
		
		$provinces = $response_server->json();
		$province_data = array();
		foreach($provinces as $province){
			if(isset($this->data['user']['office_province']['id']) && $this->data['user']['office_province']['id'] == $province['id']){
				$province_data[] = '<option value="'.$province['id'].'" selected="selected">'.$province['name'].'</option>';
			}else{
				$province_data[] = '<option value="'.$province['id'].'">'.$province['name'].'</option>';
			}
			
		}
		
		//request district
		$districts_data = '';
		if(isset($this->data['user']['office_province']['id'])){
		    try
		    {			
			    //request user detail
			    $response_server = $client->get(base_url().'api/location/district/'.$this->data['user']['office_province']['id']);
		    }catch (RequestException $e) {
		    if ($e->hasResponse()) {
			    $response_server = $e->getResponse();
		    }
		    }
		    
		    $districts = $response_server->json();
		    $districts_data = array();
		    
		    foreach($districts as $district){
			    if($this->data['user']['office_district']['id'] == $district['id']){
				    $districts_data[] = '<option value="'.$district['id'].'" selected="selected">'.$district['name'].'</option>';
			    }else{
				    $districts_data[] = '<option value="'.$district['id'].'">'.$district['name'].'</option>';
			    }			
		    }
		}
		//request subdistrict	
		$data['user_office_provinces'] = $province_data;
		$data['user_office_districts'] = $districts_data;
				
		return $data;		
	}
	
	private function get($api_url){
		$user_id = $this->connect_auth->get_user_id();
		// $api_url = $this->config->item('api_url').'/user/me/'. $this->connect_auth->get_access_token();
		$client = new GuzzleHttp\Client();
		try{
			$response = $client->get($api_url);
		} catch (RequestException $e) {
		    		if ($e->hasResponse()) {
		        		$response = $e->getResponse();
		    		}
		}
		$me = $response->json();
		return $me;
	}
	
	private function load_view_peserta(){
		//set css and java script
		$this->data['css'] = array(
			base_url()."assets/global/plugins/typeahead/typeahead.css",
			base_url()."assets/global/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css"	
		);		
		//init js requiretment
		$this->data['js'] = array(
			base_url()."assets/global/scripts/admin-update-basic.js"=>"UpdateBasic.init();",
			base_url()."assets/global/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js"=>"",				
			base_url()."assets/global/scripts/admin-update-profile-student.js"=>"UpdateProfile.init();",		
			base_url()."assets/global/plugins/typeahead/typeahead.bundle.min.js"=>"",
			base_url()."assets/global/plugins/jquery-file-upload/js/jquery.fileupload.js"=>""
		);			
		//init education data
		$this->data['user_last_educations'] = $this->getUserLastEducationData();
		$this->data['user_employements'] = $this->getUserEmploymentData();
		$this->data['user_office_provinces'] = $this->getUserOfficeProvinceData();
		//init sidebar
		$this->data['sidebar'] = $this->init_sidebar();
		$this->view('profile_student_edit',$this->data);
	}
	
	private function load_view_lpp(){
		$this->data['css'] = array(
			base_url()."assets/global/plugins/typeahead/typeahead.css",
			base_url()."assets/global/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css"	
		);		
		//init js requiretment
		$this->data['js'] = array(
			base_url()."assets/global/scripts/admin-update-basic.js"=>"UpdateBasic.init();",
			base_url()."assets/global/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js"=>"",				
			base_url()."assets/global/scripts/admin-update-profile-lpp.js"=>"UpdateProfile.init();",		
			base_url()."assets/global/plugins/typeahead/typeahead.bundle.min.js"=>"",
			base_url()."assets/global/plugins/jquery-file-upload/js/jquery.fileupload.js"=>""
		);			

		//init sidebar
		$this->data['sidebar'] = $this->init_sidebar();
		$this->data['user_office_provinces'] = $this->getUserOfficeProvinceData();
		$this->view('profile_lpp_edit',$this->data);
	}
	
	private function load_view_narasumber(){
		$this->data['css'] = array(
			base_url()."assets/global/plugins/typeahead/typeahead.css",
			base_url()."assets/global/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css"	
		);		
		//init js requiretment
		$this->data['js'] = array(
			base_url()."assets/global/scripts/admin-update-basic.js"=>"UpdateBasic.init();",
			base_url()."assets/global/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js"=>"",				
			base_url()."assets/global/scripts/admin-update-profile-student.js"=>"UpdateProfile.init();",		
			base_url()."assets/global/plugins/typeahead/typeahead.bundle.min.js"=>"",
			base_url()."assets/global/plugins/jquery-file-upload/js/jquery.fileupload.js"=>""
		);			
		//init education data
		$this->data['user_last_educations'] = $this->getUserLastEducationData();
		$this->data['user_employements'] = $this->getUserEmploymentData();
		$this->data['user_office_provinces'] = $this->getUserOfficeProvinceData();
		//init sidebar
		$this->data['sidebar'] = $this->init_sidebar();
		$this->view('profile_narasumber_edit',$this->data);
	}
		
	private function load_view_admin(){
		//setup js and css
		$this->data['js'] = array(
			base_url()."assets/global/scripts/admin-update-basic.js"=>"UpdateBasic.init();",
			base_url()."assets/global/plugins/jquery-file-upload/js/jquery.fileupload.js"=>""
		);	
		
		$this->data['sidebar'] = $this->init_sidebar();
		$this->view('profile_user_edit',$this->data);				
	}
	
	
	private function init_sidebar(){		
		$data['active'] = "";
		$data['profile']['name'] = $this->data['user']['fullname'];
		//get avatar
		if(isset($this->data['user']['photo']) && $this->data['user']['photo'] != ''){
			$avatar = $this->data['user']['photo'];
		}else{
			$avatar = $this->config->item('simple_avatar');
		}
		
		$data['profile']['avatar'] = $avatar;
		
		if($this->data['user']['entity'] == 'peserta'){
			$status = "Peserta Pelatihan";
		}else if($this->data['user']['entity'] == 'lpp'){
			$status = "LPP";
		}else{
			$status = $this->data['user']['entity'];
		}
		
		$data['profile']['status'] = $status;	
		//set sidebar item menu	
		$data['sidebar_menu'] = array(
			array('tag'=>'back', 'title'=>'Kembali', 'icon'=>'icon-power', 'url'=>site_url('user/back_to_refferal'))
		);
		
		return $data;
	}
	
	public function view($page = 'profile_student_overview', $data = array()){
		$data['content'] = $page;
		$data['css'][] = base_url()."assets/admin/pages/css/profile.css";

		if ( ! file_exists(APPPATH.'/views/pages/dashboard/content/'.$page.'.php'))
		{
			// Whoops, we don't have a page for that!
			show_404();
		}
		
			$this->load->view('templates/header', $data);
			$this->load->view('pages/dashboard/dashboard', $data);
			$this->load->view('templates/footer', $data);			
	}
	
	private function proccess_lpp($data){
		//check training status
		if($data->training_institute_status == 'negeri'){
			//proccess negeri lpp
			unset($data->responsible_person_ktp);
			unset($data->responsible_person_ktp_upload);
			//eliminate surat tugas
			unset($data->training_task_swasta);
			$data->training_task = $data->training_task_negeri;
			unset($data->training_task_negeri);
			if($data->training_task == 'ya'){
				$data->training_task_upload = $data->training_task_negeri_upload;
				unset($data->training_task_negeri_upload);
				unset($data->training_task_swasta_upload);
			}else{
				//eliminate training task upload field
				unset($data->training_task_negeri_upload);
				unset($data->training_task_swasta_upload);
			}
			//financial_requirement_upload
			unset($data->financial_npwp_number);
			unset($data->financial_requirement_npwp_upload);
						
		}else{
			//this swasta
			//eliminate nip			
			unset($data->responsible_person_nip);
			//surat tugas
			unset($data->training_task_negeri);
			$data->training_task = $data->training_task_swasta;
			unset($data->training_task_swasta);			
			if($data->training_task == 'ya'){
				$data->training_task_upload = $data->training_task_swasta_upload;
				unset($data->training_task_negeri_upload);
				unset($data->training_task_swasta_upload);
			}else{
				unset($data->training_task_negeri_upload);
				unset($data->training_task_swasta_upload);				
			}
			//financial requirement
			$data->financial_requirement_upload = $data->financial_requirement_npwp_upload;
			unset($data->financial_requirement_npwp_upload);						
		}
	}

	// public function test_get(){
	// 	$this->user_db->generate_mysql();
	// 	$this->location_db->generate_mysql();
	// 	$this->academic_db->generate_mysql();
	// 	$this->init_config_db->generate_mysql();
	// }
}
?>