<?php
require_once APPPATH.'/controllers/Notification.php';

use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Post\PostBody;

class Dashboard extends CI_Controller
{
	var $user;
	var $entity;
	var $content_view;
	
	function __construct(){
		parent::__construct();	
		if (!$this->connect_auth->is_logged_in()){
			$state='dashboard';
			if($this->session->userdata('state')){
			    $state = $this->session->userdata('state');
			    $this->session->unset_userdata('state');
			}
			
			$client_id = $this->config->item('client_id');
			
			if($this->session->userdata('client_id')){
			    $client_id = $this->session->userdata('client_id');
			    $this->session->unset_userdata('client_id');
			}
			
			redirect(site_url().'authorize?response_type=code&client_id='.$client_id.'&state='.$state);
		}
		else
		{
			//kcfinder
			session_start();
			// config_kcfinder(array('user_id' => $this->connect_auth->get_user_id()));			
			//isLogged
			$this->user = $this->get_me();
			$this->entity = $this->user['entity'];
			$this->init_view();
			
// 			session_start();
			config_kcfinder(array('user_id' => $this->connect_auth->get_user_id()));
		}
		check_token($this->connect_auth->get_access_token(), array('peserta','admin','superadmin','lpp','fasilitator','narasumber'));
	}
	
	function index(){
		$data['title'] = 'Dashboard - '.$this->config->item('title_short');
		$data['sidebar'] = $this->initSidebar('overview');
		$this->view($this->content_view['overview'], $data);
	}
	
	function account_overview(){
// 		print_r($this->user);
		$data['title'] = 'Dashboard - '.$this->config->item('title_short');
		$data['sidebar'] = $this->initSidebar('overview');
		$this->view($this->content_view['overview'], $data);
	}
	
	function account_settings(){
		$data['title'] = 'Dashboard - '.$this->config->item('title_short');
		$data['sidebar'] = $this->initSidebar('account');
		
		//setup view for each entity
		$entity = $this->user['entity'];
		
		
		if($entity == 'peserta'){
			
			$data['css'] = array(
				base_url()."assets/global/plugins/typeahead/typeahead.css",
				base_url()."assets/global/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css"	
			);		
			//init js requiretment
			$data['js'] = array(
				base_url()."assets/global/scripts/update-basic.js"=>"UpdateBasic.init();",
				base_url()."assets/global/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js"=>"",
				base_url()."assets/global/scripts/update-profile-student.js"=>"UpdateProfile.init();",
				base_url().'assets/global/plugins/typeahead/handlebars.min.js' => '',
				base_url()."assets/global/plugins/typeahead/typeahead.bundle.min.js"=>"",
				base_url()."assets/global/plugins/jquery-file-upload/js/jquery.fileupload.js"=>""
			);	
		
			//get data
			$data['user_last_educations'] = $this->getUserLastEducationData();
			$data['user_employements'] = $this->getUserEmploymentData();
			$data['user_office_provinces'] = $this->getUserOfficeProvinceData();
			
		}else if($entity == 'narasumber'){
			$data['css'] = array(
				base_url()."assets/global/plugins/typeahead/typeahead.css",
				base_url()."assets/global/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css"	
			);		
			//init js requiretment
			$data['js'] = array(
				base_url()."assets/global/scripts/update-basic.js"=>"UpdateBasic.init();",
				base_url()."assets/global/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js"=>"",				
				base_url()."assets/global/scripts/update-profile-student.js"=>"UpdateProfile.init();",		
				base_url().'assets/global/plugins/typeahead/handlebars.min.js' => '',
				base_url()."assets/global/plugins/typeahead/typeahead.bundle.min.js"=>"",
				base_url()."assets/global/plugins/jquery-file-upload/js/jquery.fileupload.js"=>''
			);	
		
			//get data
			$data['user_last_educations'] = $this->getUserLastEducationData();
			$data['user_employements'] = $this->getUserEmploymentData();
			$data['user_office_provinces'] = $this->getUserOfficeProvinceData();
			
		}else if($entity == 'superadmin' || $entity == 'fasilitator' || $entity == 'admin'){
			$data['css'] = array(
			);		
			//init js requiretment
			$data['js'] = array(
				base_url()."assets/global/plugins/jquery-file-upload/js/jquery.fileupload.js" => '',
				base_url()."assets/global/scripts/update-basic.js"=>"UpdateBasic.init();"			
			);			
		}else if($entity == 'lpp'){
			$data['css'] = array(
			);		
			//init js requiretment
			$data['js'] = array(
				base_url()."assets/global/scripts/update-basic.js"=>"UpdateBasic.init();",			
				base_url()."assets/global/scripts/update-profile-lpp.js"=>"UpdateProfile.init();",		
				base_url()."assets/global/plugins/jquery-file-upload/js/jquery.fileupload.js"=>""
			);	
		
			//get data
			$data['user_office_provinces'] = $this->getUserOfficeProvinceData();			
		}
				
		$this->view($this->content_view['account'], $data);		
	}
	
	function logout(){
		$this->connect_auth->logout();	
		redirect();
	}
	
	public function view($page = 'profile_student_overview', $data = array()){
		$data['user'] = $this->user;
		$data['user_nav'] = $this->user;
		$data['content'] = $page;
		$data['css'][] = base_url()."assets/admin/pages/css/profile.css";

		if ( ! file_exists(APPPATH.'/views/pages/dashboard/content/'.$page.'.php'))
		{
			// Whoops, we don't have a page for that!
			show_404();
		}
		
			$this->load->view('templates/header', $data);
			$this->load->view('pages/dashboard/dashboard', $data);
			$this->load->view('templates/footer', $data);			
	}
	
	public function update_profile(){
		$inputJSON = file_get_contents('php://input');
 	   	$irequest = json_decode($inputJSON);
		
		//check if is update 
		if(isset($irequest->edit_password)){
			$irequest->password = $irequest->edit_password;
			unset($irequest->edit_password);
			unset($irequest->r_edit_password);			
		}		
		
		//check if this update is lpp
		if(isset($irequest->training_institute)){
			$this->proccess_lpp($irequest);
		}
		
		$user_id = $this->connect_auth->get_user_id();
		$api_url = $this->config->item('api_url').'/user/me/'.$this->connect_auth->get_access_token();
		
		$client = new GuzzleHttp\Client();
		try{
			$response = $client->put($api_url, ['json' => $irequest]);
		} catch (RequestException $e) {
	    		if ($e->hasResponse()) {
	        		$response = $e->getResponse();
	    		}
		}
		$status = $response->json();
		header('Content-Type: application/json');
		echo json_encode($status);
	}
	
	function verified_institute_post($user_id=null){
	    $current_url = $this->session->userdata('current_url');
	    
	    if($user_id){
		$input = array(
		    'verified_institute' => 0
		);
		
		if($this->user_db->save($user_id, $input)){
		    $Notification = new Notification();
		    $Notification->send_status_approval_institute($user_id, $verified_institute = $input['verified_institute']);
		}
	    }
	    
	    
	    redirect($current_url);
	}
	
	private function get_me(){
		$user_id = $this->connect_auth->get_user_id();
		// echo $user_id;die;
		$api_url = $this->config->item('api_url').'/user/me/'. $this->connect_auth->get_access_token();

		$client = new GuzzleHttp\Client();
		try{
			$response = $client->get($api_url);
		} catch (RequestException $e) {
		    		if ($e->hasResponse()) {
		        		$response = $e->getResponse();
		    		}
		}
		$me = $response->json();
		return $me;		
		// header('Content-Type: application/json');
		// echo json_encode($me);
	}
	
	private function init_view(){
		$entity = $this->entity;
		
		if($entity == 'peserta'){
			$this->content_view = array(
				'overview'=>'profile_student_overview',
				'account'=>'profile_student_edit'
			);
		}else if($entity == 'narasumber'){
			$this->content_view = array(
				'overview'=>'profile_narasumber_overview',
				'account'=>'profile_narasumber_edit'
			);			
		}else if($entity == 'lpp'){
			$this->content_view = array(
				'overview'=>'profile_lpp_overview',
				'account'=>'profile_lpp_edit'
			);			
		}else if($entity == 'superadmin' || $entity == 'fasilitator' || $entity == 'admin'){
			$this->content_view = array(
				'overview'=>'profile_user_overview',
				'account'=>'profile_user_edit'
			);	
		}
	}	
	
	private function initSidebar($active = 'overview'){
		$data = array();
		$status = '';
		$data['active'] = $active;
		$data['profile']['name'] = $this->user['fullname'];
		$data['profile']['avatar'] = $this->user['photo'];
		
// 		print_r($this->user);
		if($this->user['entity'] == 'peserta'){
			$status = "Peserta Pelatihan";
		}else if($this->user['entity'] == 'lpp'){
			$status = "LPP";
		}else if($this->user['entity'] == 'superadmin'){
			$status = "Super Admin";
		}else if($this->user['entity'] == 'fasilitator'){
			$status = "Fasilitator";
		}else if($this->user['entity'] == 'narasumber'){
			$status = "Narasumber";			
		}else if($this->user['entity'] == 'admin'){
			$status = "Admin";			
		}
		
		$data['profile']['status'] = $status;		
		$data['sidebar_menu'] = array(
			array('tag'=>'overview', 'title'=>'Ringkasan', 'icon'=>'icon-home', 'url'=>site_url('dashboard/account_overview')),
			array('tag'=>'account', 'title'=>'Setting Akun', 'icon'=>'icon-settings', 'url'=>site_url('dashboard/account_settings')),
			array('tag'=>'logout', 'title'=>'Keluar', 'icon'=>'icon-power', 'url'=>site_url('dashboard/logout'))
		);
		
		return $data;
	}
	
	private function getUserLastEducationData(){
		$api_url = $this->config->item('api_url').'/academic/educational_level';
		$client = new GuzzleHttp\Client();
		try{
			$response = $client->get($api_url);
		} catch (RequestException $e) {
		    		if ($e->hasResponse()) {
		        		$response = $e->getResponse();
		    		}
		}
		

		$educations = $response->json();	
		$count = count($educations);
		$edudata = array();
		
		for($i=0;$i < $count; $i++){
			if($this->user['last_education'] == $educations[$i]){
				$edudata[$i] = '<option value="'.$educations[$i].'" selected="selected">'.$educations[$i].'</option>';
			}else{
				$edudata[$i] = '<option value="'.$educations[$i].'">'.$educations[$i].'</option>';
			}
		}
		
		return $edudata;
	}
	
	private function getUserEmploymentData(){
		$api_url = $this->config->item('api_url').'/employment';
		$client = new GuzzleHttp\Client();
		try{
			$response = $client->get($api_url);
		} catch (RequestException $e) {
		    		if ($e->hasResponse()) {
		        		$response = $e->getResponse();
		    		}
		}
		

		$employments = $response->json();	
		$count = count($employments);
		$employments_data = array();
		
		for($i=0;$i < $count; $i++){
			if($this->user['employment'] == $employments[$i]){
				$employments_data[$i] = '<option value="'.$employments[$i].'" selected="selected">'.$employments[$i].'</option>';
			}else{
				$employments_data[$i] = '<option value="'.$employments[$i].'">'.$employments[$i].'</option>';
			}
		}
		
		return $employments_data;		
	}
	
	private function getUserOfficeProvinceData(){
		$client = new GuzzleHttp\Client();
		try
		{			
			//request user detail
			$response_server = $client->get(base_url().'api/location/province');
		}catch (RequestException $e) {
    		if ($e->hasResponse()) {
        		$response_server = $e->getResponse();
    		}
		}
		
		$provinces = $response_server->json();
		$province_data = array();
		foreach($provinces as $province){
			if(isset($this->user['office_province']['id']) && $this->user['office_province']['id'] == $province['id']){
				$province_data[] = '<option value="'.$province['id'].'" selected="selected">'.$province['name'].'</option>';
			}else{
				$province_data[] = '<option value="'.$province['id'].'">'.$province['name'].'</option>';
			}
			
		}
		
		//request district
		$districts_data = '';
		
		if(isset($this->user['office_province']['id'])){
		    try
		    {			
			    //request user detail
			    $response_server = $client->get(base_url().'api/location/district/'.$this->user['office_province']['id']);
		    }catch (RequestException $e) {
		    if ($e->hasResponse()) {
			    $response_server = $e->getResponse();
		    }
		    }
		    
		    $districts = $response_server->json();
		    $districts_data = array();
		    
		    foreach($districts as $district){
			    if(isset($this->user['office_district']['id']) && $this->user['office_district']['id'] == $district['id']){
				    $districts_data[] = '<option value="'.$district['id'].'" selected="selected">'.$district['name'].'</option>';
			    }else{
				    $districts_data[] = '<option value="'.$district['id'].'">'.$district['name'].'</option>';
			    }			
		    }
		    
		}
		
		//request subdistrict	
		$data['user_office_provinces'] = $province_data;
		$data['user_office_districts'] = $districts_data;
				
		return $data;		
	}
	
	private function proccess_lpp($data){
		//check training status
		if($data->training_institute_status == 'negeri'){
			//proccess negeri lpp
			unset($data->responsible_person_ktp);
			unset($data->responsible_person_ktp_upload);
			//eliminate surat tugas
			unset($data->training_task_swasta);
			$data->training_task = $data->training_task_negeri;
			unset($data->training_task_negeri);
			if($data->training_task == 'ya'){
				$data->training_task_upload = $data->training_task_negeri_upload;
				unset($data->training_task_negeri_upload);
				unset($data->training_task_swasta_upload);
			}else{
				//eliminate training task upload field
				unset($data->training_task_negeri_upload);
				unset($data->training_task_swasta_upload);
			}
			//financial_requirement_upload
			unset($data->financial_npwp_number);
			unset($data->financial_requirement_npwp_upload);
						
		}else{
			//this swasta
			//eliminate nip			
			unset($data->responsible_person_nip);
			//surat tugas
			unset($data->training_task_negeri);
			$data->training_task = $data->training_task_swasta;
			unset($data->training_task_swasta);			
			if($data->training_task == 'ya'){
				$data->training_task_upload = $data->training_task_swasta_upload;
				unset($data->training_task_negeri_upload);
				unset($data->training_task_swasta_upload);
			}else{
				unset($data->training_task_negeri_upload);
				unset($data->training_task_swasta_upload);				
			}
			//financial requirement
			$data->financial_requirement_upload = $data->financial_requirement_npwp_upload;
			unset($data->financial_requirement_npwp_upload);						
		}
	}
}
?>