<?php
require_once APPPATH.'/controllers/api/Task.php';
require_once APPPATH.'/controllers/Notification.php';
class Login extends CI_Controller{
    function __construct(){
	parent::__construct();
    }

    function index(){
		redirect('');
    }
	
	function activated_email_post(){
		
		$email = $this->input->post('email');
		
		$query = $this->user_db->get_all(array('email'=> $email, 'verified_email' => 0));
		if(count($query) == 1){
			
			$result = $query[0];
			$data_input = array(
								'verified_email_code' => sha1_salt(time())
								);
			if($callback = $this->user_db->save($result['_id']->{'$id'}, $data_input)){
				
				//send email confirmation here
				$Notification = new Notification();
				$Notification->email_confirmation($email = $result['email'], $fullname = $result['fullname'], $verification_email_code = $data_input['verified_email_code']);
				
				$notif = array('status' => true, 'msg' => 'Verifikasi email '.$email.' sudah dikirim, silahkan cek kotak masuk email anda.');
			}else{
				$notif = array('status' => false, 'msg' => 'System Error');
			}	
		}else{
			$notif = array('status' => false, 'msg' => 'Email '.$email.' anda sudah aktif / tidak ditemukan');
			
		}
		
		$this->session->set_userdata('notif', $notif);
		redirect('');
	}
	
    function forgot_password_post(){
	$result = false;
	
	if($this->user_db->email_exist($this->input->post('email'))){
	    
	    $generate_code = sha1_salt(time());
	    
	    $query = $this->user_db->get_all($filter = array('email' => $this->input->post('email')));
	    if($query && count($query)==1){
		$user = $query[0];
		
		//update
		$data_input['forgot_password_code'] = $generate_code;
		if($this->user_db->save($user['_id']->{'$id'}, $data_input)){
			
			/*
			$data['name'] = $user['fullname'];
			$data['confirmation_link'] = site_url('login/confirm_forgot_password/'.$generate_code);
			$message = $this->load->view('templates/email_forgot_password_confirmation', $data,TRUE);

		    //send email
		    $subject = 'Konfirmasi Reset Password '.$this->config->item('title_short');
		    $Task = new Task();
		    $Task->send_email($to = $this->input->post('email'), $subject, $message);
		    */
		    $Notification = new Notification();
		    $Notification->forgot_password_confirmation($email = $this->input->post('email'),  $user['fullname'], $verification_code = $generate_code);
		    
		    $result = true;
		}
		
	    }
	}
	
	if($result){
	    $this->session->set_userdata('notif', array('status' => true, 'msg' => 'Silahkan cek inbox email kamu untuk konfirmasi.'));
	}else{
	    $this->session->set_userdata('notif', array('status' => false, 'msg' => 'Gagal, Email tidak ditemukan.'));
	}
	redirect('');
    }
    
    
    function confirm_forgot_password($generate_code = null){
	$result = false;
	$query = $this->user_db->get_all($filter = array('forgot_password_code' => $generate_code));
	if($query && count($query)==1){
	    $user = $query[0];
	    
	    //update
	    $password = sha1_salt($generate_code);
	    $data_input['password'] = sha1_salt($password);	    
	    $data_input['forgot_password_code'] = '';
	    if($this->user_db->save($user['_id']->{'$id'}, $data_input)){
		//send email
		/*
		$data['password'] = $password;
		$message = $this->load->view('templates/email_new_password', $data,TRUE);				
		$subject = 'Konfirmasi Reset Password '.$this->config->item('title_short');
		$Task = new Task();
		$Task->send_email($to = $user['email'], $subject, $message);
		*/
		$Notification = new Notification();
		$Notification->forgot_password_success($email = $user['email'],  $user['fullname'], $password);
		$result = true;
	    }
	    
	}
	
	
	if($result){
	    $this->session->set_userdata('notif', array('status' => true, 'msg' => 'Berhasil, Password kamu telah dikirim ke email.'));
	}else{
	    $this->session->set_userdata('notif', array('status' => false, 'msg' => 'Gagal'));
	}
	redirect('');
    }
}
?>