<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
  img to base64
*/
function img_to_base64($path_file){
    $path = $path_file;
    $type = pathinfo($path, PATHINFO_EXTENSION);
    $data = file_get_contents($path);
    $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
    
    return $base64;
}
/*
  check valid request
*/

/*
  check token
*/

function call_api_get($url){
    $curl = curl_init();

    curl_setopt_array($curl, array(
	CURLOPT_URL => $url,
// 	CURLOPT_HEADER => true,
	CURLOPT_RETURNTRANSFER => true,
	CURLOPT_ENCODING => "",
	CURLOPT_MAXREDIRS => 10,
	CURLOPT_TIMEOUT => 30,
	CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	CURLOPT_CUSTOMREQUEST => "GET",
	CURLOPT_HTTPHEADER => array(
	  "content-type: application/json"
	),
    ));

    $response = curl_exec($curl);
    $httpcode = curl_getinfo($curl);
    $err = curl_error($curl);

    curl_close($curl);

    if ($err) {
      return false;
    } else {
      return array('header_info' => $httpcode, 'body' => $response);
    }
    
}

function call_api_post($url, $input){
    $input = json_encode($input);
    $curl = curl_init();
    curl_setopt_array($curl, array(
	CURLOPT_URL => $url,
	CURLOPT_RETURNTRANSFER => true,
	CURLOPT_ENCODING => "",
	CURLOPT_MAXREDIRS => 10,
	CURLOPT_TIMEOUT => 30,
	CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	CURLOPT_CUSTOMREQUEST => "POST",
	CURLOPT_POSTFIELDS => $input,
	CURLOPT_HTTPHEADER => array(
	  "Content-Type: application/json"
	),
    ));
    
    $response = curl_exec($curl);
    $httpcode = curl_getinfo($curl);
    $err = curl_error($curl);

    curl_close($curl);
	//print_r($response);die;

    if ($err) {
      return false;
    } else {
      return array('header_info' => $httpcode, 'body' => $response);
    }
    
}

function call_api_put($url, $input){
    $input = json_encode($input);
    $curl = curl_init();
    curl_setopt_array($curl, array(
	CURLOPT_URL => $url,
	CURLOPT_RETURNTRANSFER => true,
	CURLOPT_ENCODING => "",
	CURLOPT_MAXREDIRS => 10,
	CURLOPT_TIMEOUT => 30,
	CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	CURLOPT_CUSTOMREQUEST => "PUT",
	CURLOPT_POSTFIELDS => $input,
	CURLOPT_HTTPHEADER => array(
	  "Content-Type: application/json"
	),
    ));
    
    $response = curl_exec($curl);
    $httpcode = curl_getinfo($curl);
    $err = curl_error($curl);

    curl_close($curl);


    if ($err) {
      return false;
    } else {
      return array('header_info' => $httpcode, 'body' => $response);
    }
    
}

function check_token($token=null, $allow_role=array()){

//     echo 'help';
    $CI =& get_instance();
    
    $allow = false;
    
    if($allow_role && $token){
	$api_url = $CI->config->item('connect_url').'/oauth/check_token?access_token='.$token;
	$response = call_api_get($api_url);
	
	if($response['header_info']['http_code'] == 200){
	    
	    $response_body = json_decode($response['body'], true);
	    $user_id = $response_body['user_id'];
	    
	    
	    foreach($allow_role as $row){
		if(in_array($row, $response_body['role'])){
		    $allow = true; break;
		}
	    }
	    
	}
	
    }
//     print_r($allow_role);
    if(!$allow){
	echo 'kucuy';
	redirect($CI->config->item('connect_url').'/authorize?response_type=code&client_id='.$CI->config->item('client_id').'&state='.base64_encode(current_url()));
    }
}



function config_kcfinder($data = array()){
    
    
    $CI =& get_instance();
    
//     print_r($session_user['_id']->{'$id'});

    if($data){
	
	if (!file_exists(getcwd().'/assets/users/'.$data['user_id'].'/')) {
	    mkdir(getcwd().'/assets/users/'.$data['user_id'], 0777);
// 	    echo "The directory $dirname was successfully created.";
// 	    exit;
	}
	
	$_SESSION['KCFINDER'] = array(
	    'disabled' => false,
	    'uploadURL' => $CI->config->item('users')."/".$data['user_id'],
	    'access' => array(

	      'files' => array(
		  'upload' => true,
		  'delete' => true,
		  'copy'   => true,
		  'move'   => true,
		  'rename' => true
	      ),

	      'dirs' => array(
		  'create' => true,
		  'delete' => true,
		  'rename' => true
	      )
	  )
	);
    }
}

function check_date_format($date){
    if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$date)){
        return true;
    }else{
        return false;
    }
}

/*
  sha1 dengan salt key
*/
function sha1_salt($text){
    $CI =& get_instance();
    return sha1($text.':'.$CI->config->item('encryption_key'));
}

function check_request($param, $variable, $optional=null){
    //echo $param;
    
    if($param){
// 	print_r($param);
	
	foreach($variable as $row){
	    if(!isset($param[$row])){
// 		echo $row;
		return false;
	    }
	}
	
	
	return $param;
	
    }else{
	return false;
    }
}

/*
  md5 dengan salt key
*/
function md5_salt($text){
    $CI =& get_instance();
    return md5($text.':'.$CI->config->item('encryption_key'));
}

/*
  check token facebook
*/

function check_token_facebook($token_fb){
    $result = false;
    
    $param = http_build_query(array(
	'access_token' => $token_fb
    ));
    
    $url = 'https://graph.facebook.com/v2.0/me?'.$param;
    
    if($content = json_decode(file_get_contents($url), true)){
	if(isset($content['error'])){
	    return false;
	}else{
	    return true;
	}
    }
    
    return $result;
}

function get_friendlist_facebook_use_app($token_fb){
    
    $result = false;
    
    $param = http_build_query(array(
	'access_token' => $token_fb,
	'offset' => 0
    ));
    $url = 'https://graph.facebook.com/v2.0/me/friends?'.$param;
    
    if($content = json_decode(file_get_contents($url), true)){
	if(isset($content['error'])){
	    $result = false;
	}else{
	    /*
		format output
		array(
		    array([name] => <name>, [id] => <facebook_id)
		)
	    */
	    //print_r ($content['data']);
	    $result = $content;
	}
    }
    
    return $result;
}

/*
  check token twitter
*/

function check_token_twitter($token_twitter, $token_secret_twitter){
    $CI =& get_instance();
    $result = false;

    $settings = array(
	'oauth_access_token' => $token_twitter,
	'oauth_access_token_secret' => $token_secret_twitter,
	'consumer_key' => $CI->config->item('consumer_key'),
	'consumer_secret' => $CI->config->item('consumer_secret')
    );
    
    //print_r($settings);
    $url = 'https://api.twitter.com/1.1/account/verify_credentials.json';
    $getfield = '';
    $requestMethod = 'GET';
    $twitter = new TwitterAPIExchange($settings);
    $content = json_decode($twitter->buildOauth($url, $requestMethod)
		->performRequest(), true);
    
    if(isset($content['error']) || isset($content['errors'])){
	$result = false;
    }else{
	/*
	    format output
	    array(
		array([name] => <name>, [id] => <facebook_id)
	    )
	*/
	//print_r ($content['data']);
	$result = $content;
    }
    return $result;
}

function get_friendlist_twitter($token_twitter, $token_secret_twitter){
    $output = array();
    $result = false;
    $CI =& get_instance();
    
    if($user = check_token_twitter($token_twitter, $token_secret_twitter)){
	$screen_name = $user['screen_name'];
	$settings = array(
	    'oauth_access_token' => $token_twitter,
	    'oauth_access_token_secret' => $token_secret_twitter,
	    'consumer_key' => $CI->config->item('consumer_key'),
	    'consumer_secret' => $CI->config->item('consumer_secret')
	);
	
	$cursor = -1;
	
	do{
	    
	    $url = 'https://api.twitter.com/1.1/friends/ids.json';
	    $getfield = '?cursor='.$cursor.'&amp;screen_name='.$screen_name.'&amp;count=5000';
	    $requestMethod = 'GET';
	    $twitter = new TwitterAPIExchange($settings);
	    $content = '';
	    $content = $twitter->setGetfield($getfield)
				->buildOauth($url, $requestMethod)
				->performRequest();
	    
	    if($content = json_decode($content, true)){
		//$result = $content; break;
		if(isset($content['error']) || isset($content['errors'])){
		    $result = false;
		    break;
		}else{
		    
		    $output = array_merge($output,$content['ids']);
		    $result = $output;
		    $cursor = $content['next_cursor_str'];
		}
	    }
	}while($cursor > 0);
    }else{
	$return = false;
    }

    return $result;
}

function get_namefriendlist_twitter($token_twitter, $token_secret_twitter){
    $output = array();
    $result = false;
    $CI =& get_instance();
    
    if($user = check_token_twitter($token_twitter, $token_secret_twitter)){
	$screen_name = $user['screen_name'];
	$settings = array(
	    'oauth_access_token' => $token_twitter,
	    'oauth_access_token_secret' => $token_secret_twitter,
	    'consumer_key' => $CI->config->item('consumer_key'),
	    'consumer_secret' => $CI->config->item('consumer_secret')
	);
	
	$cursor = -1;
	
	do{
	    
	    $url = 'https://api.twitter.com/1.1/friends/list.json';
	    $getfield = '?cursor='.$cursor.'&amp;screen_name='.$screen_name.'&amp;skip_status=true&amp;include_user_entities=false&amp;count=200';
	    $requestMethod = 'GET';
	    $twitter = new TwitterAPIExchange($settings);
	    $content = '';
	    $content = $twitter->setGetfield($getfield)
				->buildOauth($url, $requestMethod)
				->performRequest();
	    
	    if($content = json_decode($content, true)){
		//$result = $content; break;
		if(isset($content['error']) || isset($content['errors'])){
		    $result = false;
		    break;
		}else{
		    
		    $output = array_merge($output,$content['users']);
		    $result = $output;
		    $cursor = $content['next_cursor_str'];
		    //print_r($content);
		    //echo $cursor.' '; 
		}
	    }
	}while($cursor > 0);
    }else{
	$return = false;
    }
    //print_r($result);
    return $result;
}

function get_namefriendlist_limit_twitter($token_twitter, $token_secret_twitter, &$cursor=null){
    $output = array();
    $result = false;
    $CI =& get_instance();
    
    if($user = check_token_twitter($token_twitter, $token_secret_twitter)){
	$screen_name = $user['screen_name'];
	$settings = array(
	    'oauth_access_token' => $token_twitter,
	    'oauth_access_token_secret' => $token_secret_twitter,
	    'consumer_key' => $CI->config->item('consumer_key'),
	    'consumer_secret' => $CI->config->item('consumer_secret')
	);
	
	if($cursor==null)
	    $cursor = -1;
	
	$url = 'https://api.twitter.com/1.1/friends/list.json';
	$getfield = '?cursor='.$cursor.'&amp;screen_name='.$screen_name.'&amp;skip_status=true&amp;include_user_entities=false&amp;count=100';
	$requestMethod = 'GET';
	$twitter = new TwitterAPIExchange($settings);
	$content = '';
	$content = $twitter->setGetfield($getfield)
			    ->buildOauth($url, $requestMethod)
			    ->performRequest();
	
	if($content = json_decode($content, true)){
	    //$result = $content; break;
	    if(isset($content['error']) || isset($content['errors'])){
		$result = false;
		break;
	    }else{
		
		$output = array_merge($output,$content['users']);
		$result = $output;
		$cursor = $content['next_cursor_str'];
		//print_r($content);
		//echo $cursor.' '; 
	    }
	}
	
    }else{
	$return = false;
    }
    //print_r($result);
    return $result;
}

function time_to_day($time){
    return floor((time()-$time)/(60*60*24));
}

function get_hashtag($text){
    
    preg_match_all("/(#\w+)/", $text, $matches);
    /*
    if($matches[0])
	return '"'.implode('", "', $matches[0]).'"';
    else
	return false;
    */
    return $matches[0];
}

function get_random_code($length=4){
    $an = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    $su = strlen($an) - 1;
    
    $string = '';
    
    for($i=0; $i<$length; $i++){
	$string .= substr($an, rand(0, $su), 1);
    }
    
    return $string;
}

/*
check token dipake di api
*/
function check_token_api($token=null, $allow_role=array()){
    $CI =& get_instance();
    $allow = false;
    
    if($allow_role && $token){
	$api_url = $CI->config->item('connect_url').'/oauth/check_token?access_token='.$token;
	$response = call_api_get($api_url);
	$response_body = json_decode($response['body'], true);
	
	if($response['header_info']['http_code'] == 200){
	    
	    
	    $user_id = $response_body['user_id'];
	    
	    $user['user_id'] = $user_id;
	    
// 	    $allow = true;
	    foreach($allow_role as $row){
		if(in_array($row, $response_body['role'])){
		    $allow = true; break;
		}
	    }
	    
	    if(!$allow){
		$CI->response(
		    array(
			'error'=> 'forbidden',
			'error_description' => 'forbidden access'
		    )
		,403); //forbidden
	    }
	}else{
	    $CI->response($response_body,$response['header_info']['http_code']);
	}
	
    }
    
    if(!$allow){
	$CI->response(
	    array(
		'error'=> 'forbidden',
		'error_description' => 'forbidden access'
	    )
	,403);
    }
}