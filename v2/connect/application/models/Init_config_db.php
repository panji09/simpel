<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Init_config_db extends CI_Model{
    function __construct(){
        // Call the Model constructor
        parent::__construct();
    }
    
    function get(){
	$query = $this->mongoci->get('init_config');
	return $query[0];
    }
	
    function get_pns_rank(){
	return $this->get()['pns_rank'];
    }
    
    function get_entity(){
	return $this->get()['entity'];
    }
    
    function get_institution(){
	return $this->get()['institution'];
    }
    
    function get_employment(){
	return $this->get()['employment'];
    }
    
    function get_educational_level(){
	return $this->get()['educational_level'];
    }
	
	function generate_data($row){
		$temp['name'] =  $row;
		return $temp;	
	}
	
	
	function generate_mysql(){
		$this->db->empty_table('pns_rank');
		$this->db->empty_table('entity');
		$this->db->empty_table('educational_level');		
		$this->db->empty_table('employment');	
		$this->db->empty_table('institution');			
		
		$data_pns_rank = array();		
		$data_entity = array();		
		$data_educational_level = array();	
		$data_employment = array();	
		$data_institution = array();					
		
		$pns_rank  = $this->get_pns_rank();
		if($pns_rank){
			foreach($pns_rank as $row){
				$data_pns_rank[] = $this->generate_data($row);
			}
			$this->db->insert_batch('pns_rank', $data_pns_rank);
		}
		
		$entity = $this->get_entity();
		if($entity){
			foreach($entity as $row){
				$data_entity[] = $this->generate_data($row);
			}
			$this->db->insert_batch('entity', $data_entity);
		}
			
		$educational_level = $this->get_educational_level($row);
		if($educational_level){
			foreach($educational_level as $row){
				$data_educational_level[] = $this->generate_data($row);
			}
			$this->db->insert_batch('educational_level', $data_educational_level);	
		}
		
		$employement = $this->get_employment();
		if($employement){
			foreach($employement as $row){
				$data_employment[] = $this->generate_data($row);
			}
			$this->db->insert_batch('employment', $data_employment);
		}
		
		$institution = $this->get_institution();
		if($institution){
			foreach($institution as $row){
				$data_institution[] = $this->generate_data($row);
			}
			$this->db->insert_batch('institution', $data_institution);						 	
		}		
	}
}
?>