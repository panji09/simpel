<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class User_db extends CI_Model{
    function __construct(){
        // Call the Model constructor
        parent::__construct();
    }
    
    function exist($id){
	return count($this->get_all(array('id' => $id))) == 1;
    }
	
    
    function get($id){
	return $this->get_all($filter = array('id' => $id));
    }
    
    function user_exist($username){
		return count($this->get_all(array('username' => $username))) == 1;
    }
    
    function email_exist($email){
		return count($this->get_all(array('email' => $email))) == 1;
    }

	function active_user_exist($username){
		return count($this->get_all(array(
										  'username' => $username,
										  'activated' => 1,
										  'verified_email' =>1
										  )
									)
					 ) == 1;
    }
    
    function active_email_exist($email){
		return count($this->get_all(array(
										  'email' => $email,
										  'activated' => 1,
										  'verified_email' =>1
										  )
									)
					 ) == 1;
    }
	
    
//     function get_by_token($token){
// 	return $this->get_all($filter = array('token' => $token));
//     }
    
    function get_all($filter=null, $limit=null, $offset=null){
	
	if(isset($filter['id']))
		$this->mongoci->where('_id',  new MongoId($filter['id']));
	    
	
	if(isset($filter['username']))
	    $this->mongoci->where('username',  $filter['username']);
	
	if(isset($filter['email']))
	    $this->mongoci->where('email',  $filter['email']);
	
	if(isset($filter['verified_email']))
	    $this->mongoci->where('verified_email',  intval($filter['verified_email']));
	
	//office_province
	if(isset($filter['office_province']))
	    $this->mongoci->where('office_province.id',  $filter['office_province']);
	
	
	if(isset($filter['entity']))
	    $this->mongoci->where('entity',  $filter['entity']);
	
	if(isset($filter['verified_institute']))
	    $this->mongoci->where('verified_institute',  $filter['verified_institute']);
	
	if(isset($filter['verified_institute_in']))
	    $this->mongoci->whereIn('verified_institute',  $filter['verified_institute_in']);
	
	if(isset($filter['password']))
	    $this->mongoci->where('password',  sha1_salt($filter['password']));
	
	if(isset($filter['verified_email_code']))
	    $this->mongoci->where('verified_email_code',  $filter['verified_email_code']);	
	
	if(isset($filter['forgot_password_code']))
	    $this->mongoci->where('forgot_password_code',  $filter['forgot_password_code']);	
	
	
// 	if(isset($filter['token']))
// 	    $this->mongoci->where('token',  $filter['token']);
// 	
	if(isset($filter['fullname'])){
	    $regex = '/' . $filter['fullname'] . '/i';
	    
	    $search_name = array(
		'fullname' => new MongoRegex($regex),
	    );
	    $this->mongoci->orWhere($search_name);
	    
	}
	
	if(isset($filter['search_name'])){
	    $regex = '/' . $filter['search_name'] . '/i';
	    
	    $search_name = array(
		'fullname' => new MongoRegex($regex),
	    );
	    $this->mongoci->Where($search_name);
	    
	}
	
	if(isset($filter['activated']))
	    $this->mongoci->where('activated',  $filter['activated']);
	
	if($limit)
	    $this->mongoci->limit($limit);
        if($offset)
	    $this->mongoci->offset($offset);
	    
	$this->mongoci->where('deleted',0);
        
        $this->mongoci->orderBy(array('created' => 'DESC'));
        
		
        $result =  $this->mongoci->get('user');
		
		return $result;
		
        //$this->mongoci->lastQuery();		
    }
    
    function save($user_id=null, $data_user=array(), $data_push=array()){
	$result = false;
	
	$last_update = array('time' => time(), 'ip' => $this->input->ip_address());
	
	if($user_id && $this->exist($user_id)){
	    //update
	    $this->mongoci->where('_id', new MongoId($user_id));
	    
	    if(isset($data_user['verified_institute'])){
		$data_user['verified_institute'] = intval($data_user['verified_institute']);
	    }
	    if($data_user)
		$this->mongoci->set($data_user);
	    
	    $this->mongoci->set('last_update', $last_update);
// 	    $this->mongoci->push('log_last_update', $last_update);
	    
	    if($data_push)
		$this->mongoci->push($data_push);

	    $result = $this->mongoci->update('user');
	    
	}else{
	    //insert
	    $data_user['last_update'] = $last_update;
// 	    $data_user['log_last_update'] = array($last_update);
	    $data_user['created'] = time();
	    $data_user['deleted'] = 0;
	    
	    if(!$this->user_exist($data_user['username']) && !$this->email_exist($data_user['email']))	
		$result = $this->mongoci->insert('user', $data_user);
		
		$user_id = ($result ? $result->{'$id'} : '');
	    
	}
	
		if($user_id){
			$this->update_mysql($user_id);
		}
	
	return $result;
    }
	 
    function delete($user_id=null){
		
	return $this->save($user_id, array('deleted' => 1));
    }
    
	
	function generate_data_user($row){
		$row_connect['id'] = $row['_id']->{'$id'};		
		$row_connect['photo'] = isset($row['photo']) ? $row['photo'] : '';
		$row_connect['username'] = isset($row['username']) ? $row['username'] : '';
		$row_connect['password'] = isset($row['password']) ? $row['password'] :'';		
		$row_connect['email'] = isset($row['email']) ? $row['email'] : '';
		$row_connect['fullname'] = isset($row['fullname']) ? $row['fullname'] : '';
		$row_connect['birth_place'] = isset($row['birth_place']) ? $row['birth_place'] : '';
		$row_connect['birth_date'] = isset($row['birth_date']) ? $row['birth_date'] : '';
		$row_connect['phone'] = isset($row['phone']) ? $row['phone'] : '';
		$row_connect['gender'] = isset($row['gender']) ? $row['gender'] : '';
		$row_connect['ktp_id'] = isset($row['ktp_id']) ? $row['ktp_id'] : '';
		$row_connect['created'] = isset($row['created']) ? $row['created'] : '';
		$row_connect['deleted'] = isset($row['deleted']) ? $row['deleted'] : '';
		
		return $row_connect;
	}
	
	function generate_data_user_detail($row){
	$data_user_detail['user_id'] = $row['_id']->{'$id'};			
	$data_user_detail['training_institute'] = isset($row['training_institute']) ? $row['training_institute'] : ' ';
	$data_user_detail['training_institute_status'] = isset($row['training_institute_status']) ? $row['training_institute_status'] : '';
	$data_user_detail['office_address'] = isset($row['office_address']) ? $row['office_address'] : '';
	$data_user_detail['office_province_id'] = isset($row['office_province']['id']) ? $row['office_province']['id'] : '';
	$data_user_detail['office_province_name'] = isset($row['office_province']['name']) ? $row['office_province']['name'] : '';
	$data_user_detail['office_district_id'] = isset($row['office_district']['id']) ? $row['office_district']['id'] : '';
	$data_user_detail['office_district_name'] = isset($row['office_district']['name']) ? $row['office_district']['name'] : '';		
	$data_user_detail['office_phone'] = isset($row['office_phone']) ? $row['office_phone'] : '';
	$data_user_detail['office_fax'] = isset($row['office_fax']) ? $row['office_fax'] : '';
	$data_user_detail['responsible_person_name'] = isset($row['responsible_person_name']) ? $row['responsible_person_name'] : '';
	$data_user_detail['responsible_person_position'] = isset($row['responsible_person_position']) ? $row['responsible_person_position'] : '';
	$data_user_detail['responsible_person_nip'] = isset($row['responsible_person_nip']) ? $row['responsible_person_nip'] : '';
	$data_user_detail['structure_organization_upload'] = isset($row['structure_organization_upload']) ? $row['structure_organization_upload'] : '';
	$data_user_detail['accreditation_upload'] = isset($row['accreditation_upload']) ? $row['accreditation_upload'] : '';
	$data_user_detail['commitment_upload'] = isset($row['commitment_upload']) ? $row['commitment_upload'] : '';
	$data_user_detail['financial_requirement_upload'] = isset($row['financial_requirement_upload']) ? $row['financial_requirement_upload'] : '';
	$data_user_detail['quality_management_upload'] = isset($row['quality_management_upload']) ? $row['quality_management_upload'] : '';
	$data_user_detail['standart_training_upload'] = isset($row['standart_training_upload']) ? $row['standart_training_upload'] : '';
	$data_user_detail['training_task'] = isset($row['training_task']) ? $row['training_task'] : '';
	$data_user_detail['training_task_upload'] = isset($row['training_task_upload']) ? $row['training_task_upload'] : '';
	$data_user_detail['entity'] = isset($row['entity']) ? $row['entity'] : '';
	$data_user_detail['role'] = isset($row['role']) ? implode(",", $row['role']) : '';
	$data_user_detail['verified_email'] = isset($row['verified_email']) ? $row['verified_email'] : '';
	$data_user_detail['verified_email_code'] = isset($row['verified_email_code']) ? $row['verified_email_code'] : '';
	$data_user_detail['deleted'] = isset($row['deleted']) ? $row['deleted'] : '';
	$data_user_detail['activated'] = isset($row['activated']) ? $row['activated'] : '';
	$data_user_detail['last_education'] = isset($row['last_education']) ? $row['last_education'] : '';
	$data_user_detail['academic_degree'] = isset($row['academic_degree']) ? $row['academic_degree'] : '';
	$data_user_detail['employment'] = isset($row['employment']) ? $row['employment'] : '';
	$data_user_detail['nip_id'] = isset($row['nip_id']) ? $row['nip_id'] : '';
	$data_user_detail['institution'] = isset($row['institution']) ? $row['institution'] : '';
	$data_user_detail['work_unit'] = isset($row['work_unit']) ? $row['work_unit'] : '';
	$data_user_detail['head_name'] = isset($row['head_name']) ? $row['head_name'] : '';
	$data_user_detail['head_email'] = isset($row['head_email']) ? $row['head_email'] : '';
	$data_user_detail['head_phone'] = isset($row['head_phone']) ? $row['head_phone'] : '';
	$data_user_detail['financial_npwp_number'] = isset($row['financial_npwp_number']) ? $row['financial_npwp_number'] : '';
	
	return $data_user_detail;
	}
	
	function generate_mysql(){
		
		$query = $this->get_all();
		$this->db->empty_table('user');
		$this->db->empty_table('user_detail');
		
		
		$data_user = array();
		$data_user_detail = array();
		foreach($query as $row){			
			$data_user[] = $this->generate_data_user($row);
			$data_user_detail[] = $this->generate_data_user_detail($row);
		} 
		
		 $this->db->insert_batch('user', $data_user);
		 $this->db->insert_batch('user_detail', $data_user_detail);
		 
	}
	
	function update_mysql($user_id = null){
		if($user_id){
			$query = $this->get($user_id);
			
			$data_user = $this->generate_data_user($query[0]);
			$data_user_detail = $this->generate_data_user_detail($query[0]);			
			
			$this->db->where('id', $user_id);
			$this->db->from('user');
			
			if($this->db->count_all_results() == 1){
				//update user
				$this->db->where('id', $user_id);
				$this->db->update('user', $data_user); 
				//update user detail
				$this->db->where('user_id', $user_id);
				$this->db->update('user_detail', $data_user_detail); 
			}else{
				//insert				
				$this->db->insert('user', $data_user); 
				$this->db->insert('user_detail', $data_user_detail); 
			}
		}
	}
	
}
?> 
