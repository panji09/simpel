<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Location_db extends CI_Model{
    function __construct(){
        // Call the Model constructor
        parent::__construct();
    }
    
    function get(){
	$query = $this->mongoci->get('location');
	return $query[0];
    }
    
    function get_province(){
	return $this->get()['provinsi'];
    }
    
    function get_district(){
	return $this->get()['kabkota'];
    }
    
    function get_subdistrict(){
	return $this->get()['kecamatan'];
    }
	
	function generate_data_provinsi($row){
		$temp['id'] = $row['id'];
		$temp['name'] =  isset($row['name']) ? $row['name'] : '';
		
		return $temp;
	}
	
	function generate_data_kabkota($row){
		$temp['id'] = $row['id'];
		$temp['name'] =  isset($row['name']) ? $row['name'] : '';
		$temp['provinsi_id'] =  isset($row['provinsi_id']) ? $row['provinsi_id'] : '';		
		return $temp;		
	}
	
	function generate_data_kecamatan($row){
		$temp['id'] = $row['id'];
		$temp['name'] =  isset($row['name']) ? $row['name'] : '';
		$temp['kabkota_id'] =  isset($row['kabkota_id']) ? $row['kabkota_id'] : '';		
		return $temp;		
	}
	
	function generate_mysql(){
		$this->db->empty_table('provinsi');
		$this->db->empty_table('kabkota');
		$this->db->empty_table('kecamatan');	
		
		$data_provinsi = array();		
		$data_kabkota = array();		
		$data_kecamatan = array();	
		
		$provinsi = $this->get_province();
		foreach($provinsi as $row){
			$data_provinsi[] = $this->generate_data_provinsi($row);
		}
		
		$kabkota = $this->get_district();
		foreach($kabkota as $row){
			$data_kabkota[] = $this->generate_data_kabkota($row);
		}
			
		$kecamatan = $this->get_subdistrict();
		foreach($kecamatan as $row){
			$data_kecamatan[] = $this->generate_data_kecamatan($row);
		}
				
		 $this->db->insert_batch('provinsi', $data_provinsi);
		 $this->db->insert_batch('kabkota', $data_kabkota);				
		 $this->db->insert_batch('kecamatan', $data_kecamatan);						 
					
	}
}
?>