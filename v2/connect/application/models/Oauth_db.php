<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Oauth_db extends CI_Model{
    function __construct(){
        // Call the Model constructor
        parent::__construct();
    }
    
    function authorization_code_get_all($filter=null){
    
	if(isset($filter['authorization_code']))
	    $this->mongoci->where('authorization_code',  $filter['authorization_code']);
	
	return $this->mongoci->get('oauth_authorization_codes');
        
    }
    
    function authorization_code_exist($authorization_code=null){
	return count($this->authorization_code_get_all(array('authorization_code' => $authorization_code))) == 1;
    }
    
    function authorization_codes_save($authorization_code=null, $data_input=array()){
	$result = false;
	
	if($authorization_code && $this->authorization_code_exist($authorization_code)){
	    //update
	    $this->mongoci->where('authorization_code', $authorization_code);
	    $this->mongoci->set($data_input);
	    $result = $this->mongoci->update('oauth_authorization_codes');
	}
	return $result;
    }
}
?>