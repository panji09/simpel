<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Academic_db extends CI_Model{
    function __construct(){
        // Call the Model constructor
        parent::__construct();
    }
    
    function get(){
	$query = $this->mongoci->get('academic');
	return $query[0];
    }
    
    function get_academic_degree(){
	return $this->get()['academic_degree'];
    }
    
    function get_educational_level(){
	return $this->get()['educational_level'];
    }
	
	function generate_educational_level($row){
		$temp['name'] =  $row;
		return $temp;
	}
	
	function generate_academic_degree($row){
		$temp['degree'] =  isset($row['degree']) ? $row['degree'] : '';		
		$temp['long_name'] =  isset($row['long_name']) ? $row['long_name'] : '';		
		$temp['sort_name'] =  isset($row['sort_name']) ? $row['sort_name'] : '';						
		return $temp;
	}	
	
	function generate_mysql(){
		$this->db->empty_table('educational_level');
		$this->db->empty_table('academic_degree');	
		
		$data_educational_level = array();		
		$data_academic_degree = array();		
		
		$educational_level = $this->get_educational_level();
		foreach($educational_level as $row){
			$data_educational_level[] = $this->generate_educational_level($row);
		}
		
		$academic_degree = $this->get_academic_degree();
		foreach($academic_degree as $row){
			$data_academic_degree[] = $this->generate_academic_degree($row);
		}
				
		 $this->db->insert_batch('educational_level', $data_educational_level);
		 $this->db->insert_batch('academic_degree', $data_academic_degree);									 
					
	}
}
?>