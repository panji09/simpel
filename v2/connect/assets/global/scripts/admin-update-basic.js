var UpdateBasic = function(){
	var oldUsername = "";
	var oldEmailAddress = "";
	
	var handleSubmitPhoto = function(){
		var formUrl = URL_BASE+"user/update/"+ID+"?access_token="+CODE;
		var itemData = jQuery.parseJSON(JSON.stringify($('#edit_change_photo').serializeObject()));		
		var sendInfo = JSON.stringify(itemData);		
		$.ajax({
		           type: "POST",
		           url: formUrl,
		           dataType: "json",
		           success: function (msg) {
		               if (msg) {
						   bootbox.dialog({
	                       message: "Update data berhasil.",
	                       title: "Update Sukses",
	                       buttons: {
	                         success: {
	                           label: "Success!",
	                           className: "green",
	                           callback: function() {
	                            	location.reload(true);
	                           }
	                         }
						 }
					 });						   
						   
		               } else {
		                   alert("Cannot add to list !");
		               }
		           },
				   error: function(xhr, status, error) {
					   bootbox.dialog({
                       message: "Update data tidak berhasil.",
                       title: "Update Gagal",
                       buttons: {
                         success: {
                           label: "OK!",
                           className: "green",
                           callback: function() {
                            	
                           }
                         }
					 }
				 });
				    },

		           data: sendInfo
		       });
	}
	
	var handleFileUpload = function(){
	      $('#photo_upload').fileupload({
		  dataType: 'json',
		  done: function (e, data) {
		      $.each(data.result.files, function (index, file) {
			$('#photo').val(file.url);
		      });
		  }
	      });
	      $('.fileinput-exists').click(function(){
		  $('#photo').val('');
	      });	      	      
	}
	
	var getKecamatan = function(id, component){
		$.ajax({
		    url: URL_API+"/location/subdistrict/" + id,
			type: "GET",
		    dataType: "jsonp",

		    // Work with the response
		    success: function( response ) {
				$.each(response, function(index, item){
					$(component).append("<option value='"+item.id+"'>"+item.name+"</option>");
				});
		    }
		});				
	}	
	
	var checkUsername = function(name, element, oldata){
		if(name == oldata) return;
		var sendInfo = JSON.stringify({username:name});
		console.log('send ' + sendInfo);
		$.ajax({
		    url: URL_API+"/register/check_username",
			type: "POST",
			contentType: "application/json; charset=utf-8",
			data:sendInfo,

		    // Work with the response
		    success: function( response ) {
			   console.log("success: " + response);
		    },
		   error: function(xhr, status, error) {
			   console.log("error: " + xhr.responseText);
           $(element)
               .closest('.form-group').removeClass('has-success').addClass('has-error'); // set error class to the
			   //insert error
			   var obj = $.parseJSON(xhr.responseText);
			   if($(element).attr("name") == "username"){
			   		$('#username-error').html(obj.error_description);
			   }else{
			   	 	$('#email-error').html("email exist");
			   }
			   
		    },
		});
	}
			
	var initOldData = function(){
		oldUsername = $("input[name$='username']").val();
		oldEmailAddress = $("input[name$='email']").val();
	}
	
	var handleSubmitPasswordData = function(){
		var formUrl = URL_BASE+"user/update/"+ID+"?access_token="+CODE;
		var itemData = jQuery.parseJSON(JSON.stringify($('#edit_change_password').serializeObject()));		
		var sendInfo = JSON.stringify(itemData);		
		$.ajax({
		           type: "POST",
		           url: formUrl,
		           dataType: "json",
		           success: function (msg) {
		               if (msg) {
						   bootbox.dialog({
	                       message: "Update data berhasil.",
	                       title: "Update Sukses",
	                       buttons: {
	                         success: {
	                           label: "Success!",
	                           className: "green",
	                           callback: function() {
	                            	location.reload(true);
	                           }
	                         }
						 }
					 });						   
						   
		               } else {
		                   alert("Cannot add to list !");
		               }
		           },
				   error: function(xhr, status, error) {
					   bootbox.dialog({
                       message: "Update data tidak berhasil.",
                       title: "Update Gagal",
                       buttons: {
                         success: {
                           label: "OK!",
                           className: "green",
                           callback: function() {
                            	
                           }
                         }
					 }
				 });
				    },

		           data: sendInfo
		       });
	}	
	
	var handleSubmitExtraParamData = function(){
		var formUrl = URL_BASE+"user/update/"+ID+"?access_token="+CODE;
		var itemData = jQuery.parseJSON(JSON.stringify($('#form_extra_param').serializeObject()));		
		var sendInfo = JSON.stringify(itemData);		
		$.ajax({
		           type: "POST",
		           url: formUrl,
		           dataType: "json",
		           success: function (msg) {
		               if (msg) {
						   bootbox.dialog({
	                       message: "Update data berhasil.",
	                       title: "Update Sukses",
	                       buttons: {
	                         success: {
	                           label: "Success!",
	                           className: "green",
	                           callback: function() {
	                            	location.reload(true);
	                           }
	                         }
						 }
					 });						   
						   
		               } else {
		                   alert("Cannot add to list !");
		               }
		           },
				   error: function(xhr, status, error) {
					   bootbox.dialog({
                       message: "Update data tidak berhasil.",
                       title: "Update Gagal",
                       buttons: {
                         success: {
                           label: "OK!",
                           className: "green",
                           callback: function() {
                            	
                           }
                         }
					 }
				 });
				    },

		           data: sendInfo
		       });
	}
	
	//serialize form to json object
	$.fn.serializeObject = function()
	{
	    var o = {};
	    var a = this.serializeArray();
	    $.each(a, function() {
	        if (o[this.name] !== undefined) {
	            if (!o[this.name].push) {
	                o[this.name] = [o[this.name]];
	            }
	            o[this.name].push(this.value || '');
	        } else {
	            o[this.name] = this.value || '';
	        }
	    });
	    return o;
	};
	
	var handleSubmitData = function(){
		handleFileUpload();
		var formUrl = URL_BASE+"user/update/"+ID+"?access_token="+CODE;
		var itemData = jQuery.parseJSON(JSON.stringify($('#form_user_basic').serializeObject()));			
		var sendInfo = JSON.stringify(itemData);

		$.ajax({
		           type: "POST",
		           url: formUrl,
		           dataType: "json",
		           success: function (msg) {
					   console.log(msg);
		               if (msg) {
						   bootbox.dialog({
	                       message: "Update data berhasil.",
	                       title: "Update Sukses",
	                       buttons: {
	                         success: {
	                           label: "Success!",
	                           className: "green",
	                           callback: function() {
	                            	location.reload(true);
	                           }
	                         }
						 }
					 });						   
						   
		               } else {
		                   alert("Cannot add to list !");
		               }
		           },
				   error: function(xhr, status, error) {
					   bootbox.dialog({
                       message: "Update data tidak berhasil.",
                       title: "Update Gagal",
                       buttons: {
                         success: {
                           label: "OK!",
                           className: "green",
                           callback: function() {
                            	
                           }
                         }
					 }
				 });
				    },

		           data: sendInfo
		       });
	}
	
	return {
		init:function(){
			var form_update_photo = $('#edit_change_photo');
			var form_extra_param = $('#form_extra_param');
            var form = $('#form_user_basic');
			var form_update_password = $('#edit_change_password');			
            var error = $('.alert-danger', form);
            var success = $('.alert-success', form);

			// validation for Peserta Pelatihan
            form.validate({
                doNotHideMessage: true, //this option enables to show the error/success messages on tab switch.
                errorElement: 'span', //default input error message container
                errorClass: 'help-block help-block-error', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                rules: {
                    //global user
					username: {
						minlength: 5,
						required: true,
					},
                    email: {
                        required: true,
                        email: true
                    },

					fullname: {
                        required: true
                    },
                    birth_place: {
                        required: true
                    },
					birth_date: {
						required: true
					},
                    phone: {
                        required: true
                    },
					'gender': {
                        required: true,
						minlength: 1
                    },
                    address: {
                        required: true,
						
                    },
                    province: {
                        required: true
                    },
                    district: {
                        required: true
                    },
                    subdistrict: {
                        required: false
                    }
                },
                messages: { // custom messages for radio buttons and checkboxes
                    'gender': {
                        required: "Pilih salah satu opsi",
                        minlength: jQuery.validator.format("Pilih salah satu opsi")
                    }
                },

                errorPlacement: function (error, element) { // render error placement for each input type
                    if (element.attr("name") == "gender") { // for uniform checkboxes, insert the after the given container
                        error.insertAfter("#form_gender_error");
                    } else if (element.attr("name") == "birth_date") {
						error.insertAfter("#form_tgl_lahir_error");
					} else {
                        error.insertAfter(element); // for other inputs, just perform default behavior
                    }
                },

                invalidHandler: function (event, validator) { //display error alert on form submit   
                    success.hide();
                    error.show();
                    Metronic.scrollTo(error, -200);
                },

                highlight: function (element) { // hightlight error inputs
                    $(element)
                        .closest('.form-group').removeClass('has-success').addClass('has-error'); // set error class to the control group
                },

                unhighlight: function (element) { // revert the change done by hightlight
                    $(element)
                        .closest('.form-group').removeClass('has-error'); // set error class to the control group
                },

                success: function (label) {
                    if (label.attr("for") == "gender") { // for checkboxes and radio buttons, no need to show OK icon
                        label
                            .closest('.form-group').removeClass('has-error').addClass('has-success');
                        label.remove(); // remove error label here
                    } else { // display success icon for other inputs
                        label
                            .addClass('valid') // mark the current input as valid and display OK icon
                        .closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
                    }
                },

                submitHandler: function (form) {
                    // success.show();
                    error.hide();
					handleSubmitData();
                    //add here some ajax code to submit your form or just call form.submit() if you want to submit the form without 
                }

            });
			
            form_update_password.validate({
                doNotHideMessage: true, //this option enables to show the error/success messages on tab switch.
                errorElement: 'span', //default input error message container
                errorClass: 'help-block help-block-error', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                rules: {
                    edit_password: {
                        minlength: 5,
                        required: true,
                        equalTo: "#edit_form_password"
                    },
                    r_edit_password: {
                        minlength: 5,
                        required: true,
                        equalTo: "#edit_form_password"
                    },
                },
                messages: { // custom messages for radio buttons and checkboxes
                    'gender': {
                        required: "Pilih salah satu opsi",
                        minlength: jQuery.validator.format("Pilih salah satu opsi")
                    }
                },

                errorPlacement: function (error, element) { // render error placement for each input type
                    if (element.attr("name") == "gender") { // for uniform checkboxes, insert the after the given container
                        error.insertAfter("#form_gender_error");
                    } else if (element.attr("name") == "birth_date") {
						error.insertAfter("#form_tgl_lahir_error");
					} else {
                        error.insertAfter(element); // for other inputs, just perform default behavior
                    }
                },

                invalidHandler: function (event, validator) { //display error alert on form submit   
                    success.hide();
                    error.show();
                    Metronic.scrollTo(error, -200);
                },

                highlight: function (element) { // hightlight error inputs
                    $(element)
                        .closest('.form-group').removeClass('has-success').addClass('has-error'); // set error class to the control group
                },

                unhighlight: function (element) { // revert the change done by hightlight
                    $(element)
                        .closest('.form-group').removeClass('has-error'); // set error class to the control group
                },

                success: function (label) {
                    if (label.attr("for") == "gender") { // for checkboxes and radio buttons, no need to show OK icon
                        label
                            .closest('.form-group').removeClass('has-error').addClass('has-success');
                        label.remove(); // remove error label here
                    } else { // display success icon for other inputs
                        label
                            .addClass('valid') // mark the current input as valid and display OK icon
                        .closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
                    }
                },

                submitHandler: function (form) {
                    // success.show();
                    error.hide();
					handleSubmitPasswordData();
                }
            });
						
            //initialize datepicker
            $('.tanggal-wizard').datepicker({
                rtl: Metronic.isRTL(),
                autoclose: true
            });
            $('.tanggal-wizard .form-control').change(function() {
                form.validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input 
            })
			
        
            $('#user_provinsi_list, #user_kabupaten_list, #user_kecamatan_list', form).change(function () {
                form.validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
            });
		
		    //apply validation on select2 dropdown value change, this only needed for chosen dropdown integration.
            // $('#country_list', form).change(function () {
            //     form.validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
            // });
			//check username
			$("input[name$='username']").focusout(function(){
				checkUsername($(this).val(), $(this), oldUsername);
			});
			//check email
			$("input[name$='email']").focusout(function(){
				checkUsername($(this).val(), $(this), oldEmailAddress);
			});
			
		    form_update_photo.submit(function( event ) {
	// 	      alert( "Handler for .submit() called." );
	// 	      event.preventDefault();
		      handleSubmitPhoto();
	      
		    });
		    
		    form_extra_param.submit(function( event ) {
	// 	      alert( "Handler for .submit() called." );
	// 	      event.preventDefault();
		      handleSubmitExtraParamData();
	      
		    });
			
			initOldData();
		}	
	};
}();