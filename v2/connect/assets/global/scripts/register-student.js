var Register = function(){
	var handleFileUpload = function(){
	      $('#photo_upload').fileupload({
		  dataType: 'json',
		  done: function (e, data) {
		      $.each(data.result.files, function (index, file) {
			$('#photo').val(file.url);
		      });
		  }
	      });
	      $('.fileinput-exists').click(function(){
		  $('#photo').val('');
	      });
	}
	var getKota = function(id, component){
		$.ajax({
		    url: URL_API+"/location/district/" + id,
			type: "GET",
		    dataType: "jsonp",

		    // Work with the response
		    success: function( response ) {
				$.each(response, function(index, item){
					$(component).append("<option value='"+item.id+"'>"+item.name+"</option>");
				});
		    }
		});				
	}
	
	var getKecamatan = function(id, component){
		$.ajax({
		    url: URL_API+"/location/subdistrict/" + id,
			type: "GET",
		    dataType: "jsonp",

		    // Work with the response
		    success: function( response ) {
				$.each(response, function(index, item){
					$(component).append("<option value='"+item.id+"'>"+item.name+"</option>");
				});
		    }
		});				
	}
	
	var checkUsername = function(name, element){
		var sendInfo = JSON.stringify({username:name});
		console.log('send ' + sendInfo);
		$.ajax({
		    url: URL_API+"/register/check_username",
			type: "POST",
			contentType: "application/json; charset=utf-8",
			data:sendInfo,

		    // Work with the response
		    success: function( response ) {
			   console.log("success: " + response);
		    },
		   error: function(xhr, status, error) {
			   console.log("error: " + xhr.responseText);
           $(element)
               .closest('.form-group').removeClass('has-success').addClass('has-error'); // set error class to the
			   //insert error
			   var obj = $.parseJSON(xhr.responseText);
			   if($(element).attr("name") == "username"){
			   		$('#username-error').html(obj.error_description);
			   }else{
			   	 	$('#email-error').html("email exist");
			   }
			   
		    },
		});
	}
	
	var handleProvinceData = function(){		
		//init province first
		$.ajax({
		    url: URL_API+"/location/province",
			type: "GET",
		    dataType: "jsonp",
		    // Work with the response
		    success: function( response ) {
				$.each(response, function(index, item){
					$("#user_provinsi_list").append("<option value='"+item.id+"'>"+item.name+"</option>");
					$("#domisili_provinsi_list_kerja").append("<option value='"+item.id+"'>"+item.name+"</option>");						
				});
		    }
		});
		
		//set selection tool on change callback
		$('#user_provinsi_list').on('change', function() {
			//clear old data
			var $this = $(this);
			$('#user_kabupaten_list').empty();
			$("#user_kabupaten_list").select2('data', {id: null, text:""});
			$("#user_kabupaten_list").append("<option></option>");
		    getKota($this.val(), "#user_kabupaten_list");
			$('#user_kabupaten_list').attr("data-placeholder", "Pilih");
			$('#user_kabupaten_list').data("select2").setPlaceholder();
			//now clear kabupaten
			$("#user_kecamatan_list").empty();
			$("#user_kecamatan_list").select2('data', {id: null, text:""});
			$("#user_kecamatan_list").append("<option></option>");
			$('#user_kecamatan_list').attr("data-placeholder", "Pilih");
			$('#user_kecamatan_list').data("select2").setPlaceholder();

		});
		
		$('#user_kabupaten_list').on('change', function() {
			var $this = $(this);
			$('#user_kecamatan_list').empty();
			$('#user_kecamatan_list').select2('data', {id: null, text:""});
			$('#user_kecamatan_list').append("<option></option>");
			//get kecamatan
		    getKecamatan($this.val(), "#user_kecamatan_list");
			$('#user_kecamatan_list').attr("data-placeholder", "Pilih");
			$('#user_kecamatan_list').data("select2").setPlaceholder();
		});

		$('#domisili_provinsi_list_kerja').on('change', function() {
			//clear old data
			var $this = $(this);
			$('#domisili_kabupaten_list_kerja').empty();
			$("#domisili_kabupaten_list_kerja").select2('data', {id: null, text:""});
			$("#domisili_kabupaten_list_kerja").append("<option></option>");
		    getKota($this.val(), "#domisili_kabupaten_list_kerja");
			$('#domisili_kabupaten_list_kerja').attr("data-placeholder", "Pilih");
			$('#domisili_kabupaten_list_kerja').data("select2").setPlaceholder();
		});
	}
	
	var handleLatestEducation = function(){
		$.ajax({
		    url: URL_API+"/academic/educational_level",
			type: "GET",
		    dataType: "jsonp",

		    // Work with the response
		    success: function( response ) {
				$.each(response, function(index, item){
					$("#pendidikan_list").append("<option value='"+item+"'>"+item+"</option>");
				});
		    }
		});			
	}
	
	var handleStatusKepegawaian = function(){
		$.ajax({
		    url: URL_API+"/employment",
			type: "GET",
		    dataType: "jsonp",

		    // Work with the response
		    success: function( response ) {
				$.each(response, function(index, item){
					$("#status_kepegawaian_list").append("<option value='"+item+"'>"+item+"</option>");
				});
		    }
		});			
	}
	
	//serialize form to json object
	$.fn.serializeObject = function()
	{
	    var o = {};
	    var a = this.serializeArray();
	    $.each(a, function() {
	        if (o[this.name] !== undefined) {
	            if (!o[this.name].push) {
	                o[this.name] = [o[this.name]];
	            }
	            o[this.name].push(this.value || '');
	        } else {
	            o[this.name] = this.value || '';
	        }
	    });
	    return o;
	};
	
	var handleSubmitData = function(){
		var formUrl = URL_BASE+"oauth/handle_register/peserta";		
		var itemData = jQuery.parseJSON(JSON.stringify($('#submit_form').serializeObject()));

		//office province
		var officeProvince = new Object();
		officeProvince.id =  $("#domisili_provinsi_list_kerja").val();
		officeProvince.name = $("#domisili_provinsi_list_kerja").select2('data').text;		
		var officeDistrict = new Object();
		//office district
		officeDistrict.id =  $("#domisili_kabupaten_list_kerja").val();
		officeDistrict.name = $("#domisili_kabupaten_list_kerja").select2('data').text;		
		//set data
		itemData.office_province = officeProvince;
		itemData.office_district = officeDistrict;		
		//pass data			
		var sendInfo = JSON.stringify(itemData);
		
		$.ajax({
		           type: "POST",
		           url: formUrl,
		           dataType: "json",
		           success: function (msg) {
					   console.log(msg);
		               if (msg) {
						   bootbox.dialog({
	                       message: "Selamat pendaftaran Anda berhasil. Silahkan cek email Anda untuk melakukan verifikasi.",
	                       title: "Registrasi Sukses",
	                       buttons: {
	                         success: {
	                           label: "Success!",
	                           className: "green",
	                           callback: function() {
//	                            	location.reload(true);
									window.location.assign(msg.refferal);
	                           }
	                         }
						 }
					 });						   
						   
		               } else {
		                   alert("Cannot add to list !");
		               }
		           },
				   error: function(xhr, status, error) {
						response = jQuery.parseJSON( xhr.responseText );
						textoutput = '';
						
						if(response.error == 'email_exist'){
							textoutput = 'Email anda sudah terdaftar!';
						}else if(response.error == 'username_exist' ){
							textoutput = 'Username anda sudah terdaftar!';
						}else if(response.error == 'invalid_field' ){
							textoutput = 'kolom isian kurang lengkap!';
						}else{
							textoutput = response.error_description;
						}
						
						bootbox.dialog({
							message: "Pendaftaran Anda belum berhasil. <br>"+textoutput,
							title: "Registrasi Gagal",
							buttons: {
								success: {
									label: "OK!",
									className: "green",
									callback: function() {
								
									}
								}
							}
						});
				    },

		           data: sendInfo
		       });
	}
	
	// Autocompletion
    var handleTwitterTypeahead = function() {

		// Gelar akademis
        var gelarAkademis = new Bloodhound({
          datumTokenizer: function(d) { return Bloodhound.tokenizers.whitespace(d.name); },
          queryTokenizer: Bloodhound.tokenizers.whitespace,
          limit: 10,
          prefetch: {
            url: URL_API+'/academic/academic_degree/sort_name',
            filter: function(list) {
              return $.map(list, function(country) { return { name: country }; });
            }
          }
        });

        gelarAkademis.initialize();

        if (Metronic.isRTL()) {
          $('#academic_degree').attr("dir", "rtl");
        }
		
		
        // $('#academic_degree').typeahead(null, {
        //   name: 'academic_degree',
        //   displayKey: 'name',
        //   hint: (Metronic.isRTL() ? false : true),
        //   source: gelarAkademis.ttAdapter()
        // });

		$('#academic_degree').tagsinput({
		  typeaheadjs: {
		    name: 'academic_degree',
		    displayKey: 'name',
		    valueKey: 'name',
			hint: (Metronic.isRTL() ? false : true),
		    source: gelarAkademis.ttAdapter()
		  }
		});
		
		
		// Institusi
        var institution = new Bloodhound({
          datumTokenizer: function(d) { return Bloodhound.tokenizers.whitespace(d.name); },
          queryTokenizer: Bloodhound.tokenizers.whitespace,
          limit: 10,
          prefetch: {
            url: URL_API+'/institution',
            filter: function(list) {
              return $.map(list, function(country) { return { name: country }; });
            }
          }
        });

        institution.initialize();

        if (Metronic.isRTL()) {
          $('#institution').attr("dir", "rtl");
        }
        $('#institution').typeahead(null, {
          name: 'institution',
          displayKey: 'name',
          hint: (Metronic.isRTL() ? false : true),
          source: institution.ttAdapter()
        });
    }
	
	
	var handleStatusEmployee = function(){
		$('.pns').hide();	
		$('#status_kepegawaian_list').on('change', function() {
			//clear old data
			var $this = $(this);
			if($this.val() == 'PNS' || $this.val() == 'TNI' || $this.val() == 'POLRI'){
				$('.pns').show();
			}else{
				$('.pns').hide();				
			}
		});
	}
	
	return {
		init:function(){
            if (!jQuery().bootstrapWizard) {
                return;
            }
			
			handleProvinceData();
			handleLatestEducation();
			handleStatusKepegawaian();
			handleStatusEmployee();
			
			handleFileUpload();
// 			handleUpload();
			
			// autocompletion
			handleTwitterTypeahead();
			
			
			//validation
            var form = $('#submit_form');
            var error = $('.alert-danger', form);
            var success = $('.alert-success', form);

			// validation for Peserta Pelatihan
            form.validate({
                doNotHideMessage: true, //this option enables to show the error/success messages on tab switch.
                errorElement: 'span', //default input error message container
                errorClass: 'help-block help-block-error', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                rules: {
                    //global user
					username: {
						minlength: 5,
						required: true,
					},
                    email: {
                        required: true,
                        email: true
                    },
                    remail: {
                        required: true,
                        email: true,
						equalTo: "#submit_form_email"
                    },
                    password: {
                        minlength: 7,
                        required: true,
                        equalTo: "#submit_form_password"
                    },
                    rpassword: {
                        minlength: 7,
                        required: true,
                        equalTo: "#submit_form_password"
                    },
					fullname: {
                        required: true
                    },
                    birth_place: {
                        required: true
                    },
					birth_date: {
						required: true
					},
                    phone: {
                        required: true
                    },
					'gender': {
                        required: true,
						minlength: 1
                    },
                    ktp_id: {
                        required: true
                    },
                    nip_id: {
                        required: true
                    },
					institution:{
						required: true
					},
                    office_province: {
                        required: true
                    },
                    office_zip_code: {
                        required: true
                    },
		    office_address: {
                        required: true
                    },
		    office_district: {
                        required: true
                    },                   
					last_education: {
						required: true
					},                  
                    academic_degree: {
                        required: true,						
                    },
					employment:{
						required: true
					}
                },
                messages: { // custom messages for radio buttons and checkboxes
                    'gender': {
                        required: "Pilih salah satu opsi",
                        minlength: jQuery.validator.format("Pilih salah satu opsi")
                    }
                },

                errorPlacement: function (error, element) { // render error placement for each input type
                    if (element.attr("name") == "gender") { // for uniform checkboxes, insert the after the given container
                        error.insertAfter("#form_gender_error");
                    } else if (element.attr("name") == "birth_date") {
						error.insertAfter("#form_tgl_lahir_error");
					} else {
                        error.insertAfter(element); // for other inputs, just perform default behavior
                    }
                },

                invalidHandler: function (event, validator) { //display error alert on form submit   
                    success.hide();
                    error.show();
                    Metronic.scrollTo(error, -200);
                },

                highlight: function (element) { // hightlight error inputs
                    $(element)
                        .closest('.form-group').removeClass('has-success').addClass('has-error'); // set error class to the control group
                },

                unhighlight: function (element) { // revert the change done by hightlight
                    $(element)
                        .closest('.form-group').removeClass('has-error'); // set error class to the control group
                },

                success: function (label) {
                    if (label.attr("for") == "gender") { // for checkboxes and radio buttons, no need to show OK icon
                        label
                            .closest('.form-group').removeClass('has-error').addClass('has-success');
                        label.remove(); // remove error label here
                    } else { // display success icon for other inputs
                        label
                            .addClass('valid') // mark the current input as valid and display OK icon
                        .closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
                    }
                },

                submitHandler: function (form) {
                    success.show();
                    error.hide();
					
                    //add here some ajax code to submit your form or just call form.submit() if you want to submit the form without ajax
                }

            });
	    
            //initialize datepicker
            $('.tanggal-wizard').datepicker({
                rtl: Metronic.isRTL(),
                autoclose: true
            });
            $('.tanggal-wizard .form-control').change(function() {
                form.validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input 
            })
			

            var displayConfirm = function() {
                $('#tab3 .form-control-static', form).each(function(){
                    var input = $('[name="'+$(this).attr("data-display")+'"]', form);
                    if (input.is(":radio")) {
                        input = $('[name="'+$(this).attr("data-display")+'"]:checked', form);
                    }
                    if (input.is(":hidden") || input.is(":text") || input.is("textarea")) {
                        if($(input).data('is_url')=="1"){
				    
			    $(this).html('<a href="'+input.val()+'" target="_blank">'+input.attr('data-filename')+'</a>');
			}else if($(input).data('is_img')=="1"){
			    var img_src = input.val();
			    if(img_src == ''){
			      img_src = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMgAAACWCAYAAACb3McZAAAKQWlDQ1BJQ0MgUHJvZmlsZQAASA2dlndUU9kWh8+9N73QEiIgJfQaegkg0jtIFQRRiUmAUAKGhCZ2RAVGFBEpVmRUwAFHhyJjRRQLg4Ji1wnyEFDGwVFEReXdjGsJ7601896a/cdZ39nnt9fZZ+9917oAUPyCBMJ0WAGANKFYFO7rwVwSE8vE9wIYEAEOWAHA4WZmBEf4RALU/L09mZmoSMaz9u4ugGS72yy/UCZz1v9/kSI3QyQGAApF1TY8fiYX5QKUU7PFGTL/BMr0lSkyhjEyFqEJoqwi48SvbPan5iu7yZiXJuShGlnOGbw0noy7UN6aJeGjjAShXJgl4GejfAdlvVRJmgDl9yjT0/icTAAwFJlfzOcmoWyJMkUUGe6J8gIACJTEObxyDov5OWieAHimZ+SKBIlJYqYR15hp5ejIZvrxs1P5YjErlMNN4Yh4TM/0tAyOMBeAr2+WRQElWW2ZaJHtrRzt7VnW5mj5v9nfHn5T/T3IevtV8Sbsz55BjJ5Z32zsrC+9FgD2JFqbHbO+lVUAtG0GQOXhrE/vIADyBQC03pzzHoZsXpLE4gwnC4vs7GxzAZ9rLivoN/ufgm/Kv4Y595nL7vtWO6YXP4EjSRUzZUXlpqemS0TMzAwOl89k/fcQ/+PAOWnNycMsnJ/AF/GF6FVR6JQJhIlou4U8gViQLmQKhH/V4X8YNicHGX6daxRodV8AfYU5ULhJB8hvPQBDIwMkbj96An3rWxAxCsi+vGitka9zjzJ6/uf6Hwtcim7hTEEiU+b2DI9kciWiLBmj34RswQISkAd0oAo0gS4wAixgDRyAM3AD3iAAhIBIEAOWAy5IAmlABLJBPtgACkEx2AF2g2pwANSBetAEToI2cAZcBFfADXALDIBHQAqGwUswAd6BaQiC8BAVokGqkBakD5lC1hAbWgh5Q0FQOBQDxUOJkBCSQPnQJqgYKoOqoUNQPfQjdBq6CF2D+qAH0CA0Bv0BfYQRmALTYQ3YALaA2bA7HAhHwsvgRHgVnAcXwNvhSrgWPg63whfhG/AALIVfwpMIQMgIA9FGWAgb8URCkFgkAREha5EipAKpRZqQDqQbuY1IkXHkAwaHoWGYGBbGGeOHWYzhYlZh1mJKMNWYY5hWTBfmNmYQM4H5gqVi1bGmWCesP3YJNhGbjS3EVmCPYFuwl7ED2GHsOxwOx8AZ4hxwfrgYXDJuNa4Etw/XjLuA68MN4SbxeLwq3hTvgg/Bc/BifCG+Cn8cfx7fjx/GvyeQCVoEa4IPIZYgJGwkVBAaCOcI/YQRwjRRgahPdCKGEHnEXGIpsY7YQbxJHCZOkxRJhiQXUiQpmbSBVElqIl0mPSa9IZPJOmRHchhZQF5PriSfIF8lD5I/UJQoJhRPShxFQtlOOUq5QHlAeUOlUg2obtRYqpi6nVpPvUR9Sn0vR5Mzl/OX48mtk6uRa5Xrl3slT5TXl3eXXy6fJ18hf0r+pvy4AlHBQMFTgaOwVqFG4bTCPYVJRZqilWKIYppiiWKD4jXFUSW8koGStxJPqUDpsNIlpSEaQtOledK4tE20Otpl2jAdRzek+9OT6cX0H+i99AllJWVb5SjlHOUa5bPKUgbCMGD4M1IZpYyTjLuMj/M05rnP48/bNq9pXv+8KZX5Km4qfJUilWaVAZWPqkxVb9UU1Z2qbapP1DBqJmphatlq+9Uuq43Pp893ns+dXzT/5PyH6rC6iXq4+mr1w+o96pMamhq+GhkaVRqXNMY1GZpumsma5ZrnNMe0aFoLtQRa5VrntV4wlZnuzFRmJbOLOaGtru2nLdE+pN2rPa1jqLNYZ6NOs84TXZIuWzdBt1y3U3dCT0svWC9fr1HvoT5Rn62fpL9Hv1t/ysDQINpgi0GbwaihiqG/YZ5ho+FjI6qRq9Eqo1qjO8Y4Y7ZxivE+41smsImdSZJJjclNU9jU3lRgus+0zwxr5mgmNKs1u8eisNxZWaxG1qA5wzzIfKN5m/krCz2LWIudFt0WXyztLFMt6ywfWSlZBVhttOqw+sPaxJprXWN9x4Zq42Ozzqbd5rWtqS3fdr/tfTuaXbDdFrtOu8/2DvYi+yb7MQc9h3iHvQ732HR2KLuEfdUR6+jhuM7xjOMHJ3snsdNJp9+dWc4pzg3OowsMF/AX1C0YctFx4bgccpEuZC6MX3hwodRV25XjWuv6zE3Xjed2xG3E3dg92f24+ysPSw+RR4vHlKeT5xrPC16Il69XkVevt5L3Yu9q76c+Oj6JPo0+E752vqt9L/hh/QL9dvrd89fw5/rX+08EOASsCegKpARGBFYHPgsyCRIFdQTDwQHBu4IfL9JfJFzUFgJC/EN2hTwJNQxdFfpzGC4sNKwm7Hm4VXh+eHcELWJFREPEu0iPyNLIR4uNFksWd0bJR8VF1UdNRXtFl0VLl1gsWbPkRoxajCCmPRYfGxV7JHZyqffS3UuH4+ziCuPuLjNclrPs2nK15anLz66QX8FZcSoeGx8d3xD/iRPCqeVMrvRfuXflBNeTu4f7kufGK+eN8V34ZfyRBJeEsoTRRJfEXYljSa5JFUnjAk9BteB1sl/ygeSplJCUoykzqdGpzWmEtPi000IlYYqwK10zPSe9L8M0ozBDuspp1e5VE6JA0ZFMKHNZZruYjv5M9UiMJJslg1kLs2qy3mdHZZ/KUcwR5vTkmuRuyx3J88n7fjVmNXd1Z752/ob8wTXuaw6thdauXNu5Tnddwbrh9b7rj20gbUjZ8MtGy41lG99uit7UUaBRsL5gaLPv5sZCuUJR4b0tzlsObMVsFWzt3WazrWrblyJe0fViy+KK4k8l3JLr31l9V/ndzPaE7b2l9qX7d+B2CHfc3em681iZYlle2dCu4F2t5czyovK3u1fsvlZhW3FgD2mPZI+0MqiyvUqvakfVp+qk6oEaj5rmvep7t+2d2sfb17/fbX/TAY0DxQc+HhQcvH/I91BrrUFtxWHc4azDz+ui6rq/Z39ff0TtSPGRz0eFR6XHwo911TvU1zeoN5Q2wo2SxrHjccdv/eD1Q3sTq+lQM6O5+AQ4ITnx4sf4H++eDDzZeYp9qukn/Z/2ttBailqh1tzWibakNml7THvf6YDTnR3OHS0/m/989Iz2mZqzymdLz5HOFZybOZ93fvJCxoXxi4kXhzpXdD66tOTSna6wrt7LgZevXvG5cqnbvfv8VZerZ645XTt9nX297Yb9jdYeu56WX+x+aem172296XCz/ZbjrY6+BX3n+l37L972un3ljv+dGwOLBvruLr57/17cPel93v3RB6kPXj/Mejj9aP1j7OOiJwpPKp6qP6391fjXZqm99Oyg12DPs4hnj4a4Qy//lfmvT8MFz6nPK0a0RupHrUfPjPmM3Xqx9MXwy4yX0+OFvyn+tveV0auffnf7vWdiycTwa9HrmT9K3qi+OfrW9m3nZOjk03dp76anit6rvj/2gf2h+2P0x5Hp7E/4T5WfjT93fAn88ngmbWbm3/eE8/syOll+AAAJRElEQVR4Ae2bWaiOXRTH13HM85i5CJmVEEnEjeGCC1wZSkQuKCVT6MuVqRTKUETmXChCylAoIZHMyjzPjnk8n7V53s77es/unPVx+M767QvP++y11/M+67fW/9372c+Rk5eXly80CEAgK4EyWXvphAAEAgEEQiFAIEIAgUTgYIIAAqEGIBAhgEAicDBBAIFQAxCIEEAgETiYIIBAqAEIRAggkAgcTBBAINQABCIEEEgEDiYIIBBqAAIRAggkAgcTBBAINQCBCAEEEoGDCQIIhBqAQIQAAonAwQQBBEINQCBCAIFE4GCCAAKhBiAQIYBAInAwQQCBUAMQiBBAIBE4mCCAQKgBCEQIIJAIHEwQQCDUAAQiBBBIBA4mCCAQagACEQIIJAIHEwQQCDUAgQgBBBKBgwkCCIQagECEAAKJwMEEAQRCDUAgQgCBROBgggACoQYgECGAQCJwMEEAgVADEIgQQCAROJgggECoAQhECCCQCBxMEEAg1AAEIgQQSAQOJgggEGoAAhECCCQCBxMEEAg1AIEIAQQSgYMJAgiEGoBAhAACicDBBAEEQg1AIEIAgUTgYIIAAqEGIBAhgEAicDBBAIFQAxCIEEAgETiYIIBAqAEIRAggkAgcTBBAINQABCIEEEgEDiYIIBBqAAIRAggkAgcTBBAINQCBCAEEEoGDCQIIhBqAQIQAAonAwQQBBEINQCBCAIFE4GCCAAIpgRo4fPiwTJs2TR4/flwC38ZX/EoCCORX0izkWvPmzZOVK1fKjh07ChlB999KoOzfemOl6b6mT58uO3fulCFDhpSmsFzEkpOXl5fvIlKChICBQO7MmTP/MfiVKpfJkyfLpUuX5P379zJnzhw5ceKE9OrVS8qWTZ9gz507J7pc2rRpk3z8+FE6duxYJA579+6VpUuXSvPmzaVevXopn6S/UaNGsmLFClm3bp00btxYPn36JLNnz5Z9+/aJ2ho0aJDy0Q83btwQnZUOHTokNWrUkAULFkhubq60bNkybdyaNWtk0aJFUr58edmzZ49s3rxZBg0alDbm8+fPsnjx4nB/V65ckR49eoRrpQ1yfJJeAU5BaGHWrVtXqlWrJrdv3xYtmpo1a8qMGTNSRO7evRuK68WLF6Hgdu3aFQp59OjRqTGFfThz5oysX79eBg8eLG3btk0NS/qPHj0qz58/l2fPnsmxY8fCfdy/f19ev34dxKp9BdvYsWPl5MmT4T506fbo0SNp2rSp9O/fPzVs//79MmXKlHB+5MiRMPbJkyeybNmy1Bj9MHXqVFm7dm0Qrgr2zp07Qaxpgxyf8JD+I/kqiuPHj8uGDRtCj/6aFmxbtmwRFYf+smtBV61aVVavXl1wiPnzwIEDw6ygs9aDBw9k6NChcv369TDjnD9/XrSwk3bv3r0gjlatWonOaN26dUtMaUedMbQtXLhQtPALXiMZ+ObNG9G4BgwYIBqvCmzbtm1BrMkY70cE8qMCdPlTsWJF6dSpU+h5+/ZtWm0kv+LDhw+XNm3aSIcOHeTs2bPy4cOHtHGWk549ewa31q1bh2Pnzp2lQoUKoiLQpkuupF29ejV87NevX1h6DRs2LDGlHS9evBjO9X51KahLt8ym4nv37p3o9+sSTY/6Q3Hq1KnMoW7PWWL9SH25cuXCJy3MbC15h6FLL221a9cOx6dPn4bnhHBi/KdSpUrBM3nmSc6z3cvLly/D2OQ+6tSpk/Vbv22+hP7kPvU5RpeJBVsyq2zcuFEOHjwoOjtpS/oLjvX6GYFkZD4nJyej5/upPsBr019abWXKfJ989Re4JNuXL1/C1yViSo6Z96D3q7Ek8eiDemZLZj+dXXRG1JlGl1nNmjXLHOr2HIEUMfX6EK9Nl17Vq1cPRz1P+vVzSTTdtdKmzw/a9EE+W9Nx+fn5YVyVKlXCDl3muGR26d69u8yaNStsEly+fDm1tMsc7/GcZ5AiZr1FixZhpK7bdZ2uhaQFlhRsES/zn4c1bNgwXCPZRLhw4ULWa9avXz/064P8169f5ebNmz+Na9KkSei7du1aOG7dujXMIBob7TsBZpCMSkiWJBndog+7ulU7ZswY0YdpXc+PHz8+c9hvP9cNAn2XortUI0eOlAMHDmT9zj59+sju3btlxIgR0q5du6w7Uyr69u3by/bt24PY9Zq1atVKbVRkvbCzTmaQIia8d+/e8u2latj10e1gLcC5c+cW0fvXDVMBz58/P7zX0HcdkyZNynrxUaNGSdeuXcM7ksqVK0uXLl2yjlu1apXorKR/K6bvYpYvXx62sLMOdtjJn5oUM+n6Bl3X//pL+yeb3oM+oGfb6Sp4X/ryUZeCffv2ldOnT4d3OQXtyeeHDx+G56lkEyLp935kBilmBehu0J8Wh96yPnjHxDFx4sTwfPTq1asQ4a1bt8LmQmHh6jML4viZDgL5mUmp6NEXiLqLNW7cOJkwYUL4vyi6W0UrHgGWWMXj9b8ZrTtXS5YsCX9Kon/XpX+EqM8bJb0t/b8BVsiNIpBCwJSmbp1JCtudK01x/o5YWGL9Dqp/2TURhz0hCMTODk8HBBCIgyQTop0AArGzw9MBAQTiIMmEaCeAQOzs8HRAAIE4SDIh2gkgEDs7PB0QQCAOkkyIdgIIxM4OTwcEEIiDJBOinQACsbPD0wEBBOIgyYRoJ4BA7OzwdEAAgThIMiHaCSAQOzs8HRBAIA6STIh2AgjEzg5PBwQQiIMkE6KdAAKxs8PTAQEE4iDJhGgngEDs7PB0QACBOEgyIdoJIBA7OzwdEEAgDpJMiHYCCMTODk8HBBCIgyQTop0AArGzw9MBAQTiIMmEaCeAQOzs8HRAAIE4SDIh2gkgEDs7PB0QQCAOkkyIdgIIxM4OTwcEEIiDJBOinQACsbPD0wEBBOIgyYRoJ4BA7OzwdEAAgThIMiHaCSAQOzs8HRBAIA6STIh2AgjEzg5PBwQQiIMkE6KdAAKxs8PTAQEE4iDJhGgngEDs7PB0QACBOEgyIdoJIBA7OzwdEEAgDpJMiHYCCMTODk8HBBCIgyQTop0AArGzw9MBAQTiIMmEaCeAQOzs8HRAAIE4SDIh2gkgEDs7PB0QQCAOkkyIdgIIxM4OTwcEEIiDJBOinQACsbPD0wEBBOIgyYRoJ4BA7OzwdEAAgThIMiHaCfwLBhH/YbiL2gYAAAAASUVORK5CYII=';
			    }
			    $(this).html('<img style="max-width: 200px; max-height: 150px;" class="img-responsive" src="'+img_src+'" ></img>');
			}else{
			    $(this).html(input.val());
			}
                    } else if (input.is("select")) {
                        $(this).html(input.find('option:selected').text());
                    } else if (input.is(":radio") && input.is(":checked")) {
                        $(this).html(input.attr("data-title"));
                    }
                });
            }

            var handleTitle = function(tab, navigation, index) {
                var total = navigation.find('li').length;
                var current = index + 1;
                // set wizard title
                $('.step-title', $('#form_wizard_1')).text('Step ' + (index + 1) + ' of ' + total);
                // set done steps
                jQuery('li', $('#form_wizard_1')).removeClass("done");
                var li_list = navigation.find('li');
                for (var i = 0; i < index; i++) {
                    jQuery(li_list[i]).addClass("done");
                }

                if (current == 1) {
                    $('#form_wizard_1').find('.button-previous').hide();
                } else {
                    $('#form_wizard_1').find('.button-previous').show();
                }

                if (current >= total) {
                    $('#form_wizard_1').find('.button-next').hide();
                    $('#form_wizard_1').find('.button-submit').show();
                    displayConfirm();
                } else {
                    $('#form_wizard_1').find('.button-next').show();
                    $('#form_wizard_1').find('.button-submit').hide();
                }
                Metronic.scrollTo($('.page-title'));
            }

            // default form wizard
            $('#form_wizard_1').bootstrapWizard({
                'nextSelector': '.button-next',
                'previousSelector': '.button-previous',
                onTabClick: function (tab, navigation, index, clickedIndex) {
		    
                    return false;
                    /*
                    success.hide();
                    error.hide();
                    if (form.valid() == false) {
                        return false;
                    }
                    handleTitle(tab, navigation, clickedIndex);
                    */
                },
                onNext: function (tab, navigation, index) {
                    success.hide();
                    error.hide();

                    if (form.valid() == false) {
                        return false;
                    }

                    handleTitle(tab, navigation, index);
                },
                onPrevious: function (tab, navigation, index) {
                    success.hide();
                    error.hide();

                    handleTitle(tab, navigation, index);
                },
                onTabShow: function (tab, navigation, index) {
                    var total = navigation.find('li').length;
                    var current = index + 1;
                    var $percent = (current / total) * 100;
                    $('#form_wizard_1').find('.progress-bar').css({
                        width: $percent + '%'
                    });
                }
            });
	    
	    
	    
            $('#form_wizard_1').find('.button-previous').hide();
            $('#form_wizard_1 .button-submit').click(function () {
				handleSubmitData();
            }).hide();

        
            $('#user_provinsi_list, #user_kabupaten_list, #user_kecamatan_list', form).change(function () {
                form.validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
            });
		
		    //apply validation on select2 dropdown value change, this only needed for chosen dropdown integration.
            // $('#country_list', form).change(function () {
            //     form.validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
            // });
	    //check username
	    $("input[name$='username']").focusout(function(){
		    checkUsername($(this).val(), $(this));
	    });
	    //check email
	    $("input[name$='email']").focusout(function(){
		    checkUsername($(this).val(), $(this));
	    });
	    
	    //default
	    $('input[name=gender][value=M]').attr('checked', true); 
	    
	    $(document).on('click','#check_procurement_certificate',function(){
		no_cert = $('input[name=procurement_certificate]').val();
		
		
		url_certificate = URL_API+"/certificate/index/"+no_cert;
		$.get( url_certificate, function( data ) {
		    if(typeof(data.error) != "undefined" && data.error !== null) {
			//kosong
			console.log('No sertifikat tidak ditemukan!');
			alert('No sertifikat tidak ditemukan!');
		    }else{
			//ada
			console.log('ditemukan');
			console.log(data[0]);
			data_certificate = data[0];
			$('input[name=email]').val(data_certificate.email);
			$('input[name=remail]').val(data_certificate.email);
			$('input[name=photo]').val(data_certificate.path_photo);
			$('.fileinput-new img').attr('src',data_certificate.path_photo);
			$('input[name=fullname]').val(data_certificate.nama);
			$('input[name=birth_place]').val(data_certificate.tempat_lahir);
			$('input[name=birth_date]').attr('data-date',data_certificate.tanggal_lahir);
			$('input[name=birth_date]').val(data_certificate.tanggal_lahir);
			$('input[name=phone]').val(data_certificate.no_telp);
			
			if(data_certificate.jenis_kelamin=='P'){
			    $('input[name=gender][value=F]').attr('checked', true);
			    $('input[name=gender][value=F]').parent().attr('class', 'checked');
			    
			    $('input[name=gender][value=M]').attr('checked', false);
			    $('input[name=gender][value=M]').parent().attr('class', '');
			}else{
			    $('input[name=gender][value=M]').attr('checked', true);
			    $('input[name=gender][value=M]').parent().attr('class', 'checked');
			    
			    $('input[name=gender][value=F]').attr('checked', false);
			    $('input[name=gender][value=F]').parent().attr('class', '');
			}
			$('#pendidikan_list').select2("val",data_certificate.pend_akhir);
			
			$('#academic_degree').tagsinput('add', data_certificate.gelar);
			
			$('#status_kepegawaian_list').select2("val",data_certificate.status_kepegawaian).trigger('change');
			
			$('input[name=nip_id]').val(data_certificate.nip);
			$('input[name=ktp_id]').val(data_certificate.no_ktp);
			
			$('input[name=institution]').val(data_certificate.instansi);
			$('input[name=work_unit]').val(data_certificate.satker);
			$('textarea[name=office_address]').text(data_certificate.alamat_kerja);
			
			
		    }
		});
	    });
	    
	    }	
	};
}();