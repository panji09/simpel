var UpdateProfile = function(){

	//serialize form to json object
	$.fn.serializeObject = function()
	{
	    var o = {};
	    var a = this.serializeArray();
	    $.each(a, function() {
	        if (o[this.name] !== undefined) {
	            if (!o[this.name].push) {
	                o[this.name] = [o[this.name]];
	            }
	            o[this.name].push(this.value || '');
	        } else {
	            o[this.name] = this.value || '';
	        }
	    });
	    return o;
	};
	
	var handleSubmitProfileData = function(){
		var formUrl = URL_BASE+"user/update/"+ID+"?access_token="+CODE;
		// var formUrl = URL_BASE+"dashboard/update_profile";
		var itemData = jQuery.parseJSON(JSON.stringify($('#edit_profile_lpp').serializeObject()));
		
		var province = new Object();
		province.id =  $("#provinsi_kantor_list").val();
		province.name = $("#provinsi_kantor_list").select2('data').text;		

		//district
		var district = new Object();
		district.id =  $("#kabupaten_kantor_list").val();
		district.name = $("#kabupaten_kantor_list").select2('data').text;		

		//set data
		itemData.office_province = province;
		itemData.office_district = district;
		
		var sendInfo = JSON.stringify(itemData);
		console.log(sendInfo);
		$.ajax({
		           type: "POST",
		           url: formUrl,
		           dataType: "json",
		           success: function (msg) {
					   console.log(msg);
		               if (msg) {
						   bootbox.dialog({
	                       message: "Update data berhasil.",
	                       title: "Update Sukses",
	                       buttons: {
	                         success: {
	                           label: "Success!",
	                           className: "green",
	                           callback: function() {
	                            	location.reload(true);
	                           }
	                         }
						 }
					 });						   
						   
		               } else {
		                   alert("Cannot add to list !");
		               }
		           },
				   error: function(xhr, status, error) {
					   bootbox.dialog({
                       message: "Update data tidak berhasil.",
                       title: "Update Gagal",
                       buttons: {
                         success: {
                           label: "OK!",
                           className: "green",
                           callback: function() {
                            	
                           }
                         }
					 }
				 });
				    },

		           data: sendInfo
		       });
	}
		
	
	var getKota = function(id, component){
		$.ajax({
		    url: URL_API+"/location/district/" + id,
			type: "GET",
		    dataType: "jsonp",

		    // Work with the response
		    success: function( response ) {
				$.each(response, function(index, item){
					$(component).append("<option value='"+item.id+"'>"+item.name+"</option>");
				});
		    }
		});				
	}
	
	
	var handleProvinceData = function(){		
		//init province first
		$.ajax({
		    url: URL_API+"/location/province",
			type: "GET",
		    dataType: "jsonp",
		    // Work with the response
		    success: function( response ) {
				$.each(response, function(index, item){					
					$("#provinsi_kantor_list").append("<option value='"+item.id+"'>"+item.name+"</option>");						
				});
		    }
		});
		
		//set selection tool on change callback
		$('#user_provinsi_list').on('change', function() {
			//clear old data
			var $this = $(this);
			$('#user_kabupaten_list').empty();
			$("#user_kabupaten_list").select2('data', {id: null, text:""});
			$("#user_kabupaten_list").append("<option></option>");
		    getKota($this.val(), "#user_kabupaten_list");
			$('#user_kabupaten_list').attr("data-placeholder", "Pilih");
			$('#user_kabupaten_list').data("select2").setPlaceholder();
			//now clear kabupaten
			$("#user_kecamatan_list").empty();
			$("#user_kecamatan_list").select2('data', {id: null, text:""});
			$("#user_kecamatan_list").append("<option></option>");
			$('#user_kecamatan_list').attr("data-placeholder", "Pilih");
			$('#user_kecamatan_list').data("select2").setPlaceholder();

		});
		

		$('#provinsi_kantor_list').on('change', function() {
			//clear old data
			var $this = $(this);
			$('#kabupaten_kantor_list').empty();
			$("#kabupaten_kantor_list").select2('data', {id: null, text:""});
			$("#kabupaten_kantor_list").append("<option></option>");
		    getKota($this.val(), "#kabupaten_kantor_list");
			$('#kabupaten_kantor_list').attr("data-placeholder", "Pilih");
			$('#kabupaten_kantor_list').data("select2").setPlaceholder();
		});
	}
	
	var show_status_negeri = function(){
	    $(".box").not(".negeri").hide();
	    //$("input.swasta").val("");
	    $(".negeri").show();
	    $(".ya_negeri").hide();
	}
	
	var show_status_swasta = function(){
	    $(".box").not(".swasta").hide();
	  //  $("input.negeri").val("");
	    $(".swasta").show();
	    $(".ya_swasta").hide();
	}
	
	var training_task_negeri_ya = function(){
	    $(".ya_swasta").hide();				
	    $(".ya_negeri").show();
	}
	
	var training_task_negeri_tidak = function(){
	    $(".ya_negeri").hide();
	    $(".ya_swasta").hide();
	    // $("input.kosong").val("");
	}
	
	var LPPHideShow = function() {
		
		
		$("input[name$='training_institute_status']").change(function() {
			if($(this).attr("value")=="negeri"){
			    show_status_negeri();
			}

			if($(this).attr("value")=="swasta"){
			    show_status_swasta();
			}
		});
	   
	    $("input[name$='training_task_negeri']").click(function() {
			if($(this).attr("value")=="ya"){
			    training_task_negeri_ya();
			}
		
			if($(this).attr("value")=="tidak"){
			    training_task_negeri_tidak();
			}
       });
    
	$("input[name$='training_task_swasta']").click(function() {
		if($(this).attr("value")=="ya"){
			$(".ya_negeri").hide();			
			$(".ya_swasta").show();
		}
	
		if($(this).attr("value")=="tidak"){
			$(".ya_negeri").hide();			
			$(".ya_swasta").hide();
			//$("input.kosong").val("");
		}
      });
	}
	    
	var initData = function(){
		console.log($("input[name$='training_institute_status']").val());
		if($("input[name$='training_institute_status']").val() == 'negeri'){
			show_status_negeri();
			//now check training task
			if($("input[name$='training_task_negeri']").val() == 'ya'){
				training_task_negeri_ya();
			}else{
				training_task_negeri_tidak();				
			}
		}else{
			show_status_swasta();
			if($("input[name$='training_task_swasta']").val() == 'ya'){
				$(".ya_negeri").hide();			
				$(".ya_swasta").show();
			}else{
				$(".ya_negeri").hide();			
				$(".ya_swasta").hide();			
			}
		}
	}
	
	return {
		init:function(){
		  handleProvinceData();
            var form = $('#edit_profile_lpp');
            var error = $('.alert-danger', form);
            var success = $('.alert-success', form);

		    LPPHideShow(); 
			// validation for Peserta Pelatihan
			//init data
			initData();
			
            form.validate({
                doNotHideMessage: true, //this option enables to show the error/success messages on tab switch.
                errorElement: 'span', //default input error message container
                errorClass: 'help-block help-block-error', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                rules: {
                    //global user
                    last_education: {
                        required: true,
						
                    },
                    academic_degree: {
                        required: true
                    },
					employment:{
						required: true
					},
					ktp_id:{
						required: true
					},
					institution:{
						required: true
					},
                    office_province: {
                        required: true
                    },
                    office_district: {
                        required: false
                    }
                },
                messages: { // custom messages for radio buttons and checkboxes
                    'gender': {
                        required: "Pilih salah satu opsi",
                        minlength: jQuery.validator.format("Pilih salah satu opsi")
                    }
                },

                errorPlacement: function (error, element) { // render error placement for each input type
                    if (element.attr("name") == "gender") { // for uniform checkboxes, insert the after the given container
                        error.insertAfter("#form_gender_error");
                    } else if (element.attr("name") == "birth_date") {
						error.insertAfter("#form_tgl_lahir_error");
					} else {
                        error.insertAfter(element); // for other inputs, just perform default behavior
                    }
                },

                invalidHandler: function (event, validator) { //display error alert on form submit   
                    success.hide();
                    error.show();
                    Metronic.scrollTo(error, -200);
                },

                highlight: function (element) { // hightlight error inputs
                    $(element)
                        .closest('.form-group').removeClass('has-success').addClass('has-error'); // set error class to the control group
                },

                unhighlight: function (element) { // revert the change done by hightlight
                    $(element)
                        .closest('.form-group').removeClass('has-error'); // set error class to the control group
                },

                success: function (label) {
                    if (label.attr("for") == "gender") { // for checkboxes and radio buttons, no need to show OK icon
                        label
                            .closest('.form-group').removeClass('has-error').addClass('has-success');
                        label.remove(); // remove error label here
                    } else { // display success icon for other inputs
                        label
                            .addClass('valid') // mark the current input as valid and display OK icon
                        .closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
                    }
                },

                submitHandler: function (form) {
                    // success.show();
                    error.hide();
					handleSubmitProfileData();
                    //add here some ajax code to submit your form or just call form.submit() if you want to submit the form without 
                }

            });			
		}	
	};
}();