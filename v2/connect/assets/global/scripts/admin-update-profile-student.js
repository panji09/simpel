var UpdateProfile = function(){

	//serialize form to json object
	$.fn.serializeObject = function()
	{
	    var o = {};
	    var a = this.serializeArray();
	    $.each(a, function() {
	        if (o[this.name] !== undefined) {
	            if (!o[this.name].push) {
	                o[this.name] = [o[this.name]];
	            }
	            o[this.name].push(this.value || '');
	        } else {
	            o[this.name] = this.value || '';
	        }
	    });
	    return o;
	};
	
	var getKota = function(id, component){
		$.ajax({
		    url: URL_API+"/location/district/" + id,
			type: "GET",
		    dataType: "jsonp",

		    // Work with the response
		    success: function( response ) {
				$.each(response, function(index, item){
					$(component).append("<option value='"+item.id+"'>"+item.name+"</option>");
				});
		    }
		});				
	}
	
	
	var handleProvinceData = function(){		
		//init province first
		$.ajax({
		    url: URL_API+"/location/province",
			type: "GET",
		    dataType: "jsonp",
		    // Work with the response
		    success: function( response ) {
				$.each(response, function(index, item){					
					$("#provinsi_kantor_list").append("<option value='"+item.id+"'>"+item.name+"</option>");						
				});
		    }
		});
		
		//set selection tool on change callback
		$('#domisili_provinsi_list_kerja').on('change', function() {
			//clear old data
			var $this = $(this);
			$('#domisili_kabupaten_list_kerja').empty();
			$("#domisili_kabupaten_list_kerja").select2('data', {id: null, text:""});
			$("#domisili_kabupaten_list_kerja").append("<option></option>");
		    getKota($this.val(), "#domisili_kabupaten_list_kerja");
			$('#domisili_kabupaten_list_kerja').attr("data-placeholder", "Pilih");
			$('#domisili_kabupaten_list_kerja').data("select2").setPlaceholder();
			

		});
		

	}
	
	
	var handleSubmitProfileData = function(){
		var formUrl = URL_BASE+"user/update/"+ID+"?access_token="+CODE;
		var itemData = jQuery.parseJSON(JSON.stringify($('#form_user_detail').serializeObject()));
		
		var province = new Object();
		province.id =  $("#domisili_provinsi_list_kerja").val();
		province.name = $("#domisili_provinsi_list_kerja").select2('data').text;		

		//district
		var district = new Object();
		district.id =  $("#domisili_kabupaten_list_kerja").val();
		district.name = $("#domisili_kabupaten_list_kerja").select2('data').text;		

		//set data
		itemData.office_province = province;
		itemData.office_district = district;
		
		var sendInfo = JSON.stringify(itemData);

		$.ajax({
		           type: "POST",
		           url: formUrl,
		           dataType: "json",
		           success: function (msg) {
					   console.log(msg);
		               if (msg) {
						   bootbox.dialog({
	                       message: "Update data berhasil.",
	                       title: "Update Sukses",
	                       buttons: {
	                         success: {
	                           label: "Success!",
	                           className: "green",
	                           callback: function() {
	                            	location.reload(true);
	                           }
	                         }
						 }
					 });						   
						   
		               } else {
		                   alert("Cannot add to list !");
		               }
		           },
				   error: function(xhr, status, error) {
					   bootbox.dialog({
                       message: "Update data tidak berhasil.",
                       title: "Update Gagal",
                       buttons: {
                         success: {
                           label: "OK!",
                           className: "green",
                           callback: function() {
                            	
                           }
                         }
					 }
				 });
				    },

		           data: sendInfo
		       });
	}
	

		
    var handleTwitterTypeahead = function() {

		// Gelar akademis
        var gelarAkademis = new Bloodhound({
          datumTokenizer: function(d) { return Bloodhound.tokenizers.whitespace(d.name); },
          queryTokenizer: Bloodhound.tokenizers.whitespace,
          limit: 10,
          prefetch: {
            url: URL_API+'/academic/academic_degree/sort_name',
            filter: function(list) {
              return $.map(list, function(country) { return { name: country }; });
            }
          }
        });

        gelarAkademis.initialize();

        if (Metronic.isRTL()) {
          $('#academic_degree').attr("dir", "rtl");
        }
		
		$('#academic_degree').tagsinput({
		  typeaheadjs: {
		    name: 'academic_degree',
		    displayKey: 'name',
		    valueKey: 'name',
			hint: (Metronic.isRTL() ? false : true),
		    source: gelarAkademis.ttAdapter()
		  }
		});
				
		// Institusi
        var institution = new Bloodhound({
          datumTokenizer: function(d) { return Bloodhound.tokenizers.whitespace(d.name); },
          queryTokenizer: Bloodhound.tokenizers.whitespace,
          limit: 10,
          prefetch: {
            url: URL_API+'/institution',
            filter: function(list) {
              return $.map(list, function(country) { return { name: country }; });
            }
          }
        });

        institution.initialize();

        if (Metronic.isRTL()) {
          $('#institution').attr("dir", "rtl");
        }
        $('#institution').typeahead(null, {
          name: 'institution',
          displayKey: 'name',
          hint: (Metronic.isRTL() ? false : true),
          source: institution.ttAdapter()
        });
    }
    
    var handleStatusEmployee = function(){
// 		$('.pns').hide();
		 function check_status_employee (select){
		    var $this = $(select);
		    if($this.val() == 'PNS' || $this.val() == 'TNI' || $this.val() == 'POLRI'){
			    $('.pns').show();
		    }else{
			    $('.pns').hide();				
		    }
		}
		check_status_employee('#status_kepegawaian_list');
		$('#status_kepegawaian_list').on('change', function() {
			//clear old data
			check_status_employee(this);
		});
	}	
	
	return {
		init:function(){			
			handleTwitterTypeahead();
			handleProvinceData();
			var form = $('#form_user_detail');
				    // var form_update_password = $('#edit_change_password');
			var error = $('.alert-danger', form);
			var success = $('.alert-success', form);

				    // validation for Peserta Pelatihan
			form.validate({
			    doNotHideMessage: true, //this option enables to show the error/success messages on tab switch.
			    errorElement: 'span', //default input error message container
			    errorClass: 'help-block help-block-error', // default input error message class
			    focusInvalid: false, // do not focus the last invalid input
			    rules: {
				//global user
				last_education: {
				    required: true,
							    
				},
				academic_degree: {
				    required: true
				},
						    employment:{
							    required: true
						    },
						    ktp_id:{
							    required: true
						    },
						    institution:{
							    required: true
						    },
				office_province: {
				    required: true
				},
				office_district: {
				    required: false
				}
			    },
			    messages: { // custom messages for radio buttons and checkboxes
				'gender': {
				    required: "Pilih salah satu opsi",
				    minlength: jQuery.validator.format("Pilih salah satu opsi")
				}
			    },

			    errorPlacement: function (error, element) { // render error placement for each input type
				if (element.attr("name") == "gender") { // for uniform checkboxes, insert the after the given container
				    error.insertAfter("#form_gender_error");
				} else if (element.attr("name") == "birth_date") {
							    error.insertAfter("#form_tgl_lahir_error");
						    } else {
				    error.insertAfter(element); // for other inputs, just perform default behavior
				}
			    },

			    invalidHandler: function (event, validator) { //display error alert on form submit   
				success.hide();
				error.show();
				Metronic.scrollTo(error, -200);
			    },

			    highlight: function (element) { // hightlight error inputs
				$(element)
				    .closest('.form-group').removeClass('has-success').addClass('has-error'); // set error class to the control group
			    },

			    unhighlight: function (element) { // revert the change done by hightlight
				$(element)
				    .closest('.form-group').removeClass('has-error'); // set error class to the control group
			    },

			    success: function (label) {
				if (label.attr("for") == "gender") { // for checkboxes and radio buttons, no need to show OK icon
				    label
					.closest('.form-group').removeClass('has-error').addClass('has-success');
				    label.remove(); // remove error label here
				} else { // display success icon for other inputs
				    label
					.addClass('valid') // mark the current input as valid and display OK icon
				    .closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
				}
			    },

			    submitHandler: function (form) {
				// success.show();
				error.hide();
						    handleSubmitProfileData();
				//add here some ajax code to submit your form or just call form.submit() if you want to submit the form without 
			    }

			});
						    
			$('#domisili_provinsi_list_kerja, #domisili_kabupaten_list_kerja', form).change(function () {
			    form.validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
			});
			handleStatusEmployee();
		}	
	};
}();