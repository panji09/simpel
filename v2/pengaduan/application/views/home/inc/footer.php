    <footer class="site-footer">
        <div class="container">
            <div class="row">
                <div class="col-md-5">
                    <div class="footer-widget">
                        <h4 class="footer-widget-title">Hubungi Kami</h4>
                        <p>Kementerian Pendidikan dan Kebudayaan<br>Jalan Jenderal Sudirman Senayan Jakarta 10270.
                        <ul class="list-links">
			    <li><i class="fa fa-phone"></i> Call center : <a href="tel:0215725980">021 5725980</a></li>
			    <li><i class="fa fa-phone"></i> Call center : <a href="tel:177">177</a></li>
			    <li><i class="fa fa-phone"></i> Telepon alternatif : <a href="tel:0215703303">021 5703303</a> / <a href="tel:5711144">5711144</a> ext. 2115</li>
			    <li><i class="fa fa-envelope"></i> SMS : 0811976929</li>
			    <li><i class="fa fa-envelope"></i> Email : <a href="mailto:pengaduan@kemdikbud.go.id">pengaduan[at]kemdikbud.go.id</a></li>
                        </ul>
                        </p>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="footer-widget">
                        <h4 class="footer-widget-title">Tautan Pendukung</h4>
                        <ul class="list-links">
                            <li>Kementerian Pendidikan dan Kebudayaan RI - <a href="#">http://www.kemdikbud.go.id/</a></li>
                            
                        </ul>
                    </div>
                </div>
                
                <div class="col-md-3">
                    <div class="footer-widget">
                        <ul class="footer-media-icons">
                            <li><a href="http://www.facebook.com/Kemdikbud.RI" class="fa fa-facebook"></a></li>
                            <li><a href="https://twitter.com/Kemdikbud_RI" class="fa fa-twitter"></a></li>
                        </ul>
                    </div>
                </div>
            </div> <!-- /.row -->

            <div class="bottom-footer">
                <div class="row">
                    <div class="col-md-5">
                        <p class="small-text">&copy; Copyright 2014. Kemdikbud. All Rights Reserved.</p>
                    </div> <!-- /.col-md-5 -->
                    
                </div> <!-- /.row -->
            </div> <!-- /.bottom-footer -->

        </div> <!-- /.container -->
    </footer> <!-- /.site-footer --> 
