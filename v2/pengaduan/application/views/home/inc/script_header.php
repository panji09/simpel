    <link href='http://fonts.googleapis.com/css?family=Raleway:400,100,200,300,500,600,700,800' rel='stylesheet' type='text/css'>
        
    <!-- CSS Bootstrap & Custom -->
    <link href="<?=$this->config->item('plugin')?>/bootstrap/css/bootstrap.css" rel="stylesheet" media="screen">
    
    <link href="<?=$this->config->item('home_css')?>/font-awesome.min.css" rel="stylesheet" media="screen">
    <link href="<?=$this->config->item('home_css')?>/animate.css" rel="stylesheet" media="screen">
    
    <link href="<?=$this->config->item('home_css')?>/style.css" rel="stylesheet" media="screen">
        
    <!-- Favicons -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?=$this->config->item('home_img')?>/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?=$this->config->item('home_img')?>/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?=$this->config->item('home_img')?>/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="<?=$this->config->item('home_img')?>/ico/apple-touch-icon-57-precomposed.png">
    <link rel="shortcut icon" href="<?=$this->config->item('home_img')?>/logo_kurikulum_2013.png">
    
    <!-- JavaScripts -->
    <script src="<?=$this->config->item('home_js')?>/jquery-1.10.2.min.js"></script>
    <script src="<?=$this->config->item('home_js')?>/jquery-migrate-1.2.1.min.js"></script>
    <script src="<?=$this->config->item('home_js')?>/modernizr.js"></script>
    <!--[if lt IE 8]>
	<div style=' clear: both; text-align:center; position: relative;'>
            <a href="http://www.microsoft.com/windows/internet-explorer/default.aspx?ocid=ie6_countdown_bannercode"><img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" alt="" /></a>
        </div>
    <![endif]-->
    
    <?php $page = $this->uri->segment(1);?>
    <?php if($page == 'lihat_pengaduan'):?>
	<link href="<?=$this->config->item('home_css')?>/custom-lihat-pengaduan.css" rel="stylesheet">
    <?php endif;?>
    
    <?php if($page == 'pengaduan'):?>
	<link href="<?=$this->config->item('home_css')?>/custom-pengaduan.css" rel="stylesheet">
    <?php endif;?>
    <!--bootstrap validator-->
    <link rel="stylesheet" href="<?=$this->config->item('plugin')?>/bootstrap-validator/dist/css/bootstrapValidator.min.css"/>
    <!--./bootstrap validator-->