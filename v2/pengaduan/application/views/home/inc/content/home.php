    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="main-slideshow">
                    <div class="flexslider">
                        <ul class="slides">
                            <li>
                                <img src="<?=$this->config->item('home_img')?>/display5.jpg" alt="Slide 1"/>
                                <!--
                                <div class="slider-caption">
                                    <h2><a href="blog-single.html">teks slider 1..</a></p>
                                </div>
                                -->
                            </li>
                            <li>
                                <img src="<?=$this->config->item('home_img')?>/display2.jpg" alt="Slide 2"/>
                                <!--
                                <div class="slider-caption">
                                    <h2><a href="blog-single.html">teks slider 1..</a></p>
                                </div>
                                -->
                            </li>
                            <li>
                                <img src="<?=$this->config->item('home_img')?>/display3.jpg" alt="Slide 3"/>
                                <!--
                                <div class="slider-caption">
                                    <h2><a href="blog-single.html">teks slider 1..</a></p>
                                </div>
                                -->
                            </li>
                            <li>
                                <img src="<?=$this->config->item('home_img')?>/display4.jpg" alt="Slide 4"/>
                                <!--
                                <div class="slider-caption">
                                    <h2><a href="blog-single.html">teks slider 1..</a></p>
                                </div>
                                -->
                            </li>
                        </ul> <!-- /.slides -->
                    </div> <!-- /.flexslider -->
                </div> <!-- /.main-slideshow -->
            </div> <!-- /.col-md-12 -->
            
            <div class="col-md-4">
                <?=$this->load->view('home/inc/widget_form_report')?>
            </div> <!-- /.col-md-4 -->
        </div>
    </div>


    <div class="container">
        <div class="row">
            
            <!-- Here begin Main Content -->
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-12">
                        <div class="widget-item">
                            <!--<h2 class="welcome-text">Pengaduan Berdasarkan Kategori dan Status Penanganan</h2>-->
                            <div id="show_chart" align="center" style="height:450px;"><!--Chart Di Load disini--></div>
                            <?php $data['report'] = $this->report_db->kategori_status()->result();?>
			    <?=$this->load->view('handling/modules/chart/report_kategori_status',$data)?>
			    
			    <div id="sharethis">
				    <span class='st_facebook_vcount' displayText='Facebook'></span>
				    <span class='st_twitter_vcount' displayText='Tweet'></span>
				    <span class='st_googleplus_vcount' displayText='Google +'></span>
				    <span class='st_email_vcount' displayText='Email'></span>
			    </div>
                        </div> <!-- /.widget-item -->
                    </div> <!-- /.col-md-12 -->
                </div> <!-- /.row -->

                
                

            </div> <!-- /.col-md-12 -->
            
            
        </div>
    </div> 
