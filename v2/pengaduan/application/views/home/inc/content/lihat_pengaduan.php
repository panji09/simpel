    <!-- Being Page Title -->
    <div class="container">
        <div class="page-title clearfix">
            <div class="row">
                <div class="col-md-12">
                    <h6><a href="index.html">Home</a></h6>
                    <h6><span class="page-active">Lihat Pengaduan</span></h6>
                </div>
            </div>
        </div>
    </div>
    
    <div class="container">
        <div class="row">
	    <!-- Here begin Main Content -->
	    <div class="col-md-12">
		
		<div class="row">
		    <div class="col-md-12">
			<div id="blog-comments" class="blog-post-container">
			    <div class="blog-post-inner">
				<form role="form" method='post' action='<?=site_url('lihat_pengaduan/post_filter')?>'>
				    <div class='row'>
					<div class="col-md-3">
					    <div class='form-group'>
					      <label for="">Cari</label>
					      <?php $type = array(1 => 'Teks', 2 => 'Kode Pengaduan');?>
					      <select name='type' class="form-control">
						<?php foreach($type as $row_id => $row_name):?>
						    <option value='<?=$row_id?>' <?=($row_id == $this->session->userdata('type') ? 'selected' : '')?>><?=$row_name?></option>
						<?php endforeach;?>
						
						
					      </select>
					    </div>
					    
					    <div class="form-group">
					      <input name='cari' type="text" class="form-control" placeholder="Cari" value="<?=$this->session->userdata('cari')?>">
					    </div>
					</div>
					<div class="col-md-3">
					    <div class='form-group'>
					      <label for="">Tahun</label>
					      <select name='tahun' class="form-control">
						<option value="">-- semua --</option>
						<?php for($tahun=2014; $tahun<=date('Y'); $tahun++):?>
						<option value="<?=$tahun?>" <?=($tahun == $this->session->userdata('tahun') ? 'selected' : '')?>><?=$tahun?></option>
						<?php endfor;?>
						
					      </select>
					    </div>
					</div>
					<div class="col-md-3">
					    <div class='form-group'>
					      <label for="">Lokasi</label>
					      <select name='provinsi' id='provinsi' class="form-control">
						<option value="">-- semua --</option>
						<?php foreach($this->region_db->provinsi()->result() as $row):?>
						<option value="<?=$row->id?>" <?=($row->id == $this->session->userdata('provinsi') ? 'selected' : '')?>><?=$row->name?></option>
						<?php endforeach;?>
						
					      </select>
					    </div>
					    <div class='form-group'>
					      <select name='kabkota' id='kabkota' class="form-control">
						<option value="">-- semua --</option>
						<?php foreach($this->region_db->kabkota(array('provinsi_id' => $this->session->userdata('provinsi')))->result() as $row):?>
						<option value="<?=$row->id?>" <?=($row->id == $this->session->userdata('kabkota') ? 'selected' : '')?>><?=$row->name?></option>
						<?php endforeach;?>
					      </select>
					    </div>
					</div>
					<div class="col-md-3">
					    <div class='form-group'>
					      <label for="">Kategori</label>
					      <select name='kategori' id='kategori' class="form-control">
						<option value=''>-- semua --</option>
						<?php 
						      $kategori = $this->select_db->kategori()->result();
						      foreach($kategori as $row):?>
						      <option value="<?=$row->id?>" <?=($row->id == $this->session->userdata('kategori') ? 'selected' : '')?>><?=$row->name?></option>
						  <?php endforeach;?>
					      </select>
					    </div>
					</div>
				    </div>
				    <div class='row'>
					<div class="col-md-2 col-md-offset-8">
					    <input type='button' class='mainBtn btn-block' onclick="location.href='<?=site_url('lihat_pengaduan/clear_filter')?>'" value='Hapus'>
					    
					</div>
					<div class="col-md-2">
					    
					    <input type='submit' class='mainBtn btn-block' value='Filter'>
					</div>
					
				    </div>
				<form>
			    </div>
			</div>
		    </div>
		</div>
	    </div>
	</div>
    </div>
    
    <div class="container">
        <div class="row">
	    <!-- Here begin Main Content -->
	    <div class="col-md-12">
		
		<div class="row">
		    <div class="col-md-12">
			<div id="blog-comments" class="blog-post-container">
			    <div class="blog-post-inner">
				<?php
				    $pengaduan = $this->pengaduan_db->get_all($filter=array('approved' => 1, 'published' => 1));
				    if($pengaduan->num_rows())
				    foreach( $pengaduan->result() as $row):
					
				?>
				    <div class="row box-comment">
					<div class="col-md-12">
					    <div class="media">
						<div class="media-body list-event-item">
						    <h5 class="media-heading">
							<?php
							    $query = $this->pengaduan_db->get_param($row->id);
							    $param = array();
							    foreach($query->result() as $row1){
								$param[$row1->key] = $row1->value;
							    }
							?>
							<div class="list-event-header">
							    Diposting : 
							    <span class="event-place small-text"><i class="fa fa-calendar-o"></i><?=mysqldatetime_to_date_id($row->tanggal)?></span>
							    Oleh : 
							    <span class="event-place small-text"><i class="fa fa-user"></i><?=($param['tampil_nama'] ? $row->nama : 'xxx')?></span>
							    <span class="event-place small-text"><i class="fa fa-phone-square"></i><?=($param['tampil_telp'] ? '<a href="tel:'.$row->handphone.'">'.$row->handphone.'</a>' : 'xxx')?></span>
							    <span class="event-place small-text"><i class="fa fa-home"></i><?=($param['tampil_alamat'] ? $row->alamat_rumah : 'xxx')?></span>
							    <?=ucwords($row->lokasi)?> : 
							    <span class="event-place small-text"><?=($row->lokasi_id == 1 ? $row->jenjang.' '.$row->status_sekolah.' '.$row->nama_lokasi : $row->nama_lokasi).', '.$row->provinsi,', '.$row->kabkota.', '.$row->kecamatan?></span>
							    Ditujukan :
							    <span class="event-place small-text"><i class="fa fa-user"></i><?='Tim '.ucwords($row->level)?></span>
							</div>
						    </h5>
						    <p><?=nl2br($row->deskripsi)?></p>
						</div>
					    </div>
					</div>
				    </div>
				    
				    <?php
					$respon = $this->respon_db->get_all(array('pengaduan_id' => $row->id));
					if($respon->num_rows()):
					foreach($respon->result() as $row_respon):
				    ?>
				    <div class="row box-comment">
					<div class="col-md-11 col-md-offset-1">
					    <div class="media">
						<div class="pull-left">
						    <img alt="" width='60' src="<?=$this->config->item('home_img')?>/logo_kurikulum_2013.png" class="media-object">
						</div>
						<div class="media-body list-event-item">
						    <h5 class="media-heading">
							<div class="list-event-header">
							    Direspon :
							    <span class="event-place small-text"> <i class="fa fa-calendar-o"></i><?=mysqldatetime_to_date_id($row_respon->tanggal)?></span>
							    Oleh : 
							    <span class="event-place small-text"><i class="fa fa-user"></i><?=$row_respon->level?></span>
							    
							</div>
						    </h5>
						    <p><?=$row_respon->deskripsi?></p>
						</div>
					    </div>
					</div>
				    </div>
				    <?php endforeach;?>
				    <div class='divider-comment'></div>
				    <?php endif;?>
				<?php endforeach;?>
				
			    </div>
			</div>
		    </div>
		</div>
	    </div>
	</div>
    </div>
