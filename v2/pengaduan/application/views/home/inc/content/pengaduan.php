    
    <!-- Being Page Title -->
    <div class="container">
        <div class="page-title clearfix">
            <div class="row">
                <div class="col-md-12">
                    <h6><a href="index.html">Home</a></h6>
                    <h6><span class="page-active">Pengaduan Online</span></h6>
                </div>
            </div>
        </div>
    </div>
    
    <div class="container">
        <div class="row">
	    <!-- Here begin Main Content -->
	    <div class="col-md-12">
		
		<div class="row">
		    <div class="col-md-12">
			<div id="blog-comments" class="blog-post-container">
			    <div class="blog-post-inner">
				<!--content-->
				<div class="alert alert-warning alert-dismissible" role="alert">
				    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				    <strong>Perhatian</strong> Mengadu secara komplit dan bertanggung jawab sebelum anda bertanya lebih jauh, silahkan <a href="<?=site_url('faq');?>">lihat FAQ</a>
				</div>
				
				<?php
				    $notif=$this->session->flashdata('success');
				    if($notif):
				?>
				  <div class="alert <?=($notif['status'] ? 'alert-success' : 'alert-danger')?> alert-dismissible" role="alert">
				      <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				      <?=$notif['msg']?>
				  </div>
				<?php endif;?>
				<form id='pengaduan_form' role='form' class='form-pengaduan' method='post' action='<?=site_url('pengaduan/post_pengaduan')?>'
				    
				>
				
				    
				    <h4 style='display:none'>Ditujukan Kepada</h4>
				    <div class='row' style="display:none">
					
					<div class="col-md-10 col-md-offset-2">
					    <div class='form-group'>
						<select name='level' id='level' class="form-control" required data-bv-notempty-message="Harus diisi">
						  <option value='1'>-- pilih --</option>
						  <?php 
						      $level = $this->select_db->level()->result();
						      foreach($level as $row):?>
							<option value="<?=$row->id?>">Tim <?=ucfirst($row->name)?></option>
						  <?php endforeach;?>
						</select>
					    </div>
					</div>
				    </div>
				    
				    <h4>Lokasi Kejadian</h4>
				    <div class='row'>
					<div class="col-md-2">Daerah</div>
					<div class="col-md-10">
					    <div class="row">
						<div class="col-md-4">
						    <div class='form-group'>
							<label for="">Provinsi</label>
							<select name='provinsi' id='provinsi' class="form-control"  required data-bv-notempty-message="Harus diisi">
							  <option value=''>-- pilih provinsi --</option>
							  <?php foreach($this->region_db->provinsi()->result() as $row):?>
							      <option value='<?=$row->id?>'><?=$row->name?></option>
							  <?php endforeach;?>
							  
							</select>
						    </div>
						</div>
						<div class="col-md-4">
						    <div class='form-group'>
							<label for="">Kabupaten / Kota</label>
							<select name='kabkota' id='kabkota' class="form-control"  required data-bv-notempty-message="Harus diisi">
							  <option value=''>-- pilih kab/kota --</option>
							</select>
						    </div>
						</div>
						<div class="col-md-4">
						    <div class='form-group'>
							<label for="">Kecamatan</label>
							<select class="form-control" id='kecamatan' name='kecamatan'  required data-bv-notempty-message="Harus diisi">
							  <option value=''>-- pilih kecamatan --</option>
							  
							</select>
						    </div>
						</div>
					    </div>
					</div>
				    </div>
				    <div class='row'>
					<div class="col-md-2">Lokasi</div>
					<div class="col-md-10">
					    <div class='form-group'>
						<select name='lokasi' id='lokasi' class="form-control"  required data-bv-notempty-message="Harus diisi">
						    <option value=''>-- pilih --</option>
						    <?php foreach($this->select_db->lokasi()->result() as $row):?>
							<option value='<?=$row->id?>'><?=$row->name?></option>
						    <?php endforeach;?>	
						</select>
					    </div>
					</div>
				    </div>
				    
				    <div class='row' id='jenjang_sekolah_form' style='display:none'>
					<div class="col-md-2">Jenjang</div>
					<div class="col-md-10">
					    <div class="row">
						<div class="col-md-6">
						    <div class='form-group'>
							<label for="">Status Sekolah</label>
							<select name='status_sekolah' id='status_sekolah' class="form-control" required data-bv-notempty-message="Harus diisi">
							  <option value=''>-- pilih --</option>
							  <?php 
							      $status_sekolah = $this->select_db->status_sekolah()->result();
							      foreach($status_sekolah as $row):?>
								<option value="<?=$row->id?>"><?=$row->name?></option>
							  <?php endforeach;?>
							</select>
						    </div>
						</div>
						<div class="col-md-6">
						    <div class='form-group'>
							<label for="">Jenjang Pendidikan</label>
							<select name='jenjang' id='jenjang' class="form-control" required data-bv-notempty-message="Harus diisi">
							  <option value=''>-- pilih --</option>
							  <?php 
							      $jenjang = $this->select_db->jenjang()->result();
							      foreach($jenjang as $row):?>
								<option value="<?=$row->id?>"><?=$row->name?></option>
							  <?php endforeach;?>
							</select>
						    </div>
						</div>
					    </div>
					    
					</div>
				    </div>
				    <div class='row'>
					<div class="col-md-2" id='title_nama_lokasi'>Nama Lokasi</div>
					<div class="col-md-10">
					    <div class="form-group">
						<input  required name='nama_lokasi' id='nama_lokasi' type="text" class="form-control" placeholder="Nama Lokasi" data-bv-notempty-message="Harus diisi">
					    </div>
					</div>
				    </div>
				    
				    <h4>Jenis Pengaduan</h4>
				    <div class='row'>
					<div class="col-md-2">Kategori</div>
					<div class="col-md-10">
					    <div class='form-group'>
						<select name='kategori' id='kategori' class="form-control" required data-bv-notempty-message="Harus diisi">
						  <option value=''>-- pilih --</option>
						  <?php 
						      $kategori = $this->select_db->kategori()->result();
						      foreach($kategori as $row):?>
						      <option value="<?=$row->id?>"><?=$row->name?></option>
						  <?php endforeach;?>
						</select>
					    </div>
					</div>
				    </div>
				    
				    <div class='row'>
					<div class="col-md-2">Substansi</div>
					<div class="col-md-10">
					    <div class='form-group'>
						<?php
						    $isu = $this->select_db->isu()->result();
						    foreach($isu as $row):
						    ?>
							<div class="checkbox-inline no_indent">
							  <label>
							    <input type="checkbox" name="isu[]" value="<?=$row->id?>"> <?=$row->name?>
							  </label>
							</div>
						    <?php endforeach;?>
					    </div>
					</div>
				    </div>
				    
				    
				    <h4>Identitas Pelapor</h4>
				    <div class='row'>
					<div class="col-md-2">Peran</div>
					<div class="col-md-10">
					    <div class='form-group'>
						<select name='sumber_info' id='sumber_info' class="form-control" required data-bv-notempty-message="Harus diisi">
						  <option value=''>-- pilih --</option>
						  <?php 
						      $sumber = $this->select_db->sumber_info($role='home')->result();
						      foreach($sumber as $row):?>
						      <option value="<?=$row->id?>"><?=$row->name?></option>
						  <?php endforeach;?>
						</select>
					    </div>
					    <div class="form-group" id='sumber_lain_form' style='display:none'>
						<input name='sumber_lain' id='sumber_lain' type="text" required class="form-control" placeholder="Sumber lain" required data-bv-notempty-message="Harus diisi">
					    </div>
					</div>
				    </div>
				    <div class='row'>
					<div class="col-md-2">Nama</div>
					<div class="col-md-10">
					    <div class="form-group">
						<input name='nama' id='nama' type="text" class="form-control" placeholder="Nama" required data-bv-notempty-message="Harus diisi">
					    </div>
					    <div class="checkbox">
						<label>
						    <input type="checkbox" name="param[tampil_nama]" value="1"> Apakah anda ingin nama anda ditampilkan? 
						</label>
					    </div>
					</div>
				    </div>
				    <div class='row'>
					<div class="col-md-2">Email</div>
					<div class="col-md-10">
					    <div class="form-group">
						<input name='email' type="email" class="form-control" placeholder="Email" required data-bv-notempty-message="Harus diisi">
					    </div>
					</div>
				    </div>
				    <div class='row'>
					<div class="col-md-2">Telp</div>
					<div class="col-md-10">
					    <div class="form-group">
						<input id='telp' name='telp' type="text" class="form-control" placeholder="Telp" required data-bv-notempty-message="Harus diisi">
					    </div>
					    <div class="checkbox">
						<label>
						    <input type="checkbox" name="param[tampil_telp]" value="1"> Apakah anda ingin telp anda ditampilkan? 
						</label>
					    </div>
					</div>
				    </div>
				    <div class='row'>
					<div class="col-md-2">Alamat</div>
					<div class="col-md-10">
					    <div class="form-group">
						<textarea name='alamat' id='alamat' class="form-control" rows="3" required data-bv-notempty-message="Harus diisi"></textarea>
					    </div>
					    <div class="checkbox">
						<label>
						    <input type="checkbox" name="param[tampil_alamat]" value="1"> Apakah anda ingin alamat anda ditampilkan? 
						</label>
					    </div>
					</div>
				    </div>
				    
				    <h4>Deskripsi</h4>
				    <div class='row'>
					<div class="col-md-2" id='title_deskripsi'></div>
					<div class="col-md-10">
					    <div class="form-group">
						<textarea name='deskripsi' id='deskripsi' class="form-control" rows="3" required data-bv-notempty-message="Harus diisi"></textarea>
					    </div>
					</div>
				    </div>
				    
				    <h4>Verifikasi</h4>
				    <div class='row'>
					
					<div class="col-md-10 col-md-offset-2">
					    <div class="form-group">
						<?php
						    $publickey = $this->config->item('public_key');; // you got this from the signup page
						    echo recaptcha_get_html($publickey);
						?>
					    </div>
					    
					</div>
				    </div>
				    <div class='row'>
					<div class="col-md-3 col-md-offset-9">
					    <input type='submit' class='mainBtn btn-block' value='Kirim'>
					</div>
					
				    </div>
				</form>
				<!--./content-->
			    </div>
			</div>
		    </div>
		</div>
	    </div>
	</div>
    </div>
