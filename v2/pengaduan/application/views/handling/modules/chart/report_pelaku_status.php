<p align="center"><strong>Keterangan : </strong>yang dimaksud dengan status pengaduan adalah progres penanganan pengaduan (pending / proses / selesai)</p>

<div class="table-responsive">
<table class="table">
<tr>
	<td><strong>Pelaku</strong></td>
	<td><strong>Pending</strong></td>
	<td><strong>Proses</strong></td>
	<td><strong>Selesai</strong></td>
	<td><strong>Total</strong></td>
</tr>
<?php
	$t_pending = $t_proses = $t_selesai = $t_tot=0;
	foreach($report as $row):?>
<tr>
	<td><?=$row->pelaku?></td>
	<td><?=$row->pending?></td>
	<td><?=$row->proses?></td>
	<td><?=$row->selesai?></td>
	<td><?=$row->total?></td>
	<?php
	$t_pending+=$row->pending;
	$t_proses+=$row->proses;
	$t_selesai+=$row->selesai;
	$t_tot+=$row->total;
	?>
</tr>
<?php endforeach;?>
<tr>
	<td><strong>Total</strong></td>
	<td><strong><?=$t_pending?></strong></td>
	<td><strong><?=$t_proses?></strong></td>
	<td><strong><?=$t_selesai?></strong></td>
	<td><strong><?=$t_tot?></strong></td>
</tr>
</table>
</div>
