<?php
    $edit = false;
    $uri3 = $this->uri->segment(3);
    $uri4 = $this->uri->segment(4);
    if($uri3=='form_jawab' && $uri4!=''){
	$edit = true;
    }
?>
        <header><label>Deskripsi Pengaduan</label></header>
        <div class="row">
          <div class="col-sm-2">Oleh</div>
          <div class="col-sm-10">
		<div class="form-inline">
		  <?php
		  $query = $this->pengaduan_db->get_param($pengaduan->id);
		  $param = array();
		  foreach($query->result() as $row1){
		      $param[$row1->key] = $row1->value;
		  }
		  
		  ?>
		      
		      <span class="glyphicon glyphicon-envelope"></span> <?=$pengaduan->email?>
		      <span class="glyphicon glyphicon-user"></span> <?=($param['tampil_nama'] ? $pengaduan->nama : 'xxx')?>
		      <span class="glyphicon glyphicon-phone-alt"></span> <?=($param['tampil_telp'] ? '<a href="tel:'.$pengaduan->handphone.'">'.$pengaduan->handphone.'</a>' : 'xxx')?>
		      <span class="glyphicon glyphicon-home"></span> <?=($param['tampil_alamat'] ? $pengaduan->alamat_rumah : 'xxx')?>
		</div>
          </div>
          <div class="col-sm-2">Deskripsi</div>
          <div class="col-sm-10">
		<div class="form-inline">
		  <div class="expandable"><p><?=nl2br(html_entity_decode($pengaduan->deskripsi))?></p></div>
		</div>
          </div>
        </div>
        <header><label>Penanganan Pengaduan</label></header>
        <div class="row">
          <div class="col-sm-2"><label>Kategori</label></div>
          <div class="col-sm-10">
            
            <div class='form-inline'>
                <?=$pengaduan->kategori?>
                
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-2"><label>Media</label></div>
          <div class="col-sm-10"><?=$pengaduan->media?></div>
        </div>
        <div class="row">
          <div class="col-sm-2"><label>Tanggal</label></div>
          <div class="col-sm-10">
            <div class="input-group date">
                <input type="text" class="form-control" placeholder='tanggal' name='tanggal' value='<?=(isset($respon->tanggal) ? mysqldatetime_to_date($respon->tanggal,"d/m/Y H:i") : date("d/m/Y H:i",time()));?>'> <span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
              </div>
            </div>
        </div>
        <div class="row">
          <div class="col-sm-2"><label>Status Penyelesaian Penanganan</label></div>
          <div class="col-sm-10 form-inline">
            
            <?php
		
		$kategori_penyelesaian1 = '';
		if($edit){    
		    if($query = $this->respon_db->get(base64_decode($uri4))){
			$respon = $query->row();
			if($query = $this->select_db->penyelesaian(array('id' => $respon->penyelesaian_id))){
			    $penyelesaian = $query->row();
			    $kategori_penyelesaian1 = $this->select_db->kategori_penyelesaian(array('id' => $penyelesaian->kategori_penyelesaian_id));
			}
			//$pengaduan = $this->pengaduan_db->get_pengaduan($respon->pengaduan_id)->row();
		    }
		}
		
		$kategori_penyelesaian = $this->select_db->kategori_penyelesaian()->result();
		foreach($kategori_penyelesaian as $row):
            ?>
            <div class="radio">
              <label>
                <input class='kategori_penyelesaian' type="radio" name='kategori_penyelesaian' value="<?=$row->id?>" <?=($edit && $kategori_penyelesaian1 ? 'checked' : '')?> >
                <?=$row->name?>
              </label>
            </div>
            <?php endforeach;?>
            
            <!--penyelesaian-->
            <select style='display:none;' id='penyelesaian' class="form-control" name='penyelesaian'>
              <option value="">-Pilih-</option>
              <?php
		  if($edit):
		    foreach($this->select_db->penyelesaian(array('kategori_penyelesaian_id' => $penyelesaian->kategori_penyelesaian_id))->result() as $row):
		    //echo $this->db->last_query();
              ?>
              <option value="<?=$row->id?>" <?=($respon->penyelesaian_id == $row->id ? 'selected' : '')?>><?=$row->name?></option>
		  <?php endforeach;?>
              <?php endif;?>
              
            </select>
            <!--end penyelesaian-->
            
           
          </div>
        </div>
        <div class="row">
          <div class="col-sm-2"><label>Status</label></div>
          <div class="col-sm-10 form-inline">
	    <?php
		$status = $this->select_db->status()->result();
		foreach($status as $row):
	    ?>
            <div class="radio">
              <label>
                <input type="radio" name='status' value="<?=$row->id?>" <?=($pengaduan->status_id == $row->id ? 'checked' : '')?> >
                <?=$row->name?>
              </label>
            </div>
            <?php
		endforeach;
            ?>
            
          </div>
        </div>
        <div class="row">
          <div class="col-sm-2"><label>Pelaku</label></div>
          <div class="col-sm-10 form-inline">
	    <?php
            $pelaku=$this->select_db->pelaku()->result();
            if(isset($pengaduan->id))
		$pengaduan_pelaku = $this->pengaduan_db->get_pelaku($pengaduan->id)->result();
            foreach($pelaku as $row):
            ?>
            <div class="checkbox">
              <label>
                <input class='pelaku' type="checkbox" name='pelaku[]' value="<?=$row->id?>" <?=(isset($pengaduan_pelaku->pelaku_id) && $row->id == $pengaduan_pelaku->pelaku_id ? 'checked="checked"' : '')?>>
                <?=$row->name?>
              </label>
            </div>
            
            <?php endforeach;?>
            <div class="form-group">
              <label class="sr-only" for="lokasi">Lokasi</label>
              <input type="text" class="form-control" id="lokasi" placeholder="Lainnya" name='nama_sekolah' value=''>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-2"><label>Uraian</label></div>
          <div class="col-sm-10"><textarea class="form-control" rows="3" name='uraian'><?=(isset($respon->deskripsi) ? $respon->deskripsi : '')?></textarea></div>
        </div>
        
        </div>