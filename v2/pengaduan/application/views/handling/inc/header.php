    <?php $user_handling = $this->session->userdata('user_handling');?>
    <div class="navbar navbar-default navbar-static-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <div class="page-header" style="margin: 0; border-bottom: none">
              <a href="#" style="float:left"><img style="height:70px;" src="<?=$this->config->item('home_img')?>/logo_kurikulum_2013.png"></a>
              <h2 style="margin-top: 8px; color:#fff">Pelayanan dan Penanganan Pengaduan Masyarakat<br>Kurikulum 2013</h2></div>
        </div>
          <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
            <li><p class="navbar-text" style="color: #fff">Selamat Datang,</p></li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" style="font-weight: bold;
text-transform: capitalize;
font-size: 126%; color: #fff"><?=$user_handling->nama?> <b class="caret"></b></a>
              <ul class="dropdown-menu">
                <li><a href="#" data-toggle='modal' data-target='#modal_ganti_password'><span class="glyphicon glyphicon-lock"></span>Ganti Password</a></li>
                <li><a href="#" data-toggle='modal' data-target='#modal_bantuan'><span class="glyphicon glyphicon-question-sign"></span> bantuan</a></li>
                <li><a href="#" data-toggle='modal' data-target='#modal_logout'><span class="glyphicon glyphicon-log-out"></span> Keluar</a></li>
              </ul>
            </li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </div>
    
    