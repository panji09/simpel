<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Administrator Console</title>
<link rel="stylesheet" href="<?=$this->config->item('handling_css')?>/g3n.css" type="text/css" media="screen" />
<style>

.logo a {
    background: none;
}
#header{
    background: #003a6a !important;
    
}
.button input{
    background: #003a6a !important;
    border-color: #fff !important;
}
</style>
</head>

<body>
<div id="header" >
	<div class="logo">
		<a href="" title="BOS"><img src='<?=$this->config->item('home_img')?>/logo_kurikulum_2013.png' height='70px'></a>
		
	</div>
	<div class='textnya' style='font-size:1.8em; margin:-1.5em 0 0 0; font-weight:bold; text-align:center;'>
		<span style="color:#fff;">Pelayanan dan Penanganan Pengaduan Masyarakat<br>Kurikulum 2013</span>
	</div>
</div>



<div id="loginform">
	<form name="loginform" method="post" action="<?=site_url('handling/login/post_login')?>">
		<div class="l" >
			<span class="branding"></span>
			<h1>Login Form</h1>
			<?php if($this->session->flashdata('login.error')):?>
				<div class="message"><p class="error"><?=$this->session->flashdata('login.error')?></p></div>
			<?php endif;?>
			<p id="form-login-username">
				<label for="txtuserid">User ID</label>
				<input name="username" id="txtuserid" type="text" class="inputbox" size="29" />
			</p>
			<p id="form-login-password">
				<label for="txtpassword">Password</label>
				<input name="password" id="txtpassword" type="password" class="inputbox" size="29" />
			</p>
			<div class="button">
				<input type="submit" name="btnlogin" id="btnlogin" value="Login" />
				<input type="button" name="btncancel" id="btncancel" value="Kembali" onclick="javascript:location.href='<?=site_url('')?>';"/>
				
			</div>
		</div>
	</form>
</div>

<div id="footer">
	<p class="copyright">
		&copy; <?=date('Y'); ?> Kementerian Pendidikan dan Kebudayaan RI
	</p>
</div>
</body>
</html>
