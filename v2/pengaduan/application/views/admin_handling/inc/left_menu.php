<?php
$uri2=$this->uri->segment(2);
$uri3=$this->uri->segment(3);
$uri4=$this->uri->segment(4);
$admin = $this->session->userdata('admin');
$role_level = $this->user_admin_db->get_role_level($admin->id)->row();
?>
<div id="left">
        <div class="media user-media">
          <a class="user-link" href="">
            <img class="media-object img-thumbnail user-img" alt="User Picture" src="<?=$this->config->item('admin_handling_img')?>/user.png">
            <?php
		$query = $this->pengaduan_db->get_all(array('approved' => 0));
		
		//echo $this->db->last_query();
		$num_unapproved = $query->num_rows();
		
		if($num_unapproved):
            ?>
            <span class="label label-danger user-label"><?=$num_unapproved?></span>
	    <?php endif;
	    ?>
          </a>
          <div class="media-body">
            <h5 class="media-heading"><?=$admin->nama?></h5>
            <ul class="list-unstyled user-info">
              <li> <a href=""><?=$this->select_db->level_admin(array('id' =>$role_level->level_id))->row()->name?></a> </li>
              <li>Terakhir Login :
                <br>
                <small>
                  <i class="fa fa-calendar"></i>&nbsp;<time class="" datetime="<?=date('c',$admin->last_login)?>"><?=time_ago($admin->last_login)?></time>
		</small>
              </li>
            </ul>
          </div>
        </div>

        <!-- #menu -->
        <ul id="menu" class="collapse">
          <li class="nav-header">Menu</li>
          <li class="nav-divider"></li>
          <li <?=($uri2=='pengaduan' || $uri2=='pesan_sms' ? 'class="active"' : '')?>>
            <a href="javascript:;">
              <i class="glyphicon-th-list"></i>
              <span class="link-title">Pengaduan</span>
              <span class="fa arrow"></span>
            </a>
            <ul>
              <li <?=($uri2=='pengaduan' ? 'class="active"' : '')?>>
                <a href="<?=site_url('admin_handling/pengaduan')?>">
                  <i class="fa fa-angle-right"></i>&nbsp;Pengaduan Online
                </a>
              </li>
              <li <?=($uri2=='pesan_sms' ? 'class="active"' : '')?>>
                <a href="<?=site_url('admin_handling/pesan_sms')?>">
                  <i class="fa fa-angle-right"></i>&nbsp;Pesan SMS
                </a>
              </li>
            </ul>
          </li>
          
          <?php
	      
	      if($role_level->level_id == 1):
          ?>
          <li <?=($uri2=='user_handling' ? 'class="active"' : '')?>>
            <a href="<?=site_url('admin_handling/user_handling')?>">
              <i class="fa fa-table"></i>&nbsp; User Handling</a>
          </li>
          <li <?=($uri2=='user_admin' ? 'class="active"' : '')?>>
            <a href="<?=site_url('admin_handling/user_admin')?>">
              <i class="fa fa-table"></i>&nbsp; User Admin</a>
          </li>
          <?php
	      endif;
          ?>
          
        </ul><!-- /#menu -->
      </div><!-- /#left -->