    <script src="<?=$this->config->item('plugin')?>/jquery.min.js"></script>
    <script src="<?=$this->config->item('plugin')?>/bootstrap/js/bootstrap.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
    <script src="<?=$this->config->item('plugin')?>/fullcalendar-1.6.2/fullcalendar/fullcalendar.min.js"></script>
    <script src="<?=$this->config->item('plugin')?>/tablesorter/js/jquery.tablesorter.min.js"></script>
    <script src="<?=$this->config->item('plugin')?>/sparkline/jquery.sparkline.min.js"></script>
    <script src="<?=$this->config->item('plugin')?>/flot/jquery.flot.js"></script>
    <script src="<?=$this->config->item('plugin')?>/flot/jquery.flot.selection.js"></script>
    <script src="<?=$this->config->item('plugin')?>/flot/jquery.flot.resize.js"></script>
    <script src="<?=$this->config->item('admin_handling_js')?>/main.js"></script>
    <script src="<?=$this->config->item('plugin')?>/ckeditor/ckeditor.js"></script>
    <script src="<?=$this->config->item('plugin')?>/formwizard/js/jquery.form.wizard.js"></script>
    <script src="<?=$this->config->item('plugin')?>/jquery-validation-1.11.1/dist/jquery.validate.min.js"></script>
    <script src="<?=$this->config->item('plugin')?>/jquery-timeago/jquery.timeago.js"></script>
    <script src="<?=$this->config->item('plugin')?>/jquery-expander/jquery.expander.min.js"></script>
    
    <!-- datatables-->
    <script type="text/javascript" language="javascript" src="<?=$this->config->item('admin_plugin')?>/datatables/media/js/jquery.dataTables.js"></script>
    <script src="<?=$this->config->item('handling_plugin')?>/datatables-bootstrap/js/datatables.js"></script>
    <!-- end datatables-->
    
    <!--time ago-->
    <script>
    $(document).ready(function() {
      $("time.timeago").timeago();
    });
    </script>
    <!--./time ago -->
    
    <!-- halaman -->
    <?php if($this->uri->segment(2)=='halaman' && ($this->uri->segment(4)=='edit' || $this->uri->segment(4)=='add')):?>
     <script>
      $(document).ready(function(){
	$('#form_content').submit(function(){
	 
	  $('#hide_input').html('');
	  $('.name_link').each(function(index){
	    append = '<input type="hidden" name="name_link[]" value="'+$(this).val()+'">';
	    $('#hide_input').append(append);
	  });
	  $('.name_file').each(function(index){
	    append = '<input type="hidden" name="name_file[]" value="'+$(this).val()+'">';
	    $('#hide_input').append(append);
	  });
	  $('.size_file').each(function(index){
	    append = '<input type="hidden" name="size_file[]" value="'+$(this).val()+'">';
	    $('#hide_input').append(append);
	  });
	  
	});
      });
      </script>
    <!-- The blueimp Gallery widget -->
    <div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls" data-filter=":even">
        <div class="slides"></div>
        <h3 class="title"></h3>
        <a class="prev">‹</a>
        <a class="next">›</a>
        <a class="close">×</a>
        <a class="play-pause"></a>
        <ol class="indicator"></ol>
    </div>
    <!-- The template to display files available for upload -->
    <script id="template-upload" type="text/x-tmpl">
    {% for (var i=0, file; file=o.files[i]; i++) { %}
        <tr class="template-upload fade">
            <td>
                <span class="preview"></span>
            </td>
            <td>
                <p class="name">{%=file.name%}</p>
                <strong class="error text-danger"></strong>
            </td>
            <td>
                <p class="size">Processing...</p>
                <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="progress-bar progress-bar-success" style="width:0%;"></div></div>
            </td>
            <td>
                {% if (!i && !o.options.autoUpload) { %}
                    <button class="btn btn-primary start" disabled>
                        <i class="glyphicon glyphicon-upload"></i>
                        <span>Start</span>
                    </button>
                {% } %}
                {% if (!i) { %}
                    <button class="btn btn-warning cancel">
                        <i class="glyphicon glyphicon-ban-circle"></i>
                        <span>Cancel</span>
                    </button>
                {% } %}
            </td>
        </tr>
    {% } %}
    </script>
    <!-- The template to display files available for download -->
    <script id="template-download" type="text/x-tmpl">
    {% for (var i=0, file; file=o.files[i]; i++) { %}
        <tr class="template-download fade">
            <td>
                <span class="preview">
                    {% if (file.thumbnailUrl) { %}
                        <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" data-gallery><img src="{%=file.thumbnailUrl%}"></a>
                    {% } %}
                </span>
            </td>
            <td>
                <p class="name">
                    {% if (file.url) { %}
                        <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" {%=file.thumbnailUrl?'data-gallery':''%}>{%=file.name%}</a>
                    {% } else { %}
                        <span>{%=file.name%}</span>
                    {% } %}
                </p>
                {% if (file.error) { %}
                    <div><span class="label label-danger">Error</span> {%=file.error%}</div>
                    
                {% }else{ %}
		    <p class="name">
		      <input class='form-control name_link' type='text' value='{%=file.name%}'>
		      <input class='form-control name_file' type='hidden' value='{%=file.name%}'>
		      <input class='form-control size_file' type='hidden' value='{%=o.formatFileSize(file.size)%}'>
		    </p>
                {% } %}
            </td>
            <td>
                <span class="size">{%=o.formatFileSize(file.size)%}</span>
            </td>
            <td>
                {% if (file.deleteUrl) { %}
                    <button class="btn btn-danger delete" data-type="{%=file.deleteType%}" data-url="{%=file.deleteUrl%}"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
                        <i class="glyphicon glyphicon-trash"></i>
                        <span>Delete</span>
                    </button>
                    <input type="checkbox" name="delete" value="1" class="toggle">
                {% } else { %}
                    <button class="btn btn-warning cancel">
                        <i class="glyphicon glyphicon-ban-circle"></i>
                        <span>Cancel</span>
                    </button>
                {% } %}
            </td>
        </tr>
    {% } %}
    </script>
    <!-- The jQuery UI widget factory, can be omitted if jQuery UI is already included -->
    <script src="<?=$this->config->item('plugin')?>/jquery-file-upload/js/vendor/jquery.ui.widget.js"></script>
    <!-- The Templates plugin is included to render the upload/download listings -->
    <script src="<?=$this->config->item('plugin')?>/jquery-file-upload/js/tmpl.min.js"></script>
    <!-- The Load Image plugin is included for the preview images and image resizing functionality -->
    <script src="<?=$this->config->item('plugin')?>/jquery-file-upload/js/load-image.min.js"></script>
    <!-- The Canvas to Blob plugin is included for image resizing functionality -->
    <script src="<?=$this->config->item('plugin')?>/jquery-file-upload/js/canvas-to-blob.min.js"></script>
    <!-- Bootstrap JS is not required, but included for the responsive demo navigation 
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script> -->
    <!-- blueimp Gallery script -->
    <script src="<?=$this->config->item('plugin')?>/jquery-file-upload/js/jquery.blueimp-gallery.min.js"></script>
    <!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
    <script src="<?=$this->config->item('plugin')?>/jquery-file-upload/js/jquery.iframe-transport.js"></script>
    <!-- The basic File Upload plugin -->
    <script src="<?=$this->config->item('plugin')?>/jquery-file-upload/js/jquery.fileupload.js"></script>
    <!-- The File Upload processing plugin -->
    <script src="<?=$this->config->item('plugin')?>/jquery-file-upload/js/jquery.fileupload-process.js"></script>
    <!-- The File Upload image preview & resize plugin -->
    <script src="<?=$this->config->item('plugin')?>/jquery-file-upload/js/jquery.fileupload-image.js"></script>
    <!-- The File Upload audio preview plugin -->
    <script src="<?=$this->config->item('plugin')?>/jquery-file-upload/js/jquery.fileupload-audio.js"></script>
    <!-- The File Upload video preview plugin -->
    <script src="<?=$this->config->item('plugin')?>/jquery-file-upload/js/jquery.fileupload-video.js"></script>
    <!-- The File Upload validation plugin -->
    <script src="<?=$this->config->item('plugin')?>/jquery-file-upload/js/jquery.fileupload-validate.js"></script>
    <!-- The File Upload user interface plugin -->
    <script src="<?=$this->config->item('plugin')?>/jquery-file-upload/js/jquery.fileupload-ui.js"></script>
    <!-- The main application script -->
    <script src="<?=$this->config->item('plugin')?>/jquery-file-upload/js/main.js"></script>
    <!-- The XDomainRequest Transport is included for cross-domain file deletion for IE 8 and IE 9 -->
    <!--[if (gte IE 8)&(lt IE 10)]>
    <script src="<?=$this->config->item('plugin')?>/jquery-file-upload/js/cors/jquery.xdr-transport.js"></script>
    <![endif]-->
    <?php endif;?>
    
    
    
    <?php if($this->uri->segment(2)=='halaman'):?>
    <script type="text/javascript" charset="utf-8">
      var asInitVals = new Array();
      var i=0;
      var update=true;
      $(document).ready(function() {		      
        $.fn.dataTableExt.oApi.fnPagingInfo = function ( oSettings )
        {
          return {
            "iStart":         oSettings._iDisplayStart,
            "iEnd":           oSettings.fnDisplayEnd(),
            "iLength":        oSettings._iDisplayLength,
            "iTotal":         oSettings.fnRecordsTotal(),
            "iFilteredTotal": oSettings.fnRecordsDisplay(),
            "iPage":          Math.ceil( oSettings._iDisplayStart / oSettings._iDisplayLength ),
            "iTotalPages":    Math.ceil( oSettings.fnRecordsDisplay() / oSettings._iDisplayLength )
          };
        }

        var oTable = $('#content_dinamis').dataTable( {
          "bStateSave": false,
          "bProcessing": true,
          "bServerSide": true,
          "iDisplayLength": 25,
          "sPaginationType": "bs_full",
          "sAjaxSource": "<?=(isset($load_url) ? $load_url : '')?>",
          "sServerMethod": "POST",
          "aoColumnDefs": [ 
            { "bSortable": false, "aTargets": [ 0 ] }, //no
            { "bSortable": false, "aTargets": [ 5 ] }, //action
            {
              "fnRender": function ( oObj ) {
                if(update){
                  i=oTable.fnPagingInfo().iStart;
                  update=false;
                }
                if(i+1 == oTable.fnPagingInfo().iEnd){
                  update=true;
                }
                
                return ++i;
                      
              },
              "bUseRendered": false,
              "aTargets": [ 0 ] //no
            }
          ],
          "fnDrawCallback": function () {
	      $('.btn_delete').click(function(){
		  $('#link_delete').attr('href', $(this).attr('data-href'));
	      });
          }
        } );
			      
			    
        $('#content_dinamis').each(function(){
            var datatable = $(this);
            // SEARCH - Add the placeholder for Search and Turn this into in-line form control
            var search_input = datatable.closest('.dataTables_wrapper').find('div[id$=_filter] input');
            search_input.attr('placeholder', 'Search');
            search_input.addClass('form-control input-sm');
            // LENGTH - Inline-Form control
            var length_sel = datatable.closest('.dataTables_wrapper').find('div[id$=_length] select');
            length_sel.addClass('form-control input-sm');
        });
        
			    
      });
      </script>
      <?php endif;?>
      <!-- end halaman -->
      
            <!-- pesan sms -->
      <?php if($this->uri->segment(2)=='pesan_sms'):?>
      <!-- datatables-->
      <script type="text/javascript" language="javascript" src="<?=$this->config->item('admin_plugin')?>/datatables/media/js/jquery.dataTables.js"></script>
      <script src="<?=$this->config->item('handling_plugin')?>/datatables-bootstrap/js/datatables.js"></script>
      <script type="text/javascript" charset="utf-8">
	var asInitVals = new Array();
	var i=0;
	var update=true;
	$(document).ready(function() {		      
	  $.fn.dataTableExt.oApi.fnPagingInfo = function ( oSettings )
	  {
	    return {
	      "iStart":         oSettings._iDisplayStart,
	      "iEnd":           oSettings.fnDisplayEnd(),
	      "iLength":        oSettings._iDisplayLength,
	      "iTotal":         oSettings.fnRecordsTotal(),
	      "iFilteredTotal": oSettings.fnRecordsDisplay(),
	      "iPage":          Math.ceil( oSettings._iDisplayStart / oSettings._iDisplayLength ),
	      "iTotalPages":    Math.ceil( oSettings.fnRecordsDisplay() / oSettings._iDisplayLength )
	    };
	  }

	  var oTable = $('#pengaduan').dataTable( {
	    "bStateSave": false,
	    "bProcessing": true,
	    "bServerSide": true,
	    "iDisplayLength": 25,
	    "sPaginationType": "bs_full",
	    "sAjaxSource": "<?=(isset($load_url) ? $load_url : '')?>",
	    "sServerMethod": "POST",
	    "aoColumnDefs": [ 
	      { "bSortable": false, "aTargets": [ 0 ] }, //no
	      { "bSortable": false, "aTargets": [ 12 ] }, //action
	      {
		"fnRender": function ( oObj ) {
		  if(update){
		    i=oTable.fnPagingInfo().iStart;
		    update=false;
		  }
		  if(i+1 == oTable.fnPagingInfo().iEnd){
		    update=true;
		  }
		  
		  return ++i;
			
		},
		"bUseRendered": false,
		"aTargets": [ 0 ] //no
	      }
	    ],
	    "fnDrawCallback": function () {
		$('.btn_delete').click(function(){
		    $('#modal_delete_reason form').attr('action', $(this).attr('data-action'));
		});
		$('div.expander').expander({
		    slicePoint: 400,
		    widow: 2,
		    expandEffect: 'show',
		    userCollapseText: '[^]'
		});
		$('.btn_edit').click(function(){
		    $('#modal_approved form').attr('action', $(this).attr('data-action'));
		    data_published = $(this).attr('data-published');
		    data_approved = $(this).attr('data-approved');
		    $( "#modal_approved .modal-body" ).html('<img src="<?=$this->config->item('admin_handling_img')?>/loading.gif">');
		    $.get( "<?=site_url('admin_handling/pesan_sms/load_pengaduan')?>/"+$(this).attr('data-id'), function( data ) {
		      
			$( "#modal_approved .modal-body" ).html( data );
			//alert( data_published );
			
			if(data_published=='ya'){
			  $('#published').prop('checked',true);
			}
			else{
			    $('#published').prop('checked',false);
			}
			
			if(data_approved=='ya'){
			    $('#approved').prop('checked',true);
			}
			else{
			    $('#approved').prop('checked',false);
			}
		    });
		    
		    
		    
		});
	    }
	  } );
	  
	  oTable.fnSort( [ [2,'DESC'], [10,'DESC']] ); //sort tanggal
	  $('#pengaduan').each(function(){
	      var datatable = $(this);
	      // SEARCH - Add the placeholder for Search and Turn this into in-line form control
	      var search_input = datatable.closest('.dataTables_wrapper').find('div[id$=_filter] input');
	      search_input.attr('placeholder', 'Search');
	      search_input.addClass('form-control input-sm');
	      // LENGTH - Inline-Form control
	      var length_sel = datatable.closest('.dataTables_wrapper').find('div[id$=_length] select');
	      length_sel.addClass('form-control input-sm');
	  });
	  
			      
	});
      </script>
      <!-- end datatables-->
      <?php endif;?>
      <!-- end pesan sms -->
      
      
      <!-- pengaduan -->
      <?php if($this->uri->segment(2)=='pengaduan'):?>
      <!-- datatables-->
      <script type="text/javascript" language="javascript" src="<?=$this->config->item('admin_plugin')?>/datatables/media/js/jquery.dataTables.js"></script>
      <script src="<?=$this->config->item('handling_plugin')?>/datatables-bootstrap/js/datatables.js"></script>
      <script type="text/javascript" charset="utf-8">
	var asInitVals = new Array();
	var i=0;
	var update=true;
	$(document).ready(function() {		      
	  $.fn.dataTableExt.oApi.fnPagingInfo = function ( oSettings )
	  {
	    return {
	      "iStart":         oSettings._iDisplayStart,
	      "iEnd":           oSettings.fnDisplayEnd(),
	      "iLength":        oSettings._iDisplayLength,
	      "iTotal":         oSettings.fnRecordsTotal(),
	      "iFilteredTotal": oSettings.fnRecordsDisplay(),
	      "iPage":          Math.ceil( oSettings._iDisplayStart / oSettings._iDisplayLength ),
	      "iTotalPages":    Math.ceil( oSettings.fnRecordsDisplay() / oSettings._iDisplayLength )
	    };
	  }

	  var oTable = $('#pengaduan').dataTable( {
	    "bStateSave": false,
	    "bProcessing": true,
	    "bServerSide": true,
	    "iDisplayLength": 25,
	    "sPaginationType": "bs_full",
	    "sAjaxSource": "<?=(isset($load_url) ? $load_url : '')?>",
	    "sServerMethod": "POST",
	    "aoColumnDefs": [ 
	      { "bSortable": false, "aTargets": [ 0 ] }, //no
	      { "bSortable": false, "aTargets": [ 11 ] }, //action
	      {
		"fnRender": function ( oObj ) {
		  if(update){
		    i=oTable.fnPagingInfo().iStart;
		    update=false;
		  }
		  if(i+1 == oTable.fnPagingInfo().iEnd){
		    update=true;
		  }
		  
		  return ++i;
			
		},
		"bUseRendered": false,
		"aTargets": [ 0 ] //no
	      }
	    ],
	    "fnDrawCallback": function () {
		$('.btn_delete').click(function(){
		    $('#modal_delete_reason form').attr('action', $(this).attr('data-action'));
		});
		$('div.expander').expander({
		    slicePoint: 400,
		    widow: 2,
		    expandEffect: 'show',
		    userCollapseText: '[^]'
		});
		$('.btn_edit').click(function(){
		    $('#modal_approved form').attr('action', $(this).attr('data-action'));
		    data_published = $(this).attr('data-published');
		    data_approved = $(this).attr('data-approved');
		    $( "#modal_approved .modal-body" ).html('<img src="<?=$this->config->item('admin_handling_img')?>/loading.gif">');
		    $.get( "<?=site_url('admin_handling/pengaduan/load_pengaduan')?>/"+$(this).attr('data-id'), function( data ) {
		      
			$( "#modal_approved .modal-body" ).html( data );
			//alert( data_published );
			
			if(data_published=='ya'){
			  $('#published').prop('checked',true);
			}
			else{
			    $('#published').prop('checked',false);
			}
			
			if(data_approved=='ya'){
			    $('#approved').prop('checked',true);
			}
			else{
			    $('#approved').prop('checked',false);
			}
		    });
		    
		    
		    
		});
	    }
	  } );
	  
	  oTable.fnSort( [ [2,'DESC'], [10,'DESC']] ); //sort tanggal
	  $('#pengaduan').each(function(){
	      var datatable = $(this);
	      // SEARCH - Add the placeholder for Search and Turn this into in-line form control
	      var search_input = datatable.closest('.dataTables_wrapper').find('div[id$=_filter] input');
	      search_input.attr('placeholder', 'Search');
	      search_input.addClass('form-control input-sm');
	      // LENGTH - Inline-Form control
	      var length_sel = datatable.closest('.dataTables_wrapper').find('div[id$=_length] select');
	      length_sel.addClass('form-control input-sm');
	  });
	  
			      
	});
      </script>
      <!-- end datatables-->
      <?php endif;?>
      <!-- end pengaduan -->
      
      <!-- user handling -->
      <?php if($this->uri->segment(2)=='user_handling' && ($this->uri->segment(3)=='add' || $this->uri->segment(3)=='edit')):?>
      <script>
	$(document).ready(function(){
	    $("#provinsi").change(function() {
		$("#kabkota").empty();
		
		$.post('<?=site_url('services/get_kabkota')?>', { provinsi_id : $("#provinsi option:selected").val() }, function(data) {
			$.each(data, function(i, item){
			    $("#kabkota").append(
				'<option value="' + item.id + '">'+item.name + '</option>'
			    );
			})
		    },
		    'json'
		);
	    });
	    
	    $("#kabkota").change(function() {
		$.post('<?=site_url('services/get_kecamatan')?>', { kabkota_id : $("#kabkota option:selected").val() }, function(data) {
			$.each(data, function(i, item){
			    $("#kecamatan").append(
				'<option value="' + item.id + '">'+item.name + '</option>'
			    );
			})
		    },
		    'json'
		);
	    });
	    function show_level(){
		
		if($('#level').val() == 2){
		    $('#provinsi_form').show();
		    $('#kabkota_form').hide();
		}else if($('#level').val() == 3){
		    $('#provinsi_form').show();
		    $('#kabkota_form').show();
		}else{
		    $('#provinsi_form').hide();
		    $('#kabkota_form').hide();
		}
	    }
	    show_level();
	    $('#level').change(function(){
		show_level();
		//$('#kabkota').show();
	    });
	    $("#wizardForm").formwizard({
	      formPluginEnabled: true,
	      validationEnabled: true,
	      focusFirstInput: true,
	      validationOptions: {
		  rules: {
		      nama: {
			  required: true,
			  minlength: 3
		      },
		      email: {
			  required: true,
			  email: true
		      },
		      username: {
			  required: true,
			  minlength: 6
		      },
		      <?php if($this->uri->segment(3)=='add'):?>
		      password: {
			  required: true,
			  minlength: 6
		      }
		      <?php endif;?>
		  },
		  errorClass: 'help-block',
		  errorElement: 'span',
		  highlight: function(element, errorClass, validClass) {
		      $(element).parents('.form-group').removeClass('has-success').addClass('has-error');
		  },
		  unhighlight: function(element, errorClass, validClass) {
		      $(element).parents('.form-group').removeClass('has-error').addClass('has-success');
		  }
	      }
	  });
	});
      </script>
      <?php endif;?>
      <?php if($this->uri->segment(2)=='user_handling'):?>
      <!-- datatables-->
      <script type="text/javascript" language="javascript" src="<?=$this->config->item('admin_plugin')?>/datatables/media/js/jquery.dataTables.js"></script>
      <script src="<?=$this->config->item('handling_plugin')?>/datatables-bootstrap/js/datatables.js"></script>
      <script type="text/javascript" charset="utf-8">
	var asInitVals = new Array();
	var i=0;
	var update=true;
	$(document).ready(function() {		      
	  $.fn.dataTableExt.oApi.fnPagingInfo = function ( oSettings )
	  {
	    return {
	      "iStart":         oSettings._iDisplayStart,
	      "iEnd":           oSettings.fnDisplayEnd(),
	      "iLength":        oSettings._iDisplayLength,
	      "iTotal":         oSettings.fnRecordsTotal(),
	      "iFilteredTotal": oSettings.fnRecordsDisplay(),
	      "iPage":          Math.ceil( oSettings._iDisplayStart / oSettings._iDisplayLength ),
	      "iTotalPages":    Math.ceil( oSettings.fnRecordsDisplay() / oSettings._iDisplayLength )
	    };
	  }

	  var oTable = $('#user_handling').dataTable( {
	    "bStateSave": false,
	    "bProcessing": true,
	    "bServerSide": true,
	    "iDisplayLength": 25,
	    "sPaginationType": "bs_full",
	    "sAjaxSource": "<?=(isset($load_url) ? $load_url : '')?>",
	    "sServerMethod": "POST",
	    "aoColumnDefs": [ 
	      { "bSortable": false, "aTargets": [ 0 ] }, //no
	      { "bSortable": false, "aTargets": [ 5 ] }, //action
	      {
		"fnRender": function ( oObj ) {
		  if(update){
		    i=oTable.fnPagingInfo().iStart;
		    update=false;
		  }
		  if(i+1 == oTable.fnPagingInfo().iEnd){
		    update=true;
		  }
		  
		  return ++i;
			
		},
		"bUseRendered": false,
		"aTargets": [ 0 ] //no
	      }
	    ],
	    "fnDrawCallback": function () {
		$('.btn_delete').click(function(){
		    $('#link_delete').attr('href', $(this).attr('data-href'));
		});
	    }
	  } );
				
			      
	  $('#pengaduan').each(function(){
	      var datatable = $(this);
	      // SEARCH - Add the placeholder for Search and Turn this into in-line form control
	      var search_input = datatable.closest('.dataTables_wrapper').find('div[id$=_filter] input');
	      search_input.attr('placeholder', 'Search');
	      search_input.addClass('form-control input-sm');
	      // LENGTH - Inline-Form control
	      var length_sel = datatable.closest('.dataTables_wrapper').find('div[id$=_length] select');
	      length_sel.addClass('form-control input-sm');
	  });
	  
			      
	});
      </script>
      <!-- end datatables-->
      <?php endif;?>
      <!-- end user handling -->
      
      <!-------------------------------------------------------->
      <!-- user admin -->
      <?php if($this->uri->segment(2)=='user_admin' && ($this->uri->segment(3)=='add' || $this->uri->segment(3)=='edit')):?>
      <script>
	$(document).ready(function(){
	    $("#provinsi").change(function() {
		$("#kabkota").empty();
		
		$.post('<?=site_url('services/get_kabkota')?>', { provinsi_id : $("#provinsi option:selected").val() }, function(data) {
			$.each(data, function(i, item){
			    $("#kabkota").append(
				'<option value="' + item.id + '">'+item.name + '</option>'
			    );
			})
		    },
		    'json'
		);
	    });
	    
	    $("#kabkota").change(function() {
		$.post('<?=site_url('services/get_kecamatan')?>', { kabkota_id : $("#kabkota option:selected").val() }, function(data) {
			$.each(data, function(i, item){
			    $("#kecamatan").append(
				'<option value="' + item.id + '">'+item.name + '</option>'
			    );
			})
		    },
		    'json'
		);
	    });
	    function show_level(){
		
		if($('#level').val() == 2){
		    $('#provinsi_form').show();
		    $('#kabkota_form').hide();
		}else if($('#level').val() == 3){
		    $('#provinsi_form').show();
		    $('#kabkota_form').show();
		}else{
		    $('#provinsi_form').hide();
		    $('#kabkota_form').hide();
		}
	    }
	    show_level();
	    $('#level').change(function(){
		show_level();
		//$('#kabkota').show();
	    });
	    $("#wizardForm").formwizard({
	      formPluginEnabled: true,
	      validationEnabled: true,
	      focusFirstInput: true,
	      validationOptions: {
		  rules: {
		      nama: {
			  required: true,
			  minlength: 3
		      },
		      email: {
			  required: true,
			  email: true
		      },
		      username: {
			  required: true,
			  minlength: 6
		      },
		      <?php if($this->uri->segment(3)=='add'):?>
		      password: {
			  required: true,
			  minlength: 6
		      }
		      <?php endif;?>
		  },
		  errorClass: 'help-block',
		  errorElement: 'span',
		  highlight: function(element, errorClass, validClass) {
		      $(element).parents('.form-group').removeClass('has-success').addClass('has-error');
		  },
		  unhighlight: function(element, errorClass, validClass) {
		      $(element).parents('.form-group').removeClass('has-error').addClass('has-success');
		  }
	      }
	  });
	});
      </script>
      <?php endif;?>
      <?php if($this->uri->segment(2)=='user_admin'):?>
      <!-- datatables-->
      <script type="text/javascript" language="javascript" src="<?=$this->config->item('admin_plugin')?>/datatables/media/js/jquery.dataTables.js"></script>
      <script src="<?=$this->config->item('handling_plugin')?>/datatables-bootstrap/js/datatables.js"></script>
      <script type="text/javascript" charset="utf-8">
	var asInitVals = new Array();
	var i=0;
	var update=true;
	$(document).ready(function() {		      
	  $.fn.dataTableExt.oApi.fnPagingInfo = function ( oSettings )
	  {
	    return {
	      "iStart":         oSettings._iDisplayStart,
	      "iEnd":           oSettings.fnDisplayEnd(),
	      "iLength":        oSettings._iDisplayLength,
	      "iTotal":         oSettings.fnRecordsTotal(),
	      "iFilteredTotal": oSettings.fnRecordsDisplay(),
	      "iPage":          Math.ceil( oSettings._iDisplayStart / oSettings._iDisplayLength ),
	      "iTotalPages":    Math.ceil( oSettings.fnRecordsDisplay() / oSettings._iDisplayLength )
	    };
	  }

	  var oTable = $('#user_handling').dataTable( {
	    "bStateSave": false,
	    "bProcessing": true,
	    "bServerSide": true,
	    "iDisplayLength": 25,
	    "sPaginationType": "bs_full",
	    "sAjaxSource": "<?=(isset($load_url) ? $load_url : '')?>",
	    "sServerMethod": "POST",
	    "aoColumnDefs": [ 
	      { "bSortable": false, "aTargets": [ 0 ] }, //no
	      { "bSortable": false, "aTargets": [ 4 ] }, //action
	      {
		"fnRender": function ( oObj ) {
		  if(update){
		    i=oTable.fnPagingInfo().iStart;
		    update=false;
		  }
		  if(i+1 == oTable.fnPagingInfo().iEnd){
		    update=true;
		  }
		  
		  return ++i;
			
		},
		"bUseRendered": false,
		"aTargets": [ 0 ] //no
	      }
	    ],
	    "fnDrawCallback": function () {
		$('.btn_delete').click(function(){
		    $('#link_delete').attr('href', $(this).attr('data-href'));
		});
	    }
	  } );
				
			      
	  $('#pengaduan').each(function(){
	      var datatable = $(this);
	      // SEARCH - Add the placeholder for Search and Turn this into in-line form control
	      var search_input = datatable.closest('.dataTables_wrapper').find('div[id$=_filter] input');
	      search_input.attr('placeholder', 'Search');
	      search_input.addClass('form-control input-sm');
	      // LENGTH - Inline-Form control
	      var length_sel = datatable.closest('.dataTables_wrapper').find('div[id$=_length] select');
	      length_sel.addClass('form-control input-sm');
	  });
	  
			      
	});
      </script>
      <!-- end datatables-->
      <?php endif;?>
      <!-- end user admin -->