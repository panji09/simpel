<?php $this->session->set_userdata('redirect','admin_handling/halaman/'.$this->uri->segment(3));?>
            <div class="row">
                <div class="col-lg-12">
                    <div class="box">
                        <header class="dark">
                            <div class="icons">
                                <i class="fa fa-ok"></i>
                            </div>
                            <h5><?=$title?></h5>
                            <div class="toolbar">
                              <ul class="nav">
                                <li>
                                  <div class="btn-group">
                                    <a class="accordion-toggle btn btn-xs minimize-box" data-toggle="collapse" href="#collapse2">
                                      <i class="fa fa-chevron-up"></i>
                                    </a>
                                  </div>
                                </li>
                              </ul>
                            </div>
                        </header>
                        <div id="collapse2" class="body collapse in">
		      <form id="wizardForm" method="post" action="<?=site_url('admin_handling/user_admin/post_add/'.$id)?>" role='form' class="wizardForm">
                      <fieldset class="step" id="first">
			<h4 class="text-primary pull-right">User Settings</h4>
                        <div class="clearfix"></div>
                        <div class="form-group">
			  <label for="">Nama</label>
			  <input type="text" class="form-control" id="" placeholder="Nama" name='nama' value='<?=$user_handling->nama?>'>
			</div>
			<div class="form-group">
			  <label for="">NIP</label>
			  <input type="text" class="form-control" id="" placeholder="Nip" name='nip' value='<?=$user_handling->nip?>'>
			</div>
			<div class="form-group">
			  <label for="">jabatan Dinas</label>
			  <input type="text" class="form-control" id="" placeholder="Jabatan Dinas" name='jabatan_dinas' value='<?=$user_handling->jabatan_dinas?>'>
			</div>
			
			<div class="form-group">
			  <label for="">Email</label>
			  <input type="text" class="form-control" id="" placeholder="Email" name='email' value='<?=$user_handling->email?>'>
			</div>
			<div class="form-group">
			  <label for="">Alamat Kantor</label>
			  <textarea class="form-control" id="" placeholder="Alamat Kantor" name='alamat_kantor'><?=$user_handling->alamat_kantor?></textarea>
			</div>
			<div class="form-group">
			  <label for="">Telp Kantor</label>
			  <input type="text" class="form-control" id="" placeholder="Telp Kantor" name='telp_kantor' value='<?=$user_handling->telp_kantor?>'>
			</div>
			<div class="form-group">
			  <label for="">Fax Kantor</label>
			  <input type="text" class="form-control" id="" placeholder="Fax Kantor" name='fax_kantor' value='<?=$user_handling->fax_kantor?>'>
			</div>
			<div class="form-group">
			  <label for="">Alamat Rumah</label>
			  <textarea class="form-control" id="" placeholder="Alamat Rumah" name='alamat_rumah'><?=$user_handling->alamat_rumah?></textarea>
			</div>
			<div class="form-group">
			  <label for="">Handphone</label>
			  <input type="text" class="form-control" id="" placeholder="Handphone" name='handphone' value='<?=$user_handling->handphone?>'>
			</div>
                      </fieldset>
                      <fieldset class="step" id="second">
			<h4 class="text-primary pull-right">User Login</h4>
                        <div class="clearfix"></div>
                        <div class="form-group">
			  <label for="">Username</label>
			  <input type="text" name='username' class="form-control" id="" placeholder="username" value='<?=$user_handling->username?>'>
			</div>
			<div class="form-group">
			  <label for="">Password</label>
			  <input type="password" name='password' class="form-control" id="" placeholder="Password" value=''>
			</div>
			<div class="form-group">
			  <label for="">Aktif</label>
			  <div class="checkbox">
			    <label>
			      <input type="checkbox" name='activated' value='1' <?=($user_handling->activated ? 'checked="checked"' : '')?>> Aktif
			    </label>
			  </div>
			</div>
			
                      </fieldset>
                      <fieldset class="step" id="last">
                        <h4 class="text-primary pull-right">Role</h4>
                        <div class="clearfix"></div>
			<div class="form-group">
			  <label for="">Role Level</label>
			  <select id="level" class="form-control" name='level'>
			    <?php
			      $role_level = new stdClass();
			      $role_level->level_id=null;
			      $role_level->user_id=null;
			      if($id){
				  $role_level = $this->user_admin_db->get_role_level($id)->row();
				   
			      }
			      $level=$this->select_db->level_admin()->result();
			      foreach($level as $row):
			    ?>
			    <option value='<?=$row->id?>' <?=($row->id == $role_level->level_id ? 'selected="selected"' : '')?>><?=$row->name?></option>
			    <?php endforeach;?>
			  </select>
			</div>
			
                      </fieldset>
                      <div class="form-actions">
                        <input class="navigation_button btn" id="back" value="Back" type="reset" />
                        <input class="navigation_button btn btn-primary" id="next" value="Next" type="submit" />
                      </div>
                    </form>
                        </div>
                    </div>
                </div>
            </div>