<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Pengaduan_db extends CI_Model{
    function __construct(){
        // Call the Model constructor
        parent::__construct();
    }
    function exist($id){
        return $this->db->get_where('pengaduan',  array('id' => $id));
    }
    function save(&$id, $data_person, $data_pengaduan, $data_param_pengaduan, $data_isu){
	$result=false;
        $CI =& get_instance();
	$CI->load->model('admin_handling/person_db');
	
	$this->db->trans_start();
	
	$exist=$this->exist($id);
	if($exist->num_rows() == 1){
	    //update
	    $pengaduan = $exist->row();
	    
	    //save person
	    if($CI->person_db->save($pengaduan->person_id, $data_person)){
		if($this->save_pengaduan($id, $data_pengaduan)){	
		    $result = $this->save_param($pengaduan_id = $pengaduan->id, $data_param_pengaduan);
		    
		    $this->save_isu($pengaduan_id = $pengaduan->id, $data_isu);
		}
	    }
	}else{
	    //insert
	    //save person
	    $person_id = null;
	    if($CI->person_db->save($person_id, $data_person)){
		$data_pengaduan['person_id'] = $person_id;
		if($this->save_pengaduan($id, $data_pengaduan))
		    $result = $this->save_param($id, $data_param_pengaduan);
		    $this->save_isu($id, $data_isu);
	    }
	}
	
	$this->db->trans_complete();
	
	//echo $this->db->last_query();
	return $result;
    }
    
    function save_isu($pengaduan_id, $data_isu){
	$this->db->delete('pengaduan_isu', array('pengaduan_id' => $pengaduan_id));
	$result = true;
	if($data_isu)
	foreach($data_isu as $value){
	    $result = $this->db->insert('pengaduan_isu', array('pengaduan_id' => $pengaduan_id, 'isu_id' => $value));
	}
	//echo $this->db->last_query();
	return $result;
    }
    
    function save_pengaduan(&$id, $data_pengaduan){
	$result=false;
        $this->db->trans_start();
	
	$exist=$this->exist($id);
	if($exist->num_rows() == 1){
	    //update
	    $this->db->where('id', $id);
	    $result = $this->db->update('pengaduan', $data_pengaduan);
	}else{
	    //insert
	    $result = $this->db->insert('pengaduan', $data_pengaduan);
	    $id = $this->db->insert_id();
	}
	
	$this->db->trans_complete();
	return $result;
    }
    
    function get_pengaduan($id){
	return $this->get_pengaduan_all(array('id' => $id));
    }
    function get_pengaduan_all($filter=array()){
	$this->db->from('pengaduan');
	$this->db->where('deleted',0);
	
	if(isset($filter['id']))
	    $this->db->where('id', $filter['id']);
	
	return $this->db->get();
    }
    function save_param($pengaduan_id, $key_value){
	$result = false;
	
	//print_r($key_param);
	$this->db->delete('pengaduan_param', array('pengaduan_id' => $pengaduan_id));
	foreach($key_value as $key => $value){
	    $result = $this->db->insert('pengaduan_param', array('pengaduan_id' => $pengaduan_id, 'key' => $key, 'value' => $value));
	}
	//echo $this->db->last_query();
	return $result;
    }
    
    function approved($pengaduan_id, $data_pengaduan, $data_isu=array()){
	$result = false;
	
	$exist = $this->exist($pengaduan_id);
	if($exist->num_rows() == 1){
	    $pengaduan = $exist->row();
	    
	    if(!$pengaduan->approved)
		$data_pengaduan['kode_pengaduan'] = $pengaduan->id.$pengaduan->kecamatan_id.$pengaduan->program_id.$pengaduan->kategori_id.$pengaduan->jenjang_id;
	    
	    $result = $this->save_pengaduan($pengaduan_id, $data_pengaduan);
	    
	    if($pengaduan->media_id == 2) //sms
		$this->save_isu($pengaduan_id = $pengaduan->id, $data_isu);
	    
	}
	
	return $result;
	
    }
    
    function get($id){
	return $this->get_all(array('id' => $id));
    }
    
    function get_token_confirm($token_confirm){
	return $this->get_all($filter=array('token_confirm' => $token_confirm), $limit=null, $offset=0, $deleted = 1);
    }
    
    function get_all($filter=array(), $limit=null, $offset=0, $deleted = 0){
	$this->db->select('
	  a.id, 
	  b.nama,
	  a.tanggal,
	  a.nama_lokasi,
	  b.email, 
	  b.alamat_rumah, 
	  b.handphone, 
	  e.name as provinsi,
	  c.name as kecamatan, 
	  d.name as kabkota, 
	  f.name as jenjang, 
	  g.name as sumber, 
	  h.name as kategori, 
	  i.name as program,
	  j.name as media,
	  k.name as status,
	  l.name as level,
	  m.name as status_sekolah,
	  n.name as lokasi,
	  o.name as operator,
	  a.deskripsi,
	  a.status_id,
	  a.provinsi_id,
	  a.kabkota_id,
	  a.kecamatan_id,
	  a.jenjang_id,
	  a.kategori_id,
	  a.sumber_id,
	  a.sumber_lain,
	  a.program_id,
	  a.kode_pengaduan,
	  a.status_id,
	  a.level_id,
	  a.status_sekolah_id,
	  a.lokasi_id,
	  a.person_id,
	  a.published,
	  a.approved
	  ');
	$this->db->from('pengaduan as a');
	$this->db->join('person as b','a.person_id = b.id','left');
	$this->db->join('kecamatan as c','a.kecamatan_id = c.id','left');
	$this->db->join('kabkota as d','a.kabkota_id = d.id','left');
	$this->db->join('provinsi as e','a.provinsi_id = e.id','left');
	$this->db->join('jenjang as f','a.jenjang_id = f.id','left');
	$this->db->join('sumber as g','a.sumber_id = g.id','left');
	$this->db->join('kategori as h','a.kategori_id = h.id','left');
	$this->db->join('program as i','a.program_id = i.id','left');
	$this->db->join('media as j','a.media_id = j.id','left');
	$this->db->join('status as k','a.status_id = k.id','left');
	$this->db->join('level as l','a.level_id = l.id','left');
	$this->db->join('status_sekolah as m','a.status_sekolah_id = m.id','left');
	$this->db->join('lokasi as n','a.lokasi_id = n.id','left');
	$this->db->join('operator as o','b.operator_id = o.id','left');
	
	if($filter){
	    if(isset($filter['published']))
		$this->db->where('a.published', $filter['published']);
	    
	    if(isset($filter['approved']))
		$this->db->where('a.approved', $filter['approved']);
	    
	    if(isset($filter['status'])){
		$this->db->where('a.approved', 1);
		$this->db->where('a.status_id', $filter['status']);
	    }
	    if(isset($filter['id']))
		$this->db->where('a.id', $filter['id']);
	    
	    if(isset($filter['provinsi']))
		$this->db->where('a.provinsi_id', $filter['provinsi']);
	    
	    if(isset($filter['kabkota']))
		$this->db->where('a.kabkota_id', $filter['kabkota']);
	    
	    if(isset($filter['kecamatan']))
		$this->db->where('a.kecamatan_id', $filter['kecamatan']);
		
	    if(isset($filter['tahun']) && $filter['tahun']!='all'){
		$this->db->where('YEAR(a.tanggal)', intval($filter['tahun']));
	    }
	    
	    if(isset($filter['triwulan']) && $filter['triwulan']!='all'){
		switch(intval($filter['triwulan'])){
		    case 1 :
			$min=1; $max=3;
		    break;
		    case 2 :
			$min=4; $max=6;
		    break;
		    case 3 :
			$min=7; $max=9;
		    break;
		    case 4 :
			$min=10; $max=12;
		    break;
		}
		
		$this->db->where('MONTH(a.tanggal) >=',$min);
		$this->db->where('MONTH(a.tanggal) <=',$max);
	    }
	    
	    if(isset($filter['program']))
		$this->db->where('a.program_id IN ('.implode(",", $filter['program']).')');
	    
	    if(isset($filter['jenjang']))
		$this->db->where('(a.jenjang_id IN ('.implode(",", $filter['jenjang']).') OR a.jenjang_id is NULL)');
	    
	    if(isset($filter['token_confirm']))
		$this->db->where('a.token_confirm', $filter['token_confirm']);
	}
	
	if($deleted)
	    $this->db->where('a.deleted',1);
	else
	    $this->db->where('a.deleted',0);
	    
	if($limit)
	    $this->db->limit($limit, $offset);
	
	return $this->db->get();
    }
    
    function get_param($pengaduan_id){
	$this->db->where('pengaduan_id', $pengaduan_id);
	return $this->db->get('pengaduan_param');
    }
    
    function get_isu($pengaduan_id){
	$this->db->where('pengaduan_id', $pengaduan_id);
	return $this->db->get('pengaduan_isu');
    }
    
    function get_pelaku($pengaduan_id){
	//$this->db->select('a.');
	$this->db->from('pelaku a, pengaduan_pelaku b');
	$this->db->where('b.pelaku_id = a.id');
	$this->db->where('b.pengaduan_id', $pengaduan_id);
	
	return $this->db->get('');
    }
    
    function save_pelaku($pengaduan_id, $pelaku=array()){
	$result = false;
	if($pelaku){
	    $this->db->delete('pengaduan_pelaku', array('pengaduan_id' => $pengaduan_id));
	    foreach($pelaku as $row){
		$result = $this->db->insert('pengaduan_pelaku', array('pengaduan_id' => $pengaduan_id, 'pelaku_id' => $row['pelaku_id'], 'pelaku_lain' => $row['pelaku_lain']));
	    }
	}
	
	return $result;
    }
    
    function delete($id){
	$result=false;
	if($this->exist($id)){
	    $result = $this->save_pengaduan($id,array('deleted' => 1));
	}
	return $result;
    }
}
?>