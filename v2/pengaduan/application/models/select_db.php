<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Select_db extends CI_Model{
    function __construct(){
        // Call the Model constructor
        parent::__construct();
    }
    
    function faq1(){
        $this->db->where('id = 1');
        return $this->db->get('pol_faq');
    }
    function faq($param=null, $data=null){
		if($param=='by_kategori')
			$this->db->where('id_faq_kategori',$data['id_kategori']);
		
		$this->db->order_by('id_faq_kategori','ASC');
		$this->db->order_by('id','ASC');
		return $this->db->get('pol_faq');
	}
    
    
    function sumber_info($role='home'){
	if($role=='home'){
	    $id=array(6,7);
	    $this->db->where_not_in('id',$id);
	}
	$this->db->where('deleted',0);
	$this->db->order_by('sort','ASC');
	return $this->db->get('sumber');
    }
    
    
    function user_handling($data=null){
	if(isset($data['id']))
	    $this->db->where('id',$data['id']);
	
	return $this->db->get('t_user');
    }
    
    
		
    
    function faq_kategori($param=null, $data=null){
	$this->db->order_by('id','ASC');
		    return $this->db->get('pol_faq_kategori');
		    
    }
    
    function kategori($data=null){
	if(isset($data['id']))
	    $this->db->where('id',$data['id']);
	
	$this->db->where('deleted',0);
	$this->db->order_by('sort','asc');
	return $this->db->get('kategori');
    }
    
    function isu($data=null){
	if(isset($data['id']))
	    $this->db->where('id',$data['id']);
	
	$this->db->where('deleted',0);
	$this->db->order_by('sort','asc');
	return $this->db->get('isu');
    }
    
    function program(){
	$this->db->where('deleted',0);
	return $this->db->get('program');
    }
    
    function level($data=null){
	if(isset($data['id']))
	    $this->db->where('id',$data['id']);
	
	$this->db->where('deleted',0);
	$this->db->order_by('sort','asc');
	return $this->db->get('level');
    }
    
    function level_admin($data=null){
	if(isset($data['id']))
	    $this->db->where('id',$data['id']);
	
	$this->db->where('deleted',0);
	$this->db->order_by('sort','asc');
	return $this->db->get('level_admin');
    }
    
    function status_sekolah(){
	return $this->db->get('status_sekolah');
    }
    
    function lokasi(){
	return $this->db->get('lokasi');
    }
    function role_jenjang($data=null){
	if(isset($data['user_id']))
	    $this->db->where('user_id',$data['user_id']);
	
	return $this->db->get('pol_role_jenjang');
    }
    
    function jenjang(){
	$this->db->where('deleted',0);
	return $this->db->get('jenjang');
    }
    
    function kategori_penyelesaian($data=null){
	if(isset($data['id']))
	    $this->db->where('id',$data['id']);
	    
	$this->db->order_by('id');
	return $this->db->get('kategori_penyelesaian');
    }
    function penyelesaian($data=null){
	if(isset($data['id']))
	    $this->db->where('id',$data['id']);
	
	if(isset($data['kategori_penyelesaian_id'])){
	    $this->db->where('kategori_penyelesaian_id', $data['kategori_penyelesaian_id']);
	}
	return $this->db->get('penyelesaian');
    }
    function media($data=null){
	if(isset($data['id']))
	    $this->db->where('id',$data['id']);
	
	$this->db->where('deleted',0);
	$this->db->order_by('sort','asc');
	return $this->db->get('media');
    }
    
    function status($data=array()){
	if(isset($data['id']))
	    $this->db->where('id', $data['id']);
	
	$this->db->where('deleted',0);
	$this->db->order_by('sort','asc');
	return $this->db->get('status');
    }
    
    function pelaku($data=array()){
	if(isset($data['id']))
	    $this->db->where('id', $data['id']);
	
	$this->db->where('deleted',0);
	$this->db->order_by('sort','asc');
	return $this->db->get('pelaku');
    }
    
    function report_kategori(){
	$this->db->where('deleted',0);
	$this->db->order_by('sort');
	return $this->db->get('report_kategori');
    }
}
?>