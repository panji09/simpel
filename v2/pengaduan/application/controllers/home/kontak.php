<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once(APPPATH.'third_party/recaptcha/recaptchalib.php');

class Kontak extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->helper(array('url','date', 'init'));
		$this->load->model(array('admin_handling/region_db', 'admin_handling/content_dinamis_db','admin_handling/content_statis_db', 'admin_handling/content_files_db', 'admin_handling/pengaduan_db', 'select_db', 'admin_handling/respon_db'));
		$this->load->library(array('initlib','session','email'));
		$this->load->database();
		
		$this->config->load('recaptcha');
		session_start();
	}
	
	public function index(){
		$data['title']='Pelayanan dan Penanganan Pengaduan Masyarakat BOS : Kontak';
		
		//$data['report']=$this->Select_db->report_kategori('by_status');
		$data['content'] = 'kontak';
		$this->load->view('home/main',$data);
		
	}
	
	function post_kontak(){
	    //print_r($_POST);
	    $privatekey = $this->config->item('private_key');
	    $resp = recaptcha_check_answer(
		$privatekey,
		$_SERVER["REMOTE_ADDR"],
		$_POST["recaptcha_challenge_field"],
		$_POST["recaptcha_response_field"]
	    );
	    
	    
	    if(!$resp->is_valid) {
		$this->session->set_flashdata('success', array('status' => false, 'msg' => 'kode verifikasi error'));
	    }else{
		//print_r($_POST);
		$email = $this->input->post('email');
		$nama = $this->input->post('nama');
		$pesan = $this->input->post('pesan');
		
		$email_to = 'panji09@gmail.com';
		send_email($from=array('name'=>'P3M Kurikukum', 'email' => 'ch.emailer.system@gmail.com'), $to=array($email_to), $cc=array(), $bcc=array(), $subject='P3M Kurikukum | Saran/Masukan dari '.$nama.' ('.$email.')', $pesan, $reply_to=array('name'=>$nama, 'email' => $email));
		$this->session->set_flashdata('success', array('status' => true, 'msg' => 'pesan terkirim.'));
	    }
	    
	    redirect('kontak');
	}
	
}

/* End of file home.php */
/* Location: ./application/controllers/home.php */