<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once(APPPATH.'third_party/recaptcha/recaptchalib.php');

class Pengaduan extends CI_Controller {
	function __construct(){
	    parent::__construct();
	    $this->load->helper(array('url','date','init'));
	    $this->load->model(array('admin_handling/region_db', 'admin_handling/content_dinamis_db','admin_handling/content_statis_db', 'admin_handling/content_files_db', 'admin_handling/pengaduan_db', 'select_db', 'admin_handling/respon_db','admin_handling/region_db','admin_handling/user_admin_db'));
	    $this->load->library(array('initlib','session','email', 'form_validation'));
	    $this->load->database();
	    
	    $this->config->load('recaptcha');
	    
	    $this->program_id = 4;
	}
	
	public function index(){
	    
	    $data['title']='Pelayanan dan Penanganan Pengaduan Masyarakat BOS : Pengaduan';
	    
	    $data['content'] = 'pengaduan';
	    $this->load->view('home/main',$data);
	}
	
	function post_pengaduan(){
	    
	    $privatekey = $this->config->item('private_key');
	    $resp = recaptcha_check_answer(
		$privatekey,
		$_SERVER["REMOTE_ADDR"],
		$_POST["recaptcha_challenge_field"],
		$_POST["recaptcha_response_field"]
	    );

	    if (!$resp->is_valid) {
		$this->session->set_flashdata('success', array('status' => false, 'msg' => 'kode verifikasi error'));
	    } else {
		
		$this->form_validation->set_rules('level', 'Ditujukan Kepada', 'required');
		$this->form_validation->set_rules('provinsi', 'Provinsi	', 'required');
		$this->form_validation->set_rules('kabkota', 'Kabkota', 'required');
		$this->form_validation->set_rules('kecamatan', 'Kecamatan', 'required');
		
		$this->form_validation->set_rules('lokasi', 'Lokasi', 'required');
		
		if($this->input->post('lokasi')==1){//lokasi madrasah
		    $this->form_validation->set_rules('jenjang', 'Jenjang', 'required');
		    $this->form_validation->set_rules('nama_lokasi', 'Nama Lokasi', 'required');
		}
		
		$this->form_validation->set_rules('kategori', 'Kategori Pengaduan', 'required');
		$this->form_validation->set_rules('sumber_info', 'Identitas Pelapor', 'required');
		
		if($this->input->post('sumber_info')==9){//lain2
		    $this->form_validation->set_rules('sumber_lain', 'Sumber Lain', 'required');
		}
		
		$this->form_validation->set_rules('nama', 'Nama', 'required');
		$this->form_validation->set_rules('email', 'Email', 'required');
		$this->form_validation->set_rules('telp', 'Telp', 'required');
		$this->form_validation->set_rules('alamat', 'Alamat', 'required');
		$this->form_validation->set_rules('deskripsi', 'Deskripsi', 'required');
		
		if ($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('success', array('status' => false, 'msg' => 'validasi error'));
			
		}else{
			//sukses
			$data_person=array(
			    'nama' => $this->input->post('nama'),
			    'email' => $this->input->post('email'),
			    'handphone' => $this->input->post('telp'),
			    'alamat_rumah' => $this->input->post('alamat')
			);
			
			$data_pengaduan=array(
			    'program_id' => $this->program_id,
			    'level_id' => $this->input->post('level'),
			    'kecamatan_id' => $this->input->post('kecamatan'),
			    'kabkota_id' => $this->input->post('kabkota'),
			    'provinsi_id' => $this->input->post('provinsi'),
			    'lokasi_id' => $this->input->post('lokasi'),
			    'jenjang_id' => ($this->input->post('lokasi') == 1 ? $this->input->post('jenjang') : null),
			    'status_sekolah_id' => ($this->input->post('lokasi') == 1 ? $this->input->post('status_sekolah') : null),
			    'nama_lokasi' => htmlentities($this->input->post('nama_lokasi')),
			    'tanggal' => date('Y-m-d H:i:s'),
			    'sumber_id' => $this->input->post('sumber_info'),
			    'sumber_lain' => ($this->input->post('sumber_info')==9999 ? htmlentities($this->input->post('sumber_lain')) : null),
			    'kategori_id' => $this->input->post('kategori'),
			    'deskripsi' => htmlentities($this->input->post('deskripsi')),
			    'last_update' => date('Y-m-d H:i:s'),
			    'media_id' => 1,
			    'token_confirm' => md5_salt(time()),
			    'deleted' => 1,
			    'reason_deleted' => 'waiting for email confirmation'
			);
			
			$param = $this->input->post('param');
			
			$data_param_pengaduan = array(
			    'tampil_nama' => (isset($param['tampil_nama']) ? '1' : '0'),
			    'tampil_telp' => (isset($param['tampil_telp']) ? '1' : '0'),
			    'tampil_alamat' => (isset($param['tampil_alamat']) ? '1' : '0')
			);
			
			$data_isu = $this->input->post('isu');
			//print_r($data_isu);
			if($this->pengaduan_db->save($id=null,$data_person, $data_pengaduan, $data_param_pengaduan, $data_isu)){
			   
			   $data_email = array(
			      'nama' => $data_person['nama'],
			      'link' => site_url('pengaduan/confirm_email/'.$data_pengaduan['token_confirm'])
			   );
			   $message = $this->load->view('email/konfirmasi_pengaduan_email',$data_email,true);
			   send_email($from=array('name'=>'P3M Kurikukum', 'email' => 'ch.emailer.system@gmail.com'), $to=array($data_person['email']), $cc=array(), $bcc=array(), $subject='P3M Kurikukum | Konfirmasi Email', $message, $reply_to=array());
			   $this->session->set_flashdata('success', array('status' => true, 'msg' => 'Silahkan cek email anda untuk konfirmasi'));
			    
			}else{
			    //echo $this->db->last_query();
			    $this->session->set_flashdata('success', array('status' => false, 'msg' => 'simpan error'));
			}
			
			
		}
		
	    }
	    
	    redirect('pengaduan');
	}
	
	function confirm_email($token=null){
	    if($token){
		$query = $this->pengaduan_db->get_token_confirm($token);
		if($query->num_rows() == 1){
		    $row = $query->row();
		    
		    $data_email = array(
			'nama' => $row->nama,
			'content' => array(
			    'Ditujukan Kepada' => 'Tim '.ucfirst($row->level),
			    'Daerah' => $row->provinsi.', '.$row->kabkota.', '.$row->kecamatan,
			    'Lokasi' => ($row->lokasi_id == 1 ? $row->jenjang.' '.$row->status_sekolah.', '.$row->nama_lokasi : $row->nama_lokasi),
			    'Kategori' => $row->kategori,
			    'Deskripsi' => $row->deskripsi
			),
			'status' => 'Menunggu verifikasi'
		    );
		    //bcc to admin
		    $query = $this->user_admin_db->get_all();
		    if($query->num_rows()){
			$email_admin = array();
			foreach($query->result() as $row_admin){
			    $email_admin[] = $row_admin->email;
			}
			
			if($email_admin){
			    $email_admin = implode(',',$email_admin);
			    $email_admin = explode(',',$email_admin);
			}
		    }
		    $message = $this->load->view('email/status_pengaduan', $data_email,true);
		    send_email($from=array('name'=>'P3M Kurikukum', 'email' => 'ch.emailer.system@gmail.com'), $to=array($row->email), $cc=array(), $bcc=$email_admin, $subject='P3M Kurikukum | Status Pengaduan', $message);
		    
		    $this->pengaduan_db->save_pengaduan($row->id,array('token_confirm' => null, 'deleted' => 0, 'reason_deleted' => ''));
		    
		    $this->session->set_flashdata('success', array('status' => true, 'msg' => 'Terima kasih, telah berkontribusi untuk meningkatkan kualitas pendidikan yang lebih baik'));
		}else{
		    $this->session->set_flashdata('success', array('status' => false, 'msg' => 'Error'));
		}
	    }else{
		$this->session->set_flashdata('success', array('status' => false, 'msg' => 'Konfirmasi email gagal'));
	    }
	    
	    redirect('pengaduan'); 
	}
	
	function test_email(){
	    $data_email = array(
	      'nama' => 'panji',
	      'link' => site_url('pengaduan/confirm_email/1234')
	    );
	    $message = $this->load->view('email/konfirmasi_pengaduan_email',$data_email,true);
	    send_email($from=array('name'=>'P3M Kurikukum', 'email' => 'ch.emailer.system@gmail.com'), $to=array('panji09@gmail.com'), $cc=array(), $bcc=array(), $subject='P3M Kurikukum | Konfirmasi Email', $message);
	}
	
	function test($token){
	    
	    
	}
}

/* End of file home.php */
/* Location: ./application/controllers/home.php */