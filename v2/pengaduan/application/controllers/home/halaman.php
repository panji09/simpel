<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Halaman extends CI_Controller {
	function __construct(){
	    parent::__construct();
	    $this->load->helper(array('url','date'));
	    $this->load->model(array('admin_handling/region_db', 'admin_handling/content_dinamis_db','admin_handling/content_statis_db', 'admin_handling/content_files_db', 'admin_handling/pengaduan_db', 'Select_db', 'admin_handling/respon_db'));
	    $this->load->library(array('initlib','session','email'));
	    $this->load->database();
	    session_start();
	}
	
	function index(){
	    redirect('halaman/berita');
	}
	public function berita($id=null){
	    $data['title']='Pelayanan dan Penanganan Pengaduan Masyarakat BOS : Halaman/Berita';
	    
	    $data['berita']=$this->content_dinamis_db->get_all($content_kategori_id=1, $offset=0, $limit=5);
	    $data['pengumuman']=$this->content_dinamis_db->get_all($content_kategori_id=2, $offset=0, $limit=5);
	    $data['peraturan']=$this->content_dinamis_db->get_all($content_kategori_id=3, $offset=0, $limit=5);
	    $data['publikasi']=$this->content_dinamis_db->get_all($content_kategori_id=4, $offset=0, $limit=5);
	    
	    if($id){
		$data['halaman_main'] = $this->content_dinamis_db->get($id);
		$data['content'] = 'halaman_single';
	    }else{
		$data['halaman_main'] = $this->content_dinamis_db->get_all($content_kategori_id=1, $offset=0, $limit=5);
		$data['content'] = 'halaman';
	    }
	    $this->load->view('home/main',$data);
	      
	}
	
	function pengumuman($id=null){
	    $data['title']='Pelayanan dan Penanganan Pengaduan Masyarakat BOS : Halaman/Pengumuman';
	    
	    $data['berita']=$this->content_dinamis_db->get_all($content_kategori_id=1, $offset=0, $limit=5);
	    $data['pengumuman']=$this->content_dinamis_db->get_all($content_kategori_id=2, $offset=0, $limit=5);
	    $data['peraturan']=$this->content_dinamis_db->get_all($content_kategori_id=3, $offset=0, $limit=5);
	    $data['publikasi']=$this->content_dinamis_db->get_all($content_kategori_id=4, $offset=0, $limit=5);
	    
	    if($id){
		$data['halaman_main'] = $this->content_dinamis_db->get($id);
		$data['content'] = 'halaman_single';
	    }else{
		$data['halaman_main'] = $this->content_dinamis_db->get_all($content_kategori_id=2, $offset=0, $limit=5);
		$data['content'] = 'halaman';
	    }
	    $this->load->view('home/main',$data);
	}
	
	function peraturan($id=null){
	    $data['title']='Pelayanan dan Penanganan Pengaduan Masyarakat BOS : Halaman/Peraturan Perundangan';
	    
	    $data['berita']=$this->content_dinamis_db->get_all($content_kategori_id=1, $offset=0, $limit=5);
	    $data['pengumuman']=$this->content_dinamis_db->get_all($content_kategori_id=2, $offset=0, $limit=5);
	    $data['peraturan']=$this->content_dinamis_db->get_all($content_kategori_id=3, $offset=0, $limit=5);
	    $data['publikasi']=$this->content_dinamis_db->get_all($content_kategori_id=4, $offset=0, $limit=5);
	    
	    if($id){
		$data['halaman_main'] = $this->content_dinamis_db->get($id);
		$data['content'] = 'halaman_single';
	    }else{
		$data['halaman_main'] = $this->content_dinamis_db->get_all($content_kategori_id=3, $offset=0, $limit=5);
		$data['content'] = 'halaman';
	    }
	    $this->load->view('home/main',$data);
	}
	
	function publikasi($id=null){
	    $data['title']='Pelayanan dan Penanganan Pengaduan Masyarakat BOS : Halaman/Publikasi';
	    
	    $data['berita']=$this->content_dinamis_db->get_all($content_kategori_id=1, $offset=0, $limit=5);
	    $data['pengumuman']=$this->content_dinamis_db->get_all($content_kategori_id=2, $offset=0, $limit=5);
	    $data['peraturan']=$this->content_dinamis_db->get_all($content_kategori_id=3, $offset=0, $limit=5);
	    $data['publikasi']=$this->content_dinamis_db->get_all($content_kategori_id=4, $offset=0, $limit=5);
	    
	    if($id){
		$data['halaman_main'] = $this->content_dinamis_db->get($id);
		$data['content'] = 'halaman_single';
	    }else{
		$data['halaman_main'] = $this->content_dinamis_db->get_all($content_kategori_id=4, $offset=0, $limit=5);
		$data['content'] = 'halaman';
	    }
	    $this->load->view('home/main',$data);
	}
}

/* End of file home.php */
/* Location: ./application/controllers/home.php */