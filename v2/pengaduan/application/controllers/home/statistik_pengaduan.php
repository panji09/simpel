<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Statistik_pengaduan extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->helper(array('url','date'));
		$this->load->model(array('admin_handling/region_db', 'admin_handling/content_dinamis_db','admin_handling/content_statis_db', 'admin_handling/content_files_db', 'admin_handling/pengaduan_db', 'select_db', 'admin_handling/respon_db', 'handling/report_db'));
		$this->load->library(array('initlib','session','email'));
		$this->load->database();
		session_start();
		
	}
	
	function index($kategori=null,$tahun=null, $provinsi=null, $kabkota=null){
	    $data['title']='Pelayanan dan Penanganan Pengaduan Masyarakat BOS : Statistik Pengaduan';
		
	    if(!$kategori)
		redirect('statistik_pengaduan/index/report_kategori/all/all/all');
	    
	    if(!$tahun && !$provinsi && !$kabkota)
		redirect('statistik_pengaduan/index/'.$kategori.'/all/all/all');
	    
	    /*
	    if(!$provinsi)
		redirect('statistik_pengaduan/index/'.$kategori.'/all/all/all');
	    
	    if(!$kabkota)
		redirect('statistik_pengaduan/index/'.$kategori.'/all/all/all');
	    */
	    
	    //$data['kabkota'] = $this->region_db->kabkota(array('provinsi_id' => $provinsi));
	    $data['content'] = 'statistik_pengaduan';
	    $this->load->view('home/main',$data);
	}
    
	function post_report(){
	    $kategori = $this->input->post('kategori');
	    $tahun = $this->input->post('tahun');
	    $provinsi = $this->input->post('provinsi');
	    $kabkota = $this->input->post('kabkota');
	    redirect('statistik_pengaduan/index/'.$kategori.'/'.$tahun.'/'.$provinsi.'/'.$kabkota);
	}
	
}

/* End of file home.php */
/* Location: ./application/controllers/home.php */