<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tentang extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->helper(array('url','date'));
		$this->load->model(array('admin_handling/region_db', 'admin_handling/content_dinamis_db','admin_handling/content_statis_db', 'admin_handling/content_files_db', 'admin_handling/pengaduan_db', 'Select_db', 'admin_handling/respon_db'));
		$this->load->library(array('initlib','session','email'));
		$this->load->database();
		session_start();
	}
	
	public function index(){
		$data['title']='Pelayanan dan Penanganan Pengaduan Masyarakat BOS : Tentang';
		
		$data['tentang']=$this->content_statis_db->get('about_bos');
		$data['content'] = 'tentang';
		$this->load->view('home/main',$data);
		
	}
	
}

/* End of file home.php */
/* Location: ./application/controllers/home.php */