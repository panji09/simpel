<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_handling extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
    
    function __construct(){
        parent::__construct();
        $this->load->helper(array('url','date'));
        $this->load->library(array('session','initlib','datatables'));
        $this->load->model(array('select_db','admin_handling/region_db', 'admin_handling/user_handling_db','admin_handling/user_admin_db', 'admin_handling/pengaduan_db'));
        $this->load->database();
        
	$this->initlib->cek_session_admin();
	
	$admin = $this->session->userdata('admin');
	$role_level = $this->user_admin_db->get_role_level($admin->id)->row();
	if(!$role_level->level_id == 1){
	    redirect('admin_handling/login');
	}
    }
    
    function index(){
	$data['title'] = 'User Handling';
	$data['module'] = 'user_handling';
	$data['load_url'] = site_url('admin_handling/user_handling/ajax_user_handling');
	$this->load->view('admin_handling/main',$data);
    }
    
    function add(){
	$data['title'] = 'User Handling Add';
	$data['module'] = 'user_handling_add';
	$data['id'] = null;
	$data['user_handling'] = $this->user_handling_db->get($data['id']);
	$this->load->view('admin_handling/main',$data);
    }
    
    function edit($id=null){
	$data['title'] = 'User Handling Edit';
	$data['module'] = 'user_handling_add';
	$data['id'] = $id;
	$data['user_handling'] = $this->user_handling_db->get($id);
	$this->load->view('admin_handling/main',$data);
    }
    function post_add($id=null){
	$data_person=array(
	    'nama' => $this->input->post('nama'),
	    'nip' => $this->input->post('nip'),
	    'jabatan_dinas' => $this->input->post('jabatan_dinas'),
	    'jabatan_bos' => $this->input->post('jabatan_bos'),
	    'email' => $this->input->post('email'),
	    'alamat_kantor' => $this->input->post('alamat_kantor'),
	    'telp_kantor' => $this->input->post('telp_kantor'),
	    'fax_kantor' => $this->input->post('fax_kantor'),
	    'alamat_rumah' => $this->input->post('alamat_rumah'),
	    'handphone' => $this->input->post('handphone')
	);
	
	$data_user_handling=array(
	    'username' => $this->input->post('username'),
	    'activated' => ($this->input->post('activated') ? 1 : 0),
	    'last_update' => time()
	);
	
	if($this->input->post('password')!='')
	    $data_user_handling['password'] = md5($this->input->post('password'));
	
	$data_role_jenjang = $this->input->post('jenjang');
	$data_role_program = $this->input->post('program');
	
	$data_role_level['level_id'] = $this->input->post('level');
	if($this->input->post('level')==2 && $this->input->post('provinsi')!=''){
	    $data_role_level['provinsi_id'] = $this->input->post('provinsi');
	}elseif($this->input->post('level')==3 && $this->input->post('provinsi')!='' && $this->input->post('kabkota')!=''){
	    $data_role_level['provinsi_id'] = $this->input->post('provinsi');
	    $data_role_level['kabkota_id'] = $this->input->post('kabkota');
	}
	
	if($this->user_handling_db->save($id, $data_person, $data_user_handling, $data_role_jenjang, $data_role_level, $data_role_program)){
            $this->session->set_flashdata('success', array('status' => true, 'msg' => 'sukses'));
        }else{
            $this->session->set_flashdata('success', array('status' => false, 'msg' => 'gagal'));
        }
        
        redirect('admin_handling/user_handling');
    }
    function ajax_user_handling(){
	$this->datatables->select('
	    a.id as id,
	    a.username,
	    if(c.level_id = 1, f.name, if(c.level_id = 2, concat(f.name, " (", h.name, ")"), concat(f.name, " (", h.name, ",", i.name,")"))) as role_level,
	    group_concat(distinct e.name separator ",") as role_jenjang,
	    if(a.activated = 1,"ya","tidak") as aktif,
	    a.id as action,
	    h.name as provinsi,
	    i.name as kabkota
	',false);
	$this->datatables->from('user_handling a');
	$this->datatables->join('role_jenjang b','a.id = b.user_id','left');
	$this->datatables->join('jenjang e', 'e.id = b.jenjang_id', 'left');
	$this->datatables->join('role_level c','a.id = c.user_id','left');
	$this->datatables->join('level f', 'f.id = c.level_id', 'left');
	$this->datatables->join('role_program d','a.id = d.user_id','left');
	$this->datatables->join('program g', 'g.id = d.program_id', 'left');
	$this->datatables->join('provinsi h', 'h.id = c.provinsi_id', 'left');
	$this->datatables->join('kabkota i', 'i.id = c.kabkota_id', 'left');
	
	$this->datatables->where('a.deleted = 0');
	$this->datatables->group_by('a.id','desc');
	//$this->datatables->edit_column('role_level','$1 (daerahnya: $2, $3)','role_level, provinsi, kabkota');
	$this->datatables->edit_column(
	    'action',
	    '
	    <div class="btn-group btn-group-sm" style="min-width: 100px">
		<a href="$2/$1" class="btn btn-default" >Edit</a>
		<button type="button" class="btn btn-default btn_delete" data-href="$3/$1" data-toggle="modal" data-target="#modal_delete">Hapus</button>
	    </div>',
	    'id, site_url("admin_handling/user_handling/edit"), site_url("admin_handling/user_handling/delete")'
	);
	echo $this->datatables->generate();
	//echo $this->db->last_query();
    }
    
    function delete($id){
	
	if($this->user_handling_db->delete($id)){
            $this->session->set_flashdata('success', array('status' => true, 'msg' => 'sukses'));
        }else{
            $this->session->set_flashdata('success', array('status' => false, 'msg' => 'gagal'));
        }
	
	redirect('admin_handling/user_handling');
    }
}
 ?>
