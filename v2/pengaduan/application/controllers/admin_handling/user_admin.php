<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_admin extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
    
    function __construct(){
        parent::__construct();
        $this->load->helper(array('url','date'));
        $this->load->library(array('session','initlib','datatables'));
        $this->load->model(array('select_db','admin_handling/region_db', 'admin_handling/user_admin_db', 'admin_handling/pengaduan_db'));
        $this->load->database();
        
	$this->initlib->cek_session_admin();
	
	$admin = $this->session->userdata('admin');
	$role_level = $this->user_admin_db->get_role_level($admin->id)->row();
	if(!$role_level->level_id == 1){
	    redirect('admin_handling/login');
	}
    }
    
    function index(){
	$data['title'] = 'User Admin';
	$data['module'] = 'user_admin';
	$data['load_url'] = site_url('admin_handling/user_admin/ajax_user_admin');
	$this->load->view('admin_handling/main',$data);
    }
    
    function add(){
	$data['title'] = 'User Admin Add';
	$data['module'] = 'user_admin_add';
	$data['id'] = null;
	$data['user_handling'] = $this->user_admin_db->get($data['id']);
	$this->load->view('admin_handling/main',$data);
    }
    
    function edit($id=null){
	$data['title'] = 'User Admin Edit';
	$data['module'] = 'user_admin_add';
	$data['id'] = $id;
	$data['user_handling'] = $this->user_admin_db->get($id);
	$this->load->view('admin_handling/main',$data);
    }
    function post_add($id=null){
	$data_person=array(
	    'nama' => $this->input->post('nama'),
	    'nip' => $this->input->post('nip'),
	    'jabatan_dinas' => $this->input->post('jabatan_dinas'),
	    'jabatan_bos' => $this->input->post('jabatan_bos'),
	    'email' => $this->input->post('email'),
	    'alamat_kantor' => $this->input->post('alamat_kantor'),
	    'telp_kantor' => $this->input->post('telp_kantor'),
	    'fax_kantor' => $this->input->post('fax_kantor'),
	    'alamat_rumah' => $this->input->post('alamat_rumah'),
	    'handphone' => $this->input->post('handphone')
	);
	
	$data_user_admin=array(
	    'username' => $this->input->post('username'),
	    'activated' => ($this->input->post('activated') ? 1 : 0),
	    'last_update' => time()
	);
	
	if($this->input->post('password')!='')
	    $data_user_admin['password'] = md5($this->input->post('password'));
	
	
	$data_role_level['level_id'] = $this->input->post('level');
	
	if($this->user_admin_db->save($id, $data_person, $data_user_admin, $data_role_level)){
            $this->session->set_flashdata('success', array('status' => true, 'msg' => 'sukses'));
        }else{
            $this->session->set_flashdata('success', array('status' => false, 'msg' => 'gagal'));
        }
        
        redirect('admin_handling/user_admin');
    }
    function ajax_user_admin(){
	$this->datatables->select('
	    a.id as id,
	    a.username,
	    c.name as role_level,
	    if(a.activated = 1,"ya","tidak") as aktif,
	    a.id as action
	',false);
	$this->datatables->from('user a');
	$this->datatables->join('role_level_admin b','a.id = b.user_id','left');
	$this->datatables->join('level_admin c', 'c.id = b.level_id', 'left');
	
	$this->datatables->where('a.deleted = 0');
	$this->datatables->group_by('a.id','desc');
	
	
	$this->datatables->edit_column(
	    'action',
	    '
	    <div class="btn-group btn-group-sm" style="min-width: 100px">
		<a href="$2/$1" class="btn btn-default" >Edit</a>
		<button type="button" class="btn btn-default btn_delete" data-href="$3/$1" data-toggle="modal" data-target="#modal_delete">Hapus</button>
	    </div>',
	    'id, site_url("admin_handling/user_admin/edit"), site_url("admin_handling/user_admin/delete")'
	);
	
	echo $this->datatables->generate();
	//echo $this->db->last_query();
    }
    
    function delete($id){
	
	if($this->user_handling_db->delete($id)){
            $this->session->set_flashdata('success', array('status' => true, 'msg' => 'sukses'));
        }else{
            $this->session->set_flashdata('success', array('status' => false, 'msg' => 'gagal'));
        }
	
	redirect('admin_handling/user_handling');
    }
}
 ?>
