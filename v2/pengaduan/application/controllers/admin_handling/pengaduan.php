<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pengaduan extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
    
    function __construct(){
        parent::__construct();
        $this->load->helper(array('init', 'url','date'));
        $this->load->library(array('session','initlib','datatables'));
        $this->load->model(array('select_db', 'admin_handling/content_statis_db', 'admin_handling/content_dinamis_db', 'admin_handling/content_files_db', 'admin_handling/pengaduan_db', 'admin_handling/user_admin_db'));
        $this->load->database();
        
	$this->initlib->cek_session_admin();
    }
    
    function index(){
	$data['title'] = 'Pengaduan';
	$data['module'] = 'pengaduan';
	$data['load_url'] = site_url('admin_handling/pengaduan/ajax_pengaduan');
	$this->load->view('admin_handling/main',$data);
	//redirect('admin_handling/pengaduan');
    }
    
    function load_pengaduan($id){
	$query = $this->pengaduan_db->get($id);
	
	if($query->num_rows()==1){
	    $pengaduan = $query->row();
	    $query = $this->pengaduan_db->get_param($pengaduan->id);
	    //print_r($query->result());
	    //$param = array();
	    foreach($query->result() as $row){
		$param[$row->key] = $row->value;
	    }
	    echo "<table class='table'>
		  <tbody>
		      <tr>
			  <td><label>Ditujukan Kepada</label></td>
			  <td>Tim ".ucfirst($pengaduan->level)."</td>
		      </tr>
		      <tr>
			  <td><label>Daerah</label></td>
			  <td>".$pengaduan->provinsi.", ".$pengaduan->kabkota.", ".$pengaduan->kecamatan."</td>
		      </tr>
		      <tr>
			  <td><label>Lokasi</label></td>
			  <td>".($pengaduan->lokasi_id == 1 ? $pengaduan->jenjang.' '.ucfirst($pengaduan->status_sekolah).', '.$pengaduan->nama_lokasi : $pengaduan->nama_lokasi)."</td>
		      </tr>
		      <tr>
			  <td><label>Kategori</label></td>
			  <td>".$pengaduan->kategori."</td>
		      </tr>
		      <tr>
			  <td><label>Profesi</label></td>
			  <td>".($pengaduan->sumber_id == 9999 ? $pengaduan->sumber.'('.$pengaduan->sumber_lain.')' : $pengaduan->sumber)."</td>
		      </tr>
		      <tr>
			  <td><label>Nama</label></td>
			  <td>".$pengaduan->nama." (Tampilkan : ".($param['tampil_nama']==1 ? 'Ya' : 'Tidak').")</td>
		      </tr>
		      <tr>
			  <td><label>Email</label></td>
			  <td>".$pengaduan->email."</td>
		      </tr>
		      <tr>
			  <td><label>Telp</label></td>
			  <td>".$pengaduan->handphone." (Tampilkan : ".($param['tampil_telp']==1 ? 'Ya' : 'Tidak').")</td>
		      </tr>
		      <tr>
			  <td><label>Alamat</label></td>
			  <td>".$pengaduan->alamat_rumah." (Tampilkan : ".($param['tampil_alamat']==1 ? 'Ya' : 'Tidak').")</td>
		      </tr>
		      <tr>
			  <td><label>Deskripsi</label></td>
			  <td>".$pengaduan->deskripsi."</td>
		      </tr>
		      <tr>
			  <td>&nbsp</td>
			  <td><input type='checkbox' name='published' value='1' id='published'> Publikasi</td>
		      </tr>
		  </tbody>
	      </table>";
	}
    }
    function ajax_pengaduan(){
	$this->datatables->select('
	    a.id as id,
	    a.kode_pengaduan as kode_pengaduan,
	    a.tanggal as tanggal,
	    i.name as ditujukan,
	    j.name as media,
	    h.name as kategori,
	    group_concat(distinct l.name separator ",") as isu,
	    concat(e.name,", ",d.name,", ",c.name) as lokasi,
	    a.deskripsi as deskripsi,
	    if(a.published = 1,"ya","tidak") as tampilkan,
	    a.approved as approved,
	    a.id as action
	',false);
	
	$this->datatables->from('pengaduan as a');
	$this->datatables->join('person as b','a.person_id = b.id','left');
	$this->datatables->join('kecamatan as c','a.kecamatan_id = c.id','left');
	$this->datatables->join('kabkota as d','a.kabkota_id = d.id','left');
	$this->datatables->join('provinsi as e','a.provinsi_id = e.id','left');
	$this->datatables->join('jenjang as f','a.jenjang_id = f.id','left');
	$this->datatables->join('sumber as g','a.sumber_id = g.id','left');
	$this->datatables->join('kategori as h','a.kategori_id = h.id','left');
	$this->datatables->join('level as i','a.level_id = i.id','left');
	$this->datatables->join('media as j','a.media_id = j.id','left');
	$this->datatables->join('pengaduan_isu as k', 'k.pengaduan_id = a.id', 'left');
	$this->datatables->join('isu as l', 'l.id = k.isu_id', 'left');
	
	$this->datatables->where('a.deleted',0);
	$this->datatables->where('a.media_id',1);
	
	$this->datatables->group_by('a.id','DESC');
	$this->datatables->edit_column('deskripsi','<div class="expander">$1</div>','deskripsi');
	$this->datatables->edit_column('tanggal','$1','mysqldatetime_to_date(tanggal,"d/m/Y, H:i:s")');
	$this->datatables->edit_column('approved','$1','check(approved)');
	$this->datatables->edit_column(
	    'action',
	    '
	    <div class="btn-group btn-group-sm" style="min-width: 100px">
		<button type="button" class="btn btn-default btn_edit" data-id=$1 data-approved=$4 data-published=$5 data-action="$2/$1" data-toggle="modal" data-target="#modal_approved">Terima</button>
		<button type="button" class="btn btn-default btn_delete" data-action="$3/$1" data-toggle="modal" data-target="#modal_delete_reason">Tolak</button>
	    </div>',
	    'id, site_url("admin_handling/pengaduan/approved"), site_url("admin_handling/pengaduan/delete"), verifikasi, tampilkan'
	);
	echo $this->datatables->generate();
    }
    
    function delete($pengaduan_id){
	$data_pengaduan = array(
	    'deleted' => 1,
	    'reason_deleted' => $this->input->post('reason_deleted')
	);
	
	if($this->pengaduan_db->save_pengaduan($pengaduan_id, $data_pengaduan)){
            $this->session->set_flashdata('success', array('status' => true, 'msg' => 'sukses'));
        }else{
            $this->session->set_flashdata('success', array('status' => false, 'msg' => 'gagal'));
        }
	redirect('admin_handling/pengaduan');
    }
    
    function approved($pengaduan_id){
	$data_pengaduan = array(
	    'approved' => 1,
	    'published' => ($this->input->post('published') ? 1 : 0)
	);
	
	
	if($this->pengaduan_db->approved($pengaduan_id, $data_pengaduan)){
	    $query = $this->pengaduan_db->get($pengaduan_id);
	    if($query->num_rows() == 1){
		$row = $query->row();
		    
		$data_email = array(
		    'nama' => $row->nama,
		    'content' => array(
			'Kode Pengaduan' => $row->kode_pengaduan,
			'Publikasi' => ($row->published ? 'Ya' : 'Tidak'),
			'Verifikasi' => ($row->approved ? 'Ya' : 'Tidak'),
			'Ditujukan Kepada' => 'Tim '.ucfirst($row->level),
			'Daerah' => $row->provinsi.', '.$row->kabkota.', '.$row->kecamatan,
			'Lokasi' => ($row->lokasi_id == 1 ? $row->jenjang.' '.$row->status_sekolah.', '.$row->nama_lokasi : $row->nama_lokasi),
			'Kategori' => $row->kategori,
			'Deskripsi' => $row->deskripsi
		    ),
		    'status' => 'Pending'
		);
		
		
		
		//bcc to admin
		$query = $this->user_admin_db->get_all();
		if($query->num_rows()){
		    $email_admin = array();
		    foreach($query->result() as $row_admin){
			$email_admin[] = $row_admin->email;
		    }
		    
		    if($email_admin){
			$email_admin = implode(',',$email_admin);
			$email_admin = explode(',',$email_admin);
		    }
		}
		$message = $this->load->view('email/status_pengaduan', $data_email,true);
		send_email($from=array('name'=>'P3M Kurikulum', 'email' => 'ch.emailer.system@gmail.com'), $to=array($row->email), $cc=array(), $bcc=$email_admin, $subject='P3M Kurikulum | Status Pengaduan', $message);
	    }
	    
            $this->session->set_flashdata('success', array('status' => true, 'msg' => 'sukses'));
        }else{
            $this->session->set_flashdata('success', array('status' => false, 'msg' => 'gagal'));
        }
	redirect('admin_handling/pengaduan');
    }
}
 ?>
