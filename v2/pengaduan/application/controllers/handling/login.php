<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
    
    function __construct(){
        parent::__construct();
        //$this->load->model(array('_TEMPLATE_db'));
	$this->load->helper(array('url','date'));
        $this->load->model(array('handling/login_db', 'admin_handling/user_handling_db'));
	$this->load->library(array('session','initlib'));
        $this->load->database();
    }
    
    function index(){

	$this->load->view('handling/login_view');
    }
    function post_login(){
        $data['username']=$this->input->post('username');
        $data['password']=$this->input->post('password');
        if($islogin=$this->login_db->login_handling($data)){
            $this->session->set_userdata('user_handling',$islogin);
            $this->session->set_userdata('handling_logged_in',true);
            
            //session role_jenjang
            $query=$this->user_handling_db->get_role_jenjang($islogin->id);
            foreach($query->result() as $row){
		$result[]=$row->jenjang_id;
            }
            $this->session->set_userdata('role_jenjang', $result);
            
            //session role_program
            $result=array();
            $query=$this->user_handling_db->get_role_program($islogin->id);
            foreach($query->result() as $row){
		$result[]=$row->program_id;
            }
            $this->session->set_userdata('role_program', $result);
            
            //session role_level
            $result='';
            $result=$this->user_handling_db->get_role_level($islogin->id)->row();
            $this->session->set_userdata('role_level', $result);
            
            redirect('handling');
        }else{
            //echo 'gagal';
            //echo $this->db->last_query();
            $this->session->set_flashdata('login.error','User ID atau Password salah!');
            
            redirect('handling/login');
        }
    }
    function logout(){
	$user_login = $this->session->userdata('user_handling');
        $this->user_handling_db->save_user_handling($user_login->id, array('last_logout' => time()));
        
        $this->session->unset_userdata('handling_logged_in');
	$this->session->unset_userdata('filter');
	$this->session->unset_userdata('role_jenjang');
	$this->session->unset_userdata('role_level');
	$this->session->unset_userdata('role_program');
	$this->session->unset_userdata('user_handling');
	$this->session->unset_userdata('filter_laporan');
	$this->session->unset_userdata('filter_akses_log');
	
        redirect('');
    }
    
}
 ?>
