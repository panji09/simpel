<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Pengaduan extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->helper(array('url','date','init'));
        $this->load->model(array('select_db','admin_handling/region_db','admin_handling/pengaduan_db', 'admin_handling/respon_db', 'admin_handling/person_db','admin_handling/user_handling_db'));
        $this->load->library(array('initlib','session','datatables','email'));
        $this->load->database();
        
	//$this->provinsi=31;
	$this->role_jenjang = $this->session->userdata('role_jenjang');
	$this->role_level = $this->session->userdata('role_level');
	$this->role_program = $this->session->userdata('role_program');
    
	$this->initlib->cek_session_handling();
	
	$this->user_handling = $this->session->userdata('user_handling');
	//print_r( $this->session->userdata('user_handling')->id);
	
	$this->program_id = 4;
    }
    function index(){
	//print_r($_SERVER); die();
        //print_r($this->role_level);
        $user_login=$this->session->userdata('handling');
        $data['title'] = 'Aplikasi P3M : Data Pengaduan';
        $data['module']='mod_handling';
        
        //load ajax pengaduan/filter
        $data['link_ajax_pengaduan']=site_url('handling/pengaduan/ajax_pengaduan');
        //$data['pengaduan']=$this->select_db->handling_pengaduan();
        
        //print_r($data['pengaduan']->result()); echo $this->db->last_query(); die();
        $this->load->view('handling/main_view',$data);
    }
    
    
    function post_respon($pengaduan_id, $respon_id=null){
	$pengaduan_id=base64_decode($pengaduan_id);
	$user_handling = $this->session->userdata('user_handling');
	if($respon_id)
	    $respon_id=base64_decode($respon_id);
	
	$tanggal = $this->input->post('tanggal');
	$tanggal = date_to_mysqldatetime($tanggal.':00');
	$data_respon=array(
	    'pengaduan_id' => $pengaduan_id,
	    'user_handling_id' => $user_handling->id,
	    'tanggal' => $tanggal,
	    'deskripsi' => $this->input->post('uraian'),
	    'last_update' => date("Y-m-d H:i:s"),
	    'status_id' => $this->input->post('status'),
	    'penyelesaian_id' => $this->input->post('penyelesaian')
	);
	
	$data_pengaduan=array(
	    'status_id' => $this->input->post('status')
	);
	
	$pelaku = $this->input->post('pelaku');
	if($this->respon_db->save($respon_id, $data_respon, $data_pengaduan)){
	    $this->pengaduan_db->save_pelaku($pengaduan_id, $pelaku);
            $this->session->set_flashdata('success', array('status' => true, 'msg' => 'sukses'));
        }else{
            $this->session->set_flashdata('success', array('status' => false, 'msg' => 'gagal'));
        }
        
        redirect('handling/pengaduan');
    }
    
    function post_delete_respon($respon_id){
	$respon_id = base64_decode($respon_id);
	
	if($this->respon_db->delete($respon_id)){
	    $this->session->set_flashdata('success', array('status' => true, 'msg' => 'sukses'));
        }else{
            //echo $this->db->last_query();
            $this->session->set_flashdata('success', array('status' => false, 'msg' => 'gagal'));
        }
        
	redirect('handling/pengaduan');
    }
    
    function post_filter_pengaduan(){
        $filter=array(
            'tahun' => $this->input->post('tahun'),
            'triwulan' => $this->input->post('triwulan'),
            'provinsi' => $this->input->post('provinsi'),
            'kabkota' => $this->input->post('kabkota')
        );
        
        $this->session->set_userdata('filter',$filter);
        
        redirect('handling/pengaduan');
    }
    
    function post_pengaduan($id=null){
	if($id)
	    $id=base64_decode($id);
	//print_r($_POST);
	$data_person=array(
	    'nama' => ($this->input->post('nama') != '' ? $this->input->post('nama') : null),
	    'email' => ($this->input->post('email') != '' ? $this->input->post('email') : null),
	    'handphone' => ($this->input->post('telp') != '' ? $this->input->post('telp') : null),
	    'alamat_rumah' => ($this->input->post('alamat') != '' ? $this->input->post('alamat') : null)
	);
	
	$tanggal = $this->input->post('tanggal');
	$tanggal = date_to_mysqldatetime($tanggal.':00');
	
	/*
	$data_pengaduan=array(
	    'program_id' => $this->input->post('program'),
	    'kecamatan_id' => $this->input->post('kecamatan'),
	    'kabkota_id' => $this->input->post('kabkota'),
	    'provinsi_id' => $this->input->post('provinsi'),
	    'jenjang_id' => $this->input->post('jenjang'),
	    'nama_sekolah' => $this->input->post('sekolah'),
	    'tanggal' => $tanggal,
	    'sumber_id' => $this->input->post('sumber_info'),
	    'sumber_lain' => ($this->input->post('sumber_lain')=='' ? null : $this->input->post('sumber_lain')),
	    'kategori_id' => $this->input->post('kategori'),
	    'deskripsi' => $this->input->post('deskripsi'),
	    'last_update' => date('Y-m-d H:i:s'),
	    'media_id' => 3 //lainnya
	);
	*/
	$data_pengaduan=array(
	      'program_id' => $this->program_id,
	      'level_id' => $this->input->post('level'),
	      'kecamatan_id' => $this->input->post('kecamatan'),
	      'kabkota_id' => $this->input->post('kabkota'),
	      'provinsi_id' => $this->input->post('provinsi'),
	      'lokasi_id' => $this->input->post('lokasi'),
	      'jenjang_id' => ($this->input->post('lokasi') == 1 ? $this->input->post('jenjang') : null),
	      'status_sekolah_id' => ($this->input->post('lokasi') == 1 ? $this->input->post('status_sekolah') : null),
	      'nama_lokasi' => htmlentities($this->input->post('nama_lokasi')),
	      'tanggal' => $tanggal,
	      'sumber_id' => $this->input->post('sumber_info'),
	      'sumber_lain' => ($this->input->post('sumber_info')==9999 ? htmlentities($this->input->post('sumber_lain')) : null),
	      'kategori_id' => $this->input->post('kategori'),
	      'deskripsi' => htmlentities($this->input->post('deskripsi')),
	      'last_update' => date('Y-m-d H:i:s'),
	      'media_id' => 3,
	      'token_confirm' => '',
	      'deleted' => 0,
	      'reason_deleted' => '',
	      'user_handling_id' => $this->user_handling->id,
	      'approved' => 1,
	      'published' =>  0
	  );
	$data_param_pengaduan = array(
	    'tampil_nama' => 1,
	    'tampil_telp' => 1,
	    'tampil_alamat' => 1
	);
	//print_r($data_pengaduan);
	
	if($this->pengaduan_db->save($id,$data_person, $data_pengaduan, $data_param_pengaduan)){
		$this->session->set_flashdata('success', array('status' => true, 'msg' => 'sukses'));	
        }else{
            $this->session->set_flashdata('success', array('status' => false, 'msg' => 'gagal'));
        }
	
	//echo $this->db->last_query();
	redirect('handling/pengaduan');
    }
    
    function post_delete_pengaduan($id){
	$id = base64_decode($id);
	
	if($this->pengaduan_db->delete($id)){
	    $this->session->set_flashdata('success', array('status' => true, 'msg' => 'sukses'));
        }else{
            //echo $this->db->last_query();
            $this->session->set_flashdata('success', array('status' => false, 'msg' => 'gagal'));
        }
        
	redirect('handling/pengaduan');
	
    }
    
    function modal_pengaduan($param=null, $id=null){
	
	$data='';
	if($id)
	    $id=base64_decode($id);
	switch($param){
	    case 'add' :
	    break;
	    case 'edit' :
		$data['pengaduan_id']=$id;
		//$data['pengaduan']=$this->pengaduan_db->get($id)->row();
		//$pengaduan_id = $data['pengaduan']->id;
		//$data['respon']=$this->respon_db->get($pengaduan_id)->row();
		//echo $this->db->last_query();
		//print_r($data['pengaduan']); 
	    break;
	}
	
	$this->load->view('handling/modal_pengaduan_view',$data);
    }
    
    function modal_jawab($pengaduan_id){
	
	if($pengaduan_id)
	    $pengaduan_id=base64_decode($pengaduan_id);
	    
	$data['pengaduan']=$this->pengaduan_db->get($pengaduan_id)->row();
	$data['respon']=$this->respon_db->get_all(array('pengaduan_id' => $pengaduan_id));
	//echo $this->db->last_query();
	$this->load->view('handling/modal_jawab_view',$data);
    }
    function form_jawab($respon_id){
	
	$respon_id=base64_decode($respon_id);
	
	$exist = $this->respon_db->exist($respon_id);
	
	if($exist->num_rows() == 1){
	    $respon = $exist->row();
	    $data['pengaduan']=$this->pengaduan_db->get($respon->pengaduan_id)->row();
	    $data['respon']=$this->respon_db->get($respon_id)->row();
	    
	    $this->load->view('handling/form_jawab_view',$data);
	}
	
    }
    function ajax_pengaduan(){
        $filter = $this->session->userdata('filter');
        $role_jenjang = $this->role_jenjang;
        $role_program = $this->role_program;
        $role_level = $this->role_level;
        
	$this->datatables->select("
	    a.id as id,
	    a.tanggal as tanggal,
	    a.kode_pengaduan as kode_pengaduan,
	    h.name as level,
	    b.name as kategori,
	    a.deskripsi as deskripsi,
	    a.nama_lokasi as nama_lokasi,
	    concat(c.name,', ',d.name,', ',e.name) as lokasi,
	    g.name as status,
	    a.id as action,
	    a.level_id as level_id,
	    a.user_handling_id as user_handling_id
	",false);
	$this->datatables->from('pengaduan a');
	$this->datatables->join('kategori b','a.kategori_id = b.id', 'left');
	$this->datatables->join('provinsi c','c.id = a.provinsi_id', 'left');
	$this->datatables->join('kabkota d','d.id = a.kabkota_id', 'left');
	$this->datatables->join('kecamatan e','e.id = a.kecamatan_id', 'left');
	$this->datatables->join('program f','f.id = a.program_id', 'left');
	$this->datatables->join('status g','g.id = a.status_id', 'left');
	$this->datatables->join('level h','h.id = a.level_id', 'left');
	
	$this->datatables->where('a.approved',1);
	$this->datatables->where('a.deleted',0);
	
	//filter
	if($filter){
	    if(isset($filter['tahun']) && $filter['tahun']!='all'){
		$this->datatables->where('YEAR(a.tanggal)', intval($filter['tahun']));
	    }
	    
	    if(isset($filter['triwulan']) && $filter['triwulan']!='all'){
		switch(intval($filter['triwulan'])){
		    case 1 :
			$min=1; $max=3;
		    break;
		    case 2 :
			$min=4; $max=6;
		    break;
		    case 3 :
			$min=7; $max=9;
		    break;
		    case 4 :
			$min=10; $max=12;
		    break;
		}
		
		$this->datatables->where('MONTH(a.tanggal) >=',$min);
		$this->datatables->where('MONTH(a.tanggal) <=',$max);
	    }
	    
	    if(isset($filter['kabkota']) && $filter['kabkota']!='all'){
		$this->datatables->where('a.kabkota_id', intval($filter['kabkota']));
	    }
	    
	    if(isset($filter['provinsi']) && $filter['provinsi']!='all'){
		$this->datatables->where('a.provinsi_id', intval($filter['provinsi']));
	    }
	}
	//end filter
        
        //role
        if($role_level->level_id==2){
	    $this->datatables->where('a.provinsi_id',$role_level->provinsi_id);
	}elseif($role_level->level_id==3){
	    $this->datatables->where('a.provinsi_id',$role_level->provinsi_id);
	    $this->datatables->where('a.kabkota_id',$role_level->kabkota_id);
	}
	$this->datatables->where('a.program_id IN ('.implode(",", $role_program).')');
        $this->datatables->where('(a.jenjang_id IN ('.implode(",", $role_jenjang).') OR a.jenjang_id is null)');
        //$this->datatables->or_where('a.jenjang_id is null');
        //end role
        
        
        $this->datatables->edit_column('tanggal','$1','mysqldatetime_to_date(tanggal,"d/m/Y, H:i:s")');
        $this->datatables->edit_column('deskripsi','<div class="expandable"><p>$1</p></div>','htmlentities(deskripsi)');
        $this->datatables->edit_column('nama_lokasi','$1','html_entity_decode(nama_lokasi)');
        $this->datatables->edit_column(
            'action',
            '
            <div class="btn-group btn-group-sm" style="min-width: 100px">
                <button type="button" data-href="$4/$3" data-action="$5/$3" data-target="#modal_jawab" data-toggle="modal" data-id=$1 class="jawab btn btn-default">Jawab</button>
                <button style="$7" type="button" data-label="Edit Pengaduan" data-action="$6/$3" data-href="$2/$3" data-target="#modal_pengaduan" data-toggle="modal" data-id=$1 class="btn_pengaduan btn btn-default">Edit</button>
                <button style="$7" type="button" data-toggle="modal" data-target="#modal_delete" data-title="Hapus" data-body="Apakah anda yakin menghapus pengaduan ini?" data-href="$8/$3" class="respon-delete btn btn-default">Hapus</button>
            </div>','id, site_url("handling/pengaduan/modal_pengaduan/edit"), base64_encode(id), site_url("handling/pengaduan/modal_jawab"), site_url("handling/pengaduan/post_respon"), site_url("handling/pengaduan/post_pengaduan"), display_edit(user_handling_id), site_url("handling/pengaduan/post_delete_pengaduan")');
	
	echo $this->datatables->generate();
	//echo $this->db->last_query();
        //echo $this->db->last_query();
	/*
	$this->datatables->select('
            a.KdPengaduan as id,
            a.TglKetahui as tanggal,
            a.KdPengaduan as kode_pengaduan,
            b.short as kategori,
            a.NmSekolah as sekolah,
            concat_ws(", ",c.NmProv, d.NmKab, e.NmKec) as lokasi,
            a.Deskripsi as deskripsi,
            concat(lower(f.status),", ",if(a.KdProSel = 3, datediff(a.TglUpdate2, a.TglKetahui), datediff(CURDATE(),a.TglKetahui))," hari"),
            null as action
        ', false);
        $this->datatables->from('g3n_status f, g3n_kategori b,  g3n_propinsi c, g3n_kabupaten d, g3n_pengaduan a');
        $this->datatables->join('g3n_kecamatan e','a.KdKec = e.KdKec','left');
	$this->datatables->where('isdelete = 0');
	$this->datatables->where('a.KdKategori = b.KdKat');
	$this->datatables->where('a.KdProv = c.KdProv');
	$this->datatables->where('a.KdKab = d.KdKab');
	$this->datatables->where('a.KdProSel = f.KdProSel');
        
        //filter tahun
        if($tahun!='all')
            $this->datatables->where('YEAR(a.TglKetahui)', intval($tahun));
        
        if($triwulan!='all'){
            
            switch(intval($triwulan)){
                case 1 :
                    $min=1; $max=3;
                break;
                case 2 :
                    $min=4; $max=6;
                break;
                case 3 :
                    $min=7; $max=9;
                break;
                case 4 :
                    $min=10; $max=12;
                break;
            }
            
            $this->datatables->where('MONTH(a.TglKetahui) >=',$min);
	    $this->datatables->where('MONTH(a.TglKetahui) <=',$max);
        }
        
        
        $this->datatables->where('a.KdProv',$this->provinsi);
        
        //filter kabkota
        if($kabkota!='all')
            $this->datatables->where('a.KdKab', intval($kabkota));
        
        //filter jenjang
        if($filter_jenjang)
            $this->datatables->where('a.KdJenjang IN ('.implode(",", $filter_jenjang).')');
	
	$this->datatables->edit_column('tanggal','$1','mysqldatetime_to_date(tanggal,"d/m/Y, H:i:s")');
        $this->datatables->edit_column('deskripsi','<div class="expandable"><p>$1</p></div>','deskripsi');
        $this->datatables->edit_column(
            'action',
            '
            <div class="btn-group btn-group-sm" style="min-width: 100px">
                <button type="button" data-label="Edit Pengaduan" data-href="$2/$3" data-target="#modal_pengaduan" data-toggle="modal" data-id=$1 class="btn_pengaduan btn btn-default">Edit</button>
                <button type="button" data-href="$4/$3" data-action="$5/$3" data-target="#modal_jawab" data-toggle="modal" data-id=$1 class="jawab btn btn-default">Jawab</button>
            </div>','id,site_url("handling/modal_pengaduan/edit"), base64_encode(id),site_url("handling/modal_jawab"),site_url("handling/post_respon")');
        
        echo $this->datatables->generate();
        */
	
        //$this->db->last_query();
    }
    
    function template(){
        $this->load->view('handling/template_view');
    }
}

?>