<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class User_info extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->helper(array('url','date'));
        $this->load->model(array('select_db','admin_handling/region_db','admin_handling/pengaduan_db', 'admin_handling/person_db', 'admin_handling/user_handling_db', 'admin_handling/respon_db'));
        $this->load->library(array('initlib','session','datatables','email'));
        $this->load->database();
        
	$this->role_jenjang = $this->session->userdata('role_jenjang');
	$this->role_level = $this->session->userdata('role_level');
	$this->role_program = $this->session->userdata('role_program');
    
	$this->initlib->cek_session_handling();
    }
    function index(){
	//print_r($_SERVER); die();
        
        $user_login=$this->session->userdata('user_handling');
        //print_r($user_login);
        $data['title'] = 'Aplikasi P3M : User Info';
        $data['module']='mod_user_info';
        $data['user_handling'] = $this->session->userdata('user_handling');
        $this->load->view('handling/main_view',$data);
    }
    
    function post_update(){
	$data_person=array(
	    'nama' => $this->input->post('nama'),
	    'nip' => $this->input->post('nip'),
	    'jabatan_dinas' => $this->input->post('jabatan_dinas'),
	    'email' => $this->input->post('email'),
	    'alamat_kantor' => $this->input->post('alamat_kantor'),
	    'telp_kantor' => $this->input->post('telp_kantor'),
	    'fax_kantor' => $this->input->post('fax_kantor'),
	    'alamat_rumah' => $this->input->post('alamat_rumah'),
	    'handphone' => $this->input->post('handphone')
	);
	//print_r($data_person);
	$user_login=$this->session->userdata('user_handling');
	//print_r($user_login->person_id);
	if($this->person_db->save($user_login->person_id, $data_person)){
	    
	    $data_person['id'] = $user_login->id;
	    $data_person['person_id'] = $user_login->person_id;
	    $data_person['username'] = $user_login->username;
	    $data_person['last_login'] = $user_login->last_login;
	    
	    $data_person = json_decode(json_encode($data_person), FALSE);
	    $this->session->set_userdata('user_handling',$data_person);
            $this->session->set_flashdata('success', array('status' => true, 'msg' => 'sukses'));
        }else{
            $this->session->set_flashdata('success', array('status' => false, 'msg' => 'gagal'));
        }
        
	redirect('handling/user_info');
    }
    
}

?>